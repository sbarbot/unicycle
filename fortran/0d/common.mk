
OBJRS=$(SRC)/macros.f90 $(patsubst %,$(DST)/%, types.o \
      getopt_m.o getdata.o ode45.o ratestate.o )

$(shell mkdir -p $(DST))

$(DST)/%.o:$(SRC)/%.c
	$(COMPILE.c) $^ -o $(DST)/$*.o

$(DST)/%.o: $(SRC)/%.f $(SRC)/macros.f90
	$(COMPILE.f) $(filter-out $(SRC)/macros.f90,$^) -o $(DST)/$*.o

$(DST)/%.o: $(SRC)/%.f90 $(SRC)/macros.f90
	$(COMPILE.f) $(filter-out $(SRC)/macros.f90,$^) -o $(DST)/$*.o -J $(DST)

unicycle-0d-ratestate: $(filter-out $(SRC)/macros.f90,$(OBJRS))
	$(LINK.f) -o $(DST)/$@ $^ $(LIBS)

clean:
	$(RM) $(DST)/*.o $(DST)/*.mod

.NOTPARALLEL:

