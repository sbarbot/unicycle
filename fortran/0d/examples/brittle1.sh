#!/bin/bash -e

# computes the slip evolution of a spring-slider system with radiation damping
# the spring stiffness is given by 
#
#    k = 7 pi / 16 G / R, 
#
# where R is the radius of the asperity

selfdir=$(dirname $0)

WDIR="$selfdir/brittle1"

if [ ! -e "$WDIR" ]; then
	echo "adding directory $WDIR"
	mkdir -p "$WDIR"
fi

unicycle-0d-ratestate --epsilon 1e-6 --maximum-step 3.15e6 --maximum-iterations 200000 $* <<EOF
# output directory
$WDIR
# elastic moduli
30e3
# time interval
3.15e9
# Vpl radius
 1e-9   1e3
# tau0 mu0 sig    a     b    L   Vo  G/2Vs
    -1 0.6 1e2 0.01 0.014 1e-3 1e-6      5
EOF

gnuplot --persist <<EOF
set yrange [-12:1]
plot '$WDIR/patch.dat' u (\$1/3.15e7):5 w l, -9
quit
EOF


