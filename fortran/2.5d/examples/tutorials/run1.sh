#!/bin/bash

set -e

selfdir=$(dirname $0)

N=800

WDIR=$selfdir/output1
GDIR=$selfdir/greens
FLT=$WDIR/flt.2d

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

echo "" | awk -v N=$N 'BEGIN{i=1;pi=atan2(1.0,0.0)*2.0}{
       	x2o=0;
       	x3o=0;
       	width=25;
       	dip=90;
	rake=90;
	beta=0;
	d2=1+cos(pi*dip/180.0)-1;
	d3=1+sin(pi*dip/180.0)-1;
       	for (j=0;j<N;j++){
       	        x2=x2o+j*d2*width;
       	        x3=x3o+j*d3*width;
       	        printf "%05d %e %16.11e %16.11e %f %f %f %f\n", i,1e-09,x2,x3,width,dip,rake,beta;
       	        i=i+1;
       	}
}' > $FLT

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli (MPa)
30e3 30e3
# time interval (s)
5e9
# number of patches
`grep -v "#" $FLT | wc -l`
# n    Vl   x2   x3  width   dip  rake  beta
`grep -v "#" $FLT`
# number of frictional patches
`grep -v "#" $FLT | wc -l`
# n tau0 mu0 sig    a     b    L   Vo G/2Vs
`grep -v "#" $FLT | awk '{ 
	depth=$4; 
	tau0=-1;
	mu0=0.6;
	sig=100;
	a=1.0e-2;
	b=(depth>5e3 && depth<10e3)?a+4e-3:a-4e-3;
	L=1e-2;
	Vo=1e-6;
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,5;\
	}'`
# number of observation patches
`grep -v "#" $FLT | awk '{if (0 == (NR-1)%4){print $0}}' | wc -l | awk '{print $1+8}'`
# n   index rate
  1      50    1
  2     100    1
  3     150    1
  4     200    1
  5     250    1
  6     300    1
  7     350    1
  8     400    1
# subsample the fault in space and time
`grep -v "#" $FLT | awk 'BEGIN{i=9}{if (0 == (NR-1)%4){print i,NR,20;i=i+1}}'`
# number of observation points
21
# n name    x2 x3
  1 0001 -10e3  0
  2 0002  -9e3  0
  3 0003  -8e3  0
  4 0004  -7e3  0
  5 0005  -6e3  0
  6 0006  -5e3  0
  7 0007  -4e3  0
  8 0008  -3e3  0
  9 0009  -2e3  0
 10 0010  -1e3  0
 11 0011     0  0
 12 0012   1e3  0
 13 0013   2e3  0
 14 0014   3e3  0
 15 0015   4e3  0
 16 0016   5e3  0
 17 0017   6e3  0
 18 0018   7e3  0
 19 0019   8e3  0
 20 0020   9e3  0
 21 0021  10e3  0
# number of events
0
EOF

if [ -e $GDIR/greens-0000.grd ]; then
	IMPORT_GREENS="--import-greens $GDIR"
fi

if [ "0" == "0" ]; then
time mpirun -n 1 unicycle-2d-ratestate \
	--maximum-step 3.15e7 \
	--export-netcdf \
	--export-greens $selfdir/greens \
	$IMPORT_GREENS \
	--maximum-iterations 1000000 \
	$WDIR/in.param
fi


files=`grep -v "#" $FLT | awk 'BEGIN{i=9}{if (0 == (NR-1)%4){print i; i=i+1}}' | \
	awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`

echo "# export $WDIR/step-log10vs.grd"
yRange=`for i in $files; do wc -l $i | awk '{print $1}'; done | minmax -C | awk '{print ($2-1)*20+1}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index 'function z(x){return (0==x)?1e-40:x}{print i,(NR-1)*20+1,log(z($9))/log(10)}'
done | surface -I1/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/step-log10vs.grd

echo "# export $WDIR/step-log10vd.grd"
yRange=`for i in $files; do wc -l $i | awk '{print $1}'; done | minmax -C | awk '{print ($2-1)*20+1}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	grep -v "#" $i | awk -v i=$index 'function z(x){return (0==x)?1e-40:x}{print i,(NR-1)*20+1,log(z($10))/log(10)}'
done | surface -I1/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/step-log10vd.grd

echo "# export $WDIR/slip-log10v.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $2}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$2,$8}'
done | surface -I1/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/slip-log10v.grd

echo "# export $WDIR/time-log10v.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $1/3.15e7}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$1/3.15e7,$8}'
done | \
	blockmean -I4/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -E | awk '{print $1,$2,$6}' | \
	surface -I4/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/time-log10v.grd


