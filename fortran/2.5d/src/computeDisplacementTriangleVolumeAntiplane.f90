!------------------------------------------------------------------------
!! subroutine computeDisplacementTriangleVolumeAntiplane
!! computes the displacement field due to plastic strain in triangle volume
!! element, following
!!
!!   Barbot S., Deformation of a Half-Space from Anelastic Strain Confined
!!   in a Tetrahedral Volume, submitted to Bull. Seism. Soc. Am., 2018.
!!
!! and considering the following geometry:
!!
!!              surface
!!      -------------+-------------- E (x2)
!!                   |
!!                   |     + A
!!                   |    /  . 
!!                   |   /     .  
!!                   |  /        .            
!!                   | /           .      
!!                   |/              + B
!!                   /            .
!!                  /|          /  
!!                 / :       .
!!                /  |    /
!!               /   : .
!!              /   /|
!!             / .   :
!!            +      |
!!          C        :
!!                   |
!!                   D (x3)
!!
!!
!! INPUT:
!! x2, x3             east coordinates and depth of the observation point,
!! A, B, C            coordinates of the 3 vertices A = ( xA, zA),
!! eij                source strain components 12 and 13 in the volume element,
!!
!! OUTPUT:
!! u1                 displacement in strike direction
!!
!! \author Sylvain Barbot (08/23/20) - original Fortran form
!-----------------------------------------------------------------------
SUBROUTINE computeDisplacementTriangleVolumeAntiplane( &
                x2,x3,A,B,C,e12,e13,u1)

  IMPLICIT NONE

  REAL*8, INTENT(IN) :: x2,x3
  REAL*8, DIMENSION(2), INTENT(IN) :: A,B,C
  REAL*8, INTENT(IN) :: e12,e13
  REAL*8, INTENT(OUT) :: u1
    
  REAL*8, PARAMETER :: PI = 3.141592653589793115997963468544185161_8

  ! normal vectors
  REAL*8, DIMENSION(2) :: nA, nB, nC

  IF (0 .GT. x3) THEN
     WRITE (0,'("error: observation depth (x3) must be positive")')
     STOP 1
  END IF

  IF ((0 .GT. A(2)) .OR. &
      (0 .GT. B(2)) .OR. &
      (0 .GT. C(2))) THEN
     WRITE (0,'("error: vertex depth must be positive")')
     STOP 1
  END IF

  ! unit vectors
  nA = (/ C(2)-B(2), &
          B(1)-C(1) /) / NORM2(C-B)
  nB = (/ C(2)-A(2), &
          A(1)-C(1) /) / NORM2(C-A)
  nC = (/ B(2)-A(2), &
          A(1)-B(1) /) / NORM2(B-A)
  
  ! check that unit vectors are pointing outward
  IF (DOT_PRODUCT(nA,A-(B+C)/2) .GT. 0) THEN
      nA=-nA
  END IF
  IF (DOT_PRODUCT(nB,B-(A+C)/2) .GT. 0) THEN
      nB=-nB
  END IF
  IF (DOT_PRODUCT(nC,C-(A+B)/2) .GT. 0) THEN
      nC=-nC
  END IF

  ! stress components
  u1=2*(e12*nC(1)+e13*nC(2))*T11(A,B) &
    +2*(e12*nA(1)+e13*nA(2))*T11(B,C) &
    +2*(e12*nB(1)+e13*nB(2))*T11(C,A)

CONTAINS

  REAL*8 FUNCTION T11(A,B)
    IMPLICIT NONE
    REAL*8, DIMENSION(2), INTENT(IN) :: A, B

    REAL*8, DIMENSION(2) :: xA,xB

    ! basis vectors
    REAL*8, DIMENSION(2) :: av,nv

    ! initialize
    xA=A
    xB=B

    ! radial vector
    av=(xB-xA)/2
    av=av/NORM2(av)

    ! normal vector
    nv=(/ -av(2), av(1) /)

    T11=(gFunction(xB,av,nv)-gFunction(xA,av,nv))/(4*pi)

    ! image
    xA(2)=-xA(2)
    xB(2)=-xB(2)

    ! radial vector
    av=(xB-xA)/2
    av=av/NORM2(av)

    ! normal vector
    nv=(/ -av(2), av(1) /)

    T11=T11+(gFunction(xB,av,nv)-gFunction(xA,av,nv))/(4*pi)

  END FUNCTION T11

  REAL*8 FUNCTION gFunction(r,a,n)
    IMPLICIT NONE
    REAL*8, DIMENSION(2), INTENT(IN) :: r
    REAL*8, DIMENSION(2), INTENT(IN) :: a,n

    gFunction=-(a(1)*(x2-r(1))+a(2)*(x3-r(2))) * LOG( (x2-r(1))**2+(x3-r(2))**2 ) &
        -2*(n(1)*(x2-r(1))+n(2)*(x3-r(2))) * ATAN( (a(1)*(x2-r(1))+a(2)*(x3-r(2))) &
                                                  /(n(1)*(x2-r(1))+n(2)*(x3-r(2))) )

  END FUNCTION gFunction

END SUBROUTINE computeDisplacementTriangleVolumeAntiplane

