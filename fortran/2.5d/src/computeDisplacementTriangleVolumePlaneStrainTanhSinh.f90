!------------------------------------------------------------------------
!! subroutine computeDisplacementTriangleVolumePlaneStrainTanhSinh
!! computes the displacement field associated with triangle strain volumes 
!! using the double-exponential numerical integration, as in 
!!
!!   Barbot S., "Deformation of a Half-Space from Anelastic Strain Confined
!!   in a Tetrahedral Volume", submitted to Bull. Seism. Soc. Am., 2018.
!!
!! and considering the following geometry:
!!
!!              surface
!!      -------------+-------------- E (x2)
!!                   |
!!                   |     + A
!!                   |    /  . 
!!                   |   /     .  
!!                   |  /        .            
!!                   | /           .      
!!                   |/              + B
!!                   /            .
!!                  /|          /  
!!                 / :       .
!!                /  |    /
!!               /   : .
!!              /   /|
!!             / .   :
!!            +      |
!!          C        :
!!                   |
!!                   D (x3)
!!
!!
!! INPUT:
!! x2, x3             east coordinates and depth of the observation point,
!! A, B, C            coordinates of the 3 vertices A = ( xA, zA),
!! eij                source strain component 22, 23 and 33 in the shear zone,
!! nu                 Poisson's ratio the half space.
!!
!! OUTPUT:
!! u2                 displacement component in the east direction,
!! u3                 displacement component in the depth direction.
!!
!! \author Sylvain Barbot (03/17/18) - original Fortran form
!-----------------------------------------------------------------------
SUBROUTINE computeDisplacementTriangleVolumePlaneStrainTanhSinh( &
                              x2,x3,A,B,C, &
                              e22,e23,e33,nu,u2,u3)

  IMPLICIT NONE

  REAL*8, INTENT(IN) :: x2,x3
  REAL*8, DIMENSION(2), INTENT(IN) :: A,B,C
  REAL*8, INTENT(IN) :: e22,e23,e33
  REAL*8, INTENT(IN) :: nu
  REAL*8, INTENT(OUT) :: u2,u3
    
  REAL*8, PARAMETER :: PI = 3.141592653589793115997963468544185161_8

  ! normal vectors
  REAL*8, DIMENSION(2) :: nA, nB, nC

  ! trace
  REAL*8 :: ekk

  ! moment density
  REAL*8 :: m22,m23,m33

  ! Lame parameter
  REAL*8 :: lambda

  ! double integration coordinates and weight
  REAL*8 :: xk,wk

  ! counter
  INTEGER :: k

  ! the parameters h=0.01 and n=300 give 16 digits of accuracy
  ! double integration precision
  REAL*8, PARAMETER :: h=0.01
  ! double integration number of integration points (n=int(1/h*3))
  INTEGER, PARAMETER :: n=300

  ! check valid parameters
  IF ((-1._8 .GT. nu) .OR. (0.5_8 .LT. nu)) THEN
     WRITE (0,'("error: -1<=nu<=0.5, nu=",ES9.2E2," given.")') nu
     STOP 1
  END IF

  IF (0 .GT. x3) THEN
     WRITE (0,'("error: observation depth (x3) must be positive")')
     STOP 1
  END IF

  IF ((0 .GT. A(2)) .OR. &
      (0 .GT. B(2)) .OR. &
      (0 .GT. C(2))) THEN
     WRITE (0,'("error: vertex depth must be positive")')
     STOP 1
  END IF

  ! Lame parameter
  lambda=2*nu/(1-2*nu)

  ! isotroPIc strain
  ekk=e22+e33

  ! unit vectors
  nA = (/ C(2)-B(2), &
          B(1)-C(1) /) / NORM2(C-B)
  nB = (/ C(2)-A(2), &
          A(1)-C(1) /) / NORM2(C-A)
  nC = (/ B(2)-A(2), &
          A(1)-B(1) /) / NORM2(B-A)
  
  ! check that unit vectors are pointing outward
  IF (DOT_PRODUCT(nA,A-(B+C)/2) .GT. 0) THEN
      nA=-nA
  END IF
  IF (DOT_PRODUCT(nB,B-(A+C)/2) .GT. 0) THEN
      nB=-nB
  END IF
  IF (DOT_PRODUCT(nC,C-(A+B)/2) .GT. 0) THEN
      nC=-nC
  END IF

  ! moment density
  m22=lambda*ekk+2*e22
  m23=2*e23
  m33=lambda*ekk+2*e33

  ! numerical solution
  u2=0.d0
  u3=0.d0
  DO k=-n,n
     wk=(0.5*h*PI*COSH(k*h))/(COSH(0.5*PI*SINH(k*h)))**2
     xk=TANH(0.5*PI*SINH(k*h))
     u2=u2+wk*IU2(xk)
     u3=u3+wk*IU3(xk)
  END DO

CONTAINS

  ! linear combinations of Green's functions
  REAL*8 FUNCTION IU2(t)
    REAL*8, INTENT(IN) :: t

    IU2= (m22*nC(1)+m23*nC(2))*NORM2(B-A)/2*G22(y(t,A(1),B(1)),y(t,A(2),B(2))) &
        +(m23*nC(1)+m33*nC(2))*NORM2(B-A)/2*G32(y(t,A(1),B(1)),y(t,A(2),B(2))) &
        +(m22*nA(1)+m23*nA(2))*NORM2(C-B)/2*G22(y(t,B(1),C(1)),y(t,B(2),C(2))) &
        +(m23*nA(1)+m33*nA(2))*NORM2(C-B)/2*G32(y(t,B(1),C(1)),y(t,B(2),C(2))) &
        +(m22*nB(1)+m23*nB(2))*NORM2(A-C)/2*G22(y(t,C(1),A(1)),y(t,C(2),A(2))) &
        +(m23*nB(1)+m33*nB(2))*NORM2(A-C)/2*G32(y(t,C(1),A(1)),y(t,C(2),A(2)))

  END FUNCTION IU2

  ! linear combinations of Green's functions
  REAL*8 FUNCTION IU3(t)
    REAL*8, INTENT(IN) :: t

    IU3= (m22*nC(1)+m23*nC(2))*NORM2(B-A)/2*G23(y(t,A(1),B(1)),y(t,A(2),B(2))) &
        +(m23*nC(1)+m33*nC(2))*NORM2(B-A)/2*G33(y(t,A(1),B(1)),y(t,A(2),B(2))) &
        +(m22*nA(1)+m23*nA(2))*NORM2(C-B)/2*G23(y(t,B(1),C(1)),y(t,B(2),C(2))) &
        +(m23*nA(1)+m33*nA(2))*NORM2(C-B)/2*G33(y(t,B(1),C(1)),y(t,B(2),C(2))) &
        +(m22*nB(1)+m23*nB(2))*NORM2(A-C)/2*G23(y(t,C(1),A(1)),y(t,C(2),A(2))) &
        +(m23*nB(1)+m33*nB(2))*NORM2(A-C)/2*G33(y(t,C(1),A(1)),y(t,C(2),A(2)))

  END FUNCTION IU3

  ! interpolate from a to b
  REAL*8 FUNCTION y(t,a,b)
    REAL*8, INTENT(IN) :: t,a,b

    y=(a+b)/2+t*(b-a)/2

  END FUNCTION y

  !---------------------------------------------------------------
  !> function G22
  !! Green's function G22 for an elastic half space in plane strain
  !! corresponding to the displacement in the x2 direction due to a
  !! line force in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G22(y2,y3)
    REAL*8, INTENT(IN) :: y2,y3

    G22=-1/(2*pi*(1-nu))*( &
          (3-4*nu)/4*log(r1(y2,y3))+(8*nu**2-12*nu+5)/4*log(r2(y2,y3))+(x3-y3)**2/r1(y2,y3)**2/4 &
         +((3-4*nu)*(x3+y3)**2+2*y3*(x3+y3)-2*y3**2)/r2(y2,y3)**2/4-(y3*x3*(x3+y3)**2)/r2(y2,y3)**4 &
        )
  END FUNCTION G22

  !---------------------------------------------------------------
  !> function G23
  !! Green's function G23 for an elastic half space in plane strain
  !! corresponding to the displacement in the x3 direction due to a
  !! line force in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G23(y2,y3)
    REAL*8, INTENT(IN) :: y2,y3

    G23= 1/(2*pi*(1-nu))*( &
         (1-2*nu)*(1-nu)*atan((x2-y2)/(x3+y3))+((x3-y3)*(x2-y2))/r1(y2,y3)**2/4 &
         +(3-4*nu)*((x2-y2)*(x3-y3))/r2(y2,y3)**2/4-(y3*x3*(x2-y2)*(x3+y3))/r2(y2,y3)**4 &
        )
  END FUNCTION G23

  !---------------------------------------------------------------
  !> function G32
  !! Green's function G32 for an elastic half space in plane strain
  !! corresponding to the displacement in the x2 direction due to a
  !! line force in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G32(y2,y3)
    REAL*8, INTENT(IN) :: y2,y3

    G32= 1/(2*pi*(1-nu))*( &
         -(1-2*nu)*(1-nu)*atan((x2-y2)/(x3+y3)) &
         +(x3-y3)*(x2-y2)/r1(y2,y3)**2/4 &
         +(3-4*nu)*(x2-y2)*(x3-y3)/r2(y2,y3)**2/4 &
         +y3*x3*(x2-y2)*(x3+y3)/r2(y2,y3)**4 &
        )

  END FUNCTION G32

  !---------------------------------------------------------------
  !> function G33
  !! Green's function G33 for an elastic half space in plane strain
  !! corresponding to the displacement in the x3 direction due to a
  !! line force in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G33(y2,y3)
    REAL*8, INTENT(IN) :: y2,y3

    G33= 1/(2*pi*(1-nu))*( &
         -(3-4*nu)/4*log(r1(y2,y3))-(8*nu**2-12*nu+5)/4*log(r2(y2,y3)) &
         -(x2-y2)**2/r1(y2,y3)**2/4+(2*y3*x3-(3-4*nu)*(x2-y2)**2)/r2(y2,y3)**2/4-y3*x3*(x2-y2)**2/r2(y2,y3)**4 &
        )

  END FUNCTION G33

  ! Radii
  REAL*8 FUNCTION r1(y2,y3)
    REAL*8, INTENT(IN) :: y2,y3

    r1=SQRT((x2-y2)**2+(x3-y3)**2)

  END FUNCTION r1

  REAL*8 FUNCTION r2(y2,y3)
    REAL*8, INTENT(IN) :: y2,y3

    r2=SQRT((x2-y2)**2+(x3+y3)**2)

  END FUNCTION r2

END SUBROUTINE computeDisplacementTriangleVolumePlaneStrainTanhSinh



