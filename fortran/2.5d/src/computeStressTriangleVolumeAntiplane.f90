!------------------------------------------------------------------------
!! subroutine computeStressTriangleVolumeAntiplane
!! computes the stress field due to plastic strain in triangle volume element
!! following
!!
!!   Barbot S., Deformation of a Half-Space from Anelastic Strain Confined
!!   in a Tetrahedral Volume, submitted to Bull. Seism. Soc. Am., 2018.
!!
!! and considering the following geometry:
!!
!!              surface
!!      -------------+-------------- E (x2)
!!                   |
!!                   |     + A
!!                   |    /  . 
!!                   |   /     .  
!!                   |  /        .            
!!                   | /           .      
!!                   |/              + B
!!                   /            .
!!                  /|          /  
!!                 / :       .
!!                /  |    /
!!               /   : .
!!              /   /|
!!             / .   :
!!            +      |
!!          C        :
!!                   |
!!                   D (x3)
!!
!!
!! INPUT:
!! x2, x3             east coordinates and depth of the observation point,
!! A, B, C            coordinates of the 3 vertices A = ( xA, zA),
!! eij                source strain components 12 and 13 in the volume element,
!!
!! G                  rigidity in the half space.
!!
!! OUTPUT:
!! s12                horizontal shear stress,
!! s13                vertical shear stress.
!!
!! \author Sylvain Barbot (04/10/18) - original Fortran form
!-----------------------------------------------------------------------
SUBROUTINE computeStressTriangleVolumeAntiplane( &
                x2,x3,A,B,C,e12,e13,G,s12,s13)

  IMPLICIT NONE

  REAL*8, INTENT(IN) :: x2,x3
  REAL*8, DIMENSION(2), INTENT(IN) :: A,B,C
  REAL*8, INTENT(IN) :: e12,e13
  REAL*8, INTENT(IN) :: G
  REAL*8, INTENT(OUT) :: s12,s13
    
  REAL*8, PARAMETER :: PI = 3.141592653589793115997963468544185161_8

  ! Heaviside function
  REAL*8, EXTERNAL :: heaviside

  ! normal vectors
  REAL*8, DIMENSION(2) :: nA, nB, nC

  ! eigenstrain
  REAL*8 :: omega

  IF (0 .GT. x3) THEN
     WRITE (0,'("error: observation depth (x3) must be positive")')
     STOP 1
  END IF

  IF ((0 .GT. A(2)) .OR. &
      (0 .GT. B(2)) .OR. &
      (0 .GT. C(2))) THEN
     WRITE (0,'("error: vertex depth must be positive")')
     STOP 1
  END IF

  ! unit vectors
  nA = (/ C(2)-B(2), &
          B(1)-C(1) /) / NORM2(C-B)
  nB = (/ C(2)-A(2), &
          A(1)-C(1) /) / NORM2(C-A)
  nC = (/ B(2)-A(2), &
          A(1)-B(1) /) / NORM2(B-A)
  
  ! check that unit vectors are pointing outward
  IF (DOT_PRODUCT(nA,A-(B+C)/2) .GT. 0) THEN
      nA=-nA
  END IF
  IF (DOT_PRODUCT(nB,B-(A+C)/2) .GT. 0) THEN
      nB=-nB
  END IF
  IF (DOT_PRODUCT(nC,C-(A+B)/2) .GT. 0) THEN
      nC=-nC
  END IF

  ! remove anelastic strain
  omega=heaviside(((A(1)+B(1))/2-x2)*nC(1)+((A(2)+B(2))/2-x3)*nC(2)) &
       *heaviside(((B(1)+C(1))/2-x2)*nA(1)+((B(2)+C(2))/2-x3)*nA(2)) &
       *heaviside(((C(1)+A(1))/2-x2)*nB(1)+((C(2)+A(2))/2-x3)*nB(2))

  ! stress components
  s12=2*(e12*nC(1)+e13*nC(2))*T112(A,B) &
     +2*(e12*nA(1)+e13*nA(2))*T112(B,C) &
     +2*(e12*nB(1)+e13*nB(2))*T112(C,A)

  s13=2*(e12*nC(1)+e13*nC(2))*T113(A,B) &
     +2*(e12*nA(1)+e13*nA(2))*T113(B,C) &
     +2*(e12*nB(1)+e13*nB(2))*T113(C,A)

  s12=2*G*(s12-omega*e12)
  s13=2*G*(s13-omega*e13)

CONTAINS

  REAL*8 FUNCTION T112(A,B)
    IMPLICIT NONE
    REAL*8, DIMENSION(2), INTENT(IN) :: A, B

    REAL*8, DIMENSION(2) :: xA,xB

    ! basis vectors
    REAL*8, DIMENSION(2) :: av,nv

    ! initialize
    xA=A
    xB=B

    ! radial vector
    av=(xB-xA)/2;
    av=av/NORM2(av);

    ! normal vector
    nv=(/ -av(2), av(1) /)

    t112=gamma2(xB,av,nv)-Gamma2(xA,av,nv);

    ! image
    xA(2)=-xA(2)
    xB(2)=-xB(2)

    ! radial vector
    av=(xB-xA)/2
    av=av/NORM2(av)

    ! normal vector
    nv=(/ -av(2), av(1) /)

    t112=t112+(gamma2(xB,av,nv)-gamma2(xA,av,nv))

  END FUNCTION T112

  REAL*8 FUNCTION T113(A,B)
    IMPLICIT NONE
    REAL*8, DIMENSION(2), INTENT(IN) :: A, B

    REAL*8, DIMENSION(2) :: xA,xB

    ! basis vectors
    REAL*8, DIMENSION(2) :: av,nv

    ! initialize
    xA=A
    xB=B

    ! radial vector
    av=(xB-xA)/2
    av=av/NORM2(av)

    ! normal vector
    nv=(/ -av(2), av(1) /)

    t113=gamma3(xB,av,nv)-Gamma3(xA,av,nv)

    ! image
    xA(2)=-xA(2)
    xB(2)=-xB(2)

    ! radial vector
    av=(xB-xA)/2
    av=av/NORM2(av)

    ! normal vector
    nv=(/ -av(2), av(1) /)

    t113=t113+(gamma3(xB,av,nv)-gamma3(xA,av,nv))

  END FUNCTION T113

  REAL*8 FUNCTION gamma2(r,a,n)
    IMPLICIT NONE
    REAL*8, DIMENSION(2), INTENT(IN) :: r
    REAL*8, DIMENSION(2), INTENT(IN) :: a,n

    gamma2= a(1)*LOG( (x2-r(1))**2+(x3-r(2))**2 )/(8*pi) &
           +n(1)*ATAN( (a(1)*(x2-r(1))+a(2)*(x3-r(2))) &
                      /(n(1)*(x2-r(1))+n(2)*(x3-r(2))) )/(4*pi)

  END FUNCTION gamma2

  REAL*8 FUNCTION gamma3(r,a,n)
    IMPLICIT NONE
    REAL*8, DIMENSION(2), INTENT(IN) :: r
    REAL*8, DIMENSION(2), INTENT(IN) :: a,n

    gamma3= a(2)*LOG( (x2-r(1))**2+(x3-r(2))**2 )/(8*pi) &
           +n(2)*ATAN( (a(1)*(x2-r(1))+a(2)*(x3-r(2))) &
                      /(n(1)*(x2-r(1))+n(2)*(x3-r(2))) )/(4*pi)

  END FUNCTION gamma3

END SUBROUTINE computeStressTriangleVolumeAntiplane

