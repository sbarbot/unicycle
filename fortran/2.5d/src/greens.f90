!-----------------------------------------------------------------------
! Copyright 2020 Sylvain Barbot
!
! This file is part of UNICYCLE
!
! UNICYCLE is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! UNICYCLE is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with UNICYCLE.  If not, see <http://www.gnu.org/licenses/>.
!-----------------------------------------------------------------------

#include "macros.f90"

MODULE greens

  IMPLICIT NONE

  PUBLIC

  !------------------------------------------------------------------------
  !! The Green's function for traction and stress interaction amongst 
  !! dislocations and strain volumes has the following layout
  !!
  !!       / KK KR KT \
  !!   G = |          |
  !!       \ LK LR LT /
  !!
  !! where
  !!
  !!   KK is the matrix for traction on faults due to fault slip
  !!   KR is the matrix for traction on faults due to strain in rectangle volume elements
  !!   KT is the matrix for traction on faults due to strain in triangle volume elements
  !!   LK is the matrix for stress in volumes due to fault slip
  !!   LR is the matrix for stress in volumes due to strain in rectangle volume elements
  !!   LT is the matrix for stress in volumes due to strain in triangle volume elements
  !!
  !! The functions provided in this module compute the matrices KK, KR, KT
  !! LK, LR, and LL separately so they can be subsequently combined.
  !------------------------------------------------------------------------


CONTAINS

  !-----------------------------------------------------------------------
  !> subroutine initG
  !! initiate the stress interaction matrix G
  !----------------------------------------------------------------------
  SUBROUTINE initG(in,layout,G)
    USE types
    IMPLICIT NONE
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in
    TYPE(LAYOUT_STRUCT), INTENT(IN) :: layout
    REAL*8, DIMENSION(:,:), ALLOCATABLE, INTENT(OUT) :: G

    INCLUDE 'mpif.h'

    INTEGER :: ierr,rank,csize
    INTEGER :: m,n

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

    ! number of columns
    n=in%patch%ns*DGF_PATCH &
     +in%rectangleVolume%ns*DGF_VOLUME &
     +in%triangleVolume%ns*DGF_VOLUME

    ! number of rows in current thread
    m=layout%listForceN(1+rank)

    ALLOCATE(G(n,m),STAT=ierr)
    IF (0 /= ierr) STOP "could not allocate the Greens function matrix"

  END SUBROUTINE initG

  !-----------------------------------------------------------------------
  !> subroutine buildG
  !! Builds the stress interaction matrix G
  !----------------------------------------------------------------------
  SUBROUTINE buildG(in,layout,G)
    USE kernels
    USE types
    IMPLICIT NONE
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in
    TYPE(LAYOUT_STRUCT), INTENT(IN) :: layout
    REAL*8, DIMENSION(:,:), ALLOCATABLE, INTENT(OUT) :: G

    INCLUDE 'mpif.h'

    INTEGER :: ierr,rank,csize
    INTEGER :: elementIndex,elementType

    INTEGER :: i,k,n,m

    ! traction kernels
    REAL*8, DIMENSION(:,:,:), ALLOCATABLE :: KK,KR,KT

    ! stress kernels
    REAL*8, DIMENSION(:,:,:), ALLOCATABLE :: LK,LR,LT
  
    ! identity matrix
    REAL*8, DIMENSION(5,5), PARAMETER :: &
               eye=RESHAPE( (/ 1._8,0._8,0._8,0._8,0._8, &
                               0._8,1._8,0._8,0._8,0._8, &
                               0._8,0._8,1._8,0._8,0._8, &
                               0._8,0._8,0._8,1._8,0._8, &
                               0._8,0._8,0._8,0._8,1._8 /), (/ 5,5 /))
  
    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

    ! number of columns
    n=in%patch%ns*DGF_PATCH &
     +in%rectangleVolume%ns*DGF_VOLUME &
     +in%triangleVolume%ns*DGF_VOLUME

    ! number of rows in current thread
    m=layout%listForceN(1+rank)

    ALLOCATE(G(n,m),STAT=ierr)

    ! three types of traction sources
    ALLOCATE(KK(DGF_PATCH,in%patch%ns,DGF_VECTOR), &
             KR(DGF_VOLUME,in%rectangleVolume%ns,DGF_VECTOR), &
             KT(DGF_VOLUME,in%triangleVolume%ns,DGF_VECTOR),STAT=ierr)
    IF (0 /= ierr) STOP "could not allocate the traction kernels"
      
    ! three types of stress sources
    ALLOCATE(LK(DGF_PATCH,in%patch%ns,DGF_TENSOR), &
             LR(DGF_VOLUME,in%rectangleVolume%ns,DGF_TENSOR), &
             LT(DGF_VOLUME,in%triangleVolume%ns,DGF_TENSOR),STAT=ierr)
    IF (0 /= ierr) STOP "could not allocate the kernels"
      
    ! loop over elements owned by current thread
    k=1
    DO i=1,SIZE(layout%elementIndex)
       elementType= layout%elementType(i)
       elementIndex=layout%elementIndex(i)

       SELECT CASE (elementType)
       CASE (FLAG_PATCH)
          ! G(:,k:k+layout%elementForceDGF(i)-1) = Green's function
          CALL tractionRows(in%patch%xc(:,elementIndex), &
                            in%patch%sv(:,elementIndex), &
                            in%patch%dv(:,elementIndex), &
                            in%patch%nv(:,elementIndex), &
                            layout%elementForceDGF(i),n,G(:,k))
       CASE (FLAG_RECTANGLE_VOLUME)
          ! G(:,k:k+layout%elementForceDGF(i)-1)= Green's function
          CALL stressRows(in%rectangleVolume%xc(:,elementIndex), &
                          in%rectangleVolume%sv(:,elementIndex), &
                          in%rectangleVolume%dv(:,elementIndex), &
                          in%rectangleVolume%nv(:,elementIndex), &
                          layout%elementForceDGF(i),n,G(:,k))
       CASE (FLAG_TRIANGLE_VOLUME)
          ! G(:,k:k+layout%elementForceDGF(i)-1)= Green's function
          CALL stressRows(in%triangleVolume%xc(:,elementIndex), &
                          in%triangleVolume%sv(:,elementIndex), &
                          in%triangleVolume%dv(:,elementIndex), &
                          in%triangleVolume%nv(:,elementIndex), &
                          layout%elementForceDGF(i),n,G(:,k))
       END SELECT

       k=k+layout%elementForceDGF(i)
    END DO

    DEALLOCATE(KK,KR,KT,LK,LR,LT)

  CONTAINS

    !-----------------------------------------------------------------------
    !> subroutine tractionRows
    !! evaluates all the relevant rows for traction change due to slip (strike
    !! slip and dip slip) on fault patches, and strain
    !! (12, 13, 22, 23, 33) in volume elements.
    !!
    !! \author Sylvain Barbot (08/23/2020)
    !----------------------------------------------------------------------
    SUBROUTINE tractionRows(xc,sv,dv,nv,m,n,rows)
      IMPLICIT NONE
      REAL*8, DIMENSION(3), INTENT(IN) :: xc,sv,dv,nv
      INTEGER, INTENT(IN) :: m,n
      REAL*8, DIMENSION(n*m), INTENT(OUT) :: rows
  
      INTEGER :: j
  
      !----------------------------------
      ! fault patch
      !----------------------------------
      DO j=1,DGF_PATCH
         CALL computeTractionKernelsPatch2d(xc,sv,dv,nv, &
                 in%patch%ns,in%patch%x, &
                 in%patch%width, &
                 in%patch%dip, &
                 in%patch%beta, &
                 eye(j,1),eye(j,2), &
                 in%mu,in%lambda, &
                 KK(j,:,1),KK(j,:,2),KK(j,:,3))
      END DO
  
      !----------------------------------
      ! rectangle volume element
      !----------------------------------
      DO j=1,DGF_VOLUME
         CALL computeTractionKernelsRectangleVolume2d(xc,sv,dv,nv, &
                 in%rectangleVolume%ns,in%rectangleVolume%x, &
                 in%rectangleVolume%thickness,in%rectangleVolume%width, &
                 in%rectangleVolume%dip, &
                 eye(j,1),eye(j,2),eye(j,3),eye(j,4),eye(j,5), &
                 in%mu,in%lambda, &
                 KR(j,:,1),KR(j,:,2),KR(j,:,3))
      END DO
  
      !----------------------------------
      ! triangle volume element
      !----------------------------------
      DO j=1,DGF_VOLUME
         CALL computeTractionKernelsTriangleVolume2d(xc,sv,dv,nv, &
                 in%triangleVolume%ns, &
                 in%triangleVolume%i1, &
                 in%triangleVolume%i2, &
                 in%triangleVolume%i3, &
                 in%triangleVolume%nVe, &
                 in%triangleVolume%v, &
                 eye(j,1),eye(j,2),eye(j,3),eye(j,4),eye(j,5), &
                 in%mu,in%lambda, &
                 KT(j,:,1),KT(j,:,2),KT(j,:,3))
      END DO
  
      ! concatenate kernels
      rows=(/ KK(:,:,1),KR(:,:,1),KT(:,:,1), &
              KK(:,:,2),KR(:,:,2),KT(:,:,2), &
              KK(:,:,3),KR(:,:,3),KT(:,:,3) /)
  
    END SUBROUTINE tractionRows
  
    !-----------------------------------------------------------------------
    !> subroutine stressRows
    !! evaluates all the relevant rows for stress change due to slip (strike
    !! slip and dip slip) on fault patches, and strain
    !! (12, 13, 22, 23, 33) in volume elements.
    !!
    !! \author Sylvain Barbot (08/24/2020)
    !----------------------------------------------------------------------
    SUBROUTINE stressRows(xc,sv,dv,nv,m,n,rows)
      IMPLICIT NONE
      REAL*8, DIMENSION(3), INTENT(IN) :: xc,sv,dv,nv
      INTEGER, INTENT(IN) :: m,n
      REAL*8, DIMENSION(n*m), INTENT(OUT) :: rows
  
      INTEGER :: j
  
      !----------------------------------
      ! fault patch
      !----------------------------------
      DO j=1,DGF_PATCH
         CALL computeStressKernelsPatch2d(xc,sv,dv,nv, &
                 in%patch%ns,in%patch%x, &
                 in%patch%width, &
                 in%patch%dip, &
                 in%patch%beta, &
                 eye(j,1),eye(j,2), &
                 in%mu,in%lambda, &
                 LK(j,:,1),LK(j,:,2),LK(j,:,3),LK(j,:,4),LK(j,:,5))
      END DO
  
      !----------------------------------
      ! rectangle volume element
      !----------------------------------
      DO j=1,DGF_VOLUME
         CALL computeStressKernelsRectangleVolume2d(xc,sv,dv,nv, &
                 in%rectangleVolume%ns,in%rectangleVolume%x, &
                 in%rectangleVolume%thickness,in%rectangleVolume%width, &
                 in%rectangleVolume%dip, &
                 eye(j,1),eye(j,2),eye(j,3),eye(j,4),eye(j,5), &
                 in%mu,in%lambda, &
                 LR(j,:,1),LR(j,:,2),LR(j,:,3),LR(j,:,4),LR(j,:,5))
      END DO
  
      !----------------------------------
      ! triangle volume element
      !----------------------------------
      DO j=1,DGF_VOLUME
         CALL computeStressKernelsTriangleVolume2d(xc,sv,dv,nv, &
                 in%triangleVolume%ns, &
                 in%triangleVolume%i1, &
                 in%triangleVolume%i2, &
                 in%triangleVolume%i3, &
                 in%triangleVolume%nVe, &
                 in%triangleVolume%v, &
                 eye(j,1),eye(j,2),eye(j,3),eye(j,4),eye(j,5), &
                 in%mu,in%lambda, &
                 LT(j,:,1),LT(j,:,2),LT(j,:,3),LT(j,:,4),LT(j,:,5))
      END DO
  
      ! concatenate kernels
      rows=(/ LK(:,:,1),LR(:,:,1),LT(:,:,1), &
              LK(:,:,2),LR(:,:,2),LT(:,:,2), &
              LK(:,:,3),LR(:,:,3),LT(:,:,3), &
              LK(:,:,4),LR(:,:,4),LT(:,:,4), &
              LK(:,:,5),LR(:,:,5),LT(:,:,5) /)
  
    END SUBROUTINE stressRows
  
  END SUBROUTINE buildG
  
  !-----------------------------------------------------------------------
  !> subroutine buildO
  !! Builds the displacement matrix O following the layout below
  !!
  !!
  !!     /         **  \   +----------------------------------------+
  !!     |         **  |   |                                        |
  !!     |         **  |   |   nObservation                         |
  !! O = |         **  |   | (      *       )  * (  varying size  ) |
  !!     |         **  |   |   DISPLACEMENT                         |
  !!     |         **  |   |   _VECTOR_DGF                          |
  !!     |         **  |   |                                        |
  !!     \         **  /   +----------------------------------------+
  !!
  !! where each thread owns a few columns of O, marked schematically ****.
  !! The number of columns depends of the type of element owned by the
  !! current thread.
  !!
  !! The layout was chosen to minimize the MPI communication assuming
  !! that 
  !!
  !!   nObservation*DISPLACEMENT_VECTOR_DGF < (nPatch*dPatch+nVolume*dVolume).
  !!
  !! \author Sylvain Barbot (08/24/2020)
  !----------------------------------------------------------------------
  SUBROUTINE buildO(in,layout,O,Of,Ol)
    USE kernels
    USE types

    IMPLICIT NONE

    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in
    TYPE(LAYOUT_STRUCT), INTENT(IN) :: layout
    REAL*8, DIMENSION(:,:), ALLOCATABLE, INTENT(OUT) :: O,Of,Ol

    INCLUDE 'mpif.h'

    INTEGER :: ierr,rank,csize
    INTEGER :: i,j,k,l,n,m,p
    INTEGER :: elementIndex,elementType

    TYPE(PATCH_ELEMENT_STRUCT) :: patch
    TYPE(VOLUME_ELEMENT_STRUCT) :: volume
    REAL*8 :: nu

    ! identity matrix
    REAL*8, DIMENSION(5,5), PARAMETER :: &
               eye=RESHAPE( (/ 1._8,0._8,0._8,0._8,0._8, &
                               0._8,1._8,0._8,0._8,0._8, &
                               0._8,0._8,1._8,0._8,0._8, &
                               0._8,0._8,0._8,1._8,0._8, &
                               0._8,0._8,0._8,0._8,1._8 /), (/ 5,5 /))
  
    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

    ! Poisson's ratio
    nu=in%lambda/(in%lambda+in%mu)/2.d0

    ! number of columns in current thread
    n=layout%listVelocityN(rank+1)

    ! number of rows
    m=in%nObservationPoint*DISPLACEMENT_VECTOR_DGF

    ALLOCATE(O(n,m),Of(n,m),Ol(n,m),STAT=ierr)
    IF (0 /= ierr) STOP "could not allocate the displacement kernels"

    ! zero out partial contribution matrices
    Of=0._8
    Ol=0._8

    ! initiate counter for displacement vector
    p=1
    ! loop over observation points
    DO k=1,in%nObservationPoint

       ! initiate counter for source kinematics
       l=1

       ! loop over elements owned by current thread
       DO i=1,SIZE(layout%elementIndex)
          elementType= layout%elementType(i)
          elementIndex=layout%elementIndex(i)

          SELECT CASE (elementType)
          CASE (FLAG_PATCH)

             patch=in%patch%s(elementIndex)

             !---------------
             ! fault patches
             !---------------
             DO j=1,1
                ! antiplane strain
                CALL computeDisplacementPatchAntiplane( &
                        in%observationPoint(k)%x(2), &
                        in%observationPoint(k)%x(3), &
                        in%patch%x(2,elementIndex), &
                        in%patch%x(3,elementIndex), &
                        in%patch%width(elementIndex), &
                        in%patch%dip(elementIndex), &
                        eye(j,1), &
                        O(l+j-1,p+0))
                ! in-plane strain
                O(l+j-1,p+1)=0
                O(l+j-1,p+2)=0

                ! contribution from faulting alone
                Of(l+j-1,p  )=O(l+j-1,p)
                Of(l+j-1,p+1)=O(l+j-1,p+1)
                Of(l+j-1,p+2)=O(l+j-1,p+2)
             END DO

             DO j=2,DGF_PATCH
                ! antiplane strain
                O(l+j-1,p+0)=0
                ! in-plane strain
                CALL computeDisplacementPatchPlaneStrain( &
                        in%observationPoint(k)%x(2), &
                        in%observationPoint(k)%x(3), &
                        in%patch%x(2,elementIndex), &
                        in%patch%x(3,elementIndex), &
                        in%patch%width(elementIndex), &
                        in%patch%dip(elementIndex), &
                        in%patch%beta(elementIndex), &
                        eye(j,2), &
                        in%mu,in%lambda, &
                        O(l+j-1,p+1),O(l+j-1,p+2))

                ! contribution from faulting alone
                Of(l+j-1,p  )=O(l+j-1,p)
                Of(l+j-1,p+1)=O(l+j-1,p+1)
                Of(l+j-1,p+2)=O(l+j-1,p+2)
             END DO

             l=l+DGF_PATCH

          CASE (FLAG_RECTANGLE_VOLUME)

             volume=in%rectangleVolume%s(elementIndex)

             !-------------------------
             ! rectangle volume element
             !-------------------------
             DO j=1,2
                ! antiplane strain
                CALL computeDisplacementRectangleVolumeAntiplane( &
                        in%observationPoint(k)%x(2), &
                        in%observationPoint(k)%x(3), &
                        in%rectangleVolume%x(2,elementIndex), &
                        in%rectangleVolume%x(3,elementIndex), &
                        in%rectangleVolume%thickness(elementIndex), &
                        in%rectangleVolume%width(elementIndex), &
                        in%rectangleVolume%dip(elementIndex), &
                        eye(j,1),eye(j,2), &
                        O(l+j-1,p+0))

                ! in-plane strain
                O(l+j-1,p+1)=0
                O(l+j-1,p+2)=0

                ! contribution from flow alone
                Ol(l+j-1,p  )=O(l+j-1,p  )
                Ol(l+j-1,p+1)=O(l+j-1,p+1)
                Ol(l+j-1,p+2)=O(l+j-1,p+2)
             END DO

             DO j=3,DGF_VOLUME
                ! antiplane strain
                O(l+j-1,p+0)=0

                ! in-plane strain
                CALL computeDisplacementRectangleVolumePlanestrain( &
                        in%observationPoint(k)%x(2), &
                        in%observationPoint(k)%x(3), &
                        in%rectangleVolume%x(2,elementIndex), &
                        in%rectangleVolume%x(3,elementIndex), &
                        in%rectangleVolume%thickness(elementIndex), &
                        in%rectangleVolume%width(elementIndex), &
                        eye(j,3),eye(j,4),eye(j,5), &
                        in%mu,in%lambda, &
                        O(l+j-1,p+1),O(l+j-1,p+2))

                ! contribution from flow alone
                Ol(l+j-1,p  )=O(l+j-1,p  )
                Ol(l+j-1,p+1)=O(l+j-1,p+1)
                Ol(l+j-1,p+2)=O(l+j-1,p+2)
             END DO

             l=l+DGF_VOLUME

          CASE (FLAG_TRIANGLE_VOLUME)

             volume=in%triangleVolume%s(elementIndex)

             !------------------------
             ! triangle volume element
             !------------------------
             DO j=1,2
                ! antiplane strain
                CALL computeDisplacementTriangleVolumeAntiplane( &
                        in%observationPoint(k)%x(2), &
                        in%observationPoint(k)%x(3), &
                        in%triangleVolume%v(:,in%triangleVolume%i1(elementIndex)), &
                        in%triangleVolume%v(:,in%triangleVolume%i2(elementIndex)), &
                        in%triangleVolume%v(:,in%triangleVolume%i3(elementIndex)), &
                        eye(j,1),eye(j,2), &
                        O(l+j-1,p+0))

                ! in-plane strain
                O(l+j-1,p+1)=0
                O(l+j-1,p+2)=0

                ! contribution from flow alone
                Ol(l+j-1,p  )=O(l+j-1,p  )
                Ol(l+j-1,p+1)=O(l+j-1,p+1)
                Ol(l+j-1,p+2)=O(l+j-1,p+2)
             END DO

             DO j=3,DGF_VOLUME
                ! antiplane strain
                O(l+j-1,p+0)=0

                ! in-plane strain
                CALL computeDisplacementTriangleVolumePlanestrain( &
                        in%observationPoint(k)%x(2), &
                        in%observationPoint(k)%x(3), &
                        in%triangleVolume%v(:,in%triangleVolume%i1(elementIndex)), &
                        in%triangleVolume%v(:,in%triangleVolume%i2(elementIndex)), &
                        in%triangleVolume%v(:,in%triangleVolume%i3(elementIndex)), &
                        eye(j,3),eye(j,4),eye(j,5), &
                        in%mu,in%lambda, &
                        O(l+j-1,p+1),O(l+j-1,p+2))

                ! contribution from flow alone
                Ol(l+j-1,p  )=O(l+j-1,p  )
                Ol(l+j-1,p+1)=O(l+j-1,p+1)
                Ol(l+j-1,p+2)=O(l+j-1,p+2)
             END DO

             l=l+DGF_VOLUME

          CASE DEFAULT
             WRITE(STDERR,'("wrong case: this is a bug.")')
             WRITE_DEBUG_INFO(-1)
             STOP 2
          END SELECT

       END DO ! elements owned by current thread

       p=p+DISPLACEMENT_VECTOR_DGF

    END DO ! observation points
  
  END SUBROUTINE buildO
  
END MODULE greens

