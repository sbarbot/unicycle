!-----------------------------------------------------------------------
! Copyright 2020 Sylvain Barbot
!
! This file is part of UNICYCLE
!
! UNICYCLE is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! UNICYCLE is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with UNICYCLE.  If not, see <http://www.gnu.org/licenses/>.
!-----------------------------------------------------------------------

#include "macros.f90"

MODULE kernels

  IMPLICIT NONE

  REAL*8, PARAMETER, PRIVATE :: pi = 3.141592653589793_8

CONTAINS

  !------------------------------------------------------------------------
  !> function heaviside
  !! computes the Heaviside function
  !------------------------------------------------------------------------
  REAL*8 FUNCTION heaviside(x)
    REAL*8, INTENT(IN) :: x

     IF (0 .LT. x) THEN
        heaviside=1.0_8
     ELSE
        heaviside=0.0_8
     END IF

  END FUNCTION heaviside

  !------------------------------------------------------------------------
  !> function Omega(x)
  !! evaluates the boxcar function
  !------------------------------------------------------------------------
  REAL*8 FUNCTION omega(x)
    REAL*8, INTENT(IN) :: x

    omega=heaviside(x+0.5_8)-heaviside(x-0.5_8)

  END FUNCTION omega

  !------------------------------------------------------------------------
  !> subroutine computeReferenceSystem2d
  !! computes the center position and local reference system tied to the patch
  !!
  !! INPUT:
  !! @param ns
  !! @param x           - upper left coordinate of fault patch (north, east, down)
  !! @param dip         - dip angle (radian)
  !! @param W           - width of the dislocation
  !!
  !! OUTPUT:
  !! @param sv,dv,nv    - strike, dip and normal vectors of the fault patch
  !! @param xc          - coordinates (north, east, down) of the center
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeReferenceSystem2d(ns,x,W,dip,sv,dv,nv,xc)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: ns
    REAL*8, DIMENSION(3,ns), INTENT(IN) :: x
    REAL*8, DIMENSION(ns), INTENT(IN) :: dip,W
    REAL*8, DIMENSION(3,ns), INTENT(OUT) :: sv,dv,nv,xc

    ! unit vectors in the strike direction
    sv(1,1:ns)=1._8
    sv(2,1:ns)=0._8
    sv(3,1:ns)=0._8
            
    ! unit vectors in the dip direction
    dv(1,1:ns)=+0._8
    dv(2,1:ns)=-COS(dip(1:ns))
    dv(3,1:ns)=-SIN(dip(1:ns))
            
    ! unit vectors in the normal direction
    nv(1,1:ns)=-0._8
    nv(2,1:ns)=+SIN(dip(1:ns))
    nv(3,1:ns)=-COS(dip(1:ns))
            
    ! center of fault patch
    xc(1,1:ns)=x(1,1:ns)-W(1:ns)/2*dv(1,1:ns)
    xc(2,1:ns)=x(2,1:ns)-W(1:ns)/2*dv(2,1:ns)
    xc(3,1:ns)=x(3,1:ns)-W(1:ns)/2*dv(3,1:ns)
                
                
  END SUBROUTINE computeReferenceSystem2d

  !------------------------------------------------------------------------
  !> subroutine computeStressPatchAntiplane
  !! calculates the stress associated with a dislocation in an elastic
  !! elastic half-space in 2.5d.
  !!
  !! INPUT:
  !! @param x2,x3    - coordinates of observation points
  !! @param q2,q3    - coordinates of upper left corner of the dislocation
  !! @param W        - width of the line dislocation
  !! @param dip      - dip of the line dislocation (radian)
  !!
  !! @param ss       - strike-slip component
  !!
  !! @param G        - rigidity
  !!
  !! OUTPUT:
  !! s12,s13         - antiplane stress components
  !!
  !! KNOWN BUGS: 
  !! the dip angle is ignored.
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeStressPatchAntiplane(x2,x3, &
                                  q2,q3,W,dip, &
                                  ss,G,s12,s13)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x2,x3,q2,q3
    REAL*8, INTENT(IN) :: W,dip
    REAL*8, INTENT(IN) :: ss
    REAL*8, INTENT(IN) :: G
    REAL*8, INTENT(OUT) :: s12,s13

    REAL*8 :: r2

    r2=x2-q2

    s12=ss*G*( &
        -(x3-q3  )/(r2**2+(x3-q3  )**2) &
        +(x3+q3  )/(r2**2+(x3+q3  )**2) &
        +(x3-q3-W)/(r2**2+(x3-q3-W)**2) &
        -(x3+q3+W)/(r2**2+(x3+q3+W)**2))/2/pi

    s13=ss*G*( &
         r2/(r2**2+(x3-q3  )**2) &
        -r2/(r2**2+(x3+q3  )**2) &
        -r2/(r2**2+(x3-q3-W)**2) &
        +r2/(r2**2+(x3+q3+W)**2))/2/pi

  END SUBROUTINE computeStressPatchAntiplane

  !------------------------------------------------------------------------
  !> subroutine computeStressPatchPlaneStrain
  !! calculates the stress associated with a dislocation in an elastic 
  !! half-space in plane strain condition.
  !!
  !! INPUT:
  !! @param x2,x3    - coordinates of observation points
  !! @param s        - normal slip
  !! @param q2,q3    - coordinates of upper left corner of rectangular
  !!                   dislocation
  !! @param W        - width of the line dislocation
  !! @param dipd     - dip of the line dislocation (radian)
  !! @param beta     - take-off angle of the line dislocation (radian)
  !!
  !! @param ds       - dip-slip component
  !!
  !! @param G        - rigidity
  !! @param lambda   - Lame parameter
  !!
  !! OUTPUT:
  !! s22,s23,s33     - the stress components
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeStressPatchPlaneStrain(x2,x3,q2,q3,W,dip,beta,ds,G,lambda,s22,s23,s33)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x2,x3,q2,q3
    REAL*8, INTENT(IN) :: W,dip,beta
    REAL*8, INTENT(IN) :: ds
    REAL*8, INTENT(IN) :: G,lambda
    REAL*8, INTENT(OUT) :: s22,s23,s33

    REAL*8 :: T,eps22p,eps23p

    ! infinitesimal thickness
    T=W/1e4
    ! infinite opening strain
    eps22p= ds/(      T)*SIN(beta)
    ! infinite shear strain, positive for thrust
    eps23p=-ds/(2.0_8*T)*COS(beta)

    CALL computeStressRectangleVolumePlaneStrain( &
            x2,x3,q2,q3,T,W,dip, &
            eps22p,eps23p,0._8,G,lambda,s22,s23,s33)

  END SUBROUTINE computeStressPatchPlaneStrain

  !------------------------------------------------------------------------
  !> subroutine computeDisplacementPatchAntiplane
  !! calculates the displacements associated with a dislocation in an
  !! elastic half-space in antiplane condition.
  !!
  !! INPUT:
  !! @param x2,x3    - coordinates of observation points
  !! @param q2,q3    - coordinates of upper left corner of line dislocation
  !! @param W        - width of the line dislocation
  !! @param dip      - dip of the line dislocation (radian)
  !!
  !! @param ss       - strike-slip component
  !!
  !! OUTPUT:
  !! u1              - displacement in the strike direction
  !!
  !! KNOWN BUGS:
  !! the dip angle is ignored.
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeDisplacementPatchAntiplane(x2,x3, &
                                        q2,q3,W,dip, &
                                        ss,u1)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x2,x3,q2,q3
    REAL*8, INTENT(IN) :: W,dip
    REAL*8, INTENT(IN) :: ss
    REAL*8, INTENT(OUT) :: u1

    u1=ss*(+atan((x3-q3)/(x2-q2)) &
           -atan((x3+q3)/(x2-q2)) &
           -atan((x3-q3-W)/(x2-q2)) &
           +atan((x3+q3+W)/(x2-q2)))/2._8/pi

  END SUBROUTINE computeDisplacementPatchAntiplane

  !------------------------------------------------------------------------
  !> subroutine computeDisplacementPatchPlaneStrain
  !! calculates the displacements associated with a dislocation in an
  !! elastic half-space in plane strain condition.
  !!
  !! INPUT:
  !! @param x2,x3    - coordinates of observation points
  !! @param s        - normal slip
  !! @param q2,q3    - coordinates of upper left corner of line dislocation
  !! @param W        - width of the line dislocation
  !! @param dip      - dip angle of the line dislocation (radian)
  !! @param beta     - take-off angle of the line dislocation (radian)
  !!
  !! @param ds       - dip slip
  !!
  !! @param G        - rigidity
  !!
  !! OUTPUT:
  !! u2,u3           - the displacement components in the east and depth
  !!                   directions.
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeDisplacementPatchPlaneStrain(x2,x3, &
                          q2,q3,W,dip,beta,ds,G,lambda,u2,u3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x2,x3,q2,q3
    REAL*8, INTENT(IN) :: W,dip,beta
    REAL*8, INTENT(IN) :: ds
    REAL*8, INTENT(IN) :: G,lambda
    REAL*8, INTENT(OUT) :: u2,u3

    REAL*8 :: T,eps22p,eps23p

    ! infinitesimal thickness
    T=W/1e4
    ! extension
    eps22p= ds/(      T)*SIN(beta)
    ! infinite shear strain
    eps23p=-ds/(2.0_8*T)*COS(beta)

    CALL computeDisplacementRectangleVolumePlaneStrain( &
            x2,x3,q2,q3,T,W,dip, &
            eps22p,eps23p,0._8,G,lambda,u2,u3)

  END SUBROUTINE computeDisplacementPatchPlaneStrain

  !------------------------------------------------------------------------
  !> subroutine computeDisplacementRectangleVolumeAntiplane
  !! calculates the displacements associated with a dislocation in an
  !! elastic half-space in antiplane condition.
  !!
  !! INPUT:
  !! @param x2,x3    - coordinates of observation points
  !! @param q2,q3    - coordinates of upper left corner of line dislocation
  !! @param W        - width of the line dislocation
  !! @param dip      - dip of the line dislocation (radian)
  !! @param e12,e13  - anelastic strain in the horizontal and depth directions
  !!
  !! OUTPUT:
  !! u1              - displacement in the strike direction
  !!
  !! KNOWN BUGS:
  !! the dip angle is ignored.
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeDisplacementRectangleVolumeAntiplane(x2,x3, &
                                        q2,q3,T,W,dip, &
                                        e12,e13,u1)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x2,x3,q2,q3
    REAL*8, INTENT(IN) :: T,W,dip
    REAL*8, INTENT(IN) :: e12,e13
    REAL*8, INTENT(OUT) :: u1

    REAL*8 :: r2

    r2=x2-q2

    ! displacement due to distributed anelastic strain
    u1=e12/(2*pi)*( &
           (x3-q3-W)*log((r2-T/2)**2+(x3-q3-W)**2) &
          -(x3-q3-W)*log((r2+T/2)**2+(x3-q3-W)**2) &
          -(x3-q3)*log((r2-T/2)**2+(x3-q3)**2) &
          +(x3-q3)*log((r2+T/2)**2+(x3-q3)**2) &
          +2*(r2-T/2)*(atan((x3-q3-W)/(r2-T/2))-atan((x3-q3)/(r2-T/2))) &
          +2*(r2+T/2)*(atan((x3-q3)/(r2+T/2))-atan((x3-q3-W)/(r2+T/2))) &
          +(x3+q3+W)*log((r2+T/2)**2+(x3+q3+W)**2) &
          -(x3+q3+W)*log((r2-T/2)**2+(x3+q3+W)**2) &
          -(x3+q3)*log((r2+T/2)**2+(x3+q3)**2) &
          +(x3+q3)*log((r2-T/2)**2+(x3+q3)**2) &
          +2*(r2+T/2)*(atan((x3+q3+W)/(r2+T/2))-atan((x3+q3)/(r2+T/2))) &
          +2*(r2-T/2)*(atan((x3+q3)/(r2-T/2))-atan((x3+q3+W)/(r2-T/2))) &
       ) + &
       e13/(2*pi)*( &
           (r2-T/2)*log((r2-T/2)**2+(x3-q3-W)**2) &
          -(r2+T/2)*log((r2+T/2)**2+(x3-q3-W)**2) &
          -(r2-T/2)*log((r2-T/2)**2+(x3-q3)**2) &
          +(r2+T/2)*log((r2+T/2)**2+(x3-q3)**2) &
          +2*(x3-W-q3)*(atan((r2-T/2)/(x3-q3-W))-atan((r2+T/2)/(x3-q3-W))) &
          +2*(x3-q3)*(atan((r2+T/2)/(x3-q3))-atan((r2-T/2)/(x3-q3))) &
          +(r2-T/2)*log((r2-T/2)**2+(x3+q3+W)**2) &
          -(r2+T/2)*log((r2+T/2)**2+(x3+q3+W)**2) &
          -(r2-T/2)*log((r2-T/2)**2+(x3+q3)**2) &
          +(r2+T/2)*log((r2+T/2)**2+(x3+q3)**2) &
          +2*(x3+W+q3)*(atan((r2-T/2)/(x3+q3+W))-atan((r2+T/2)/(x3+q3+W))) &
          +2*(x3+q3)*(atan((r2+T/2)/(x3+q3))-atan((r2-T/2)/(x3+q3))) &
       )

  END SUBROUTINE computeDisplacementRectangleVolumeAntiplane

  !------------------------------------------------------------------------
  !> subroutine computeTractionKernelsPatch2d
  !! calculates the traction kernels associated with dislocations in an
  !! elastic half-space in 2.5d.
  !!
  !! INPUT:
  !! @param x          - coordinates of observation points
  !! @param sv,dv,nv   - strike, dip and normal vector of observation points
  !! @param ns         - number of sources
  !! @param y          - coordinate (NED) of sources
  !! @param W          - width of the line dislocation
  !! @param dip        - dip of the line dislocation
  !! @param beta       - take-off angle of the line dislocation (radian)
  !!
  !! @param G          - rigidity
  !! @param lambda     - Lame parameter
  !!
  !! OUTPUT:
  !! ts                - traction in the strike direction (antiplane strain)
  !! td                - traction in the dip direction (in-plane strain)
  !! tn                - traction in the normal direction (in-plane strain)
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeTractionKernelsPatch2d(x,sv,dv,nv, &
                        ns,y,W,dip,beta, &
                        ss,ds, &
                        G,lambda, &
                        ts,td,tn)
    IMPLICIT NONE
    REAL*8, DIMENSION(3), INTENT(IN) :: x,sv,dv,nv
    INTEGER, INTENT(IN) :: ns
    REAL*8, DIMENSION(3,ns), INTENT(IN) :: y
    REAL*8, DIMENSION(ns), INTENT(IN) :: W,dip,beta
    REAL*8, INTENT(IN) :: ss,ds
    REAL*8, INTENT(IN) :: G,lambda
    REAL*8, DIMENSION(ns), INTENT(OUT) :: ts,td,tn

    INTEGER :: i
    REAL*8 :: s12,s13,s22,s23,s33

    ! initiate
    ts=0
    td=0
    tn=0

    ! strike-slip component
    DO i=1,ns
       CALL computeStressPatchAntiplane( &
                      x(2),x(3), &
                      y(2,i),y(3,i),W(i),dip(i), &
                      ss,G,s12,s13)

       ! rotate to receiver system of coordinates
       ts(i)= (           nv(2)*s12+nv(3)*s13 )*sv(1) &
             +( nv(1)*s12                     )*sv(2) &
             +( nv(1)*s13                     )*sv(3)

    END DO

    ! dip-slip component
    DO i=1,ns
       CALL computeStressPatchPlaneStrain( &
                      x(2),x(3), &
                      y(2,i),y(3,i),W(i),dip(i),beta(i), &
                      ds,G,lambda,s22,s23,s33)

       ! rotate to receiver system of coordinates
       td(i)= ( nv(2)*s22+nv(3)*s23 )*dv(2) &
             +( nv(2)*s23+nv(3)*s33 )*dv(3)
       tn(i)= ( nv(2)*s22+nv(3)*s23 )*nv(2) &
             +( nv(2)*s23+nv(3)*s33 )*nv(3)

    END DO

  END SUBROUTINE computeTractionKernelsPatch2d

  !------------------------------------------------------------------------
  !> subroutine computeStressKernelsPatch2d
  !! calculates the stress kernels associated with a dislocation in an
  !! in an elastic half-space in 2.5d.
  !!
  !! INPUT:
  !! @param x          - coordinates of observation points
  !! @param sv,dv,nv   - strike, dip and normal vector of observation points
  !! @param ns         - number of sources
  !! @param y          - coordinate (NED) of sources
  !! @param W          - width of the dislocation
  !! @param dip        - dip angle of the dislocation (radian)
  !! @param beta       - take-off angle of the line dislocation (radian)
  !!
  !! @param s          - strike slip
  !!
  !! @param G          - rigidity
  !! @param lambda     - Lame parameter
  !!
  !! OUTPUT:
  !! s12,s13           - antiplane-strain stress components
  !! s22,s23,s33       - in-plane strain stress components
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeStressKernelsPatch2d(x,sv,dv,nv, &
                        ns,y,W,dip,beta, &
                        ss,ds, &
                        G,lambda,s12,s13,s22,s23,s33)
    IMPLICIT NONE
    REAL*8, DIMENSION(3), INTENT(IN) :: x,sv,dv,nv
    INTEGER, INTENT(IN) :: ns
    REAL*8, DIMENSION(3,ns), INTENT(IN) :: y
    REAL*8, DIMENSION(ns), INTENT(IN) :: W,dip,beta
    REAL*8, INTENT(IN) :: ss,ds
    REAL*8, INTENT(IN) :: G,lambda
    REAL*8, DIMENSION(ns), INTENT(OUT) :: s12,s13,s22,s23,s33

    INTEGER :: i
    REAL*8 :: s12p,s13p,s22p,s23p,s33p

    ! initiate
    s12=0
    s13=0
    s22=0
    s23=0
    s33=0

    ! strike-slip component
    DO i=1,ns
       CALL computeStressPatchAntiplane( &
                      x(2),x(3), &
                      y(2,i),y(3,i),W(i),dip(i), &
                      ss,G,s12(i),s13(i))

       ! rotate to receiver system of coordinates
       s12p= (              sv(2)*s12(i)+sv(3)*s13(i) )*nv(1) &
            +( sv(1)*s12(i)                           )*nv(2) &
            +( sv(1)*s13(i)                           )*nv(3)
       s13p=-(              sv(2)*s12(i)+sv(3)*s13(i) )*dv(1) &
            -( sv(1)*s12(i)                           )*dv(2) &
            -( sv(1)*s13(i)                           )*dv(3)

       s12(i)=s12p
       s13(i)=s13p

    END DO

    ! dip-slip component
    DO i=1,ns
       CALL computeStressPatchPlaneStrain( &
                      x(2),x(3), &
                      y(2,i),y(3,i),W(i),dip(i),beta(i), &
                      ds,G,lambda,s22(i),s23(i),s33(i))

       ! rotate to receiver system of coordinates
       s22p= (  nv(2)*s22(i)+nv(3)*s23(i) )*nv(2) &
            +(  nv(2)*s23(i)+nv(3)*s33(i) )*nv(3)
       s23p=-(  nv(2)*s22(i)+nv(3)*s23(i) )*dv(2) &
            -(  nv(2)*s23(i)+nv(3)*s33(i) )*dv(3)
       s33p=-( -dv(2)*s22(i)-dv(3)*s23(i) )*dv(2) &
            -( -dv(2)*s23(i)-dv(3)*s33(i) )*dv(3)

       s22(i)=s22p
       s23(i)=s23p
       s33(i)=s33p

    END DO

  END SUBROUTINE computeStressKernelsPatch2d

  !------------------------------------------------------------------------
  !> subroutine ComputeStressRectnagleVolumeAntiplane computes the stress field 
  !! associated with deforming strain volume using the analytic solution of
  !!
  !!   Lambert, V., and S. Barbot. "Contribution of viscoelastic flow in 
  !!   earthquake cycles within the lithosphere‐asthenosphere system." 
  !!   Geophysical Research Letters 43.19 (2016).
  !!
  !! considering the following geometry:
  !!
  !!                              q2,q3
  !!                   +------------@-------------+---> E (x2)
  !!                   |                          |
  !!                   |                        w |
  !!                   |                        i |
  !!                   |                        d |
  !!                   |                        t |
  !!                   |                        h |
  !!                   |                          |
  !!                   +--------------------------+
  !!                   :     t h i c k n e s s 
  !!                   :
  !!                   |
  !!                   Z (x3)
  !!
  !!
  !! INPUT:
  !! @param x2, x3         easting, and depth of the observation point
  !!                       in unprimed system of coordinates.
  !! @param q2, q3         east and depth coordinates of the strain volume,
  !! @param T, W           thickness, and width of the strain volume,
  !! @param epsijp         anelastic strain component 12 and 13
  !!                       in the strain volume in the system of reference tied to 
  !!                       the strain volume (primed reference system),
  !! @param G              rigidity.
  !!
  !! OUTPUT:
  !! s12,s13               stress components in the unprimed reference system.
  !!
  !! KNOWN BUGS:
  !! the dip angle is ignored.
  !!
  !! \author Sylvain Barbot (06/09/17) - original form
  !------------------------------------------------------------------------
  SUBROUTINE computeStressRectangleVolumeAntiplane(x2,x3,q2,q3,T,W,dip, &
                                eps12p,eps13p,G,s12,s13)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x2,x3,q2,q3,T,W,dip
    REAL*8, INTENT(IN) :: eps12p,eps13p
    REAL*8, INTENT(IN) :: G
    REAL*8, INTENT(OUT) :: s12,s13
    
    REAL*8 :: r2

    r2=x2-q2

    s12= G/pi*eps12p*( &
           atan((x3-q3  )/(r2+T/2))-atan((x3-q3  )/(r2-T/2)) &
          +atan((x3-q3-W)/(r2-T/2))-atan((x3-q3-W)/(r2+T/2)) &
          -atan((x3+q3+W)/(r2-T/2))-atan((x3+q3  )/(r2+T/2)) &
          +atan((x3+q3  )/(r2-T/2))+atan((x3+q3+W)/(r2+T/2))) &
       + G/(2*pi)*eps13p*( &
           log((r2-T/2)**2+(x3-q3-W)**2) - log((r2+T/2)**2+(x3-q3-W)**2) &
          +log((r2-T/2)**2+(x3+q3+W)**2) - log((r2+T/2)**2+(x3+q3+W)**2) &
          -log((r2-T/2)**2+(x3-q3)**2)   + log((r2+T/2)**2+(x3-q3)**2) &
          -log((r2-T/2)**2+(x3+q3)**2)   + log((r2+T/2)**2+(x3+q3)**2)) &
       -2*G*eps12p*omega(r2/T)*omega((x3-(2*q3+W)/2._8)/W)

    s13= G/(2*pi)*eps12p*( &
           log((r2-T/2)**2+(x3-q3-W)**2) - log((r2+T/2)**2+(x3-q3-W)**2) &
          -log((r2-T/2)**2+(x3+q3+W)**2) + log((r2+T/2)**2+(x3+q3+W)**2) &
          -log((r2-T/2)**2+(x3-q3)**2)   + log((r2+T/2)**2+(x3-q3)**2) &
          +log((r2-T/2)**2+(x3+q3)**2)   - log((r2+T/2)**2+(x3+q3)**2)) &
       + G/pi*eps13p*( &
           atan((r2+T/2)/(x3-q3))  -atan((r2-T/2)/(x3-q3)) &
          -atan((r2+T/2)/(x3-q3-W))+atan((r2-T/2)/(x3-q3-W)) &
          +atan((r2+T/2)/(x3+q3))  -atan((r2-T/2)/(x3+q3)) &
          -atan((r2+T/2)/(x3+q3+W))+atan((r2-T/2)/(x3+q3+W))) &
       -2*G*eps13p*omega(r2/T)*omega((x3-(2*q3+W)/2)/W)

  END SUBROUTINE computeStressRectangleVolumeAntiplane

  !------------------------------------------------------------------------
  !> subroutine computeTractionKernelsRectangleVolume2d
  !! calculates the traction kernels associated with a volume elements
  !! in antiplane strain using the analytic solution of
  !!
  !!   Lambert, V., and S. Barbot. "Contribution of viscoelastic flow in 
  !!   earthquake cycles within the lithosphere‐asthenosphere system" 
  !!   Geophysical Research Letters, 43.19 (2016).
  !!
  !! INPUT:
  !! @param x2,x3      - coordinates of observation points
  !! @param sv,dv,nv   - strike, dip and normal vector of observation points
  !! @param ns         - number of sources
  !! @param y          - position (NED) of strain volume
  !! @param T,W        - thickness and width of the strain volume
  !! @param dip        - dip angle (radian) of the strain volume
  !! @param e12p
  !!        e13p       - strain components in the primed reference system
  !!                     tied to the strain volume
  !! @param G          - rigidity
  !!
  !! OUTPUT:
  !! ts                - traction in the strike direction.
  !!
  !! \author Sylvain Barbot (06/09/17) - original form
  !------------------------------------------------------------------------
  SUBROUTINE computeTractionKernelsRectangleVolume2d( &
                         x,sv,dv,nv, &
                         ns,y,T,W,dip, &
                         e12p,e13p,e22p,e23p,e33p,G,lambda,ts,td,tn)
    REAL*8, DIMENSION(3), INTENT(IN) :: x
    REAL*8, DIMENSION(3), INTENT(IN) :: sv,dv,nv
    INTEGER, INTENT(IN) :: ns
    REAL*8, DIMENSION(3,ns), INTENT(IN) :: y
    REAL*8, DIMENSION(ns), INTENT(IN) :: T,W,dip
    REAL*8, INTENT(IN) :: e12p,e13p,e22p,e23p,e33p
    REAL*8, INTENT(IN) :: G,lambda
    REAL*8, DIMENSION(ns), INTENT(OUT) :: ts,td,tn

    INTEGER :: i
    REAL*8 :: s12,s13,s22,s23,s33

    ! antiplane strain components
    DO i=1,ns
       CALL computeStressRectangleVolumeAntiplane( &
              x(2),x(3), &
              y(2,i),y(3,i),T(i),W(i),dip(i), &
              e12p,e13p,G,s12,s13)

       ! rotate to receiver system of coordinates
       ts(i)= (           nv(2)*s12+nv(3)*s13 )*sv(1) &
             +( nv(1)*s12                     )*sv(2) &
             +( nv(1)*s13                     )*sv(3)

    END DO

    ! in-plane strain components
    DO i=1,ns
       CALL computeStressRectangleVolumePlaneStrain( &
              x(2),x(3), &
              y(2,i),y(3,i),T(i),W(i),dip(i), &
              e22p,e23p,e33p,G,lambda,s22,s23,s33)

       ! rotate to receiver system of coordinates
       td(i)= ( nv(2)*s22+nv(3)*s23 )*dv(2) &
             +( nv(2)*s23+nv(3)*s33 )*dv(3)
       tn(i)= ( nv(2)*s22+nv(3)*s23 )*nv(2) &
             +( nv(2)*s23+nv(3)*s33 )*nv(3)

    END DO

  END SUBROUTINE computeTractionKernelsRectangleVolume2d

  !------------------------------------------------------------------------
  !> subroutine computeStressKernelsRectangleVolume2d
  !! calculates the traction kernels associated with strain in finite
  !! volumes in antiplane strain using the analytic solution of 
  !!
  !!   Lambert, V., and S. Barbot. "Contribution of viscoelastic flow in 
  !!   earthquake cycles within the lithosphere‐asthenosphere system." 
  !!   Geophysical Research Letters 43.19 (2016).
  !!
  !! INPUT:
  !! @param x2,x3          - coordinates of observation points
  !! @param sv,dv,nv       - strike, dip and normal vector of observation points
  !! @param ns             - number of sources
  !! @param y              - position (NED) of strain volume
  !! @param T,W            - thickness and width of the strain volume
  !! @param dip            - dip angle (radian) of the strain volume
  !!
  !! @param e12p,e13p      - antiplane strain components in the primed reference system
  !!                         tied to the strain volume
  !!
  !! @param e22p,e23p,e33p - strain components in the primed reference system
  !!                         tied to the strain volume
  !!
  !! @param G,lambda       - rigidity, Lame parameter
  !!
  !! OUTPUT:
  !! s12,s13               - the antiplane stress components in the reference
  !!                         system tied to the strain volume
  !! s22,s23,s33           - the in-plane stress components in the reference
  !!                         system tied to the strain volume
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeStressKernelsRectangleVolume2d( &
                         x,sv,dv,nv, &
                         ns,y,T,W,dip, &
                         e12p,e13p,e22p,e23p,e33p,G,lambda,s12,s13,s22,s23,s33)
    IMPLICIT NONE
    REAL*8, DIMENSION(3), INTENT(IN) :: x
    REAL*8, DIMENSION(3), INTENT(IN) :: sv,dv,nv
    INTEGER, INTENT(IN) :: ns
    REAL*8, DIMENSION(3,ns), INTENT(IN) :: y
    REAL*8, DIMENSION(ns), INTENT(IN) :: T,W,dip
    REAL*8, INTENT(IN) :: e12p,e13p,e22p,e23p,e33p
    REAL*8, INTENT(IN) :: G,lambda
    REAL*8, DIMENSION(ns), INTENT(OUT) :: s12,s13,s22,s23,s33

    INTEGER :: i
    REAL*8 :: s12p,s13p,s22p,s23p,s33p

    ! antiplane strain
    DO i=1,ns
       CALL computeStressRectangleVolumeAntiplane( &
              x(2),x(3), &
              y(2,i),y(3,i),T(i),W(i),dip(i), &
              e12p,e13p,G,s12(i),s13(i))

       ! rotate to receiver system of coordinates
       s12p= (              sv(2)*s12(i)+sv(3)*s13(i) )*nv(1) &
            +( sv(1)*s12(i)                           )*nv(2) &
            +( sv(1)*s13(i)                           )*nv(3)
       s13p=-(              sv(2)*s12(i)+sv(3)*s13(i) )*dv(1) &
            -( sv(1)*s12(i)                           )*dv(2) &
            -( sv(1)*s13(i)                           )*dv(3)

       s12(i)=s12p
       s13(i)=s13p

    END DO

    ! in-plane strain
    DO i=1,ns
       CALL computeStressRectangleVolumePlaneStrain( &
              x(2),x(3), &
              y(2,i),y(3,i),T(i),W(i),dip(i), &
              e22p,e23p,e33p,G,lambda,s22(i),s23(i),s33(i))

       ! rotate to receiver system of coordinates
       s22p= (  nv(2)*s22(i)+nv(3)*s23(i) )*nv(2) &
            +(  nv(2)*s23(i)+nv(3)*s33(i) )*nv(3)
       s23p=-(  nv(2)*s22(i)+nv(3)*s23(i) )*dv(2) &
            -(  nv(2)*s23(i)+nv(3)*s33(i) )*dv(3)
       s33p=-( -dv(2)*s22(i)-dv(3)*s23(i) )*dv(2) &
            -( -dv(2)*s23(i)-dv(3)*s33(i) )*dv(3)

       s22(i)=s22p
       s23(i)=s23p
       s33(i)=s33p

    END DO

  END SUBROUTINE computeStressKernelsRectangleVolume2d

  !------------------------------------------------------------------------
  !> subroutine computeStressKernelsTriangleVolume2d
  !! calculates the stress kernels associated with strain in triangle
  !! volume elements in 2.5d using the solutions of 
  !!
  !!   Barbot S., "Deformation of a Half-Space from Anelastic Strain Confined
  !!   in a Tetrahedral Volume", submitted to Bull. Seism. Soc. Am., 2018.
  !!
  !! INPUT:
  !! @param x2,x3        - coordinates of observation points
  !! @param sv,dv,nv     - strike, dip and normal vector of observation points
  !! @param ns           - number of sources
  !! @param i1,i2,i3     - index of the three vertices
  !! @param nVe          - number of vertex coordinates
  !! @param v            - vertex coordinates
  !!
  !! @param e12,e13      - antiplane strain components in triangle volume element
  !! @param e22,e23,e33  - plane-strain components in triangle volume element
  !!
  !! @param G            - rigidity
  !! @param lambda       - Lame parameter.
  !!
  !! OUTPUT:
  !! s12,s13,s22,s23,s33 - stress components.
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeStressKernelsTriangleVolume2d( &
                         x,sv,dv,nv, &
                         ns,i1,i2,i3,nVe,v, &
                         e12,e13,e22,e23,e33,G,lambda, &
                         s12,s13,s22,s23,s33)
    REAL*8, DIMENSION(3), INTENT(IN) :: x
    REAL*8, DIMENSION(3), INTENT(IN) :: sv,dv,nv
    INTEGER, INTENT(IN) :: ns
    INTEGER, DIMENSION(ns), INTENT(IN) :: i1,i2,i3
    INTEGER, INTENT(IN) :: nVe
    REAL*8, DIMENSION(3,nVe), INTENT(IN) :: v
    REAL*8, INTENT(IN) :: e12,e13,e22,e23,e33
    REAL*8, INTENT(IN) :: G,lambda
    REAL*8, DIMENSION(ns), INTENT(OUT) :: s12,s13,s22,s23,s33

    INTEGER :: i
    REAL*8 :: s12p,s13p,s22p,s23p,s33p

    ! Poisson's ratio
    REAL*8 :: nu

    nu=lambda/2/(lambda+G)

    DO i=1,ns
       CALL computeStressTriangleVolumeAntiplane( &
              x(2),x(3), &
              v(2:3,i1(i)), &
              v(2:3,i2(i)), &
              v(2:3,i3(i)), &
              e12,e13,G,s12(i),s13(i))

       ! rotate to receiver system of coordinates
       s12p= (              sv(2)*s12(i)+sv(3)*s13(i) )*nv(1) &
            +( sv(1)*s12(i)                           )*nv(2) &
            +( sv(1)*s13(i)                           )*nv(3)
       s13p=-(              sv(2)*s12(i)+sv(3)*s13(i) )*dv(1) &
            -( sv(1)*s12(i)                           )*dv(2) &
            -( sv(1)*s13(i)                           )*dv(3)

       s12(i)=s12p
       s13(i)=s12p

    END DO

    DO i=1,ns
       CALL computeStressTriangleVolumePlaneStrainMixedQuad( &
              x(2),x(3), &
              v(2:3,i1(i)), &
              v(2:3,i2(i)), &
              v(2:3,i3(i)), &
              e22,e23,e33,G,nu,s22(i),s23(i),s33(i))

       ! rotate to receiver system of coordinates
       s22p= (  nv(2)*s22(i)+nv(3)*s23(i) )*nv(2) &
            +(  nv(2)*s23(i)+nv(3)*s33(i) )*nv(3)
       s23p=-(  nv(2)*s22(i)+nv(3)*s23(i) )*dv(2) &
            -(  nv(2)*s23(i)+nv(3)*s33(i) )*dv(3)
       s33p=-( -dv(2)*s22(i)-dv(3)*s23(i) )*dv(2) &
            -( -dv(2)*s23(i)-dv(3)*s33(i) )*dv(3)

       s22(i)=s22p
       s23(i)=s23p
       s33(i)=s33p

    END DO

  END SUBROUTINE computeStressKernelsTriangleVolume2d

  !------------------------------------------------------------------------
  !> subroutine computeStressKernelsTriangleVolume2d
  !! calculates the stress kernels associated with strain in triangle
  !! volume elements in 2.5d using the solutions of 
  !!
  !!   Barbot S., "Deformation of a Half-Space from Anelastic Strain Confined
  !!   in a Tetrahedral Volume", submitted to Bull. Seism. Soc. Am., 2018.
  !!
  !! INPUT:
  !! @param x2,x3        - coordinates of observation points
  !! @param sv,dv,nv     - strike, dip and normal vector of observation points
  !! @param ns           - number of sources
  !! @param i1,i2,i3     - index of the three vertices
  !! @param nVe          - number of vertex coordinates
  !! @param v            - vertex coordinates
  !!
  !! @param e12,e13      - antiplane strain components in triangle volume element
  !! @param e22,e23,e33  - plane-strain components in triangle volume element
  !!
  !! @param G            - rigidity
  !! @param lambda       - Lame parameter.
  !!
  !! OUTPUT:
  !! ts,td,tn            - traction components.
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeTractionKernelsTriangleVolume2d( &
                         x,sv,dv,nv, &
                         ns,i1,i2,i3,nVe,v, &
                         e12,e13,e22,e23,e33,G,lambda, &
                         ts,td,tn)
    REAL*8, DIMENSION(3), INTENT(IN) :: x
    REAL*8, DIMENSION(3), INTENT(IN) :: sv,dv,nv
    INTEGER, INTENT(IN) :: ns
    INTEGER, DIMENSION(ns), INTENT(IN) :: i1,i2,i3
    INTEGER, INTENT(IN) :: nVe
    REAL*8, DIMENSION(3,nVe), INTENT(IN) :: v
    REAL*8, INTENT(IN) :: e12,e13,e22,e23,e33
    REAL*8, INTENT(IN) :: G,lambda
    REAL*8, DIMENSION(ns), INTENT(OUT) :: ts,td,tn

    INTEGER :: i
    REAL*8 :: s12,s13,s22,s23,s33

    ! Poisson's ratio
    REAL*8 :: nu

    nu=lambda/2/(lambda+G)

    DO i=1,ns
       CALL computeStressTriangleVolumeAntiplane( &
              x(2),x(3), &
              v(2:3,i1(i)), &
              v(2:3,i2(i)), &
              v(2:3,i3(i)), &
              e12,e13,G,s12,s13)

       ! rotate to receiver system of coordinates
       ts(i)= (           nv(2)*s12+nv(3)*s13 )*sv(1) &
             +( nv(1)*s12                     )*sv(2) &
             +( nv(1)*s13                     )*sv(3)

    END DO

    DO i=1,ns
       CALL computeStressTriangleVolumePlaneStrainMixedQuad( &
              x(2),x(3), &
              v(2:3,i1(i)), &
              v(2:3,i2(i)), &
              v(2:3,i3(i)), &
              e22,e23,e33,G,nu,s22,s23,s33)

       ! rotate to receiver system of coordinates
       td(i)= ( nv(2)*s22+nv(3)*s23 )*dv(2) &
             +( nv(2)*s23+nv(3)*s33 )*dv(3)
       tn(i)= ( nv(2)*s22+nv(3)*s23 )*nv(2) &
             +( nv(2)*s23+nv(3)*s33 )*nv(3)

    END DO

  END SUBROUTINE computeTractionKernelsTriangleVolume2d

END MODULE kernels


