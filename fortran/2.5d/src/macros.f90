
#ifndef UNICYCLE_MACROS

#define UNICYCLE_MACROS 1

#define ACOSH(X) REAL(LOG(X+ZSQRT(DCMPLX(X-1._8,0._8)*(X+1._8))),8)
#define ACOTH(X) 0.5_8*REAL(LOG(DCMPLX(X+1._8,0._8))-LOG(DCMPLX(X-1._8,0._8)),8)

#define WRITE_DEBUG_INFO(e) WRITE (0,'("ERROR ",I3.3," at line ",I5.5,", rank ",I4.4)') e,__LINE__,rank
#define WRITE_DEBUG_INFO_SERIAL(e) WRITE (0,'("error ",I3.3," at line ",I5.5)') e,__LINE__

! for the Runge-Kutta method
#define TINY 1.d-30

!-------------------
! device numbers
!-------------------

! standard output
#define STDOUT 6

! standard error
#define STDERR 0

! time and time step file
#define FPTIME 14

! Paraview files
#define FPVTP 15

! output files
#define FPOUT 16

! data parallelism
#define FLAG_PATCH 1
#define FLAG_RECTANGLE_VOLUME 2
#define FLAG_TRIANGLE_VOLUME 3

!-----------------------
! degrees of freedom
!-----------------------

! number of slip components on patches
#define DGF_PATCH 2

! number of strain components in volumes
#define DGF_VOLUME 5

! number of traction components on patches
#define DGF_VECTOR 3

! number of stress components on volumes
#define DGF_TENSOR 5

!-----------------------
! index in state vector
!-----------------------

! patches
#define STATE_VECTOR_SLIP_STRIKE     0
#define STATE_VECTOR_SLIP_DIP        1
#define STATE_VECTOR_TRACTION_STRIKE 2
#define STATE_VECTOR_TRACTION_DIP    3
#define STATE_VECTOR_TRACTION_NORMAL 4
#define STATE_VECTOR_STATE_1         5
#define STATE_VECTOR_VELOCITY        6
#ifdef BATH
#define STATE_VECTOR_TEMPERATURE     7
#define STATE_VECTOR_DGF_PATCH       8
#else
#define STATE_VECTOR_DGF_PATCH       7
#endif

! volumes
#define STATE_VECTOR_E12         0
#define STATE_VECTOR_E13         1
#define STATE_VECTOR_E22         2
#define STATE_VECTOR_E23         3
#define STATE_VECTOR_E33         4
#define STATE_VECTOR_S12         5
#define STATE_VECTOR_S13         6
#define STATE_VECTOR_S22         7
#define STATE_VECTOR_S23         8
#define STATE_VECTOR_S33         9
#define STATE_VECTOR_DGF_VOLUME 10

!-------------------------------------
! index in traction and stress vector
!-------------------------------------

! patches
#define TRACTION_VECTOR_STRIKE  0
#define TRACTION_VECTOR_DIP     1
#define TRACTION_VECTOR_NORMAL  2

! volumes
#define TRACTION_VECTOR_S12     0
#define TRACTION_VECTOR_S13     1
#define TRACTION_VECTOR_S22     2
#define TRACTION_VECTOR_S23     3
#define TRACTION_VECTOR_S33     4

!-------------------------------------
! index in displacement vector
!-------------------------------------

#define DISPLACEMENT_VECTOR_NORTH 0
#define DISPLACEMENT_VECTOR_EAST  1
#define DISPLACEMENT_VECTOR_DOWN  2
#define DISPLACEMENT_VECTOR_DGF   3

#endif

