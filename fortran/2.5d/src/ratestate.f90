!> program Unicycle (unified cycle of earthquakes) simulates evolution
!! of fault slip and distributed strain using the integral method under
!! the radiation damping approximation.
!!
!! \mainpage
!! 
!! The Green's function for traction and stress interaction amongst 
!! fault patches and triangular and rectangular volume elements has the 
!! following layout
!!
!!       / KK KR KT \
!!   G = |          |
!!       \ LK LR LT /
!!
!! where
!!
!!   KK is the matrix for traction on faults due to fault slip
!!   KR is the matrix for traction on faults due to strain in rectangle volume elements
!!   KT is the matrix for traction on faults due to strain in triangle volume elements
!!   LK is the matrix for stress in volumes due to fault slip
!!   LR is the matrix for stress in volumes due to strain in rectangle volume elements
!!   LT is the matrix for stress in volumes due to strain in triangle volume elements
!!
!! All fault patches have two slip components: strike slip and dip slip.
!! The interaction matrix becomes
!!
!!        / KKss    0  \
!!   KK = |            |
!!        \   0   KKdd /
!!
!! All volume elements have 5 components of strain. The interation matrix
!! can be written
!!
!!
!!       / LR1212  LR1213     0       0       0   \
!!       |                                        |
!!       | LR1312  LR1313     0       0       0   |
!!       |                                        |
!!  LR = |    0       0    LR2222  LR2223  LR2233 |
!!       |                                        |
!!       |    0       0    LR2322  LR2323  LR2333 |
!!       |                                        |
!!       \    0       0    LR3322  LR3323  LR3333 /
!!
!! The time evolution is evaluated numerically using the 4th/5th order
!! Runge-Kutta method with adaptive time steps. The state vector is 
!! as follows:
!!
!!    / R1 1       \   +-------------------+
!!    | .          |   |                   |
!!    | R1 dPatch  |   |                   |
!!    | .          |   |  nPatch * dPatch  |
!!    | .          |   |                   |
!!    | Rn 1       |   |                   |
!!    | .          |   |                   |
!!    \ Rn dPatch  /   +-------------------+
!!
!! where nPatch is the number of patches and dPatch is the degrees of 
!! freedom for patches.
!!
!! For every patch, we have the following items in the state vector
!!
!!   /  ss   \  1
!!   |  sd   |  .
!!   |  ts   |  .
!!   |  td   |  .
!!   |  tn   |  .
!!   ! theta*|  .
!!   \  v*   /  dPatch
!!
!! where ts, td, and td are the local traction in the strike, dip, and
!! normal directions, ss and sd are the cumulative slip in the strike and dip 
!! directions, v*=log10(v) is the logarithm of the norm of the velocity,
!! and theta*=log10(theta) is the logarithm of the state variable in the 
!! rate and state friction framework.
!!
!! References:<br>
!!
!!   Barbot S., J. D.-P. Moore, and V. Lambert, "Displacement and Stress
!!   Associated with Distributed Anelastic Deformation in a Half-Space",
!!   Bull. Seism. Soc. Am., 10.1785/0120160237, 2017.
!!
!!   Barbot, S., "Deformation of a Half‐Space from Anelastic Strain
!!   Confined in a Tetrahedral Volume", Bull. Seism. Soc. Am.,
!!   10.1785/0120180058, 2020.
!!
!! \author Sylvain Barbot (2020).
!----------------------------------------------------------------------
PROGRAM ratestate

#include "macros.f90"

#ifdef NETCDF
  USE exportnetcdf
#endif
  USE greens
  USE ode45
  USE types 

  IMPLICIT NONE

  INCLUDE 'mpif.h'

  REAL*8, PARAMETER :: DEG2RAD = 0.01745329251994329547437168059786927_8

  ! MPI rank and size
  INTEGER :: rank,csize

  ! error flag
  INTEGER :: ierr

#ifdef NETCDF
  ! netcdf file and variable
  INTEGER :: ncid,y_varid,z_varid,ncCount
#endif

  CHARACTER(512) :: filename

  ! maximum velocity
  REAL*8 :: vMax,vMaxAll

  ! moment rate
  REAL*8 :: momentRate, momentRateAll

  ! scaling factor
  REAL*8, PARAMETER :: lg10=LOG(1.d1)

  ! state vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: y
  ! rate of change of state vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: dydt,yscal
  ! temporary variables
  REAL*8, DIMENSION(:), ALLOCATABLE :: ytmp,ytmp1,ytmp2,ytmp3

  ! slip velocity vector and strain rate tensor array (temporary space)
  REAL*8, DIMENSION(:), ALLOCATABLE :: v

  ! slip velocity vector and strain rate tensor array (temporary space)
  REAL*8, DIMENSION(:), ALLOCATABLE :: vAll

  ! traction vector and stress tensor array (temporary space)
  REAL*8, DIMENSION(:), ALLOCATABLE :: t

  ! displacement and kinematics vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: d,u,dAll

  ! Green's functions
  REAL*8, DIMENSION(:,:), ALLOCATABLE :: G,O,Of,Ol

  ! time
  REAL*8 :: time,t0
  ! time step
  REAL*8 :: dt_try,dt_next,dt_done
  ! time steps
  INTEGER :: i,j

  ! type of friction law (default)
  INTEGER :: frictionLawType=1

  ! maximum number of time steps (default)
  INTEGER :: maximumIterations=1000000

  ! layout for parallelism
  TYPE(LAYOUT_STRUCT) :: layout

  ! model parameters
  TYPE(SIMULATION_STRUCT) :: in

  ! initialization
  CALL MPI_INIT(ierr)
  CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
  CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

  ! start time
  time=0.d0

  ! initial tentative time step
  dt_next=1.0d-3

  ! retrieve input parameters from command line
  CALL init(in)
  CALL FLUSH(STDOUT)

  IF (in%isdryrun .AND. 0 .EQ. rank) THEN
     PRINT '("dry run: abort calculation")'
  END IF
  IF (in%isdryrun .OR. in%isversion .OR. in%ishelp) THEN
     CALL MPI_FINALIZE(ierr)
     STOP
  END IF

  ! calculate basis vectors
  CALL initGeometry(in)

  ! describe data layout for parallelism
  CALL initParallelism(in,layout)

#ifdef NETCDF
  IF (in%isImportGreens) THEN
     CALL initG(in,layout,G)
     IF (0 .EQ. rank) THEN
        PRINT '("# import Green''s function.")'
     END IF
     CALL importGreensNetcdf(G)
  ELSE
     ! calculate the stress interaction matrix
     IF (0 .EQ. rank) THEN
        PRINT '("# computing Green''s functions.")'
     END IF
     CALL buildG(in,layout,G)
     IF (in%isExportGreens) THEN
        IF (0 .EQ. rank) THEN
           PRINT '("# exporting Green''s functions to ",a)',TRIM(in%greensFunctionDirectory)
        END IF
        CALL exportGreensNetcdf(G)
     END IF
  END IF
#else
  ! calculate the stress interaction matrix
  IF (0 .EQ. rank) THEN
     PRINT '("# computing Green''s functions.")'
  END IF
  CALL buildG(in,layout,G)
#endif

  CALL buildO(in,layout,O,Of,Ol)
  DEALLOCATE(Of,Ol)

  IF (0 .EQ. rank) THEN
     PRINT 2000
  END IF

  ! velocity vector and strain rate tensor array (t=G*vAll)
  ALLOCATE(u(layout%listVelocityN(1+rank)), &
           v(layout%listVelocityN(1+rank)), &
           vAll(SUM(layout%listVelocityN)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the velocity and strain rate vector"

  ! traction vector and stress tensor array (t=G*v)
  ALLOCATE(t(layout%listForceN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the traction and stress vector"

  ! displacement vector (d=O*v)
  ALLOCATE(d   (in%nObservationPoint*DISPLACEMENT_VECTOR_DGF), &
           dAll(in%nObservationPoint*DISPLACEMENT_VECTOR_DGF),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the displacement vector"

  ! state vector
  ALLOCATE(y(layout%listStateN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the state vector"

  ! rate of state vector
  ALLOCATE(dydt(layout%listStateN(1+rank)), &
           yscal(layout%listStateN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the state vectors"

  ALLOCATE(ytmp (layout%listStateN(1+rank)), &
           ytmp1(layout%listStateN(1+rank)), &
           ytmp2(layout%listStateN(1+rank)), &
           ytmp3(layout%listStateN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the RKQS work space"

  ! allocate buffer from ode45 module
  ALLOCATE(AK2(layout%listStateN(1+rank)), &
           AK3(layout%listStateN(1+rank)), &
           AK4(layout%listStateN(1+rank)), &
           AK5(layout%listStateN(1+rank)), &
           AK6(layout%listStateN(1+rank)), &
           yrkck(layout%listStateN(1+rank)), STAT=ierr)
  IF (ierr>0) STOP "could not allocate the AK1-6 work space"

  IF (0 .EQ. rank) THEN
     OPEN (UNIT=FPTIME,FILE=in%timeFilename,IOSTAT=ierr,FORM="FORMATTED")
     IF (ierr>0) THEN
        WRITE_DEBUG_INFO(102)
        WRITE (STDERR,'("error: unable to access ",a)') TRIM(in%timefilename)
        STOP 1
     END IF
  END IF

  ! initialize the y vector
  IF (0 .EQ. rank) THEN
     PRINT '("# initialize state vector.")'
  END IF
  CALL initStateVector(layout%listStateN(1+rank),y,in)
  IF (0 .EQ. rank) THEN
     PRINT 2000
  END IF

#ifdef NETCDF
  ! initialize netcdf output
  IF (in%isExportNetcdf) THEN
     CALL initnc()
  END IF
#endif

  ! gather maximum velocity
  CALL MPI_REDUCE(vMax,vMaxAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

  ! gather moment rate
  CALL MPI_REDUCE(momentRate,momentRateAll,1,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

  ! initialize output
  IF (0 .EQ. rank) THEN
     WRITE(STDOUT,'("# export to ",a)') TRIM(in%wdir)
     PRINT 2000
     WRITE(STDOUT,'("#       n               time                 dt       vMax")')
     WRITE(STDOUT,'(I9.9,ES19.12E2,ES19.12E2,ES11.4E2)') 0,time,dt_next,vMaxAll
     WRITE(FPTIME,'("#               time                 dt               vMax         Moment-rate")')
  END IF

  ! initialize observation patch
  DO j=1,in%nObservationState
     IF ((in%observationState(1,j) .GE. layout%listOffset(rank+1)) .AND. &
         (in%observationState(1,j) .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN

        in%observationState(3,j)=100+j
        WRITE (filename,'(a,"/patch-",I8.8,"-",I8.8,".dat")') TRIM(in%wdir),j,in%observationState(1,j)
        OPEN (UNIT=in%observationState(3,j), &
              FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
        IF (ierr>0) THEN
           WRITE_DEBUG_INFO(102)
           WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
           STOP 1
        END IF
     END IF
  END DO

  ! initialize observation points
  IF (0 .EQ. rank) THEN
     DO j=1,in%nObservationPoint
        in%observationPoint(j)%file=1000+j
        WRITE (filename,'(a,"/opts-",a,".dat")') TRIM(in%wdir),TRIM(in%observationPoint(j)%name)
        OPEN (UNIT=in%observationPoint(j)%file, &
              FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
        IF (ierr>0) THEN
           WRITE_DEBUG_INFO(102)
           WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
           STOP 1
        END IF
     END DO
  END IF

  ! main loop
  DO i=1,maximumIterations

     CALL odefun(layout%listStateN(1+rank),time,y,dydt)

     CALL export()
     CALL exportPoints()

#ifdef NETCDF
     IF (0 .EQ. rank) THEN
        IF (in%isExportNetcdf) THEN
           IF (0 .EQ. MOD(i,20)) THEN
              CALL exportnc(in%nPatch)
           END IF
        END IF
     END IF
#endif

     dt_try=dt_next
     yscal=ABS(y)+ABS(dt_try*dydt)+TINY

     t0=time
     CALL RKQSm(layout%listStateN(1+rank),t0,y,dydt, &
               yscal,ytmp1,ytmp2,ytmp3,dt_try,dt_done,dt_next,odefun)

     time=time+dt_done

     ! end calculation
     IF (in%interval .LE. time) THEN
        EXIT
     END IF
   
  END DO

  IF (0 .EQ. rank) THEN
     PRINT '(I9.9," time steps.")', i
  END IF

  IF (0 .EQ. rank) THEN
     CLOSE(FPTIME)
  END IF

  ! close observation state files
  DO j=1,in%nObservationState
     IF ((in%observationState(1,j) .GE. layout%listOffset(rank+1)) .AND. &
         (in%observationState(1,j) .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN
        CLOSE(in%observationState(3,j))
     END IF
  END DO

  ! close the observation points
  IF (0 .EQ. rank) THEN
     DO j=1,in%nObservationPoint
        CLOSE(in%observationPoint(j)%file)
     END DO
  END IF

  DEALLOCATE(y,dydt,yscal)
  DEALLOCATE(ytmp,ytmp1,ytmp2,ytmp3,yrkck)
  DEALLOCATE(AK2,AK3,AK4,AK5,AK6)
  DEALLOCATE(G,v,vAll,t)
  DEALLOCATE(layout%listForceN)
  DEALLOCATE(layout%listVelocityN,layout%listVelocityOffset)
  DEALLOCATE(layout%listStateN,layout%listStateOffset)
  DEALLOCATE(layout%elementStateIndex)
  DEALLOCATE(layout%listElements,layout%listOffset)
  DEALLOCATE(layout%elementType)
  DEALLOCATE(O,d,u,dAll)

  CALL MPI_FINALIZE(ierr)

2000 FORMAT ("# ----------------------------------------------------------------------------")
     
CONTAINS

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportGreensNetcdf
  ! export the Greens function by rank
  !----------------------------------------------------------------------
  SUBROUTINE exportGreensNetcdf(M)
    REAL*8, DIMENSION(:,:), INTENT(IN) :: M

    REAL*8, DIMENSION(:), ALLOCATABLE :: x,y

    INTEGER :: i,ierr
    CHARACTER(LEN=256) :: filename

    ALLOCATE(x(SIZE(M,1)),y(SIZE(M,2)),STAT=ierr)
    IF (ierr/=0) STOP "could not allocate netcdf coordinate for Greens function"

    ! loop over all patch elements
    DO i=1,SIZE(M,1)
       x(i)=REAL(i,8)
    END DO
    DO i=1,SIZE(M,2)
       y(i)=REAL(i,8)
    END DO

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/greens-",I4.4,".grd")') TRIM(in%greensFunctionDirectory),rank
    CALL writeNetcdf(filename,SIZE(M,1),x,SIZE(M,2),y,M,1)

    DEALLOCATE(x,y)

  END SUBROUTINE exportGreensNetcdf
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine importGreensNetcdf
  ! import the Greens function by rank
  !----------------------------------------------------------------------
  SUBROUTINE importGreensNetcdf(M)
    REAL*8, DIMENSION(:,:), INTENT(OUT) :: M

    CHARACTER(LEN=256) :: filename

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/greens-",I4.4,".grd")') TRIM(in%greensFunctionDirectory),rank
    CALL readNetcdf(filename,SIZE(M,1),SIZE(M,2),M)

  END SUBROUTINE importGreensNetcdf
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine initnc
  ! initializes the coordinates of netcdf files
  !----------------------------------------------------------------------
  SUBROUTINE initnc()

    REAL*8, DIMENSION(:), ALLOCATABLE :: x

    INTEGER :: i,ierr
    CHARACTER(LEN=256) :: filename

    IF (0 .EQ. rank) THEN

       ! initialize the number of exports
       ncCount = 0

       ALLOCATE(x(in%nPatch),STAT=ierr)
       IF (ierr/=0) STOP "could not allocate netcdf coordinate"

       ! loop over all patch elements
       DO i=1,in%nPatch
          x(i)=REAL(i,8)
       END DO

       ! netcdf file is compatible with GMT
       WRITE (filename,'(a,"/log10v.grd")') TRIM(in%wdir)
       CALL openNetcdfUnlimited(filename,in%nPatch,x,ncid,y_varid,z_varid)

       DEALLOCATE(x)

    END IF

  END SUBROUTINE initnc
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportnc
  ! export time series of log10(v)
  !----------------------------------------------------------------------
  SUBROUTINE exportnc(n)
    INTEGER, INTENT(IN) :: n

    REAL*4, DIMENSION(n) :: z

    INTEGER :: j,l,ierr

    ! update the export count
    ncCount=ncCount+1

    ! loop over all patch elements
    DO j=1,in%nPatch
       ! dip slip
       z(j)=REAL(LOG10( &
               SQRT((in%patch%s(j)%Vl*COS(in%patch%s(j)%rake)+vAll(DGF_PATCH*(j-1)+1))**2 &
                   +(in%patch%s(j)%Vl*SIN(in%patch%s(j)%rake)+vAll(DGF_PATCH*(j-1)+2))**2)),4)
    END DO

    CALL writeNetcdfUnlimited(ncid,y_varid,z_varid,ncCount,n,z)

  END SUBROUTINE exportnc
#endif

  !-----------------------------------------------------------------------
  !> subroutine exportPoints
  ! export observation points
  !----------------------------------------------------------------------
  SUBROUTINE exportPoints()

    IMPLICIT NONE

    INTEGER :: i,k,l,ierr
    INTEGER :: elementIndex
    CHARACTER(1024) :: formatString
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    !-----------------------------------------------------------------
    ! step 1/4 - gather the kinematics from the state vector
    !-----------------------------------------------------------------

    ! element index in d vector
    k=1
    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementIndex=layout%elementIndex(i)

       patch=in%patch%s(elementIndex)

       ! strike-slip and dip-slip components
       u(k:k+layout%elementVelocityDGF(i)-1)= (/ &
               y(l+STATE_VECTOR_SLIP_STRIKE), &
               y(l+STATE_VECTOR_SLIP_DIP) /)

       l=l+in%dPatch

       k=k+layout%elementVelocityDGF(i)
    END DO

    !-----------------------------------------------------------------
    ! step 2/4 - calculate the rate of traction and rate of stress
    !-----------------------------------------------------------------

#ifdef __BLAS__
    ! use the BLAS library to compute the matrix vector product
    CALL DGEMV("T",SIZE(O,1),SIZE(O,2), &
                1._8,O,SIZE(O,1),u,1,0.d0,d,1)
#else
    ! slower, Fortran intrinsic
    d=MATMUL(TRANSPOSE(O),u)
#endif

    !-----------------------------------------------------------------
    ! step 3/4 - master thread adds the contribution of all elements
    !-----------------------------------------------------------------

    CALL MPI_REDUCE(d,dAll,in%nObservationPoint*DISPLACEMENT_VECTOR_DGF, &
                    MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

    !-----------------------------------------------------------------
    ! step 4/4 - master thread writes to disk
    !-----------------------------------------------------------------

    IF (0 .EQ. rank) THEN
       formatString="(ES21.14E2"
       DO i=1,DISPLACEMENT_VECTOR_DGF
          formatString=TRIM(formatString)//",X,ES19.12E2"
       END DO
       formatString=TRIM(formatString)//")"

       ! element index in d vector
       k=1
       DO i=1,in%nObservationPoint
          WRITE (in%observationPoint(i)%file,TRIM(formatString)) time,dAll(k:k+DISPLACEMENT_VECTOR_DGF-1)
          k=k+DISPLACEMENT_VECTOR_DGF
       END DO
    END IF

  END SUBROUTINE exportPoints

  !-----------------------------------------------------------------------
  !> subroutine export
  ! write the state variables of patch elements, and other information.
  !----------------------------------------------------------------------
  SUBROUTINE export()

    ! degrees of freedom
    INTEGER :: dgf

    ! counters
    INTEGER :: j,k

    ! index in state vector
    INTEGER :: index

    ! format string
    CHARACTER(1024) :: formatString

    ! gather maximum velocity
    CALL MPI_REDUCE(vMax,vMaxAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

    ! gather moment-rate
    CALL MPI_REDUCE(momentRate,momentRateAll,1,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

    ! export observation state
    DO j=1,in%nObservationState
       IF ((in%observationState(1,j) .GE. layout%listOffset(rank+1)) .AND. &
           (in%observationState(1,j) .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN

          ! check observation state sampling rate
          IF (0 .EQ. MOD(i-1,in%observationState(2,j))) THEN
             dgf=in%dPatch

             formatString="(ES22.16E2"
             DO k=1,dgf
                formatString=TRIM(formatString)//",X,ES20.12E3,X,ES20.12E3"
             END DO
             formatString=TRIM(formatString)//")"

             index=layout%elementStateIndex(in%observationState(1,j)-layout%listOffset(rank+1)+1)-dgf
             WRITE (in%observationState(3,j),TRIM(formatString)) time, &
                       y(index+1:index+dgf), &
                    dydt(index+1:index+dgf)
          END IF
       END IF
    END DO

    IF (0 .EQ. rank) THEN
       WRITE(FPTIME,'(ES21.15E2,ES19.12E2,ES19.12E2,ES20.12E2)') time,dt_done,vMaxAll,momentRateAll
       IF (0 .EQ. MOD(i,50)) THEN
          WRITE(STDOUT,'(I9.9,ES19.12E2,ES19.12E2,ES11.4E2)') i,time,dt_done,vMaxAll
          CALL FLUSH(STDOUT)
          CALL FLUSH(FPTIME)
       END IF
    END IF

  END SUBROUTINE export

  !-----------------------------------------------------------------------
  !> subroutine initStateVector
  ! initialize the state vector
  !
  ! INPUT:
  ! @param n - number of state elements own by current thread
  ! @param y - the state vector (segment owned by currect thread)
  !
  !----------------------------------------------------------------------
  SUBROUTINE initStateVector(n,y,in)
    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: n
    REAL*8, INTENT(OUT)    :: y(n)
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in

    INTEGER :: i,l
    INTEGER :: elementIndex
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    ! velocity perturbation
    REAL*8 :: modifier = 0.98d0

    ! initial stress
    REAL*8 :: tau

    ! initial velocity
    REAL*8 :: Vinit

    ! initial state variable (s)
    REAL*8 :: Tinit

    ! zero out state vector
    y=0._8

    ! maximum velocity
    vMax=0._8

    ! moment-rate
    momentRate=0._8

    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementIndex=layout%elementIndex(i)
       patch=in%patch%s(elementIndex)

       ! traction
       IF (0 .GT. patch%tau0) THEN

          ! initial velocity
          Vinit=modifier*patch%Vl

          SELECT CASE(frictionLawType)
          CASE(1)
             ! multiplicative form of rate-state friction (Barbot, 2018)
             tau = patch%mu0*patch%sig*exp((patch%a-patch%b)/patch%mu0*log(Vinit/patch%Vo))
          CASE(2)
             ! additive form of rate-state friction (Ruina, 1983)
             tau = patch%sig*(patch%mu0+(patch%a-patch%b)*log(Vinit/patch%Vo))
          CASE(3)
             ! arcsinh form of rate-state friction (Rice & Benzion, 1996)
             tau = patch%a*patch%sig*ASINH(patch%Vl/2/patch%Vo*exp((patch%mu0+patch%b*log(patch%Vo/Vinit))/patch%a))
          CASE(4)
             ! cut-off velocity form of rate-state friction (Okubo, 1989)
             tau = patch%sig*(patch%mu0-patch%a*log(1._8+patch%Vo/patch%Vl)+patch%b*log(1._8+patch%V2/patch%Vl))
          CASE DEFAULT
             WRITE (0,'("unhandled option ", a, " (this is a bug")') frictionLawType
             WRITE_DEBUG_INFO(100)
             STOP 3
          END SELECT
       ELSE
          ! set traction from input file
          tau = patch%tau0

          ! set initial velocity to local loading rate
          Vinit = patch%Vl
       END IF

       ! set state variable log10(theta) compatible with velocity and traction
       SELECT CASE(frictionLawType)
       CASE(1)
          ! multiplicative form of rate-state friction (Barbot, 2019)
          Tinit=(LOG(patch%L/patch%Vo)+(patch%a*LOG(patch%Vo/patch%Vl)+patch%mu0*LOG(tau/patch%mu0/patch%sig))/patch%b)/lg10
       CASE(2)
          ! additive form of rate-state friction (Ruina, 1983)
          Tinit=(LOG(patch%L/patch%Vo)+(patch%a*LOG(patch%Vo/patch%Vl)+tau/patch%sig-patch%mu0)/patch%b)/lg10
       CASE(3)
          ! arcsinh form of rate-state friction (Rice & Benzion, 1996)
          Tinit=(LOG(patch%L/patch%Vo) &
                 +patch%a/patch%b*LOG(2*patch%Vo/patch%Vl*SINH(tau/patch%a/patch%sig)) &
                 -patch%mu0/patch%b)/lg10
       CASE(4)
          ! rate-state friction with cut-off velocity (Okubo, 1989)
          Tinit=(LOG(patch%L/patch%V2)+(patch%a*LOG(patch%V2/patch%Vl)+tau/patch%sig-patch%mu0)/patch%b)/lg10
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug")') frictionLawType
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT

       ! strike slip
       y(l+STATE_VECTOR_SLIP_STRIKE) = 0._8

       ! dip slip
       y(l+STATE_VECTOR_SLIP_DIP) = 0._8

       ! traction in strike direction
       y(l+STATE_VECTOR_TRACTION_STRIKE) = tau*COS(patch%rake)

       ! traction in dip direction
       y(l+STATE_VECTOR_TRACTION_DIP) = tau*SIN(patch%rake)

       ! traction in normal direction
       y(l+STATE_VECTOR_TRACTION_NORMAL) = 0._8

       ! state variable log10(theta)
       y(l+STATE_VECTOR_STATE_1) = Tinit

       ! maximum velocity
       vMax=MAX(patch%Vl,vMax)

       ! moment-rate
       momentRate=momentRate+(Vinit-patch%Vl)*in%mu*in%patch%width(elementIndex)

       ! slip velocity log10(V)
       y(l+STATE_VECTOR_VELOCITY) = log(Vinit)/lg10

       l=l+in%dPatch

    END DO

  END SUBROUTINE initStateVector

  !-----------------------------------------------------------------------
  !> subroutine odefun
  ! evalutes the derivative of the state vector
  !
  ! @param n - number of state elements own by current thread
  ! @param m - degrees of freedom
  !
  ! DESCRIPTION:
  !   1- extract slip velocity and strain rate from state vector
  !   2- calculate the rate of traction and rate of stress
  !   3- calculate the rate of remaining state variables
  !----------------------------------------------------------------------
  SUBROUTINE odefun(n,time,y,dydt)
    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: n
    REAL*8, INTENT(IN)    :: time
    REAL*8, INTENT(IN)    :: y(n)
    REAL*8, INTENT(INOUT) :: dydt(n)

    INTEGER :: i,j,k,l,ierr
    INTEGER :: elementIndex
    TYPE(PATCH_ELEMENT_STRUCT) :: patch
    REAL*8 :: correction

    ! traction components in the strike and dip directions
    REAL*8 :: ts, td

    ! scalar rate of shear traction
    REAL*8 :: dtau

    ! velocity scalar
    REAL*8 :: velocity

    ! slip velocity in the strike and dip directions
    REAL*8 :: vs,vd

    ! rake of traction and velocity
    REAL*8 :: rake

    ! friction
    REAL*8 :: friction

    ! state variable theta
    REAL*8 :: theta

    ! normal stress
    REAL*8 :: sigma

    ! modifier for the arcsinh form of rate-state friction
    REAL*8 :: reg

    ! maximum velocity
    vMax=0._8

    ! initialize rate of state vector
    dydt=0._8

    ! initialize moment-rate
    momentRate=0._8

    !--------------------------------------------------------------------
    ! step 1/3 - extract slip velocity and strain rate from state vector
    !--------------------------------------------------------------------

    ! element index in v vector
    k=1
    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementIndex=layout%elementIndex(i)

       patch=in%patch%s(elementIndex)

       ! traction and rake
       ts=y(l+STATE_VECTOR_TRACTION_STRIKE)
       td=y(l+STATE_VECTOR_TRACTION_DIP)
       rake=ATAN2(td,ts)

       ! slip velocity
       velocity=DEXP(y(l+STATE_VECTOR_VELOCITY)*lg10)
       vs=velocity*COS(rake)
       vd=velocity*SIN(rake)

       ! maximum velocity
       vMax=MAX(velocity,vMax)

       ! update state vector (rate of slip components)
       dydt(l+STATE_VECTOR_SLIP_STRIKE)=vs
       dydt(l+STATE_VECTOR_SLIP_DIP   )=vd

       ! v(k:k+layout%elementVelocityDGF(i)-1) = slip velocity
       v(k:k+layout%elementVelocityDGF(i)-1)=(/ &
                     vs-patch%Vl*COS(patch%rake), &
                     vd-patch%Vl*SIN(patch%rake) /)

       l=l+in%dPatch

       k=k+layout%elementVelocityDGF(i)
    END DO

    ! all threads gather velocity from all threads
    CALL MPI_ALLGATHERV(v,layout%listVelocityN(1+rank),MPI_REAL8, &
                        vAll,layout%listVelocityN,layout%listVelocityOffset,MPI_REAL8, &
                        MPI_COMM_WORLD,ierr)

    !-----------------------------------------------------------------
    ! step 2/3 - calculate the rate of traction and rate of stress
    !-----------------------------------------------------------------

#ifdef __BLAS__
    ! use the BLAS library to compute the matrix vector product
    CALL DGEMV("T",SIZE(G,1),SIZE(G,2), &
                1._8,G,SIZE(G,1),vAll,1,0.d0,t,1)
#else
    ! slower, Fortran intrinsic
    t=MATMUL(TRANSPOSE(G),vAll)
#endif

    !-----------------------------------------------------------------
    ! step 3/3 - calculate the rate of remaining state variables
    !-----------------------------------------------------------------

    ! element index in t vector
    j=1
    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementIndex=layout%elementIndex(i)

       patch=in%patch%s(elementIndex)

       ! traction and rake
       ts=y(l+STATE_VECTOR_TRACTION_STRIKE)
       td=y(l+STATE_VECTOR_TRACTION_DIP)
       rake=ATAN2(td,ts)

       ! slip velocity
       velocity=DEXP(y(l+STATE_VECTOR_VELOCITY)*lg10)

       ! moment-rate
       momentRate=momentRate+(velocity-patch%Vl)*in%mu*in%patch%width(elementIndex)

       ! rate of state
       dydt(l+STATE_VECTOR_STATE_1)=(EXP(-y(l+STATE_VECTOR_STATE_1)*lg10)-velocity/patch%L)/lg10

       ! scalar rate of shear traction
       dtau=t(j+TRACTION_VECTOR_STRIKE)*COS(rake) &
           +t(j+TRACTION_VECTOR_DIP)   *SIN(rake)
          
       ! normal stress
       sigma=patch%sig-y(l+STATE_VECTOR_TRACTION_NORMAL)
       !sigma=patch%sig

       SELECT CASE(frictionLawType)
       CASE(1)
          ! multiplicative form of rate-state friction (Barbot, 2018)
          friction=patch%mu0*exp(patch%a/patch%mu0*log(velocity/patch%Vo) &
                                +patch%b/patch%mu0*(y(l+STATE_VECTOR_STATE_1)*lg10+log(patch%Vo/patch%L)))

          ! acceleration (1/V dV/dt) / log(10)
          !                               +friction*t(j+TRACTION_VECTOR_NORMAL)) / &
          dydt(l+STATE_VECTOR_VELOCITY)= &
               (dtau-patch%b*sigma*friction/patch%mu0*dydt(l+STATE_VECTOR_STATE_1)*lg10 &
                                                                               ) / &
               (patch%a*sigma*friction/patch%mu0+patch%damping*velocity) / lg10
       CASE(2)
          ! additive form of rate-state friction (Ruina, 1983)
          friction=patch%mu0+patch%a*log(velocity/patch%Vo) &
                            +patch%b*(y(l+STATE_VECTOR_STATE_1)*lg10+LOG(patch%Vo/patch%L))

          ! acceleration
          dydt(l+STATE_VECTOR_VELOCITY)= &
               (dtau-patch%b*sigma*dydt(l+STATE_VECTOR_STATE_1)*lg10 &
                                          +friction*t(j+TRACTION_VECTOR_NORMAL)) / &
               (patch%a*sigma+patch%damping*velocity) / lg10
       CASE(3)
          ! arcsinh form of rate-state friction (Rice & Benzion, 1996)
          reg=2.0d0*patch%Vo/velocity*exp(-(patch%mu0+patch%b*(y(STATE_VECTOR_STATE_1)*lg10-log(patch%L/patch%Vo)))/patch%a)

          friction=patch%a*ASINH(1._8/reg)

          reg=1.0d0/SQRT(1._8+reg**2)

          ! acceleration
          dydt(l+STATE_VECTOR_VELOCITY)= &
                  (dtau-patch%b*sigma*dydt(l+STATE_VECTOR_STATE_1)*lg10*reg &
                                          +friction*t(j+TRACTION_VECTOR_NORMAL)) / &
                  (patch%a*sigma*reg+patch%damping*velocity) / lg10
       CASE(4)
          ! cut-off velocity form of rate-state friction (Okubo, 1989)
          friction=patch%mu0-patch%a*log(1._8+patch%Vo/velocity) &
                            +patch%b*log(1._8+exp(y(l+STATE_VECTOR_STATE_1)*lg10)/patch%L*patch%V2)

          ! state variable
          theta=exp(y(l+STATE_VECTOR_STATE_1)*lg10)

          ! acceleration
          dydt(l+STATE_VECTOR_VELOCITY)= &
               (dtau-patch%b*sigma*dydt(l+STATE_VECTOR_STATE_1)*lg10/(1._8+patch%L/patch%V2/theta) &
                                          +friction*t(j+TRACTION_VECTOR_NORMAL)) / &
               (patch%a*sigma/(1._8+velocity/patch%Vo)+patch%damping*velocity) / lg10
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug")') frictionLawType
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT

       ! correction
       correction=patch%damping*velocity*dydt(l+STATE_VECTOR_VELOCITY)*lg10

       ! traction rate
       dydt(l+STATE_VECTOR_TRACTION_STRIKE)=t(j+TRACTION_VECTOR_STRIKE)-correction*COS(rake)
       dydt(l+STATE_VECTOR_TRACTION_DIP   )=t(j+TRACTION_VECTOR_DIP   )-correction*SIN(rake)
       dydt(l+STATE_VECTOR_TRACTION_NORMAL)=t(j+TRACTION_VECTOR_NORMAL)

       l=l+in%dPatch

       j=j+layout%elementForceDGF(i)
    END DO

  END SUBROUTINE odefun

  !-----------------------------------------------------------------------
  !> subroutine initParallelism()
  !! initialize variables describe the data layout
  !! for parallelism.
  !!
  !! OUTPUT:
  !! layout    - list of receiver type and type index
  !-----------------------------------------------------------------------
  SUBROUTINE initParallelism(in,layout)
    IMPLICIT NONE
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in
    TYPE(LAYOUT_STRUCT), INTENT(OUT) :: layout

    INTEGER :: i,j,k,ierr,remainder
    INTEGER :: cumulativeIndex,cumulativeVelocityIndex
    INTEGER :: rank,csize
    INTEGER :: nElements

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

    ! list of number of elements in thread
    ALLOCATE(layout%listElements(csize), &
             layout%listOffset(csize),STAT=ierr)
    IF (ierr>0) STOP "could not allocate the list"

    ! total number of elements
    nElements=in%patch%ns &
             +in%rectangleVolume%ns &
             +in%triangleVolume%ns

    remainder=nElements-INT(nElements/csize)*csize
    IF (0 .LT. remainder) THEN
       layout%listElements(1:(csize-remainder))      =INT(nElements/csize)
       layout%listElements((csize-remainder+1):csize)=INT(nElements/csize)+1
    ELSE
       layout%listElements(1:csize)=INT(nElements/csize)
    END IF

    ! element start index in thread
    j=0
    k=0
    DO i=1,csize
       j=k+1
       k=k+layout%listElements(i)
       layout%listOffset(i)=j
    END DO

    ALLOCATE(layout%elementType       (layout%listElements(1+rank)), &
             layout%elementIndex      (layout%listElements(1+rank)), &
             layout%elementStateIndex (layout%listElements(1+rank)), &
             layout%elementVelocityDGF(layout%listElements(1+rank)), &
             layout%elementVelocityIndex(layout%listElements(1+rank)), &
             layout%elementStateDGF   (layout%listElements(1+rank)), &
             layout%elementForceDGF   (layout%listElements(1+rank)),STAT=ierr)
    IF (ierr>0) STOP "could not allocate the layout elements"

    j=1
    cumulativeIndex=0
    cumulativeVelocityIndex=0
    DO i=1,in%patch%ns
       IF ((i .GE. layout%listOffset(1+rank)) .AND. &
           (i .LT. (layout%listOffset(1+rank)+layout%listElements(1+rank)))) THEN
          layout%elementType(j)=FLAG_PATCH
          layout%elementIndex(j)=i
          layout%elementStateIndex(j)=cumulativeIndex+STATE_VECTOR_DGF_PATCH
          cumulativeIndex=layout%elementStateIndex(j)
          layout%elementVelocityDGF(j)=DGF_PATCH
          layout%elementVelocityIndex(j)=cumulativeVelocityIndex+DGF_PATCH
          cumulativeVelocityIndex=layout%elementVelocityIndex(j)
          layout%elementStateDGF(j)=in%dPatch
          layout%elementForceDGF(j)=DGF_VECTOR
          j=j+1
       END IF
    END DO

    ! for MPI_ALLGATHERV
    ALLOCATE(layout%listVelocityN(csize), &
             layout%listVelocityOffset(csize), &
             layout%listStateN(csize), &
             layout%listStateOffset(csize), &
             layout%listForceN(csize), &
             STAT=ierr)
    IF (ierr>0) STOP "could not allocate the size list"

    ! share number of elements in threads
    CALL MPI_ALLGATHER(SUM(layout%elementVelocityDGF),1,MPI_INTEGER,layout%listVelocityN,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
    CALL MPI_ALLGATHER(SUM(layout%elementStateDGF),   1,MPI_INTEGER,layout%listStateN,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
    CALL MPI_ALLGATHER(SUM(layout%elementForceDGF),   1,MPI_INTEGER,layout%listForceN,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)

    j=0
    k=0
    DO i=1,csize
       j=k+1
       k=k+layout%listVelocityN(i)
       layout%listVelocityOffset(i)=j-1
    END DO

    j=0
    k=0
    DO i=1,csize
       j=k+1
       k=k+layout%listStateN(i)
       layout%listStateOffset(i)=j-1
    END DO

  END SUBROUTINE initParallelism


  !-----------------------------------------------------------------------
  !> subroutine initGeometry
  ! initializes the position and local reference system vectors
  !
  ! INPUT:
  ! @param in      - input parameters data structure
  !-----------------------------------------------------------------------
  SUBROUTINE initGeometry(in)
    USE types
    USE kernels
    TYPE(SIMULATION_STRUCT), INTENT(INOUT) :: in

    IF (0 .LT. in%patch%ns) THEN
       CALL computeReferenceSystem2d( &
                in%patch%ns, &
                in%patch%x, &
                in%patch%width, &
                in%patch%dip, &
                in%patch%sv, &
                in%patch%dv, &
                in%patch%nv, &
                in%patch%xc)
    END IF

  END SUBROUTINE initGeometry

  !---------------------------------------------------------------------
  !> subroutine init
  !! reads simulation parameters from the standard input and initialize
  !! model parameters.
  !!
  !! INPUT:
  !! @param unit - the unit number used to read input data
  !!
  !! OUTPUT:
  !! @param in
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !---------------------------------------------------------------------
  SUBROUTINE init(in)
    USE types
    USE getopt_m

    TYPE(SIMULATION_STRUCT), INTENT(OUT) :: in

    INCLUDE 'mpif.h'

    CHARACTER :: ch
    CHARACTER(512) :: dataline
    CHARACTER(256) :: filename
    INTEGER :: iunit,noptions
!$  INTEGER :: omp_get_num_procs,omp_get_max_threads
    TYPE(OPTION_S) :: opts(10)
    REAL*8, DIMENSION(3) :: normal

    INTEGER :: i,k,rank,size,ierr,position
    INTEGER :: maxVertexIndex=-1
    INTEGER, PARAMETER :: psize=1024
    INTEGER :: dummy
    CHARACTER, DIMENSION(psize) :: packed

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,size,ierr)

    ! define long options, such as --dry-run
    ! parse the command line for options
    opts( 1)=OPTION_S("version",.FALSE.,CHAR(21))
    opts( 2)=OPTION_S("dry-run",.FALSE.,CHAR(22))
    opts( 3)=OPTION_S("epsilon",.TRUE.,'e')
    opts( 4)=OPTION_S("export-greens",.TRUE.,'g')
    opts( 5)=OPTION_S("export-netcdf",.FALSE.,'n')
    opts( 6)=OPTION_S("friction-law",.TRUE.,'f')
    opts( 7)=OPTION_S("import-greens",.TRUE.,'p')
    opts( 8)=OPTION_S("maximum-step",.TRUE.,'m')
    opts( 9)=OPTION_S("maximum-iterations",.TRUE.,'i')
    opts(10)=OPTION_S("help",.FALSE.,'h')

    noptions=0
    DO
       ch=getopt("he:f:g:i:m:p:",opts)
       SELECT CASE(ch)
       CASE(CHAR(0))
          EXIT
       CASE(CHAR(21))
          ! option version
          in%isversion=.TRUE.
       CASE(CHAR(22))
          ! option dry-run
          in%isdryrun=.TRUE.
       CASE('e')
          ! numerical accuracy (variable epsilon sits in the ode45 module)
          READ(optarg,*) epsilon
          noptions=noptions+1
       CASE('f')
          ! type of friction law
          READ(optarg,*) frictionLawType
          noptions=noptions+1
       CASE('g')
          ! export Greens functions to netcdf file
          READ(optarg,'(a)') in%greensFunctionDirectory
          in%isExportGreens=.TRUE.
          noptions=noptions+1
       CASE('i')
          ! maximum number of iterations
          READ(optarg,*) maximumIterations
          noptions=noptions+1
       CASE('m')
          ! maximum time step (variable maximumTimeStep sits in the ode45 module)
          READ(optarg,*) maximumTimeStep
          noptions=noptions+1
       CASE('n')
          ! export in netcdf format
          in%isExportNetcdf=.TRUE.
       CASE('p')
          ! import Greens functions from netcdf file
          READ(optarg,'(a)') in%greensFunctionDirectory
          in%isImportGreens=.TRUE.
          noptions=noptions+1
       CASE('h')
          ! option help
          in%ishelp=.TRUE.
       CASE('?')
          WRITE_DEBUG_INFO(100)
          in%ishelp=.TRUE.
          EXIT
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug")') optopt
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT
       noptions=noptions+1
    END DO

    IF (in%isversion) THEN
       CALL printversion()
       ! abort parameter input
       STOP
    END IF

    IF (in%ishelp) THEN
       CALL printhelp()
       ! abort parameter input
       STOP
    END IF

    in%nPatch=0
    ! minimum number of dynamic variables for patches
    in%dPatch=STATE_VECTOR_DGF_PATCH
    in%patch%ns=0
    in%nVolume=0
    in%dVolume=STATE_VECTOR_DGF_VOLUME
    in%rectangleVolume%ns=0
    in%triangleVolume%ns=0

    IF (0.eq.rank) THEN
       PRINT 2000
       PRINT '("# unicycle-2d-ratestate")'
       PRINT '("# quasi-dynamic earthquake simulation in three-dimensional media")'
       PRINT '("# with the integral method.")'
       SELECT CASE(frictionLawType)
       CASE(1)
          PRINT '("# friction law: multiplicative form of rate-state friction (Barbot, 2018)")'
       CASE(2)
          PRINT '("# friction law: additive form of rate-state friction (Ruina, 1983)")'
       CASE(3)
          PRINT '("# friction law: arcsinh form of rate-state friction (Rice & BenZion, 1996)")'
       CASE(4)
          PRINT '("# friction law: rate-state with cut-off velocity (Okubo, 1989)")'
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug")') frictionLawType
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT
       PRINT '("# numerical accuracy: ",ES11.4)', epsilon
       PRINT '("# maximum iterations: ",I11)', maximumIterations
       PRINT '("# maximum time step: ",ES12.4)', maximumTimeStep
       PRINT '("# number of threads: ",I12)', csize
#ifdef NETCDF
       IF (in%isImportGreens) THEN
          WRITE (STDOUT,'("# import greens function:         yes (",a,")")') TRIM(in%greensFunctionDirectory)
       ELSE
          IF (in%isExportGreens) THEN
             WRITE (STDOUT,'("# export greens function:         yes (",a,")")') TRIM(in%greensFunctionDirectory)
          END IF
       END IF
#endif
!$     PRINT '("#     * parallel OpenMP implementation with ",I3.3,"/",I3.3," threads")', &
!$                  omp_get_max_threads(),omp_get_num_procs()
       PRINT 2000

       IF (noptions .LT. COMMAND_ARGUMENT_COUNT()) THEN
          ! read from input file
          iunit=25
          CALL GET_COMMAND_ARGUMENT(noptions+1,filename)
          OPEN (UNIT=iunit,FILE=filename,IOSTAT=ierr)
       ELSE
          ! get input parameters from standard input
          iunit=5
       END IF

       PRINT '("# output directory")'
       CALL getdata(iunit,dataline)
       READ (dataline,'(a)') in%wdir
       PRINT '(2X,a)', TRIM(in%wdir)

       in%timeFilename=TRIM(in%wdir)//"/time.dat"

       ! test write permissions on output directory
       OPEN (UNIT=FPTIME,FILE=in%timeFilename,POSITION="APPEND",&
               IOSTAT=ierr,FORM="FORMATTED")
       IF (ierr>0) THEN
          WRITE_DEBUG_INFO(102)
          WRITE (STDERR,'("error: unable to access ",a)') TRIM(in%timefilename)
          STOP 1
       END IF
       CLOSE(FPTIME)
   
       PRINT '("# elastic moduli (lambda, mu)")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%lambda,in%mu
       PRINT '(2ES9.2E1)', in%lambda,in%mu

       IF (0 .GT. in%mu) THEN
          WRITE_DEBUG_INFO(-1)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("input error: shear modulus must be positive")')
          STOP 2
       END IF

       in%nu=in%lambda/2._8/(in%lambda+in%mu)
       IF (-1._8 .GT. in%nu) THEN
          WRITE_DEBUG_INFO(-1)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("input error: Poisson''s ratio must be greater than -1.")')
          STOP 2
       END IF
       IF (0.5_8 .LT. in%nu) THEN
          WRITE_DEBUG_INFO(-1)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("input error: Poisson''s ratio must be lower than 0.5.")')
          STOP 2
       END IF

       PRINT '("# time interval")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%interval
       PRINT '(ES20.12E2)', in%interval

       IF (in%interval .LE. 0._8) THEN
          WRITE (STDERR,'("**** error **** ")')
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("simulation time must be positive. exiting.")')
          STOP 1
       END IF

       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !                  P A T C H E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT '("# number of patches")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%patch%ns
       PRINT '(I5)', in%patch%ns
       IF (in%patch%ns .GT. 0) THEN
          ALLOCATE(in%patch%s(in%patch%ns), &
                   in%patch%x(3,in%patch%ns), &
                   in%patch%xc(3,in%patch%ns), &
                   in%patch%width(in%patch%ns), &
                   in%patch%dip(in%patch%ns), &
                   in%patch%beta(in%patch%ns), &
                   in%patch%sv(3,in%patch%ns), &
                   in%patch%dv(3,in%patch%ns), &
                   in%patch%nv(3,in%patch%ns),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the rectangular patch list"
          PRINT 2000
          PRINT '("#    n       Vl       x2       x3   width   dip   rake   beta")'
          PRINT 2000
          DO k=1,in%patch%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                  in%patch%s(k)%Vl, &
                  in%patch%x(2,k), &
                  in%patch%x(3,k), &
                  in%patch%width(k), &
                  in%patch%dip(k), &
                  in%patch%s(k)%rake, &
                  in%patch%beta(k)
             in%patch%s(k)%opening=0
   
             PRINT '(I6,3ES9.2E1,ES8.2E1,f6.1,2f7.1)',i, &
                  in%patch%s(k)%Vl, &
                  in%patch%x(2,k), &
                  in%patch%x(3,k), &
                  in%patch%width(k), &
                  in%patch%dip(k), &
                  in%patch%s(k)%rake, &
                  in%patch%s(k)%beta

             ! convert to radians
             in%patch%dip(k)=in%patch%dip(k)*DEG2RAD     
             in%patch%s(k)%rake=in%patch%s(k)%rake*DEG2RAD     
             in%patch%s(k)%beta=in%patch%s(k)%beta*DEG2RAD     

             IF (i .NE. k) THEN
                WRITE (STDERR,'("invalid patch definition")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
             IF (in%patch%width(k) .LE. 0._8) THEN
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: patch width must be positive.")')
                STOP 1
             END IF
                
          END DO
   
          ! export the patches
          filename=TRIM(in%wdir)//"/rfaults.flt.2d"
          OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          WRITE (FPOUT,'("#  n        Vl            x2            x3   width     dip  beta")')
          DO k=1,in%patch%ns
             WRITE (FPOUT,'(I4,ES10.2E2,2ES14.7E1,3ES8.2E1)') k, &
               in%patch%s(k)%Vl, &
               in%patch%x(2,k), &
               in%patch%x(3,k), &
               in%patch%width(k), &
               in%patch%dip(k), &
               in%patch%beta(k)
          END DO
          CLOSE(FPOUT)
                
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          !        F R I C T I O N   P R O P E R T I E S
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          PRINT 2000
          PRINT '("# number of frictional patches")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) dummy
          PRINT '(I5)', dummy
          IF (dummy .NE. in%patch%ns) THEN
             WRITE_DEBUG_INFO(-1)
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE(STDERR,'("input error: all patches require frictional properties")')
             STOP 2
          END IF
          SELECT CASE(frictionLawType)
          CASE(1:3)
             PRINT '("#    n     tau0      mu0      sig        a        b        L       Vo  G/(2Vs)")'
             PRINT 2000
             DO k=1,in%patch%ns
                CALL getdata(iunit,dataline)
                READ (dataline,*,IOSTAT=ierr) i, &
                      in%patch%s(k)%tau0, &
                      in%patch%s(k)%mu0, &
                      in%patch%s(k)%sig, &
                      in%patch%s(k)%a, &
                      in%patch%s(k)%b, &
                      in%patch%s(k)%L, &
                      in%patch%s(k)%Vo, &
                      in%patch%s(k)%damping
   
                PRINT '(I6,8ES9.2E1)',i, &
                     in%patch%s(k)%tau0, &
                     in%patch%s(k)%mu0, &
                     in%patch%s(k)%sig, &
                     in%patch%s(k)%a, &
                     in%patch%s(k)%b, &
                     in%patch%s(k)%L, &
                     in%patch%s(k)%Vo, &
                     in%patch%s(k)%damping
                
                IF (i .NE. k) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("invalid friction property definition for patch")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   WRITE (STDERR,'("error in input file: unexpected index")')
                   STOP 1
                END IF

                IF (0 .GE. in%patch%s(k)%L) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("error: invalid characteristic weakening distance for patch")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   STOP 1
                END IF
                IF (0 .GE. in%patch%s(k)%a) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("error: invalid velocity dependence for patch")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   STOP 1
                END IF
                IF (0 .GE. in%patch%s(k)%Vo) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("error: invalid reference velocity for patch")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   STOP 1
                END IF

             END DO

          CASE(4)
             PRINT '("#    n     tau0      mu0      sig        a        b        L       V1       V2  G/(2Vs)")'
             PRINT 2000
             DO k=1,in%patch%ns
                CALL getdata(iunit,dataline)
                READ (dataline,*,IOSTAT=ierr) i, &
                      in%patch%s(k)%tau0, &
                      in%patch%s(k)%mu0, &
                      in%patch%s(k)%sig, &
                      in%patch%s(k)%a, &
                      in%patch%s(k)%b, &
                      in%patch%s(k)%L, &
                      in%patch%s(k)%Vo, &
                      in%patch%s(k)%V2, &
                      in%patch%s(k)%damping
   
                PRINT '(I6,9ES9.2E1)',i, &
                     in%patch%s(k)%tau0, &
                     in%patch%s(k)%mu0, &
                     in%patch%s(k)%sig, &
                     in%patch%s(k)%a, &
                     in%patch%s(k)%b, &
                     in%patch%s(k)%L, &
                     in%patch%s(k)%Vo, &
                     in%patch%s(k)%V2, &
                     in%patch%s(k)%damping

                IF (i .NE. k) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("invalid friction property definition for patch")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   WRITE (STDERR,'("error in input file: unexpected index")')
                   STOP 1
                END IF

                IF (0 .GE. in%patch%s(k)%L) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("error: invalid characteristic weakening distance for patch")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   STOP 1
                END IF
                IF (0 .GE. in%patch%s(k)%a) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("error: invalid velocity dependence for patch")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   STOP 1
                END IF
                IF (0 .GE. in%patch%s(k)%Vo) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("error: invalid reference velocity for patch")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   STOP 1
                END IF
   
             END DO

          CASE DEFAULT
             WRITE (0,'("unhandled option ", a, " (this is a bug")') frictionLawType
             WRITE_DEBUG_INFO(100)
             STOP 3
          END SELECT

       END IF
          
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !       O B S E R V A T I O N   P A T C H E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of observation patches")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%nObservationState
       PRINT '(I5)', in%nObservationState
       IF (0 .LT. in%nObservationState) THEN
          ALLOCATE(in%observationState(3,in%nObservationState),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the observation patches"
          PRINT 2000
          PRINT '("#   n      i rate")'
          PRINT 2000
          DO k=1,in%nObservationState
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i,in%observationState(1:2,k)

             PRINT '(I5,X,I6,X,I4)', i, in%observationState(1:2,k)

             IF (0 .GE. in%observationState(2,k)) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: invalid subsampling rate")')
                STOP 1
             END IF

             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO
       END IF

       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !        O B S E R V A T I O N   P O I N T S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of observation points")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%nObservationPoint
       PRINT '(I5)', in%nObservationPoint
       IF (0 .LT. in%nObservationPoint) THEN
          ALLOCATE(in%observationPoint(in%nObservationPoint),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the observation points"
          PRINT 2000
          PRINT '("# n name       x2       x3")'
          PRINT 2000
          DO k=1,in%nObservationPoint
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                     in%observationPoint(k)%name, &
                     in%observationPoint(k)%x(2), &
                     in%observationPoint(k)%x(3)

             PRINT '(I3.3,X,a4,2ES9.2E1)',i, &
                     in%observationPoint(k)%name, &
                     in%observationPoint(k)%x(2), &
                     in%observationPoint(k)%x(3)

             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO

          ! export list of observation points
          filename=TRIM(in%wdir)//"/opts.dat"
          OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          WRITE (FPOUT,'("# n name        x1        x2        x3")')
          DO k=1,in%nObservationPoint
             WRITE (FPOUT,'(I3.3,X,a4,3ES10.2E1)') k, &
                     in%observationPoint(k)%name, &
                     in%observationPoint(k)%x(1), &
                     in%observationPoint(k)%x(2), &
                     in%observationPoint(k)%x(3)
          END DO
          CLOSE(FPOUT)

       END IF

       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !                  E V E N T S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT '("# number of events")'
       CALL getdata(iunit,dataline)
       READ (dataline,*) in%ne
       PRINT '(I5)', in%ne
       IF (in%ne .GT. 0) ALLOCATE(in%event(in%ne),STAT=ierr)
       IF (ierr>0) STOP "could not allocate the event list"
       
       DO i=1,in%ne
          IF (1 .NE. i) THEN
             PRINT '("# time of next event")'
             CALL getdata(iunit,dataline)
             READ (dataline,*) in%event(i)%time
             in%event(i)%i=i-1
             PRINT '(ES9.2E1)', in%event(i)%time
   
             IF (in%event(i)%time .LE. in%event(i-1)%time) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'(a,a)') "input file error. ", &
                     "timing of perturbations must increase, quiting."
                STOP 1
             END IF
          ELSE
             in%event(1)%time=0._8
             in%event(1)%i=0
          END IF
   
       END DO
   
       ! test the presence of dislocations for coseismic calculation
       IF ((in%patch%ns .EQ. 0) .AND. &
           (in%interval .LE. 0._8)) THEN
   
          WRITE_DEBUG_INFO(300)
          WRITE (STDERR,'("nothing to do. exiting.")')
          STOP 1
       END IF
   
       PRINT 2000
       ! flush standard output
       CALL FLUSH(6)      

       in%nPatch=in%patch%ns

       position=0
       CALL MPI_PACK(in%interval,           1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%lambda,             1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%mu,                 1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nu,                 1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%patch%ns,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%ne,                 1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nObservationState,  1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nObservationPoint,  1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)

       position=0
       CALL MPI_PACK(in%wdir,256,MPI_CHARACTER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)

       ! send the rectangular patches (geometry and friction properties) 
       DO i=1,in%patch%ns
          position=0
          CALL MPI_PACK(in%patch%s(i)%Vl,     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%x(1,i),      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%x(2,i),      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%x(3,i),      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%width(i),    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%dip(i),      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%rake,   1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%beta,   1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%opening,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%tau0,   1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%mu0,    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%sig,    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%a,      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%b,      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%L,      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%Vo,     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%V2,     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%damping,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the observation state
       DO i=1,in%nObservationState
          position=0
          CALL MPI_PACK(in%observationState(:,i),3,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the observation points
       DO i=1,in%nObservationPoint
          position=0
          CALL MPI_PACK(in%observationPoint(i)%name,10,MPI_CHARACTER,packed,psize,position,MPI_COMM_WORLD,ierr)
          DO k=1,3
             CALL MPI_PACK(in%observationPoint(i)%x(k),1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          END DO
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the perturbation events
       DO k=1,in%ne
          CALL MPI_PACK(in%event(k)%time,1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%event(k)%i,   1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

    ELSE ! if 0.NE.rank

       !------------------------------------------------------------------
       ! S L A V E S
       !------------------------------------------------------------------

       position=0
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%interval,           1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%lambda,             1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%mu,                 1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nu,                 1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%patch%ns,           1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%ne,                 1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nObservationState,  1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nObservationPoint,  1,MPI_INTEGER,MPI_COMM_WORLD,ierr)

       position=0
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%wdir,256,MPI_CHARACTER,MPI_COMM_WORLD,ierr)

       IF (0 .LT. in%patch%ns) &
                    ALLOCATE(in%patch%s(in%patch%ns), &
                             in%patch%x(3,in%patch%ns), &
                             in%patch%xc(3,in%patch%ns), &
                             in%patch%width(in%patch%ns), &
                             in%patch%dip(in%patch%ns), &
                             in%patch%beta(in%patch%ns), &
                             in%patch%sv(3,in%patch%ns), &
                             in%patch%dv(3,in%patch%ns), &
                             in%patch%nv(3,in%patch%ns), &
                             STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory"

       IF (0 .LT. in%ne) ALLOCATE(in%event(in%ne),STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory"

       DO i=1,in%patch%ns
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%Vl,     1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%x(1,i),      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%x(2,i),      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%x(3,i),      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%width(i),    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%dip(i),      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%rake,   1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%beta,   1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%opening,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%tau0,   1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%mu0,    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%sig,    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%a,      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%b,      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%L,      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%Vo,     1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%V2,     1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%damping,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
       END DO

       IF (0 .LT. in%nObservationState) &
                    ALLOCATE(in%observationState(3,in%nObservationState),STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for observation states"

       DO i=1,in%nObservationState
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%observationState(:,i),3,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       END DO

       IF (0 .LT. in%nObservationPoint) &
                    ALLOCATE(in%observationPoint(in%nObservationPoint), &
                             STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for observation points"

       DO i=1,in%nObservationPoint
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%observationPoint(i)%name,10,MPI_CHARACTER,MPI_COMM_WORLD,ierr)
          DO k=1,3
             CALL MPI_UNPACK(packed,psize,position,in%observationPoint(i)%x(k),1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          END DO
       END DO

       DO i=1,in%ne
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%event(i)%time,1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%event(i)%i,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       END DO

       CALL FLUSH(6)      

       in%nPatch=in%patch%ns

    END IF ! master or slaves

2000 FORMAT ("# ----------------------------------------------------------------------------")
   
  END SUBROUTINE init
   
  !-----------------------------------------------
  !> subroutine cross
  !! compute the cross product between two vectors
  !-----------------------------------------------
  SUBROUTINE cross(u,v,w)
    REAL*8, DIMENSION(3), INTENT(IN) :: u,v
    REAL*8, DIMENSION(3), INTENT(OUT) :: w

    w(1)=u(2)*v(3)-u(3)*v(2)
    w(2)=u(3)*v(1)-u(1)*v(3)
    w(3)=u(1)*v(2)-u(2)*v(1)

  END SUBROUTINE cross

  !-----------------------------------------------
  !> subroutine printhelp
  !! displays a help message with master thread.
  !-----------------------------------------------
  SUBROUTINE printhelp()

    INTEGER :: rank,size,ierr
    INCLUDE 'mpif.h'

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,size,ierr)

    IF (0.EQ.rank) THEN
       PRINT '("usage:")'
       PRINT '("")'
       PRINT '("mpirun -n 2 unicycle-2d-ratestate [-h] [--dry-run] [--help] [--epsilon 1e-6] [filename]")'
       PRINT '("")'
       PRINT '("options:")'
       PRINT '("   -h                       print this message and abort calculation")'
       PRINT '("   --dry-run                abort calculation, only output geometry")'
       PRINT '("   --help                   print this message and abort calculation")'
       PRINT '("   --version                print version number and exit")'
       PRINT '("   --epsilon                set the numerical accuracy [1E-6]")'
       PRINT '("   --friction-law           type of friction law [1]")'
       PRINT '("       1: multiplicative    form of rate-state friction (Barbot, 2019)")'
       PRINT '("       2: additive          form of rate-state friction (Ruina, 1983)")'
       PRINT '("       3: arcsinh           form of rate-state friction (Rice & Benzion, 1996)")'
       PRINT '("       4: cut-off velocity  form of rate-state friction (Okubo, 1989)")'
       PRINT '("   --export-greens wdir     export the Green''s function to file in wdir")'
       PRINT '("   --import-greens wdir     import the Green''s function from file in wdir")'
       PRINT '("   --maximum-iterations     set the maximum time step [1000000]")'
       PRINT '("   --maximum-step           set the maximum time step [none]")'
       PRINT '("")'
       PRINT '("description:")'
       PRINT '("   simulates elasto-dynamics on faults in 2.5 dimensions")'
       PRINT '("   following the radiation-damping approximation")'
       PRINT '("   using the integral method.")'
       PRINT '("")'
       PRINT '("see also: ""man unicycle""")'
       PRINT '("")'
       PRINT '("                        88                                   88")'
       PRINT '("                        """"                                   88")'
       PRINT '("                                                             88")'
       PRINT '("88       88 8b,dPPYba,  88  ,adPPYba, 8b       d8  ,adPPYba, 88  ,adPPYba,")'
       PRINT '("88       88 88P''   `""8a 88 a8""     """" `8b     d8'' a8""     """" 88 a8P_____88")'
       PRINT '("88       88 88       88 88 8b          `8b   d8''  8b         88 8PP""""""""""""""")'
       PRINT '("""8a,   ,a88 88       88 88 ""8a,   ,aa   `8b,d8''   ""8a,   ,aa 88 ""8b,   ,aa")'
       PRINT '(" ""YbbdP''''Y8 88       88 88  """"Ybbd8""''     Y88''     """"Ybbd8""'' 88  `""Ybbd8""''")'
       PRINT '("                                          d8''")'
       PRINT '("                                         d8''")'
       PRINT '("")'

       CALL FLUSH(6)
    END IF

  END SUBROUTINE printhelp

  !-----------------------------------------------
  !> subroutine printversion
  !! displays code version with master thread.
  !-----------------------------------------------
  SUBROUTINE printversion()

    INTEGER :: rank,size,ierr
    INCLUDE 'mpif.h'

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,size,ierr)

    IF (0.EQ.rank) THEN
       PRINT '("unicycle-2d-ratestate version 1.0.0, compiled on ",a)', __DATE__
       PRINT '("")'
       CALL FLUSH(6)
    END IF

  END SUBROUTINE printversion

END PROGRAM ratestate




