
OBJRS=$(SRC)/macros.f90 $(patsubst %,$(DST)/%, types.o \
      getopt_m.o exportnetcdf.o getdata.o \
      antiplane.o greens.o ode45.o ratestate-serial.o )

$(shell mkdir -p $(DST))

$(DST)/%.o:$(SRC)/%.c
	$(COMPILE.c) $^ -o $(DST)/$*.o

$(DST)/%.o: $(SRC)/%.f $(SRC)/macros.f90
	$(COMPILE.f) $(filter-out $(SRC)/macros.f90,$^) -o $(DST)/$*.o

$(DST)/%.o: $(SRC)/%.f90 $(SRC)/macros.f90
	$(COMPILE.f) $(filter-out $(SRC)/macros.f90,$^) -o $(DST)/$*.o -J $(DST)

unicycle-ap-ratestate-serial: $(filter-out $(SRC)/macros.f90,$(OBJRS))
	$(LINK.f) -o $(DST)/$@ $^ $(LIBS)

clean:
	$(RM) $(DST)/*.o $(DST)/*.mod

.NOTPARALLEL:

