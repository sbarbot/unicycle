!-----------------------------------------------------------------------
! Copyright 2017-2019 Sylvain Barbot
!
! This file is part of UNICYCLE
!
! UNICYCLE is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! UNICYCLE is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with UNICYCLE.  If not, see <http://www.gnu.org/licenses/>.
!-----------------------------------------------------------------------

#include "macros.f90"

MODULE greens

  USE antiplane

  IMPLICIT NONE

  PUBLIC

  !------------------------------------------------------------------------
  !! The Green's function for traction and stress interaction amongst 
  !! dislocations and strain volumes has the following layout
  !!
  !!       / KK  KL \
  !!   G = |        |
  !!       \ LK  LL /
  !!
  !! where
  !!
  !!   KK is the matrix for traction on faults due to fault slip
  !!   KL is the matrix for traction on faults due to strain in finite volumes
  !!
  !!   LK is the matrix for stress in volumes due to fault slip
  !!   LL is the matrix for stress in volumes due to strain in finite volumes
  !!
  !! The functions provided in this module computes the matrices KK, KL
  !! LK, and LL separately so they can be subsequently combined.
  !!
  !! The Green's function for observation points interaction amongst 
  !! dislocations and strain volumes has the following layout
  !!
  !!       /        \
  !!   O = | OK  OL |
  !!       \        /
  !!
  !! where
  !!
  !!   OK is the matrix for displacements due to fault slip
  !!   OL is the matrix for displacements due to strain in finite volumes
  !!
  !------------------------------------------------------------------------


CONTAINS

  !-----------------------------------------------------------------------
  !> subroutine buildG
  !! Builds the stress interaction matrix G following the layout
  !! illustrated below
  !!
  !!
  !!     /             \   +------------------------------------------+
  !!     |             |   |                                          |
  !!     |             |   |                      nPatch  * dPatch    |
  !! G = |             |   | ( varying size ) * (         +         ) |
  !!     |             |   |                      nVolume * dVolume   |
  !!     |             |   |                                          |
  !!     \             /   +------------------------------------------+
  !!
  !! \author Sylvain Barbot (03/08/2017)
  !----------------------------------------------------------------------
  SUBROUTINE buildG(in,G)
    USE types
    IMPLICIT NONE
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in
    REAL*8, DIMENSION(:,:), ALLOCATABLE, INTENT(OUT) :: G

    INTEGER :: ierr
    INTEGER :: i,k,n,m

    ! traction kernels
    REAL*8, DIMENSION(:,:,:), ALLOCATABLE :: KK,KL

    ! stress kernels
    REAL*8, DIMENSION(:,:,:), ALLOCATABLE :: LK,LL
  
    ! identity matrix
    REAL*8, DIMENSION(2,2), PARAMETER :: &
               eye=RESHAPE( (/ 1._8,0._8, &
                               0._8,1._8 /), (/ 2,2 /))
  
    ! number of columns
    n=in%patch%ns*DGF_PATCH+ &
      in%strainVolume%ns*DGF_VOLUME

    ! number of rows in current thread
    m=in%patch%ns*DGF_VECTOR+ &
      in%strainVolume%ns*DGF_TENSOR

    ALLOCATE(G(n,m),STAT=ierr)

    ! two types of traction sources
    ALLOCATE(KK(DGF_PATCH,in%patch%ns,DGF_VECTOR), &
             KL(DGF_VOLUME,in%strainVolume%ns,DGF_VECTOR),STAT=ierr)
    IF (0 /= ierr) STOP "could not allocate the traction kernels"

    ! two types of stress sources
    ALLOCATE(LK(DGF_PATCH,in%patch%ns,DGF_TENSOR), &
             LL(DGF_VOLUME,in%strainVolume%ns,DGF_TENSOR),STAT=ierr)
    IF (0 /= ierr) STOP "could not allocate the kernels"
      
    ! loop over elements owned by current thread
    k=1
    DO i=1,in%patch%ns
       ! G(:,k:k+DGF_VECTOR-1) = Green's function
       CALL tractionRows(in%patch%xc(:,i), &
                      in%patch%sv(:,i), &
                      in%patch%dv(:,i), &
                      in%patch%nv(:,i), &
                      DGF_VECTOR,n,G(:,k))

       k=k+DGF_VECTOR
    END DO

    DO i=1,in%strainVolume%ns
       ! G(:,k:k+DGF_TENSOR-1)= Green's function
       CALL stressRows(in%strainVolume%xc(:,i), &
                      in%strainVolume%sv(:,i), &
                      in%strainVolume%dv(:,i), &
                      in%strainVolume%nv(:,i), &
                      DGF_TENSOR,n,G(:,k))

       k=k+DGF_TENSOR
    END DO

    DEALLOCATE(KK,KL)
    DEALLOCATE(LK,LL)
      
  CONTAINS

    !-----------------------------------------------------------------------
    !> subroutine tractionRows
    !! evaluates all the relevant rows for traction change due to strike slip
    !! in patches and strain (12 and 13) in strain volumes.
    !!
    !! \author Sylvain Barbot (03/08/2017)
    !----------------------------------------------------------------------
    SUBROUTINE tractionRows(xc,sv,dv,nv,m,n,rows)
      IMPLICIT NONE
      REAL*8, DIMENSION(3), INTENT(IN) :: xc,sv,dv,nv
      INTEGER, INTENT(IN) :: m,n
      REAL*8, DIMENSION(n*m), INTENT(OUT) :: rows
  
      INTEGER :: j
  
      ! patches
      DO j=1,DGF_PATCH
         CALL computeTractionKernelsAntiplane(xc,sv,dv,nv, &
                 in%patch%ns,in%patch%x, &
                 in%patch%width, &
                 in%patch%dip, &
                 in%mu,KK(j,:,1))
      END DO
  
      ! strain volumes
      IF (0 .LT. in%strainVolume%ns) THEN
         DO j=1,DGF_VOLUME
            CALL computeTractionKernelsStrainVolumeAntiplane(xc,sv,dv,nv, &
                    in%strainVolume%ns,in%strainVolume%x, &
                    in%strainVolume%thickness,in%strainVolume%width, &
                    in%strainVolume%dip, &
                    eye(j,1),eye(j,2),in%mu,KL(j,:,1))
         END DO
      END IF
  
      ! concatenate kernels
      rows=(/ KK(:,:,1),KL(:,:,1) /)
  
    END SUBROUTINE tractionRows
  
    !-----------------------------------------------------------------------
    !> subroutine stressRows
    !! evaluates all the relevant rows for stress change due to strike slip
    !! in patches and strain (12 and 13) in finite volumes.
    !!
    !! \author Sylvain Barbot (06/09/2017)
    !----------------------------------------------------------------------
    SUBROUTINE stressRows(xc,sv,dv,nv,m,n,rows)
      IMPLICIT NONE
      REAL*8, DIMENSION(3), INTENT(IN) :: xc,sv,dv,nv
      INTEGER, INTENT(IN) :: m,n
      REAL*8, DIMENSION(n*m), INTENT(OUT) :: rows
  
      INTEGER :: j
  
      ! patches
      DO j=1,DGF_PATCH
         CALL computeStressKernelsAntiplane(xc,sv,dv,nv, &
                 in%patch%ns,in%patch%x, &
                 in%patch%width, &
                 in%patch%dip,in%mu,LK(j,:,1),LK(j,:,2))
      END DO
  
      ! strain volumes
      DO j=1,DGF_VOLUME
         CALL computeStressKernelsStrainVolumeAntiplane(xc,sv,dv,nv, &
                 in%strainVolume%ns,in%strainVolume%x, &
                 in%strainVolume%thickness,in%strainVolume%width, &
                 in%strainVolume%dip, &
                 eye(j,1),eye(j,2),in%mu,LL(j,:,1),LL(j,:,2))
      END DO
  
      ! concatenate kernels
      rows=(/ LK(:,:,1),LL(:,:,1), &
              LK(:,:,2),LL(:,:,2) /)
  
    END SUBROUTINE stressRows
  
  END SUBROUTINE buildG
  
  !-----------------------------------------------------------------------
  !> subroutine buildO
  !! Builds the displacement matrix O following the layout below
  !!
  !!
  !!     /             \   +----------------------------------------+
  !!     |             |   |                                        |
  !!     |             |   |   nObservation                         |
  !! O = |             |   | (      *       )  * (  varying size  ) |
  !!     |             |   |   DISPLACEMENT                         |
  !!     |             |   |   _VECTOR_DGF                          |
  !!     |             |   |                                        |
  !!     \             /   +----------------------------------------+
  !!
  !! \author Sylvain Barbot (06/28/2017)
  !----------------------------------------------------------------------
  SUBROUTINE buildO(in,O,Of,Ol)
    USE types

    IMPLICIT NONE

    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in
    REAL*8, DIMENSION(:,:), ALLOCATABLE, INTENT(OUT) :: O,Of,Ol

    INTEGER :: ierr
    INTEGER :: i,j,k,l,n,m,p

    TYPE(PATCH_ELEMENT_STRUCT) :: patch
    TYPE(STRAINVOLUME_ELEMENT_STRUCT) :: volume

    ! identity matrix
    REAL*8, DIMENSION(2,2), PARAMETER :: &
               eye=RESHAPE( (/ 1._8,0._8, &
                               0._8,1._8 /), (/ 2,2 /))
  
    ! number of columns
    n=in%patch%ns*DGF_PATCH+ &
      in%strainVolume%ns*DGF_VOLUME

    ! number of rows
    m=in%nObservationPoint*DISPLACEMENT_VECTOR_DGF

    ALLOCATE(O(n,m),Of(n,m),Ol(n,m),STAT=ierr)
    IF (0 /= ierr) STOP "could not allocate the displacement kernels"

    ! zero out partial contribution matrices
    Of=0._8
    Ol=0._8

    ! initiate counter for displacement vector
    p=1
    ! loop over observation points
    DO k=1,in%nObservationPoint

       ! initiate counter for source kinematics
       l=1

       ! loop over elements owned by current thread
       DO i=1,in%patch%ns

          patch=in%patch%s(i)

          ! fault patches
          DO j=1,DGF_PATCH
             CALL computeDisplacementAntiplane( &
                     in%observationPoint(k)%x(2), &
                     in%observationPoint(k)%x(3), &
                     in%patch%x(2,i), &
                     in%patch%x(3,i), &
                     in%patch%width(i), &
                     in%patch%dip(i), &
                     1._8,O(l+j-1,p))

             ! contribution from faulting alone
             Of(l+j-1,p)=O(l+j-1,p)
          END DO

          l=l+DGF_PATCH

       END DO ! elements owned by current thread

       p=p+DISPLACEMENT_VECTOR_DGF

    END DO

    DO k=1,in%nObservationPoint

       ! initiate counter for source kinematics
       l=1

       ! loop over volume elements
       DO i=1,in%strainVolume%ns

          volume=in%strainVolume%s(i)

          ! strain volumes
          DO j=1,DGF_VOLUME
             CALL computeDisplacementStrainVolumeAntiplane( &
                     in%observationPoint(k)%x(2), &
                     in%observationPoint(k)%x(3), &
                     in%strainVolume%x(2,i), &
                     in%strainVolume%x(3,i), &
                     in%strainVolume%thickness(i), &
                     in%strainVolume%width(i), &
                     in%strainVolume%dip(i), &
                     eye(j,1),eye(j,2),O(l+j-1,p))

             ! contribution from flow alone
             Ol(l+j-1,p)=O(l+j-1,p)
          END DO

          l=l+DGF_VOLUME

       END DO

       p=p+DISPLACEMENT_VECTOR_DGF

    END DO
  
  END SUBROUTINE buildO

END MODULE greens


