!-----------------------------------------------------------------------
!> program RateState_serial simulates evolution of fault slip in condition of 
!! antiplane strain under the radiation damping approximation.
!!
!! \mainpage
!! 
!! The time evolution is evaluated numerically using the 4th/5th order
!! Runge-Kutta method with adaptive time steps. The state vector is 
!! as follows:
!!
!!    / P1 1       \   +-----------------------+
!!    | .          |   |                       |
!!    | P1 dPatch  |   |                       |
!!    | .          |   |    nPatch * dPatch    |
!!    | .          |   |                       |
!!    | Pn 1       |   |                       |
!!    | .          |   |                       |
!!    \ Pn dPatch  /   +-----------------------+
!!
!! where nPatch is the number of patches and dPatch is the degrees of 
!! freedom per patch. 
!!
!! For every patch, we have the following items in the state vector
!!
!!   /   ts    \  1
!!   |   ss    |  .
!!   !  theta* |  .
!!   \   v*    /  dPatch
!!
!! where ts is the local traction in the strike direction, ss is the slip 
!! in the strike direction, v* is the logarithm of the norm of the 
!! velocity vector (v*=log10(V)), and theta* is the logarithm of the state 
!! variable (theta*=log10(theta)) in the rate and state friction framework.
!!
!! \author Sylvain Barbot (2017).
!----------------------------------------------------------------------
PROGRAM ratestate_serial

#include "macros.f90"

#ifdef NETCDF
  USE exportnetcdf
#endif
  USE antiplane
  USE greens
  USE ode45
  USE types 

  IMPLICIT NONE

  REAL*8, PARAMETER :: DEG2RAD = 0.01745329251994329547437168059786927_8

  ! error flag
  INTEGER :: ierr

#ifdef NETCDF
  ! netcdf file and variable
  INTEGER :: ncid,y_varid,z_varid,ncCount
#endif

  CHARACTER(256) :: filename

  ! maximum velocity
  REAL*8 :: vMax

  ! moment rate
  REAL*8 :: momentRate

  ! scaling factor
  REAL*8, PARAMETER :: lg10=LOG(1.d1)

  ! state vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: y
  ! rate of change of state vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: dydt,yscal
  ! temporary variables
  REAL*8, DIMENSION(:), ALLOCATABLE :: ytmp,ytmp1,ytmp2,ytmp3

  ! slip velocity vector and strain rate tensor array
  REAL*8, DIMENSION(:), ALLOCATABLE :: v

  ! traction vector and stress tensor array
  REAL*8, DIMENSION(:), ALLOCATABLE :: t

  ! displacement and kinematics vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: d,u

  ! Green's functions
  REAL*8, DIMENSION(:,:), ALLOCATABLE :: G,O,Of,Ol

  ! time
  REAL*8 :: time,t0
  ! time step
  REAL*8 :: dt_try,dt_next,dt_done
  ! time steps
  INTEGER :: i,j

  ! type of friction law (default)
  INTEGER :: frictionLawType=1

  ! maximum number of time steps (default)
  INTEGER :: maximumIterations=1000000

  ! layout for parallelism
  TYPE(LAYOUT_STRUCT) :: layout

  ! model parameters
  TYPE(SIMULATION_STRUCT) :: in

  ! start time
  time=0.d0

  ! initial tentative time step
  dt_next=1.0d-3

  ! retrieve input parameters from command line
  CALL init(in)
  CALL FLUSH(STDOUT)

  IF (in%isdryrun) THEN
     PRINT '("dry run: abort calculation")'
  END IF
  IF (in%isdryrun .OR. in%isversion .OR. in%ishelp) THEN
     STOP
  END IF

  ! calculate basis vectors
  CALL initGeometry(in)

  ! calculate the stress interaction matrix
  PRINT '("# computing Green''s functions.")'

  CALL buildG(in,G)
  CALL buildO(in,O,Of,Ol)

#ifdef __CUDA__
  CALL cu_init(SIZE(G,1),SIZE(G,2),G)
#endif

#ifdef NETCDF
  IF (in%isexportgreens) THEN
     CALL exportGreensNetcdf(G)
  END IF
#endif

  DEALLOCATE(Of,Ol)
  PRINT 2000

  ! velocity vector and strain rate tensor array (t=G*v)
  ALLOCATE(u(in%patch%ns*DGF_PATCH+in%strainVolume%ns*DGF_VOLUME), &
           v(in%patch%ns*DGF_PATCH+in%strainVolume%ns*DGF_VOLUME),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the velocity and strain rate vector"

  ! traction vector and stress tensor array (t=G*v)
  ALLOCATE(t(in%patch%ns*DGF_VECTOR+in%strainVolume%ns*DGF_TENSOR),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the traction and stress vector"

  ! displacement vector (d=O*v)
  ALLOCATE(d(in%nObservationPoint*DISPLACEMENT_VECTOR_DGF),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the displacement vector"

  ALLOCATE(y(in%patch%ns*STATE_VECTOR_DGF_PATCH+in%strainVolume%ns*STATE_VECTOR_DGF_VOLUME),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the state vector"

  ALLOCATE(dydt(SIZE(y)), &
           yscal(SIZE(y)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the state vectors"

  ALLOCATE(ytmp (SIZE(y)), &
           ytmp1(SIZE(y)), &
           ytmp2(SIZE(y)), &
           ytmp3(SIZE(y)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the RKQS work space"

  ! allocate buffer from ode45 module
  ALLOCATE(AK2(SIZE(y)), &
           AK3(SIZE(y)), &
           AK4(SIZE(y)), &
           AK5(SIZE(y)), &
           AK6(SIZE(y)), &
           yrkck(SIZE(y)), STAT=ierr)
  IF (ierr>0) STOP "could not allocate the AK1-6 work space"

  OPEN (UNIT=FPTIME,FILE=in%timeFilename,IOSTAT=ierr,FORM="FORMATTED")
  IF (ierr>0) THEN
     WRITE_DEBUG_INFO(102)
     WRITE (STDERR,'("error: unable to access ",a)') TRIM(in%timefilename)
     STOP 1
  END IF

  ! initialize the y vector
  CALL initStateVector(SIZE(y),y,in)

#ifdef NETCDF
  ! initialize netcdf output
  IF (in%isexportnetcdf) THEN
     CALL initnc()
  END IF
#endif

  ! initialize output
  WRITE(STDOUT,'("# export to ",a)') TRIM(in%wdir)
  PRINT 2000
  WRITE(STDOUT,'("#       n               time                 dt       vMax")')
  WRITE(STDOUT,'(I9.9,ES19.12E2,ES19.12E2,ES11.4E2)') 0,time,dt_next,vMax
  WRITE(FPTIME,'("#               time                 dt               vMax         Moment-rate")')

  ! initialize observation patch
  DO j=1,in%nObservationState
     in%observationState(3,j)=100+j
     WRITE (filename,'(a,"/patch-",I8.8,"-",I8.8,".dat")') TRIM(in%wdir),j,in%observationState(1,j)
     OPEN (UNIT=in%observationState(3,j), &
           FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
     IF (ierr>0) THEN
        WRITE_DEBUG_INFO(102)
        WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
        STOP 1
     END IF
  END DO

  ! initialize observation points
  DO j=1,in%nObservationPoint
     in%observationPoint(j)%file=1000+j
     WRITE (filename,'(a,"/opts-",a,".dat")') TRIM(in%wdir),TRIM(in%observationPoint(j)%name)
     OPEN (UNIT=in%observationPoint(j)%file, &
           FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
     IF (ierr>0) THEN
        WRITE_DEBUG_INFO(102)
        WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
        STOP 1
     END IF
  END DO

  ! main loop
  DO i=1,maximumIterations

     CALL odefun(SIZE(y),time,y,dydt)

     CALL export()
     CALL exportPoints()

#ifdef NETCDF
     IF (in%isexportnetcdf) THEN
        IF (0 .EQ. MOD(i,20)) THEN
           CALL exportnc(in%patch%ns)
        END IF
     END IF
#endif

     dt_try=dt_next
     yscal(:)=abs(y(:))+abs(dt_try*dydt(:))+TINY

     t0=0.d0
     CALL RKQSm(SIZE(y),t0,y,dydt, &
               yscal,ytmp1,ytmp2,ytmp3,dt_try,dt_done,dt_next,odefun)

     time=time+dt_done

     ! end calculation
     IF (in%interval .LE. time) THEN
        EXIT
     END IF
   
  END DO

  PRINT '(I9.9," time steps.")', i
  CLOSE(FPTIME)

  ! close observation patch files
  DO j=1,in%nObservationState
     CLOSE(in%observationState(3,j))
  END DO

  ! close the observation points
  DO j=1,in%nObservationPoint
     CLOSE(in%observationPoint(j)%file)
  END DO

#ifdef NETCDF
  ! close the netcdf file
  IF (in%isexportnetcdf) THEN
     CALL closeNetcdfUnlimited(ncid,y_varid,z_varid,ncCount)
  END IF
#endif

  DEALLOCATE(in%patch%s,in%patch%x,in%patch%xc,in%patch%width,in%patch%dip,in%patch%sv,in%patch%dv,in%patch%nv)
  IF (ALLOCATED(in%observationState)) DEALLOCATE(in%observationState)
  IF (ALLOCATED(in%observationPoint)) DEALLOCATE(in%observationPoint)
  DEALLOCATE(y,dydt,yscal)
  DEALLOCATE(ytmp,ytmp1,ytmp2,ytmp3,yrkck)
  DEALLOCATE(AK2,AK3,AK4,AK5,AK6)
  DEALLOCATE(G,v,t)
  DEALLOCATE(O,d,u)

#ifdef __CUDA__
  CALL cu_close()
#endif

2000 FORMAT ("# ----------------------------------------------------------------------------")
     
CONTAINS

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportGreensNetcdf
  ! initializes the coordinates of netcdf files
  !----------------------------------------------------------------------
  SUBROUTINE exportGreensNetcdf(M)
    REAL*8, DIMENSION(:,:), INTENT(IN) :: M

    REAL*8, DIMENSION(:), ALLOCATABLE :: x,y

    INTEGER :: i,ierr
    CHARACTER(LEN=256) :: filename

    ALLOCATE(x(SIZE(M,1)),y(SIZE(M,2)),STAT=ierr)
    IF (ierr/=0) STOP "could not allocate netcdf coordinate for Greens function"

    ! loop over all patch elements
    DO i=1,SIZE(M,1)
       x(i)=REAL(i,8)
    END DO
    DO i=1,SIZE(M,2)
       y(i)=REAL(i,8)
    END DO

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/greens.grd")') TRIM(in%greensFunctionDirectory)
    CALL writeNetcdf(filename,SIZE(M,1),x,SIZE(M,2),y,M,1)

    DEALLOCATE(x,y)

  END SUBROUTINE exportGreensNetcdf
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine initnc
  ! initializes the coordinates of netcdf files
  !----------------------------------------------------------------------
  SUBROUTINE initnc()

    REAL*8, DIMENSION(:), ALLOCATABLE :: x

    INTEGER :: i,ierr
    CHARACTER(LEN=256) :: filename

    ! initialize the number of exports
    ncCount = 0

    ALLOCATE(x(in%patch%ns),STAT=ierr)
    IF (ierr/=0) STOP "could not allocate netcdf coordinate"

    ! loop over all patch elements
    DO i=1,in%patch%ns
       x(i)=REAL(i,8)
    END DO

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/log10v.grd")') TRIM(in%wdir)
    CALL openNetcdfUnlimited(filename,in%patch%ns,x,ncid,y_varid,z_varid)

    DEALLOCATE(x)


  END SUBROUTINE initnc
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportnc
  ! export time series of log10(v)
  !----------------------------------------------------------------------
  SUBROUTINE exportnc(n)
    INTEGER, INTENT(IN) :: n

    REAL*4, DIMENSION(n) :: z

    INTEGER :: j

    ! update the export count
    ncCount=ncCount+1

    ! loop over all patch elements
    DO j=1,in%patch%ns
       ! dip slip
       z(j)=REAL(LOG10(in%patch%s(j)%Vl+v(DGF_PATCH*(j-1)+1)),4)
    END DO

    CALL writeNetcdfUnlimited(ncid,y_varid,z_varid,ncCount,n,z)

  END SUBROUTINE exportnc
#endif

  !-----------------------------------------------------------------------
  !> subroutine exportPoints
  ! export observation points
  !----------------------------------------------------------------------
  SUBROUTINE exportPoints()

    IMPLICIT NONE

    INTEGER :: j,k,l,ierr
    CHARACTER(1024) :: formatString
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    !-----------------------------------------------------------------
    ! step 1/4 - gather the kinematics from the state vector
    !-----------------------------------------------------------------

    ! element index in d vector
    k=1
    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO j=1,in%patch%ns
       patch=in%patch%s(j)

       ! strike slip
       u(k)=y(l+STATE_VECTOR_SLIP_STRIKE)

       l=l+in%dPatch

       k=k+DGF_VECTOR
    END DO

    !-----------------------------------------------------------------
    ! step 2/4 - calculate the rate of traction and rate of stress
    !-----------------------------------------------------------------

#ifdef __BLAS__
    ! use the BLAS library to compute the matrix vector product
    CALL DGEMV("T",SIZE(O,1),SIZE(O,2), &
                1._8,O,SIZE(O,1),u,1,0.d0,d,1)
#else
    ! slower, Fortran intrinsic
    d=MATMUL(TRANSPOSE(O),u)
#endif

    !-----------------------------------------------------------------
    ! step 4/4 - master thread writes to disk
    !-----------------------------------------------------------------

    formatString="(ES19.12E2"
    DO j=1,DISPLACEMENT_VECTOR_DGF
       formatString=TRIM(formatString)//",X,ES19.12E2"
    END DO
    formatString=TRIM(formatString)//")"

    ! element index in d vector
    k=1
    DO j=1,in%nObservationPoint
       IF (0 .EQ. MOD(i-1,in%observationPoint(j)%rate)) THEN
          WRITE (in%observationPoint(j)%file,TRIM(formatString)) time,d(k:k+DISPLACEMENT_VECTOR_DGF-1)
       END IF
       k=k+DISPLACEMENT_VECTOR_DGF
    END DO

  END SUBROUTINE exportPoints

  !-----------------------------------------------------------------------
  !> subroutine export
  ! write the state variables of elements, either patch or volume, and 
  ! other information.
  !----------------------------------------------------------------------
  SUBROUTINE export()

    IMPLICIT NONE

    ! degrees of freedom
    INTEGER :: dgf

    ! counters
    INTEGER :: j,k

    ! index in state vector
    INTEGER :: index

    ! format string
    CHARACTER(1024) :: formatString

    ! export observation state
    DO j=1,in%nObservationState

       ! check observation state sampling rate
       IF (0 .EQ. MOD(i-1,in%observationState(2,j))) THEN
          dgf=STATE_VECTOR_DGF_PATCH

          formatString="(ES21.14E2"
          DO k=1,dgf
             formatString=TRIM(formatString)//",X,ES20.12E3,X,ES20.12E3"
          END DO
          formatString=TRIM(formatString)//")"

          index=(in%observationState(1,j)-1)*dgf+1
          WRITE (in%observationState(3,j),TRIM(formatString)) time, &
                    y(index:index+dgf-1), &
                 dydt(index:index+dgf-1)
       END IF
    END DO

    WRITE(FPTIME,'(ES20.14E2,ES19.12E2,ES19.12E2,ES20.12E2)') time,dt_done,vMax,momentRate
    IF (0 .EQ. MOD(i,50)) THEN
       WRITE(STDOUT,'(I9.9,ES19.12E2,ES19.12E2,ES11.4E2)') i,time,dt_done,vMax
       CALL FLUSH(STDOUT)
    END IF

  END SUBROUTINE export

  !-----------------------------------------------------------------------
  !> subroutine initStateVector
  ! initialize the state vector
  !
  ! INPUT:
  ! @param n - number of state elements own by current thread
  ! @param y - the state vector (segment owned by currect thread)
  !
  !----------------------------------------------------------------------
  SUBROUTINE initStateVector(n,y,in)

    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: n
    REAL*8, INTENT(OUT)    :: y(n)
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in

    INTEGER :: i,l
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    ! initial traction (Pa)
    REAL*8 :: tau

    ! initial state variable (s)
    REAL*8 :: Tinit

    ! initial velocity (m/s)
    REAL*8 :: Vinit

    ! zero out state vector
    y=0._8

    ! maximum velocity
    vMax=0._8

    ! moment-rate
    momentRate=0._8

    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,in%patch%ns

       patch=in%patch%s(i)

       ! traction in strike direction
       IF (0 .GT. patch%tau0) THEN

          ! set initial velocity to local loading rate
          Vinit = 0.98d0*patch%Vl

          ! set traction compatible with velocity
          SELECT CASE(frictionLawType)
          CASE(1)
             ! multiplicative form of rate-state friction (Barbot, 2019)
             tau = patch%mu0*patch%sig*exp((patch%a-patch%b)/patch%mu0*log(Vinit/patch%Vo))
          CASE(2)
             ! additive form of rate-state friction (Ruina, 1983)
             tau = patch%sig*(patch%mu0+(patch%a-patch%b)*log(Vinit/patch%Vo))
          CASE(3)
             ! arcsinh form of rate-state friction (Rice & Benzion, 1996)
             tau = patch%a*patch%sig*ASINH(Vinit/2/patch%Vo*exp((patch%mu0+patch%b*log(patch%Vo/Vinit))/patch%a))
          CASE(4)
             ! rate-state friction with cut-off velocity (Okubo, 1989)
             tau = patch%sig*(patch%mu0+(patch%a-patch%b)*log(Vinit/patch%Vo))
          CASE DEFAULT
             WRITE (0,'("unhandled option ", a, " (this is a bug")') frictionLawType
             WRITE_DEBUG_INFO(100)
             STOP 3
          END SELECT
       ELSE
          ! set traction from input file
          tau = patch%tau0

          ! set initial velocity to local loading rate
          Vinit = patch%Vl
       END IF

       ! set state variable log10(theta) compatible with velocity and traction
       SELECT CASE(frictionLawType)
       CASE(1)
          ! multiplicative form of rate-state friction (Barbot, 2019)
          Tinit=(LOG(patch%L/patch%Vo)+(patch%a*LOG(patch%Vo/patch%Vl)+patch%mu0*LOG(tau/patch%mu0/patch%sig))/patch%b)/lg10
       CASE(2)
          ! additive form of rate-state friction (Ruina, 1983)
          Tinit=(LOG(patch%L/patch%Vo)+(patch%a*LOG(patch%Vo/patch%Vl)+tau/patch%sig-patch%mu0)/patch%b)/lg10
       CASE(3)
          ! arcsinh form of rate-state friction (Rice & Benzion, 1996)
          Tinit=(LOG(patch%L/patch%Vo) &
                 +patch%a/patch%b*LOG(2*patch%Vo/patch%Vl*SINH(tau/patch%a/patch%sig)) &
                 -patch%mu0/patch%b)/lg10
       CASE(4)
          ! rate-state friction with cut-off velocity (Okubo, 1989)
          Tinit=(LOG(patch%L/patch%V2)+(patch%a*LOG(patch%V2/patch%Vl)+tau/patch%sig-patch%mu0)/patch%b)/lg10
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug")') frictionLawType
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT

       ! strike slip
       y(l+STATE_VECTOR_SLIP_STRIKE) = 0._8

       ! initial traction
       y(l+STATE_VECTOR_TRACTION_STRIKE) = tau

       ! log10(state variable)
       y(l+STATE_VECTOR_STATE_1) = Tinit

       ! maximum velocity
       vMax=MAX(Vinit,vMax)

       ! moment-rate
       momentRate=momentRate+(Vinit-patch%Vl)*in%mu*in%patch%width(i)

       ! slip velocity log10(V)
       y(l+STATE_VECTOR_VELOCITY) = log(Vinit)/lg10

       l=l+in%dPatch
    END DO

  END SUBROUTINE initStateVector

  !-----------------------------------------------------------------------
  !> subroutine odefun
  ! evalutes the derivative of the state vector
  !
  ! @param n - number of state elements own by current thread
  ! @param m - degrees of freedom
  !
  ! DESCRIPTION:
  !   1- extract slip velocity and strain rate from state vector
  !   2- calculate the rate of traction and rate of stress
  !   3- calculate the rate of remaining state variables
  !----------------------------------------------------------------------
  SUBROUTINE odefun(n,time,y,dydt)

    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: n
    REAL*8, INTENT(IN)    :: time
    REAL*8, INTENT(IN)    :: y(n)
    REAL*8, INTENT(INOUT) :: dydt(n)

    INTEGER :: i,j,k,l,ierr
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    ! scalar rate of shear traction
    REAL*8 :: dtau

    ! slip velocity in the strike direction
    REAL*8 :: velocity

    ! modifier for the arcsinh form of rate-state friction
    REAL*8 :: reg

    ! zero out rate of state
    dydt=0._8

    ! maximum velocity
    vMax=0._8

    ! initialize moment-rate
    momentRate=0._8

    !--------------------------------------------------------------------
    ! step 1/3 - extract slip velocity and strain rate from state vector
    !--------------------------------------------------------------------

    ! element index in state vector
    l=1
    ! element index in v vector
    k=1

    ! loop over elements owned by current thread
    DO i=1,in%patch%ns

       patch=in%patch%s(i)

       ! slip velocity
       velocity=EXP(y(l+STATE_VECTOR_VELOCITY)*lg10)

       ! maximum velocity
       vMax=MAX(velocity,vMax)

       ! update state vector (rate of slip components)
       dydt(l+STATE_VECTOR_SLIP_STRIKE)=velocity

       v(k)=velocity-patch%Vl

       ! element index in state vector
       l=l+STATE_VECTOR_DGF_PATCH

       ! element index in v vector
       k=k+DGF_VECTOR
    END DO

    !-----------------------------------------------------------------
    ! step 2/3 - calculate the rate of traction and rate of stress
    !-----------------------------------------------------------------

#ifdef __CUDA__
    CALL cu_mvp(SIZE(G,1),SIZE(G,2),v,t)
#else
#ifdef __BLAS__
    ! use the BLAS library to compute the matrix vector product
    CALL DGEMV("T",SIZE(G,1),SIZE(G,2), &
                1._8,G,SIZE(G,1),v,1,0.d0,t,1)
#else
    ! slower, Fortran intrinsic
    t=MATMUL(TRANSPOSE(G),v)
#endif
#endif

    !-----------------------------------------------------------------
    ! step 3/3 - calculate the rate of remaining state variables
    !-----------------------------------------------------------------

    ! element index in t vector
    j=1
    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,in%patch%ns
       patch=in%patch%s(i)

       ! slip velocity
       velocity=EXP(y(l+STATE_VECTOR_VELOCITY)*lg10)

       ! moment-rate
       momentRate=momentRate+(velocity-patch%Vl)*in%mu*in%patch%width(i)

       ! rate of state
       dydt(l+STATE_VECTOR_STATE_1)=(EXP(-y(l+STATE_VECTOR_STATE_1)*lg10)-velocity/patch%L)/lg10

       ! scalar rate of shear traction
       dtau=t(j+TRACTION_VECTOR_STRIKE)
       
       ! acceleration
       SELECT CASE(frictionLawType)
       CASE(1)
          ! multiplicative form of rate-state friction (Barbot, 2019)
          dydt(l+STATE_VECTOR_VELOCITY)= &
                  (dtau-patch%b*y(l+STATE_VECTOR_TRACTION_STRIKE)/patch%mu0*dydt(l+STATE_VECTOR_STATE_1)*lg10) / &
                  (patch%a*y(l+STATE_VECTOR_TRACTION_STRIKE)/patch%mu0+patch%damping*velocity) / lg10
       CASE(2)
          ! additive form of rate-state friction (Ruina, 1983)
          dydt(l+STATE_VECTOR_VELOCITY)= &
                  (dtau-patch%b*patch%sig*dydt(l+STATE_VECTOR_STATE_1)*lg10) / &
                  (patch%a*patch%sig+patch%damping*velocity) / lg10
       CASE(3)
          ! arcsinh form of rate-state friction (Rice & Benzion, 1996)
          reg=2.0d0*patch%Vo/velocity*exp(-(patch%mu0+patch%b*(y(STATE_VECTOR_STATE_1)*lg10-log(patch%L/patch%Vo)))/patch%a)
          reg=1d0/SQRT(1._8+reg**2)
          dydt(l+STATE_VECTOR_VELOCITY)= &
                  (dtau-patch%b*patch%sig*dydt(l+STATE_VECTOR_STATE_1)*lg10*reg) / &
                  (patch%a*patch%sig*reg+patch%damping*velocity) / lg10
       CASE(4)
          ! rate-state friction with cut-off velocity (Okubo, 1989)
          dydt(l+STATE_VECTOR_VELOCITY)= &
                  (dtau-patch%b*patch%sig*dydt(l+STATE_VECTOR_STATE_1)*lg10*exp(y(l+STATE_VECTOR_STATE_1)*lg10) &
                                  /(patch%L/patch%V2+exp(y(l+STATE_VECTOR_STATE_1)*lg10))) / &
                  (patch%a*patch%sig*patch%Vo/(patch%Vo+velocity)+patch%damping*velocity) / lg10
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug")') frictionLawType
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT

       ! return the traction rate
       dydt(l+STATE_VECTOR_TRACTION_STRIKE)=dtau-patch%damping*velocity*dydt(l+STATE_VECTOR_VELOCITY)*lg10

       l=l+STATE_VECTOR_DGF_PATCH

       j=j+DGF_VECTOR
    END DO

  END SUBROUTINE odefun

  !-----------------------------------------------------------------------
  !> subroutine initGeometry
  ! initializes the position and local reference system vectors
  !
  ! INPUT:
  ! @param in      - input parameters data structure
  !-----------------------------------------------------------------------
  SUBROUTINE initGeometry(in)
    USE antiplane
    USE types

    IMPLICIT NONE

    TYPE(SIMULATION_STRUCT), INTENT(INOUT) :: in
  
    IF (0 .LT. in%patch%ns) THEN
       CALL computeReferenceSystemAntiplane( &
                in%patch%ns, &
                in%patch%x, &
                in%patch%width, &
                in%patch%dip, &
                in%patch%sv, &
                in%patch%dv, &
                in%patch%nv, &
                in%patch%xc)
    END IF

  END SUBROUTINE initGeometry

  !---------------------------------------------------------------------
  !> subroutine init
  !! reads simulation parameters from the standard input and initialize
  !! model parameters.
  !!
  !! INPUT:
  !! @param unit - the unit number used to read input data
  !!
  !! OUTPUT:
  !! @param in
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !---------------------------------------------------------------------
  SUBROUTINE init(in)
    USE types
    USE getopt_m
  
    IMPLICIT NONE

    TYPE(SIMULATION_STRUCT), INTENT(OUT) :: in
  
    CHARACTER :: ch
    CHARACTER(256) :: dataline,filename
    INTEGER :: iunit,noptions
!$  INTEGER :: omp_get_num_procs,omp_get_max_threads
    TYPE(OPTION_S) :: opts(9)
  
    INTEGER :: k,ierr,i,position
    INTEGER, PARAMETER :: psize=512
    INTEGER :: dummy
    CHARACTER, DIMENSION(psize) :: packed
  
    ! define long options, such as --dry-run
    ! parse the command line for options
    opts(1)=OPTION_S("version",.FALSE.,CHAR(21))
    opts(2)=OPTION_S("dry-run",.FALSE.,CHAR(22))
    opts(3)=OPTION_S("epsilon",.TRUE.,'e')
    opts(4)=OPTION_S("export-greens",.TRUE.,'g')
    opts(5)=OPTION_S("export-netcdf",.FALSE.,'n')
    opts(6)=OPTION_S("friction-law",.TRUE.,'f')
    opts(7)=OPTION_S("maximum-step",.TRUE.,'m')
    opts(8)=OPTION_S("maximum-iterations",.TRUE.,'i')
    opts(9)=OPTION_S("help",.FALSE.,'h')
  
    noptions=0
    DO
       ch=getopt("he:g:i:m:n",opts)
       SELECT CASE(ch)
       CASE(CHAR(0))
          EXIT
       CASE(CHAR(21))
          ! option version
          in%isversion=.TRUE.
       CASE(CHAR(22))
          ! option dry-run
          in%isdryrun=.TRUE.
       CASE('e')
          ! numerical accuracy (variable epsilon sits in the ode45 module)
          READ(optarg,*) epsilon
          noptions=noptions+1
       CASE('f')
          ! type of friction law
          READ(optarg,*) frictionLawType
          noptions=noptions+1
       CASE('g')
          ! export Greens functions to netcdf file
          READ(optarg,'(a)') in%greensFunctionDirectory
          in%isexportgreens=.TRUE.
          noptions=noptions+1
       CASE('i')
          ! maximum number of iterations
          READ(optarg,*) maximumIterations
          noptions=noptions+1
       CASE('m')
          ! maximum time step (variable maximumTimeStep sits in the ode45 module)
          READ(optarg,*) maximumTimeStep
          noptions=noptions+1
       CASE('n')
          ! export in netcdf
          in%isexportnetcdf=.TRUE.
       CASE('h')
          ! option help
          in%ishelp=.TRUE.
       CASE('?')
          WRITE_DEBUG_INFO(100)
          in%ishelp=.TRUE.
          EXIT
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug")') optopt
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT
       noptions=noptions+1
    END DO
  
    IF (in%isversion) THEN
       CALL printversion()
       ! abort parameter input
       STOP
    END IF
  
    IF (in%ishelp) THEN
       CALL printhelp()
       ! abort parameter input
       STOP
    END IF
  
    ! minimum number of dynamic variables for patches
    in%dPatch=4
    in%patch%ns=0
    in%strainVolume%ns=0
  
    PRINT 2000
    PRINT '("# RATESTATE")'
    PRINT '("# quasi-dynamic earthquake simulation in antiplane strain")'
    SELECT CASE(frictionLawType)
    CASE(1)
       PRINT '("# friction law: multiplicative form of rate-state friction (Barbot, 2019)")'
    CASE(2)
       PRINT '("# friction law: additive form of rate-state friction (Ruina, 1983)")'
    CASE(3)
       PRINT '("# friction law: arcsinh form of rate-state friction (Rice & BenZion, 1996)")'
    CASE(4)
       PRINT '("# friction law: rate-state friction with cut-off velocity (Okubo, 1989)")'
    CASE DEFAULT
       WRITE (0,'("unhandled option ", a, " (this is a bug")') frictionLawType
       WRITE_DEBUG_INFO(100)
       STOP 3
    END SELECT
    PRINT '("# numerical accuracy: ",ES11.4)', epsilon
    PRINT '("# maximum iterations: ",I11)', maximumIterations
    PRINT '("# maximum time step: ",ES12.4)', maximumTimeStep
    PRINT '("# serial calculation")'
    IF (in%isexportnetcdf) THEN
       PRINT '("# export velocity to netcdf:  yes")'
    ELSE
       PRINT '("# export velocity to netcdf:   no")'
    END IF
    IF (in%isexportgreens) THEN
       PRINT '("# export greens function:     yes")'
    END IF
#ifdef __CUDA__
    PRINT '("# cuda optimization:          yes")'
#endif
    PRINT 2000
  
    IF (noptions .LT. COMMAND_ARGUMENT_COUNT()) THEN
       ! read from input file
       iunit=25
       CALL GET_COMMAND_ARGUMENT(noptions+1,filename)
       OPEN (UNIT=iunit,FILE=filename,IOSTAT=ierr)
    ELSE
       ! get input parameters from standard input
       iunit=5
    END IF
  
    PRINT '("# output directory")'
    CALL getdata(iunit,dataline)
    READ (dataline,'(a)') in%wdir
    PRINT '(2X,a)', TRIM(in%wdir)
  
    in%timeFilename=TRIM(in%wdir)//"/time.dat"
  
    ! test write permissions on output directory
    OPEN (UNIT=FPTIME,FILE=in%timeFilename,POSITION="APPEND",&
            IOSTAT=ierr,FORM="FORMATTED")
    IF (ierr>0) THEN
       WRITE_DEBUG_INFO(102)
       WRITE (STDERR,'("error: unable to access ",a)') TRIM(in%timefilename)
       STOP 1
    END IF
    CLOSE(FPTIME)
     
    PRINT '("# shear modulus")'
    CALL getdata(iunit,dataline)
    READ  (dataline,*) in%mu
    PRINT '(ES9.2E1)', in%mu
 
    IF (0 .GT. in%mu) THEN
       WRITE_DEBUG_INFO(-1)
       WRITE (STDERR,'(a)') TRIM(dataline)
       WRITE (STDERR,'("input error: shear modulus must be positive")')
       STOP 2
    END IF
  
    PRINT '("# time interval")'
    CALL getdata(iunit,dataline)
    READ  (dataline,*) in%interval
    PRINT '(ES20.12E2)', in%interval
  
    IF (in%interval .LE. 0._8) THEN
       WRITE (STDERR,'("**** error **** ")')
       WRITE (STDERR,'(a)') TRIM(dataline)
       WRITE (STDERR,'("simulation time must be positive. exiting.")')
       STOP 1
    END IF
  
    ! - - - - - - - - - - - - - - - - - - - - - - - - - -
    !                   P A T C H E S
    ! - - - - - - - - - - - - - - - - - - - - - - - - - -
    PRINT '("# number of patches")'
    CALL getdata(iunit,dataline)
    READ  (dataline,*) in%patch%ns
    PRINT '(I5)', in%patch%ns
    IF (in%patch%ns .GT. 0) THEN
       ALLOCATE(in%patch%s(in%patch%ns), &
                in%patch%x(3,in%patch%ns), &
                in%patch%xc(3,in%patch%ns), &
                in%patch%width(in%patch%ns), &
                in%patch%dip(in%patch%ns), &
                in%patch%sv(3,in%patch%ns), &
                in%patch%dv(3,in%patch%ns), &
                in%patch%nv(3,in%patch%ns),STAT=ierr)
       IF (ierr>0) STOP "could not allocate the patch list"
       PRINT 2000
       PRINT '("# n       Vl       x2       x3    width      dip")'
       PRINT 2000
       DO k=1,in%patch%ns
          CALL getdata(iunit,dataline)
          READ (dataline,*,IOSTAT=ierr) i, &
               in%patch%s(k)%Vl, &
               in%patch%x(2,k), &
               in%patch%x(3,k), &
               in%patch%width(k), &
               in%patch%dip(k)
   
          PRINT '(I3.3,2ES9.2E1,ES9.3E1,ES9.3E1,ES9.2E1)',i, &
               in%patch%s(k)%Vl, &
               in%patch%x(2,k), &
               in%patch%x(3,k), &
               in%patch%width(k), &
               in%patch%dip(k)
                
          ! convert to radians
          in%patch%dip(k)=in%patch%dip(k)*DEG2RAD     

          IF (i .ne. k) THEN
             WRITE (STDERR,'("invalid patch definition")')
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE (STDERR,'("error in input file: unexpected index")')
             STOP 1
          END IF
          IF (in%patch%width(k) .LE. 0._8) THEN
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE (STDERR,'("error in input file: patch width must be positive.")')
             STOP 1
          END IF
                
       END DO
   
       ! export the fault patches
       filename=TRIM(in%wdir)//"/rfaults.flt.2d"
       OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
       IF (ierr>0) THEN
          WRITE_DEBUG_INFO(102)
          WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
          STOP 1
       END IF
       WRITE (FPOUT,'("#  n        Vl            x2            x3       width         dip")')
       DO k=1,in%patch%ns
          WRITE (FPOUT,'(I4,ES10.2E2,2ES14.7E1,2ES12.6E1)') k, &
            in%patch%s(k)%Vl, &
            in%patch%x(2,k), &
            in%patch%x(3,k), &
            in%patch%width(k), &
            in%patch%dip(k)
       END DO
       CLOSE(FPOUT)
                
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !        F R I C T I O N   P R O P E R T I E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of frictional patches")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) dummy
       PRINT '(I5)', dummy
       IF (dummy .NE. in%patch%ns) THEN
          WRITE_DEBUG_INFO(-1)
          WRITE(STDERR,'("input error: all patches require frictional properties")')
          STOP 2
       END IF
       SELECT CASE(frictionLawType)
       CASE(1:3)
          PRINT '("# n     tau0      mu0      sig        a        b        L       Vo  G/(2Vs)")'
          PRINT 2000
          DO k=1,in%patch%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                   in%patch%s(k)%tau0, &
                   in%patch%s(k)%mu0, &
                   in%patch%s(k)%sig, &
                   in%patch%s(k)%a, &
                   in%patch%s(k)%b, &
                   in%patch%s(k)%L, &
                   in%patch%s(k)%Vo, &
                   in%patch%s(k)%damping

            PRINT '(I3.3,8ES9.2E1)',i, &
                   in%patch%s(k)%tau0, &
                   in%patch%s(k)%mu0, &
                   in%patch%s(k)%sig, &
                   in%patch%s(k)%a, &
                   in%patch%s(k)%b, &
                   in%patch%s(k)%L, &
                   in%patch%s(k)%Vo, &
                   in%patch%s(k)%damping
             
             IF (i .ne. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'("invalid friction property definition for patch")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO

       CASE(4)
          PRINT '("# n     tau0      mu0      sig        a        b        L       V1       V2  G/(2Vs)")'
          PRINT 2000
          DO k=1,in%patch%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                   in%patch%s(k)%tau0, &
                   in%patch%s(k)%mu0, &
                   in%patch%s(k)%sig, &
                   in%patch%s(k)%a, &
                   in%patch%s(k)%b, &
                   in%patch%s(k)%L, &
                   in%patch%s(k)%Vo, &
                   in%patch%s(k)%V2, &
                   in%patch%s(k)%damping

            PRINT '(I3.3,9ES9.2E1)',i, &
                   in%patch%s(k)%tau0, &
                   in%patch%s(k)%mu0, &
                   in%patch%s(k)%sig, &
                   in%patch%s(k)%a, &
                   in%patch%s(k)%b, &
                   in%patch%s(k)%L, &
                   in%patch%s(k)%Vo, &
                   in%patch%s(k)%V2, &
                   in%patch%s(k)%damping
             
             IF (i .ne. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'("invalid friction property definition for patch")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO

       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug")') frictionLawType
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT

    END IF
       
    ! - - - - - - - - - - - - - - - - - - - - - - - - - -
    !       O B S E R V A T I O N   P A T C H E S
    ! - - - - - - - - - - - - - - - - - - - - - - - - - -
    PRINT 2000
    PRINT '("# number of observation patches")'
    CALL getdata(iunit,dataline)
    READ  (dataline,*) in%nObservationState
    PRINT '(I5)', in%nObservationState
    IF (0 .LT. in%nObservationState) THEN
       ALLOCATE(in%observationState(3,in%nObservationState),STAT=ierr)
       IF (ierr>0) STOP "could not allocate the observation patches"
       PRINT 2000
       PRINT '("# n      i rate")'
       PRINT 2000
       DO k=1,in%nObservationState
          CALL getdata(iunit,dataline)
          READ (dataline,*,IOSTAT=ierr) i,in%observationState(1:2,k)

          PRINT '(I3.3,X,I6,X,I4)',i,in%observationState(1:2,k)

          IF (0 .GE. in%observationState(2,k)) THEN
             WRITE_DEBUG_INFO(200)
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE (STDERR,'("error in input file: sampling rate must be a strictly positive integer")')
             STOP 1
          END IF

          IF (i .NE. k) THEN
             WRITE_DEBUG_INFO(200)
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE (STDERR,'("error in input file: unexpected index")')
             STOP 1
          END IF
       END DO
    END IF

    ! - - - - - - - - - - - - - - - - - - - - - - - - - -
    !        O B S E R V A T I O N   P O I N T S
    ! - - - - - - - - - - - - - - - - - - - - - - - - - -
    PRINT 2000
    PRINT '("# number of observation points")'
    CALL getdata(iunit,dataline)
    READ  (dataline,*) in%nObservationPoint
    PRINT '(I5)', in%nObservationPoint
    IF (0 .LT. in%nObservationPoint) THEN
       ALLOCATE(in%observationPoint(in%nObservationPoint),STAT=ierr)
       IF (ierr>0) STOP "could not allocate the observation points"
       PRINT 2000
       PRINT '("# n name       x2       x3 rate")'
       PRINT 2000
       DO k=1,in%nObservationPoint
          CALL getdata(iunit,dataline)
          READ (dataline,*,IOSTAT=ierr) i, &
                  in%observationPoint(k)%name, &
                  in%observationPoint(k)%x(2), &
                  in%observationPoint(k)%x(3), &
                  in%observationPoint(k)%rate

          ! set along-strike coordinate to zero for 2d solutions
          in%observationPoint(k)%x(1)=0._8

          PRINT '(I3.3,X,a4,2ES9.2E1,X,I4)',i, &
                  in%observationPoint(k)%name, &
                  in%observationPoint(k)%x(2), &
                  in%observationPoint(k)%x(3), &
                  in%observationPoint(k)%rate

          IF (0 .GE. in%observationPoint(k)%rate) THEN
             WRITE_DEBUG_INFO(200)
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE (STDERR,'("error in input file: sampling rate must be a strictly positive integer")')
             STOP 1
          END IF

          IF (i .NE. k) THEN
             WRITE_DEBUG_INFO(200)
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE (STDERR,'("error in input file: unexpected index")')
             STOP 1
          END IF
       END DO

       filename=TRIM(in%wdir)//"/opts.dat"
       OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
       IF (ierr>0) THEN
          WRITE_DEBUG_INFO(102)
          WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
          STOP 1
       END IF
       WRITE (FPOUT,'("#      x2       x3 name rate")')
       DO k=1,in%nObservationPoint
          WRITE (FPOUT,'(2ES9.2E1,X,a4,X,I4)') &
                  in%observationPoint(k)%x(2), &
                  in%observationPoint(k)%x(3), &
                  in%observationPoint(k)%name, &
                  in%observationPoint(k)%rate
       END DO
       CLOSE(FPOUT)

    END IF

    ! test the presence of dislocations
    IF ((in%patch%ns .EQ. 0) .AND. &
        (in%interval .LE. 0._8)) THEN
   
       WRITE_DEBUG_INFO(300)
       WRITE (STDERR,'("nothing to do. exiting.")')
       STOP 1
    END IF
   
    PRINT 2000
    ! flush standard output
    CALL FLUSH(6)      

2000 FORMAT ("# ----------------------------------------------------------------------------")
     
  END SUBROUTINE init

  !-----------------------------------------------
  !> subroutine printhelp
  !! displays a help message with master thread.
  !-----------------------------------------------
  SUBROUTINE printhelp()
  
    INTEGER :: ierr
    
    PRINT '("usage:")'
    PRINT '("")'
#ifdef __CUDA__
    PRINT '("unicycle-ap-ratestate-cu [-h] [--dry-run] [--help] [--epsilon 1e-6] [filename]")'
#else
    PRINT '("unicycle-ap-ratestate-serial [-h] [--dry-run] [--help] [--epsilon 1e-6] [filename]")'
#endif
    PRINT '("")'
    PRINT '("options:")'
    PRINT '("   -h                      prints this message and aborts calculation")'
    PRINT '("   --dry-run               abort calculation, only output geometry")'
    PRINT '("   --export-netcdf         export the kinematics to a netcdf file")'
    PRINT '("   --help                  prints this message and aborts calculation")'
    PRINT '("   --version               print version number and exit")'
    PRINT '("   --epsilon               set the numerical accuracy [1E-6]")'
    PRINT '("   --export-greens wdir    export the Greens function to file")'
    PRINT '("   --friction-law          type of friction law [1]")'
    PRINT '("       1: multiplicative   form of rate-state friction (Barbot, 2019)")'
    PRINT '("       2: additive         form of rate-state friction (Ruina, 1983)")'
    PRINT '("       3: arcsinh          form of rate-state friction (Rice & Benzion, 1996)")'
    PRINT '("       4: cut-off velocity form of rate-state friction (Okubo, 1989)")'
    PRINT '("   --maximum-iterations    set the maximum time step [1000000]")'
    PRINT '("   --maximum-step          set the maximum time step [none]")'
    PRINT '("")'
    PRINT '("description:")'
    PRINT '("   simulates elasto-dynamics on faults in antiplane strain")'
    PRINT '("   following the radiation-damping approximation")'
    PRINT '("   using the integral method.")'
    PRINT '("")'
    PRINT '("   if filename is not provided, reads from standard input.")'
    PRINT '("")'
    PRINT '("see also: ""man unicycle""")'
    PRINT '("")'
    PRINT '("            \_o___      ")'
    PRINT '("     _         \        ")'
    PRINT '("    / \       __\       ")'
    PRINT '("   |   |      \ /       ")'
    PRINT '("    \_/       ~|\       ")'
    PRINT '("               |~       ")'
    PRINT '("               -        ")'
    PRINT '("             ( * )      ")'
    PRINT '("--------------------------------")'
    PRINT '("")'
    CALL FLUSH(6)
    
  END SUBROUTINE printhelp
  
  !-----------------------------------------------
  !> subroutine printversion
  !! displays code version with master thread.
  !-----------------------------------------------
  SUBROUTINE printversion()
    
    INTEGER :: ierr
    
    PRINT '("unicycle-ap-ratestate-serial version 1.0.0, compiled on ",a)', __DATE__
    PRINT '("")'
    CALL FLUSH(6)
  
  END SUBROUTINE printversion
    
END PROGRAM ratestate_serial

