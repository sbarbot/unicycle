
OBJRS=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, \
      types_ap.o getdata.o antiplane.o greens_ap.o ratestate.o )

OBJCZ=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, \
      types_ap.o getdata.o antiplane.o greens_cz.o ratestate_cz.o )

OBJTH=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, \
      types_ap_bath.o getdata.o antiplane.o greens_ap_bath.o ratestate_bath.o )

OBJLU=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, types.o \
      getopt_m.o exportnetcdf.o getdata.o \
      antiplane.o greens.o ode45.o ratestate_lubrication.o )

OBJHG=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, types.o \
      getopt_m.o exportnetcdf.o getdata.o \
      antiplane.o greens.o ode45.o healing_bath.o )

OBJGR=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, types.o \
      getopt_m.o exportnetcdf.o getdata.o \
      antiplane.o greens.o ode45.o healing_bath_granite.o )

OBJVC=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, \
      types_ap.o getdata.o antiplane.o greens_ap.o viscouscycles.o )

OBJTB=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, \
      types_ap_thermobaric.o getdata.o antiplane.o greens_ap_thermobaric.o rootbisection.o thermobaric.o )

LIB=$(patsubst %,$(LIBDST)/%, \
      getopt_m.o exportnetcdf.o rk.o gauss.o)

$(shell mkdir -p $(DST))
$(shell mkdir -p $(LIBDST))

$(DST)/%.o:$(SRC)/%.c
	$(COMPILE.c) $^ -o $(DST)/$*.o

$(DST)/%.o: $(SRC)/%.f $(SRC)/macros.h90
	$(COMPILE.f) $(filter-out $(SRC)/macros.h90,$^) -o $(DST)/$*.o

$(DST)/%.o: $(SRC)/%.f90 $(SRC)/macros.h90
	$(COMPILE.f) $(filter-out $(SRC)/macros.h90,$^) -o $(DST)/$*.o -J $(DST)

$(LIBDST)/%.o: $(LIBSRC)/%.f90
	$(COMPILE.f) $^ -o $(LIBDST)/$*.o -J $(LIBDST)

all: lib \
	unicycle-ap-ratestate \
	unicycle-ap-ratestate-cz \
	unicycle-ap-viscouscycles \
	unicycle-ap-ratestate-bath unicycle-ap-thermobaric

unicycle-ap-ratestate: $(filter-out $(SRC)/macros.h90,$(OBJRS)) $(LIB)
	$(LINK.f) -o $(DST)/$@ $^ $(LIBS)

unicycle-ap-ratestate-cz: $(filter-out $(SRC)/macros.h90,$(OBJCZ)) $(LIB)
	$(LINK.f) -o $(DST)/$@ $^ $(LIBS)

unicycle-ap-ratestate-bath: $(filter-out $(SRC)/macros.h90,$(OBJTH)) $(LIB)
	$(LINK.f) -o $(DST)/$@ -DBATH $^ $(LIBS)

unicycle-ap-ratestate-lubrication: $(filter-out $(SRC)/macros.h90,$(OBJLU))
	$(LINK.f) -o $(DST)/$@ -DBATH -DLUBRICATION $^ $(LIBS)

unicycle-ap-healing-bath: $(filter-out $(SRC)/macros.h90,$(OBJHG))
	$(LINK.f) -o $(DST)/$@ -DBATH $^ $(LIBS)

unicycle-ap-healing-bath-granite: $(filter-out $(SRC)/macros.h90,$(OBJGR))
	$(LINK.f) -o $(DST)/$@ -DBATH $^ $(LIBS)

unicycle-ap-viscouscycles: $(filter-out $(SRC)/macros.h90,$(OBJVC)) $(LIB)
	$(LINK.f) -o $(DST)/$@ $^ $(LIBS)

unicycle-ap-thermobaric: $(filter-out $(SRC)/macros.h90,$(OBJTB)) $(LIB)
	$(LINK.f) -o $(DST)/$@ $^ $(LIBS)

lib: $(LIB)

clean:
	$(RM) $(DST)/*.o $(DST)/*.mod

.NOTPARALLEL:

