#!/bin/bash

# SEAS Benchmark Problem BP1
#
# This initial benchmark considers a planar fault embedded in a homogeneous, 
# linear elastic half-space with a free surface. The fault is governed by
# rate-and-state friction down to the depth Wf and creeps at an imposed
# constant rate Vp down to the infinite depth. The simulations will include
# the nucleation, propagation, and arrest of earthquakes, and aseismic slip
# in the post- and inter-seismic periods.

set -e
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

selfdir=$(dirname $0)

N=1600

WDIR=$selfdir/output-bp1
FLT=$WDIR/flt.2d

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

echo "" | awk -v N=$N 'BEGIN{i=1;pi=atan2(1.0,0.0)*2.0}{
       	x2o=0;
       	x3o=0;
	Vpl=1e-9;
       	width=25;
       	dip=90;
	d2=1+cos(pi*dip/180.0)-1;
	d3=1+sin(pi*dip/180.0)-1;
       	for (j=0;j<N;j++){
       	        x2=x2o+j*d2*width;
       	        x3=x3o+j*d3*width;
       	        printf "%05d %e %16.11e %16.11e %f %f\n", i,Vpl,x2,x3,width,dip;
       	        i=i+1;
       	}
}' > $FLT

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli (MPa)
32038.12032
# time interval (s)
9.45e10
# number of patches
`grep -v "#" $FLT | wc -l`
# n  Vpl x2   x3 width dip
`grep -v "#" $FLT`
# number of frictional patches
`grep -v "#" $FLT | wc -l`
# n tau0 mu0 sig    a     b    L   Vo G/2Vs
`grep -v "#" $FLT | awk 'function boxcar(x){return (x>=-0.5 && x<=0.5)?1:0};function heavi(x){return (x>0)?1:0};function ramp(x){return x*boxcar(x-0.5)+heavi(x-1)} function asinh(x){return log(x+sqrt(1+x^2))} function sinh(x){return (exp(x)-exp(-x))/2}{ 
	depth=$4; 
	mu0=0.6;
	sig=50;
	amax=0.025;
	a=1.0e-2+ramp((depth-15e3)/3e3)*(amax-0.01);
	b=0.015;
	Vo=1e-6;
	L=8e-3;
	rho=2670;
	Vs=3464;
	G=rho*Vs^2/1e6;
	damping=G/Vs/2;
	Vpl=1e-9;
	tau0=amax*sig*asinh(Vpl/Vo/2*exp((mu0+b*log(Vo/Vpl))/amax));
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,damping;
	}'`
# number of observation patches
`grep -v "#" $FLT | awk '{if (0 == (NR-1)%4){print $0}}' | wc -l | awk '{print $1+12}'`
# n   index rate
  1       1    1
  2     101    1
  3     201    1
  4     301    1
  5     401    1
  6     501    1
  7     601    1
  8     701    1
  9     801    1
 10    1001    1
 11    1201    1
 12    1401    1
# subsample the fault in space and time
`grep -v "#" $FLT | awk 'BEGIN{i=13}{if (0 == (NR-1)%4){print i,NR,20;i=i+1}}'`
# number of observation points
0
EOF

time mpirun -n 2 unicycle-ap-ratestate \
	--friction-law 3 \
	--maximum-step 3.15e7 \
	--export-netcdf \
	--maximum-iterations 1000000 \
	$WDIR/in.param

files=`grep -v "#" $FLT | awk 'BEGIN{i=13}{if (0 == (NR-1)%4){print i; i=i+1}}' | \
	awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`

echo "# export to $WDIR/slip-log10v.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $2}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$2,$5}'
done | surface -I1/`echo $yRange | awk '{print $1/2048}'` -R1/$N/0/$yRange -G$WDIR/slip-log10v.grd

echo "# export to $WDIR/log10v.grd"
yRange=`for i in $files; do wc -l $i | awk '{print $1}'; done | minmax -C | awk '{print ($2-1)*20+1}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	grep -v "#" $i | awk -v i=$index '{print i,(NR-1)*20+1,$5}'
done | surface -I1/20 -R1/$N/1/$yRange -G$WDIR/log10v2.grd

echo "# export to $WDIR/time-log10v.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $1/3.15e7}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	grep -v "#" $i | awk -v i=$index '{print i,$1/3.15e7,$5}'
done | blockmean -I1/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -E | awk '{print $1,$2,$6}' | \
	surface -I1/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/time-log10v.grd

# convert to SCEC format
files=`grep -v "#" $FLT | awk 'BEGIN{i=13}{if (0 == (NR-1)%4){print i; i=i+1}}' | \
	awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`

minStep=`grep -v "#" $WDIR/time.dat | minmax -C -H1 | awk '{print $3}'`
maxStep=`grep -v "#" $WDIR/time.dat | minmax -C | awk '{print $4}'`
numStep=`grep -v "#" $WDIR/time.dat | wc -l | awk '{print $1}'`

for i in `printf "%s\n" {1..12} | awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	depth=`echo $index | awk '{print ($1-1) * 25}'`
	station=$WDIR/fltst_dp`echo $depth | awk '{printf "%03.3d",$1/100}'`

	echo "# export to $station"

	grep -v "#" $i | \
		awk -v d="`date`" -v minStep=$minStep -v maxStep=$maxStep -v numStep=$numStep -v depth=$depth 'BEGIN{
		print "# problem=Benchmark problem (No.1)"; 
		print "# author=Sylvain Barbot"; 
		print "# date="d; 
		print "# code=unicycle-ap-ratestate"; 
		print "# code_version=beta"; 
		print "# element_size=25 m"; 
		print "# minimum_time_step="minStep; 
		print "# maximum_time_step="maxStep; 
		print "# num_time_steps="numStep; 
		print "# location=on fault, "depth"km depth"; 
		print "# Column #1 = Time (s)"; 
		print "# Column #2 = Slip (m)"; 
		print "# Column #3 = Slip rate (log10 m/s)"; 
		print "# Column #4 = Shear stress (MPa)"; 
		print "# Column #5 = State (log10 s)"; 
		print "t slip slip_rate shear_stress state"
	}{
		print $1,$2,$5,$3,$4
	}' > $station
done
