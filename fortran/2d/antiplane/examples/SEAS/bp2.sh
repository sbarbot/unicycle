#!/bin/bash

# SEAS Benchmark Problem BP2
#
# The problem set-up for this second benchmark is identical to that of the first benchmark
# problem (BP1) except for some changes in model parameters (Dc and tf) and suggested cell
# sizes. All other parameters are unchanged. Please also note some changes in the sections
# related to data output: (1) locations for time series output are dierent from BP1 and (2)
# depth intervals for slip prole output depend on the cell size (as opposed to 500 m in BP1).

set -e
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

selfdir=$(dirname $0)

for WIDTH in 800 400 300 200 100 50 25; do

	WDIR=`echo "" | awk -v wdir=$selfdir -v width=$WIDTH '{printf "%s/output-bp2-%03d",wdir,width}'`

	if [ ! -e $WDIR ]; then
		echo adding directory $WDIR
		mkdir $WDIR
	fi

	N=`echo 40e3 $WIDTH | awk '{print $1/$2}'`

	JUMP=`echo 2.4e3 $WIDTH | awk '{print int($1/$2)}'`

	FLT=$WDIR/flt.2d

	echo "# using $WDIR"
	echo "# using $N patches of ${WIDTH}m width"

	echo "" | awk -v N=$N -v width=$WIDTH 'BEGIN{i=1;pi=atan2(1.0,0.0)*2.0}{
	       	x2o=0;
	       	x3o=0;
		Vpl=1e-9;
	       	dip=90;
		d2=1+cos(pi*dip/180.0)-1;
		d3=1+sin(pi*dip/180.0)-1;
	       	for (j=0;j<N;j++){
	       	        x2=x2o+j*d2*width;
	       	        x3=x3o+j*d3*width;
	       	        printf "%05d %e %16.11e %16.11e %f %f\n", i,Vpl,x2,x3,width,dip;
	       	        i=i+1;
	       	}
	}' > $FLT

	cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli (MPa)
32038.12032
# time interval (s)
3.78432e10
# number of patches
`grep -v "#" $FLT | wc -l`
# n  Vpl x2   x3 width dip
`grep -v "#" $FLT`
# number of frictional patches
`grep -v "#" $FLT | wc -l`
# n tau0 mu0 sig    a     b    L   Vo G/2Vs
`grep -v "#" $FLT | awk '
	function boxcar(x){return (x>=-0.5 && x<=0.5)?1:0};
	function heavi(x){return (x>0)?1:0};function ramp(x){return x*boxcar(x-0.5)+heavi(x-1)};
	function asinh(x){return log(x+sqrt(1+x^2))};
	function sinh(x){return (exp(x)-exp(-x))/2};
	{ 
	depth=$4; 
	mu0=0.6;
	sig=50;
	amax=0.025;
	a=1.0e-2+ramp((depth-15e3)/3e3)*(amax-0.01);
	b=0.015;
	Vo=1e-6;
	L=4e-3;
	rho=2670;
	Vs=3464;
	G=rho*Vs^2/1e6;
	damping=G/Vs/2;
	Vpl=1e-9;
	tau0=amax*sig*asinh(Vpl/Vo/2*exp((mu0+b*log(Vo/Vpl))/amax));
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,damping;
	}'`
# number of observation patches
`grep -v "#" $FLT | awk -v jump=$JUMP '{if (0 == (NR-1)%jump){print $0}}' | wc -l | awk '{print $1+12}'`
# n   index rate
`echo "" | awk -v width=$WIDTH '{
	print  1,int(   0e3/width)+1,1
	print  2,int( 2.4e3/width)+1,1
	print  3,int( 4.8e3/width)+1,1
	print  4,int( 7.2e3/width)+1,1
	print  5,int( 9.6e3/width)+1,1
	print  6,int(12.0e3/width)+1,1
	print  7,int(14.4e3/width)+1,1
	print  8,int(16.8e3/width)+1,1
	print  9,int(19.2e3/width)+1,1
	print 10,int(24.0e3/width)+1,1
	print 11,int(28.8e3/width)+1,1
	print 12,int(36.0e3/width)+1,1
}'`
# subsample the fault in space and time
`grep -v "#" $FLT | awk -v jump=$JUMP 'BEGIN{i=13}{if (0 == (NR-1)%jump){print i,NR,10;i=i+1}}'`
# number of observation points
0
EOF

	time mpirun -n 2 unicycle-ap-ratestate \
		--friction-law 3 \
		--maximum-step 3.15e7 \
		--export-netcdf \
		--maximum-iterations 1000000 \
		$WDIR/in.param

	files=`grep -v "#" $FLT | awk -v jump=$JUMP 'BEGIN{i=13}{if (0 == (NR-1)%jump){print i; i=i+1}}' | \
		awk -v wdir=$WDIR '{printf "%s/patch-%08d* ",wdir,$1}'`

	# slip-log10v.grd
	echo "# export to $WDIR/slip-log10v.grd"
	yRange=`for i in $files; do tail -n 1 $i | awk '{print $2}'; done | minmax -C | awk '{print $2}'`
	for i in $files; do
		index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
		cat $i | awk -v i=$index '{print i,$2,$5}'
	done | surface -I1/`echo $yRange | awk '{print $1/2048}'` -R1/$N/0/$yRange -G$WDIR/slip-log10v.grd

	# log10v.grd"
	echo "# export to $WDIR/log10v.grd"
	yRange=`for i in $files; do wc -l $i | awk '{print $1}'; done | minmax -C | awk '{print ($2-1)*10+1}'`
	for i in $files; do
		index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
		grep -v "#" $i | awk -v i=$index '{print i,(NR-1)*20+1,$5}'
	done | surface -I1/10 -R1/$N/1/$yRange -G$WDIR/log10v2.grd

	# time-log10v.grd"
	echo "# export to $WDIR/time-log10v.grd"
	yRange=`for i in $files; do tail -n 1 $i | awk '{print $1/3.15e7}'; done | minmax -C | awk '{print $2}'`
	for i in $files; do
		index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
		grep -v "#" $i | awk -v i=$index '{print i,$1/3.15e7,$5}'
	done | blockmean -I1/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -E | awk '{print $1,$2,$6}' | \
		surface -I1/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/time-log10v.grd

	# convert to SCEC format
	minStep=`grep -v "#" $WDIR/time.dat | minmax -C -H1 | awk '{print $3}'`
	maxStep=`grep -v "#" $WDIR/time.dat | minmax -C | awk '{print $4}'`
	numStep=`grep -v "#" $WDIR/time.dat | wc -l | awk '{print $1}'`

	for i in `printf "%s\n" {1..12} | awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`; do
		index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
		depth=`echo $index | awk -v width=$WIDTH '{print ($1-1) * width}'`
		station=$WDIR/fltst_dp`echo $depth | awk '{printf "%03.3d",$1/100}'`

		echo "# export to $station"

		grep -v "#" $i | \
			awk -v d="`date`" -v minStep=$minStep -v maxStep=$maxStep -v numStep=$numStep -v depth=$depth 'BEGIN{
			print "# problem=Benchmark problem (No.2)";
			print "# author=Sylvain Barbot"; 
			print "# date="d; 
			print "# code=unicycle-ap-ratestate"; 
			print "# code_version=beta"; 
			print "# element_size=$WIDTH m"; 
			print "# minimum_time_step="minStep; 
			print "# maximum_time_step="maxStep; 
			print "# num_time_steps="numStep; 
			print "# location=on fault, "depth"km depth"; 
			print "# Column #1 = Time (s)"; 
			print "# Column #2 = Slip (m)"; 
			print "# Column #3 = Slip rate (log10 m/s)"; 
			print "# Column #4 = Shear stress (MPa)"; 
			print "# Column #5 = State (log10 s)"; 
			print "t slip slip_rate shear_stress state"
		}{
			print $1,$2,$5,$3,$4
		}' > $station
	done

done

