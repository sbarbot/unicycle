#!/bin/bash

# thermally weakening patch embedded in a strengthening
# generating slow-slip events.

selfdir=$(dirname $0)

N=1000

WDIR=output1
FLT=$WDIR/flt.2d

if [ ! -e $WDIR ]; then
	echo "# adding directory $WDIR"
	mkdir $WDIR
fi

echo "" | awk -v N=$N 'BEGIN{i=1;pi=atan2(1.0,0.0)*2.0}{
       	x2o=0;
       	x3o=10e3;
       	width=20;
       	dip=90;
	d2=1+cos(pi*dip/180.0)-1;
	d3=1+sin(pi*dip/180.0)-1;
       	for (j=0;j<N;j++){
       	        x2=x2o+j*d2*width;
       	        x3=x3o+j*d3*width;
       	        printf "%05d %e %16.11e %16.11e %f %f\n", i,1e-09,x2,x3,width,dip;
       	        i=i+1;
       	}
}' > $FLT

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli (MPa), universal gas constant (J/K/mol)
30e3 8.314462618153
# time interval (s)
5e9
# number of patches
`grep -v "#" $FLT | wc -l`
# n  Vpl x2   x3 width dip
`grep -v "#" $FLT`
# number of frictional properties
`grep -v "#" $FLT | wc -l`
# n tau0 mu0 sig    a     b    L   Vo G/2Vs
`grep -v "#" $FLT | awk '{ 
	depth=$4; 
	tau0=-1;
	mu0=0.6;
	sig=50;
	a=1.0e-2;
	b=a-4e-3;
	L=5e-3;
	Vo=1e-6;
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,5;
	}'`
# number of thermal properties
`grep -v "#" $FLT | wc -l`
# n        Q        H        D        W        w     rhoc       Tb
`grep -v "#" $FLT | awk -v fw=1.5e3 '{ 
	depth=$4; 
	Q=90e3;
	H=60e3;
	D=1e-6;
	W=2;
	w=(depth>17e3 && depth<(17e3+fw))?5e-4:1e-0;
	rhoc=2.6;
	Tb=600+273.15;
	printf "%06d %e %e %e %e %e %e %e\n", NR,Q,H,D,W,w,rhoc,Tb;
	}'`
# number of observation patches
0
# number of observation points
0
EOF

time mpirun -n 2 unicycle-ap-ratestate-bath \
	--maximum-step 3.15e7 \
	--export-netcdf \
	--export-netcdf-temperature \
	--export-netcdf-slip \
	--export-netcdf-stress \
	--export-netcdf-rate 20 \
	--maximum-iterations 1000000 \
	$WDIR/in.param

