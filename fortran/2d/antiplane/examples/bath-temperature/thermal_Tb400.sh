#!/bin/bash

# cycle of slow-slip events due to thermal instabilities
# for a rate-and-state friction fault in non-isothermal
# conditions. Simulations are a two-dimensional approximation
# of the deep slow-slip events at Parkfield (Rousset et al., 2019).

selfdir=$(dirname $0)

N=1000
FW=1.1e3

WDIR=$selfdir/thermal_Tb400
FLT=$WDIR/flt.2d

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

echo "" | awk -v N=$N 'BEGIN{i=1;pi=atan2(1.0,0.0)*2.0}{
       	x2o=0;
       	x3o=10e3;
       	width=20;
       	dip=90;
	d2=1+cos(pi*dip/180.0)-1;
	d3=1+sin(pi*dip/180.0)-1;
       	for (j=0;j<N;j++){
       	        x2=x2o+j*d2*width;
       	        x3=x3o+j*d3*width;
       	        printf "%05d %e %16.11e %16.11e %f %f\n", i,1e-09,x2,x3,width,dip;
       	        i=i+1;
       	}
}' > $FLT

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli (MPa), universal gas constant (J/K/mol)
40e3 8.314462618153
# time interval (s)
6.3e9     # 200 years
# number of patches
`grep -v "#" $FLT | wc -l`
# n  Vpl x2   x3 width dip
`grep -v "#" $FLT`
# number of frictional properties
`grep -v "#" $FLT | wc -l`
# n tau0 mu0 sig    a     b    L   Vo G/2Vs
`grep -v "#" $FLT | awk '{ 
	depth=$4; 
	tau0=-1;
	mu0=0.5667;
	sig=25
	a=.0097;
	b=.0062;
	L=.0011;
	Vo=1e-6;
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,5;
	}'`
# number of thermal properties
`grep -v "#" $FLT | wc -l`
# n        Q        H        D        W        w     rhoc       Tb
`grep -v "#" $FLT | awk -v fw=$FW '{ 
	depth=$4; 
	Q=90.191e3;
	H=59.844e3;
	D=.67439e-6;
	W=1.4820;
	w=(depth>17e3 && depth<17e3+fw)?2.29e-4:1e-0;
	rhoc=2.6;
	Tb=400+273.15;
	printf "%06d %e %e %e %e %e %e %e\n", NR,Q,H,D,W,w,rhoc,Tb;
	}'`
# number of observation patches
`grep -v "#" $FLT | awk '{if (0 == (NR-1)%4){print $0}}' | wc -l | awk '{print $1+8}'`
# n   index rate
  1     100    1
  2     200    1
  3     300    1
  4     400    1
  5     500    1
  6     600    1
  7     700    1
  8     800    1
# subsample the fault in space and time
`grep -v "#" $FLT | awk 'BEGIN{i=9}{if (0 == (NR-1)%4){print i,NR,20;i=i+1}}'`
# number of observation points
0
EOF

time mpirun -n 16 unicycle-ap-ratestate-bath \
	--maximum-step 3.15e7 \
	--export-netcdf \
	--maximum-iterations 1000000 \
	$WDIR/in.param

files=`grep -v "#" $FLT | awk 'BEGIN{i=9}{if (0 == (NR-1)%4){print i; i=i+1}}' | \
	awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`

echo "# export $WDIR/time-temp.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $1/3.15e7}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$1/3.15e7,$6}'
done | \
	blockmean -I4/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -E | awk '{print $1,$2,$6}' | \
	surface -I4/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/time-temp.grd

echo "# export $WDIR/time-log10v.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $1/3.15e7}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$1/3.15e7,$5}'
done | \
	blockmean -I4/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -E | awk '{print $1,$2,$6}' | \
	surface -I4/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/time-log10v.grd

echo "# export $WDIR/slip-temp.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $2}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$2,$6}'
done | surface -I1/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/slip-temp.grd

echo "# export $WDIR/slip-log10v.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $2}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$2,$5}'
done | surface -I1/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/slip-log10v.grd

