#!/bin/bash

# Explore the dynamics of a velocity-weakening region on a vertical
# strike-slip fault as a function of the width of the seismogenic zone..
# The simulations produce in turn creep, long-term slow-slip events, short-term
# slow-slip events, very low-frequency earthquakes, bilateral ruptures, 
# unilateral ruptures, and partial ruptures sequences of increasing complexity
# with increasing width, corresponding to an increase in the Ruina number Ru=R/h*.

selfdir=$(dirname $0)

N=1600

for i in 3.00E3 3.25E3 3.50E3 3.75E3 4.00E3 4.25E3 4.50E3 4.75E3 5.00E3 5.25E3 5.30E3 5.35E3 5.40E3 5.45E3 5.50E3 5.55E3 5.60E3 5.65E3 5.70E3 5.75E3 5.80E3 5.85E3 5.90E3 5.95E3 6.00E3 6.05E3 6.10E3 6.15E3 6.20E3 6.25E3 6.50E3 6.75E3 7.00E3 7.25E3 7.50E3 8.00E3 9.00E3 1.00E4 1.10E4 1.20E4 1.30E4 1.40E4 1.50E4 1.60E4 1.70E4 1.80E4 1.90E4 2.00E4 2.10E4 2.20E4 2.30E4 2.40E4 2.50E4 2.75E4 3.00E4; do

	WDIR=$selfdir/output-W$i
	FLT=$WDIR/flt.2d

	if [ ! -e $WDIR ]; then
		echo adding directory $WDIR
		mkdir $WDIR
	else
		continue
	fi

	echo "" | awk -v N=$N 'BEGIN{i=1;pi=atan2(1.0,0.0)*2.0}{
        	x2o=0;
        	x3o=0;
        	width=25;
        	dip=90;
		d2=1+cos(pi*dip/180.0)-1;
		d3=1+sin(pi*dip/180.0)-1;
        	for (j=0;j<N;j++){
        	        x2=x2o+j*d2*width;
        	        x3=x3o+j*d3*width;
        	        printf "%05d %e %16.11e %16.11e %f %f\n", i,1e-09,x2,x3,width,dip;
        	        i=i+1;
        	}
	}' > $FLT

	cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli (MPa)
30e3
# time interval (s)
4e10
# number of patches
`grep -v "#" $FLT | wc -l`
# n  Vpl x2   x3 width dip
`grep -v "#" $FLT`
# number of frictional patches
`grep -v "#" $FLT | wc -l`
# n tau0 mu0 sig    a     b    L   Vo G/2Vs
`grep -v "#" $FLT | awk -v W=$i '{ 
	depth=$4; 
	tau0=-1;
	mu0=0.6;
	sig=100;
	a=1.0e-2;
	b=(depth>5e3 && depth<5e3+W)?a+4e-3:a-4e-3;
	L=4.0e-2;
	Vo=1e-6;
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,5;\
	}'`
# number of observation patches
`grep -v "#" $FLT | awk '{if (0 == (NR-1)%4){print $0}}' | wc -l | awk '{print $1+8}'`
# n   index rate
  1      50    1
  2     100    1
  3     150    1
  4     200    1
  5     250    1
  6     300    1
  7     350    1
  8     400    1
# subsample the fault in space and time
`grep -v "#" $FLT | awk 'BEGIN{i=9}{if (0 == (NR-1)%4){print i,NR,20;i=i+1}}'`
# number of observation points
0
EOF

	time mpirun -n 16 unicycle-ap-ratestate \
		--maximum-step 3.15e7 \
		--export-netcdf \
		--maximum-iterations 1000000 \
		$WDIR/in.param

	files=`grep -v "#" $FLT | awk 'BEGIN{i=9}{if (0 == (NR-1)%4){print i; i=i+1}}' | \
		awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`

	echo "# export $WDIR/slip-log10v.grd"
	yRange=`for i in $files; do tail -n 1 $i | awk '{print $2}'; done | minmax -C | awk '{print $2}'`
	for i in $files; do
		index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
		cat $i | awk -v i=$index '{print i,$2,$5}'
	done | surface -I1/`echo $yRange | awk '{print $1/512}'` -R1/$N/0/$yRange -G$WDIR/slip-log10v.grd

	echo "# export $WDIR/time-log10v.grd"
	yRange=`for i in $files; do tail -n 1 $i | awk '{print $1/3.15e7}'; done | minmax -C | awk '{print $2}'`
	for i in $files; do
		index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
		cat $i | awk -v i=$index '{print i,$1/3.15e7,$5}'
	done | \
		blockmean -I4/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -E | awk '{print $1,$2,$6}' | \
		surface -I4/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/time-log10v.grd

done

