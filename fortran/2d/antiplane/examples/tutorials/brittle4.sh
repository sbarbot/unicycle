#!/bin/bash -e

# earthquake cycle on a long vertical strike-slip fault 
# in antiplane strain with the radiation damping approximation
# using rate-state friction with a cut-off velocity (Okubo, 1989)

selfdir=$(dirname $0)

WDIR="$selfdir/brittle4"

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

WIDTH=1e2
NW=500

GEOMETRY=$(echo "" | awk -v w="$WIDTH" -v nw="$NW"  \
	'{for (j=1;j<=nw;j++){ 
		Vl=1e-9; 
		x2=0;
		x3=(j-1)*w;
		printf "%3d %9.2e %9.2e %9.2e %9.2e %3d\n", 
		j,Vl,x2,x3,w,90}}')

FRICTION=$(echo "$GEOMETRY" | awk -v nw="$NW" 'function min(x,y){return (x<y)?x:y}{ 
	depth=$4; 
	tau0=-1;
	V=$2; 
	L=0.01; 
	a=1e-2; 
	b=(5e3<= depth && depth<=15e3)?a+4.0e-3:a-4e-3; 
	mu0=0.6; 
	sig=1e2; 
	V1=1e-0; 
	V2=1e-6; 
	printf "%3d %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %d\n", 
		NR,   tau0,   mu0,   sig,     a,     b,     L,    V1,   V2, 5}')

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli
30e3
# time interval
3.15e10
# number of patches
$(echo "$GEOMETRY" | wc -l)
# n  Vpl           x2           x3    width      dip
$(echo "$GEOMETRY")
# number of frictional patches
$(echo "$GEOMETRY" | wc -l)
# n  mu0       sig         a         b         L        V1     V2     G/2Vs
$(echo "$FRICTION")
# number of observation patches (at 2, 5, 10, and 20 km depth)
4
# n   index rate
  1      20    1
  2      50    1
  3     100    1
  4     200    1
# number of observation points
5
# n name  x2 x3 rate
  1 0001 5e2  0    1
  2 0002 5e3  0    1
  3 0003 1e4  0    1
  4 0004 2e4  0    1
  5 0005 4e4  0    1
EOF

time mpirun -n 2 unicycle-ap-ratestate \
	--friction-law 4 \
	--epsilon 1e-6 \
	--export-netcdf \
	--maximum-step 3.15e6 \
	$* "$WDIR/in.param"





