#!/bin/bash -e

# earthquake cycle on a long vertical strike-slip fault 
# in antiplane strain with the radiation damping approximation

selfdir=$(dirname $0)

WDIR="$selfdir/test-results/test1"

if [ ! -e "$WDIR" ]; then
	echo "# adding directory $WDIR"
	mkdir -p "$WDIR"
fi

WIDTH=1e2
NW=500

GEOMETRY=$(echo "" | awk -v w="$WIDTH" -v nw="$NW"  \
	'{for (j=1;j<=nw;j++){ 
		Vl=1e-9; 
		x2=0;
		x3=(j-1)*w;
		printf "%3d %11.4e %11.4e %11.4e %11.4e %3d\n", 
		j,Vl,x2,x3,w,90}}')

FRICTION=$(echo "$GEOMETRY" | awk -v nw="$NW" 'function min(x,y){return (x<y)?x:y}{ 
	depth=$4; 
	tau0=-1;
	V=$2; 
	L=0.01; 
	a=1e-2; 
	b=(5e3<= depth && depth<=15e3)?a+4.0e-3:a-4e-3; 
	mu0=0.6; 
	sig=1e2; 
	Vo=1e-6; 
	printf "%3d %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %d\n", 
		NR,   tau0,   mu0,   sig,     a,     b,     L,    Vo, 5}')

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli
30e3
# time interval
1e10
# number of patches
$(echo "$GEOMETRY" | wc -l)
# n   Vl           x2           x3    width      dip   dtau
$(echo "$GEOMETRY")
# number of frictional patches
$(echo "$GEOMETRY" | wc -l)
# n  tau0    mu0       sig         a         b         L        Vo     G/2Vs
$(echo "$FRICTION")
# number of observation patches (at 2, 5, 10, and 20 km depth)
4
# n   index rate
  1      20    1
  2      50    1
  3     100    1
  4     200    1
# number of observation points
5
# n name  x2 x3 rate
  1 0001 5e2  0    1
  2 0002 5e3  0    1
  3 0003 1e4  0    1
  4 0004 2e4  0    1
  5 0005 4e4  0    1
EOF

mpirun -n 2 --allow-run-as-root unicycle-ap-ratestate \
	--epsilon 1e-6 \
	--export-netcdf \
	--maximum-step 3.15e6 \
	$* "$WDIR/in.param"

ls -al $WDIR/{in.param,log10v.grd,rfaults.flt.2d,time.dat}
ls -al $WDIR/{opts-0001.dat,opts-0002.dat,opts-0003.dat,opts-0004.dat,opts-0005.dat,opts.dat}
ls -al $WDIR/{patch-00000001-00000020.dat,patch-00000002-00000050.dat,patch-00000003-00000100.dat,patch-00000004-00000200.dat}

gnuplot <<EOF
set term dumb
set logscale y
set xlabel "Time (year)"
set ylabel "Velocity (m/s)"
plot '$WDIR/time.dat' using (\$1/3.15e7):3 with line title 'Velocity (m/s)'
EOF

echo "# test successful" | tee $WDIR/report.xml

