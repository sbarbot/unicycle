#!/bin/bash -e

# earthquake cycle on a vertical strike-slip fault in antiplane strain in a viscoelastic medium
# with the radiation damping approximation. W=10 km, h*=750 m, Ru=13.33, Rb=0.28.
#
# brittle layer from  0 to 20 km depth
# ductile layer from 20 to 30 km depth

selfdir=$(dirname $0)

WDIR="$selfdir/test-results/viscous1"

if [ ! -e "$WDIR" ]; then
	echo "# adding directory $WDIR"
	mkdir -p "$WDIR"
fi

WIDTH=50
NW=400

GEOMETRY=$(echo "" | awk -v w="$WIDTH" -v nw="$NW"  \
	'{for (j=1;j<=nw;j++){ 
		Vl=1e-9; 
		x2=0;
		z3=(j-1)*w;
		x3=(z3>=25e3)?z3+10e3:z3;
		printf "%3d %11.4e %11.4e %11.4e %11.4e %3d\n", 
		j,Vl,x2,x3,w,90}}')

FRICTION=$(echo "$GEOMETRY" | awk -v nw="$NW" 'function min(x,y){return (x<y)?x:y}{ 
	x3=$4; 
	V=$2; 
	tau0=-1;
	L=0.01; 
	a=1e-2; 
	b=(5e3<=x3 && x3<=15e3)?a+4.0e-3:a-4e-3; 
	mu0=0.6; 
	sig=1e2; 
	Vo=1e-6; 
	printf "%3d %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %d\n", 
		NR,   tau0,   mu0,   sig,     a,     b,     L,    Vo, 5}')

N2=50
N3=50
WIDTH=2e2
THICKNESS=2e2;
MANTLE=$(echo "" | awk -v n2="$N2" -v n3="$N3" -v thickness="$THICKNESS" -v width="$WIDTH" \
	'{for (j3=1;j3<=n3;j3++){ 
		x3=20e3+(j3-1)*width;
		for (j2=1;j2<=n2;j2++){
			e12=1e-14;
			e13=0;
			x2=(j2-n2/2)*thickness;
			dip=90;
			printf "%4d %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %d\n", 
			j2+(j3-1)*n2, e12, e13, x2, x3, thickness, width, dip
		}
	}}')

RHEOLOGY=$(echo "$MANTLE" | awk -v nw="$NW" 'function min(x,y){return (x<y)?x:y}{ 
	x2=$4; 
	x3=$5; 
	sII=-1;
	gammadot0m=1e-12;
	n=1;
	printf "%3d %10.2e %10.2e %10.2e\n", 
		NR,   sII,   gammadot0m,     n}')

OBS=$(echo "" | awk -v n2="$N2" -v n3="$N3" \
	'{for (j3=1;j3<=n3;j3++){ 
		printf "%4d %d %d\n",j3,n2/2+(j3-1)*n2,20
	}}')


cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# rigidity, universal gas constant
30e3
# time interval
5e9
# number of patches
$(echo "$GEOMETRY" | wc -l)
# n   Vl           x2           x3    width      dip
$(echo "$GEOMETRY")
# number of frictional patches
$(echo "$GEOMETRY" | wc -l)
# n  tau0    mu0       sig         a         b         L        Vo     G/2Vs
$(echo "$FRICTION")
# number of cuboid volume elements
$(echo "$MANTLE" | wc -l)
# n       e12       e13        x2        x3    thickness width
$(echo "$MANTLE")
# number of nonlinear Maxwell volume elements
$(echo "$MANTLE" | wc -l)
# n       sII    gammadot0m         n
$(echo "$RHEOLOGY")
# number of nonlinear Kelvin volume elements
0
# number of observation patches
4
# n   index (2, 5, 10, and 20 km depth) sampling rate
  1      20                                         1
  2      50                                         1
  3     100                                         1
  4     200                                         1
# number of observation volumes
$(echo "$OBS" | wc -l)
# n    index
$(echo "$OBS")
# number of observation points
5
# n name  x2 x3 rate
  1 0001 5e2  0    1
  2 0002 5e3  0    1
  3 0003 1e4  0    1
  4 0004 2e4  0    1
  5 0005 4e4  0    1
EOF

mpirun -n 2 --allow-run-as-root unicycle-ap-viscouscycles \
	--epsilon 1e-6 \
	--export-netcdf \
	--maximum-step 3.15e6 \
	$* "$WDIR/in.param"

# list some expected files
ls -al $WDIR/log10v.grd $WDIR/opts-0001.dat $WDIR/patch-00000001-00000020.dat $WDIR/time.dat

# plot simulation results
gnuplot <<EOF
set terminal dumb
set logscale y
set xlabel "Time (year)"
set ylabel "Velocity (m/s)"
plot '$WDIR/time.dat' using (\$1/3.15e7):3 with line title 'Velocity (m/s)'
EOF

gnuplot <<EOF
set terminal dumb
set logscale y
set xlabel "Time (year)"
set ylabel "Plastic strain-rate (1/s)"
plot '$WDIR/time.dat' using (\$1/3.15e7):5 with line title 'Strain-rate (/s)'
EOF

