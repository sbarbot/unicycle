#!/bin/bash -e

# earthquake cycle on a vertical strike-slip fault in antiplane strain in a viscoelastic medium
# with the radiation damping approximation. W=10 km, h*=750 m, Ru=13.33, Rb=0.28. 
# Linear viscosity. Rheological parameters from Rutter & Broddie (2004a) for diffusion creep 
# with grain-size of 3 µm and a lower-crust temperature of 800 ºC. 
#
# brittle layer from  0 to 20 km depth
# ductile layer from 20 to 30 km depth

# map.sh -H -p -15/-12/0.01 -c turbo.cpt viscous1/eII-time.grd

selfdir=$(dirname $0)

WDIR="$selfdir/viscous1"

if [ ! -e "$WDIR" ]; then
	echo "# adding directory $WDIR"
	mkdir -p "$WDIR"
fi

WIDTH=50
NW=400

GEOMETRY=$(echo "" | awk -v w="$WIDTH" -v nw="$NW"  \
	'{for (j=1;j<=nw;j++){ 
		Vl=1e-9; 
		x2=0;
		x3=(j-1)*w;
		printf "%03d %10.3e %11.4e %11.4e %11.4e %3d\n", 
		j,Vl,x2,x3,w,90}}')

FRICTION=$(echo "$GEOMETRY" | awk -v nw="$NW" 'function min(x,y){return (x<y)?x:y}{ 
	x3=$4; 
	V=$2; 
	tau0=-1;
	L=0.01; 
	a=1e-2; 
	b=(5e3<=x3 && x3<=15e3)?a+4.0e-3:a-4e-3; 
	mu0=0.6; 
	sig=1e2; 
	Vo=1e-6; 
	printf "%3d %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %d\n", 
		NR,   tau0,   mu0,   sig,     a,     b,     L,    Vo, 5}')

N2=50
N3=50
WIDTH=2e2
THICKNESS=2e2;
MANTLE=$(echo "" | awk -v n2="$N2" -v n3="$N3" -v thickness="$THICKNESS" -v width="$WIDTH" \
	'{for (j3=1;j3<=n3;j3++){ 
		x3=20e3+(j3-1)*width;
		for (j2=1;j2<=n2;j2++){
			x2=(j2-n2/2)*thickness;
			e12=1e-15*exp(-0.5*(x2/2.5e3)^2);
			e13=0;
			dip=90;
			printf "%4d %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %d\n", 
			j2+(j3-1)*n2, e12, e13, x2, x3, thickness, width, dip
		}
	}}')

RHEOLOGY=$(echo "$MANTLE" | awk -v nw="$NW" 'function min(x,y){return (x<y)?x:y}{ 
	x2=$4; 
	x3=$5; 
	sII=-1;
	gammadot0m=1e-12*exp(-0.5*(x2/2.5e3)^2);
	n=1;
	printf "%3d %10.2e %10.2e %10.2e\n", 
		NR,  sII, gammadot0m,  n}')

OBS=$(echo "" | awk -v n2="$N2" -v n3="$N3" \
	'{for (j3=1;j3<=n3;j3++){ 
		printf "%4d %d %d\n",j3,n2/2+(j3-1)*n2,20
	}}')


cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# rigidity, universal gas constant
30e3
# time interval
1e10
# number of patches
$(echo "$GEOMETRY" | wc -l)
# n   Vl           x2           x3    width      dip
$(echo "$GEOMETRY")
# number of frictional patches
$(echo "$GEOMETRY" | wc -l)
# n  tau0    mu0       sig         a         b         L        Vo     G/2Vs
$(echo "$FRICTION")
# number of cuboid volume elements
$(echo "$MANTLE" | wc -l)
# n       e12       e13        x2        x3    thickness width
$(echo "$MANTLE")
# number of nonlinear Maxwell volume elements
$(echo "$MANTLE" | wc -l)
# n    sII    gammadot0m         n
$(echo "$RHEOLOGY")
# number of nonlinear Kelvin volume elements
0
# number of observation patches
4
# n   index (2, 5, 10, and 20 km depth) sampling rate
  1      20                                         1
  2      50                                         1
  3     100                                         1
  4     200                                         1
# number of observation volumes
$(echo "$OBS" | wc -l)
# n    index
$(echo "$OBS")
# number of observation points
7
# n name  x2 x3 rate
  1 0001 5e2  0    1
  2 0002 5e3  0    1
  3 0003 1e4  0    1
  4 0004 2e4  0    1
  5 0005 4e4  0    1
  6 0006 5e4  0    1
  7 0007 6e4  0    1
EOF

time mpirun -n 2 unicycle-ap-viscouscycles \
	--epsilon 1e-6 \
	--export-netcdf \
	--export-volume \
	--maximum-step 3.15e6 \
	$* "$WDIR/in.param"

echo "# exporting to $WDIR/e12.grd"
xRange=$(ls $WDIR/volume-*.dat | wc -l | awk '{print $1}')
for i in $WDIR/volume-*.dat; do
	if [ "" == "$yRange" ]; then
		yRange=$(wc -l $i | awk '{print $1}')
	fi
done
c=1;for i in $WDIR/volume-*.dat; do
	cat $i | awk -v i=$c '{print i,NR,$6}'
	c=$((c+1))
done | echo xyz2grd -I1/1 -R1/$xRange/1/$yRange -G$WDIR/e12.grd
yRange=""

echo "# exporting to $WDIR/eII.grd"
xRange=$(ls $WDIR/volume-*.dat | wc -l | awk '{print $1}')
c=1;for i in $WDIR/volume-*.dat; do
	if [ "" == "$yRange" ]; then
		yRange=$(wc -l $i | awk '{print $1}')
	fi
done
c=1;for i in $WDIR/volume-*.dat; do
	cat $i | awk -v i=$c '{print i,NR,log(sqrt($6^2+$7^2))/log(10)}'
	c=$((c+1))
done | gmt xyz2grd -I1/1 -R1/$xRange/1/$yRange -G$WDIR/eII.grd
yRange=""

echo "# exporting $WDIR/time-log10v.grd"
xRange=$(gmt grdinfo -C $WDIR/log10v.grd | awk '{print $3}')
steps=$(gmt grdinfo -C $WDIR/log10v.grd | awk '{print $5*20}')
yRange=$(grep -v "#" $WDIR/time.dat | awk -v i="$steps" 'NR==i{print $1/3.15e7;next}')
dt=$(echo "$yRange" | awk '{print $1/500}')
join -o 1.2,2.2,1.3 <(gmt grd2xyz $WDIR/log10v.grd | awk '{printf "%06d %06d %20.16f\n",$2,$1,$3}' | sort) \
	<(grep -v "#" $WDIR/time.dat | awk '0==NR%20 {printf "%06d %20.17f\n",NR/20,$1/3.15e7}') |
	gmt blockmode -E -I1/$dt -R1/$xRange/0/$yRange | awk '{print $1,$2,$NF}' | \
	gmt surface -I1/$dt -R1/$xRange/0/$yRange -G$WDIR/time-log10v.grd
yRange=""

echo "# exporting to $WDIR/time-eII.grd"
xRange=$(ls $WDIR/volume-*.dat | wc -l | awk '{print $1}')
for i in $WDIR/volume-*.dat; do
	if [ "" == "$yRange" ]; then
		yRange=$(tail -n 1 $i | awk '{print $1/3.15e7}')
		dt=$(echo "$yRange" | awk '{print $1/500}')
	fi
done
c=1;for i in $WDIR/volume-*.dat; do
	cat $i | awk -v i=$c '{print i,$1/3.15e7,log(sqrt($6^2+$7^2))/log(10)}'
	c=$((c+1))
done | gmt blockmode -E -I1/$dt -R1/$xRange/0/$yRange | awk '{print $1,$2,$NF}' | \
	gmt surface -I1/$dt -R1/$xRange/0/$yRange -G$WDIR/time-eII.grd
yRange=""

