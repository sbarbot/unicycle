!-----------------------------------------------------------------------
! Copyright 2017 Sylvain Barbot
!
! This file is part of UNICYCLE
!
! UNICYCLE is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! UNICYCLE is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with UNICYCLE.  If not, see <http://www.gnu.org/licenses/>.
!-----------------------------------------------------------------------

#include "macros.h90"

MODULE antiplane

  IMPLICIT NONE

  REAL*8, PARAMETER, PRIVATE :: pi = 3.141592653589793_8

CONTAINS

  !------------------------------------------------------------------------
  !> function heaviside
  !! computes the Heaviside function
  !------------------------------------------------------------------------
  REAL*8 FUNCTION heaviside(x)
    REAL*8, INTENT(IN) :: x

     IF (0 .LT. x) THEN
        heaviside=1.0_8
     ELSE
        heaviside=0.0_8
     END IF

  END FUNCTION heaviside

  !------------------------------------------------------------------------
  !> function Omega(x)
  !! evaluates the boxcar function
  !------------------------------------------------------------------------
  REAL*8 FUNCTION omega(x)
    REAL*8, INTENT(IN) :: x

    omega=heaviside(x+0.5_8)-heaviside(x-0.5_8)

  END FUNCTION omega

  !------------------------------------------------------------------------
  !> subroutine computeReferenceSystemAntiplane
  !! computes the center position and local reference system tied to the patch
  !!
  !! INPUT:
  !! @param ns
  !! @param x           - upper left coordinate of fault patch (north, east, down)
  !! @param dip         - dip angle (radian)
  !! @param W           - width of the dislocation
  !!
  !! OUTPUT:
  !! @param sv,dv,nv    - strike, dip and normal vectors of the fault patch
  !! @param xc          - coordinates (north, east, down) of the center
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeReferenceSystemAntiplane(ns,x,W,dip,sv,dv,nv,xc)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: ns
    REAL*8, DIMENSION(3,ns), INTENT(IN) :: x
    REAL*8, DIMENSION(ns), INTENT(IN) :: dip,W
    REAL*8, DIMENSION(3,ns), INTENT(OUT) :: sv,dv,nv,xc

    ! unit vectors in the strike direction
    sv(1,1:ns)=1._8
    sv(2,1:ns)=0._8
    sv(3,1:ns)=0._8
            
    ! unit vectors in the dip direction
    dv(1,1:ns)=+0._8
    dv(2,1:ns)=-COS(dip(1:ns))
    dv(3,1:ns)=-SIN(dip(1:ns))
            
    ! unit vectors in the normal direction
    nv(1,1:ns)=-0._8
    nv(2,1:ns)=+SIN(dip(1:ns))
    nv(3,1:ns)=-COS(dip(1:ns))
            
    ! center of fault patch
    xc(1,1:ns)=x(1,1:ns)-W(1:ns)/2*dv(1,1:ns)
    xc(2,1:ns)=x(2,1:ns)-W(1:ns)/2*dv(2,1:ns)
    xc(3,1:ns)=x(3,1:ns)-W(1:ns)/2*dv(3,1:ns)
                
                
  END SUBROUTINE computeReferenceSystemAntiplane

  !------------------------------------------------------------------------
  !> subroutine computeStressAntiplane
  !! calculates the stress associated with a dislocation in an elastic
  !! elastic half-space in antiplane strain condition.
  !!
  !! INPUT:
  !! @param x2,x3    - coordinates of observation points
  !! @param q2,q3    - coordinates of upper left corner of the dislocation
  !! @param W        - width of the line dislocation
  !! @param dip      - dip of the line dislocation (radian)
  !! @param G        - rigidity
  !!
  !! OUTPUT:
  !! s12,s13         - the stress components
  !!
  !! KNOWN BUGS: 
  !! the dip angle is ignored.
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeStressAntiplane(x2,x3, &
                                  q2,q3,W,dip, &
                                  G,s12,s13)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x2,x3,q2,q3
    REAL*8, INTENT(IN) :: W,dip
    REAL*8, INTENT(IN) :: G
    REAL*8, INTENT(OUT) :: s12,s13

    REAL*8 :: r2

    r2=x2-q2

    s12=G*( &
        -(x3-q3  )/(r2**2+(x3-q3  )**2) &
        +(x3+q3  )/(r2**2+(x3+q3  )**2) &
        +(x3-q3-W)/(r2**2+(x3-q3-W)**2) &
        -(x3+q3+W)/(r2**2+(x3+q3+W)**2))/2/pi

    s13=G*( &
         r2/(r2**2+(x3-q3  )**2) &
        -r2/(r2**2+(x3+q3  )**2) &
        -r2/(r2**2+(x3-q3-W)**2) &
        +r2/(r2**2+(x3+q3+W)**2))/2/pi

  END SUBROUTINE computeStressAntiplane

  !------------------------------------------------------------------------
  !> subroutine computeDisplacementAntiplane
  !! calculates the displacements associated with a dislocation in an
  !! elastic half-space in antiplane condition.
  !!
  !! INPUT:
  !! @param x2,x3    - coordinates of observation points
  !! @param q2,q3    - coordinates of upper left corner of line dislocation
  !! @param W        - width of the line dislocation
  !! @param dip      - dip of the line dislocation (radian)
  !! @param s        - strike slip
  !!
  !! OUTPUT:
  !! u1              - displacement in the strike direction
  !!
  !! KNOWN BUGS:
  !! the dip angle is ignored.
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeDisplacementAntiplane(x2,x3, &
                                        q2,q3,W,dip, &
                                        s,u1)
    REAL*8, INTENT(IN) :: x2,x3,q2,q3
    REAL*8, INTENT(IN) :: W,dip
    REAL*8, INTENT(IN) :: s
    REAL*8, INTENT(OUT) :: u1

    u1=s*(+atan((x3-q3)/(x2-q2)) &
          -atan((x3+q3)/(x2-q2)) &
          -atan((x3-q3-W)/(x2-q2)) &
          +atan((x3+q3+W)/(x2-q2)))/2._8/pi;

  END SUBROUTINE computeDisplacementAntiplane

  !------------------------------------------------------------------------
  !> subroutine computeDisplacementRectangleAntiplane
  !! calculates the displacements associated with a dislocation in an
  !! elastic half-space in antiplane condition.
  !!
  !! INPUT:
  !! @param x2,x3    - coordinates of observation points
  !! @param q2,q3    - coordinates of upper left corner of line dislocation
  !! @param W        - width of the line dislocation
  !! @param dip      - dip of the line dislocation (radian)
  !! @param e12,e13  - anelastic strain in the horizontal and depth directions
  !!
  !! OUTPUT:
  !! u1              - displacement in the strike direction
  !!
  !! KNOWN BUGS:
  !! the dip angle is ignored.
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeDisplacementRectangleAntiplane(x2,x3, &
                                        q2,q3,T,W,dip, &
                                        e12,e13,u1)
    REAL*8, INTENT(IN) :: x2,x3,q2,q3
    REAL*8, INTENT(IN) :: T,W,dip
    REAL*8, INTENT(IN) :: e12,e13
    REAL*8, INTENT(OUT) :: u1

    REAL*8 :: r2

    r2=x2-q2

    ! displacement due to distributed anelastic strain
    u1=e12/(2*pi)*( &
           (x3-q3-W)*log((r2-T/2)**2+(x3-q3-W)**2) &
          -(x3-q3-W)*log((r2+T/2)**2+(x3-q3-W)**2) &
          -(x3-q3)*log((r2-T/2)**2+(x3-q3)**2) &
          +(x3-q3)*log((r2+T/2)**2+(x3-q3)**2) &
          +2*(r2-T/2)*(atan((x3-q3-W)/(r2-T/2))-atan((x3-q3)/(r2-T/2))) &
          +2*(r2+T/2)*(atan((x3-q3)/(r2+T/2))-atan((x3-q3-W)/(r2+T/2))) &
          +(x3+q3+W)*log((r2+T/2)**2+(x3+q3+W)**2) &
          -(x3+q3+W)*log((r2-T/2)**2+(x3+q3+W)**2) &
          -(x3+q3)*log((r2+T/2)**2+(x3+q3)**2) &
          +(x3+q3)*log((r2-T/2)**2+(x3+q3)**2) &
          +2*(r2+T/2)*(atan((x3+q3+W)/(r2+T/2))-atan((x3+q3)/(r2+T/2))) &
          +2*(r2-T/2)*(atan((x3+q3)/(r2-T/2))-atan((x3+q3+W)/(r2-T/2))) &
       ) + &
       e13/(2*pi)*( &
           (r2-T/2)*log((r2-T/2)**2+(x3-q3-W)**2) &
          -(r2+T/2)*log((r2+T/2)**2+(x3-q3-W)**2) &
          -(r2-T/2)*log((r2-T/2)**2+(x3-q3)**2) &
          +(r2+T/2)*log((r2+T/2)**2+(x3-q3)**2) &
          +2*(x3-W-q3)*(atan((r2-T/2)/(x3-q3-W))-atan((r2+T/2)/(x3-q3-W))) &
          +2*(x3-q3)*(atan((r2+T/2)/(x3-q3))-atan((r2-T/2)/(x3-q3))) &
          +(r2-T/2)*log((r2-T/2)**2+(x3+q3+W)**2) &
          -(r2+T/2)*log((r2+T/2)**2+(x3+q3+W)**2) &
          -(r2-T/2)*log((r2-T/2)**2+(x3+q3)**2) &
          +(r2+T/2)*log((r2+T/2)**2+(x3+q3)**2) &
          +2*(x3+W+q3)*(atan((r2-T/2)/(x3+q3+W))-atan((r2+T/2)/(x3+q3+W))) &
          +2*(x3+q3)*(atan((r2+T/2)/(x3+q3))-atan((r2-T/2)/(x3+q3))) &
       )

  END SUBROUTINE computeDisplacementRectangleAntiplane

  !------------------------------------------------------------------------
  !> subroutine computeDisplacementKernelsAntiplane
  !! calculates the displacement kernels associated with dislocations in
  !! an elastic half-space in antiplane condition.
  !!
  !! INPUT:
  !! @param x          - coordinates of observation points
  !! @param ns         - number of sources
  !! @param y          - coordinate (NED) of sources
  !! @param W          - width of the line dislocation
  !! @param dip        - dip of the line dislocation
  !!
  !! OUTPUT:
  !! u1                - array, displacement in the strike direction.
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeDisplacementKernelsAntiplane(x, &
                        ns,y,W,dip, &
                        u1)
    REAL*8, DIMENSION(3), INTENT(IN) :: x
    INTEGER, INTENT(IN) :: ns
    REAL*8, DIMENSION(3,ns), INTENT(IN) :: y
    REAL*8, DIMENSION(ns), INTENT(IN) :: W,dip
    REAL*8, DIMENSION(ns), INTENT(OUT) :: u1

    INTEGER :: i

    DO i=1,ns
       CALL computeDisplacementAntiplane( &
                      x(2),x(3), &
                      y(2,i),y(3,i),W(i),dip(i), &
                      1._8,u1(i))
    END DO

  END SUBROUTINE computeDisplacementKernelsAntiplane

  !------------------------------------------------------------------------
  !> subroutine computeDisplacementKernelsRectangleAntiplane
  !! calculates the displacement kernels associated with distributed
  !! anelastic deformation an elastic half-space in antiplane condition.
  !!
  !! INPUT:
  !! @param x          - coordinates of observation points
  !! @param ns         - number of sources
  !! @param y          - coordinate (NED) of sources
  !! @param T,W        - thickness and width of the strain volume
  !! @param dip        - dip of the line dislocation
  !!
  !! OUTPUT:
  !! u1                - array, displacement in the strike direction.
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeDisplacementKernelsRectangleAntiplane(x, &
                        ns,y,T,W,dip, &
                        e12p,e13p,u1)
    REAL*8, DIMENSION(3), INTENT(IN) :: x
    INTEGER, INTENT(IN) :: ns
    REAL*8, DIMENSION(3,ns), INTENT(IN) :: y
    REAL*8, DIMENSION(ns), INTENT(IN) :: T,W,dip
    REAL*8, INTENT(IN) :: e12p,e13p
    REAL*8, DIMENSION(ns), INTENT(OUT) :: u1

    INTEGER :: i

    DO i=1,ns
       CALL computeDisplacementRectangleAntiplane( &
                      x(2),x(3), &
                      y(2,i),y(3,i),T(i),W(i),dip(i), &
                      e12p,e13p,u1(i))
    END DO

  END SUBROUTINE computeDisplacementKernelsRectangleAntiplane

  !------------------------------------------------------------------------
  !> subroutine computeTractionKernelsAntiplane
  !! calculates the traction kernels associated with a dislocations in an
  !! elastic half-space in antiplane condition.
  !!
  !! INPUT:
  !! @param x          - coordinates of observation points
  !! @param sv,dv,nv   - strike, dip and normal vector of observation points
  !! @param ns         - number of sources
  !! @param y          - coordinate (NED) of sources
  !! @param W          - width of the line dislocation
  !! @param dip        - dip of the line dislocation
  !! @param s          - strike slip
  !! @param G          - rigidity
  !!
  !! OUTPUT:
  !! ts                - traction in the strike direction.
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeTractionKernelsAntiplane(x,sv,dv,nv, &
                        ns,y,W,dip, &
                        G, &
                        ts)
    REAL*8, DIMENSION(3), INTENT(IN) :: x,sv,dv,nv
    INTEGER, INTENT(IN) :: ns
    REAL*8, DIMENSION(3,ns), INTENT(IN) :: y
    REAL*8, DIMENSION(ns), INTENT(IN) :: W,dip
    REAL*8, INTENT(IN) :: G
    REAL*8, DIMENSION(ns), INTENT(OUT) :: ts

    INTEGER :: i
    REAL*8 :: s12,s13

    DO i=1,ns
       CALL computeStressAntiplane( &
                      x(2),x(3), &
                      y(2,i),y(3,i),W(i),dip(i), &
                      G,s12,s13)

       ! rotate to receiver system of coordinates
       ts(i)= (           nv(2)*s12+nv(3)*s13 )*sv(1) &
             +( nv(1)*s12                     )*sv(2) &
             +( nv(1)*s13                     )*sv(3)

    END DO

  END SUBROUTINE computeTractionKernelsAntiplane

  !------------------------------------------------------------------------
  !> subroutine computeStressKernelsAntiplane
  !! calculates the stress kernels associated with a dislocation in an
  !! in an elastic half-space in antiplane condition.
  !!
  !! INPUT:
  !! @param x          - coordinates of observation points
  !! @param sv,dv,nv   - strike, dip and normal vector of observation points
  !! @param ns         - number of sources
  !! @param y          - coordinate (NED) of sources
  !! @param W          - width of the dislocation
  !! @param dip        - dip angle of the dislocation
  !! @param s          - strike slip
  !! @param G          - rigidity
  !!
  !! OUTPUT:
  !! s12,s13           - the stress components
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeStressKernelsAntiplane(p,x,w,sv,dv,nv, &
                        ns,y,width,dip, &
                        G,s12,s13)
    INTEGER, INTENT(IN) :: p
    REAL*8, DIMENSION(3,p), INTENT(IN) :: x
    REAL*8, DIMENSION(p), INTENT(IN) :: w
    REAL*8, DIMENSION(3), INTENT(IN) :: sv,dv,nv
    INTEGER, INTENT(IN) :: ns
    REAL*8, DIMENSION(3,ns), INTENT(IN) :: y
    REAL*8, DIMENSION(ns), INTENT(IN) :: width,dip
    REAL*8, INTENT(IN) :: G
    REAL*8, DIMENSION(ns), INTENT(OUT) :: s12,s13

    INTEGER :: i,j
    REAL*8 :: s12p,s13p

    DO i=1,ns
       ! initialize volume integral
       s12p=0._8
       s13p=0._8

       ! loop over integration points
       DO j=1,p
          CALL computeStressAntiplane( &
                         x(2,j),x(3,j), &
                         y(2,i),y(3,i),width(i),dip(i), &
                         G,s12(i),s13(i))

          ! rotate to receiver system of coordinates
          s12p=s12p+w(j)*( (              sv(2)*s12(i)+sv(3)*s13(i) )*nv(1) &
                          +( sv(1)*s12(i)                           )*nv(2) &
                          +( sv(1)*s13(i)                           )*nv(3))
          s13p=s13p+w(j)*(-(              sv(2)*s12(i)+sv(3)*s13(i) )*dv(1) &
                          -( sv(1)*s12(i)                           )*dv(2) &
                          -( sv(1)*s13(i)                           )*dv(3))

       END DO

       ! weighted average
       s12(i)=s12p
       s13(i)=s13p

    END DO

  END SUBROUTINE computeStressKernelsAntiplane

  !------------------------------------------------------------------------
  !> subroutine ComputeStressRectangleAntiplane computes the stress field 
  !! associated with deforming strain volume using the analytic solution of
  !!
  !!   Lambert, V., and S. Barbot. Contribution of viscoelastic flow in 
  !!   earthquake cycles within the lithosphere‐asthenosphere system. 
  !!   Geophysical Research Letters 43.19 (2016).
  !!
  !! considering the following geometry:
  !!
  !!                              q2,q3
  !!                   +------------@------------+---> E (x2)
  !!                   |                         |
  !!                   |                       w |
  !!                   |                       i |
  !!                   |                       d |
  !!                   |                       t |
  !!                   |                       h |
  !!                   |                         |
  !!                   +-------------------------+
  !!                   :     t h i c k n e s s 
  !!                   :
  !!                   |
  !!                   Z (x3)
  !!
  !!
  !! INPUT:
  !! @param x2, x3         easting, and depth of the observation point
  !!                       in unprimed system of coordinates.
  !! @param q2, q3         east and depth coordinates of the strain volume,
  !! @param T, W           thickness, and width of the strain volume,
  !! @param epsijp         anelastic strain component 12 and 13
  !!                       in the strain volume in the system of reference tied to 
  !!                       the strain volume (primed reference system),
  !! @param G              rigidity.
  !!
  !! OUTPUT:
  !! s12,s13               stress components in the unprimed reference system.
  !!
  !! KNOWN BUGS:
  !! the dip angle is ignored.
  !!
  !! \author Sylvain Barbot (06/09/17) - original form
  !------------------------------------------------------------------------
  SUBROUTINE computeStressRectangleAntiplane(x2,x3,q2,q3,T,W,dip, &
                                eps12p,eps13p,G,s12,s13)

    REAL*8, INTENT(IN) :: x2,x3,q2,q3,T,W,dip
    REAL*8, INTENT(IN) :: eps12p,eps13p
    REAL*8, INTENT(IN) :: G
    REAL*8, INTENT(OUT) :: s12,s13
    
    REAL*8 :: r2

    r2=x2-q2

    s12= G/pi*eps12p*( &
           atan((x3-q3  )/(r2+T/2))-atan((x3-q3  )/(r2-T/2)) &
          +atan((x3-q3-W)/(r2-T/2))-atan((x3-q3-W)/(r2+T/2)) &
          -atan((x3+q3+W)/(r2-T/2))-atan((x3+q3  )/(r2+T/2)) &
          +atan((x3+q3  )/(r2-T/2))+atan((x3+q3+W)/(r2+T/2))) &
       + G/(2*pi)*eps13p*( &
           log((r2-T/2)**2+(x3-q3-W)**2) - log((r2+T/2)**2+(x3-q3-W)**2) &
          +log((r2-T/2)**2+(x3+q3+W)**2) - log((r2+T/2)**2+(x3+q3+W)**2) &
          -log((r2-T/2)**2+(x3-q3)**2)   + log((r2+T/2)**2+(x3-q3)**2) &
          -log((r2-T/2)**2+(x3+q3)**2)   + log((r2+T/2)**2+(x3+q3)**2)) &
       -2*G*eps12p*omega(r2/T)*omega((x3-(2*q3+W)/2._8)/W)

    s13= G/(2*pi)*eps12p*( &
           log((r2-T/2)**2+(x3-q3-W)**2) - log((r2+T/2)**2+(x3-q3-W)**2) &
          -log((r2-T/2)**2+(x3+q3+W)**2) + log((r2+T/2)**2+(x3+q3+W)**2) &
          -log((r2-T/2)**2+(x3-q3)**2)   + log((r2+T/2)**2+(x3-q3)**2) &
          +log((r2-T/2)**2+(x3+q3)**2)   - log((r2+T/2)**2+(x3+q3)**2)) &
       + G/pi*eps13p*( &
           atan((r2+T/2)/(x3-q3))  -atan((r2-T/2)/(x3-q3)) &
          -atan((r2+T/2)/(x3-q3-W))+atan((r2-T/2)/(x3-q3-W)) &
          +atan((r2+T/2)/(x3+q3))  -atan((r2-T/2)/(x3+q3)) &
          -atan((r2+T/2)/(x3+q3+W))+atan((r2-T/2)/(x3+q3+W))) &
       -2*G*eps13p*omega(r2/T)*omega((x3-(2*q3+W)/2)/W)

  END SUBROUTINE computeStressRectangleAntiplane

  !------------------------------------------------------------------------
  !> subroutine computeTractionKernelsStrainVolumeAntiplane
  !! calculates the traction kernels associated with a strain volumes
  !! in antiplane strain using the analytic solution of
  !!
  !!   Lambert, V., and S. Barbot. Contribution of viscoelastic flow in 
  !!   earthquake cycles within the lithosphere‐asthenosphere system.
  !!   Geophysical Research Letters 43.19 (2016).
  !!
  !! INPUT:
  !! @param x2,x3      - coordinates of observation points
  !! @param sv,dv,nv   - strike, dip and normal vector of observation points
  !! @param ns         - number of sources
  !! @param y          - position (NED) of strain volume
  !! @param T,W        - thickness and width of the strain volume
  !! @param dip        - dip angle (radian) of the strain volume
  !! @param e12p
  !!        e13p       - strain components in the primed reference system
  !!                     tied to the strain volume
  !! @param G          - rigidity
  !!
  !! OUTPUT:
  !! ts                - traction in the strike direction.
  !!
  !! \author Sylvain Barbot (06/09/17) - original form
  !------------------------------------------------------------------------
  SUBROUTINE computeTractionKernelsRectangleAntiplane( &
                         x,sv,dv,nv, &
                         ns,y,T,W,dip, &
                         e12p,e13p,G,ts)
    REAL*8, DIMENSION(3), INTENT(IN) :: x
    REAL*8, DIMENSION(3), INTENT(IN) :: sv,dv,nv
    INTEGER, INTENT(IN) :: ns
    REAL*8, DIMENSION(3,ns), INTENT(IN) :: y
    REAL*8, DIMENSION(ns), INTENT(IN) :: T,W,dip
    REAL*8, INTENT(IN) :: e12p,e13p
    REAL*8, INTENT(IN) :: G
    REAL*8, DIMENSION(ns), INTENT(OUT) :: ts

    INTEGER :: i
    REAL*8 :: s12,s13

    DO i=1,ns
       CALL computeStressRectangleAntiplane( &
              x(2),x(3), &
              y(2,i),y(3,i),T(i),W(i),dip(i), &
              e12p,e13p,G,s12,s13)

       ! rotate to receiver system of coordinates
       ts(i)= (           nv(2)*s12+nv(3)*s13 )*sv(1) &
             +( nv(1)*s12                     )*sv(2) &
             +( nv(1)*s13                     )*sv(3)

    END DO

  END SUBROUTINE computeTractionKernelsRectangleAntiplane

  !------------------------------------------------------------------------
  !> subroutine computeStressKernelsRectangleAntiplane
  !! calculates the traction kernels associated with strain in finite
  !! volumes in antiplane strain using the analytic solution of 
  !!
  !!   Lambert, V., and S. Barbot. Contribution of viscoelastic flow in 
  !!   earthquake cycles within the lithosphere‐asthenosphere system.
  !!   Geophysical Research Letters 43.19 (2016).
  !!
  !! INPUT:
  !! @param x2,x3      - coordinates of observation points
  !! @param sv,dv,nv   - strike, dip and normal vector of observation points
  !! @param ns         - number of sources
  !! @param y          - position (NED) of the rectangle volume element
  !! @param thickness  - thickness of the rectangle volume element
  !! @param width      - width of the rectangle volume element
  !! @param dip        - dip angle (radian) of the rectangle volume element
  !! @param e12p, e13p - strain components in the primed reference system
  !!                     tied to the volume element
  !! @param G          - rigidity.
  !!
  !! OUTPUT:
  !! s12,s13           - the stress components in the reference
  !!                     system tied to the volume element.
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeStressKernelsRectangleAntiplane( &
                         p,x,w,sv,dv,nv, &
                         ns,y,thickness,width,dip, &
                         e12p,e13p,G,s12,s13)
    INTEGER, INTENT(IN) :: p
    REAL*8, DIMENSION(3,p), INTENT(IN) :: x
    REAL*8, DIMENSION(p), INTENT(IN) :: w
    REAL*8, DIMENSION(3), INTENT(IN) :: sv,dv,nv
    INTEGER, INTENT(IN) :: ns
    REAL*8, DIMENSION(3,ns), INTENT(IN) :: y
    REAL*8, DIMENSION(ns), INTENT(IN) :: thickness,width,dip
    REAL*8, INTENT(IN) :: e12p,e13p
    REAL*8, INTENT(IN) :: G
    REAL*8, DIMENSION(ns), INTENT(OUT) :: s12,s13

    INTEGER :: i,j
    REAL*8 :: s12p,s13p

    DO i=1,ns
       ! initialize integral
       s12p=0._8
       s13p=0._8

       ! sum over volume
       DO j=1,p
          CALL computeStressRectangleAntiplane( &
                 x(2,j),x(3,j), &
                 y(2,i),y(3,i),thickness(i),width(i),dip(i), &
                 e12p,e13p,G,s12(i),s13(i))

          ! rotate to receiver system of coordinates
          s12p=s12p+w(j)*( (              sv(2)*s12(i)+sv(3)*s13(i) )*nv(1) &
                          +( sv(1)*s12(i)                           )*nv(2) &
                          +( sv(1)*s13(i)                           )*nv(3))
          s13p=s13p+w(j)*(-(              sv(2)*s12(i)+sv(3)*s13(i) )*dv(1) &
                          -( sv(1)*s12(i)                           )*dv(2) &
                          -( sv(1)*s13(i)                           )*dv(3))
       END DO

       ! volume average
       s12(i)=s12p
       s13(i)=s13p

    END DO

  END SUBROUTINE computeStressKernelsRectangleAntiplane

  !------------------------------------------------------------------------
  !> subroutine computeDisplacementFaultCompliantZoneAntiplane
  !! calculates the displacements associated with a dislocation in an
  !! elastic half-space with a compliant zone in antiplane condition.
  !! The dislocation is vertical, centered in the compliant zone.
  !!
  !! INPUT:
  !! @param x2,x3    - coordinates of observation points
  !! @param q3       - coordinates of upper left corner of line dislocation
  !! @param W        - width of the line dislocation
  !! @param T        - thickness of the compliant zone
  !! @param G        - rigidity in the surrounding half-space
  !! @param Gcz      - rigidity in the compliant zone
  !! @param s        - strike slip
  !!
  !! OUTPUT:
  !! u1              - displacement in the strike direction
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu) - 08/10/2019
  !------------------------------------------------------------------------
  SUBROUTINE computeDisplacementFaultCompliantZoneAntiplane( &
                  x2,x3,q3,W,T,G,Gcz,s,u1)
    REAL*8, INTENT(IN) :: x2,x3,q3
    REAL*8, INTENT(IN) :: W,T,G,Gcz
    REAL*8, INTENT(IN) :: s
    REAL*8, INTENT(OUT) :: u1

    INTEGER :: m
    INTEGER, PARAMETER :: truncation=500
    REAL*8 :: K

    ! compliance
    K=(G-Gcz)/(G+Gcz)

    ! initialize displacement
    u1=0d0

    IF (-T/2 .GT. x2) THEN

       ! x2 < -T/2
       u1=ATAN2(x2,x3-q3-W) &
         -ATAN2(x2,x3-q3  ) &
         -ATAN2(x2,x3+q3+W) &
         +ATAN2(x2,x3+q3  )

       DO m=1,truncation
          u1=u1+K**m*(ATAN2((x2-m*T),(x3-q3-W)) &
                     -ATAN2((x2-m*T),(x3-q3  )) &
                     -ATAN2((x2-m*T),(x3+q3+W)) &
                     +ATAN2((x2-m*T),(x3+q3  )))
       END DO

       u1=s*u1*(1-K)/pi/2

    ELSE IF (T/2 .GE. x2) THEN

       ! |x2| <= T/2
       u1=ATAN2(x2,x3-q3-W) &
         -ATAN2(x2,x3-q3  ) &
         -ATAN2(x2,x3+q3+W) &
         +ATAN2(x2,x3+q3  )

       DO m=1,truncation
          u1=u1+K**m*( &
               +ATAN2((x2-m*T),(x3-q3-W)) &
               +ATAN2((x2+m*T),(x3-q3-W)) &
               -ATAN2((x2-m*T),(x3-q3  )) &
               -ATAN2((x2+m*T),(x3-q3  )) &
               -ATAN2((x2-m*T),(x3+q3+W)) &
               -ATAN2((x2+m*T),(x3+q3+W)) &
               +ATAN2((x2-m*T),(x3+q3  )) &
               +ATAN2((x2+m*T),(x3+q3  )))
       END DO

       u1=s*u1/pi/2

    ELSE

       ! x2 > T/2
       u1=ATAN2(x2,x3-q3-W) &
         -ATAN2(x2,x3-q3  ) &
         -ATAN2(x2,x3+q3+W) &
         +ATAN2(x2,x3+q3  )

       DO m=1,truncation
          u1=u1+K**m*( &
                  +ATAN2((x2+m*T),(x3-q3-W)) &
                  -ATAN2((x2+m*T),(x3-q3  )) &
                  -ATAN2((x2+m*T),(x3+q3+W)) &
                  +ATAN2((x2+m*T),(x3+q3  )))
       END DO

       u1=s*u1*(1-K)/pi/2

    END IF

  END SUBROUTINE computeDisplacementFaultCompliantZoneAntiplane

  !------------------------------------------------------------------------
  !> subroutine computeStressFaultCompliantZoneAntiplane
  !! calculates the displacements associated with a dislocation in an
  !! elastic half-space with a compliant zone in antiplane condition.
  !! The dislocation is vertical, centered in the compliant zone.
  !!
  !! INPUT:
  !! @param x2,x3    - coordinates of observation points
  !! @param q3       - coordinates of upper left corner of line dislocation
  !! @param W        - width of the line dislocation
  !! @param T        - thickness of the compliant zone
  !! @param G        - rigidity in the surrounding half-space
  !! @param Gcz      - rigidity in the compliant zone
  !! @param s        - strike slip
  !!
  !! OUTPUT:
  !! s12             - stress in the horizontal direction
  !! s13             - stress in the vertical direction
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu) - 08/10/2019
  !------------------------------------------------------------------------
  SUBROUTINE computeStressFaultCompliantZoneAntiplane( &
                  x2,x3,q3,W,T,G,Gcz,s,s12,s13)
    REAL*8, INTENT(IN) :: x2,x3,q3
    REAL*8, INTENT(IN) :: W,T,G,Gcz
    REAL*8, INTENT(IN) :: s
    REAL*8, INTENT(OUT) :: s12,s13

    INTEGER :: m
    INTEGER, PARAMETER :: truncation=500
    REAL*8 :: K

    ! compliance
    K=(G-Gcz)/(G+Gcz)

    ! initialize stress components
    s12=0d0
    s13=0d0

    ! stress component s13
    IF (-T/2 .GT. x2) THEN

       ! x2 < -T/2
       s12=(x3-q3-W)/(x2**2+(x3-q3-W)**2) &
          -(x3-q3  )/(x2**2+(x3-q3  )**2) &
          -(x3+q3+W)/(x2**2+(x3+q3+W)**2) &
          +(x3+q3  )/(x2**2+(x3+q3  )**2)

       DO m=1,truncation
          s12=s12+K**m*( &
                  +(x3-q3-W)/((x2-m*T)**2+(x3-q3-W)**2) &
                  -(x3-q3  )/((x2-m*T)**2+(x3-q3  )**2) &
                  -(x3+q3+W)/((x2-m*T)**2+(x3+q3+W)**2) &
                  +(x3+q3  )/((x2-m*T)**2+(x3+q3  )**2))
       END DO

       s12=s*G*s12*(1-K)/pi/2

    ELSE IF (T/2 .GE. x2) THEN

       ! |x2| <= T/2
       s12=(x3-q3-W)/(x2**2+(x3-q3-W)**2) &
          -(x3-q3  )/(x2**2+(x3-q3  )**2) &
          -(x3+q3+W)/(x2**2+(x3+q3+W)**2) &
          +(x3+q3  )/(x2**2+(x3+q3  )**2)

       DO m=1,truncation
          s12=s12+K**m*( &
               +(x3-q3-W)/((x2-m*T)**2+(x3-q3-W)**2) &
               +(x3-q3-W)/((x2+m*T)**2+(x3-q3-W)**2) &
               -(x3-q3  )/((x2-m*T)**2+(x3-q3  )**2) &
               -(x3-q3  )/((x2+m*T)**2+(x3-q3  )**2) &
               -(x3+q3+W)/((x2-m*T)**2+(x3+q3+W)**2) &
               -(x3+q3+W)/((x2+m*T)**2+(x3+q3+W)**2) &
               +(x3+q3  )/((x2-m*T)**2+(x3+q3  )**2) &
               +(x3+q3  )/((x2+m*T)**2+(x3+q3  )**2))
       END DO

       s12=s*Gcz*s12/pi/2

    ELSE

       ! x2 > T/2
       s12=(x3-q3-W)/(x2**2+(x3-q3-W)**2) &
          -(x3-q3  )/(x2**2+(x3-q3  )**2) &
          -(x3+q3+W)/(x2**2+(x3+q3+W)**2) &
          +(x3+q3  )/(x2**2+(x3+q3  )**2)

       DO m=1,truncation
          s12=s12+K**m*( &
                  (x3-q3-W)/((x2+m*T)**2+(x3-q3-W)**2) &
                 -(x3-q3  )/((x2+m*T)**2+(x3-q3  )**2) &
                 -(x3+q3+W)/((x2+m*T)**2+(x3+q3+W)**2) &
                 +(x3+q3  )/((x2+m*T)**2+(x3+q3  )**2))
       END DO

       s12=s*G*s12*(1-K)/pi/2

    END IF

    ! stress component s13
    IF (-T/2 .GT. x2) THEN

       ! |x2| <= T/2
       s13=x2/(x2**2+(x3-q3-W)**2) &
          -x2/(x2**2+(x3-q3  )**2) &
          -x2/(x2**2+(x3+q3+W)**2) &
          +x2/(x2**2+(x3+q3  )**2)

       DO m=1,truncation
          s13=s13+K**m*( &
                  (x2-m*T)/((x2-m*T)**2+(x3-q3-W)**2) &
                 -(x2-m*T)/((x2-m*T)**2+(x3-q3  )**2) &
                 -(x2-m*T)/((x2-m*T)**2+(x3+q3+W)**2) &
                 +(x2-m*T)/((x2-m*T)**2+(x3+q3  )**2))
       END DO

       s13=-s*G*s13*(1-K)/pi/2

    ELSE IF (T/2 .GE. x2) THEN

       ! |x2| <= T/2
       s13=x2/(x2**2+(x3-q3-W)**2) &
          -x2/(x2**2+(x3-q3  )**2) &
          -x2/(x2**2+(x3+q3+W)**2) &
          +x2/(x2**2+(x3+q3  )**2)

       DO m=1,truncation
          s13=s13+K**m*( &
               +(x2-m*T)/((x2-m*T)**2+(x3-q3-W)**2) &
               +(x2+m*T)/((x2+m*T)**2+(x3-q3-W)**2) &
               -(x2-m*T)/((x2-m*T)**2+(x3-q3  )**2) &
               -(x2+m*T)/((x2+m*T)**2+(x3-q3  )**2) &
               -(x2-m*T)/((x2-m*T)**2+(x3+q3+W)**2) &
               -(x2+m*T)/((x2+m*T)**2+(x3+q3+W)**2) &
               +(x2-m*T)/((x2-m*T)**2+(x3+q3  )**2) &
               +(x2+m*T)/((x2+m*T)**2+(x3+q3  )**2))
       END DO

       s13=-s*Gcz*s13/pi/2

    ELSE

       ! x2 > T/2
       s13=x2/(x2**2+(x3-q3-W)**2) &
          -x2/(x2**2+(x3-q3  )**2) &
          -x2/(x2**2+(x3+q3+W)**2) &
          +x2/(x2**2+(x3+q3  )**2)

       DO m=1,truncation
          s13=s13+K**m*( &
                  (x2+m*T)/((x2+m*T)**2+(x3-q3-W)**2) &
                 -(x2+m*T)/((x2+m*T)**2+(x3-q3  )**2) &
                 -(x2+m*T)/((x2+m*T)**2+(x3+q3+W)**2) &
                 +(x2+m*T)/((x2+m*T)**2+(x3+q3  )**2))
       END DO

       s13=-s*G*s13*(1-K)/pi/2

    END IF

  END SUBROUTINE computeStressFaultCompliantZoneAntiplane

  !------------------------------------------------------------------------
  !> subroutine computeDisplacementKernelsFaultCompliantZoneAntiplane
  !! calculates the displacement kernels associated with dislocations in
  !! a compliant zone embedded in an elastic half-space in antiplane condition.
  !!
  !! INPUT:
  !! @param x          - coordinates of observation points
  !! @param ns         - number of sources
  !! @param y          - coordinate (NED) of sources
  !! @param W          - width of the line dislocation
  !! @param T          - thickness of the compliant zone
  !! @param Gcz        - rigidity of the compliant zone
  !! @param G          - rigidity of the surrounding medium
  !!
  !! OUTPUT:
  !! u1                - array, displacement in the strike direction.
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeDisplacementKernelsFaultCompliantZoneAntiplane(x, &
                        ns,y,W,T,G,Gcz, &
                        u1)
    REAL*8, DIMENSION(3), INTENT(IN) :: x
    INTEGER, INTENT(IN) :: ns
    REAL*8, DIMENSION(3,ns), INTENT(IN) :: y
    REAL*8, DIMENSION(ns), INTENT(IN) :: W
    REAL*8, INTENT(IN) :: T,G,Gcz
    REAL*8, DIMENSION(ns), INTENT(OUT) :: u1

    INTEGER :: i

    DO i=1,ns
       CALL computeDisplacementFaultCompliantZoneAntiplane( &
                      x(2),x(3), &
                      y(3,i),W(i), &
                      T,G,Gcz, &
                      1._8,u1(i))
    END DO

  END SUBROUTINE computeDisplacementKernelsFaultCompliantZoneAntiplane

  !------------------------------------------------------------------------
  !> subroutine computeTractionKernelsFaultCompliantZoneAntiplane
  !! calculates the traction kernels associated with a dislocations in a 
  !! compliant zone embedded in an elastic half-space in antiplane condition.
  !!
  !! INPUT:
  !! @param x          - coordinates of observation points
  !! @param sv,dv,nv   - strike, dip and normal vector of observation points
  !! @param ns         - number of sources
  !! @param y          - coordinate (NED) of sources
  !! @param W          - width of the line dislocation
  !! @param dip        - dip of the line dislocation
  !! @param s          - strike slip
  !! @param T          - thickness of the compliant zone
  !! @param Gcz        - rigidity of the compliant zone
  !! @param G          - rigidity of the surrounding medium
  !!
  !! OUTPUT:
  !! ts                - traction in the strike direction.
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeTractionKernelsFaultCompliantZoneAntiplane(x,sv,dv,nv, &
                        ns,y,W,dip, &
                        T,G,Gcz, &
                        ts)
    REAL*8, DIMENSION(3), INTENT(IN) :: x,sv,dv,nv
    INTEGER, INTENT(IN) :: ns
    REAL*8, DIMENSION(3,ns), INTENT(IN) :: y
    REAL*8, DIMENSION(ns), INTENT(IN) :: W,dip
    REAL*8, INTENT(IN) :: T,G,Gcz
    REAL*8, DIMENSION(ns), INTENT(OUT) :: ts

    INTEGER :: i
    REAL*8 :: s12,s13

    DO i=1,ns
       CALL computeStressFaultCompliantZoneAntiplane( &
                      x(2),x(3), &
                      y(3,i),W(i), &
                      T,G,Gcz,1._8,s12,s13)

       ! rotate to receiver system of coordinates
       ts(i)= (           nv(2)*s12+nv(3)*s13 )*sv(1) &
             +( nv(1)*s12                     )*sv(2) &
             +( nv(1)*s13                     )*sv(3)

    END DO

  END SUBROUTINE computeTractionKernelsFaultCompliantZoneAntiplane

  !------------------------------------------------------------------------
  !> subroutine computeStressKernelsFaultCompliantZoneAntiplane
  !! calculates the stress kernels associated with a dislocation in a
  !! compliant zone embedded in an elastic half-space in antiplane condition.
  !!
  !! INPUT:
  !! @param x          - coordinates of observation points
  !! @param sv,dv,nv   - strike, dip and normal vector of observation points
  !! @param ns         - number of sources
  !! @param y          - coordinate (NED) of sources
  !! @param W          - width of the dislocation
  !! @param dip        - dip angle of the dislocation
  !! @param s          - strike slip
  !! @param G          - rigidity
  !!
  !! OUTPUT:
  !! s12,s13           - the stress components
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeStressKernelsFaultCompliantZoneAntiplane( &
                        p,x,w,sv,dv,nv, &
                        ns,y,width,dip, &
                        T,G,Gcz,s12,s13)
    INTEGER, INTENT(IN) :: p
    REAL*8, DIMENSION(3,p), INTENT(IN) :: x
    REAL*8, DIMENSION(p), INTENT(IN) :: w
    REAL*8, DIMENSION(3), INTENT(IN) :: sv,dv,nv
    INTEGER, INTENT(IN) :: ns
    REAL*8, DIMENSION(3,ns), INTENT(IN) :: y
    REAL*8, DIMENSION(ns), INTENT(IN) :: width,dip
    REAL*8, INTENT(IN) :: T,G,Gcz
    REAL*8, DIMENSION(ns), INTENT(OUT) :: s12,s13

    INTEGER :: i,j
    REAL*8 :: s12p,s13p

    DO i=1,ns
       ! initialize volume integral
       s12p=0._8
       s13p=0._8

       ! loop over integration points
       DO j=1,p
          CALL computeStressFaultCompliantZoneAntiplane( &
                         x(2,j),x(3,j), &
                         y(3,i),width(i), &
                         T,G,Gcz,1._8,s12(i),s13(i))

          ! rotate to receiver system of coordinates
          s12p=s12p+w(j)*( (              sv(2)*s12(i)+sv(3)*s13(i) )*nv(1) &
                          +( sv(1)*s12(i)                           )*nv(2) &
                          +( sv(1)*s13(i)                           )*nv(3) )
          s13p=s13p+w(j)*(-(              sv(2)*s12(i)+sv(3)*s13(i) )*dv(1) &
                          -( sv(1)*s12(i)                           )*dv(2) &
                          -( sv(1)*s13(i)                           )*dv(3) )

       END DO

       s12(i)=s12p
       s13(i)=s13p

    END DO

  END SUBROUTINE computeStressKernelsFaultCompliantZoneAntiplane

END MODULE antiplane


