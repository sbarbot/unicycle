!-----------------------------------------------------------------------
!> program RateState-Bath simulates evolution of fault slip in condition of 
!! antiplane strain under the radiation damping approximation in
!! non-isothermal conditions, approximating the thermal diffusion with
!! a bath temperature.
!!
!! \mainpage
!! 
!! The time evolution is evaluated numerically using the 4th/5th order
!! Runge-Kutta method with adaptive time steps. The state vector is 
!! as follows:
!!
!!    / P1 1       \   +-----------------------+
!!    | .          |   |                       |
!!    | P1 dPatch  |   |                       |
!!    | .          |   |    nPatch * dPatch    |
!!    | .          |   |                       |
!!    | Pn 1       |   |                       |
!!    | .          |   |                       |
!!    \ Pn dPatch  /   +-----------------------+
!!
!! where nPatch is the number of patches and dPatch is the degrees of 
!! freedom per patch. 
!!
!! For every patch, we have the following items in the state vector
!!
!!   /   ss    \  1
!!   |   ts    |  .
!!   !  theta* |  .
!!   !   v*    |  .
!!   \   T     /  dPatch
!!
!! where ts is the local traction in the strike direction, ss is the slip 
!! in the strike direction, v* is the logarithm of the norm of the 
!! velocity vector (v*=log10(V)), theta* is the logarithm of the state 
!! variable (theta*=log10(theta)) in the rate and state friction framework,
!! and T is the evolving temperature on the fault patch.
!!
!! References:<br>
!!
!!   Barbot S., "Modulation of fault strength during the seismic cycle
!!   by grain-size evolution around contact junctions", Tectonophysics,
!!   765, 129-145, doi:j.tecto.2019.05.004, 2019
!!
!! \author Sylvain Barbot (2019).
!----------------------------------------------------------------------
PROGRAM ratestate_bath

#define BATH 1
#include "macros.h90"

#ifdef NETCDF
  USE exportnetcdf
#endif
  USE antiplane
  USE greens_ap_bath
  USE rk
  USE mpi_f08
  USE types_ap_bath

  IMPLICIT NONE

  REAL*8, PARAMETER :: DEG2RAD = 0.01745329251994329547437168059786927_8

  ! MPI rank and size
  INTEGER :: rank,csize

  ! error flag
  INTEGER :: ierr

#ifdef NETCDF
  ! netcdf files and variable
  TYPE(PROFILE_STRUCT) :: netcdfVelocity,netcdfTemperature,netcdfSlip,netcdfStress
#endif

  CHARACTER(256) :: filename

  ! maximum velocity
  REAL*8 :: vMax,vMaxAll

  ! maximum temperature
  REAL*8 :: tMax,tMaxAll

  ! moment rate
  REAL*8 :: momentRate, momentRateAll

  ! scaling factor
  REAL*8, PARAMETER :: lg10=LOG(1.d1)

  ! state vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: y
  ! rate of change of state vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: dydt,yscal
  ! temporary variables
  REAL*8, DIMENSION(:), ALLOCATABLE :: ytmp,ytmp1,ytmp2,ytmp3

  ! slip velocity vector and strain rate tensor array
  REAL*8, DIMENSION(:), ALLOCATABLE :: v,vAll

  ! vector array (temperature, slip, shear stress)
  REAL*4, DIMENSION(:), ALLOCATABLE :: vector,vectorAll

  ! traction vector and stress tensor array
  REAL*8, DIMENSION(:), ALLOCATABLE :: t

  ! displacement and kinematics vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: d,u,dAll

  ! Green's functions
  REAL*8, DIMENSION(:,:), ALLOCATABLE :: G,O,Of,Ol

  ! time
  REAL*8 :: time,t0
  ! time step
  REAL*8 :: dt_try,dt_next,dt_done
  ! time steps
  INTEGER :: i,j

  ! type of friction law (default)
  INTEGER :: frictionLawType=1

  ! maximum number of time steps (default)
  INTEGER :: maximumIterations=1000000

  ! rate of NETCDF export (default)
  INTEGER :: exportNetcdfRate=20

  ! export temperature to NETCDF
  LOGICAL :: isExportNetcdfTemperature=.FALSE.

  ! export slip to NETCDF
  LOGICAL :: isExportNetcdfSlip=.FALSE.

  ! export slip to NETCDF
  LOGICAL :: isExportNetcdfStress=.FALSE.

  ! layout for parallelism
  TYPE(LAYOUT_STRUCT) :: layout

  ! model parameters
  TYPE(SIMULATION_STRUCT) :: in

  ! initialization
  CALL MPI_INIT(ierr)
  CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
  CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

  ! start time
  time=0.d0

  ! initial tentative time step
  dt_next=1.0d-3

  ! retrieve input parameters from command line
  CALL init(in)
  CALL FLUSH(STDOUT)

  IF (in%isdryrun .AND. 0 .EQ. rank) THEN
     PRINT '("dry run: abort calculation")'
  END IF
  IF (in%isdryrun .OR. in%isversion .OR. in%ishelp) THEN
     CALL MPI_FINALIZE(ierr)
     STOP
  END IF

  ! calculate basis vectors
  CALL initGeometry(in)

  ! describe data layout for parallelism
  CALL initParallelism(in,layout)

  ! calculate the stress interaction matrix
  IF (0 .EQ. rank) THEN
     PRINT '("# computing Green''s functions.")'
  END IF

  CALL buildG(in,layout,G)
  CALL buildO(in,layout,O,Of,Ol)

#ifdef __CUDA__
  CALL cu_init(SIZE(G,1),SIZE(G,2),G)
#endif

#ifdef NETCDF
  IF (in%isexportgreens) THEN
     CALL exportGreensNetcdf(G)
  END IF
#endif

  DEALLOCATE(Of,Ol)
  IF (0 .EQ. rank) THEN
     PRINT 2000
  END IF

  ! velocity vector and strain rate tensor array (t=G*vAll)
  ALLOCATE(u(layout%listVelocityN(1+rank)), &
           v(layout%listVelocityN(1+rank)), &
           vAll(SUM(layout%listVelocityN)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the velocity vector"

  IF (isExportNetcdfTemperature .OR. isExportNetcdfSlip .OR. isExportNetcdfStress) THEN
     ALLOCATE(vector(layout%listElements(1+rank)),STAT=ierr)
     IF (ierr>0) STOP "could not allocate the scalar vector"
     IF (0 .EQ. rank) THEN
        ALLOCATE(vectorAll(SUM(layout%listElements)),STAT=ierr)
        IF (ierr>0) STOP "could not allocate the scalar vector"
     END IF
  END IF

  ! traction vector and stress tensor array (t=G*v)
  ALLOCATE(t(layout%listForceN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the traction and stress vector"

  ! displacement vector (d=O*v)
  ALLOCATE(d   (in%nObservationPoint*DISPLACEMENT_VECTOR_DGF), &
           dAll(in%nObservationPoint*DISPLACEMENT_VECTOR_DGF),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the displacement vector"

  ALLOCATE(y(layout%listStateN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the state vector"

  ALLOCATE(dydt(layout%listStateN(1+rank)), &
           yscal(layout%listStateN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the state vectors"

  ALLOCATE(ytmp (layout%listStateN(1+rank)), &
           ytmp1(layout%listStateN(1+rank)), &
           ytmp2(layout%listStateN(1+rank)), &
           ytmp3(layout%listStateN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the RKQS work space"

  ! allocate buffer from rk module
#ifdef ODE45
  ALLOCATE(buffer(layout%listStateN(1+rank),5),SOURCE=0.0d0,STAT=ierr)
#else
  ALLOCATE(buffer(layout%listStateN(1+rank),3),SOURCE=0.0d0,STAT=ierr)
#endif
  IF (ierr>0) STOP "could not allocate the buffer work space"

  IF (0 .EQ. rank) THEN
     OPEN (UNIT=FPTIME,FILE=in%timeFilename,IOSTAT=ierr,FORM="FORMATTED")
     IF (ierr>0) THEN
        WRITE_DEBUG_INFO(102)
        WRITE (STDERR,'("error: unable to access ",a)') TRIM(in%timefilename)
        STOP 1
     END IF
  END IF

  ! initialize the y vector
  CALL initStateVector(layout%listStateN(1+rank),y,in)

#ifdef NETCDF
  ! initialize netcdf output
  IF (in%isexportnetcdf) THEN
     WRITE (netcdfVelocity%filename,'(a,"/log10v.grd")') TRIM(in%wdir)
     CALL initNetcdf(netcdfVelocity)

     IF (isExportNetcdfTemperature) THEN
        WRITE (netcdfTemperature%filename,'(a,"/temp.grd")') TRIM(in%wdir)
        CALL initNetcdf(netcdfTemperature)
     END IF

     IF (isExportNetcdfSlip) THEN
        WRITE (netcdfSlip%filename,'(a,"/slip.grd")') TRIM(in%wdir)
        CALL initNetcdf(netcdfSlip)
     END IF

     IF (isExportNetcdfStress) THEN
        WRITE (netcdfStress%filename,'(a,"/shear.grd")') TRIM(in%wdir)
        CALL initNetcdf(netcdfStress)
     END IF
  END IF
#endif

  ! gather maximum velocity
  CALL MPI_REDUCE(vMax,vMaxAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

  ! gather maximum temperature
  CALL MPI_REDUCE(tMax,tMaxAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

  ! gather moment rate
  CALL MPI_REDUCE(momentRate,momentRateAll,1,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

  ! initialize output
  IF (0 .EQ. rank) THEN
     WRITE(STDOUT,'("# export to ",a)') TRIM(in%wdir)
     PRINT 2000
     WRITE(STDOUT,'("#       n               time                 dt       vMax       tMax")')
     WRITE(STDOUT,'(I9.9,ES19.12E2,ES19.12E2,ES11.4E2,ES11.4E2)') 0,time,dt_next,vMaxAll,tMaxAll
     WRITE(FPTIME,'("#               time                 dt               vMax         Moment-rate       tMax")')
  END IF

  ! initialize observation patch
  DO j=1,in%nObservationState
     IF ((in%observationState(1,j) .GE.  layout%listOffset(rank+1)) .AND. &
         (in%observationState(1,j) .LT. (layout%listOffset(rank+1)+layout%listElements(rank+1)))) THEN

        in%observationState(3,j)=100+j
        WRITE (filename,'(a,"/patch-",I8.8,"-",I8.8,".dat")') TRIM(in%wdir),j,in%observationState(1,j)
        OPEN (UNIT=in%observationState(3,j), &
              FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
        IF (ierr>0) THEN
           WRITE_DEBUG_INFO(102)
           WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
           STOP 1
        END IF
     END IF
  END DO

  ! initialize observation points
  IF (0 .EQ. rank) THEN
     DO j=1,in%nObservationPoint
        in%observationPoint(j)%file=1000+j
        WRITE (filename,'(a,"/opts-",a,".dat")') TRIM(in%wdir),TRIM(in%observationPoint(j)%name)
        OPEN (UNIT=in%observationPoint(j)%file, &
              FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
        IF (ierr>0) THEN
           WRITE_DEBUG_INFO(102)
           WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
           STOP 1
        END IF
     END DO
  END IF

  ! main loop
#ifdef ODE23
  CALL odefun(layout%listStateN(1+rank),time,y,dydt)
#endif
  DO i=1,maximumIterations

#ifdef ODE45
     CALL odefun(layout%listStateN(1+rank),time,y,dydt)
#endif

     CALL export()
     CALL exportPoints()

#ifdef NETCDF
     IF (in%isexportnetcdf) THEN
        IF (0 .EQ. MOD(i,exportNetcdfRate)) THEN
           IF (0 .EQ. rank) THEN
              CALL exportNetcdfVelocity(netcdfVelocity,in%nPatch)
           END IF
           IF (isExportNetcdfTemperature) THEN
              CALL exportNetcdfTemperature(netcdfTemperature,in%nPatch)
           END IF
           IF (isExportNetcdfSlip) THEN
              CALL exportNetcdfSlip(netcdfSlip,in%nPatch)
           END IF
           IF (isExportNetcdfStress) THEN
              CALL exportNetcdfStress(netcdfStress,in%nPatch)
           END IF
        END IF
     END IF
#endif

     dt_try=dt_next
     yscal(:)=abs(y(:))+abs(dt_try*dydt(:))+TINY

     t0=time
#ifdef ODE45
     CALL rungeKutta(layout%listStateN(1+rank),t0,y,dydt, &
               yscal,ytmp1,ytmp2,ytmp3,dt_try,dt_done,dt_next,odefun,timeStep45)
#else
     CALL rungeKutta(layout%listStateN(1+rank),t0,y,dydt, &
               yscal,ytmp1,ytmp2,ytmp3,dt_try,dt_done,dt_next,odefun,timeStep23)
#endif

     time=time+dt_done

     ! end calculation
     IF (in%interval .LE. time) THEN
        EXIT
     END IF
   
  END DO

  IF (0 .EQ. rank) THEN
     PRINT '(I9.9," time steps.")', i
  END IF

  IF (0 .EQ. rank) THEN
     CLOSE(FPTIME)
  END IF

  ! close observation patch files
  DO j=1,in%nObservationState
     IF ((in%observationState(1,j) .GE. layout%listOffset(rank+1)) .AND. &
         (in%observationState(1,j) .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN
        CLOSE(in%observationState(3,j))
     END IF
  END DO

  ! close the observation points
  IF (0 .EQ. rank) THEN
     DO j=1,in%nObservationPoint
        CLOSE(in%observationPoint(j)%file)
     END DO
  END IF

#ifdef NETCDF
  ! close the netcdf file
  IF (0 .EQ. rank) THEN
     IF (in%isexportnetcdf) THEN
        CALL closeNetcdfUnlimited(netcdfVelocity%ncid,netcdfVelocity%y_varid, &
                                  netcdfVelocity%z_varid,netcdfVelocity%ncCount)
        IF (isExportNetcdfTemperature) THEN
           CALL closeNetcdfUnlimited(netcdfTemperature%ncid,netcdfTemperature%y_varid, &
                                     netcdfTemperature%z_varid,netcdfTemperature%ncCount)
        END IF
        IF (isExportNetcdfSlip) THEN
           CALL closeNetcdfUnlimited(netcdfSlip%ncid,netcdfSlip%y_varid, &
                                     netcdfSlip%z_varid,netcdfSlip%ncCount)
        END IF
        IF (isExportNetcdfStress) THEN
           CALL closeNetcdfUnlimited(netcdfStress%ncid,netcdfStress%y_varid, &
                                     netcdfStress%z_varid,netcdfStress%ncCount)
        END IF
     END IF
  END IF
#endif

  IF (ALLOCATED(in%observationState)) DEALLOCATE(in%observationState)
  IF (ALLOCATED(in%observationPoint)) DEALLOCATE(in%observationPoint)
  DEALLOCATE(y,dydt,yscal)
  DEALLOCATE(ytmp,ytmp1,ytmp2,ytmp3)
  DEALLOCATE(buffer)
  DEALLOCATE(G,v,vAll,t)
  DEALLOCATE(layout%listForceN)
  DEALLOCATE(layout%listVelocityN,layout%listVelocityOffset)
  DEALLOCATE(layout%listStateN,layout%listStateOffset)
  DEALLOCATE(layout%elementStateIndex)
  DEALLOCATE(layout%listElements,layout%listOffset)
  DEALLOCATE(O,d,u,dAll)
  IF (isExportNetcdfTemperature .OR. isExportNetcdfSlip .OR. isExportNetcdfStress) THEN
     DEALLOCATE(vector)
     IF (0 .EQ. rank) DEALLOCATE(vectorAll)
  END IF

#ifdef __CUDA__
  CALL cu_close()
#endif

  CALL MPI_FINALIZE(ierr)

2000 FORMAT ("# ----------------------------------------------------------------------------")
     
CONTAINS

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportGreensNetcdf
  ! initializes the coordinates of netcdf files
  !----------------------------------------------------------------------
  SUBROUTINE exportGreensNetcdf(M)
    REAL*8, DIMENSION(:,:), INTENT(IN) :: M

    REAL*8, DIMENSION(:), ALLOCATABLE :: x,y

    INTEGER :: i,ierr
    CHARACTER(LEN=256) :: filename

    ALLOCATE(x(SIZE(M,1)),y(SIZE(M,2)),STAT=ierr)
    IF (ierr/=0) STOP "could not allocate netcdf coordinate for Greens function"

    ! loop over all patch elements
    DO i=1,SIZE(M,1)
       x(i)=REAL(i,8)
    END DO
    DO i=1,SIZE(M,2)
       y(i)=REAL(i,8)
    END DO

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/greens-",I4.4,".grd")') TRIM(in%greensFunctionDirectory),rank
    CALL writeNetcdf(filename,SIZE(M,1),x,SIZE(M,2),y,M,1)

    DEALLOCATE(x,y)

  END SUBROUTINE exportGreensNetcdf
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine initNetcdf
  ! initializes the coordinates of netcdf files
  !----------------------------------------------------------------------
  SUBROUTINE initNetcdf(netcdf)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: netcdf

    REAL*8, DIMENSION(:), ALLOCATABLE :: x

    INTEGER :: i,ierr

    IF (0 .EQ. rank) THEN

       ! initialize the number of exports
       netcdf%ncCount=0

       ALLOCATE(x(in%nPatch),STAT=ierr)
       IF (ierr/=0) STOP "could not allocate netcdf coordinate"

       ! loop over all patch elements
       DO i=1,in%nPatch
          x(i)=REAL(i,8)
       END DO

       ! netcdf file is compatible with GMT
       CALL openNetcdfUnlimited(netcdf%filename,in%nPatch,x,netcdf%ncid,netcdf%y_varid,netcdf%z_varid)

       DEALLOCATE(x)

    END IF

  END SUBROUTINE initNetcdf
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportNetcdfVelocity
  ! export time series of log10(v)
  !----------------------------------------------------------------------
  SUBROUTINE exportNetcdfVelocity(netcdf,n)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: netcdf
    INTEGER, INTENT(IN) :: n

    REAL*4, DIMENSION(n) :: z

    INTEGER :: j

    ! update the export count
    netcdf%ncCount=netcdf%ncCount+1

    ! loop over all patch elements
    DO j=1,in%nPatch
       ! strike-slip component
       z(j)=REAL(LOG10(in%patch%s(j)%Vl+vAll(DGF_PATCH*(j-1)+1)),4)
    END DO

    CALL writeNetcdfUnlimited(netcdf%ncid,netcdf%y_varid,netcdf%z_varid,netcdf%ncCount,n,z)

    ! flush every so often
    IF (0 .EQ. MOD(netcdf%ncCount,50)) THEN
       CALL flushNetcdfUnlimited(netcdf%ncid,netcdf%y_varid,netcdf%ncCount)
    END IF

  END SUBROUTINE exportNetcdfVelocity
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportNetcdfTemperature
  ! export time series of fault temperature
  !----------------------------------------------------------------------
  SUBROUTINE exportNetcdfTemperature(netcdf,n)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: netcdf
    INTEGER, INTENT(IN) :: n

    INTEGER :: i,j,l

    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       ! element index in state vector
       l=layout%elementStateIndex(i)-in%dPatch+1

       ! temperature
       vector(i)=REAL(y(l+STATE_VECTOR_TEMPERATURE)-273.15d0,4)
    END DO

    ! all threads gather velocity from all threads
    CALL MPI_GATHERV(vector,layout%listElements(1+rank),MPI_REAL4, &
                     vectorAll,layout%listElements,layout%listOffset-1,MPI_REAL4, &
                     0,MPI_COMM_WORLD,ierr)

    ! export to NETCDF
    IF (0 .EQ. rank) THEN
       ! update the export count
       netcdf%ncCount=netcdf%ncCount+1

       CALL writeNetcdfUnlimited(netcdf%ncid,netcdf%y_varid,netcdf%z_varid,netcdf%ncCount,n,vectorAll)

       ! flush every so often
       IF (0 .EQ. MOD(netcdf%ncCount,50)) THEN
          CALL flushNetcdfUnlimited(netcdf%ncid,netcdf%y_varid,netcdf%ncCount)
       END IF

    END IF

  END SUBROUTINE exportNetcdfTemperature
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportNetcdfSlip
  ! export time series of cumulative fault slip
  !----------------------------------------------------------------------
  SUBROUTINE exportNetcdfSlip(netcdf,n)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: netcdf
    INTEGER, INTENT(IN) :: n

    INTEGER :: i,j,l

    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       ! element index in state vector
       l=layout%elementStateIndex(i)-in%dPatch+1

       ! cumulative slip
       vector(i)=REAL(y(l+STATE_VECTOR_SLIP_STRIKE),4)
    END DO

    ! all threads gather velocity from all threads
    CALL MPI_GATHERV(vector,layout%listElements(1+rank),MPI_REAL4, &
                     vectorAll,layout%listElements,layout%listOffset-1,MPI_REAL4, &
                     0,MPI_COMM_WORLD,ierr)

    ! export to NETCDF
    IF (0 .EQ. rank) THEN
       ! update the export count
       netcdf%ncCount=netcdf%ncCount+1

       CALL writeNetcdfUnlimited(netcdf%ncid,netcdf%y_varid,netcdf%z_varid,netcdf%ncCount,n,vectorAll)

       ! flush every so often
       IF (0 .EQ. MOD(netcdf%ncCount,50)) THEN
          CALL flushNetcdfUnlimited(netcdf%ncid,netcdf%y_varid,netcdf%ncCount)
       END IF

    END IF

  END SUBROUTINE exportNetcdfSlip
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportNetcdfStress
  ! export time series of fault stress
  !----------------------------------------------------------------------
  SUBROUTINE exportNetcdfStress(netcdf,n)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: netcdf
    INTEGER, INTENT(IN) :: n

    INTEGER :: i,j,l

    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       ! element index in state vector
       l=layout%elementStateIndex(i)-in%dPatch+1

       ! cumulative slip
       vector(i)=REAL(y(l+STATE_VECTOR_TRACTION_STRIKE),4)
    END DO

    ! all threads gather velocity from all threads
    CALL MPI_GATHERV(vector,layout%listElements(1+rank),MPI_REAL4, &
                     vectorAll,layout%listElements,layout%listOffset-1,MPI_REAL4, &
                     0,MPI_COMM_WORLD,ierr)

    ! export to NETCDF
    IF (0 .EQ. rank) THEN
       ! update the export count
       netcdf%ncCount=netcdf%ncCount+1

       CALL writeNetcdfUnlimited(netcdf%ncid,netcdf%y_varid,netcdf%z_varid,netcdf%ncCount,n,vectorAll)

       ! flush every so often
       IF (0 .EQ. MOD(netcdf%ncCount,50)) THEN
          CALL flushNetcdfUnlimited(netcdf%ncid,netcdf%y_varid,netcdf%ncCount)
       END IF

    END IF

  END SUBROUTINE exportNetcdfStress
#endif

  !-----------------------------------------------------------------------
  !> subroutine exportPoints
  ! export observation points
  !----------------------------------------------------------------------
  SUBROUTINE exportPoints()

    IMPLICIT NONE

    INTEGER :: j,k,l,ierr
    INTEGER :: elementIndex
    CHARACTER(1024) :: formatString
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    !-----------------------------------------------------------------
    ! step 1/4 - gather the kinematics from the state vector
    !-----------------------------------------------------------------

    ! element index in d vector
    k=1
    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO j=1,SIZE(layout%elementIndex)
       elementIndex=layout%elementIndex(j)
       patch=in%patch%s(elementIndex)

       ! strike slip
       u(k)=y(l+STATE_VECTOR_SLIP_STRIKE)

       l=l+in%dPatch

       k=k+layout%elementVelocityDGF(j)
    END DO

    !-----------------------------------------------------------------
    ! step 2/4 - calculate the rate of traction and rate of stress
    !-----------------------------------------------------------------

#ifdef __BLAS__
    ! use the BLAS library to compute the matrix vector product
    CALL DGEMV("T",SIZE(O,1),SIZE(O,2), &
                1._8,O,SIZE(O,1),u,1,0.d0,d,1)
#else
    ! slower, Fortran intrinsic
    d=MATMUL(TRANSPOSE(O),u)
#endif

    !-----------------------------------------------------------------
    ! step 3/4 - master thread adds the contribution of all elemets
    !-----------------------------------------------------------------

    CALL MPI_REDUCE(d,dAll,in%nObservationPoint*DISPLACEMENT_VECTOR_DGF, &
                    MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

    !-----------------------------------------------------------------
    ! step 4/4 - master thread writes to disk
    !-----------------------------------------------------------------

    IF (0 .EQ. rank) THEN
       formatString="(ES19.12E2"
       DO j=1,DISPLACEMENT_VECTOR_DGF
          formatString=TRIM(formatString)//",X,ES19.12E2"
       END DO
       formatString=TRIM(formatString)//")"

       ! element index in d vector
       k=1
       DO j=1,in%nObservationPoint
          IF (0 .EQ. MOD(i-1,in%observationPoint(j)%rate)) THEN
             WRITE (in%observationPoint(j)%file,TRIM(formatString)) time,dAll(k:k+DISPLACEMENT_VECTOR_DGF-1)
          END IF
          k=k+DISPLACEMENT_VECTOR_DGF
       END DO
    END IF

  END SUBROUTINE exportPoints

  !-----------------------------------------------------------------------
  !> subroutine export
  ! write the state variables of elements, either patch or volume, and 
  ! other information.
  !----------------------------------------------------------------------
  SUBROUTINE export()

    IMPLICIT NONE

    ! degrees of freedom
    INTEGER :: dgf

    ! counters
    INTEGER :: j,k

    ! index in state vector
    INTEGER :: index

    ! format string
    CHARACTER(1024) :: formatString

    ! gather maximum velocity
    CALL MPI_REDUCE(vMax,vMaxAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

    ! gather maximum temperature
    CALL MPI_REDUCE(tMax,tMaxAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

    ! gather moment-rate
    CALL MPI_REDUCE(momentRate,momentRateAll,1,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

    ! export observation state
    DO j=1,in%nObservationState
       IF ((in%observationState(1,j) .GE. layout%listOffset(rank+1)) .AND. &
           (in%observationState(1,j) .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN

          ! check observation state sampling rate
          IF (0 .EQ. MOD(i-1,in%observationState(2,j))) THEN
             dgf=in%dPatch

             formatString="(ES21.14E2"
             DO k=1,dgf
                formatString=TRIM(formatString)//",X,ES20.12E3,X,ES20.12E3"
             END DO
             formatString=TRIM(formatString)//")"

             index=layout%elementStateIndex(in%observationState(1,j)-layout%listOffset(rank+1)+1)-dgf
             WRITE (in%observationState(3,j),TRIM(formatString)) time, &
                       y(index+1:index+dgf), &
                    dydt(index+1:index+dgf)
          END IF
       END IF
    END DO

    IF (0 .EQ. rank) THEN
       WRITE(FPTIME,'(ES20.14E2,ES19.12E2,ES19.12E2,ES20.12E2,ES11.4E2)') time,dt_done,vMaxAll,momentRateAll,tMaxAll
       IF (0 .EQ. MOD(i,50)) THEN
          WRITE(STDOUT,'(I9.9,ES19.12E2,ES19.12E2,ES11.4E2,ES11.4E2)') i,time,dt_done,vMaxAll,tMaxAll
          CALL FLUSH(STDOUT)
       END IF
    END IF

  END SUBROUTINE export

  !-----------------------------------------------------------------------
  !> subroutine initStateVector
  ! initialize the state vector
  !
  ! INPUT:
  ! @param n - number of state elements own by current thread
  ! @param y - the state vector (segment owned by currect thread)
  !
  !----------------------------------------------------------------------
  SUBROUTINE initStateVector(n,y,in)

    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: n
    REAL*8, INTENT(OUT)    :: y(n)
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in

    INTEGER :: i,l
    INTEGER :: elementIndex
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    ! initial traction (Pa)
    REAL*8 :: tau

    ! initial state variable (s)
    REAL*8 :: Tinit

    ! initial velocity (m/s)
    REAL*8 :: Vinit

    ! zero out state vector
    y=0._8

    ! maximum velocity
    vMax=0._8

    ! maximum temperature
    tMax=0._8

    ! moment-rate
    momentRate=0._8

    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementIndex=layout%elementIndex(i)
       patch=in%patch%s(elementIndex)

       ! traction in strike direction
       IF (0 .GT. patch%tau0) THEN

          ! set initial velocity to local loading rate
          Vinit = 0.98d0*patch%Vl

          ! set traction compatible with velocity
          SELECT CASE(frictionLawType)
          CASE(1)
             ! multiplicative form of rate-state friction (Barbot, 2019)
             tau = patch%mu0*patch%sig*exp((patch%a-patch%b)/patch%mu0*log(Vinit/patch%Vo))
          CASE(2:3)
             ! additive form of rate-state friction (Chester, 1994; Barbot, 2019)
             tau = patch%sig*(patch%mu0+(patch%a-patch%b)*log(Vinit/patch%Vo))
          CASE DEFAULT
             WRITE (0,'("unhandled option ", a, " (this is a bug)")') frictionLawType
             WRITE_DEBUG_INFO(100)
             STOP 3
          END SELECT
       ELSE
          ! set traction from input file
          tau = patch%tau0

          ! set initial velocity to local loading rate
          Vinit = patch%Vl
       END IF

       ! set state variable log10(theta) compatible with velocity and traction
       SELECT CASE(frictionLawType)
       CASE(1)
          ! multiplicative form of rate-state friction (Barbot, 2019)
          Tinit=(LOG(patch%L/patch%Vo)+(patch%a*LOG(patch%Vo/patch%Vl)+patch%mu0*LOG(tau/patch%mu0/patch%sig))/patch%b)/lg10
       CASE(2)
          ! additive form of rate-state friction (Ruina, 1983)
          Tinit=(LOG(patch%L/patch%Vo)+(patch%a*LOG(patch%Vo/patch%Vl)+tau/patch%sig-patch%mu0)/patch%b)/lg10
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug)")') frictionLawType
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT

       ! strike slip
       y(l+STATE_VECTOR_SLIP_STRIKE) = 0._8

       ! initial traction
       y(l+STATE_VECTOR_TRACTION_STRIKE) = tau

       ! log10(state variable)
       y(l+STATE_VECTOR_STATE_1) = Tinit

       ! maximum velocity
       vMax=MAX(Vinit,vMax)

       ! moment-rate
       momentRate=momentRate+(Vinit-patch%Vl)*in%mu*in%patch%width(elementIndex)

       ! slip velocity log10(V)
       y(l+STATE_VECTOR_VELOCITY) = log(Vinit)/lg10

       ! initial temperature
       y(l+STATE_VECTOR_TEMPERATURE) = patch%To

       ! maximum temperature
       tMax=MAX(patch%To,tMax)

       l=l+in%dPatch
    END DO

  END SUBROUTINE initStateVector

  !-----------------------------------------------------------------------
  !> subroutine odefun
  ! evalutes the derivative of the state vector
  !
  ! @param n - number of state elements own by current thread
  ! @param m - degrees of freedom
  !
  ! DESCRIPTION:
  !   1- extract slip velocity and strain rate from state vector
  !   2- calculate the rate of traction and rate of stress
  !   3- calculate the rate of remaining state variables
  !----------------------------------------------------------------------
  SUBROUTINE odefun(n,time,y,dydt)

    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: n
    REAL*8, INTENT(IN)    :: time
    REAL*8, INTENT(IN)    :: y(n)
    REAL*8, INTENT(INOUT) :: dydt(n)

    INTEGER :: i,j,k,l,ierr
    INTEGER :: elementIndex
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    ! scalar rate of shear traction
    REAL*8 :: dtau

    ! slip velocity in the strike direction
    REAL*8 :: velocity

    ! modifier for the arcsinh form of rate-state friction
    REAL*8 :: reg

    ! zero out rate of state
    dydt=0._8

    ! maximum velocity
    vMax=0._8

    ! maximum temperature
    tMax=0._8

    ! initialize moment-rate
    momentRate=0._8

    !--------------------------------------------------------------------
    ! step 1/3 - extract slip velocity and strain rate from state vector
    !--------------------------------------------------------------------

    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementIndex=layout%elementIndex(i)

       ! element index in state vector
       l=layout%elementStateIndex(i)-in%dPatch+1

       ! element index in v vector
       k=layout%elementVelocityIndex(i)-DGF_PATCH+1

       patch=in%patch%s(elementIndex)

       ! slip velocity
       velocity=EXP(y(l+STATE_VECTOR_VELOCITY)*lg10)

       ! maximum velocity
       vMax=MAX(velocity,vMax)

       ! maximum temperature
       tMax=MAX(y(l+STATE_VECTOR_TEMPERATURE),tMax)

       ! update state vector (rate of slip components)
       dydt(l+STATE_VECTOR_SLIP_STRIKE)=velocity

       v(k)=velocity-patch%Vl
    END DO

    ! all threads gather velocity from all threads
    CALL MPI_ALLGATHERV(v,layout%listVelocityN(1+rank),MPI_REAL8, &
                        vAll,layout%listVelocityN,layout%listVelocityOffset,MPI_REAL8, &
                        MPI_COMM_WORLD,ierr)

    !-----------------------------------------------------------------
    ! step 2/3 - calculate the rate of traction and rate of stress
    !-----------------------------------------------------------------

#ifdef __BLAS__
    ! use the BLAS library to compute the matrix vector product
    CALL DGEMV("T",SIZE(G,1),SIZE(G,2), &
                1._8,G,SIZE(G,1),vAll,1,0.d0,t,1)
#else
    ! slower, Fortran intrinsic
    t=MATMUL(TRANSPOSE(G),vAll)
#endif

    !-----------------------------------------------------------------
    ! step 3/3 - calculate the rate of remaining state variables
    !-----------------------------------------------------------------

    ! element index in t vector
    j=1
    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementIndex=layout%elementIndex(i)

       patch=in%patch%s(elementIndex)

       ! element index in state vector
       l=layout%elementStateIndex(i)-in%dPatch+1

       ! slip velocity
       velocity=EXP(y(l+STATE_VECTOR_VELOCITY)*lg10)

       ! moment-rate
       momentRate=momentRate+(velocity-patch%Vl)*in%mu*in%patch%width(elementIndex)

       ! rate of state
       SELECT CASE(frictionLawType)
       CASE(1:2)
          ! equivalent of aging law in isothermal condition (Barbot, 2019)
          dydt(l+STATE_VECTOR_STATE_1)=( &
                  EXP(-patch%H/in%R*(1.d0/y(l+STATE_VECTOR_TEMPERATURE)-1.d0/patch%To) &
                      -y(l+STATE_VECTOR_STATE_1)*lg10)-velocity/patch%L)/lg10
       CASE(3)
          ! equivalent of slip law in isothermal condition (Chester, 1994) (TODO)
          dydt(l+STATE_VECTOR_STATE_1)=( &
                  EXP(-patch%H/in%R*(1.d0/y(l+STATE_VECTOR_TEMPERATURE)-1.d0/patch%To) &
                      -y(l+STATE_VECTOR_STATE_1)*lg10)-velocity/patch%L)/lg10
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug)")') frictionLawType
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT

       ! rate of temperature
       dydt(l+STATE_VECTOR_TEMPERATURE)= &
               -patch%DW2*(y(l+STATE_VECTOR_TEMPERATURE)-patch%Tb) &
               +y(l+STATE_VECTOR_TRACTION_STRIKE)*velocity/patch%wRhoC

       ! scalar rate of shear traction
       dtau=t(j+TRACTION_VECTOR_STRIKE)
       
       ! acceleration
       SELECT CASE(frictionLawType)
       CASE(1)
          ! multiplicative form of rate-state friction (Barbot, 2019)
          dydt(l+STATE_VECTOR_VELOCITY)= &
                  (dtau + ( patch%a*patch%Q/in%R*dydt(l+STATE_VECTOR_TEMPERATURE)/y(l+STATE_VECTOR_TEMPERATURE)**2 &
                           -patch%b*dydt(l+STATE_VECTOR_STATE_1)*lg10)*y(l+STATE_VECTOR_TRACTION_STRIKE)/patch%mu0) / &
                  (patch%a*y(l+STATE_VECTOR_TRACTION_STRIKE)/patch%mu0+patch%damping*velocity) / lg10
       CASE(2:3)
          ! additive form of rate-state friction (Chester, 1994; Barbot, 2019)
          dydt(l+STATE_VECTOR_VELOCITY)= &
                  (dtau-patch%b*patch%sig*dydt(l+STATE_VECTOR_STATE_1)*lg10 &
                       +patch%a*patch%sig*patch%Q/in%R*dydt(l+STATE_VECTOR_TEMPERATURE)/(y(l+STATE_VECTOR_TEMPERATURE)**2)) / &
                  (patch%a*patch%sig+patch%damping*velocity) / lg10
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug)")') frictionLawType
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT

       ! return the traction rate
       dydt(l+STATE_VECTOR_TRACTION_STRIKE)=dtau-patch%damping*velocity*dydt(l+STATE_VECTOR_VELOCITY)*lg10

       l=l+in%dPatch

       j=j+layout%elementForceDGF(i)
    END DO

  END SUBROUTINE odefun

  !-----------------------------------------------------------------------
  !> subroutine initParallelism()
  !! initialize variables describe the data layout
  !! for parallelism.
  !!
  !! OUTPUT:
  !! layout    - list of receiver type and type index
  !-----------------------------------------------------------------------
  SUBROUTINE initParallelism(in,layout)

    IMPLICIT NONE

    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in
    TYPE(LAYOUT_STRUCT), INTENT(OUT) :: layout

    INTEGER :: i,j,k,remainder
    INTEGER :: cumulativeIndex,cumulativeVelocityIndex
    INTEGER :: rank,csize,ierr
    INTEGER :: nElements

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

    ! list of number of elements in thread
    ALLOCATE(layout%listElements(csize), &
             layout%listOffset(csize), STAT=ierr)
    IF (ierr>0) STOP "could not allocate the list"

    ! total number of elements
    nElements=in%patch%ns+in%rectangle%ns

    remainder=nElements-INT(nElements/csize)*csize
    IF (0 .LT. remainder) THEN
       layout%listElements(1:(csize-remainder))      =INT(nElements/csize)
       layout%listElements((csize-remainder+1):csize)=INT(nElements/csize)+1
    ELSE
       layout%listElements(1:csize)=INT(nElements/csize)
    END IF

    ! element start index in thread
    j=0
    k=0
    DO i=1,csize
       j=k+1
       k=k+layout%listElements(i)
       layout%listOffset(i)=j
    END DO

    ALLOCATE(layout%elementType         (layout%listElements(1+rank)), &
             layout%elementIndex        (layout%listElements(1+rank)), &
             layout%elementStateIndex   (layout%listElements(1+rank)), &
             layout%elementVelocityDGF  (layout%listElements(1+rank)), &
             layout%elementVelocityIndex(layout%listElements(1+rank)), &
             layout%elementStateDGF     (layout%listElements(1+rank)), &
             layout%elementForceDGF     (layout%listElements(1+rank)), STAT=ierr)
    IF (ierr>0) STOP "could not allocate the layout elements"

    j=1
    cumulativeIndex=0
    cumulativeVelocityIndex=0
    DO i=1,in%patch%ns
       IF ((i .GE. layout%listOffset(1+rank)) .AND. &
           (i .LT. (layout%listOffset(1+rank)+layout%listElements(1+rank)))) THEN
          layout%elementType(j)=FLAG_PATCH
          layout%elementIndex(j)=i
          layout%elementStateIndex(j)=cumulativeIndex+STATE_VECTOR_DGF_PATCH
          cumulativeIndex=layout%elementStateIndex(j)
          layout%elementVelocityDGF(j)=DGF_PATCH
          layout%elementVelocityIndex(j)=cumulativeVelocityIndex+DGF_PATCH
          cumulativeVelocityIndex=layout%elementVelocityIndex(j)
          layout%elementStateDGF(j)=in%dPatch
          layout%elementForceDGF(j)=DGF_VECTOR
          j=j+1
       END IF
    END DO

    ! for MPI_ALLGATHERV
    ALLOCATE(layout%listVelocityN(csize), &
             layout%listVelocityOffset(csize), &
             layout%listStateN(csize), &
             layout%listStateOffset(csize), &
             layout%listForceN(csize), &
             STAT=ierr)
    IF (ierr>0) STOP "could not allocate the size list"

    ! share number of elements in threads
    CALL MPI_ALLGATHER(SUM(layout%elementVelocityDGF),1,MPI_INTEGER,layout%listVelocityN,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
    CALL MPI_ALLGATHER(SUM(layout%elementStateDGF),   1,MPI_INTEGER,layout%listStateN,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
    CALL MPI_ALLGATHER(SUM(layout%elementForceDGF),   1,MPI_INTEGER,layout%listForceN,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)

    j=0
    k=0
    DO i=1,csize
       j=k+1
       k=k+layout%listVelocityN(i)
       layout%listVelocityOffset(i)=j-1
    END DO

    j=0
    k=0
    DO i=1,csize
       j=k+1
       k=k+layout%listStateN(i)
       layout%listStateOffset(i)=j-1
    END DO

  END SUBROUTINE initParallelism

  !-----------------------------------------------------------------------
  !> subroutine initGeometry
  ! initializes the position and local reference system vectors
  !
  ! INPUT:
  ! @param in      - input parameters data structure
  !-----------------------------------------------------------------------
  SUBROUTINE initGeometry(in)
    USE antiplane
    USE types_ap_bath

    IMPLICIT NONE

    TYPE(SIMULATION_STRUCT), INTENT(INOUT) :: in
  
    IF (0 .LT. in%patch%ns) THEN
       CALL computeReferenceSystemAntiplane( &
                in%patch%ns, &
                in%patch%x, &
                in%patch%width, &
                in%patch%dip, &
                in%patch%sv, &
                in%patch%dv, &
                in%patch%nv, &
                in%patch%xc)
    END IF

  END SUBROUTINE initGeometry

  !---------------------------------------------------------------------
  !> subroutine init
  !! reads simulation parameters from the standard input and initialize
  !! model parameters.
  !!
  !! INPUT:
  !! @param unit - the unit number used to read input data
  !!
  !! OUTPUT:
  !! @param in
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !---------------------------------------------------------------------
  SUBROUTINE init(in)
    USE types_ap_bath
    USE getopt_m
  
    IMPLICIT NONE

    TYPE(SIMULATION_STRUCT), INTENT(OUT) :: in
  
    CHARACTER :: ch
    CHARACTER(256) :: dataline,filename
    INTEGER :: iunit,noptions
    TYPE(OPTION_S) :: opts(13)
  
    INTEGER :: k,ierr,i,rank,size,position
    INTEGER, PARAMETER :: psize=512
    INTEGER :: dummy
    CHARACTER, DIMENSION(psize) :: packed

    REAL*8 :: d_diff,w_diff,w_heat,rhoc
  
    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,size,ierr)
  
    ! define long options, such as --dry-run
    ! parse the command line for options
    opts( 1)=OPTION_S("version",.FALSE.,CHAR(21))
    opts( 2)=OPTION_S("dry-run",.FALSE.,CHAR(22))
    opts( 3)=OPTION_S("epsilon",.TRUE.,'e')
    opts( 4)=OPTION_S("export-greens",.TRUE.,'g')
    opts( 5)=OPTION_S("export-netcdf",.FALSE.,'n')
    opts( 6)=OPTION_S("export-netcdf-rate",.TRUE.,'r')
    opts( 7)=OPTION_S("export-netcdf-temperature",.FALSE.,'t')
    opts( 8)=OPTION_S("export-netcdf-slip",.FALSE.,'s')
    opts( 9)=OPTION_S("export-netcdf-stress",.FALSE.,'u')
    opts(10)=OPTION_S("friction-law",.TRUE.,'f')
    opts(11)=OPTION_S("maximum-step",.TRUE.,'m')
    opts(12)=OPTION_S("maximum-iterations",.TRUE.,'i')
    opts(13)=OPTION_S("help",.FALSE.,'h')
  
    noptions=0
    DO
       ch=getopt("he:g:i:m:nr:stu",opts)
       SELECT CASE(ch)
       CASE(CHAR(0))
          EXIT
       CASE(CHAR(21))
          ! option version
          in%isversion=.TRUE.
       CASE(CHAR(22))
          ! option dry-run
          in%isdryrun=.TRUE.
       CASE('e')
          ! numerical accuracy (variable epsilon sits in the rk module)
          READ(optarg,*) epsilon
          noptions=noptions+1
       CASE('f')
          ! type of friction law
          READ(optarg,*) frictionLawType
          noptions=noptions+1
       CASE('g')
          ! export Greens functions to netcdf file
          READ(optarg,'(a)') in%greensFunctionDirectory
          in%isexportgreens=.TRUE.
          noptions=noptions+1
       CASE('i')
          ! maximum number of iterations
          READ(optarg,*) maximumIterations
          noptions=noptions+1
       CASE('m')
          ! maximum time step (variable maximumTimeStep sits in the rk module)
          READ(optarg,*) maximumTimeStep
          noptions=noptions+1
       CASE('n')
          ! export in netcdf
          in%isexportnetcdf=.TRUE.
       CASE('r')
          ! rate of netcdf export
          READ(optarg,*) exportNetcdfRate
          noptions=noptions+1
       CASE('s')
          ! export slip to netcdf
          isExportNetcdfSlip=.TRUE.
       CASE('u')
          ! export slip to netcdf
          isExportNetcdfStress=.TRUE.
       CASE('t')
          ! export temperature to netcdf
          isExportNetcdfTemperature=.TRUE.
       CASE('h')
          ! option help
          in%ishelp=.TRUE.
       CASE('?')
          WRITE_DEBUG_INFO(100)
          in%ishelp=.TRUE.
          EXIT
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug)")') optopt
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT
       noptions=noptions+1
    END DO
  
    IF (in%isversion) THEN
       CALL printversion()
       ! abort parameter input
       STOP
    END IF
  
    IF (in%ishelp) THEN
       CALL printhelp()
       ! abort parameter input
       STOP
    END IF
  
    in%nPatch=0
    ! minimum number of dynamic variables for patches
    in%dPatch=STATE_VECTOR_DGF_PATCH
    in%patch%ns=0
    in%rectangle%ns=0
  
    IF (0 .EQ. rank) THEN
       PRINT 2000
       PRINT '("# RATESTATE-BATH")'
       PRINT '("#")'
       PRINT '("# quasi-dynamic rupture simulation in antiplane")'
       PRINT '("# strain, non-isothermal conditions")'
       PRINT '("#")'
       SELECT CASE(frictionLawType)
       CASE(1)
          PRINT '("# friction law: multiplicative form with thermally activated aging law (Barbot, 2019)")'
       CASE(2)
          PRINT '("# friction law: additive form with thermally activated aging law (Barbot, 2019)")'
       CASE(3)
          PRINT '("# friction law: additive form with thermally activated slip law (Chester, 1994)")'
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug)")') frictionLawType
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT
       PRINT '("# numerical accuracy: ",ES11.4)', epsilon
       PRINT '("# maximum iterations: ",I11)', maximumIterations
       PRINT '("# maximum time step: ",ES12.4)', maximumTimeStep
       PRINT '("# number of threads:  ",I11)', csize
       IF (in%isexportnetcdf) THEN
          PRINT '("# export velocity to netcdf:  yes")'
       ELSE
          PRINT '("# export velocity to netcdf:   no")'
       END IF
       IF (in%isexportgreens) THEN
          PRINT '("# export greens function:     yes")'
       END IF
       PRINT 2000
  
       IF (noptions .LT. COMMAND_ARGUMENT_COUNT()) THEN
          ! read from input file
          iunit=25
          CALL GET_COMMAND_ARGUMENT(noptions+1,filename)
          OPEN (UNIT=iunit,FILE=filename,IOSTAT=ierr)
       ELSE
          ! get input parameters from standard input
          iunit=5
       END IF
  
       PRINT '("# output directory")'
       CALL getdata(iunit,dataline)
       READ (dataline,'(a)') in%wdir
       PRINT '(2X,a)', TRIM(in%wdir)
  
       in%timeFilename=TRIM(in%wdir)//"/time.dat"
  
       ! test write permissions on output directory
       OPEN (UNIT=FPTIME,FILE=in%timeFilename,POSITION="APPEND",&
               IOSTAT=ierr,FORM="FORMATTED")
       IF (ierr>0) THEN
          WRITE_DEBUG_INFO(102)
          WRITE (STDERR,'("error: unable to access ",a)') TRIM(in%timefilename)
          STOP 1
       END IF
       CLOSE(FPTIME)
     
       PRINT '("# shear modulus, universal gas constant")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%mu, in%R
       PRINT '(2ES9.2E1)', in%mu, in%R
  
       IF (0 .GT. in%mu) THEN
          WRITE_DEBUG_INFO(-1)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("input error: shear modulus must be positive")')
          STOP 2
       END IF
  
       IF (0 .GE. in%R) THEN
          WRITE_DEBUG_INFO(200)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("error in input file: R must be positive.")')
          STOP 1
       END IF

       PRINT '("# time interval")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%interval
       PRINT '(ES20.12E2)', in%interval
  
       IF (in%interval .LE. 0._8) THEN
          WRITE (STDERR,'("**** error **** ")')
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("simulation time must be positive. exiting.")')
          STOP 1
       END IF
  
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !                   P A T C H E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT '("# number of patches")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%patch%ns
       PRINT '(I5)', in%patch%ns
       IF (in%patch%ns .GT. 0) THEN
          ALLOCATE(in%patch%s(in%patch%ns), &
                   in%patch%x(3,in%patch%ns), &
                   in%patch%xc(3,in%patch%ns), &
                   in%patch%width(in%patch%ns), &
                   in%patch%dip(in%patch%ns), &
                   in%patch%sv(3,in%patch%ns), &
                   in%patch%dv(3,in%patch%ns), &
                   in%patch%nv(3,in%patch%ns),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the patch list"
          PRINT 2000
          PRINT '("#  n       Vl       x2       x3    width      dip")'
          PRINT 2000
          DO k=1,in%patch%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                  in%patch%s(k)%Vl, &
                  in%patch%x(2,k), &
                  in%patch%x(3,k), &
                  in%patch%width(k), &
                  in%patch%dip(k)
   
             PRINT '(I4.4,2ES9.2E1,ES9.3E1,ES9.3E1,ES9.2E1)',i, &
                  in%patch%s(k)%Vl, &
                  in%patch%x(2,k), &
                  in%patch%x(3,k), &
                  in%patch%width(k), &
                  in%patch%dip(k)
                
             ! convert to radians
             in%patch%dip(k)=in%patch%dip(k)*DEG2RAD     

             IF (i .ne. k) THEN
                WRITE (STDERR,'("invalid patch definition")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
             IF (in%patch%width(k) .LE. 0._8) THEN
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: patch width must be positive.")')
                STOP 1
             END IF
                
          END DO
   
          ! export the fault patches
          filename=TRIM(in%wdir)//"/rfaults.flt.2d"
          OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          WRITE (FPOUT,'("#  n        Vl            x2            x3   width     dip")')
          DO k=1,in%patch%ns
             WRITE (FPOUT,'(I4,ES10.2E2,2ES14.7E1,2ES8.2E1)') k, &
               in%patch%s(k)%Vl, &
               in%patch%x(2,k), &
               in%patch%x(3,k), &
               in%patch%width(k), &
               in%patch%dip(k)
          END DO
          CLOSE(FPOUT)
                
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          !        F R I C T I O N   P R O P E R T I E S
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          PRINT 2000
          PRINT '("# number of frictional patches")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) dummy
          PRINT '(I5)', dummy
          IF (dummy .NE. in%patch%ns) THEN
             WRITE_DEBUG_INFO(-1)
             WRITE(STDERR,'("input error: all patches require frictional properties")')
             STOP 2
          END IF
          PRINT '("#  n     tau0      mu0      sig        a        b        L       Vo  G/(2Vs)")'
          PRINT 2000
          DO k=1,in%patch%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                   in%patch%s(k)%tau0, &
                   in%patch%s(k)%mu0, &
                   in%patch%s(k)%sig, &
                   in%patch%s(k)%a, &
                   in%patch%s(k)%b, &
                   in%patch%s(k)%L, &
                   in%patch%s(k)%Vo, &
                   in%patch%s(k)%damping
   
            PRINT '(I4.4,8ES9.2E1)',i, &
                   in%patch%s(k)%tau0, &
                   in%patch%s(k)%mu0, &
                   in%patch%s(k)%sig, &
                   in%patch%s(k)%a, &
                   in%patch%s(k)%b, &
                   in%patch%s(k)%L, &
                   in%patch%s(k)%Vo, &
                   in%patch%s(k)%damping
                
             IF (i .ne. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'("invalid friction property definition for patch")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO
   
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          !   F A U L T   T H E R M A L   P R O P E R T I E S
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          PRINT 2000
          PRINT '("# number of thermal properties")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) dummy
          PRINT '(I5)', dummy
          IF (dummy .NE. in%patch%ns) THEN
             WRITE_DEBUG_INFO(-1)
             WRITE(STDERR,'("input error: all patches require frictional properties")')
             STOP 2
          END IF
          PRINT '("#  n        Q        H        D        W        w     rhoc       Tb")'
          PRINT 2000
          DO k=1,in%patch%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                   in%patch%s(k)%Q, &
                   in%patch%s(k)%H, &
                   d_diff,w_diff,w_heat,rhoc, &
                   in%patch%s(k)%Tb

             PRINT '(I4.4,7ES9.3E1)',i, &
                   in%patch%s(k)%Q, &
                   in%patch%s(k)%H, &
                   d_diff,w_diff,w_heat,rhoc, &
                   in%patch%s(k)%Tb
                
             ! lumped parameters
             in%patch%s(k)%wRhoC=w_heat*rhoc
             in%patch%s(k)%DW2=d_diff/w_diff**2

             ! steady-state temperature
             in%patch%s(k)%To=in%patch%s(k)%Tb+in%patch%s(k)%mu0*in%patch%s(k)%sig*in%patch%s(k)%Vl &
                     /in%patch%s(k)%wRhoC/in%patch%s(k)%DW2
   
             IF (i .ne. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'("invalid thermal friction property for patch")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO
   
       END IF
          
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !       O B S E R V A T I O N   P A T C H E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of observation patches")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%nObservationState
       PRINT '(I5)', in%nObservationState
       IF (0 .LT. in%nObservationState) THEN
          ALLOCATE(in%observationState(3,in%nObservationState),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the observation patches"
          PRINT 2000
          PRINT '("# n      i rate")'
          PRINT 2000
          DO k=1,in%nObservationState
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i,in%observationState(1:2,k)

             PRINT '(I3.3,X,I6,X,I4)',i,in%observationState(1:2,k)

             IF (0 .GE. in%observationState(2,k)) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: sampling rate must be a strictly positive integer")')
                STOP 1
             END IF

             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO
       END IF

       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !        O B S E R V A T I O N   P O I N T S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of observation points")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%nObservationPoint
       PRINT '(I5)', in%nObservationPoint
       IF (0 .LT. in%nObservationPoint) THEN
          ALLOCATE(in%observationPoint(in%nObservationPoint),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the observation points"
          PRINT 2000
          PRINT '("# n name       x2       x3 rate")'
          PRINT 2000
          DO k=1,in%nObservationPoint
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                     in%observationPoint(k)%name, &
                     in%observationPoint(k)%x(2), &
                     in%observationPoint(k)%x(3), &
                     in%observationPoint(k)%rate

             ! set along-strike coordinate to zero for 2d solutions
             in%observationPoint(k)%x(1)=0._8

             PRINT '(I3.3,X,a4,2ES9.2E1,X,I4)',i, &
                     in%observationPoint(k)%name, &
                     in%observationPoint(k)%x(2), &
                     in%observationPoint(k)%x(3), &
                     in%observationPoint(k)%rate

             IF (0 .GE. in%observationPoint(k)%rate) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: sampling rate must be a strictly positive integer")')
                STOP 1
             END IF

             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO

          filename=TRIM(in%wdir)//"/opts.dat"
          OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          WRITE (FPOUT,'("#      x2       x3 name rate")')
          DO k=1,in%nObservationPoint
             WRITE (FPOUT,'(2ES9.2E1,X,a4,X,I4)') &
                     in%observationPoint(k)%x(2), &
                     in%observationPoint(k)%x(3), &
                     in%observationPoint(k)%name, &
                     in%observationPoint(k)%rate
          END DO
          CLOSE(FPOUT)

       END IF

       ! test the presence of dislocations
       IF ((in%patch%ns .EQ. 0) .AND. &
           (in%interval .LE. 0._8)) THEN
   
          WRITE_DEBUG_INFO(300)
          WRITE (STDERR,'("nothing to do. exiting.")')
          STOP 1
       END IF
   
       PRINT 2000
       ! flush standard output
       CALL FLUSH(6)      

       position=0
       CALL MPI_PACK(in%interval,         1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%mu,               1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%R,                1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%patch%ns,         1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nObservationState,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nObservationPoint,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)

       position=0
       CALL MPI_PACK(in%wdir,256,MPI_CHARACTER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)

       ! send the patches (geometry and friction properties) 
       DO i=1,in%patch%ns
          position=0
          CALL MPI_PACK(in%patch%s(i)%Vl,     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%x(2,i),      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%x(3,i),      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%width(i),    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%dip(i),      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%tau0,   1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%mu0,    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%sig,    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%a,      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%b,      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%L,      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%Vo,     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%V2,     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%damping,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%Q      ,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%H      ,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%wRhoC  ,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%DW2    ,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%Tb     ,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%To     ,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the observation patches
       DO i=1,in%nObservationState
          position=0
          CALL MPI_PACK(in%observationState(:,i),3,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the observation points
       DO i=1,in%nObservationPoint
          position=0
          CALL MPI_PACK(in%observationPoint(i)%name,10,MPI_CHARACTER,packed,psize,position,MPI_COMM_WORLD,ierr)
          DO k=1,3
             CALL MPI_PACK(in%observationPoint(i)%x(k),1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          END DO
          CALL MPI_PACK(in%observationPoint(i)%rate,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

    ELSE ! IF 0 .NE. rank

       !------------------------------------------------------------------
       ! S L A V E S
       !------------------------------------------------------------------

       position=0
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%interval,         1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%mu,               1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%R,                1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%patch%ns,         1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nObservationState,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nObservationPoint,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)

       position=0
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%wdir,256,MPI_CHARACTER,MPI_COMM_WORLD,ierr)

       IF (0 .LT. in%patch%ns) &
                    ALLOCATE(in%patch%s(in%patch%ns), &
                             in%patch%x(3,in%patch%ns), &
                             in%patch%xc(3,in%patch%ns), &
                             in%patch%width(in%patch%ns), &
                             in%patch%dip(in%patch%ns), &
                             in%patch%sv(3,in%patch%ns), &
                             in%patch%dv(3,in%patch%ns), &
                             in%patch%nv(3,in%patch%ns), &
                             STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for patches"

       DO i=1,in%patch%ns
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%Vl,     1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%x(2,i),      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%x(3,i),      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%width(i),    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%dip(i),      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%tau0,   1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%mu0,    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%sig,    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%a,      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%b,      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%L,      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%Vo,     1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%V2,     1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%damping,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%Q      ,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%H      ,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%wRhoC  ,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%DW2    ,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%Tb     ,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%To     ,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
       END DO

       IF (0 .LT. in%nObservationState) &
                    ALLOCATE(in%observationState(3,in%nObservationState), &
                             STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for observation patches"

       DO i=1,in%nObservationState
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%observationState(:,i),3,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       END DO

       IF (0 .LT. in%nObservationPoint) &
                    ALLOCATE(in%observationPoint(in%nObservationPoint), &
                             STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for observation points"

       DO i=1,in%nObservationPoint
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%observationPoint(i)%name,10,MPI_CHARACTER,MPI_COMM_WORLD,ierr)
          DO k=1,3
             CALL MPI_UNPACK(packed,psize,position,in%observationPoint(i)%x(k),1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          END DO
          CALL MPI_UNPACK(packed,psize,position,in%observationPoint(i)%rate,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       END DO

    END IF ! master or slaves

    in%nPatch=in%patch%ns

2000 FORMAT ("# ----------------------------------------------------------------------------")
     
  END SUBROUTINE init

  !-----------------------------------------------
  !> subroutine printhelp
  !! displays a help message with master thread.
  !-----------------------------------------------
  SUBROUTINE printhelp()
  
    INTEGER :: rank,size,ierr
    
    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,size,ierr)
    
    IF (0.EQ.rank) THEN
       PRINT '("usage:")'
       PRINT '("")'
       PRINT '("mpiexec unicycle-ap-ratestate-bath [-h] [--dry-run] [--help] [--epsilon 1e-6]")'
       PRINT '("                                   [--export-netcdf] [--export-netcdf-rate]")'
       PRINT '("                                   [--export-netcdf-temperature] [--export-netcdf-slip]")'
       PRINT '("                                   [filename]")'
       PRINT '("")'
       PRINT '("options:")'
       PRINT '("   -h                            prints this message and aborts calculation")'
       PRINT '("   --dry-run                     abort calculation, only output geometry")'
       PRINT '("   --export-netcdf               export the kinematics to a netcdf file")'
       PRINT '("   --export-netcdf-rate rate     export to netcdf every rate time steps")'
       PRINT '("   --export-netcdf-temperature   export the temperature to a netcdf file")'
       PRINT '("   --export-netcdf-slip          export the cumulative slip to a netcdf file")'
       PRINT '("   --help                        prints this message and aborts calculation")'
       PRINT '("   --version                     print version number and exit")'
       PRINT '("   --epsilon                     set the numerical accuracy [1E-6]")'
       PRINT '("   --export-greens wdir          export the Greens function to file")'
       PRINT '("   --friction-law                type of friction/evolution law [1]")'
       PRINT '("       1: multiplicative         multiplicative form, thermally actived aging law (Barbot, 2019)")'
       PRINT '("       2: additive               additive form, thermally activated aging law (Barbot, 2019)")'
       PRINT '("       3: additive               additive form, thermally activated slip law (Chester, 1994)")'
       PRINT '("   --maximum-iterations          set the maximum time step [1000000]")'
       PRINT '("   --maximum-step                set the maximum time step [none]")'
       PRINT '("")'
       PRINT '("description:")'
       PRINT '("   simulates elasto-dynamics on faults in antiplane strain")'
       PRINT '("   following the radiation-damping approximation using")'
       PRINT '("   the integral method in non-isothermal conditions.")'
       PRINT '("")'
       PRINT '("   if filename is not provided, reads from standard input.")'
       PRINT '("")'
       PRINT '("see also: ""man unicycle""")'
       PRINT '("")'
       PRINT '("            \_o___      ")'
       PRINT '("               \        ")'
       PRINT '("              __\       ")'
       PRINT '("              \ /       ")'
       PRINT '("              ~|\       ")'
       PRINT '("               |~       ")'
       PRINT '("               -        ")'
       PRINT '("             ( * )      ")'
       PRINT '("--------------------------------")'
       PRINT '("")'
       CALL FLUSH(6)
    END IF
    
  END SUBROUTINE printhelp
  
  !-----------------------------------------------
  !> subroutine printversion
  !! displays code version with master thread.
  !-----------------------------------------------
  SUBROUTINE printversion()
    
    INTEGER :: rank,size,ierr
    
    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,size,ierr)
    
    IF (0.EQ.rank) THEN
       PRINT '("unicycle-ap-ratestate-bath version 1.0.0, compiled on ",a)', __DATE__
       PRINT '("")'
       CALL FLUSH(6)
    END IF
  
  END SUBROUTINE printversion
    
END PROGRAM ratestate_bath

