
OBJRS=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, \
      atan3.o boxcar.o \
      heaviside.o xlogy.o omega.o s.o \
      computeStressRectanglePlaneStrain.o \
      computeDisplacementRectanglePlaneStrain.o \
      computeStressTrianglePlaneStrainFiniteDifference.o \
      computeStressTrianglePlaneStrainMixedQuad.o \
      computeStressTrianglePlaneStrainGauss.o \
      computeDisplacementTrianglePlaneStrain.o \
      computeDisplacementTrianglePlaneStrainTanhSinh.o \
      computeDisplacementTrianglePlaneStrainMixedQuad.o \
      types_ps.o gauss.o getdata.o \
      planestrain.o greens_ps.o ratestate.o )

OBJTH=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, \
      atan3.o boxcar.o \
      heaviside.o xlogy.o omega.o s.o \
      computeStressRectanglePlaneStrain.o \
      computeDisplacementRectanglePlaneStrain.o \
      computeStressTrianglePlaneStrainFiniteDifference.o \
      computeStressTrianglePlaneStrainMixedQuad.o \
      computeStressTrianglePlaneStrainGauss.o \
      computeDisplacementTrianglePlaneStrain.o \
      computeDisplacementTrianglePlaneStrainTanhSinh.o \
      computeDisplacementTrianglePlaneStrainMixedQuad.o \
      types_ps_bath.o gauss.o getdata.o \
      planestrain.o greens_ps_bath.o ratestate_bath.o )

OBJHG=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, \
      atan3.o boxcar.o \
      heaviside.o xlogy.o omega.o s.o \
      computeStressRectanglePlaneStrain.o \
      computeDisplacementRectanglePlaneStrain.o \
      computeStressTrianglePlaneStrainFiniteDifference.o \
      computeStressTrianglePlaneStrainMixedQuad.o \
      computeDisplacementTrianglePlaneStrain.o \
      computeDisplacementTrianglePlaneStrainTanhSinh.o \
      computeDisplacementTrianglePlaneStrainMixedQuad.o \
      types.o gauss.o \
      getopt_m.o exportnetcdf.o getdata.o \
      planestrain.o greens.o ode45.o healing_bath.o )

OBJVC=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, \
      atan3.o boxcar.o \
      heaviside.o xlogy.o omega.o s.o \
      computeStressRectanglePlaneStrain.o \
      computeDisplacementRectanglePlaneStrain.o \
      computeStressTrianglePlaneStrainFiniteDifference.o \
      computeStressTrianglePlaneStrainMixedQuad.o \
      computeStressTrianglePlaneStrainGauss.o \
      computeDisplacementTrianglePlaneStrain.o \
      computeDisplacementTrianglePlaneStrainTanhSinh.o \
      computeDisplacementTrianglePlaneStrainMixedQuad.o \
      types_ps.o gauss.o getdata.o \
      planestrain.o greens_ps.o viscouscycles.o )

OBJTB=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, \
      atan3.o boxcar.o \
      heaviside.o xlogy.o omega.o s.o \
      computeStressRectanglePlaneStrain.o \
      computeDisplacementRectanglePlaneStrain.o \
      computeStressTrianglePlaneStrainFiniteDifference.o \
      computeStressTrianglePlaneStrainMixedQuad.o \
      computeStressTrianglePlaneStrainGauss.o \
      computeDisplacementTrianglePlaneStrain.o \
      computeDisplacementTrianglePlaneStrainTanhSinh.o \
      computeDisplacementTrianglePlaneStrainMixedQuad.o \
      types_ps_thermobaric.o gauss.o getdata.o \
      planestrain.o greens_ps_thermobaric.o rootbisection.o thermobaric.o )

$(shell mkdir -p $(DST))
$(shell mkdir -p $(LIBDST))

LIB=$(patsubst %,$(LIBDST)/%, \
      getopt_m.o exportnetcdf.o rk.o )

$(DST)/%.o:$(SRC)/%.c
	$(COMPILE.c) $(CFLAGS) $^ -o $(DST)/$*.o

$(DST)/%.o: $(SRC)/%.f $(SRC)/macros.h90
	$(COMPILE.f) $(FFLAGS) $(filter-out $(SRC)/macros.h90,$^) -o $(DST)/$*.o

$(DST)/%.o: $(SRC)/%.f90 $(SRC)/macros.h90
	$(COMPILE.f) $(FFLAGS) $(filter-out $(SRC)/macros.h90,$^) -o $(DST)/$*.o -J $(DST)

$(LIBDST)/%.o: $(LIBSRC)/%.f90
	$(COMPILE.f) $^ -o $(LIBDST)/$*.o -J $(LIBDST)

all: lib unicycle-ps-ratestate \
	unicycle-ps-viscouscycles \
	unicycle-ps-ratestate-bath unicycle-ps-thermobaric

unicycle-ps-ratestate: $(filter-out $(SRC)/macros.h90,$(OBJRS)) $(LIB)
	$(LINK.f) -o $(DST)/$@ $^ $(LIBS)

unicycle-ps-ratestate-bath: $(filter-out $(SRC)/macros.h90,$(OBJTH)) $(LIB)
	$(LINK.f) -o $(DST)/$@ $^ $(LIBS)

unicycle-ps-healing-bath: $(filter-out $(SRC)/macros.h90,$(OBJHG))
	$(LINK.f) -o $(DST)/$@ $^ $(LIBS)

unicycle-ps-viscouscycles: $(filter-out $(SRC)/macros.h90,$(OBJVC)) $(LIB)
	$(LINK.f) -o $(DST)/$@ $^ $(LIBS)

unicycle-ps-thermobaric: $(filter-out $(SRC)/macros.h90,$(OBJTB)) $(LIB)
	$(LINK.f) -o $(DST)/$@ $^ $(LIBS)

lib: $(LIB)

clean:
	$(RM) $(DST)/*.o $(DST)/*.mod

.NOTPARALLEL:


