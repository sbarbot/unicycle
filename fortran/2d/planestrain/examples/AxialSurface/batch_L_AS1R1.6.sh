#!/bin/bash
### Sathiakumar et al 2019; Submitted to Journal of Geophysical Research: Solid Earth
### Simulations in Figure 6
### Explore the dynamics of a fault-bend-fold as a function of characteristic distance L. Change in fault dip (phi) = 40; Sediment cut off angle=40, Rv=1.6


selfdir=$(dirname $0)


for i in 1 5E-1 4E-1 3E-1 2E-1 1E-1 5E-2 4E-2 3E-2 25E-3 20E-3 18E-3 15E-3 12E-3 10E-3 8E-3 7E-3 6E-3;  do

WDIR=$selfdir/output-L1$i
GDIR=$selfdir/greens

#Fault geomtery
FLT=$WDIR/fold-bend-fold.flt.2d

if [ ! -e $WDIR ]; then
echo adding directory $WDIR
mkdir $WDIR
fi

if [ ! -e $GDIR ]; then
echo adding directory $GDIR
mkdir $GDIR
fi
        # Create input files for the simulation; create geomtery
        #segment 1: shallow ramp
        echo "" | awk 'BEGIN{i=1;pi=atan2(1,0)*2}{
        x2o=0;
        x3o=0;
        width=20;
        dip=40;
        N=400;
        Vl=1.014713600000000e-09;
        seg=1;
        for (j=0;j<N;j++){
                    x2=x2o+j*cos(pi*dip/180)*width;
                    x3=x3o+j*sin(pi*dip/180)*width;
                    beta=(j<(N-25))?0:360;
                    printf "%05d %e %16.11e %16.11e %f %f %f %d %d\n", i,Vl,x2,x3,width,dip,beta,seg,j;
                    i=i+1;
                        }
        #segment 2: decollement
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        width=20;
        dip=0;
        N=2500;
        Vl=6.341958396752917e-10; 
        seg=2;
        for (j=0;j<N;j++){
                    x2=x2o+j*cos(pi*dip/180)*width;
                    x3=x3o+j*sin(pi*dip/180)*width;
                    beta=(j>=25)?0:360;
                    printf "%05d %e %16.11e %16.11e %f %f %f %d %d\n", i,Vl,x2,x3,width,dip,beta,seg,j;
                    i=i+1;
                    }
        #segment 3: Axial surface
        x2o=5.047214752699476e+03;
        x3o=55.933353676524350;
        width=20;
        dip=78;
        N=260;
        Vl=6.677628490121211e-10;
        seg=3;
        for (j=0;j<N;j++){
                    x2=x2o+j*cos(pi*dip/180)*width;
                    x3=x3o+j*sin(pi*dip/180)*width;
                    beta=-1.556239437654643e+02;
                    printf "%05d %e %16.11e %16.11e %f %f %f %d %d\n", i,Vl,x2,x3,width,dip,beta,seg,j;
                    i=i+1;
                    }
            }' > $FLT

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli
30e3 30e3
# time interval
1.2560e+11
# number of patches
`grep -v "#" $FLT | wc -l`
# n  Vpl           x2           x3    width      dip
`grep -v "#" $FLT`
# number of frictional patches
`grep -v "#" $FLT | wc -l`
#  n        tau0        mu0        sig          a          b          L         Vo      G/2Vs
`grep -v "#" $FLT | awk -v L=$i '{
switch ($8) {
        case 1:
                depth=$4;
                tau0=-1;
                mu0=0.6;
                sig=100;
                a=1.0e-2;
                b=1.4e-2;
                Vo=1e-6;
                break;
        case 2:
                depth=$4;
                tau0=-1;
                mu0=0.6;
                sig=100;
                a=1.0e-2;
                b=($9<600)?1.4e-2:0.6e-2;
                Vo=1e-6;
                break;
                case 3:
                depth=$4;
                tau0=-1;
                mu0=0.6;
                sig=100;
                a=5.0e-2;
                b=0;
                Vo=1e-6;
                break;
        default:
        print "error";
        }
printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,5;
}'`
# number of observation patches
`grep -v "#" $FLT | awk '{if (0==(NR-1)%4){ print $1}}' | wc -l | awk '{print $1+20}'`
# n   index rate
1     1    1
2     200    1
3     375    1
4     390    1
5     400    1
6    410    1
7    425    1
8    450    1
9    500    1
10    800    1
11    900    1
12    1000    1
13    1100    1
14    2000    1
15    2885    1
16    2901    1
17    3030    1
18    3100    1
19    3140    1
20    3160    1
`grep -v "#" $FLT | awk 'BEGIN{i=21}{if (0 == (NR-1)%4){print i,NR,20;i=i+1}}'`
# number of observation points
0
# number of events
0
EOF

if [ -e $GDIR/greens-0000.grd ]; then
IMPORT_GREENS="--import-greens $GDIR"
fi

time mpirun -n 8 unicycle-ps-ratestate \
--epsilon 1e-6 \
--friction-law 2 \
--maximum-step 3.15e6 \
--export-netcdf \
$IMPORT_GREENS \
--event-velocity-threshold 0.1 \
--export-greens $GDIR \
$* $WDIR/in.param
done
