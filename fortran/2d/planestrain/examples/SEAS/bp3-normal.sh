#!/bin/bash

# SEAS Benchmark Problem BP3
#
# The problem set-up for benchmark BP3 (-QD: quasi-dynamic; -FD: fully dynamic) is that
# for 2D plane-strain motion, with a planar, dipping fault and a free surface. Many of the
# elastic and frictional parameters are identical to those used in the first benchmark problem
# (BP1). For BP3-QD, radiation damping is used to approximate the inertia effects.

set -e
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

selfdir=$(dirname $0)

while getopts "d:p" flag
do
	case "$flag" in
	d) dset="1";DIP=$OPTARG;;
	p) pset="1";;
	esac
done
for item in $pset;do
	shift
done
for item in $dset;do
	shift;shift;
done

N=1600
DIP="${DIP:-90}"
DX=25

WDIR=$selfdir/output-bp3-normal-$DIP
FLT=$WDIR/flt.2d


if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

if [ "" == "$pset" ]; then

	echo "" | awk -v N=$N -v dip=$DIP -v width=$DX 'BEGIN{i=1;pi=atan2(1.0,0.0)*2.0}{
       		x2o=0;
     	  	x3o=0;
		Vl=-1e-9;
		d2=1+cos(pi*dip/180.0)-1;
		d3=1+sin(pi*dip/180.0)-1;
		beta=0;
	       	for (j=0;j<N;j++){
	       	        x2=x2o+j*d2*width;
	       	        x3=x3o+j*d3*width;
	       	        printf "%05d %e %16.11e %16.11e %f %f %f\n", j+1,Vl,x2,x3,width,dip,beta;
	       	}
	}' > $FLT

	cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# Lame elastic moduli (MPa)
32038.12032 32038.12032
# time interval (s)
4.8e10
# number of patches
`grep -v "#" $FLT | wc -l`
# n   Vl x2   x3 width dip
`grep -v "#" $FLT`
# number of frictional patches
`grep -v "#" $FLT | wc -l`
# n tau0 mu0 sig    a     b    L   Vo G/2Vs
`grep -v "#" $FLT | awk -v width=$DX 'function boxcar(x){return (x>=-0.5 && x<=0.5)?1:0};function heavi(x){return (x>0)?1:0};function ramp(x){return x*boxcar(x-0.5)+heavi(x-1)} function asinh(x){return log(x+sqrt(1+x^2))} function sinh(x){return (exp(x)-exp(-x))/2}{ 
	depth=$4; 
	xd=($1-1)*width;
	mu0=0.6;
	sig=50;
	amax=0.025;
	a=1.0e-2+ramp((xd-15e3)/3e3)*(amax-0.01);
	b=0.015;
	Vo=1e-6;
	L=8e-3;
	rho=2670;
	Vs=3464;
	G=rho*Vs^2/1e6;
	damping=G/Vs/2;
	Vl=1e-9;
	tau0=amax*sig*asinh(Vl/Vo/2*exp((mu0+b*log(Vo/Vl))/amax));
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,damping;
	}'`
# number of observation patches
`grep -v "#" $FLT | awk '{if (0 == (NR-1)%4){print $0}}' | wc -l | awk '{print $1+12}'`
# n   index rate
`echo "" | awk -v width=$DX '{
	split("0 2.5 5.0 7.5 10.0 12.5 15 17.5 20 25 30 35",a," ");
	for (i=1;i<=length(a);i++){
		printf "%3d %4d 1\n",i,1+a[i]*1e3/width;
	}
}'`
# subsample the fault in space and time
`grep -v "#" $FLT | awk 'BEGIN{i=13}{if (0 == (NR-1)%4){print i,NR,20;i=i+1}}'`
# number of observation points
7
# n name x2 x3 sampling
`echo "" | awk '{
	split("-32 -16 -8 0 8 16 32",a," ");
	for (i=1;i<=length(a);i++){
		printf "%3d n%+03d %f %f 1\n",i,a[i],a[i]*1e3,0;
	}
}'`
# number of events
0
EOF

	if [ -e $WDIR/greens-0000.grd ]; then
	       IMPORTGREENS="--import-greens $WDIR"
	fi       

	time mpirun -n 2 unicycle-ps-ratestate \
		--epsilon 1e-6 \
		--friction-law 2 \
		--maximum-step 3.15e7 \
		--export-netcdf \
		--export-greens $WDIR \
		$IMPORTGREENS \
		--maximum-iterations 1000000 \
		$WDIR/in.param
fi

for i in $WDIR/opts-n*.dat; do 
	station=$(dirname $i)/srfst_f`echo $(basename $i .dat) | awk '{print substr($0,6,10)}'`
	numStep=`wc -l $i | awk '{print $1}'`
	echo "# creating $station"
	grep -v "#" $i | awk -v d="`date`" -v width=$DX -v numStep=$numStep 'BEGIN{
		print "# problem=SEAS Benchmark BP3-QD"; 
		print "# author=Sylvain Barbot"; 
		print "# date="d; 
		print "# code=unicycle-ps-ratestate"; 
		print "# code_version=beta"; 
		print "# element_size="width" m";
		print "# minimum_time_step="minStep; 
		print "# maximum_time_step="maxStep; 
		print "# num_time_steps="numStep; 
		print "# location=on surface"; 
		print "# Column #1 = Time (s)"; 
		print "# Column #2 = Displacement_1 (m)"; 
		print "# Column #3 = Displacement_2 (m)"; 
		print "# Column #4 = Velocity_1 (m/s)"; 
		print "# Column #5 = Velocity_2 (m/s)"; 
		print "t disp_1 disp_2 vel_1 vel_2"
		x2=0;x3=0;t=-1;
	}{
		print t,x2,x3,($2-x2)/($1-t),($3-x3)/($1-t);t=$1;x2=$2;x3=$3
	}' > $station
done

files=`grep -v "#" $FLT | awk 'BEGIN{i=13}{if (0 == (NR-1)%4){print i; i=i+1}}' | \
	awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`

echo "# export to $WDIR/step-slip.grd"
yRange=`for i in $files; do wc -l $i | awk '{print $1}'; done | minmax -C | awk '{print ($2-1)*20+1}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	grep -v "#" $i | awk -v i=$index '{print i,(NR-1)*20+1,$2}'
done | surface -I1/20 -R1/$N/1/$yRange -G$WDIR/step-slip.grd

echo "# export to $WDIR/slip-log10v.grd"
yRange=`for i in $files; do tail -n 1 $i | awk 'function abs(x){return (x>0)?x:-x}{print abs($2)}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index 'function abs(x){return (x>0)?x:-x}{print i,abs($2),$6}'
done | surface -I1/`echo $yRange | awk '{print $1/2048}'` -R1/$N/0/$yRange -G$WDIR/slip-log10v.grd

echo "# export to $WDIR/step-log10v.grd"
yRange=`for i in $files; do wc -l $i | awk '{print $1}'; done | minmax -C | awk '{print ($2-1)*20+1}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	grep -v "#" $i | awk -v i=$index '{print i,(NR-1)*20+1,$6}'
done | surface -I1/20 -R1/$N/1/$yRange -G$WDIR/step-log10v.grd

echo "# export to $WDIR/time-log10v.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $1/3.15e7}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	grep -v "#" $i | awk -v i=$index '{print i,$1/3.15e7,$6}'
done | blockmean -I1/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -E | awk '{print $1,$2,$6}' | \
	surface -I1/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/time-log10v.grd

# convert to SCEC format
files=`grep -v "#" $FLT | awk 'BEGIN{i=13}{if (0 == (NR-1)%4){print i; i=i+1}}' | \
	awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`

minStep=`grep -v "#" $WDIR/time.dat | minmax -C -H1 | awk '{print $3}'`
maxStep=`grep -v "#" $WDIR/time.dat | minmax -C | awk '{print $4}'`
numStep=`grep -v "#" $WDIR/time.dat | wc -l | awk '{print $1}'`

for i in `printf "%s\n" {1..12} | awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	depth=`echo $index | awk -v width=$DX '{print ($1-1) * width}'`
	station=$WDIR/fltst_dp`echo $depth | awk '{printf "%03.3d",$1/100}'`
	numStep=`wc -l $i | awk '{print $1}'`

	echo "# export to $station"

	grep -v "#" $i | \
		awk -v d="`date`" -v minStep=$minStep -v maxStep=$maxStep -v numStep=$numStep -v depth=$depth -v width=$DX 'BEGIN{
		print "# problem=Benchmark problem (BP3-QD)";
		print "# author=Sylvain Barbot";
		print "# date="d;
		print "# code=unicycle-ps-ratestate";
		print "# code_version=beta";
		print "# element_size="width" m";
		print "# minimum_time_step="minStep;
		print "# maximum_time_step="maxStep;
		print "# num_time_steps="numStep;
		print "# location=on fault, "depth"km depth";
		print "# Column #1 = Time (s)";
		print "# Column #2 = Slip (m)";
		print "# Column #3 = Slip rate (log10 m/s)";
		print "# Column #4 = Shear stress (MPa)";
		print "# Column #5 = Normal stress (MPa)";
		print "# Column #6 = State (log10 s)";
		print "t slip slip_rate shear_stress normal_stress state"
	}{
		print $1,$2,$6,$3,50-$4,$5
	}' > $station
done

# slip evolution
./isochrons.sh -i 1/1.575e8 $WDIR/time.dat

echo "# processing $WDIR/slip-contours-seismic.xy"
echo "# seismic isochrons with interval 1" > $WDIR/slip-contours-seismic.xy
grep -v "#" $WDIR/isochrons-seismic.dat | while read -r line; do 
	IFS=" " read index time velocity <<< "$(echo $line)"
	index=`echo $index | awk '{print int(($1-1)/20)*20+1}'`
	next=`echo $index | awk '{print int(($1-1)/20)*20+1+1}'`
	echo "> -S$velocity" >> $WDIR/slip-contours-seismic.xy
	grd2xyz -R1/$N/$index/$next $WDIR/step-slip.grd 2> /dev/null | awk -v dx=$DX '{print $1*dx,$3}' >> $WDIR/slip-contours-seismic.xy
done

echo "# processing $WDIR/slip-contours-aseismic.xy"
echo "# aseismic isochrons with interval 1.575e8" > $WDIR/slip-contours-aseismic.xy
grep -v "#" $WDIR/isochrons-aseismic.dat | while read -r line; do 
	IFS=" " read index time velocity <<< "$(echo $line)"
	index=`echo $index | awk '{print int(($1-1)/20)*20+1}'`
	next=`echo $index | awk '{print int(($1-1)/20)*20+1+1}'`
	echo "> -S$velocity" >> $WDIR/slip-contours-aseismic.xy
	grd2xyz -R1/$N/$index/$next $WDIR/step-slip.grd 2> /dev/null | awk -v dx=$DX '{print $1*dx,$3}' >> $WDIR/slip-contours-aseismic.xy
done

