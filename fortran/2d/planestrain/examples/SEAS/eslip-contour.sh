#!/bin/bash

# extra script for mapping tool grdmap.sh
# overlays coastlines.
#
#   grdmap.sh -e ecoasts.sh selfdir/file.grd
#

set -e
self=$(basename $0)
selfdir=$(dirname $0)
cmdline=$*
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

while getopts "b:c:gp:v:H:J:" flag
do
	case "$flag" in
	b) bset=1;bds=$OPTARG;;
	c) cset=1;carg=$OPTARG;;
	g) gset=1;;
	p) pset=1;U3=$OPTARG;;
	v) vset=1;SIZE=$OPTARG;VECTOR=$OPTARG"c";;
	J) Jset="-J";PROJ=$OPTARG;;
	H) Hset=1;HEIGHT=$OPTARG;;
	esac
done
for item in $bset $cset $pset $vset $Hset $Jset; do
	shift;shift
done
for item in $gset; do
	shift
done

if [ "$#" -lt "1" ]; then
	echo ${self} overlays coastlines on a GMT map.
	echo
	echo usage: $self -b xmin/xmax/ymin/ymax file.ps
	exit 1
fi

echo $self: $cmdline
PSFILE=$1

if [ "$gset" == "" ]; then
	if [ "$Jset" != "" ]; then
		UNIT=ll
	else
		UNIT=km
	fi
else
	UNIT=ll
	PROJ=M$HEIGHT
fi

psxy -O -K -J$PROJ -R$bds -P -m \
	-W0.25p/200/210/223 <<EOF >> $PSFILE
`grep -v "#" $(dirname $1)/slip-contours-aseismic.xy | awk '{if (">"==substr($0,0,1)){print $0}else{print $1/25,$2}}'`
EOF

psxy -O -K -J$PROJ -R$bds -P -m \
	-W0.5p/0/10/3 <<EOF >> $PSFILE
`grep -v "#" $(dirname $1)/slip-contours-seismic.xy | awk '{if (">"==substr($0,0,1)){print $0}else{print $1/25,$2}}'`
EOF

