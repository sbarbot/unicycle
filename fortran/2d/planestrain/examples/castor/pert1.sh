#!/bin/bash

# earthquake cycle in plane strain with perturbations in confining pressure

selfdir=$(dirname $0)

WDIR=$selfdir/pert1

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

WIDTH=100
NW=400
DIP=60
GEOMETRY=`echo "" | awk -v w=$WIDTH -v nw=$NW -v d=$DIP \
	'BEGIN{d2r=atan2(1,0)/90} \
	{for (j=1;j<=nw;j++){ \
		Vpl=1e-9; \
		printf "%4d %9.2e %11.4e %11.5e %9.2e %3d\n", \
		j,Vpl,(j-1)*w*cos(d2r*d),1e2+(j-1)*w*sin(d2r*d),w,d}}'`

FRICTION=`echo "$GEOMETRY" | awk -v nw=$NW 'function min(x,y){return (x<y)?x:y}{ \
	depth=$4;
	L=0.01;
	a=1e-2;
	# seismogenic zone
	b=(depth>=3e3 && depth<=15e3)?a+4.0e-3:a-4e-3;
	mu0=0.6;
	sig=100;
	Vo=1e-6;
	tau0=-1;
	printf "%4d %11.4e %10.2e %10.2e %10.2e %10.2e %10.2e %10.2e %d\n", \
	         NR,  tau0,   mu0,   sig,     a,     b,     L,    Vo, 5}'`

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli
30e3 30e3
# time interval
3.15e11
# number of patches
`echo "$GEOMETRY" | wc -l`
# n  Vpl           x2           x3    width      dip
`echo "$GEOMETRY"`
# number of frictional patches
`echo "$FRICTION" | wc -l`
#  n        tau0        mu0        sig          a          b          L         Vo      G/2Vs
`echo "$FRICTION"`
# number of observation patches
8
# n   index rate
  1      25    1
  2      50    1
  3      75    1
  4     100    1
  5     125    1
  6     150    1
  7     200    1
  8     300    1
# number of observation points
0
# number of events
2
# number of stress perturbation patches
`echo "$GEOMETRY" | wc -l`
# n dtau dsig
`echo "$GEOMETRY" | awk -v nw=$NW 'function min(x,y){return (x<y)?x:y}{ \
	dtau=0;
	dsig=0;
	rise=1e2;
	printf "%4d %11.4e %11.4e %11.4e\n", NR,  dtau,   dsig, rise}'`
# time to next event
1.00e10
# number of stress perturbation patches
`echo "$GEOMETRY" | wc -l`
# n dtau dsig
`echo "$GEOMETRY" | awk -v nw=$NW 'function min(x,y){return (x<y)?x:y}{ \
	depth=$4;
	dtau=(depth>=1e3 && depth<=6e3)?+0e1:0;
	dsig=(depth>=1e3 && depth<=6e3)?-1e0:0;
	rise=1e3;
	printf "%4d %11.4e %11.4e %11.4e\n", NR,  dtau,  dsig, rise}'`
EOF

time mpirun -n 2 unicycle-ps-ratestate \
	--epsilon 1e-6 \
	--maximum-step 3.15e6 \
	--export-netcdf $* \
	$WDIR/in.param




