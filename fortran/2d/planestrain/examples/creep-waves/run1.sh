#!/bin/bash

# waves of partial coupling with duration and recurrence time of the order of 50 years
# on a dipping thrust fault. Rb=0.02, Ru=6.66.
#
# plot slip evolution with:
#
#   grdmap.sh -H -p -12/1/0.01 -c cycles.cpt output1/time-log10v.grd
#
# plot cycles in gnuplot with
#
#   plot 'output1/time.dat' u ($1/3.15e7):(log10($3)) w l, 'output1/patch-00000001-00000400.dat' u ($1/3.15e7):6 w l
#
# plot surface displacement in gnuplot with
#
#   plot 'output1/opts-GPS1.dat' u ($1/3.15e7):2 w l

set -e
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

selfdir=$(dirname $0)

N=800
DIP=30
DX=50

WDIR=$selfdir/output1
FLT=$WDIR/flt.2d


if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

echo "" | awk -v N=$N -v dip=$DIP -v width=$DX 'BEGIN{i=1;pi=atan2(1.0,0.0)*2.0}{
	x2o=0;
    	x3o=25e3;
	Vl=1e-9;
	d2=1+cos(pi*dip/180.0)-1;
	d3=1+sin(pi*dip/180.0)-1;
	beta=0;
	for (j=0;j<N;j++){
	        x2=x2o+j*d2*width;
	        x3=x3o+j*d3*width;
	        printf "%05d %e %16.11e %16.11e %f %f %f\n", j+1,Vl,x2,x3,width,dip,beta;
	}
}' > $FLT

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# Lame elastic moduli (MPa)
30e3 30e3
# time interval (s)
2.5e10
# number of patches
`grep -v "#" $FLT | wc -l`
# n   Vl x2   x3 width dip
`grep -v "#" $FLT`
# number of frictional patches
`grep -v "#" $FLT | wc -l`
# n tau0 mu0 sig    a     b    L   Vo G/2Vs
`grep -v "#" $FLT | awk -v width=$DX '{ 
	depth=$4; 
	xd=($1-1)*width;
	mu0=0.6;
	sig=20;
	bma=0.004;
	Rb=0.02;
	a=bma*(1-Rb)/Rb;
	b=(xd>5e3 && xd<=25e3)?bma/Rb:a-0.004;
	Vo=1e-6;
	L=1.6e-2;
	rho=2670;
	Vs=3464;
	G=rho*Vs^2/1e6;
	damping=G/Vs/2;
	Vl=1e-9;
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,-1,mu0,sig,a,b,L,Vo,damping;
	}'`
# number of observation patches
`grep -v "#" $FLT | awk '{if (0 == (NR-1)%4){print $0}}' | wc -l | awk '{print $1+1}'`
# n   index rate
1 `echo $N | awk '{print int($1/2)}'` 1
`grep -v "#" $FLT | awk 'BEGIN{i=2}{if (0 == (NR-1)%4){print i,NR,1;i=i+1}}'`
# number of observation points
1
# n name   x2 x3 rate
  1 GPS1 15e3  0    1
# number of events
0
EOF

if [ -e $WDIR/greens-0000.grd ]; then
       IMPORTGREENS="--import-greens $WDIR"
fi       

time mpirun -n 2 unicycle-ps-ratestate \
	--epsilon 1e-6 \
	--maximum-step 3.15e7 \
	--export-netcdf \
	--export-greens $WDIR \
	$IMPORTGREENS \
	--maximum-iterations 1000000 \
	$WDIR/in.param

exit

files=`grep -v "#" $FLT | awk 'BEGIN{i=2}{if (0 == (NR-1)%4){print i; i=i+1}}' | \
	awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`

echo "# export to $WDIR/slip-log10v.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $2}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$2,$6}'
done | surface -I1/`echo $yRange | awk '{print $1/2048}'` -R1/$N/0/$yRange -G$WDIR/slip-log10v.grd

echo "# export to $WDIR/time-log10v.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $1/3.15e7}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	grep -v "#" $i | awk -v i=$index '{print i,$1/3.15e7,$6}'
done | blockmean -I1/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -E | awk '{print $1,$2,$6}' | \
	surface -I1/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/time-log10v.grd


