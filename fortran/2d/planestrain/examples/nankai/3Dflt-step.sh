#!/bin/bash
dir=$1

#### bounds for whole dynamics
bounds="-b 0/2402/35000/70000"

#### bounds for deep SSEs
#bounds="-b 1300/2402/51115/53074"

#### create a geometry file for pseudo-3D plotting of simulation time series
ny=`grdinfo -C ${dir}/log10v.grd | awk '{print $5}'`
#awk -v n=$ny '{for(i=1;i<=n;i++){print NR,i,-$4/1e3}}' faults/nankai2.flt.2d | xyz2grd -R${dir}/log10v.grd -G${dir}/geometry.grd

#### plot in pseudo 3D
grdview.sh -H -p -12/0.6/0.01 $bounds -E 160/30 -v 20 -c creep.cpt -t 10000 -G ${dir}/log10v.grd ${dir}/geometry.grd

rm ${dir}/geometry.grd-plot.ps
