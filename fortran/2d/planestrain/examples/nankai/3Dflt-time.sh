#!/bin/bash
dir=vc_acc-miu0.1-Rb0.025-Ru1.83_dis_SZ-Rb0.24-Ru30
#reg=flt
reg=sse
here=`pwd`

if [ $reg = "flt" ]
then
bounds="-b 0/2402/31536000000/45727200000"
tick="-t 6.3072e9"
else
#bounds="-b 1300/2100/35320320000/36997120000"
bounds="-b 0/400/29959200000/36266400000"
tick="-t 10"
fi

#awk '(1300<NR && NR<2100){print NR,35320320000, -$4/1e3+71.4713973999}' faults/nankai2.flt.2d | sort -nk1 > sseWindow.txt
#awk '(1300<NR && NR<2100){print NR,36997120000, -$4/1e3+71.4713973999}' faults/nankai2.flt.2d | sort -nrk1 >> sseWindow.txt 

#### create a geometry file for pseudo-3D plotting of simulation time series
ny=`grdinfo -C ${dir}/log10v-cutshallow-time.grd | awk '{print $11}'`
y_inc=`grdinfo -C ${dir}/log10v-cutshallow-time.grd | awk '{print $9}'`
awk -v n=$ny -v inc=$y_inc '{for(i=0;i<n;i++){print NR,29959200000+i*inc,-$4/1e3}}' faults/nankai2.flt.2d | xyz2grd -R${dir}/log10v-cutshallow-time.grd -G${dir}/geometry-cutshallow-time.grd

# plot in pseudo 3D
#grdview.sh -H -p -12/1/0.01 $bounds -E 160/30 -v 10 -c cycles.cpt $tick -e ${here}/plt-sse-window.sh -G ${dir}/log10v-cut-time.grd ${dir}/geometry-cut-time.grd
grdview.sh -H -p -12/1/0.01 $bounds -E 160/30 -v 10 -c cycles.cpt $tick -G ${dir}/log10v-cutshallow-time.grd ${dir}/geometry-cutshallow-time.grd
mv ${dir}/geometry-cutshallow-time.grd-plot.pdf ${dir}/${reg}-geometry-cutshallow-time.grd-plot.pdf
