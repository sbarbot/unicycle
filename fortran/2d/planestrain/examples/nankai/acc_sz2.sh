#!/bin/bash

set -e
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

# Earthquakes in the seismogenic zone and the accretionary prism in the Nankai trough, Japan.
# Accretionary prism is velocity weakening with low background loading rate

# The setup produces a series of single and multi-segment ruptures. This is observed for L=2-10cm. 
# For L=1cm, all large ruptures break the accretionary prism, even though there are still
# earthquakes that partially rupture the seismogenic zone. For larger values of L, some events 
# propagate partially into the accretionary prism. There is then a relation between the width
# of the propagation into the prism and the value of L.
#
# Single-segment ruptures are followed by afterslip in the shallow velocity-weakening area.

selfdir=$(dirname $0)

WDIR=$selfdir/acc_sz2
FLT=faults/nankai.flt.2d

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

echo "" | awk 'BEGIN{i=1;pi=atan2(1,0)*2}{
        x2o=0;
        x3o=0;
        width=200;
        dip=2.51744;
        N=209;
        seg=1;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,0.6e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        width=200;
        dip=7.8278226;
        N=744;
        seg=2;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.8e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        width=200;
        dip=15.5623;
        N=120;
        seg=3;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.8e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        width=200;
        dip=26.3105;
        N=161;
        seg=4;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.8e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        width=200;
        dip=32.4577;
        N=267;
        seg=5;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.8e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
}' > $FLT

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli
30e3 30e3
# time interval
3e10
# number of patches
`grep -v "#" $FLT | wc -l`
# n  Vpl           x2           x3    width      dip
`grep -v "#" $FLT`
# number of frictional patches
`grep -v "#" $FLT | wc -l`
#  n        tau0        mu0        sig          a          b          L         Vo      G/2Vs
`grep -v "#" $FLT | gawk '{ 
        switch ($7) { 
	case 1:
		depth=$4; 
		tau0=-1; 
		mu0=0.2;
		sig=10;
		a=1.0e-2;
		b=1.1e-2;
		L=5e-2;
		Vo=1e-6;
		break;
	case 2:
                depth=$4;
                tau0=-1;
                mu0=0.2;
                sig=20;
                a=1.0e-2;
                b=1.4e-2;
                L=1.5e-2;
                Vo=1e-6;
		break;
	case 3:
                depth=$4;
                tau0=-1;
                mu0=0.2;
                sig=10;
                a=1.0e-2;
                b=0.6e-2;
                L=4.2e-3;
                Vo=1e-6;
		break;
	case 4:
                depth=$4;
                tau0=-1;
                mu0=0.2;
                sig=10;
                a=1.0e-2;
                b=0.6e-2;
                L=2.2e-4;
                Vo=1e-6;
		break;
	case 5:
                depth=$4;
                tau0=-1;
                mu0=0.2;
                sig=10;
                a=1.0e-2;
                b=0.6e-2;
                L=2e-1;
                Vo=1e-6;
		break;
	default:
		print "error";
        }
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,5;\
	}'`
# number of observation patches
8
# n   index rate
  1      50    1
  2     100    1
  3     215    1
  4     500    1
  5     800    1
  6    1000    1
  7    1150    1
  8    1300    1
# number of observation points
7
# n name    x2    x3 rate
  1 0001 -20e3     0    1
  2 0002   0e3     0    1
  3 0003  20e3     0    1
  4 0004  40e3     0    1
  5 0005  60e3     0    1
  6 0006 100e3     0    1
  7 0007 140e3     0    1
# number of events
0
EOF

time mpirun -n 2 unicycle-ps-ratestate \
	--maximum-step 3.15e7 \
	--export-netcdf \
	--maximum-iterations 10000000 \
	$* $WDIR/in.param


