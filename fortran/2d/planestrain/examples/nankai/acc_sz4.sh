#!/bin/bash

set -e
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

# Earthquakes in the seismogenic zone and the accretionary prism in the Nankai trough, Japan.
# Accretionary prism is velocity weakening with low background loading rate

# The setup produces single and multi-segment ruptures in the seismogenic zone, and 
# slow-slip events, very low-frequency earthquakes in the accretionary prism. 
# A key feature of the model input is low static friction of the seismogenic zone.
# Low static friction creates slow slip speeds in the seismogenic zone, which is
# not desirable.

selfdir=$(dirname $0)

WDIR=$selfdir/acc_sz4
FLT=$WDIR/nankai2.flt.2d

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
	mkdir $WDIR/greens
fi

echo "" | awk 'BEGIN{i=1;pi=atan2(1,0)*2}{
        x2o=0;
        x3o=0;
        width=250;
        dip=2.51744;
        N=167;
        seg=1;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.2e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        dip=7.8278226;
	N=595;
        seg=2;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.8e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
	dip=15.5623;
	N=96;
        seg=3;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.8e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        dip=26.3105;
        N=129;
        seg=4;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.8e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
	        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        dip=32.4577;
        N=214;
        seg=5;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.8e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
}' > $FLT

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli
30e3 30e3
# time interval
5e10
# number of patches
`grep -v "#" $FLT | wc -l`
# n  Vpl           x2           x3    width      dip
`grep -v "#" $FLT`
# number of frictional patches
`grep -v "#" $FLT | wc -l`
#  n        tau0        mu0        sig          a          b          L         Vo      G/2Vs
`grep -v "#" $FLT | gawk '{ \
        switch ($7) {
	case 1:
                depth=$4;
                tau0=-1;
                mu0=0.2;
                sig=40;
                a=4e-2;
                b=4.06e-2;
                L=8e-3;
                Vo=1e-4;
		break;
	case 2:
                depth=$4;
                tau0=-1;
                mu0=0.2;
                sig=40;
                a=4e-2;
                b=4.16e-2;
                L=4e-3;
                Vo=1e-4;
		break;
	case 3:
                depth=$4;
                tau0=-1;
                mu0=0.2;
                sig=20;
                a=4e-2;
                b=4e-2;
                L=1.9e-2;
                Vo=1e-4;
		break;
        case 4:
                depth=$4;
                tau0=-1;
                mu0=0.2;
                sig=20;
                a=4e-2;
                b=4e-2;
                L=1.9e-2;
                Vo=1e-4;
                break;
        case 5:
                depth=$4;
                tau0=-1;
                mu0=0.2;
                sig=40;
                a=4e-2;
                b=3e-2;
                L=1.9e-2;
                Vo=1e-4;
                break;
	default:
		print "error";
        }
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,5;\
	}'`
# number of observation patches
4
# n   index rate
  1      90    1
  2     300    1
  3     500    1
  4     700    1
# number of observation points
0
# number of events
0
EOF

time mpirun -n 2 unicycle-ps-ratestate \
	--maximum-step 3.15e6 \
	--event-velocity-threshold 1e-3 \
	--export-greens $WDIR/greens \
	--maximum-iterations 3000000 \
	--export-netcdf $*\
	$WDIR/in.param

#grdmap.sh -H -p -12/1/0.01 -t 1000 -d 100 -J X15c/20c -c creep.cpt ./acc_sz4/log10v.grd
