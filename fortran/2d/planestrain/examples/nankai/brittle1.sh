#!/bin/bash

# Earthquake and SSE cycles in plane strain viscoelastic media in Nankai, Japan.

selfdir=$(dirname $0)

WDIR=$selfdir/brittle1
GDIR=$selfdir/greens1

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

if [ ! -e $GDIR ]; then
	echo adding direction $GDIR
	mkdir $GDIR
fi

FLT=faults/nankai2.flt.2d

echo "" | awk 'BEGIN{i=1;pi=atan2(1,0)*2}{
        x2o=0;
        x3o=0;
        width=250;
        dip=2.51744;
        N=167;
        seg=1;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.2e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        dip=7.8278226;
	N=595;
        seg=2;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.8e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
	dip=15.5623;
	N=96;
        seg=3;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.8e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        dip=26.3105;
        N=129;
        seg=4;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.8e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
	        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        dip=32.4577;
        N=214;
        seg=5;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.8e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
}' > $FLT

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli
30e3 30e3
# time interval
4.5e10
# number of patches
`grep -v "#" $FLT | wc -l`
# n  Vpl           x2           x3    width      dip
`grep -v "#" $FLT`
# number of frictional patches
`grep -v "#" $FLT | wc -l`
#  n        tau0        mu0        sig          a          b          L         Vo      G/2Vs
`grep -v "#" $FLT | gawk '{ \
        switch ($7) {
	case 1:
                depth=$4;
                tau0=-1;
                mu0=0.6;
                sig=30;
                a=4e-2;
                b=4.156e-2;
                L=3.7e-2;
                Vo=1e-4;
		break;
	case 2:
                depth=$4;
                tau0=-1;
                mu0=0.6;
                sig=42;
                a=4e-2;
                b=4.15e-2;
                L=4.0e-3;
                Vo=1e-4;
		break;
	case 3:
                depth=$4;
                tau0=-1;
                mu0=0.6;
                sig=20;
                a=4e-2;
                b=4.05e-2;
                L=2.5e-3;
                Vo=1e-4;
		break;
        case 4:
                depth=$4;
                tau0=-1;
                mu0=0.6;
                sig=20;
                a=4e-2;
                b=4.02e-2;
                L=8e-5;
                Vo=1e-4;
                break;
        case 5:
                depth=$4;
                tau0=-1;
                mu0=0.6;
                sig=40;
                a=4e-2;
                b=3e-2;
                L=1.9e-2;
                Vo=1e-4;
                break;
	default:
		print "error";
        }
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,5;\
	}'`
# number of observation patches
`grep -v "#" $FLT | awk '{if (0 == (NR-1)%4){print $0}}' | wc -l | awk '{print $1+4}'`
# n index rate
  1    90    1
  2   300    1
  3   500    1
  4   700    1
# subsample the fault in space and time
`grep -v "#" $FLT | awk 'BEGIN{i=5}{if (0 == (NR-1)%4){print i,NR,20;i=i+1}}'`
# number of observation points
0
# number of events
0
EOF

if [ -e $GDIR/greens-0000.grd ]; then
	IMPORT_GREENS="--import-greens $GDIR"
fi

time mpirun -n 2 unicycle-ps-ratestate \
	--friction-law 1 \
	--maximum-step 3.15e6 \
	--maximum-iterations 7000000 \
	--export-greens $GDIR \
	$IMPORT_GREENS \
	--export-netcdf $*\
	$WDIR/in.param

files=`grep -v "#" $FLT | awk 'BEGIN{i=5}{if (0 == (NR-1)%4){print i; i=i+1}}' | \
	awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`

echo "# export $WDIR/slip-log10v.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $2}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$2,$6}'
done | surface -I1/`echo $yRange | awk '{print $1/512}'` -R1/$N/0/$yRange -G$WDIR/slip-log10v.grd

echo "# export $WDIR/time-log10v.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $1/3.15e7}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$1/3.15e7,$5}'
done | \
	blockmean -I4/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -E | awk '{print $1,$2,$6}' | \
	surface -I4/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/time-log10v.grd


