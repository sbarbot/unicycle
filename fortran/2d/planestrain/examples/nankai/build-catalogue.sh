#!/bin/bash

model=vc_acc-miu0.1-Rb0.025-Ru1.83_dis_SZ-Rb0.24-Ru30_Q600
ps=catalogue-qll-2-5.ps
pdf=catalogue-qll-2-5.pdf
range=200/2200/5.5/11.5
#### build catalogue from time.dat
#ecatalogue.sh -b 1E-2/1E-5 ${model}/time.dat > ${model}/catalogue-all-2-5.dat

#### plot lines
grep -v "#" ${model}/catalogue-all-2-5.dat | awk 'BEGIN{pre=0}{print $4/3.1536e7,pre"\n"$4/3.1536e7,log($9)/log(10);pre=log($9)/log(10)}' | psxy -R$range -JX27c/4c -W1p,black -Ba200f100:"Time (year)":/a2f1:"Force (N)":WSen -K > ${model}/$ps

#### plot stars
grep -v "#" ${model}/catalogue-all-2-5.dat | awk '(log($9)/log(10)>10){print $4/3.1536e7,log($9)/log(10)}' | psxy -R -J -K -O -W0.5p,black -Sa0.55c -Gdarkred >> ${model}/$ps
grep -v "#" ${model}/catalogue-all-2-5.dat | awk '(log($9)/log(10)>8&&log($9)/log(10)<10){print $4/3.1536e7,log($9)/log(10)}' | psxy -R -J -K -O -W0.5p,black -Sa0.35c -Gorange >> ${model}/$ps
grep -v "#" ${model}/catalogue-all-2-5.dat | awk '(log($9)/log(10)<8){print $4/3.1536e7,log($9)/log(10)}' | psxy -R -J -K -O -W0.5p,black -Sa0.25c -Gyellow >> ${model}/$ps

#### plot event number
grep -v "#" ${model}/catalogue-all-2-5.dat | awk '(log($9)/log(10)>10){printf"%.1f %.2f 10 0 0 MR %.0f\n",$4/3.1536e7-5,log($9)/log(10)+0.6,$1}' | pstext -R -J -K -O >> ${model}/$ps

#### plot histogram for recurrence time
#grep -v "#" ${model}/catalogue-all-2-5.dat | awk '{print $8/3.1536e7}' | pshistogram -JX10c/5c -R0/200/0/30 -Ba50f25:"Recurrence time for all earthquakes (yr)":/a10WSen:"Frequency (%)": -Z0 -Y8c -W5 -F -L0.5p -V -K -O >> ${model}/$ps
grep -v "#" ${model}/catalogue-all-2-5.dat | awk 'BEGIN{pre=0}(log($9)/log(10)>8){print $4/3.1536e7-pre;pre=$4/3.1536e7}' | pshistogram -JX10c/5c -R0/100/0/30 -Ba50f25:"Recurrence time (yr)":/a10WSen:"Frequency (%)": -Z0 -Y6.5c -W7 -F -L0.5p -V -K -O >> ${model}/$ps
grep -v "#" ${model}/catalogue-all-2-5.dat | awk 'BEGIN{pre=0}(log($9)/log(10)>10){print $4/3.1536e7-pre;pre=$4/3.1536e7}'  | pshistogram -J -X15c -W14 -F -L0.5p -R0/200/0/20 -Ba100f50:"Recurrence time (yr)":/a10WSen:"Frequency (%)": -Z0 -V -K -O >> ${model}/$ps

#### plot histogram for event size
grep -v "#" ${model}/catalogue-all-2-5.dat | awk '{print log($9)/log(10)}' | pshistogram -JX10c/5c -R0/12/0/50 -Ba2f1:"event size":/a10WSen:"Frequency (%)": -Z0 -Y6.5c -W0.05 -F -L0.5p -V -K -O >> ${model}/$ps

ps2pdf ${model}/$ps ${model}/$pdf
open ${model}/$pdf 
