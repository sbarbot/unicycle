#!/bin/bash -e

model=vc_acc-miu0.1-Rb0.025-Ru1.83_dis_SZ-Rb0.24-Ru30

grep -v "#" faults/nankai2.flt.2d | awk '{print $3/1e3,1000,0-$4/1e3}' | psxyz -R-150/600/1000/1450/-276/0 -Jx0.02c/0.02c -Jz0.02c -E160/30 -P -Ba200f100/a200f100/a200f100SwenZ+ -W0.1p,black -K > ${model}/mesh-3D.ps

#### triangle elements
while read idx e22 e23 e33 l1 l2 l3
do
grep -v "#" faults/mantle.ned | awk -v n1=$l1 -v n2=$l2 -v n3=$l3 '(NR==n1 || NR==n2 || NR==n3){print $2/1e3,1000,0-$3/1e3}' | psxyz -R -J -Jz -E160/30 -L -W0.1p,black -K -O >>  ${model}/mesh-3D.ps
done < <(grep -v "#" faults/mantle.trv)

ps2pdf ${model}/mesh-3D.ps ${model}/mesh-3D.pdf
open ${model}/mesh-3D.pdf
