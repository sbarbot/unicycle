#!/bin/bash -e

model=vc_acc-miu0.1-Rb0.025-Ru1.83_dis_SZ-Rb0.24-Ru30

grep -v "#" faults/nankai2.flt.2d | awk '{print $3/1e3,0-$4/1e3}' | gmt psxy -R-150/600/-276/0 -Jx0.03c -W1p,black -K -Ba100f50WSen > ${model}/mesh.ps

#### GPS stations 
echo aa | awk '{for(i=1;i<=11;i++)print (i-1)*70-100,5}' | gmt psxy -R -J -N -K -O -St0.6c -Gblack >> ${model}/mesh.ps

#### triangle elements
while read idx e22 e23 e33 l1 l2 l3
do
grep -v "#" faults/mantle.ned | awk -v n1=$l1 -v n2=$l2 -v n3=$l3 '(NR==n1 || NR==n2 || NR==n3){print $2/1e3,0-$3/1e3}' | gmt psxy -R -J -L -W1p,black -K -O >>  ${model}/mesh.ps
done < <(grep -v "#" faults/mantle.trv)

#### all element index
#awk '($3>110-113/185*$2 && $3<130-113/185*$2){printf "%7.3f %7.3f 5 0 1 MC  %3d\n",$2,$3,$1}' ${model}/volume-cross-section-step10999.txt | gmt pstext -R -J -Gred -K -O >> ${model}/mesh.ps
#awk '($3>110-113/185*$2 && $3<130-113/185*$2){print $3,0-$2,$1}' ${model}/volume-cross-section-step10999.txt > faults/mantle_slop.trv

#echo aa | awk '{for(i=230;i<600;i++)print i,110-113/185*i}' | gmt psxy -R -J -K -O -W1p,blue >> ${model}/mesh.ps
#echo aa | awk '{for(i=230;i<600;i++)print i,130-113/185*i}' | gmt psxy -R -J -K -O -W1p,blue >> ${model}/mesh.ps
ps2pdf ${model}/mesh.ps ${model}/mesh.pdf
open ${model}/mesh.ps
