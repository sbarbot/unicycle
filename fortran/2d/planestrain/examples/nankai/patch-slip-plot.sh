#!/bin/bash -e

gmtset FRAME_WIDTH 0.5p \
       FRAME_PEN            0.5p  \
       TICK_LENGTH          0.1c  \
       LABEL_OFFSET         0.1c  \
       PLOT_DEGREE_FORMAT   DF    \
       ANNOT_FONT_SIZE_PRIMARY 9p \
       ANNOT_FONT_PRIMARY 0\
       LABEL_FONT_SIZE 12p \
       ANNOT_OFFSET_PRIMARY 0.1c  \
       D_FORMAT             %.6f \
       BASEMAP_TYPE plain

model=vc_acc-miu0.1-Rb0.025-Ru1.83_dis_SZ-Rb0.24-Ru30
dir=${model}/data 
#range=0/240/0/180
range=0/240/72/93
#range=0/240/0/93
for i in ${dir}/patch-*-*.dat; do n=`echo $i | awk -F "-" '{print $7}'`; grep -v "#" $i | awk -v n=$n '{print n,$2,$6}'; done | surface -R$range -I0.25/0.005 -G${model}/patch-slip-log10v.grd
#for i in ${dir}/patch-*-*.dat; do n=`echo $i | awk -F "-" '{print $7}'`; grep -v "#" $i | awk -v n=$n '{slp=$2;if(n<35) slp=slp*1.5; print n,slp,$6}'; done | surface -R$range -I0.25/0.005 -G${model}/patch-slip-log10v.grd
grdmap.sh -H -p -12/1/0.01 -b $range -t 5 -c cycles.cpt ${model}/patch-slip-log10v.grd
