#!/bin/bash -e

grdimage log10v.grd -R1400/2000/40975/41198 -JX8c/25c -Ba200f100:"Down-dip distance (km)":/a40f20:"Time step":wSEn -Ccycles.cpt -K -P >  SE-SSE-dynamics.ps
grdimage log10v-newcut-time.grd -R1400/2000/28695168000/28697760000 -JX8c/25c -Ba200f100:"Down-dip distance (km)":/a864000f432000:"Time (year)":wSEn -Ccycles.cpt -K -O -X11c >> SE-SSE-dynamics.ps

ps2pdf SE-SSE-dynamics.ps

grdimage log10v.grd -R1400/2000/49239/49432 -JX8c/25c -Ba200f100:"Down-dip distance (km)":/a40f20:"Time step":wSEn -Ccycles.cpt -K -P >  SE-SSE-dynamics1.ps
grdimage log10v-newcut1-time.grd -R1400/2000/34471958400/34476710400 -JX8c/25c -Ba200f100:"Down-dip distance (km)":/a864000f432000:"Time (year)":wSEn -Ccycles.cpt -K -O -X11c >> SE-SSE-dynamics1.ps

ps2pdf SE-SSE-dynamics1.ps
