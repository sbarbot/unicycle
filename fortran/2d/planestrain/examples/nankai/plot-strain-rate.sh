#!/bin/bash -e

model=vc_acc-miu0.1-Rb0.025-Ru1.83_dis_SZ-Rb0.24-Ru30
dir=${model}/data

for idx in 00000550 00000690
do

grep -v "#" ${dir}/volume-${idx}-${idx}.dat | awk 'NR>1{ekk=$8+$10;e22p=$8-ekk/2;e33p=$10-ekk/2;e=sqrt(e22p**2+2*$9**2+e33p**2);if(e<(1e-18)/(3.1536e7))e=(1e-18)/(3.1536e7);p=($5+$7)/2;s22p=$5-p;s33p=$7-p;tau=sqrt(s22p**2+2*$6**2+s33p**2); print $1/3.1536e7, (log(tau)-log(e))/log(10), log(tau)/log(10), log(e)/log(10)}' > ${model}/vol-time-${idx}.txt

done

awk '{print $1,$4}' ${model}/vol-time-00000550.txt | psxy -Ba100f50/a4f2 -R1000/1450/-23/-13 -JX25c/3c -W0.5p,black -K > vol-time.ps

awk '{print $1,$4}' ${model}/vol-time-00000799.txt | psxy -R -J -K -O -W0.5p,black -Y5c -B >>vol-time.ps
