#!/bin/bash -e

gmtset FRAME_WIDTH 0.5p \
       FRAME_PEN            0.5p  \
       TICK_LENGTH          0.1c  \
       LABEL_OFFSET         0.1c  \
       PLOT_DEGREE_FORMAT   DF    \
       ANNOT_FONT_SIZE_PRIMARY 9p \
       ANNOT_FONT_PRIMARY 0\
       LABEL_FONT_SIZE 12p \
       ANNOT_OFFSET_PRIMARY 0.1c  \
       D_FORMAT             %.0f \
       BASEMAP_TYPE plain

ps=ss-T_eta.ps
pdf=ss-T_eta.pdf
grep 1298.31 vc_acc-miu0.1-Rb0.025-Ru1.83_dis_SZ-Rb0.24-Ru30/volume-time-vert3.txt | awk '{print $3+6,$6}' | psxy -R20/24/-275/-30 -JX5c/10c -Ba1f0.5:"effctive viscosity (Pa s)":/a100f50:"Depth (km)":WSen -W1.5p,black -K >$ps
grep 1298.31 vc_acc-miu0.1-Rb0.025-Ru1.83_dis_SZ-Rb0.24-Ru30/volume-time-vert3.txt | awk '{print 10^$4*1000,$6}' | psxy -R0/400/-275/-30 -JX5c/10c -Ba100f50:"Strength (KPa)":/a100f50:"Depth (km)":wseN -W1.5p,red -K -O >>$ps

grep 1298.31 vc_acc-miu0.1-Rb0.025-Ru1.83_dis_SZ-Rb0.24-Ru30/volume-time-slop.txt | awk '{print $3+6,$6}' | psxy -K -O -X8c -W1.5p,black -R20/24/-275/-30 -JX5c/10c -Ba1f0.5:"effctive viscosity (Pa s)":/a100f50wSen >>$ps
grep 1298.31 vc_acc-miu0.1-Rb0.025-Ru1.83_dis_SZ-Rb0.24-Ru30/volume-time-slop.txt | awk '{print 10^$4*1000,$6}' | psxy -R0/400/-275/-30 -JX5c/10c -Ba100f50:"Strength (KPa)":/a100f50:"Depth (km)":wseN -W1.5p,red -K -O >>$ps

echo aa | awk '{for(i=30;i<280;i++)print i}' | awk 'function erf(x){a=0.140012;pi=atan2(1,0)*2;return sqrt(1-exp(-x^2*(4/pi+a*x^2)/(1+a*x^2)))}{
                x3=$1*1000;
                k=3.138;
                Cp=1171;
                rho=3330;
                Kappa=k/(rho*Cp);
                age=2e15;
                T=273+1400*erf(x3/(sqrt(4*Kappa*age)))
                printf "%f %f\n", T,0-$1;
        }' | psxy -R800/1700/-275/-30 -JX5c/10c -X8c -Ba500f100:"Temperature (K)":/a100f50wSen -K -O -W1.5p,red >>$ps


ps2pdf $ps
