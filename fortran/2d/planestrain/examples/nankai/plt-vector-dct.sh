#!/bin/bash -e

model=vc_acc-miu0.1-Rb0.025-Ru1.83_dis_SZ-Rb0.24-Ru30

grdvector ${model}/u2-dct.grd ${model}/u2-dct-n.grd -I42/8 -Gblack -J -Q0.06c/0.35c/0.1cn0.4c -S0.1c -K -O >> ${model}/u3-dct.grd-plot.ps
echo 0.5 0.5 | psxy -R0/1/0/1 -JX1c/1c -Y23c -Ba1f0.5 -K -O >> ${model}/u3-dct.grd-plot.ps
