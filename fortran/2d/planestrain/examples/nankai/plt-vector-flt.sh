#!/bin/bash -e

model=vc_acc-miu0.1-Rb0.025-Ru1.83_dis_SZ-Rb0.24-Ru30

grdvector ${model}/u2-flt.grd ${model}/u2-flt-n.grd -I42/8 -Gblack -J -Q0.06c/0.35c/0.1cn0.4c -S2.5c -K -O >> ${model}/u3-flt.grd-plot.ps
