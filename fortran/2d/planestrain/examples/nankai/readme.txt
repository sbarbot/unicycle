
# scripts used in
# Shi, Q., Barbot, S., Wei, S., Tapponnier, P., Matsuzawa, T. and Shibazaki, B., 2020. 
# Structural control and system-level behavior of the seismic cycle at the Nankai Trough. 
# Earth, Planets and Space, 72(1), pp.1-31.

# create a geometry file for pseudo-3D plotting of simulation time series
grep -v "#" faults/nankai.flt.2d | awk '{print $3,$4}' | awk -v n=`grdinfo -C acc_sz2/log10v.grd | awk '{print $5}'` '{for (i=1;i<=n;i++){print NR,i,-$2/1e3}}' | xyz2grd -Racc_sz2/log10v.grd -Gacc_sz2/geometry.grd
# plot in pseudo 3D
grdview.sh -H -p -12/1/0.01 -E 160/30 -v 20 -c creep.cpt -t 500 -G acc_sz2/log10v.grd acc_sz2/geometry.grd

# full view
grdview.sh -H -p -12/1/0.01 -E 160/30 -v 20 -c creep.cpt -t 500 -G brittle6/log10v.grd brittle6/geometry.grd
# zoom on long-term and short-term SSE
grdview.sh -H -b 800/1300/60000/65000/-60/-10 -p -12/1/0.01 -E 160/30 -v 20 -c creep.cpt -t 50 -G brittle6/log10v.grd brittle6/geometry.grd

# convert time step data to time series
wdir=acc_sz3;awk 'NR==FNR {a[NR]=$1; next} {print $1,a[1+($2-1)*20],$3}' <( grep -v "#" $wdir/time.txt) <(grd2xyz $wdir/log10v.grd) | blockmode -Eh -R1/1501/0/22857332273 -I1/10000000 | awk '{print $1,$2,$6}' | surface -R1/1501/0/22857332273 -I1/10000000 -G$wdir/log10v-time.grd

# convert to time series and plot with grdview
wdir=acc_sz4_mu0.4;awk 'NR==FNR {a[NR]=$1; next} {print $1,a[1+($2-1)*20],$3}' <( grep -v "#" $wdir/time.txt) <(grd2xyz $wdir/log10v.grd) | blockmode -Eh -R1/1201/0/2e10 -I1/1e7 | awk '{print $1,$2,$6}' | surface -R1/1201/0/2e10 -I1/1e7 -G$wdir/log10v-time.grd
map.sh -H -J X7i/23c -p -12/1/0.01 -c cycles.cpt acc_sz4_mu0.4/log10v-time.grd
wdir=acc_sz4_mu0.4;grep -v "#" $wdir/nankai2.flt.2d | awk '{print $3,$4}' | awk -v R=`grdinfo -C $wdir/log10v-time.grd | awk '{print $4","$5","$9}'` 'BEGIN{split(R,arr,",");n=arr[2];start=arr[1];inc=arr[3]}{for (i=start;i<=n;i=i+inc){print NR,i,-$2/1e3}}' | xyz2grd -R$wdir/log10v-time.grd -G$wdir/geometry-time.grd
grdview.sh -H -b 0/1200/0/2e10/-70/0 -p -12/1/0.01 -E 170/40 -v 20 -c creep.cpt -G $wdir/log10v-time.grd $wdir/geometry-time.grd

# time series of time steps (integer)
wdir=acc_sz4_mu0.4;grep -v "#" $wdir/nankai2.flt.2d | awk '{print $3,$4}' | awk -v R=`grdinfo -C $wdir/log10v.grd | awk '{print $4","$5","$9}'` 'BEGIN{split(R,arr,",");n=arr[2];start=arr[1];inc=arr[3]}{for (i=start;i<=n;i=i+inc){print NR,i,-$2/1e3}}' | xyz2grd -R$wdir/log10v.grd -G$wdir/geometry.grd
grdview.sh -H -b 1/1201/0/134400/-70/0 -p -12/1/0.01 -E 170/40 -v 20 -c creep.cpt -G $wdir/log10v.grd $wdir/geometry.grd
