#!/bin/bash -e

here=`pwd`
model=vc_acc-miu0.1-Rb0.025-Ru1.83_dis_SZ-Rb0.24-Ru30
dir=${model}/data

#### vertical displacement (select a period 1150-1425yr, and remove the trend between step (54939--62488)*20)
#for i in ${dir}/opts-????.dat; do n=`echo $i | awk -F "-" '{print -100+$7*7}'`; x1=`awk '(NR==54939){print $1/3.1536e7}' $i`; x2=`awk '(NR==62488){print $1/3.1536e7}' $i`; y1=`awk '(NR==54939){print $3}' $i`; y2=`awk '(NR==62488){print $3}' $i`;grep -v "#" $i | awk -v n=$n -v x1=$x1 -v x2=$x2 -v y1=$y1 -v y2=$y2 '($1/3.1536e7>1150 && $1/3.1536e7<1425){if(NR%20==0) print n,$1/3.1536e7,0-($3-(y2-y1)/(x2-x1)*($1/3.1536e7-x1)-y1)}'; done | surface -R-100/600/1150/1425 -I7/1 -G${model}/u3-all.grd
#for i in ${dir}/opts-????.dat; do n=`echo $i | awk -F "-" '{print -100+$7*7}'`; x1=`awk '(NR==54939){print $1/3.1536e7}' $i`; x2=`awk '(NR==62488){print $1/3.1536e7}' $i`; y1=`awk '(NR==54939){print $5}' $i`; y2=`awk '(NR==62488){print $5}' $i`;grep -v "#" $i | awk -v n=$n -v x1=$x1 -v x2=$x2 -v y1=$y1 -v y2=$y2 '($1/3.1536e7>1150 && $1/3.1536e7<1425){if(NR%20==0) print n,$1/3.1536e7,0-($5-(y2-y1)/(x2-x1)*($1/3.1536e7-x1)-y1)}'; done | surface -R-100/600/1150/1425 -I7/1 -G${model}/u3-flt.grd
#for i in ${dir}/opts-????.dat; do n=`echo $i | awk -F "-" '{print -100+$7*7}'`; x1=`awk '(NR==54939){print $1/3.1536e7}' $i`; x2=`awk '(NR==62488){print $1/3.1536e7}' $i`; y1=`awk '(NR==54939){print $7}' $i`; y2=`awk '(NR==62488){print $7}' $i`;grep -v "#" $i | awk -v n=$n -v x1=$x1 -v x2=$x2 -v y1=$y1 -v y2=$y2 '($1/3.1536e7>1150 && $1/3.1536e7<1425){if(NR%20==0) print n,$1/3.1536e7,0-($7-(y2-y1)/(x2-x1)*($1/3.1536e7-x1)-y1)}'; done | surface -R-100/600/1150/1425 -I7/1 -G${model}/u3-dct.grd


#### horizontal displacement (select a period 1150-1425yr, and remove the trend between step (54939--62488)*20)
#for i in ${dir}/opts-????.dat; do n=`echo $i | awk -F "-" '{print -100+$7*7}'`; x1=`awk '(NR==54939){print $1/3.1536e7}' $i`; x2=`awk '(NR==62488){print $1/3.1536e7}' $i`; y1=`awk '(NR==54939){print $2}' $i`; y2=`awk '(NR==62488){print $2}' $i`;grep -v "#" $i | awk -v n=$n -v x1=$x1 -v x2=$x2 -v y1=$y1 -v y2=$y2 '($1/3.1536e7>1150 && $1/3.1536e7<1425){if(NR%20==0) print n,$1/3.1536e7,$2-(y2-y1)/(x2-x1)*($1/3.1536e7-x1)-y1}'; done | surface -R-100/600/1150/1425 -I7/1 -G${model}/u2-all.grd
#for i in ${dir}/opts-????.dat; do n=`echo $i | awk -F "-" '{print -100+$7*7}'`; x1=`awk '(NR==54939){print $1/3.1536e7}' $i`; x2=`awk '(NR==62488){print $1/3.1536e7}' $i`; y1=`awk '(NR==54939){print $4}' $i`; y2=`awk '(NR==62488){print $4}' $i`;grep -v "#" $i | awk -v n=$n -v x1=$x1 -v x2=$x2 -v y1=$y1 -v y2=$y2 '($1/3.1536e7>1150 && $1/3.1536e7<1425){if(NR%20==0) print n,$1/3.1536e7,$4-(y2-y1)/(x2-x1)*($1/3.1536e7-x1)-y1}'; done | surface -R-100/600/1150/1425 -I7/1 -G${model}/u2-flt.grd
#for i in ${dir}/opts-????.dat; do n=`echo $i | awk -F "-" '{print -100+$7*7}'`; x1=`awk '(NR==54939){print $1/3.1536e7}' $i`; x2=`awk '(NR==62488){print $1/3.1536e7}' $i`; y1=`awk '(NR==54939){print $6}' $i`; y2=`awk '(NR==62488){print $6}' $i`;grep -v "#" $i | awk -v n=$n -v x1=$x1 -v x2=$x2 -v y1=$y1 -v y2=$y2 '($1/3.1536e7>1150 && $1/3.1536e7<1425){if(NR%20==0) print n,$1/3.1536e7,$6-(y2-y1)/(x2-x1)*($1/3.1536e7-x1)-y1}'; done | surface -R-100/600/1150/1425 -I7/1 -G${model}/u2-dct.grd


#### plot displacement
#grdmap.sh -H -p -4/4/0.01      -c anatolia.cpt -t 50 ${model}/u3-all.grd
grdmap.sh -H -p -0.7/0.7/0.01     -c anatolia.cpt -t 50 -e ${here}/plt-vector-flt.sh ${model}/u3-flt.grd
grdmap.sh -H -p -0.1/0.1/0.001  -c anatolia.cpt -t 50 -e ${here}/plt-vector-dct.sh ${model}/u3-dct.grd

#grdmap.sh -H -p -4/4/0.001     -c anatolia.cpt -t 50 ${model}/u2-all.grd
#grdmap.sh -H -p -4/4/0.001     -c anatolia.cpt -t 50 -e ${here}/plt-vector.sh ${model}/u2-flt.grd
#grdmap.sh -H -p -0.1/0.1/0.001 -c anatolia.cpt -t 50 ${model}/u2-dct.grd
