#!/bin/bash -e
###############################################################
#PBS -S /bin/bash
#PBS -N vc_acc-miu0.1-Rb0.025-Ru1.83_dis_SZ-Rb0.24-Ru30
#PBS -j oe
#PBS -o /home/qshi003/unicycle/examples/Nankai/$PBS_JOBID.o
#PBS -q q24
#PBS -l nodes=5:ppn=24,walltime=20:00:00:00
###############################################################

WDIR=vc_acc-miu0.1-Rb0.025-Ru1.83_dis_SZ-Rb0.24-Ru30_onlyACC

FLT=faults/nankai2.flt.2d
STV=faults/mantle

NV=$(grep -cv "#" $STV.trv)

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

echo "" | awk 'BEGIN{i=1;pi=atan2(1,0)*2}{
        x2o=0;
        x3o=0;
        width=125;
        dip=2.51744;
        N=334;
        seg=1;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.2e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        dip=7.8278226;
        N=1190;
        seg=2;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.8e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        dip=15.5623;
        N=192;
        seg=3;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.8e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        dip=26.3105;
        N=258;
        seg=4;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.8e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
                x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        dip=32.4577;
        N=428;
        seg=5;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.8e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
}' > $FLT

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli
30e3 30e3
# time interval
1e11
# number of patches
`grep -v "#" $FLT | wc -l`
# n  Vpl           x2           x3    width      dip
`grep -v "#" $FLT`
# number of frictional patches
`grep -v "#" $FLT | wc -l`
#  n        tau0        mu0        sig          a          b          L         Vo      G/2Vs
`grep -v "#" $FLT | gawk '{\
	if($7==1) {        \
		depth=$4;  \
		tau0=-1;    \
		mu0=0.1;   \
		sig=35;    \
		a=5.85e-2;    \
		b=6.00e-2; \
		L=4e-2;   \
		Vo=1e-4;   \
	}else if($7==2) {  \
                depth=$4;  \
                tau0=-1;    \
                mu0=0.6;   \
                sig=20;    \
                a=1e-2;    \
                b=0.8e-2; \
                L=0.8e-2;  \
                Vo=1e-4;   \
        }else if($7==3) {  \
                depth=$4;  \
                tau0=-1;    \
                mu0=0.6;   \
                sig=20;    \
                a=2e-3;    \
                b=1e-3;  \
                L=4.1e-3;  \
                Vo=1e-4;   \
        }else if($7==4) {  \
                depth=$4;  \
                tau0=-1;    \
                mu0=0.6;   \
                sig=20;    \
                a=2e-3;    \
                b=1e-3; \
                L=7e-4;  \
                Vo=1e-4;   \
	}else{             \
                depth=$4;  \
                tau0=-1;    \
                mu0=0.6;   \
                sig=40;    \
                a=1e-2;    \
                b=0.8e-2;    \
                L=2.5e-2;    \
                Vo=1e-4;   \
        }
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,5;\
	}'`
# number of rectangle volumes
0
# number of triangle volumes
`grep -v "#" $STV.trv | wc -l`
# n e22 e23 e33 i1 i2 i3
`grep -v "#" $STV.trv`
# number of vertices
`grep -v "#" $STV.ned | wc -l`
# n       x2     x3
`grep -v "#" $STV.ned`
# number of nonlinear triangle strain volumes
`grep -v "#" $STV.trv | wc -l`
# n s22  s23  s33 gammadot0m   n     Q     R
`grep -v "#" $STV.ned | awk 'NR==FNR {
	for (j=2;j<=3;j++){a[j,0+NR]=$j}; next} { 
		A2=a[2,0+$5];
		A3=a[3,0+$5];
		B2=a[2,0+$6];
		B3=a[3,0+$6];
		C2=a[2,0+$7];
		C3=a[3,0+$7];
		x2=(A2+B2+C2)/3.0;
		x3=(A3+B3+C3)/3.0;
		print x2,x3;
	}' - <( grep -v "#" $STV.trv ) | awk '{
		x2=$1;
		x3=$2;
		g=9.8;
		gammadot0=3.6e5;
		n=3.5;
		rho=3330;
		P=rho*g*x3;
		Vol=11e-6;
		H=460e3+P*Vol;
		R=8.314;
		printf "%4d %f %f %f %f %f %f %f\n", NR,-1,-1,-1,gammadot0,n,H,R;
	}'`
# number of thermal properties
`grep -v "#" $STV.trv | wc -l`
# n  rhoc  temperature
`grep -v "#" $STV.ned | awk 'NR==FNR {
	for (j=2;j<=3;j++){a[j,0+NR]=$j}; next} { 
		A2=a[2,0+$5];
		A3=a[3,0+$5];
		B2=a[2,0+$6];
		B3=a[3,0+$6];
		C2=a[2,0+$7];
		C3=a[3,0+$7];
		x2=(A2+B2+C2)/3.0;
		x3=(A3+B3+C3)/3.0;
		print x2,x3;
	}' - <( grep -v "#" $STV.trv ) | awk 'function erf(x){a=0.140012;pi=atan2(1,0)*2;return sqrt(1-exp(-x^2*(4/pi+a*x^2)/(1+a*x^2)))}{
		x2=$1;
		x3=$2;
		k=3.138; 
		Cp=1171;
		rho=3330;
		Kappa=k/(rho*Cp);
		age=2e15;
		T=273+1400*erf(x3/(sqrt(4*Kappa*age)))
		printf "%d %f %f\n", NR,1,T;
	}'`
# number of observation patches
240
# n index rate
`echo "" | awk '{
        for (i=0;i<240;i++){
                printf "%4d %4d 20\n", i+1,i*10+1;
        }
}'`
# number of observation volumes
`grep -v "#" $STV.trv | wc -l | awk '{print $1}'`
# n index rate
`echo "" | awk -v n=$NV '{for (i=1;i<=n;i++){
	print i,i,100
}}'`
# number of observation points
100
# n name    x2    x3a   rate
`echo "" | awk '{
	for (i=0;i<100;i++){
		printf "%4d %04d %e 0e3 20\n", i+1,i+1,-100e3+i*7e3+10;
	}
}'`
# number of events
0
EOF

if [ -e "$WDIR/greens-0000.grd" ]; then
	IMPORTGREENS="--import-greens $WDIR"
fi

NPROCS=`wc -l < $PBS_NODEFILE`

time mpirun -machinefile $PBS_NODEFILE -np $NPROCS \
        unicycle-ps-viscouscycles \
	--maximum-step 3.15e7 \
        --friction-law 1 \
	--export-netcdf \
	--export-greens $WDIR \
	--maximum-iterations 25000000 \
	$IMPORTGREENS \
	$* $WDIR/in.param

