#!/bin/bash -e

# Earthquake and SSE cycles in plane strain viscoelastic media in Nankai, Japan.

selfdir=$(dirname $0)

WDIR=$selfdir/viscous6

FLT=faults/nankai.flt.2d
STV=$selfdir/faults/mantle

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

echo "" | awk 'BEGIN{i=1;pi=atan2(1,0)*2}{
        x2o=0;
        x3o=0;
        width=200;
        dip=2.51744;
        N=209;
        seg=1;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.8e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        width=200;
        dip=7.8278226;
        N=744;
        seg=2;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.8e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        width=200;
        dip=15.5623;
        N=120;
        seg=3;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.8e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        width=200;
        dip=26.3105;
        N=161;
        seg=4;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.8e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        width=200;
        dip=32.4577;
        N=267;
        seg=5;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.8e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
}' > $FLT

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli
30e3 30e3
# time interval
3e10
# number of patches
`grep -v "#" $FLT | wc -l`
# n  Vpl           x2           x3    width      dip
`grep -v "#" $FLT`
# number of frictional patches
`grep -v "#" $FLT | wc -l`
#  n        tau0        mu0        sig          a          b          L         Vo      G/2Vs
`grep -v "#" $FLT | gawk '{ 
        switch ($7) { 
	case 1:
		depth=$4; 
		tau0=-1; 
		mu0=0.2;
		sig=20;
		a=1e-2;
		b=1.25e-2;
		L=2e-1;
		Vo=1e-6;
		break;
	case 2:
                depth=$4;
                tau0=-1;
                mu0=0.2;
                sig=30;
                a=1e-2;
                b=1.22e-2;
                L=1.5e-2;
                Vo=1e-6;
		break;
	case 3:
                depth=$4;
                tau0=-1;
                mu0=0.2;
                sig=10;
                a=1e-2;
                b=1.1e-2;
                L=4.2e-3;
                Vo=1e-6;
		break;
	case 4:
                depth=$4;
                tau0=-1;
                mu0=0.2;
                sig=10;
                a=1e-2;
                b=1.04e-2;
                L=2.2e-4;
                Vo=1e-6;
		break;
	case 5:
                depth=$4;
                tau0=-1;
                mu0=0.2;
                sig=10;
                a=1e-2;
                b=2e-3;
                L=2e-1;
                Vo=1e-6;
		break;
	default:
		print "error";
        }
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,5;\
	}'`
# number of rectangle volumes
0
# number of triangle volumes
`grep -v "#" $STV.trv | wc -l`
# n e22 e23 e33 i1 i2 i3
`grep -v "#" $STV.trv`
# number of vertices
`grep -v "#" $STV.ned | wc -l`
# n       x2     x3
`grep -v "#" $STV.ned`
# number of nonlinear triangle strain volumes
`grep -v "#" $STV.trv | wc -l`
# n s22  s23  s33 gammadot0m   n     Q     R
`grep -v "#" $STV.ned | awk 'NR==FNR {
	for (j=2;j<=3;j++){a[j,0+NR]=$j}; next} { 
		A2=a[2,0+$5];
		A3=a[3,0+$5];
		B2=a[2,0+$6];
		B3=a[3,0+$6];
		C2=a[2,0+$7];
		C3=a[3,0+$7];
		x2=(A2+B2+C3)/3.0;
		x3=(A3+B3+C3)/3.0;
		print x2,x3;
	}' - <( grep -v "#" $STV.trv ) | awk '{
		x2=$1;
		x3=$2;
		g=9.8;
		gammadot0=1e-3;
		n=1;
		rho=3330;
		P=rho*g*x3;
		Vol=4e-6;
		H=335e3+P*Vol;
		R=8.314;
		printf "%4d %f %f %f %f %f %f %f\n", NR,-1,-1,-1,gammadot0,n,H,R;
	}'`
# number of thermal properties
`grep -v "#" $STV.trv | wc -l`
# n  rhoc  temperature
`grep -v "#" $STV.ned | awk 'NR==FNR {
	for (j=2;j<=3;j++){a[j,0+NR]=$j}; next} { 
		A2=a[2,0+$5];
		A3=a[3,0+$5];
		B2=a[2,0+$6];
		B3=a[3,0+$6];
		C2=a[2,0+$7];
		C3=a[3,0+$7];
		x2=(A2+B2+C3)/3.0;
		x3=(A3+B3+C3)/3.0;
		print x2,x3;
	}' - <( grep -v "#" $STV.trv ) | awk 'function erf(x){a=0.140012;pi=atan2(1,0)*2;return sqrt(1-exp(-x^2*(4/pi+a*x^2)/(1+a*x^2)))}{
		x2=$1;
		x3=$2;
		k=3.138; 
		Cp=1171;
		rho=3330;
		Kappa=k/(rho*Cp);
		age=2e15;
		T=273+1400*erf(x3/(sqrt(4*Kappa*age)))
		printf "%d %f %f\n", NR,1,T;
	}'`
# number of observation patches
8
# n index rate
  1    50    1
  2   100    1
  3   215    1
  4   500    1
  5   800    1
  6  1000    1
  7  1150    1
  8  1300    1
# number of observation volumes
69
# n index rate
# profile parallel to slab in mantle wedge
  1   675   20
  2   676   20
  3   672   20
  4   678   20
  5   680   20
  6   681   20
  7   677   20
  8   737   20
  9   735   20
 10   807   20
 11   806   20
 12   805   20
 13   689   20
 14   691   20
 15   690   20
 16   793   20
 17   792   20
 18   800   20
 19   799   20
 20   798   20
 21   725   20
 22   726   20
 23   688   20
 24   759   20
 25   760   20
 26   818   20
 27   817   20
 28   815   20
 29   758   20
 30   711   20
 31   710   20
 32   712   20
 33   762   20
 34   763   20
 35   764   20
 36   716   20
 37   859   20
 38   858   20
 39   856   20
 40   857   20
 41   774   20
 42   685   20
 43   684   20
# depth profile in oceanic mantle
 44   201   20
 45   202   20
 46   204   20
 47   203   20
 48   205   20 
 49   206   20
 50   207   20
 51   513   20
 52   514   20
 53   549   20
 54   549   20
 55   540   20
 56   641   20
 57   642   20
 58   639   20
 59   548   20
 60   545   20
 61   525   20
 62   226   20
 63   394   20
 64   393   20
 65   316   20
 66   534   20
 67   536   20
 68   535   20
 69   221   20
# number of observation points
100
# n name    x2    x3 rate
`echo "" | awk '{
	for (i=0;i<100;i++){
		printf "%4d %04d %e 0e3 20\n", i+1,i+1,-100e3+i*7e3+10;
	}
}'`
# number of events
0
EOF

if [ -e "$WDIR/greens-0000.grd" ]; then
	IMPORTGREENS="--import-greens $WDIR"
fi

time mpirun -n 2 unicycle-ps-viscouscycles \
	--maximum-step 3.15e7 \
	--export-netcdf \
	--export-greens $WDIR \
	--maximum-iterations 20000000 \
	$IMPORTGREENS \
	$* $WDIR/in.param


