#!/bin/bash -e

smooth=0
prepareGRD=0
model=vc_acc-miu0.1-Rb0.025-Ru1.83_dis_SZ-Rb0.24-Ru30
dir=${model}/data
rng_oce=-150/570/-276/-50
rng_con=230/600/-256/-30

#for step in 11075 11147
for step in 10999 11200 11600 ## 10999 during EQ; 11200  afterslip; 11600 interseismic
do

if [ $prepareGRD -gt 0 ]
then
#### generate a list of strain rate, shear stress and viscosity for all triangle elements

while read idx e22 e23 e33 l1 l2 l3
do 

x1=`grep -v "#" faults/mantle.ned | awk -v n=$l1 'NR==n{print $2/1e3}'`
y1=`grep -v "#" faults/mantle.ned | awk -v n=$l1 'NR==n{print $3/1e3}'`
x2=`grep -v "#" faults/mantle.ned | awk -v n=$l2 'NR==n{print $2/1e3}'`
y2=`grep -v "#" faults/mantle.ned | awk -v n=$l2 'NR==n{print $3/1e3}'`
x3=`grep -v "#" faults/mantle.ned | awk -v n=$l3 'NR==n{print $2/1e3}'`
y3=`grep -v "#" faults/mantle.ned | awk -v n=$l3 'NR==n{print $3/1e3}'`

grep -v "#" ${dir}/volume-000${idx}-000${idx}.dat | \
    awk -v x1=$x1 -v x2=$x2 -v x3=$x3 -v y1=$y1 -v y2=$y2 -v y3=$y3 -v id=$idx -v st=$step \
        'NR==st{ekk=$8+$10;e22p=$8-ekk/2;e33p=$10-ekk/2;e=sqrt(e22p**2+2*$9**2+e33p**2);
                if(e<(1e-18)/(3.1536e7))e=(1e-18)/(3.1536e7);p=($5+$7)/2;
                s22p=$5-p;s33p=$7-p;tau=sqrt(s22p**2+2*$6**2+s33p**2); 
                print id,(x1+x2+x3)/3,0-(y1+y2+y3)/3,$8,$9,$10,
                (log(tau)-log(e))/log(10),log(tau)/log(10),log(e)/log(10),x1,0-y1,x2,0-y2,x3,0-y3}'

done < <(grep -v "#" faults/mantle.trv) > ${model}/volume-cross-section-step${step}.txt

#### generate grd files for both oceanic and continental mantle wedge
awk '{print $2,$3,$4}' ${model}/volume-cross-section-step${step}.txt | surface -R$rng_oce -I3/3 -G${model}/e22-step${step}.grd
awk '{print $2,$3,$4}' ${model}/volume-cross-section-step${step}.txt | surface -R$rng_con -I3/3 -G${model}/e22-continental-step${step}.grd
awk '{print $2,$3,$5}' ${model}/volume-cross-section-step${step}.txt | surface -R$rng_oce -I3/3 -G${model}/e23-step${step}.grd
awk '{print $2,$3,$5}' ${model}/volume-cross-section-step${step}.txt | surface -R$rng_con -I3/3 -G${model}/e23-continental-step${step}.grd
awk '{print $2,$3,$6}' ${model}/volume-cross-section-step${step}.txt | surface -R$rng_oce -I3/3 -G${model}/e33-step${step}.grd
awk '{print $2,$3,$6}' ${model}/volume-cross-section-step${step}.txt | surface -R$rng_con -I3/3 -G${model}/e33-continental-step${step}.grd
awk '{print $2,$3,$7}' ${model}/volume-cross-section-step${step}.txt | surface -R$rng_oce -I3/3 -G${model}/eta-step${step}.grd
awk '{print $2,$3,$7}' ${model}/volume-cross-section-step${step}.txt | surface -R$rng_con -I3/3 -G${model}/eta-continental-step${step}.grd
awk '{print $2,$3,$8}' ${model}/volume-cross-section-step${step}.txt | surface -R$rng_oce -I3/3 -G${model}/tau-step${step}.grd
awk '{print $2,$3,$8}' ${model}/volume-cross-section-step${step}.txt | surface -R$rng_con -I3/3 -G${model}/tau-continental-step${step}.grd
awk '{print $2,$3,$9}' ${model}/volume-cross-section-step${step}.txt | surface -R$rng_oce -I3/3 -G${model}/e-step${step}.grd
awk '{print $2,$3,$9}' ${model}/volume-cross-section-step${step}.txt | surface -R$rng_con -I3/3 -G${model}/e-continental-step${step}.grd
fi


#### prepare CPT
cpt_eta="/Users/qshi003/Documents/src/relax/share/ViBlGrWhYeOrRe_reverse.cpt"
cpt_tau="/Users/qshi003/Documents/src/relax/share/deepsea.cpt"
cpt_e="/Users/qshi003/Documents/src/relax/share/sunset.cpt"
cpt_e22="/Users/qshi003/Documents/src/relax/share/jet.cpt"
cpt_e23="/Users/qshi003/Documents/src/relax/share/jet.cpt"
cpt_e33="/Users/qshi003/Documents/src/relax/share/jet.cpt"

if [ $smooth -gt 0 ]
then
low1=`grdinfo -C ${model}/eta-step${step}.grd | awk '{print $6}'` 
low2=`grdinfo -C ${model}/eta-continental-step${step}.grd | awk '{print $6}'`
hig1=`grdinfo -C ${model}/eta-step${step}.grd | awk '{print $7}'`
hig2=`grdinfo -C ${model}/eta-continental-step${step}.grd | awk '{print $7}'`
range1=`echo $low1 $low2 $hig1 $hig2 | awk '{l=$1;h=$3;if($2<$1)l=$2;if($3<$4)h=$4;printf "%.0f\/%.0f",l,h}'`
low1=`grdinfo -C ${model}/tau-step${step}.grd | awk '{print $6}'`
low2=`grdinfo -C ${model}/tau-continental-step${step}.grd | awk '{print $6}'`
hig1=`grdinfo -C ${model}/tau-step${step}.grd | awk '{print $7}'`
hig2=`grdinfo -C ${model}/tau-continental-step${step}.grd | awk '{print $7}'`
range2=`echo $low1 $low2 $hig1 $hig2 | awk '{l=$1;h=$3;if($2<$1)l=$2;if($3<$4)h=$4;printf "%.0f\/%.0f",l,h}'`
low1=`grdinfo -C ${model}/e-step${step}.grd | awk '{print $6}'`
low2=`grdinfo -C ${model}/e-continental-step${step}.grd | awk '{print $6}'`
hig1=`grdinfo -C ${model}/e-step${step}.grd | awk '{print $7}'`
hig2=`grdinfo -C ${model}/e-continental-step${step}.grd | awk '{print $7}'`
range3=`echo $low1 $low2 $hig1 $hig2 | awk '{l=$1;h=$3;if($2<$1)l=$2;if($3<$4)h=$4;printf "%.0f\/%.0f",l,h}'`
low1=`grdinfo -C ${model}/e22-step${step}.grd | awk '{print $6}'`
low2=`grdinfo -C ${model}/e22-continental-step${step}.grd | awk '{print $6}'`
hig1=`grdinfo -C ${model}/e22-step${step}.grd | awk '{print $7}'`
hig2=`grdinfo -C ${model}/e22-continental-step${step}.grd | awk '{print $7}'`
range4=`echo $low1 $low2 $hig1 $hig2 | awk '{l=$1;h=$3;if($2<$1)l=$2;if($3<$4)h=$4;printf "%.2e\/%.2e",l,h}'`
low1=`grdinfo -C ${model}/e23-step${step}.grd | awk '{print $6}'`
low2=`grdinfo -C ${model}/e23-continental-step${step}.grd | awk '{print $6}'`
hig1=`grdinfo -C ${model}/e23-step${step}.grd | awk '{print $7}'`
hig2=`grdinfo -C ${model}/e23-continental-step${step}.grd | awk '{print $7}'`
range5=`echo $low1 $low2 $hig1 $hig2 | awk '{l=$1;h=$3;if($2<$1)l=$2;if($3<$4)h=$4;printf "%.2e\/%.2e",l,h}'`
low1=`grdinfo -C ${model}/e33-step${step}.grd | awk '{print $6}'`
low2=`grdinfo -C ${model}/e33-continental-step${step}.grd | awk '{print $6}'`
hig1=`grdinfo -C ${model}/e33-step${step}.grd | awk '{print $7}'`
hig2=`grdinfo -C ${model}/e33-continental-step${step}.grd | awk '{print $7}'`
range6=`echo $low1 $low2 $hig1 $hig2 | awk '{l=$1;h=$3;if($2<$1)l=$2;if($3<$4)h=$4;printf "%.2e\/%.2e",l,h}'`

else
range4=`minmax -C ${model}/volume-cross-section-step${step}.txt | awk '{printf "%.2e\/%.2e",$7,$8}'`
range5=`minmax -C ${model}/volume-cross-section-step${step}.txt | awk '{printf "%.2e\/%.2e",$9,$10}'`
range6=`minmax -C ${model}/volume-cross-section-step${step}.txt | awk '{printf "%.2e\/%.2e",$11,$12}'`
range1=`minmax -C ${model}/volume-cross-section-step${step}.txt | awk '{printf "%.0f\/%.0f",$13,$14}'`
range2=`minmax -C ${model}/volume-cross-section-step${step}.txt | awk '{printf "%.2f\/%.2f",$15,$16}'`
range3=`minmax -C ${model}/volume-cross-section-step${step}.txt | awk '{printf "%.0f\/%.0f",$17,$18}'`
fi

echo $range1 $range2 $range3 $range4 $range5 $range6
#makecpt -C${cpt_eta} -T${range1}/0.01 -D> eta.cpt
#makecpt -C${cpt_tau} -T${range2}/0.01 -Ic -D> tau.cpt
#makecpt -C${cpt_e} -T${range3}/0.01 -Ic -D> e.cpt
#makecpt -C${cpt_e22} -T${range4}/1e-18 -D> e22.cpt
#makecpt -C${cpt_e23} -T${range5}/1e-18 -D> e23.cpt
#makecpt -C${cpt_e33} -T${range6}/1e-18 -D> e33.cpt
makecpt -C${cpt_eta} -T13/17/0.01 -D> eta.cpt
#makecpt -C${cpt_eta} -T13/24/0.01 -D> eta.cpt
makecpt -C${cpt_tau} -T-3/0/0.01 -Ic -D> tau.cpt
makecpt -C${cpt_e}   -T-25/-13/0.01 -Ic -D> e.cpt
makecpt -C${cpt_e22} -T-2e-16/2e-16/1e-18 -D> e22.cpt
makecpt -C${cpt_e23} -T-2e-16/2e-16/1e-18 -D> e23.cpt
makecpt -C${cpt_e33} -T-2e-16/2e-16/1e-18 -D> e33.cpt



########### Plot

for par in eta
do

if [ $par == "eta" ]
then
    cln=7
    color="a2f1"
elif [ $par == "tau" ]
then
    cln=8
    color="a1f0.5"
elif [ $par == "e" ]
then
    cln=9
    color="a4f2"
elif [ $par == "e22" ]
then
    cln=4
    color="a2e-16f1e-16"
elif [ $par == "e23" ]
then
    cln=5
    color="a2e-16f1e-16"
else
    cln=6
    color="a2e-16f1e-16"
fi


if [ $smooth -gt 0 ]
then

####### use the grd files to plot
### clip for oceanic mantle
psclip -R-150/600/-276/0 -Jx0.03c -Ba200f100WSen -K > ${model}/${par}-step${step}-smooth${smooth}.ps <<EOF
-150 -50
-150 -276
570 -276
200 -50
EOF
grdimage ${model}/${par}-step${step}.grd -R -J -C${par}.cpt -K -O >> ${model}/${par}-step${step}-smooth${smooth}.ps
psclip -C -O -K >> ${model}/${par}-step${step}-smooth${smooth}.ps

### clip for continental mantle
psclip -R-150/600/-276/0 -Jx0.03c -K -O >> ${model}/${par}-step${step}-smooth${smooth}.ps <<EOF
230 -30
600 -256
600 -30
EOF
grdimage ${model}/${par}-continental-step${step}.grd -R -J -C${par}.cpt -K -O >> ${model}/${par}-step${step}-smooth${smooth}.ps
psclip -C -O -K >> ${model}/${par}-step${step}-smooth${smooth}.ps


else

####### plot colors on all elements
awk -v n=$cln '{print "> -Z "$n"\n"$10,$11"\n"$12,$13"\n"$14,$15}' ${model}/volume-cross-section-step${step}.txt | \
    psxy -R-150/600/-276/0 -Jx0.03c -Ba100f50WSen -C${par}.cpt -L -M -W1p,black -K > ${model}/${par}-step${step}-smooth${smooth}.ps

fi

awk '{print $3/1e3,0-$4/1e3}' faults/nankai2.flt.2d | psxy -R -J -K -O -W2p,black >> ${model}/${par}-step${step}-smooth${smooth}.ps
awk '($7==2){print $3/1e3,0-$4/1e3}' faults/nankai2.flt.2d | psxy -R -J -K -O -W2p,red >> ${model}/${par}-step${step}-smooth${smooth}.ps
#echo aa | awk '{for(i=1;i<=11;i++)print (i-1)*70-100,5"\n"}' | psxy -R -J -B -N -K -O -St0.6c -Gblack >> ${model}/${par}-step${step}-smooth${smooth}.ps
psscale -D10c/15c/10c/1ch -E -B$color -C${par}.cpt -O >> ${model}/${par}-step${step}-smooth${smooth}.ps

ps2pdf ${model}/${par}-step${step}-smooth${smooth}.ps ${model}/${par}-step${step}-smooth${smooth}.pdf
open ${model}/${par}-step${step}-smooth${smooth}.pdf


done

done

rm *.cpt
