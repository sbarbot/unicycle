#!/bin/bash -e

smooth=0
prepareGRD=0
model=vc_acc-miu0.1-Rb0.025-Ru1.83_dis_SZ-Rb0.24-Ru30
dir=${model}/data
rng_oce=-150/570/-276/-50
rng_con=230/600/-256/-30
rng_color="-4e-16/4e-16/1e-17"
color="a4e-16f1e-16"

for step in 11165 11191 11220 11461 11605 12101 12117 12141 12375 12477
do

if [ $prepareGRD -gt 0 ]
then
#### generate a list of strain rate, shear stress and viscosity for all triangle elements

while read idx e22 e23 e33 l1 l2 l3
do 

x1=`grep -v "#" faults/mantle.ned | awk -v n=$l1 'NR==n{print $2/1e3}'`
y1=`grep -v "#" faults/mantle.ned | awk -v n=$l1 'NR==n{print $3/1e3}'`
x2=`grep -v "#" faults/mantle.ned | awk -v n=$l2 'NR==n{print $2/1e3}'`
y2=`grep -v "#" faults/mantle.ned | awk -v n=$l2 'NR==n{print $3/1e3}'`
x3=`grep -v "#" faults/mantle.ned | awk -v n=$l3 'NR==n{print $2/1e3}'`
y3=`grep -v "#" faults/mantle.ned | awk -v n=$l3 'NR==n{print $3/1e3}'`

grep -v "#" ${dir}/volume-000${idx}-000${idx}.dat | \
    awk -v x1=$x1 -v x2=$x2 -v x3=$x3 -v y1=$y1 -v y2=$y2 -v y3=$y3 -v id=$idx -v st=$step \
        'NR==st{ekk=$8+$10;e22p=$8-ekk/2;e33p=$10-ekk/2;e=sqrt(e22p**2+2*$9**2+e33p**2);
                if(e<(1e-18)/(3.1536e7))e=(1e-18)/(3.1536e7);p=($5+$7)/2;
                s22p=$5-p;s33p=$7-p;tau=sqrt(s22p**2+2*$6**2+s33p**2); 
                print id,(x1+x2+x3)/3,0-(y1+y2+y3)/3,$8,$9,$10,
                (log(tau)-log(e))/log(10),log(tau)/log(10),log(e)/log(10),x1,0-y1,x2,0-y2,x3,0-y3}'

done < <(grep -v "#" faults/mantle.trv) > ${model}/volume-cross-section-step${step}.txt

fi


#### prepare CPT
cpt_e="/Users/qshi003/Documents/src/relax/share/sunset.cpt"
cpt_eta="/Users/qshi003/Documents/src/relax/share/ViBlGrWhYeOrRe_reverse.cpt"
cpt_e22="/Users/qshi003/Documents/src/relax/share/jet.cpt"
cpt_e23="/Users/qshi003/Documents/src/relax/share/jet.cpt"
cpt_e33="/Users/qshi003/Documents/src/relax/share/jet.cpt"

makecpt -C${cpt_e}   -T-17/-13/0.01 -Ic -D> e.cpt
makecpt -C${cpt_eta} -T13/17/0.01 -D> eta.cpt
makecpt -C${cpt_e22} -T${rng_color} -D> e22.cpt
makecpt -C${cpt_e23} -T${rng_color} -D> e23.cpt
makecpt -C${cpt_e33} -T${rng_color} -D> e33.cpt

########### Plot
gmtset FRAME_WIDTH 0.5p \
       FRAME_PEN            0.5p  \
       TICK_LENGTH          0.1c  \
       LABEL_OFFSET         0.1c  \
       PLOT_DEGREE_FORMAT   DF    \
       ANNOT_FONT_SIZE_PRIMARY 9p \
       ANNOT_FONT_PRIMARY 0\
       LABEL_FONT_SIZE 12p \
       ANNOT_OFFSET_PRIMARY 0.1c  \
       D_FORMAT             %.3e \
       BASEMAP_TYPE plain



for par in e
do

if [ $par == "eta" ]
then
    cln=7
    color="a2f1"
elif [ $par == "e" ]
then
    cln=9
    color="a4f2"
elif [ $par == "e22" ]
then
    cln=4
elif [ $par == "e23" ]
then
    cln=5
else
    cln=6
fi

####### plot colors on all elements
awk -v n=$cln '{print "> -Z "$n"\n"$10,$11"\n"$12,$13"\n"$14,$15}' ${model}/volume-cross-section-step${step}.txt | \
    psxy -R-150/600/-276/0 -Jx0.03c -Ba100f50WSen -C${par}.cpt -L -M -W1p,black -K > ${model}/${par}-step${step}-smooth${smooth}.ps


awk '{print $3/1e3,0-$4/1e3}' faults/nankai2.flt.2d | psxy -R -J -K -O -W2p,black >> ${model}/${par}-step${step}-smooth${smooth}.ps
awk '($7==2){print $3/1e3,0-$4/1e3}' faults/nankai2.flt.2d | psxy -R -J -K -O -W2p,red >> ${model}/${par}-step${step}-smooth${smooth}.ps

psscale -D10c/15c/10c/1ch -E -B$color -C${par}.cpt -O >> ${model}/${par}-step${step}-smooth${smooth}.ps

ps2pdf ${model}/${par}-step${step}-smooth${smooth}.ps ${model}/${par}-step${step}-smooth${smooth}.pdf
open ${model}/${par}-step${step}-smooth${smooth}.pdf


done

done
