#!/bin/bash -e

prepareTXT=0
model=vc_acc-miu0.1-Rb0.025-Ru1.83_dis_SZ-Rb0.24-Ru30
dir=${model}/data
rng_oce=-150/570/-276/-50
rng_con=230/600/-256/-30

for step in 12368 ## 10999 during EQ; 11200  afterslip; 11600 interseismic
do

if [ $prepareTXT -gt 0 ]
then
#### generate a list of strain rate, shear stress and viscosity for all triangle elements

while read idx e22 e23 e33 l1 l2 l3
do 

x1=`grep -v "#" faults/mantle.ned | awk -v n=$l1 'NR==n{print $2/1e3}'`
y1=`grep -v "#" faults/mantle.ned | awk -v n=$l1 'NR==n{print $3/1e3}'`
x2=`grep -v "#" faults/mantle.ned | awk -v n=$l2 'NR==n{print $2/1e3}'`
y2=`grep -v "#" faults/mantle.ned | awk -v n=$l2 'NR==n{print $3/1e3}'`
x3=`grep -v "#" faults/mantle.ned | awk -v n=$l3 'NR==n{print $2/1e3}'`
y3=`grep -v "#" faults/mantle.ned | awk -v n=$l3 'NR==n{print $3/1e3}'`

grep -v "#" ${dir}/volume-000${idx}-000${idx}.dat | \
    awk -v x1=$x1 -v x2=$x2 -v x3=$x3 -v y1=$y1 -v y2=$y2 -v y3=$y3 -v id=$idx -v st=$step \
        'NR==st{ekk=$8+$10;e22p=$8-ekk/2;e33p=$10-ekk/2;e=sqrt(e22p**2+2*$9**2+e33p**2);
                if(e<(1e-18)/(3.1536e7))e=(1e-18)/(3.1536e7);p=($5+$7)/2;
                s22p=$5-p;s33p=$7-p;tau=sqrt(s22p**2+2*$6**2+s33p**2); 
                print id,(x1+x2+x3)/3,0-(y1+y2+y3)/3,$8,$9,$10,
                (log(tau)-log(e))/log(10),log(tau)/log(10),log(e)/log(10),x1,0-y1,x2,0-y2,x3,0-y3}'

done < <(grep -v "#" faults/mantle.trv) > ${model}/volume-cross-section-step${step}.txt

fi
done

#### prepare CPT
cpt_eta="/Users/qshi003/Documents/src/relax/share/ViBlGrWhYeOrRe_reverse.cpt"
cpt_tau="/Users/qshi003/Documents/src/relax/share/deepsea.cpt"
cpt_e="/Users/qshi003/Documents/src/relax/share/sunset.cpt"
cpt_e22="/Users/qshi003/Documents/src/relax/share/jet.cpt"
cpt_e23="/Users/qshi003/Documents/src/relax/share/jet.cpt"
cpt_e33="/Users/qshi003/Documents/src/relax/share/jet.cpt"

range4=`minmax -C ${model}/volume-cross-section-step${step}.txt | awk '{printf "%.2e\/%.2e",$7,$8}'`
range5=`minmax -C ${model}/volume-cross-section-step${step}.txt | awk '{printf "%.2e\/%.2e",$9,$10}'`
range6=`minmax -C ${model}/volume-cross-section-step${step}.txt | awk '{printf "%.2e\/%.2e",$11,$12}'`
range1=`minmax -C ${model}/volume-cross-section-step${step}.txt | awk '{printf "%.0f\/%.0f",$13,$14}'`
range2=`minmax -C ${model}/volume-cross-section-step${step}.txt | awk '{printf "%.2f\/%.2f",$15,$16}'`
range3=`minmax -C ${model}/volume-cross-section-step${step}.txt | awk '{printf "%.0f\/%.0f",$17,$18}'`

echo $range1 $range2 $range3 $range4 $range5 $range6
#makecpt -C${cpt_eta} -T${range1}/0.01 -D> eta.cpt
#makecpt -C${cpt_tau} -T${range2}/0.01 -Ic -D> tau.cpt
#makecpt -C${cpt_e} -T${range3}/0.01 -Ic -D> e.cpt
#makecpt -C${cpt_e22} -T${range4}/1e-18 -D> e22.cpt
#makecpt -C${cpt_e23} -T${range5}/1e-18 -D> e23.cpt
#makecpt -C${cpt_e33} -T${range6}/1e-18 -D> e33.cpt
makecpt -C${cpt_eta} -T13/17/0.01 -D> eta.cpt
#makecpt -C${cpt_eta} -T13/24/0.01 -D> eta.cpt
makecpt -C${cpt_tau} -T-3/0/0.01 -Ic -D> tau.cpt
makecpt -C${cpt_e}   -T-25/-13/0.01 -Ic -D> e.cpt
makecpt -C${cpt_e22} -T-2e-16/2e-16/1e-18 -D> e22.cpt
makecpt -C${cpt_e23} -T-2e-16/2e-16/1e-18 -D> e23.cpt
makecpt -C${cpt_e33} -T-2e-16/2e-16/1e-18 -D> e33.cpt



########### Plot

for par in eta
do

if [ $par == "eta" ]
then
    cln=7
    color="a2f1"
elif [ $par == "tau" ]
then
    cln=8
    color="a1f0.5"
elif [ $par == "e" ]
then
    cln=9
    color="a4f2"
elif [ $par == "e22" ]
then
    cln=4
    color="a2e-16f1e-16"
elif [ $par == "e23" ]
then
    cln=5
    color="a2e-16f1e-16"
else
    cln=6
    color="a2e-16f1e-16"
fi


####### plot colors on all elements
##### step 11600
makecpt -C${cpt_e22} -T-1e-15/1e-15/1e-18 -D> e22.cpt
makecpt -C${cpt_e23} -T-1e-15/1e-15/1e-18 -D> e23.cpt
makecpt -C${cpt_e33} -T-1e-15/1e-15/1e-18 -D> e33.cpt
awk -v n=$cln '{print "> -Z "$n"\n"$10,"1298",$11"\n"$12,"1298",$13"\n"$14,"1298",$15}' ${model}/volume-cross-section-step11600.txt | \
   psxyz -R-150/600/1228/1298/-276/0 -Jx0.02c/0.27c -Jz0.02c -E160/30 -Ba200f100/0/a200f100SZ -C${par}.cpt -L -M -W1p,black -K > ${model}/${par}-3D.ps
awk '{print $3/1e3,1298,0-$4/1e3}' faults/nankai2.flt.2d | psxyz -R -J -Jz -E160/30 -K -O -W2p,black >> ${model}/${par}-3D.ps
awk '($7==2){print $3/1e3,1298,0-$4/1e3}' faults/nankai2.flt.2d | psxyz -R -J -Jz -E160/30 -K -O -W2p,red >> ${model}/${par}-3D.ps
psxyz -R -J -Jz -L -E160/30 -K -O -W1p,black >> ${model}/${par}-3D.ps <<EOF
-150 1298 0
600 1298 0
600 1298 -276
-150 1298 -276
EOF
psscale -K -D25c/12c/3c/0.5ch -E -Ba1e-15f5e-16:"Effective viscosity (Pa S)": -C${par}.cpt -O >> ${model}/${par}-3D.ps
##### step 11200
makecpt -C${cpt_e22} -T-2e-14/2e-14/1e-18 -D> e22.cpt
makecpt -C${cpt_e23} -T-2e-14/2e-14/1e-18 -D> e23.cpt
makecpt -C${cpt_e33} -T-2e-14/2e-14/1e-18 -D> e33.cpt
awk -v n=$cln '{print "> -Z "$n"\n"$10,"1263",$11"\n"$12,"1263",$13"\n"$14,"1263",$15}' ${model}/volume-cross-section-step11200.txt | \
   psxyz -R -J -Jz -E160/30 -C${par}.cpt -L -M -W1p,black -K -O >> ${model}/${par}-3D.ps
awk '{print $3/1e3,1263,0-$4/1e3}' faults/nankai2.flt.2d | psxyz -R -J -Jz -E160/30 -K -O -W2p,black >> ${model}/${par}-3D.ps
awk '($7==2){print $3/1e3,1263,0-$4/1e3}' faults/nankai2.flt.2d | psxyz -R -J -Jz -E160/30 -K -O -W2p,red >> ${model}/${par}-3D.ps
psxyz -R -J -Jz -L -E160/30 -K -O -W1p,black >> ${model}/${par}-3D.ps <<EOF
-150 1263 0
600 1263 0
600 1263 -276
-150 1263 -276
EOF
psscale -K -D22c/7.5c/3c/0.5ch -E -Ba2e-14f1e-14:"Effective viscosity (Pa S)": -C${par}.cpt -O >> ${model}/${par}-3D.ps
##### step 10999
makecpt -C${cpt_e22} -T-2e-16/2e-16/1e-18 -D> e22.cpt
makecpt -C${cpt_e23} -T-2e-16/2e-16/1e-18 -D> e23.cpt
makecpt -C${cpt_e33} -T-2e-16/2e-16/1e-18 -D> e33.cpt
awk -v n=$cln '{print "> -Z "$n"\n"$10,"1228",$11"\n"$12,"1228",$13"\n"$14,"1228",$15}' ${model}/volume-cross-section-step10999.txt | \
   psxyz -R -J -Jz -E160/30 -C${par}.cpt -L -M -W1p,black -K -O >> ${model}/${par}-3D.ps
awk '{print $3/1e3,1228,0-$4/1e3}' faults/nankai2.flt.2d | psxyz -R -J -Jz -E160/30 -K -O -W2p,black >> ${model}/${par}-3D.ps
awk '($7==2){print $3/1e3,1228,0-$4/1e3}' faults/nankai2.flt.2d | psxyz -R -J -Jz -E160/30 -K -O -W2p,red >> ${model}/${par}-3D.ps
psxyz -R -J -Jz -L -E160/30 -K -O -W1p,black >> ${model}/${par}-3D.ps <<EOF
-150 1228 0
600 1228 0
600 1228 -276
-150 1228 -276
EOF
psscale -D19c/3c/3c/0.5ch -E -Ba2e-16f1e-16:"Effective viscosity (Pa S)": -C${par}.cpt -O >> ${model}/${par}-3D.ps

ps2pdf ${model}/${par}-3D.ps ${model}/${par}-3Dnew.pdf
open ${model}/${par}-3Dnew.pdf


done


#rm *.cpt
