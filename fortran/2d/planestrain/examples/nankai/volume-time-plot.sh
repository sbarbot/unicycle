#!/bin/bash -e

prepareGRD=0
model=vc_acc-miu0.1-Rb0.025-Ru1.83_dis_SZ-Rb0.24-Ru30
dir=${model}/data
file=vert3
#file=slop
par=eta
line=faults/mantle_${file}.trv

if [ $par = eta ]
then
color=13/17/0.01
cpt=ViBlGrWhYeOrRe_reverse.cpt
else
color=-17/-13/0.01
cpt=sunset-reverse.cpt
fi

if [ $file = "slop" ]
then
range=0/43/1000/1450
inter=2/3
else
range=0.3/1.25/1000/1450
inter=0.05/2
fi

#### generate the grd file for the mantle wedge slop or a vertical line in oceanic mantle
if [ $prepareGRD -gt 0 ]
then

while read x y elm
do 
grep -v "#" ${dir}/volume-000${elm}-000${elm}.dat | awk -v x=$x -v dp=$y 'NR>1{ekk=$8+$10;e22p=$8-ekk/2;e33p=$10-ekk/2;e=sqrt(e22p**2+2*$9**2+e33p**2);if(e<(1e-18)/(3.1536e7))e=(1e-18)/(3.1536e7);p=($5+$7)/2;s22p=$5-p;s33p=$7-p;tau=sqrt(s22p**2+2*$6**2+s33p**2); print 0-dp,$1/3.1536e7,(log(tau)-log(e))/log(10),log(tau)/log(10),log(e)/log(10),x}'
done < $line > ${model}/volume-time-${file}.txt
fi
awk '{print $1,$2,$3}' ${model}/volume-time-${file}.txt | blockmean -R$range -I$inter | surface -R$range -I$inter -G${model}/eta-${file}.grd
awk '{print $1,$2,$4}' ${model}/volume-time-${file}.txt | blockmean -R$range -I$inter | surface -R$range -I$inter -G${model}/tau-${file}.grd
awk '{print $1,$2,$5}' ${model}/volume-time-${file}.txt | blockmean -R$range -I$inter | surface -R$range -I$inter -G${model}/e-${file}.grd
awk '{print $1,$2,$6}' ${model}/volume-time-${file}.txt | blockmean -R$range -I$inter | surface -R$range -I$inter -G${model}/vol-${file}-${par}-geometry.grd



#### plot 2D
grdmap.sh -H -p $color -t 5 -c $cpt -b $range ${model}/${par}-${file}.grd


#### plot 3D
#if [ $file = "slop" ]
#then
#grdview.sh -H -p $color -t 40 -b $range -E 160/30 -v 0.155 -c $cpt -G ${model}/${par}-${file}.grd ${model}/vol-${file}-${par}-geometry.grd
#else
#grdview.sh -H -p $color -t 100 -b -150/600/1000/1450 -E 160/30 -v 2.64 -c $cpt -G ${model}/${par}-${file}.grd ${model}/vol-${file}-${par}-geometry.grd
#fi
