#!/bin/bash

set -e
self=$(basename $0)
selfdir=$(dirname $0)
cmdline=$*
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

# Explore the evolution of the real area of contact of a 200 mm long PMMA sample
# to explain the data described by Svetlizky et al. (2019) with rigidity G = 1.7 GPa.

N=4000

WDIR=$selfdir/pmma_1
GDIR=$selfdir/greens
FLT=$WDIR/flt.2d
CAT=$WDIR/ecatalogue-1E-4-1E-5.dat

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

if [ ! -e $GDIR ]; then
	echo adding directory $WDIR
	mkdir $GDIR
fi

echo "" | awk -v N=$N 'BEGIN{i=1;pi=atan2(1.0,0.0)*2.0}{
       	x2o=0;
       	x3o=0;
       	width=2e-4;
       	dip=90;
	d2=1+cos(pi*dip/180.0)-1;
	d3=1+sin(pi*dip/180.0)-1;
       	for (j=0;j<N;j++){
       	        x2=x2o+j*d2*width;
       	        x3=x3o+j*d3*width;
       	        printf "%05d %e %16.11e %16.11e %f %f\n", i,1e-7,x2,x3,width,dip;
       	        i=i+1;
       	}
}' > $FLT

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli (MPa), Lame constant, universal gas constant (J/K/mol)
1.7e3 3.97e3 8.314462618153
# time interval (s)
50
# number of patches
`grep -v "#" $FLT | wc -l`
# n  Vpl x2   x3 width dip
`grep -v "#" $FLT`
# number of frictional properties
`grep -v "#" $FLT | wc -l`
#  n    mu0      sig        n        m        Q       Vo  G/(2Vs)
`grep -v "#" $FLT | awk '{ 
	depth=$4; 
	mu0=0.30;   # reference friction coefficient
	sig=5.5;    # effective normal stress
	n=60;       # shear stress power exponent
	m=2;        # microasperity curvature power exponent
	Q=90e3;     # activation energy for slip rate
	Vo=1e-6;    # reference velocity
	damping=5;  # radiation damping coefficient
	printf "%06d %f %f %e %e %e %e %f\n", NR,mu0,sig,n,m,Q,Vo,damping;
	}'`
# number of healing properties
`grep -v "#" $FLT | wc -l`
#  n  G1  H1  p1  G2  H2  p2  G3  H3  p3 lambda  W
`grep -v "#" $FLT | awk '{ 
	depth=$4; 
	p1=1.0;     # power exponent for flattening
	p2=1.5;      
	p3=2.5;
	G1=1e-11;   # reference flatenning rate
	G2=0e-3; 
	G3=0e+10;
	H1=20e3;    # activation energy for flattening
	H2=50e3;
	H3=190e3;
	lambda=100; # reciprocal of characteristic strain
	W=5e-7;     # shear zone thickness
	printf "%06d %e %e %e %e %e %e %e %e %e %e %e\n", NR,G1,H1,p1,G2,H2,p2,G3,H3,p3,lambda,W;
	}'`
# number of thermal properties
`grep -v "#" $FLT | wc -l`
#  n       D        Wt       Tb    rhoc      w
`grep -v "#" $FLT | awk 'function min(x,y){return (x>y)?y:x}{ 
	depth=$4; 
	rhoc=2.6;     # (MPa/K) specific heat 2.6E6 J/K/m^3 or Pa/K or kg/m/s^2/K.
	D=1.1e-6;     # (m^2/s) thermal diffusivity: 0.5 mm^2/s
	w=1e+9;       # (m) thickness of shear layer: 1-10 mm
	Wt=2e-1;      # (m) thickness of thermal boundary layer: 1-100 m
	Tb=273.15+20; # constant room temperature
	printf "%06d %e %e %e %e %e\n", NR,D,Wt,Tb,rhoc,w;
	}'`
# number of observation patches
`grep -v "#" $FLT | awk '{if (0 == (NR-1)%2){print $0}}' | wc -l | awk '{print $1+8}'`
# n   index rate
  1     100    1
  2     200    1
  3     300    1
  4     400    1
  5     500    1
  6     600    1
  7     700    1
  8     800    1
# subsample the fault in space and time
`grep -v "#" $FLT | awk 'BEGIN{i=9}{if (0 == (NR-1)%2){print i,NR,1;i=i+1}}'`
# number of observation points
0
# number of events
0
EOF

if [ -e $selfdir/greens/greens-0000.grd ]; then
	IMPORT_GREENS="--import-greens $GDIR"
fi

time mpirun -n 16 unicycle-ps-healing-bath \
	--maximum-step 5e-3 \
	--export-netcdf \
	--export-greens $GDIR \
	$IMPORT_GREENS \
	--maximum-iterations 1000000 \
	--epsilon 1e-8 \
	$WDIR/in.param

ecatalogue.sh -b 1e-4/1e-5 $WDIR/time.dat > $CAT

files=`grep -v "#" $FLT | awk 'BEGIN{i=9}{if (0 == (NR-1)%2){print i; i=i+1}}' | \
	awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`

grep -v "#" $CAT | while IFS=" " read c1 ISTART IFINAL TSTART TFINAL c6; do

	MSSTART=`echo $TSTART | awk '{print $1*1e3}'`
	MSFINAL=`echo $TFINAL | awk '{print $1*1e3}'`
	DY=`echo $MSSTART $MSFINAL | awk '{print ($2-$1)/511}'`
	DURATION=`echo $MSSTART $MSFINAL | awk '{print $2-$1}'`

	OUT=$WDIR/event-${IFINAL}-${ISTART}-time-log10v.grd
	echo "# export $OUT"
	for i in $files; do
		index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
		cat $i | awk -v i=$index '{print i,$1*1e3,log($6)/log(10)}'
	done | \
		blockmean -I2/$DY -R1/$N/$MSSTART/$MSFINAL -E | awk '{print $1,$2,$6}' | \
		surface   -I1/$DY -R1/$N/$MSSTART/$MSFINAL -G$OUT

	OUT=$WDIR/event-${IFINAL}-${ISTART}-time-area.grd
	echo "# export $OUT"
	for i in $files; do
		index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
		cat $i | awk -v i=$index '{print i,$1*1e3,0.3*5.5/460*exp(2/60*log($5/1e-3))}'
	done | \
		blockmean -I2/$DY -R1/$N/$MSSTART/$MSFINAL -E | awk '{print $1,$2,$6}' | \
		surface   -I1/$DY -R1/$N/$MSSTART/$MSFINAL -G$OUT
done

echo "# export $WDIR/time-area.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $1}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$1,0.3*5.5/460*exp(2/60*log($5/1e-3))}'
done | \
	blockmean -I2/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -E | awk '{print $1,$2,$6}' | \
	surface -I1/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/time-area.grd

echo "# export $WDIR/time-temp.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $1}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$1,$7}'
done | \
	blockmean -I2/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -E | awk '{print $1,$2,$6}' | \
	surface -I1/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/time-temp.grd

echo "# export $WDIR/time-log10v.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $1}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$1,log($6)/log(10)}'
done | \
	blockmean -I4/`echo $yRange | awk '{print $1/4096}'` -R1/$N/0/$yRange -E | awk '{print $1,$2,$6}' | \
	surface -I4/`echo $yRange | awk '{print $1/4096}'` -R1/$N/0/$yRange -G$WDIR/time-log10v.grd

echo "# export $WDIR/slip-temp.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $2}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$2,$7}'
done | surface -I1/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/slip-temp.grd

echo "# export $WDIR/slip-log10v.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $2}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$2,log($6)/log(10)}'
done | surface -I1/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/slip-log10v.grd

