#!/bin/bash

# Explore the dynamics of a 15km-wide velocity-weakening region on a 20-degree
# dipping thrust fault as a function of the characteristic weakening distance L.
# The simulations produce in turn creep, long-term slow-slip events, short-term
# slow-slip events, very low-frequency earthquakes, bilateral ruptures, 
# unilateral ruptures, and partial ruptures sequences of increasing complexity
# with decreasing L, corresponding to an increase in the Ruina number Ru=R/h*.

selfdir=$(dirname $0)

N=1600

for i in 2.5E-2 2.4E-2 2.3E-2 2.25E-2 2.24E-2 2.23E-2 2.22E-2 2.21E-2 2.2E-2 2.1E-2 2.0E-2 1.9E-2 1.8E-2 1.7E-2 1.6E-2 1.5E-2 1.45E-2 1.4E-2 1.35E-2 1.3E-2 1.25E-2 1.2E-2 1.15E-2 1.125E-2 1.1125E-2 1.1E-2 1.075E-2 1.05E-2 1.025E-2 1.0125E-2 1.0E-2 9.0E-3 8.9E-3 8.8E-3 8.7E-3 8.69E-3 8.68E-3 8.67E-3 8.66E-3 8.65E-3 8.64E-3 8.63E-3 8.62E-3 8.61E-3 8.6E-3 8.5E-3 8.4E-3 8.3E-3 8.2E-3 8.1E-3 8.0E-3 7.9E-3 7.8E-3 7.7E-3 7.6E-3 7.5E-3 7.49E-3 7.48E-3 7.47E-3 7.46E-3 7.45E-3 7.44E-3 7.42E-3 7.41E-3 7.4E-3 7.39E-3 7.38E-3 7.37E-3 7.36E-3 7.35E-3 7.34E-3 7.32E-3 7.31E-3 7.3E-3 7.2E-3 7.1E-3 7.0E-3 6.0E-3 5.0E-3 4.0E-3 3.0E-3 2.0E-3 1.0E-3; do

	WDIR=$selfdir/output-L$i
	FLT=$WDIR/flt.2d

	if [ ! -e $WDIR ]; then
		echo adding directory $WDIR
		mkdir $WDIR
	else
		echo ""
		echo "# skipping $WDIR"
		echo ""
		continue
	fi

	echo "" | awk -v N=$N 'BEGIN{i=1;pi=atan2(1.0,0.0)*2.0}{
        	x2o=0;
        	x3o=1;
        	width=25;
        	dip=20;
		d2=1+cos(pi*dip/180.0)-1;
		d3=1+sin(pi*dip/180.0)-1;
        	for (j=0;j<N;j++){
        	        x2=x2o+j*d2*width;
        	        x3=x3o+j*d3*width;
        	        printf "%05d %e %16.11e %16.11e %f %f\n", i,1e-09,x2,x3,width,dip;
        	        i=i+1;
        	}
	}' > $FLT

	cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli (MPa)
30e3 30e3
# time interval (s)
2e10
# number of patches
`grep -v "#" $FLT | wc -l`
# n  Vpl x2   x3 width dip
`grep -v "#" $FLT`
# number of frictional patches
`grep -v "#" $FLT | wc -l`
# n tau0 mu0 sig    a     b    L   Vo G/2Vs
`grep -v "#" $FLT | awk -v L=$i '{ 
	depth=$4; 
	tau0=-1;
	mu0=0.68;
	sig=100;
	a=1.0e-2;
	b=(depth>5e3 && depth<10e3)?a+4e-3:a-4e-3;
	Vo=1e-6;
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,5;
	}'`
# number of observation patches
`grep -v "#" $FLT | awk '{if (0 == (NR-1)%4){print $0}}' | wc -l | awk '{print $1+8}'`
# n   index rate
  1      50    1
  2     100    1
  3     150    1
  4     200    1
  5     250    1
  6     300    1
  7     350    1
  8     400    1
# subsample the fault in space and time
`grep -v "#" $FLT | awk 'BEGIN{i=9}{if (0 == (NR-1)%4){print i,NR,20;i=i+1}}'`
# number of observation points
0
# number of events
0
EOF

	if [ -e $selfdir/greens/greens-0000.grd ]; then
		IMPORT_GREENS="--import-greens $selfdir/greens"
	fi

	time mpirun -n 2 unicycle-ps-ratestate \
		--maximum-step 3.15e7 \
		--export-netcdf \
		--export-greens $selfdir/greens \
		$IMPORT_GREENS \
		--maximum-iterations 1000000 \
		$WDIR/in.param

	files=`grep -v "#" $FLT | awk 'BEGIN{i=9}{if (0 == (NR-1)%4){print i; i=i+1}}' | \
		awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`

	echo "# export $WDIR/slip-log10v.grd"
	yRange=`for i in $files; do tail -n 1 $i | awk '{print $2}'; done | minmax -C | awk '{print $2}'`
	for i in $files; do
		index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
		cat $i | awk -v i=$index '{print i,$2,$6}'
	done | surface -I1/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/slip-log10v.grd

	echo "# export $WDIR/time-log10v.grd"
	yRange=`for i in $files; do tail -n 1 $i | awk '{print $1/3.15e7}'; done | minmax -C | awk '{print $2}'`
	for i in $files; do
		index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
		cat $i | awk -v i=$index '{print i,$1/3.15e7,$6}'
	done | \
		blockmean -I4/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -E | awk '{print $1,$2,$6}' | \
		surface -I4/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/time-log10v.grd

done

