#!/bin/bash

set -e
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

# Explore the dynamics of a 15km-wide velocity-weakening region on a 20-degree
# dipping thrust fault for a large Ruina number Ru=800.

#PBS -N large-Ru-ap
#PBS -q q24
#PBS -l nodes=1:ppn=24
#PBS -l walltime=30:00:00:00
#PBS -V

#SBATCH --job-name=large-Ru-ap
#SBATCH -N 1
#SBATCH -n 32
#SBATCH --mail-type=ALL
#SBATCH --mail-user=sbarbot@usc.edu

selfdir=$(dirname $0)

N=16384

if [ "$PBS_O_WORKDIR" != "" ]; then
	echo "MPI version:" `which mpirun`
	echo working directory: $PBS_O_WORKDIR
	cd $PBS_O_WORKDIR
	selfdir=$PBS_O_WORKDIR
	echo host: `hostname`
	echo time: `date`
	NPROCS=`wc -l < $PBS_NODEFILE`
	echo number of cpus: $NPROCS
	echo processors:
	echo `cat $PBS_NODEFILE`
fi

if [ "$SLURM_SUBMIT_DIR" != "" ]; then
	echo "MPI version:" `which mpirun`
	echo working directory: $SLURM_SUBMIT_DIR
	cd $SBATCH_O_WORKDIR
	selfdir=$SLURM_SUBMIT_DIR
	echo host: `hostname`
	echo time: `date`
	NPROCS=$SLURM_NPROCS
	echo number of cpus: $NPROCS
fi

WDIR=$selfdir/output-large-Ru
FLT=$WDIR/flt.2d

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

echo "" | awk -v N=$N 'BEGIN{i=1;pi=atan2(1.0,0.0)*2.0}{
       	x2o=0;
       	x3o=1;
       	width=2.5;
       	dip=20;
	d2=1+cos(pi*dip/180.0)-1;
	d3=1+sin(pi*dip/180.0)-1;
       	for (j=0;j<N;j++){
       	        x2=x2o+j*d2*width;
       	        x3=x3o+j*d3*width;
       	        printf "%05d %e %16.11e %16.11e %f %f\n", i,1e-09,x2,x3,width,dip;
       	        i=i+1;
       	}
}' > $FLT

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli (MPa)
30e3 30e3
# time interval (s)
8e10
# number of patches
`grep -v "#" $FLT | wc -l`
# n  Vpl x2   x3 width dip
`grep -v "#" $FLT`
# number of frictional patches
`grep -v "#" $FLT | wc -l`
# n tau0 mu0 sig    a     b    L   Vo G/2Vs
`grep -v "#" $FLT | awk '{ 
	depth=$4; 
	tau0=-1;
	mu0=0.68;
	sig=100;
	a=1.0e-2;
	b=(depth>5e3 && depth<10e3)?a+4e-3:a-4e-3;
	Vo=1e-6;
	L=2.5e-4;
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,5;
	}'`
# number of observation patches
`grep -v "#" $FLT | awk '{if (0 == (NR-1)%4){print $0}}' | wc -l | awk '{print $1+8}'`
# n   index rate
  1      50    1
  2     100    1
  3     150    1
  4     200    1
  5     250    1
  6     300    1
  7     350    1
  8     400    1
# subsample the fault in space and time
`grep -v "#" $FLT | awk 'BEGIN{i=9}{if (0 == (NR-1)%4){print i,NR,20;i=i+1}}'`
# number of observation points
0
# number of events
0
EOF

sleep 10

#time mpirun -n $NPROCS unicycle-ps-ratestate \
#	--maximum-step 3.15e7 \
#	--export-netcdf \
#	--maximum-iterations 2000000 \
#	$WDIR/in.param

files=`grep -v "#" $FLT | awk 'BEGIN{i=9}{if (0 == (NR-1)%4){print i; i=i+1}}' | \
	awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`

echo "# export $WDIR/step-log10v.grd"
yRange=`for i in $files; do grep -v "#" $i | wc | awk '{print $1}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,NR,$6}'
done | surface -I1/`echo $yRange | awk '{print $1/2048}'` -R1/$N/0/$yRange -G$WDIR/step-log10v.grd

echo "# export $WDIR/slip-log10v.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $2}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$2,$6}'
done | surface -I1/`echo $yRange | awk '{print $1/2048}'` -R1/$N/0/$yRange -G$WDIR/slip-log10v.grd

echo "# export $WDIR/time-log10v.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $1/3.15e7}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$1/3.15e7,$6}'
done | \
	blockmean -I4/`echo $yRange | awk '{print $1/2048}'` -R1/$N/0/$yRange -E | awk '{print $1,$2,$6}' | \
	surface -I4/`echo $yRange | awk '{print $1/2048}'` -R1/$N/0/$yRange -G$WDIR/time-log10v.grd


