 
# create time series of deviatoric stress (interpolate the spatial resolution by a factor of 4)
wdir=subduction; for i in $wdir/volume-*-*.dat; do n=`echo $i | awk -F "-" '{print $2}'`; grep -v "#" $i | awk -v n=$n '{p=($5+$7)/2;s22p=$5-p;s33p=$7-p;print n,NR,sqrt(s22p**2+2*$6**2+s33p**2)}'; done | surface -R1/46/1/385 -I0.25/1 -G$wdir/sig-wedge.grd

# select observation volumes of interest
for i in  $wdir/volume-0000000{1..9}-*.dat $wdir/volume-000000{10..46}-*.dat; do n=`echo $i | awk -F "-" '{print $2}'`; grep -v "#" $i | awk -v n=$n '{p=($5+$7)/2;s22p=$5-p;s33p=$7-p;print n,NR,sqrt(s22p**2+2*$6**2+s33p**2)}'; done | surface -R1/46/1/385 -I0.25/1 -G$wdir/sig-wedge.grd

# space-time interpolation
for i in  $wdir/volume-0000000{1..9}-*.dat $wdir/volume-000000{10..46}-*.dat; do n=`echo $i | awk -F "-" '{print $2}'`; grep -v "#" $i | awk -v n=$n '{ekk=($8+$10);e22p=$8-ekk/2;e33p=$10-ekk/2;print n,$1/3.15e7,sqrt(e22p**2+2*$9**2+e33p**2)*3.15e7}'; done | blockmean -R1/46/0/942 -I0.5/1 | surface -R1/46/0/942 -I0.5/1 -G$wdir/e-wedge-time.grd
wdir=subduction; for i in  $wdir/volume-000000{47..72}-*.dat; do n=`echo $i | awk -F "-" '{print $2}'`; grep -v "#" $i | awk -v n=$n '{ekk=($8+$10)/2;e22p=$8-ekk/2;e33p=$10-ekk/2;print n-46,$1/3.15e7,sqrt(e22p**2+2*$9**2+e33p**2)*3.15e7}'; done | surface -R1/26/0/942 -I0.25/1 -G$wdir/e-ocean-time.grd

# time-step plots
for i in  $wdir/volume-0000000{1..9}-*.dat $wdir/volume-000000{10..46}-*.dat; do n=`echo $i | awk -F "-" '{print $2}'`; grep -v "#" $i | awk -v n=$n '{ekk=($8+$10);e22p=$8-ekk/2;e33p=$10-ekk/2;print n,NR,sqrt(e22p**2+2*$9**2+e33p**2)*3.15e7}'; done | blockmean -R1/46/1/654 -I0.5/1 | surface -R1/46/0/654 -I0.5/1 -G$wdir/e-wedge.grd
wdir=subduction; for i in  $wdir/volume-000000{47..72}-*.dat; do n=`echo $i | awk -F "-" '{print $2}'`; grep -v "#" $i | awk -v n=$n '{ekk=($8+$10)/2;e22p=$8-ekk/2;e33p=$10-ekk/2;print n-46,NR,sqrt(e22p**2+2*$9**2+e33p**2)*3.15e7}'; done | blockmean -R1/26/1/654 -I0.25/1 | surface -R1/26/1/654 -I0.25/1 -G$wdir/e-ocean.grd


# fault slip-space plots
wdir=subduction; for i in $wdir/patch-*-*.dat; do n=`echo $i | awk -F "-" '{print $2}'`; grep -v "#" $i | awk -v n=$n '{print n,$2,$6}'; done | surface -R1/200/1/57.6 -I0.25/0.1 -G$wdir/flt-log10v-slip.grd


# time series of surface displacements
wdir=subduction; for i in $wdir/opts-????.dat; do n=`echo $i | awk -F "-" '{print -100+$2*7}'`; grep -v "#" $i | awk -v n=$n 'BEGIN{i=0}{if (0==(i%20)){print n,$1/3.15e7,-$3};i=i+1}'; done | surface -R-100/600/0/950 -I7/1 -G$wdir/u3-all-time.grd
wdir=subduction; for i in $wdir/opts-????.dat; do n=`echo $i | awk -F "-" '{print -100+$2*7}'`; grep -v "#" $i | awk -v n=$n 'BEGIN{i=0}{if (0==(i%20)){print n,$1/3.15e7,-$5};i=i+1}'; done | surface -R-100/600/0/950 -I7/1 -G$wdir/u3-flt-time.grd
wdir=subduction; for i in $wdir/opts-????.dat; do n=`echo $i | awk -F "-" '{print -100+$2*7}'`; grep -v "#" $i | awk -v n=$n 'BEGIN{i=0}{if (0==(i%20)){print n,$1/3.15e7,-$7};i=i+1}'; done | surface -R-100/600/0/950 -I7/1 -G$wdir/u3-dct-time.grd

