#!/bin/bash

set -e
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

# Megathrust earthquakes with viscoelastic relaxation in the 
# mantle wedge and in the oceanic asthenoephere with triangle
# strain volumes.

selfdir=$(dirname $0)

WDIR=$selfdir/subduction

FLT=$WDIR/flt.2d
STV=$selfdir/faults/mantle

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

echo "" | awk 'BEGIN{i=1;pi=atan2(1,0)*2}{
        x2o=0;
        x3o=0;
        width=500;
        dip=5.7106;
        N=600;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f\n", j+1,1.0e-09,x2,x3,width,dip;
        }
}' > $FLT


cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# rigidity, Lame parameter, universal gas constant
30e3 30e3 8.31446261815324
# time interval
3e10
# number of patches
`grep -v "#" $FLT | wc -l`
# n  Vpl           x2           x3    width      dip
`grep -v "#" $FLT`
# number of frictional patches
`grep -v "#" $FLT | wc -l`
#  n        tau0        mu0        sig          a          b          L         Vo      G/2Vs
`grep -v "#" $FLT | awk '{ 
	x2=$3;
	x3=$4; 
	tau0=-1; 
	mu0=0.6;
	sig=100;
	a=1.0e-2;
	b=(x3 >= 10e3 && x3 <= 20e3)?1.4e-2:0.6e-2;
	L=5e-2;
	Vo=1e-6;
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,5;\
	}'`
# number of rectangle volumes
0
# number of triangle volumes
`grep -v "#" $STV.trv | wc -l`
# n e22 e23 e33 i1 i2 i3
`grep -v "#" $STV.trv`
# number of vertices
`grep -v "#" $STV.ned | wc -l`
# n       x2     x3
`grep -v "#" $STV.ned`
# number of nonlinear Maxwell triangle volume elements
`grep -v "#" $STV.trv | wc -l`
# n s22  s23  s33 gammadot0m   n     Q
`grep -v "#" $STV.ned | awk 'NR==FNR {
	for (j=2;j<=3;j++){a[j,0+NR]=$j}; next} { 
		A2=a[2,0+$5];
		A3=a[3,0+$5];
		B2=a[2,0+$6];
		B3=a[3,0+$6];
		C2=a[2,0+$7];
		C3=a[3,0+$7];
		x2=(A2+B2+C2)/3.0;
		x3=(A3+B3+C3)/3.0;
		print x2,x3;
	}' - <( grep -v "#" $STV.trv ) | awk '{
		x2=$1;
		x3=$2;
		g=9.8;
		gammadot0=1e-3;
		n=1;
		rho=3330;
		P=rho*g*x3;
		Vol=4e-6;
		H=335e3+P*Vol;
		R=8.31446261815324;
		printf "%4d %f %f %f %f %f %f\n", NR,-1,-1,-1,gammadot0,n,H;
	}'`
# number of nonlinear Kelvin triangle volume elements
0
# number of thermal properties
`grep -v "#" $STV.trv | wc -l`
# n  rhoc  temperature
`grep -v "#" $STV.ned | awk 'NR==FNR {
	for (j=2;j<=3;j++){a[j,0+NR]=$j}; next} { 
		A2=a[2,0+$5];
		A3=a[3,0+$5];
		B2=a[2,0+$6];
		B3=a[3,0+$6];
		C2=a[2,0+$7];
		C3=a[3,0+$7];
		x2=(A2+B2+C2)/3.0;
		x3=(A3+B3+C3)/3.0;
		print x2,x3;
	}' - <( grep -v "#" $STV.trv ) | awk 'function erf(x){p=0.3275911;a1=0.254829592;a2=-0.284496736;a3=1.421413741;a4=-1.453152027;a5=1.061405429;t=1/(1+p*x);return 1-(a1*t+a2*t**2+a3*t**3+a4*t**4+a5*t**5)*exp(-x**2)}{
		x2=$1;
		x3=$2;
		k=3.138; 
		Cp=1171;
		rho=3330;
		Kappa=k/(rho*Cp);
		age=2e15;
		T=273+1400*erf(x3/(sqrt(4*Kappa*age)))
		printf "%d %f %f\n", NR,1,T;
	}'`
# number of observation patches
200
# n   index  rate
`echo "" | awk '{n=600;for (i=1;i<n;i=i+3){
	print 1+(i-1)/3, i, 20;
	}}'`
# number of observation volumes
72
# n    i rate
# profile parallel to slab in mantle wedge
  1  720   20
  2  721   20
  3  702   20
  4  689   20
  5  688   20
  6  690   20
  7  685   20
  8  715   20
  9  714   20
 10  713   20
 11  698   20
 12  711   20
 13  710   20
 14  708   20
 15  691   20
 16  684   20
 17  686   20
 18  739   20
 19  738   20
 20  737   20
 21  704   20
 22  800   20
 23  799   20
 24  855   20
 25  854   20
 26  853   20
 27  899   20
 28  901   20
 29  900   20
 30  794   20
 31  795   20
 32  735   20
 33  733   20
 34  890   20
 35  891   20
 36  893   20
 37  892   20
 38  897   20
 39  898   20
 40  896   20
 41  843   20
 42  830   20
 43  831   20
 44  829   20
 45  760   20
 46  682   20
# depth profile in oceanic mantle
 47  334   20
 48  335   20
 49  550   20
 50  549   20
 51  548   20 
 52  552   20
 53  551   20
 54  251   20
 55  252   20
 56  250   20
 57  248   20
 58  254   20
 59  253   20
 60  408   20
 61  577   20
 62  578   20
 63  579   20
 64  576   20
 65  313   20
 66  429   20
 67  430   20
 68  316   20
 69  512   20
 70  518   20
 71  517   20
 72  513   20
# number of observation points
100
# n name    x2    x3
`echo "" | awk '{
	for (i=0;i<100;i++){
		printf "%4d %04d %e 0e3 1\n", i+1,i+1,-100e3+i*7e3+10;
	}
}'`
# number of events
0
EOF

if [ -e "$WDIR/greens-0000.grd" ]; then
	IMPORTGREENS="--import-greens $WDIR"
fi

time mpirun -n 2 unicycle-ps-viscouscycles \
	--maximum-step 3.15e7 \
	--export-netcdf \
	--export-greens $WDIR \
	--maximum-iterations 10000000 \
	$IMPORTGREENS \
	$* $WDIR/in.param


