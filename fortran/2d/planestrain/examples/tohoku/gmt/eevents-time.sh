#!/bin/bash

# extra script for mapping tool grdmap.sh
# overlays coastlines.
#
#   grdmap.sh -e ecoasts.sh selfdir/file.grd
#

set -e
self=$(basename $0)
selfdir=$(dirname $0)
cmdline=$*
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

while getopts "b:c:gp:v:H:J:" flag
do
	case "$flag" in
	b) bset=1;bds=$OPTARG;;
	c) cset=1;carg=$OPTARG;;
	g) gset=1;;
	p) pset=1;U3=$OPTARG;;
	v) vset=1;SIZE=$OPTARG;VECTOR=$OPTARG"c";;
	J) Jset="-J";PROJ=$OPTARG;;
	H) Hset=1;HEIGHT=$OPTARG;;
	esac
done
for item in $bset $cset $pset $vset $Hset $Jset; do
	shift;shift
done
for item in $gset; do
	shift
done

if [ "$#" -lt "1" ]; then
	echo ${self} overlays coastlines on a GMT map.
	echo
	echo usage: $self -b xmin/xmax/ymin/ymax file.ps
	exit 1
fi

echo $self: $cmdline
PSFILE=$1
WDIR=$(dirname $1)

if [ "$gset" == "" ]; then
	if [ "$Jset" != "" ]; then
		UNIT=ll
	else
		UNIT=km
	fi
else
	UNIT=ll
	PROJ=M$HEIGHT
fi

makecpt -T5/11/0.1 -C/Users/sbarbot/Documents/src/relax/share/bgyr.cpt -Z > $selfdir/moment.cpt

makecpt -T-2/1/0.1 -C/Users/sbarbot/Documents/src/relax/share/bgyr.cpt -Z > $selfdir/peak-velocity.cpt

psxy -O -K -J$PROJ -R$bds -P -Sa -C$selfdir/moment.cpt \
	<<EOF >> $PSFILE
`grep -v "#" $WDIR/ecatalogue-1e-2.1e-4-hypo.dat | awk '{if ($9<2e7){printf "%d %f %f %f\n",$11,$4/3.15e7,log($9)/log(10),log($9)/log(10)/15}}'`
EOF

pstext -O -K -J$PROJ -R$bds -P \
	<<EOF >> $PSFILE
`grep -v "#" $WDIR/ecatalogue-1e-2.1e-4-hypo.dat | awk '{if ($9<2e7){printf "%d %f 08 0 00 CT %d\n",$11,$4/3.15e7,$1}}'`
EOF

