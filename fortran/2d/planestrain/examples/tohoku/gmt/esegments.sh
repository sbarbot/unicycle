#!/bin/bash

# extra script for mapping tool grdmap.sh
# overlays coastlines.
#
#   grdmap.sh -e ecoasts.sh selfdir/file.grd
#

set -e
self=$(basename $0)
selfdir=$(dirname $0)
cmdline=$*
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

while getopts "b:c:gp:v:H:J:" flag
do
	case "$flag" in
	b) bset=1;bds=$OPTARG;;
	c) cset=1;carg=$OPTARG;;
	g) gset=1;;
	p) pset=1;U3=$OPTARG;;
	v) vset=1;SIZE=$OPTARG;VECTOR=$OPTARG"c";;
	J) Jset="-J";PROJ=$OPTARG;;
	H) Hset=1;HEIGHT=$OPTARG;;
	esac
done
for item in $bset $cset $pset $vset $Hset $Jset; do
	shift;shift
done
for item in $gset; do
	shift
done

if [ "$#" -lt "1" ]; then
	echo ${self} overlays coastlines on a GMT map.
	echo
	echo usage: $self -b xmin/xmax/ymin/ymax file.ps
	exit 1
fi

echo $self: $cmdline
PSFILE=$1

if [ "$gset" == "" ]; then
	if [ "$Jset" != "" ]; then
		UNIT=ll
	else
		UNIT=km
	fi
else
	UNIT=ll
	PROJ=M$HEIGHT
fi

psxy -O -K -J$PROJ -R$bds -P -m -L \
	-W2.0p/250/250/213 <<EOF >> $PSFILE
`echo "" | awk -v bds=$bds '{split(bds,b,"/");split("106 300 120 290 280 267 160",a," "); \
	for (i=1;i<=length(a);i++){
		s=0;
		for (j=1;j<=i;j++){
			s=s+a[j]
		}; \
		printf ">\n";
		print s,0
		print s,b[4]
	}}'`
EOF

