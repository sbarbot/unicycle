
# list events from simulations
gnuplot
model='../faults/2d_coseismic_2km.flt';
list_1w = system('ls tohoku_1w/event*.dat');
# plot all slip distributions over slab topography
plot for [i=1:words(list_1w)] word(list_1w,i) u 2:(-$3+$4*1e3) w l t '', model u (-$4-6000):(-$5+1e4+1e3*$2) w l
# plot all slip distributions
plot for [i=1:words(list_1w)] word(list_1w,i) u 2:($4) w l t '', model u (-$4-6000):($2) w l

# plot coseismic surface displacement
plot './tohoku_1w/disp_000416464-000386558.dat' u (-$2):((-$4-5.5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_1w/disp_000416464-000386558.dat' u (-$2):((-$5-0.3)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p

# postseismic profiles
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_1w/'.p.'_000416464-000386558.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_1w/'.p.'_000416464-000386558.dat' u (-$2/1e3):(-$4*1e3) w l

# tohoku_3w/event_000530496-000493930.dat
plot './tohoku_3w/disp_000530496-000493930.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_3w/disp_000530496-000493930.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_3w/'.p.'_000530496-000493930.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_3w/'.p.'_000530496-000493930.dat' u (-$2/1e3):(-$4*1e3) w l

# tohoku_2w/event_000528724-000493782.dat
plot './tohoku_2w/disp_000528724-000493782.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_2w/disp_000528724-000493782.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_2w/'.p.'_000528724-000493782.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_2w/'.p.'_000528724-000493782.dat' u (-$2/1e3):(-$4*1e3) w l

# tohoku_2w/event_000528689-000493757.dat
plot './tohoku_2w/disp_000528689-000493757.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_2w/disp_000528689-000493757.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_2w/'.p.'_000528689-000493757.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_2w/'.p.'_000528689-000493757.dat' u (-$2/1e3):(-$4*1e3) w l

# visualize strain-rate
plot 'tohoku_2w/volume-00000002-00000645.dat' u ($1/3.15e7):(sqrt((2*$8**2+$9**2+2*$10**2)/2))/1e-14 w l

# tohoku_2w/event_000528688-000493748.dat
plot './tohoku_2w/disp_000528688-000493748.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_2w/disp_000528688-000493748.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_2w/'.p.'_000528688-000493748.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_2w/'.p.'_000528688-000493748.dat' u (-$2/1e3):(-$4*1e3) w l

# tohoku_5w/event_000528702-000493762.dat
plot './tohoku_5w/disp_000528702-000493762.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_5w/disp_000528702-000493762.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_5w/'.p.'_000528702-000493762.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_5w/'.p.'_000528702-000493762.dat' u (-$2/1e3):(-$4*1e3) w l

# tohoku_2w02/event_000528697-000493760.dat
plot './tohoku_2w02/disp_000528697-000493760.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_2w02/disp_000528697-000493760.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_2w02/'.p.'_000528697-000493760.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_2w02/'.p.'_000528697-000493760.dat' u (-$2/1e3):(-$4*1e3) w l

# tohoku_2w03/event_000528698-000493768.dat
plot './tohoku_2w03/disp_000528698-000493768.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_2w03/disp_000528698-000493768.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_2w03/'.p.'_000528698-000493768.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:(-$3) w p, './tohoku_2w03/'.p.'_000528698-000493768.dat' u (-$2/1e3):(-$5*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_2w03/'.p.'_000528698-000493768.dat' u (-$2/1e3):(-$4*1e3) w l

# tohoku_4w/event_000528691-000493754.dat
plot './tohoku_4w/disp_000528691-000493754.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_4w/disp_000528691-000493754.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_4w/'.p.'_000528691-000493754.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:(-$3) w p, './tohoku_4w/'.p.'_000528691-000493754.dat' u (-$2/1e3):(-$5*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_4w/'.p.'_000528691-000493754.dat' u (-$2/1e3):(-$4*1e3) w l

# tohoku_5w/event_000528702-000493762.dat
plot './tohoku_5w/disp_000528702-000493762.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_5w/disp_000528702-000493762.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_5w/'.p.'_000528702-000493762.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:(-$3) w p, './tohoku_5w/'.p.'_000528702-000493762.dat' u (-$2/1e3):(-$5*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_5w/'.p.'_000528702-000493762.dat' u (-$2/1e3):(-$4*1e3) w l

# tohoku_6w/event_000528706-000493767.dat
plot './tohoku_6w/disp_000528706-000493767.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_6w/disp_000528706-000493767.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_6w/'.p.'_000528706-000493767.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:(-$3) w p, './tohoku_6w/'.p.'_000528706-000493767.dat' u (-$2/1e3):(-$5*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_6w/'.p.'_000528706-000493767.dat' u (-$2/1e3):(-$4*1e3) w l

# tohoku_4wt01/event_000528761-000493815.dat
plot './tohoku_4wt01/disp_000528761-000493815.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_4wt01/disp_000528761-000493815.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_4wt01/'.p.'_000528761-000493815.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:(-$3) w p, './tohoku_4wt01/'.p.'_000528761-000493815.dat' u (-$2/1e3):(-$5*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_4wt01/'.p.'_000528761-000493815.dat' u (-$2/1e3):(-$4*1e3) w l

# tohoku_4wt02/event_000528763-000493831.dat
plot './tohoku_4wt02/disp_000528763-000493831.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_4wt02/disp_000528763-000493831.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_4wt02/'.p.'_000528763-000493831.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:(-$3) w p, './tohoku_4wt02/'.p.'_000528763-000493831.dat' u (-$2/1e3):(-$5*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_4wt02/'.p.'_000528763-000493831.dat' u (-$2/1e3):(-$4*1e3) w l

# tohoku_4wt03/event_000528765-000493825.dat
plot './tohoku_4wt03/disp_000528765-000493825.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_4wt03/disp_000528765-000493825.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_4wt03/'.p.'_000528765-000493825.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:(-$3) w p, './tohoku_4wt03/'.p.'_000528765-000493825.dat' u (-$2/1e3):(-$5*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_4wt03/'.p.'_000528765-000493825.dat' u (-$2/1e3):(-$4*1e3) w l

# tohoku_4wt04/event_000528756-000493824.dat
plot './tohoku_4wt04/disp_000528756-000493824.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_4wt04/disp_000528756-000493824.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_4wt04/'.p.'_000528756-000493824.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:(-$3) w p, './tohoku_4wt04/'.p.'_000528756-000493824.dat' u (-$2/1e3):(-$5*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_4wt04/'.p.'_000528756-000493824.dat' u (-$2/1e3):(-$4*1e3) w l

# tohoku_4wt05/event_000528758-000493820.dat
plot './tohoku_4wt05/disp_000528758-000493820.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_4wt05/disp_000528758-000493820.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_4wt05/'.p.'_000528758-000493820.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:(-$3) w p, './tohoku_4wt05/'.p.'_000528758-000493820.dat' u (-$2/1e3):(-$5*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_4wt05/'.p.'_000528758-000493820.dat' u (-$2/1e3):(-$4*1e3) w l

# tohoku_4wt06/event_000528771-000493826.dat
plot './tohoku_4wt06/disp_000528771-000493826.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_4wt06/disp_000528771-000493826.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_4wt06/'.p.'_000528771-000493826.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:(-$3) w p, './tohoku_4wt06/'.p.'_000528771-000493826.dat' u (-$2/1e3):(-$5*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_4wt06/'.p.'_000528771-000493826.dat' u (-$2/1e3):(-$4*1e3) w l

# tohoku_4wt07/event_000528759-000493831.dat
plot './tohoku_4wt07/disp_000528759-000493831.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_4wt07/disp_000528759-000493831.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_4wt07/'.p.'_000528759-000493831.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:(-$3) w p, './tohoku_4wt07/'.p.'_000528759-000493831.dat' u (-$2/1e3):(-$5*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_4wt07/'.p.'_000528759-000493831.dat' u (-$2/1e3):(-$4*1e3) w l

# tohoku_4wt08/event_000528766-000493833.dat
plot './tohoku_4wt08/disp_000528766-000493833.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_4wt08/disp_000528766-000493833.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_4wt08/'.p.'_000528766-000493833.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:(-$3) w p, './tohoku_4wt08/'.p.'_000528766-000493833.dat' u (-$2/1e3):(-$5*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_4wt08/'.p.'_000528766-000493833.dat' u (-$2/1e3):(-$4*1e3) w l

# tohoku_4wt09/event_000528762-000493818.dat
plot './tohoku_4wt09/disp_000528762-000493818.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_4wt09/disp_000528762-000493818.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_4wt09/'.p.'_000528762-000493818.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:(-$3) w p, './tohoku_4wt09/'.p.'_000528762-000493818.dat' u (-$2/1e3):(-$5*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_4wt09/'.p.'_000528762-000493818.dat' u (-$2/1e3):(-$4*1e3) w l

# tohoku_6wt01_V13/event_000528764-000493835.dat
w='tohoku_6wt01_V13';e='000528764-000493835';
plot './tohoku_6wt01_V13/disp_000528764-000493835.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_6wt01_V13/disp_000528764-000493835.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_6wt01_V13/'.p.'_000528764-000493835.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:(-$3) w p, './tohoku_6wt01_V13/'.p.'_000528764-000493835.dat' u (-$2/1e3):(-$5*1e3) w l
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, \
'./tohoku_6wt01_V13/'.p.'_000528764-000493835.dat' u (-$2/1e3):(-$4*1e3) w l, \
'./tohoku_6wt01_V13/'.p.'_000528764-000493835.dat' u (-$2/1e3):(-$6*1e3) w l, \
'./tohoku_6wt01_V13/'.p.'_000528764-000493835.dat' u (-$2/1e3):(-$8*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_6wt01_V13/'.p.'_000528764-000493835.dat' u (-$2/1e3):(-$4*1e3) w l

# tohoku_6wt01_V13_T1350_A100/event_000528777-000493841.dat
plot './tohoku_6wt01_V13_T1350_A100/disp_000528777-000493841.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_6wt01_V13_T1350_A100/disp_000528777-000493841.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_6wt01_V13_T1350_A100/'.p.'_000528777-000493841.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, \
'./tohoku_6wt01_V13_T1350_A100/'.p.'_000528777-000493841.dat' u (-$2/1e3):(-$4*1e3) w l, \
'./tohoku_6wt01_V13_T1350_A100/'.p.'_000528777-000493841.dat' u (-$2/1e3):(-$6*1e3) w l, \
'./tohoku_6wt01_V13_T1350_A100/'.p.'_000528777-000493841.dat' u (-$2/1e3):(-$8*1e3) w l
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:(-$3) w p, './tohoku_6wt01_V13_T1350_A100/'.p.'_000528777-000493841.dat' u (-$2/1e3):(-$5*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_6wt01_V13_T1350_A100/'.p.'_000528777-000493841.dat' u (-$2/1e3):(-$4*1e3) w l

# tohoku_6wt01_V13_T1350/event_000528750-000493816.dat
plot './tohoku_6wt01_V13_T1350/disp_000528750-000493816.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_6wt01_V13_T1350/disp_000528750-000493816.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_6wt01_V13_T1350/'.p.'_000528750-000493816.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, \
'./tohoku_6wt01_V13_T1350/'.p.'_000528750-000493816.dat' u (-$2/1e3):(-$4*1e3) w l, \
'./tohoku_6wt01_V13_T1350/'.p.'_000528750-000493816.dat' u (-$2/1e3):(-$6*1e3) w l, \
'./tohoku_6wt01_V13_T1350/'.p.'_000528750-000493816.dat' u (-$2/1e3):(-$8*1e3) w l
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:(-$3) w p, './tohoku_6wt01_V13_T1350/'.p.'_000528750-000493816.dat' u (-$2/1e3):(-$5*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_6wt01_V13_T1350/'.p.'_000528750-000493816.dat' u (-$2/1e3):(-$4*1e3) w l

# tohoku_6wt01_V15_T1350_A100/event_000528748-000493820.dat
plot './tohoku_6wt01_V15_T1350_A100/disp_000528748-000493820.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_6wt01_V15_T1350_A100/disp_000528748-000493820.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_6wt01_V15_T1350_A100/'.p.'_000528748-000493820.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, \
'./tohoku_6wt01_V15_T1350_A100/'.p.'_000528748-000493820.dat' u (-$2/1e3):(-$4*1e3) w l, \
'./tohoku_6wt01_V15_T1350_A100/'.p.'_000528748-000493820.dat' u (-$2/1e3):(-$6*1e3) w l, \
'./tohoku_6wt01_V15_T1350_A100/'.p.'_000528748-000493820.dat' u (-$2/1e3):(-$8*1e3) w l
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:(-$3) w p, './tohoku_6wt01_V15_T1350_A100/'.p.'_000528748-000493820.dat' u (-$2/1e3):(-$5*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_6wt01_V15_T1350_A100/'.p.'_000528748-000493820.dat' u (-$2/1e3):(-$4*1e3) w l

# tohoku_6wt01_V15_T1350/event_000528749-000493805.dat
plot './tohoku_6wt01_V15_T1350/disp_000528749-000493805.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot './tohoku_6wt01_V15_T1350/disp_000528749-000493805.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_6wt01_V15_T1350/'.p.'_000528749-000493805.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:(-$3) w p, './tohoku_6wt01_V15_T1350/'.p.'_000528749-000493805.dat' u (-$2/1e3):(-$5*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, './tohoku_6wt01_V15_T1350/'.p.'_000528749-000493805.dat' u (-$2/1e3):(-$4*1e3) w l


p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2:4 w errorbars, \
'./tohoku_6wt01_V13/'.p.'_000528764-000493835.dat' u (-$2/1e3):(-($8+$6*1.4)*1e3) w l, \
'./tohoku_6wt01_V13_T1350/'.p.'_000528750-000493816.dat' u (-$2/1e3):(-($8+$6*1.4)*1e3) w l, \
'./tohoku_6wt01_V15_T1350/'.p.'_000528749-000493805.dat' u (-$2/1e3):(-($8+$6*1.4)*1e3) w l, \
'./tohoku_6wt01_V13_T1350_A100/'.p.'_000528777-000493841.dat' u (-$2/1e3):(-($8+$6*1.4)*1e3) w l, \
'./tohoku_6wt01_V15_T1350_A100/'.p.'_000528748-000493820.dat' u (-$2/1e3):(-($8+$6*1.4)*1e3) w l

p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:(-$3):5 w errorbars, \
'./tohoku_6wt01_V13/'.p.'_000528764-000493835.dat' u (-$2/1e3):(-($9+$7*1.5)*1e3) w l, \
'./tohoku_6wt01_V13_T1350/'.p.'_000528750-000493816.dat' u (-$2/1e3):(-($9+$7*1.5)*1e3) w l, \
'./tohoku_6wt01_V15_T1350/'.p.'_000528749-000493805.dat' u (-$2/1e3):(-($9+$7*1.5)*1e3) w l, \
'./tohoku_6wt01_V13_T1350_A100/'.p.'_000528777-000493841.dat' u (-$2/1e3):(-($9+$7*1.5)*1e3) w l, \
'./tohoku_6wt01_V15_T1350_A100/'.p.'_000528748-000493820.dat' u (-$2/1e3):(-($9+$7*1.5)*1e3) w l

# tohoku_7wt01_S080_S080/event_000267521-000252925.dat
w='./tohoku_7wt01_S080_S080';e='000267521-000252925';
# tohoku_7wt01_S080_S080/event_000927988-000901558.dat
w='./tohoku_7wt01_S080_S080';e='000927988-000901558';

plot w.'/disp_'.e.'.dat' u (-$2):((-$4-5)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
plot w.'/disp_'.e.'.dat' u (-$2):((-$5-0.35)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($3) w p
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$4*1e3) w l
p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:(-$3) w p, w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$5*1e3) w l
p='profile_1209-1605';plot '../gps/'.p.'.dat' u 1:2 w p, w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$4*1e3) w l

# tohoku_7wt01_S100_S150/event_000643153-000620003.dat
w='./tohoku_7wt01_S150_S150';e='000643153-000620003';
# tohoku_7wt01_S100_S150/event_000865618-000850820.dat
w='./tohoku_7wt01_S150_S150';e='000865618-000850820';

# tohoku_7wt01_S150_S100/event_000233304-000219450.dat (smaller)
w='tohoku_7wt01_S150_S100';e='000233304-000219450';
# tohoku_7wt01_S150_S100/event_000658355-000628409.dat (larger)
w='tohoku_7wt01_S150_S100';e='000658355-000628409';

# tohoku_7wt01_S150_S150/event_000614945-000589246.dat
w='./tohoku_7wt01_S150_S150';e='000614945-000589246';

p='profile_1104-1112';plot '../gps/'.p.'.dat' u 1:2 w p, w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$4*1e3) w l, w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$6*1e3) w l, w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$8*1e3) w l

# tohoku_7wt01_S080_S075/event_000514716-000479804.dat
w='tohoku_7wt01_S080_S075';e='000514716-000479804'; # good, but not excellent coseismic displacements

# tohoku_7wt01_S080_S080/event_000927988-000901558.dat
w='tohoku_7wt01_S080_S080';e='000927988-000901558'; # very good fit to the coseismic displacements
plot w.'/disp_'.e.'.dat' u (-$2):((-$4-6.8)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p
w='tohoku_7wt01_S080_S080';e='000267521-000252925'; # coseismic displacements less good
plot w.'/disp_'.e.'.dat' u (-$2):((-$4-6.8)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p

# aftershocks (there are aseismic events in the afterslip period; first picked-up event affects the time series; second event is 10 yr later.)
w='tohoku_7wt01_S080_S080';e='000932373-000930107'; # t=2656.9098+0.1102;
w='tohoku_7wt01_S080_S080';e='000944219-000943026'; # t=2656.9098+10.7804;
w='tohoku_7wt01_S080_S080';e='000974750-000963308';
w='tohoku_7wt01_S080_S080';e='000979275-000975116';
# horizontal displacement
plot '../gps/'.p.'.dat' u 1:2:4 w errorbars, w.'/disp_'.e.'.dat' u (-$2/1e3):((-$4)*1e3) w l
# vertical displacement
plot '../gps/'.p.'.dat' u 1:(-$3):5 w errorbars, w.'/disp_'.e.'.dat' u (-$2/1e3):((-$5)*1e3) w l

# NEED TO REMOVE THE AFTERSHOCK FROM THE POSTSEISMIC DISPLACEMENT. 
# NEED TO EXAMINE THE CONTRIBUTION OF AFTERSHOCK IN THE VERTICAL DIRECTION

# tohoku_7wt01_S080_S085/event_001017700-000982534.dat
w='tohoku_7wt01_S080_S085';e='001017700-000982534'; # good coseismic, but less than S080_S080
plot w.'/disp_'.e.'.dat' u (-$2):((-$4-4.8)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p

# tohoku_7wt01_S080_S090/event_000821356-000794435.dat
w='tohoku_7wt01_S080_S090';e='000821356-000794435'; # very good fit to the coseismic displacements, like for 7wt01_S095_S090
plot w.'/disp_'.e.'.dat' u (-$2):((-$4-6.2)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p

# tohoku_7wt01_S090_S090/event_000749005-000724162.dat
w='tohoku_7wt01_S090_S090';e='000749005-000724162';
plot w.'/disp_'.e.'.dat' u (-$2):((-$4-6.2)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p; # very good fit to coseismic GPS

# tohoku_7wt01_S095_S095/event_000432760-000402436.dat
w='tohoku_7wt01_S095_S095';e='000432760-000402436'; # poor fit to coseismic displacements
plot w.'/disp_'.e.'.dat' u (-$2):((-$4-4.9)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p

# tohoku_7wt01_S095_S090/event_000683833-000658405.dat
w='tohoku_7wt01_S095_S090';e='000683833-000658405'; # very good fit to the coseismic displacements
plot w.'/disp_'.e.'.dat' u (-$2):((-$4-5.8)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p

# tohoku_7wt01_S090_S095/event_000257405-000243487.dat
w='tohoku_7wt01_S090_S095';e='000257405-000243487'; # mediocre fit to coseismic displacements
plot w.'/disp_'.e.'.dat' u (-$2):((-$4-7.0)) w l, '../gps/Coseismic_GPS.txt' u ($1*1e3):($2) w p

p='profile_1104-1112';
plot '../gps/'.p.'.dat' u 1:2:4 w errorbars,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$4*1e3-90) w l,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$6*1e3-90) w l,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$8*1e3-90) w l
plot '../gps/'.p.'.dat' u 1:2 w p,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$4*1e3-90) w l,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$6*1e3-90) w l,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$8*1e3-90) w l
plot '../gps/'.p.'.dat' u 1:2 w p,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$4*1e3-100) w l,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$6*1e3-100) w l,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$8*1e3-100) w l
plot '../gps/'.p.'.dat' u 1:2 w p,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$4*1e3-75) w l,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$6*1e3-75) w l,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$8*1e3-75) w l
plot '../gps/'.p.'.dat' u 1:2 w p,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$4*1e3-80) w l,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$6*1e3-80) w l,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$8*1e3-80) w l
plot '../gps/'.p.'.dat' u 1:(-$3) w p,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$5*1e3+50) w l,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$7*1e3+50) w l,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$9*1e3+50) w l
plot '../gps/'.p.'.dat' u 1:(-$3) w p,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$5*1e3+25) w l,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$7*1e3+25) w l,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$9*1e3+25) w l
plot '../gps/'.p.'.dat' u 1:(-$3) w p,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$5*1e3) w l,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$7*1e3) w l,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$9*1e3) w l


# remove aftershock coseismic component
awk 'NR==FNR {for (i=1;i<=NF;i++){a[NR,i]=$i}; next} {for (i=4;i<=NF;i++){$i=$i-a[FNR,i]};print $0}' <(grep -v "#" tohoku_7wt01_S080_S080/disp_000932373-000930107.dat) <(grep -v "#" tohoku_7wt01_S080_S080/profile_1104-1112_000927988-000901558.dat) > tohoku_7wt01_S080_S080/profile_1104-1112_000927988-000901558-post.dat
plot '../gps/'.p.'.dat' u 1:2:4 w errorbars,w.'/'.p.'_'.e.'-post.dat' u (-$2/1e3):(-$4*1e3-90) w l,w.'/'.p.'_'.e.'-post.dat' u (-$2/1e3):(-$6*1e3-90) w l,w.'/'.p.'_'.e.'-post.dat' u (-$2/1e3):(-$8*1e3-90) w l
plot '../gps/'.p.'.dat' u 1:2:4 w errorbars,w.'/'.p.'_'.e.'-post-1-2-3.dat' u (-$2/1e3):(-$4*1e3-70) w l,w.'/'.p.'_'.e.'-post-1-2-3.dat' u (-$2/1e3):(-$6*1e3-70) w l,w.'/'.p.'_'.e.'-post-1-2-3.dat' u (-$2/1e3):(-$8*1e3-70) w l
plot '../gps/'.p.'.dat' u ($1-20):2:4 w errorbars,w.'/'.p.'_'.e.'-post-1-2-3.dat' u (-$2/1e3):(-$4*1e3-60) w l,w.'/'.p.'_'.e.'-post-1-2-3.dat' u (-$2/1e3):(-$6*1e3-60) w l,w.'/'.p.'_'.e.'-post-1-2-3.dat' u (-$2/1e3):(-$8*1e3-60) w l
plot '../gps/'.p.'.dat' u 1:2:4 w errorbars,w.'/'.p.'_'.e.'-post-a-b-c-d.dat' u (-$2/1e3):(-$4*1e3-40) w l,w.'/'.p.'_'.e.'-post-a-b-c-d.dat' u (-$2/1e3):(-$6*1e3-40) w l,w.'/'.p.'_'.e.'-post-a-b-c-d.dat' u (-$2/1e3):(-$8*1e3-40) w l
plot '../gps/'.p.'.dat' u ($1-40):2:4 w errorbars,w.'/'.p.'_'.e.'-post-a-b-c-d.dat' u (-$2/1e3):(-$4*1e3-40) w l,w.'/'.p.'_'.e.'-post-a-b-c-d.dat' u (-$2/1e3):(-$6*1e3-40) w l,w.'/'.p.'_'.e.'-post-a-b-c-d.dat' u (-$2/1e3):(-$8*1e3-40) w l
plot '../gps/'.p.'.dat' u 1:2:4 w errorbars,w.'/'.p.'_'.e.'-post.dat' u (-$2/1e3):(-$4*1e3-80) w l,w.'/'.p.'_'.e.'-post.dat' u (-$2/1e3):(-$6*1e3-80) w l,w.'/'.p.'_'.e.'-post.dat' u (-$2/1e3):(-$8*1e3-80) w l
plot '../gps/'.p.'.dat' u 1:(-$3):5 w errorbars,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$5*1e3+20) w l,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$7*1e3+20) w l,w.'/'.p.'_'.e.'.dat' u (-$2/1e3):(-$9*1e3+20) w l
plot '../gps/'.p.'.dat' u 1:(-$3):5 w errorbars,w.'/'.p.'_'.e.'-post.dat' u (-$2/1e3):(-$5*1e3+20) w l,w.'/'.p.'_'.e.'-post.dat' u (-$2/1e3):(-$7*1e3+20) w l,w.'/'.p.'_'.e.'-post.dat' u (-$2/1e3):(-$9*1e3+20) w l
plot '../gps/'.p.'.dat' u 1:(-$3):5 w errorbars,w.'/'.p.'_'.e.'-post-1-2-3.dat' u (-$2/1e3):(-$5*1e3+20) w l,w.'/'.p.'_'.e.'-post-1-2-3.dat' u (-$2/1e3):(-$7*1e3+20) w l,w.'/'.p.'_'.e.'-post-1-2-3.dat' u (-$2/1e3):(-$9*1e3+20) w l
plot '../gps/'.p.'.dat' u 1:(-$3):5 w errorbars,w.'/'.p.'_'.e.'-post-a-b-c-d.dat' u (-$2/1e3):(-$5*1e3+20) w l,w.'/'.p.'_'.e.'-post-a-b-c-d.dat' u (-$2/1e3):(-$7*1e3+20) w l,w.'/'.p.'_'.e.'-post-a-b-c-d.dat' u (-$2/1e3):(-$9*1e3+20) w l
plot '../gps/'.p.'.dat' u 1:(-$3):5 w errorbars,w.'/'.p.'_'.e.'-post-a-b-c-d.dat' u (-$2/1e3):(-$5*1e3+30) w l,w.'/'.p.'_'.e.'-post-a-b-c-d.dat' u (-$2/1e3):(-$7*1e3+30) w l,w.'/'.p.'_'.e.'-post-a-b-c-d.dat' u (-$2/1e3):(-$9*1e3+30) w l
plot '../gps/'.p.'.dat' u ($1-40):(-$3):5 w errorbars,w.'/'.p.'_'.e.'-post-a-b-c-d.dat' u (-$2/1e3):(-$5*1e3+30) w l,w.'/'.p.'_'.e.'-post-a-b-c-d.dat' u (-$2/1e3):(-$7*1e3+30) w l,w.'/'.p.'_'.e.'-post-a-b-c-d.dat' u (-$2/1e3):(-$9*1e3+30) w l
plot '../gps/'.p.'.dat' u ($1-20):(-$3):5 w errorbars,w.'/'.p.'_'.e.'-post-1-2-3.dat' u (-$2/1e3):(-$5*1e3+30) w l,w.'/'.p.'_'.e.'-post-1-2-3.dat' u (-$2/1e3):(-$7*1e3+30) w l,w.'/'.p.'_'.e.'-post-1-2-3.dat' u (-$2/1e3):(-$9*1e3+30) w l

# convert triangle.dat to viscosity and temperature profiles
awk 'NR==FNR{a[NR]=$9-273.15;next}{if (">"==substr($0,0,1)){print "> -Z"a[(FNR-1)/5+1]}else{print $0}}' tohoku_7wt01_S080_S080/triangle.dat tohoku_7wt01_S080_S080/volume.xyz > tohoku_7wt01_S080_S080/volume-temp.xy
awk 'NR==FNR{a[NR]=$10;next}{if (">"==substr($0,0,1)){print "> -Z"a[(FNR-1)/5+1]}else{print $0}}' tohoku_7wt01_S080_S080/triangle.dat tohoku_7wt01_S080_S080/volume.xyz > tohoku_7wt01_S080_S080/volume-viscosity.xy
awk 'NR==FNR{a[NR]=log($10)/log(10);next}{if (">"==substr($0,0,1)){print "> -Z"a[(FNR-1)/5+1]}else{print $0}}' tohoku_7wt01_S080_S080/triangle.dat tohoku_7wt01_S080_S080/volume.xyz > tohoku_7wt01_S080_S080/volume-log10-viscosity.xy



# histograms
bin_number(x) = floor(x/bin_width);
rounded(x) = bin_width * ( bin_number(x) + 0.5 )
bin_width = 0.5;
set style fill solid 1.0
set boxwidth 0.05 absolute
set palette defined  (1 '#000fff', 2 '#0090ff', 3 '#0fffee', 4 '#90ff70', 5 '#ffee00', 6 '#ff7000', 7 '#ee0000' )
set cbrange [1:5]
stats 'output-L5.0E-4/ecatalogue-1E-2.1E-6.dat'
plot 'output-L5.0E-4/ecatalogue-1E-2.1E-6.dat' using (rounded(log10($9))):(1/STATS_records) smooth frequency with boxes

# add hypocenter location to event catalogue
for i in `grep -v "#" tohoku_7wt01_S080_S080/ecatalogue-1e-2.1e-4.dat | awk '{printf "%d\n",1+($2-1)/20}'`; do grd2xyz tohoku_7wt01_S080_S080/log10v.grd -R1/1500/${i}/$((${i}+1)) | minmax -C -Eh2 | awk '{print $1}'; done | paste <(grep -v "#" tohoku_7wt01_S080_S080/ecatalogue-1e-2.1e-4.dat) - > tohoku_7wt01_S080_S080/ecatalogue-1e-2.1e-4-hypo.dat
plot 'tohoku_7wt01_S080_S080-long/ecatalogue-1e-2.1e-4.dat' u (rounded(log10($9))):(1) s freq w boxes, 'tohoku_7wt01_S080_S080-long/ecatalogue.dat' u (rounded(log10($9))):(1) s freq w boxes

# moment-duration scaling
set palette defined  (1 '#000fff', 2 '#0090ff', 3 '#0fffee', 4 '#90ff70', 5 '#ffee00', 6 '#ff7000', 7 '#ee0000' )
set logscale x
set logscale y
set logscale cb
set xlabel 'Moment/length (N)'
set ylabel 'Duration (s)'
plot 'tohoku_7wt01_S080_S080-long/ecatalogue-1e-2.1e-4.dat' u 9:6:7 w points ls 5 palette, '' u ($9/2):6:(sprintf("%d",$1)) w labels notitle, sqrt(x)*2e-2
plot 'tohoku_7wt01_S080_S080-long/ecatalogue-1e-2.1e-4.dat' u 9:6:7 w points ls 5 palette,sqrt(x)*2e-2,1e-4*x

# histogram
plot "<awk -v size=0.5 'NR>3{b=int(log($9)/log(10)/size);a[b]++;bmax=b>bmax?b:bmax;bmin=b<bmin?b:bmin}END{for(i=bmin;i<=bmax;++i)print i*size,(i+1)*size,a[i]}' tohoku_7wt01_S080_S080-long/ecatalogue-1e-2.1e-4.dat" u 1:2 w p

# cumulative histogram
set logscale y
set xlabel 'Moment/length (N)'
set ylabel 'Occurrence (integer)'
plot "<awk -v size=0.5 -v bmin=1e20 'function cumsum(a,i,j){s=0;for (k=i;k<=j;k++){s=s+a[k]}return s};NR>3{b=int(log($9)/log(10)/size);a[b]++;bmax=b>bmax?b:bmax;bmin=b<bmin?b:bmin}END{for(i=bmin;i<=bmax;++i)print i*size,(i+1)*size,a[i],cumsum(a,i,bmax)}' tohoku_7wt01_S080_S080-long/ecatalogue-1e-2.1e-4.dat" u 1:(($4)) w boxes, 7e9*exp(-1.8*x)


plot "<awk -v size=0.2 -v bmin=1e20 'function cumsum(a,i,j){s=0;for (k=i;k<=j;k++){s=s+a[k]}return s};NR>3 && $11<900{b=int(log($9)/log(10)/size);a[b]++;bmax=b>bmax?b:bmax;bmin=b<bmin?b:bmin}END{for(i=bmin;i<=bmax;++i)print i*size,(i+1)*size,a[i],cumsum(a,i,bmax)}' tohoku_7wt01_S080_S080-long/ecatalogue-1e-2.1e-4-hypo.dat" u 1:(($4)) w boxes, 7e9*exp(-1.8*x)

set yrange [1e1:1e4]
plot "<awk -v size=0.2 -v bmin=1e20 'function cumsum(a,i,j){s=0;for (k=i;k<=j;k++){s=s+a[k]}return s};NR>3{b=int(log($9)/log(10)/size);a[b]++;bmax=b>bmax?b:bmax;bmin=b<bmin?b:bmin}END{for(i=bmin;i<=bmax;++i)print i*size,(i+1)*size,a[i],cumsum(a,i,bmax)}' tohoku_7wt01_S080_S080-long/ecatalogue-1e-2.1e-4.dat" u 1:(($4)) w boxes t "all events", "<awk -v size=0.2 -v bmin=1e20 'function cumsum(a,i,j){s=0;for (k=i;k<=j;k++){s=s+a[k]}return s};NR>3 && $11<900{b=int(log($9)/log(10)/size);a[b]++;bmax=b>bmax?b:bmax;bmin=b<bmin?b:bmin}END{for(i=bmin;i<=bmax;++i)print i*size,(i+1)*size,a[i],cumsum(a,i,bmax)}' tohoku_7wt01_S080_S080-long/ecatalogue-1e-2.1e-4-hypo.dat" u 1:(($4)) w boxes t "w/o deep eq", 7e9*exp(-1.8*x), 5e6*exp(-1.1*x)

plot "<awk -v size=0.2 -v bmin=1e20 -f ./chist.awk tohoku_7wt01_S080_S080-long/ecatalogue-1e-2.1e-4-hypo.dat" u (10**$1):(($4)) w boxes t "w/o deep eq"
plot "<awk -v size=0.15 -v bmin=1e20 -f ./chist.awk tohoku_7wt01_S080_S080-long/ecatalogue-1e-2.1e-4-hypo.dat" u 1:4 w boxes t "", min(1,10**3.1*(10**x)**(-0.4))
