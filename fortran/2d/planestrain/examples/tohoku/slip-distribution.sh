#!/bin/bash

set -e
self=$(basename $0)
cmdline=$*
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

# export slip and stress change for events in the provided catalogue.
# catalogue format is that of ecatalogue.sh

usage(){
        echo "usage: $self [-h] [-r 1] ecatalogue.dat"
        echo "       $self [-h] [-r 1] ecat1.dat ecat2.dat"
        echo "       awk 'NR==9' ecatalogue.dat | $self [-h] [-r 1]"
	echo ""
        echo "options:"
        echo "         -r rate informs the sampling rate of the displacement time series [1]"
        echo "         -h display this error message and exit"
	echo ""
	echo "build the slip and stress distribution that for each event"
	echo "listed in the catalogue ecatalogue.dat."
	echo ""
        
        exit
}

slip_distribution(){
	grep -v "#" $CATALOGUE | while IFS= read -r line; do
		WDIR=`echo "$line" | awk '{print $10}'`
		FLT=$WDIR/geometry.flt.2d

		START=`echo "$line" | awk '{print $2}'`
		FINAL=`echo "$line" | awk '{print $3}'`

		OUT=$WDIR/event_${FINAL}-${START}.dat

		echo "# processing $OUT"

		files=`grep -v "#" $FLT | awk 'BEGIN{i=9}{if (0 == (NR-1)%8){print i; i=i+1}}' | \
			awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`

		echo "# $self $cmdline" > $OUT
		for i in $files; do
			awk -v s=$START -v f=$FINAL -v r=$RATE 'BEGIN{s=1+int((s-1)/r);f=1+int((f-1)/r)}{
					if (NR==s){for (i=2;i<=NF;i++){u[i]=$i}};
					if (NR==f){for (i=2;i<=NF;i++){$i=$i-u[i]};$1="";print $0; exit}
				}' $i;
		done | paste <(grep -v "#" $FLT | awk 'BEGIN{i=9}{if (0 == (NR-1)%8){print $1,$3,$4}}') - >> $OUT

	done
}

while getopts "hr:" flag
do
	case "$flag" in
		h) hset=1;;
		r) rset=1;RATE=$OPTARG;;
	esac
done
for item in $rset;do
	shift;shift
done
for item in $hset;do
	shift;
done

if [ "$hset" == "1" ]; then
	usage
	exit
fi

# sampling rate
if [ "$rset" != "1" ]; then
	RATE=1
fi

echo "# $self $cmdline"
echo "# fault patches sampled every $RATE steps"

if [ ! -t 0 ]; then
	# reading standard input
	CATALOGUE=-
	slip_distribution
else
	while [ "$#" != "0" ];do
		CATALOGUE=$1
		slip_distribution
		shift
	done
fi

echo "# done"



