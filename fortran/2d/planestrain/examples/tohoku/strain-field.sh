#!/bin/bash

set -e
self=$(basename $0)
cmdline=$*
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

# export slip and stress change for events in the provided catalogue.
# catalogue format is that of ecatalogue.sh

usage(){
        echo "usage: $self [-h] wdir/volume"
	echo ""
        echo "options:"
        echo "         -h display this error message and exit"
	echo ""
	echo "create .xyz files of strain-rate for visualization with GMT."
	echo ""
        
        exit
}

while getopts "h" flag
do
	case "$flag" in
		h) hset=1;;
	esac
done
for item in $hset;do
	shift;
done

if [ "$hset" == "1" ]; then
	usage
	exit
fi

WDIR=$(dirname $1)
STV=$WDIR/$(basename $1 .trv)
SHZ=$WDIR/triangle.dat
OUT=$WDIR/triangle-steps-dedt.xyz

echo "# $self $cmdline"
echo "# $self: processing $SHZ to $OUT"

files=`grep -v "#" $SHZ | awk '{print NR+2}' | \
	awk -v d=$WDIR '{printf "%s/volume-%08d* ",d,$1}'`

for i in $files; do
	# EK33
	#grep -v "#" $i | awk '{printf "%e ",$6}END {printf "\n"}'
	# transient strain
	#grep -v "#" $i | awk '{printf "%e ",sqrt(($8**2+2*$9**2+$10**2)/2)}END {printf "\n"}'
	# total strain-rate
	grep -v "#" $i | awk '{printf "%e ",sqrt(($11**2+2*$12**2+$13**2)/2)}END {printf "\n"}'
done | gawk -v d=$WDIR ' \
	1==ARGIND {
		for(j=2;j<=3;j++){a[j,0+NR]=$j}
	}
	2==ARGIND {
		b[FNR,1,1]=a[2,0+$5];
		b[FNR,1,2]=a[3,0+$5];
		b[FNR,2,1]=a[2,0+$6];
		b[FNR,2,2]=a[3,0+$6];
		b[FNR,3,1]=a[2,0+$7];
		b[FNR,3,2]=a[3,0+$7];
	}
	3==ARGIND {
		printf "> ";
		print $0;
		printf "%e %e\n%e %e\n%e %e\n",b[FNR,1,1],b[FNR,1,2],b[FNR,2,1],b[FNR,2,2],b[FNR,3,1],b[FNR,3,2]
	}' \
	<(grep -v "#" $STV.ned) \
	<(grep -v "#" $STV.trv) - > $OUT

echo "# $self: done"



