#!/bin/bash

set -e
self=$(basename $0)
cmdline=$*
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

# export the postseismic displacement for events in catalogue provided.
# catalogue format is that of ecatalogue.sh

selfdir=$(dirname $0)

usage(){
        echo "usage: $self [-h] [-p prefix] [-r 1] -t tstart/tfinal ecatalogue.dat"
        echo "       $self [-h] [-p prefix] [-r 1] -t tstart/tfinal ecat1.dat ecat2.dat"
        echo "       awk 'NR==9' ecatalogue.dat | $self [-h] [-p prefix] [-r 1] -t tstart/tfinal"
	echo ""
	echo "options:"
        echo "         -p prefix sets the prefix of output files"
        echo "         -r rate informs the sampling rate of the displacement time series"
        echo "         -t tstart/tfinal sets the start and final time of the postseismic period"
	echo ""
	echo "output the displacement that occurred between tstart>=0 and tfinal>=0 after each"
	echo "event in the catalogue ecatalogue.dat."
	echo ""
        
        exit
}

surface_displacement_postseismic(){
	grep -v "#" $CATALOGUE | while IFS= read -r line; do
		WDIR=`echo "$line" | awk '{print $10}'`
		FLT=WDIR/geometry.flt.2d

		ISTART=`echo "$line" | awk '{print $2}'`
		IFINAL=`echo "$line" | awk '{print $3}'`

		OUT=$WDIR/${PREFIX}_${IFINAL}-${ISTART}.dat

		files=`echo "" | awk -v w=$WDIR '{for (i=0;i<=120;i++){printf "%s/opts-%04d.dat\n",w,i+1;}}'`

		echo "# processing $OUT"

		echo "# $self $cmdline" > $OUT
		for i in $files; do
			awk -v e=$ISTART -v tstart=$TSTART -v tfinal=$TFINAL -v r=$RATE 'BEGIN{e=1+int((e-1)/r);s=0} NR>=e{
					if (NR==e){to=$1};
					if ((0==s) && ($1>=to+tstart)){for (i=2;i<=NF;i++){u[i]=$i};s=1};
					if ((1==s) && ($1>=to+tfinal)){for (i=2;i<=NF;i++){$i=$i-u[i]};$1=""; print $0; exit}
				}' $i;
		done | paste <(echo "" | awk '{for (i=0;i<=120;i++){printf "%04d %12.5e 0e3\n",i+1,-100e3+i*5e3;}}') - >> $OUT
	done

}

while getopts "p:r:t:h" flag
do
	case "$flag" in
		p) pset=1;PREFIX=$OPTARG;;
		r) rset=1;RATE=$OPTARG;;
		t) tset=1;TBOUND=$OPTARG;;
		h) hset=1;;
	esac
done
for item in $pset $rset $tset;do
	shift;shift
done
for item in $hset;do
	shift;
done

if [ "$hset" == "1" ]; then
	usage
	exit
fi

if [ "$pset" != "1" ]; then
	PREFIX="post"
fi

# sampling rate
if [ "$rset" != "1" ]; then
	RATE=1
fi

if [ "$tset" == "1" ]; then
	IFS=/ read TSTART TFINAL <<< "${TBOUND}"
else
	echo "error: time bounds must be set." 1>&2
	echo ""
	usage
	exit
fi

echo "# $self $cmdline"
echo "# using prefix $PREFIX"
echo "# displacement data sampled every $RATE steps"
echo "# using interval ${TSTART}/${TFINAL}"

if [ ! -t 0 ]; then
	# reading standard input
	CATALOGUE=-
	surface_displacement_postseismic
else
	while [ "$#" != "0" ];do
		CATALOGUE=$1
		surface_displacement_postseismic
		shift
	done
fi

echo "# $self: done"

