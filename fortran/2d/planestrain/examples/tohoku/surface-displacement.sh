#!/bin/bash

set -e
self=$(basename $0)
cmdline=$*
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

# export the displacement for events in the provided catalogue.
# catalogue format is that of ecatalogue.sh

selfdir=$(dirname $0)

usage(){
        echo "usage: $self [-h] [-r 1] -t tstart/tfinal ecatalogue.dat"
        echo "       $self [-h] [-r 1] -t tstart/tfinal ecat1.dat ecat2.dat"
        echo "       awk 'NR==9' ecatalogue.dat | $self [-h] [-r 1] -t tstart/tfinal"
	echo ""
        echo "options:"
        echo "         -h display this error message and exit"
        echo "         -r rate informs the sampling rate of the displacement time series"
	echo ""
	echo "output the displacement that occurred for each event in the catalogue."
	echo ""
        
        exit
}

surface_displacement(){
	grep -v "#" $CATALOGUE | while IFS= read -r line; do
		WDIR=`echo "$line" | awk '{print $10}'`
		FLT=$WDIR/geometry.flt.2d

		ISTART=`echo "$line" | awk '{print $2}'`
		IFINAL=`echo "$line" | awk '{print $3}'`

		OUT=$WDIR/disp_${IFINAL}-${ISTART}.dat

		echo "# processing $OUT"

		files=`echo "" | awk -v w=$WDIR '{for (i=0;i<=120;i++){printf "%s/opts-%04d.dat\n",w,i+1;}}'`

		echo "# $self $cmdline" > $OUT
		for i in $files; do
			awk -v s=$ISTART -v f=$IFINAL -v r=$RATE 'BEGIN{s=1+int((s-1)/r);f=1+int((f-1)/r)}{
					if (NR==s){for (i=2;i<=NF;i++){u[i]=$i}};
					if (NR==f){for (i=2;i<=NF;i++){$i=$i-u[i]};$1="";print $0; exit}
				}' $i;
		done | paste <(echo "" | awk '{for (i=0;i<=120;i++){printf "%04d %12.5e 0e3\n",i+1,-100e3+i*5e3;}}') - >> $OUT
	done
	
}
while getopts "r:h" flag
do
	case "$flag" in
		r) rset=1;RATE=$OPTARG;;
		h) hset=1;;
	esac
done
for item in $rset;do
	shift;shift
done
for item in $hset;do
	shift;
done

if [ "$hset" == "1" ]; then
	usage
	exit
fi

# sampling rate
if [ "$rset" != "1" ]; then
	RATE=1
fi

echo "# $self $cmdline"
echo "# displacement data sampled every $RATE step(s)"

if [ ! -t 0 ]; then
	# reading standard input
	CATALOGUE=-
	surface_displacement
else
	while [ "$#" != "0" ];do
		CATALOGUE=$1
		surface_displacement
		shift
	done
fi

echo "# $self: done"

