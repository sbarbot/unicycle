#!/bin/bash

set -e
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

# earthquake cycles in viscoelastic medium in the Japan trench near Tohoku.
# mega-splay and megathrust, 60-Myr oceanic lithosphere, 23-Myr mantle wedge,
# cold nose.

selfdir=$(dirname $0)

WDIR=$selfdir/tohoku_1
GDIR=$selfdir/greens
FLT=faults/tohoku.flt.2d

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

if [ ! -e $GDIR ]; then
	echo adding directory $WDIR
	mkdir $GDIR
fi

echo "" | awk 'BEGIN{i=1;pi=atan2(1,0)*2}{
        width=200;
	# accretionary prism, mega-splay
        x2o=39318.6517062
        x3o=0;
        dip=15.739351035515375;
        N=106;
        seg=1;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1e-9,x2,x3,width,dip,seg;
                i=i+1;
        }
	# accretionary prism, megathrust
        x2o=0;
        x3o=0;
        dip=5.5;
        N=300;
        seg=2;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.66e-9,x2,x3,width,dip,seg;
                i=i+1;
        }
	# paleo-prism
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        dip=5;
        N=120;
        seg=3;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,2.66e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
	# seismogenic zone
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        dip=10.6;
        N=290;
        seg=4;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,2.66e-9,x2,x3,width,dip,seg;
                i=i+1;
        }
	# lower-crust shear zone
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        dip=18.4;
        N=280;
        seg=5;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,2.66e-9,x2,x3,width,dip,seg;
                i=i+1;
        }
	# cold nose
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        dip=30;
        N=267;
        seg=6;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,2.66e-9,x2,x3,width,dip,seg;
                i=i+1;
        }
	# upper mantle
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        dip=31;
        N=160;
        seg=7;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,2.66e-9,x2,x3,width,dip,seg;
                i=i+1;
        }
}' > $FLT

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli
30e3 30e3
# time interval
8e10
# number of patches
`grep -v "#" $FLT | wc -l`
# n  Vpl           x2           x3    width      dip
`grep -v "#" $FLT | awk '{print $1,$2,$3,$4,$5,$6,360}'`
# number of frictional patches
`grep -v "#" $FLT | wc -l`
#  n        tau0        mu0        sig          a          b          L         Vo      G/2Vs
`grep -v "#" $FLT | gawk '{ 
	depth=$4; 
	tau0=-1; 
        Vo=1e-6;
	alpha=5;
        switch ($7) { 
	case 1: # mega-splay, N=106
		mu0=0.6;
		sig=10;
		a=1e-2;
		b=1.4e-2;
		L=8e-2;
		alpha=2;
		break;
	case 2: # megathrust, N=300
                mu0=0.6;
                sig=40;
                a=1e-2;
                b=1.8e-2;
                L=8e-2;
		alpha=2;
		break;
	case 3: # paleo-prism, N=120
                mu0=0.3;
                sig=100;
                a=1e-2;
                b=1.4e-2;
                L=5e-2;
		break;
	case 4: # seismogenic zone, N=290
                mu0=0.6;
                sig=100;
                a=1e-2;
                b=1.4e-2;
                L=2e-2;
		break;
	case 5: # lower-crustal shear zone, N=280
                mu0=0.3;
                sig=100;
                a=2e-2;
                b=2.2e-2;
                L=2e-1;
		break;
	case 6: # cold nose, N=267
                mu0=0.6;
                sig=10;
                a=1e-2;
                b=6e-3;
                L=2e-1;
		break;
	case 7: # upper mantle
                mu0=0.6;
                sig=10;
                a=8e-3;
                b=6e-3;
                L=2e-1;
		break;
	default:
		print "error";
        }
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,alpha;\
	}'`
# number of rectangular volumes
#0
# number of triangular volumes
#0
# number of observation patches
`grep -v "#" $FLT | awk '{if (0 == (NR-1)%8){print $0}}' | wc -l | awk '{print $1+8}'`
# n   index rate
  1      53    1
  2     256    1
  3     466    1
  4     671    1
  5     956    1
  6    1230    1
  7    1150    1
  8    1443    1
# subsample the fault in space and time
`grep -v "#" $FLT | awk 'BEGIN{i=9}{if (0 == (NR-1)%8){print i,NR,20;i=i+1}}'`
# number of observation volumes
#0
# number of observation points
121
# n name    x2    x3 rate
`echo "" | awk '{
	for (i=0;i<=120;i++){
		printf "%4d %04d %e 0e3 1\n", i+1,i+1,-100e3+i*5e3;
	}
}'`
# number of events
0
EOF

if [ -e "$GDIR/greens-0000.grd" ]; then
	IMPORTGREENS="--import-greens $GDIR"
fi

time mpirun -n 16 /home/geovault-00/sbarbot/src/unicycle/fortran/2d/planestrain/build/unicycle-ps-ratestate \
	--friction-law 2 \
	--maximum-step 3.15e7 \
	--export-netcdf \
	--export-greens $GDIR \
	--maximum-iterations 10000000 \
	$IMPORTGREENS \
	$* $WDIR/in.param

files=`grep -v "#" $FLT | awk 'BEGIN{i=9}{if (0 == (NR-1)%8){print i; i=i+1}}' | \
		awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`

echo "# export $WDIR/slip-tau.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $2}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$2,$3}'
done | surface -I8/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/slip-tau.grd

echo "# export $WDIR/time-tau.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $1/3.15e7}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$1/3.15e7,$3}'
done | \
	blockmean -I8/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -E | awk '{print $1,$2,$6}' | \
	surface -I8/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/time-tau.grd

echo "# export $WDIR/slip-log10v.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $2}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$2,$5}'
done | surface -I8/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/slip-log10v.grd

echo "# export $WDIR/time-log10v.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $1/3.15e7}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$1/3.15e7,$5}'
done | \
	blockmean -I8/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -E | awk '{print $1,$2,$6}' | \
	surface -I8/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/time-log10v.grd

echo "# export $WDIR/slip-time.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $2}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$2,$1}'
done | surface -I8/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/slip-time.grd

echo "# done post-processing"



