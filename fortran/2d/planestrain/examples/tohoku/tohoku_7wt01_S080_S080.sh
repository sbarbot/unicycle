#!/bin/bash

set -e
self=$(basename $0)
selfdir=$(dirname $0)
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

# earthquake cycles in viscoelastic medium in the Japan trench near Tohoku.
# mega-splay and megathrust, 120-Myr oceanic lithosphere, 53-Myr mantle wedge,
# wet and cold nose in otherwise dry mantle wedge. Basal temperature: 1380 degrees.
# Transient creep with Ak=Am, Qk=Qm, and Gk=G=30e3 MPa. Activation volume: 13e3.
# Afterslip S=80 MPa in the cold nose and S=80 MPa in the upper mantle.

WDIR=$selfdir/tohoku_7wt01_S080_S080
GDIR=$selfdir/greens-viscous-sink

FLT=$WDIR/geometry.flt.2d
SHZ=$WDIR/triangle.dat
STV=$selfdir/faults/mantle-sink

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

if [ ! -e $GDIR ]; then
	echo adding directory $WDIR
	mkdir $GDIR
fi

# basal temperature (degrees)
To=1380

echo "" | awk 'BEGIN{i=1;pi=atan2(1,0)*2}{
        width=200;
	# accretionary prism, mega-splay
        x2o=39318.6517062
        x3o=0;
        dip=15.739351035515375;
        N=106;
        seg=1;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1e-9,x2,x3,width,dip,seg;
                i=i+1;
        }
	# accretionary prism, megathrust
        x2o=0;
        x3o=0;
        dip=5.5;
        N=300;
        seg=2;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,1.66e-9,x2,x3,width,dip,seg;
                i=i+1;
        }
	# paleo-prism
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        dip=5;
        N=120;
        seg=3;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,2.66e-09,x2,x3,width,dip,seg;
                i=i+1;
        }
	# seismogenic zone
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        dip=10.6;
        N=290;
        seg=4;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,2.66e-9,x2,x3,width,dip,seg;
                i=i+1;
        }
	# lower-crust shear zone
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        dip=18.4;
        N=280;
        seg=5;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,2.66e-9,x2,x3,width,dip,seg;
                i=i+1;
        }
	# cold nose
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        dip=30;
        N=267;
        seg=6;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,2.66e-9,x2,x3,width,dip,seg;
                i=i+1;
        }
	# upper mantle
        x2o=x2o+N*cos(pi*dip/180)*width;
        x3o=x3o+N*sin(pi*dip/180)*width;
        dip=31;
        N=160;
        seg=7;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f %d\n", i,2.66e-9,x2,x3,width,dip,seg;
                i=i+1;
        }
}' > $FLT

# rheology
grep -v "#" $STV.ned | awk 'NR==FNR {
	for (j=2;j<=3;j++){a[j,0+NR]=$j}; next} { 
		A2=a[2,0+$5];
		A3=a[3,0+$5];
		B2=a[2,0+$6];
		B3=a[3,0+$6];
		C2=a[2,0+$7];
		C3=a[3,0+$7];
		x2=(A2+B2+C2)/3.0;
		x3=(A3+B3+C3)/3.0;
		print x2,x3,$2,$3,$4;
	}' - <( grep -v "#" $STV.trv ) | awk -v To=$To '
	function ramp(x){return ((x>0)?x:0)}; 
	function erf(x){
		p=0.3275911;
		a1=0.254829592;
		a2=-0.284496736;
		a3=1.421413741;
		a4=-1.453152027;
		a5=1.061405429;
		t=1/(1+p*x);
		return 1-(a1*t+a2*t**2+a3*t**3+a4*t**4+a5*t**5)*exp(-x**2)
	}
	BEGIN{pi=atan2(1,0)*2}{
		# centroid position
		x2=$1;
		x3=$2;
		# background strain rate
		e22=$3;
		e23=$4;
		e33=$5;
		# slab geometry
		y2=126.6266e3;
		y3=0;
		dip=29.46*pi/180;
		n2= sin(dip);
		n3=-cos(dip);
		# rheology
		g=9.8;
		rho=3330;
		P=rho*g*x3;
		R=8.314;
		# thermal model
		k=3.138; 
		Cp=1171;
		Kappa=k/(rho*Cp);
		if ((x2-y2)*n2+(x3-y3)*n3>=0){
			# mantle wedge
			A=(350e3>=x2)?90:27;
			n=3.5;            # stress power exponent
			r=1.2;            # water-content power exponent
			Vol=11e-6;        # activation volume
			# activation enthalpy
			H=((350e3>=x2)?490e3:510e3)+P*Vol;
			# water content (wet in the mantle wedge corner)
			COH=(350e3>=x2)?4000:1000;
			# thermal age of the lithosphere
			age=53e6*3.15e7;
			# distance from slab
			z3=(x2-y2)*n2+(x3-y3)*n3;
			# temperature profile
			T=273.15+To*erf(x3/(sqrt(4*Kappa*age)))-900*exp(-z3/25e3);
			E0=A*COH**r*exp(-H/(R*T));
			Epl=sqrt((e22**2+2*e23**2+e33**2)/2);
			tau0=(Epl/E0)**(1/n);
		}
		else {
			# oceanic lithosphere
			A=90;
			n=3.5;           # stress power exponent
			r=1.2            # water-content power exponent
			Vol=13e-6;       # activation volume
			H=510e3+P*Vol;   # activation enthalpy
			COH=1000;        # water content (dry)
			age=120e6*3.15e7; # thermal age of the lithosphere
			z3=x3-ramp(-(x2-y2)*n2/n3);
			T=273.15+To*erf(z3/(sqrt(4*Kappa*age)));
			E0=A*COH**r*exp(-H/(R*T));
			Epl=sqrt((e22**2+2*e23**2+e33**2)/2);
			tau0=(Epl/E0)**(1/n);
		}
		if (x2>1e7){A=0}
		eta0=(Epl>0)?tau0/Epl*1e6:1e30;
		printf "%4d %e %e %e %e %e %e %e %e %e\n", NR,-tau0,0,tau0,A*COH**r,n,H,R,T,eta0;
	}' > $SHZ

# export GMT files
grep -v "#" $STV.ned | awk 'NR==FNR {
	for (j=2;j<=3;j++){a[j,0+NR]=$j}; next} { 
		print a[2,0+$5],a[3,0+$5],a[2,0+$6],a[3,0+$6],a[2,0+$7],a[3,0+$7];
	}' - <( grep -v "#" $STV.trv ) | \
		paste - $SHZ | \
		awk '{printf "> -Z%e\n%e %e\n%e %e\n%e %e\n",$15-273.15,$1,$2,$3,$4,$5,$6}' > $WDIR/triangle-temp.xyz

grep -v "#" $STV.ned | awk 'NR==FNR {
	for (j=2;j<=3;j++){a[j,0+NR]=$j}; next} { 
		print a[2,0+$5],a[3,0+$5],a[2,0+$6],a[3,0+$6],a[2,0+$7],a[3,0+$7];
	}' - <( grep -v "#" $STV.trv ) | \
		paste - $SHZ | \
		awk '{printf "> -Z%e\n%e %e\n%e %e\n%e %e\n",$10,$1,$2,$3,$4,$5,$6}' > $WDIR/triangle-tau0.xyz

grep -v "#" $STV.ned | awk 'NR==FNR {
	for (j=2;j<=3;j++){a[j,0+NR]=$j}; next} { 
		print a[2,0+$5],a[3,0+$5],a[2,0+$6],a[3,0+$6],a[2,0+$7],a[3,0+$7];
	}' - <( grep -v "#" $STV.trv ) | \
		paste - $SHZ | \
		awk '{printf "> -Z%e\n%e %e\n%e %e\n%e %e\n",log($16)/log(10),$1,$2,$3,$4,$5,$6}' > $WDIR/triangle-eta0.xyz

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli
30e3 30e3
# time interval
1e11
# number of patches
`grep -v "#" $FLT | wc -l`
# n  Vpl           x2           x3    width      dip
`grep -v "#" $FLT | awk '{print $1,$2,$3,$4,$5,$6,360}'`
# number of frictional patches
`grep -v "#" $FLT | wc -l`
#    n      tau0      mu0       sig            a            b            L           Vo    G/2Vs
`grep -v "#" $FLT | gawk '{ 
	x2=$3;
	depth=$4; 
	tau0=-1; 
        Vo=1e-6;
	alpha=5;
        switch ($7) { 
	case 1: # mega-splay, N=106
		mu0=0.6;
		sig=20;
		a=1e-2;
		b=1.4e-2;
		L=8e-2;
		alpha=2;
		break;
	case 2: # megathrust, N=300
                mu0=0.6;
                sig=60;
                a=1e-2;
                b=1.8e-2;
                L=8e-2;
		alpha=2;
		break;
	case 3: # paleo-prism, N=120
                mu0=0.3;
                sig=110;
                a=1e-2;
                b=1.4e-2;
                L=5e-2;
		break;
	case 4: # seismogenic zone, N=290
                mu0=0.6;
                sig=110;
                a=1e-2;
                b=1.4e-2;
                L=1e-2;
		break;
	case 5: # lower-crustal shear zone, N=280
		mu0=0.3;
                a=2e-2;
                sig=(x2<155e3)?70:20;
                b  =(x2<155e3)?1.2e-2:2.2e-2;
                L  =(x2<155e3)?1e-1:5e-3;
		break;
	case 6: # cold nose, N=267
		mu0=0.6;
                sig=80;
                a=2e-2;
                b=1e-2;
                L=2e-1;
		break;
	case 7: # upper mantle
                mu0=0.6;
                sig=80;
                a=2e-2;
                b=1e-2;
                L=2e-1;
		break;
	default:
		print "error";
		exit -1;
        }
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,alpha;\
	}'`
# number of rectangle volumes
0
# number of triangle volumes
`grep -v "#" $STV.trv | wc -l`
# n e22 e23 e33 i1 i2 i3
`grep -v "#" $STV.trv`
# number of vertices
`grep -v "#" $STV.ned | wc -l`
# n       x2     x3
`grep -v "#" $STV.ned`
# number of nonlinear Maxwell properties
`grep -v "#" $STV.trv | wc -l`
# n s22  s23  s33 gammadot0m   n     Q     R
`grep -v "#" $SHZ | awk '{printf "%4d %e %e %e %e %e %e %e\n", NR,$2,$3,$4,$5,$6,$7,$8}'`
# number of nonlinear Kelvin properties
`grep -v "#" $STV.trv | wc -l`
# n gammadot0k  Gk nk    Qk    Rk
`grep -v "#" $SHZ | awk '{printf "%4d %e %e %e %e %e\n", NR,$5,30e3,$6,$7,$8}'`
# number of thermal properties
`grep -v "#" $STV.trv | wc -l`
# n  rhoc  temperature
`grep -v "#" $SHZ | awk '{printf "%4d %f %f\n", NR,1,$9}'`
# number of observation patches
`grep -v "#" $FLT | awk '{if (0 == (NR-1)%8){print $0}}' | wc -l | awk '{print $1+8}'`
# n   index rate
  1      53    1
  2     256    1
  3     466    1
  4     671    1
  5     956    1
  6    1230    1
  7    1150    1
  8    1443    1
# subsample the fault in space and time
`grep -v "#" $FLT | awk 'BEGIN{i=9}{if (0 == (NR-1)%8){print i,NR,20;i=i+1}}'`
# number of observation volumes
`grep -v "#" $SHZ | wc -l | awk '{print $1+2}'`
# n    i rate
  1  823    1
  2  645    1
# subsample the volume elements
`grep -v "#" $SHZ | awk '{print NR+2,NR,50}'`
# number of observation points
121
# n name    x2    x3 rate
`echo "" | awk '{
	for (i=0;i<=120;i++){
		printf "%4d %04d %e 0e3 1\n", i+1,i+1,-100e3+i*5e3;
	}
}'`
EOF

if [ -e "$GDIR/greens-0000.grd" ]; then
	IMPORTGREENS="--import-greens $GDIR"
fi

time mpirun -n 16 unicycle-ps-viscouscycles \
	--friction-law 2 \
	--maximum-step 3.15e7 \
	--export-netcdf \
	--export-greens $GDIR \
	--maximum-iterations 12000000 \
	$IMPORTGREENS \
	$* $WDIR/in.param

# number of fault patches
N=`grep -v "#" $FLT | wc -l | awk '{print $1}'`

files=`grep -v "#" $FLT | awk 'BEGIN{i=9}{if (0 == (NR-1)%8){print i; i=i+1}}' | \
		awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`

echo "# export $WDIR/slip-tau.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $2}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$2,$4}'
done | surface -I8/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/slip-tau.grd

echo "# export $WDIR/time-tau.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $1/3.15e7}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$1/3.15e7,$4}'
done | \
	blockmean -I8/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -E | awk '{print $1,$2,$6}' | \
	surface -I8/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/time-tau.grd

echo "# export $WDIR/slip-log10v.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $2}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$2,$6}'
done | surface -I8/`echo $yRange | awk '{print $1/1024}'` -R1/$N/0/$yRange -G$WDIR/slip-log10v.grd

echo "# ${self}: compute $WDIR/ecatalogue.dat"
ecatalogue.sh $WDIR/time.dat | awk '{if (NR<17){if ("#"!=substr($0,0,1)){print "#",$0}else{print $0}}else{print $0}}' > $WDIR/ecatalogue.dat

echo "# ${self}: compute slip distributions"
./slip-distribution.sh -r 20 $WDIR/ecatalogue.dat

echo "# ${self}: compute surface displacements"
./surface-displacement.sh -r 1 $WDIR/ecatalogue.dat

echo "# ${self}: compute field and time series of strain-rate"
./strain-field.sh $WDIR/volume

echo "# ${self}: compute post-seismic displacement for profile_1104-1112"
./surface-displacement-postseismic.sh -p profile_1104-1112 -r 1 -t 1814400/22896000 $WDIR/ecatalogue.dat

echo "# ${self}: compute post-seismic displacement for profile_1209-1605"
./surface-displacement-postseismic.sh -p profile_1209-1605 -r 1 -t 46656000/162259200 $WDIR/ecatalogue.dat

echo "# ${self}: done"



