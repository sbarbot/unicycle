#!/bin/bash

set -e
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

# Megathrust earthquakes. W=100.5 km. h*=3750m. Ru=26.66. Rb=0.28.

selfdir=$(dirname $0)

WDIR=$selfdir/brittle1

FLT=$WDIR/flt.2d

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

echo "" | awk 'BEGIN{i=1;pi=atan2(1,0)*2}{
        x2o=0;
        x3o=0;
        width=500;
        dip=5.7106;
        N=600;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f\n", j+1,1.0e-09,x2,x3,width,dip;
        }
}' > $FLT

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# rigidity, Lame parameter
30e3 30e3
# time interval
5e10
# number of patches
`grep -cv "#" $FLT`
# n  Vl           x2           x3    width      dip
`grep -v "#" $FLT`
# number of frictional patches
`grep -cv "#" $FLT`
#  n        tau0        mu0        sig          a          b          L         Vo      G/2Vs
`grep -v "#" $FLT | awk '{ 
	x2=$3;
	x3=$4; 
	tau0=-1; 
	mu0=0.6;
	sig=100;
	a=1.0e-2;
	b=(x3 >= 10e3 && x3 <= 20e3)?1.4e-2:0.6e-2;
	L=5e-2;
	Vo=1e-6;
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,5;\
	}'`
# number of observation patches
200
# n   index  rate
`echo "" | awk '{n=600;for (i=1;i<n;i=i+3){
	print 1+(i-1)/3, i, 20;
	}}'`
# number of observation points
100
# n name    x2    x3
`echo "" | awk '{
	for (i=0;i<100;i++){
		printf "%4d %04d %e 0e3 1\n", i+1,i+1,-100e3+i*7e3+10;
	}
}'`
# number of events
0
EOF

if [ -e "$WDIR/greens-0000.grd" ]; then
	IMPORTGREENS="--import-greens $WDIR"
fi

time mpirun -n 2 unicycle-ps-ratestate \
	--maximum-step 1e7 \
	--export-netcdf \
	--export-greens $WDIR \
	--maximum-iterations 100000 \
	$IMPORTGREENS \
	$* $WDIR/in.param

echo "# exporting $WDIR/time-log10v.grd"
xRange=$(gmt grdinfo -C $WDIR/log10v.grd | awk '{print $3}')
steps=$(gmt grdinfo -C $WDIR/log10v.grd | awk '{print $5*20}')
yRange=$(grep -v "#" $WDIR/time.dat | awk -v i="$steps" 'NR==i{print $1/3.15e7;next}')
dt=$(echo "$yRange" | awk '{print $1/500}')
join -o 1.2,2.2,1.3 <(gmt grd2xyz $WDIR/log10v.grd | awk '{printf "%06d %06d %20.16f\n",$2,$1,$3}' | sort) \
	<(grep -v "#" $WDIR/time.dat | awk '0==NR%20 {printf "%06d %20.17f\n",NR/20,$1/3.15e7}') |
	gmt blockmode -E -I1/$dt -R1/$xRange/0/$yRange | awk '{print $1,$2,$NF}' | \
	gmt surface -I1/$dt -R1/$xRange/0/$yRange -G$WDIR/time-log10v.grd
yRange=""

