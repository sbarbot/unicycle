#!/bin/bash -e

# extra script for mapping tool grdmap.sh
#
#   grdmap.sh -e ecoasts.sh selfdir/file.grd
#

self=$(basename $0)
selfdir=$(dirname $0)
cmdline=$*
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

while getopts "b:c:gp:v:H:J:" flag
do
	case "$flag" in
	b) bset=1;bds=$OPTARG;;
	c) cset=1;carg=$OPTARG;;
	g) gset=1;;
	p) pset=1;U3=$OPTARG;;
	v) vset=1;SIZE=$OPTARG;VECTOR=$OPTARG"c";;
	J) Jset="-J";PROJ=$OPTARG;;
	H) Hset=1;HEIGHT=$OPTARG;;
	esac
done
for item in $bset $cset $pset $vset $Hset $Jset; do
	shift;shift
done
for item in $gset; do
	shift
done

echo $self: $cmdline
PSFILE=$1

if [ "$#" -lt "1" ]; then
	echo ${self} overlays .xy data on map.
	echo
	echo usage: $self -b xmin/xmax/ymin/ymax file.ps
	exit 1
fi

if [ "$gset" == "" ]; then
	if [ "$Jset" != "" ]; then
		UNIT=ll
	else
		UNIT=km
	fi
else
	UNIT=ll
	PROJ=M$HEIGHT
fi

VOLUME=$(basename $1 .ps)

echo "# plotting $(dirname $1)/$VOLUME.xy"

# color scale for vertical
# for diffusion creep
#gmt makecpt -T-15/-14/0.01 -C/Users/sbarbot/Documents/src/relax/share/turbo.cpt -Z > $selfdir/strain-rate.cpt
# for dislocation creep
gmt makecpt -T-15/-12/0.01 -C/Users/sbarbot/Documents/src/relax/share/turbo.cpt -Z > $selfdir/strain-rate.cpt
gmt psxy -O -K -J$PROJ -R$bds -P -L -W0.5p,black -C$selfdir/strain-rate.cpt $(dirname $1)/$VOLUME.xy >> $PSFILE


