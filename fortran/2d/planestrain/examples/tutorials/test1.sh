#!/bin/bash

set -e
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

# Megathrust earthquake cycles.

selfdir=$(dirname $0)

WDIR=$selfdir/test-results/test1

FLT=$WDIR/flt.2d

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir -p $WDIR
fi

echo "" | awk 'BEGIN{i=1;pi=atan2(1,0)*2}{
        x2o=0;
        x3o=0;
        width=500;
        dip=5.7106;
        N=600;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f\n", j+1,1.0e-09,x2,x3,width,dip;
        }
}' > $FLT

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# rigidity, Lame parameter
30e3 30e3
# time interval
3e10
# number of patches
`grep -v "#" $FLT | wc -l`
# n  Vpl           x2           x3    width      dip
`grep -v "#" $FLT`
# number of frictional patches
`grep -v "#" $FLT | wc -l`
#  n        tau0        mu0        sig          a          b          L         Vo      G/2Vs
`grep -v "#" $FLT | awk '{ 
	x2=$3;
	x3=$4; 
	tau0=-1; 
	mu0=0.6;
	sig=100;
	a=1.0e-2;
	b=(x3 >= 10e3 && x3 <= 20e3)?1.4e-2:0.6e-2;
	L=5e-2;
	Vo=1e-6;
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,5;\
	}'`
# number of observation patches
0
# number of observation points
0
# number of events
0
EOF

mpirun -n 2 --allow-run-as-root unicycle-ps-ratestate \
	--maximum-step 1e7 \
	--export-netcdf \
	--export-greens $WDIR \
	--maximum-iterations 10000000 \
	$* $WDIR/in.param

ls -al $WDIR/{log10v.grd,time.dat,greens-0000.grd}

gnuplot <<EOF
set term dumb
set logscale y
set xlabel "Time (year)"
set ylabel "Velocity (m/s)"
plot '$WDIR/time.dat' using (\$1/3.15e7):3 with line title ''
EOF

echo "# test successful" | tee $WDIR/report.xml

