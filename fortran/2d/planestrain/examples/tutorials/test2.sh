#!/bin/bash

set -e
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

# Megathrust earthquakes with viscoelastic relaxation in the 
# mantle wedge and in the oceanic asthenoephere with triangle
# volume elements.

selfdir=$(dirname $0)

WDIR=$selfdir/test-results/test2

FLT=$WDIR/flt.2d
STV=$selfdir/mantle

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir -p $WDIR
fi

echo "" | awk 'BEGIN{i=1;pi=atan2(1,0)*2}{
        x2o=0;
        x3o=0;
        width=500;
        dip=5.7106;
        N=600;
        for (j=0;j<N;j++){
                x2=x2o+j*cos(pi*dip/180)*width;
                x3=x3o+j*sin(pi*dip/180)*width;
                printf "%05d %e %16.11e %16.11e %f %f\n", j+1,1.0e-09,x2,x3,width,dip;
        }
}' > $FLT

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# rigidity, Lame parameter
30e3 30e3
# time interval
2e10
# number of patches
`grep -v "#" $FLT | wc -l`
# n  Vpl           x2           x3    width      dip
`grep -v "#" $FLT`
# number of frictional patches
`grep -v "#" $FLT | wc -l`
#  n        tau0        mu0        sig          a          b          L         Vo      G/2Vs
`grep -v "#" $FLT | awk '{ 
	x2=$3;
	x3=$4; 
	tau0=-1; 
	mu0=0.6;
	sig=100;
	a=1.0e-2;
	b=(x3 >= 10e3 && x3 <= 20e3)?1.4e-2:0.6e-2;
	L=5e-2;
	Vo=1e-6;
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,5;\
	}'`
# number of rectangle volumes
0
# number of triangle volumes
$(grep -cv "#" $STV.trv)
# n e22 e23 e33 i1 i2 i3
$(grep -v "#" $STV.trv)
# number of vertices
$(grep -cv "#" $STV.ned)
# n       x2     x3
$(grep -v "#" $STV.ned)
# number of nonlinear Maxwell triangle volume elements
$(grep -cv "#" $STV.trv)
# n sII gammadot0m   n
$(grep -v "#" $STV.ned | awk 'NR==FNR {
	for (j=2;j<=3;j++){a[j,0+NR]=$j}; next} { 
		A2=a[2,0+$5];
		A3=a[3,0+$5];
		B2=a[2,0+$6];
		B3=a[3,0+$6];
		C2=a[2,0+$7];
		C3=a[3,0+$7];
		x2=(A2+B2+C2)/3.0;
		x3=(A3+B3+C3)/3.0;
		print x2,x3;
	}' - <( grep -v "#" $STV.trv ) | awk 'function erf(x){p=0.3275911;a1=0.254829592;a2=-0.284496736;a3=1.421413741;a4=-1.453152027;a5=1.061405429;t=1/(1+p*x);return 1-(a1*t+a2*t^2+a3*t^3+a4*t^4+a5*t^5)*exp(-x^2)}{
		x2=$1;
		x3=$2;
		g=9.8;
		n=1;
		rho=3330;
		P=rho*g*x3;
		Vol=4e-6;
		H=335e3+P*Vol;
		R=8.31446261815324;
		k=3.138; 
		Cp=1171;
		rho=3330;
		Kappa=k/(rho*Cp);
		age=2e15;
		T=273+1400*erf(x3/(sqrt(4*Kappa*age)))
		gammadot0=1e-3*exp(-H/(R*T));
		printf "%4d %f %e %f\n", NR,-1,gammadot0,n;
	}')
# number of nonlinear Kelvin triangle volume elements
0
# number of observation patches
0
# number of observation volumes
0
# number of observation points
0
# number of events
0
EOF

time mpirun -n 2 --allow-run-as-root unicycle-ps-viscouscycles \
	--maximum-step 1e7 \
	--export-netcdf \
	--export-volume \
	--export-greens $WDIR \
	--maximum-iterations 1000000 \
	$* $WDIR/in.param

ls -al $WDIR/{log10v.grd,time.dat,greens-0000.grd}

gnuplot <<EOF
set term dumb
set logscale y
set xlabel "Time (year)"
set ylabel "Velocity (m/s)"
plot '$WDIR/time.dat' using (\$1/3.15e7):3 with line title 'Velocity (m/s)'
EOF

gnuplot <<EOF
set terminal dumb
set logscale y
set xlabel "Time (year)"
set ylabel "Plastic strain-rate (1/s)"
plot '$WDIR/time.dat' using (\$1/3.15e7):5 with line title 'Strain-rate (/s)'
EOF

echo "# test successful" | tee $WDIR/report.xml

