#!/bin/bash

set -e
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

# earthquake cycle in plane strain in a ramp/flat/ramp
#
#   \
#    \ ramp
#     \
#      \  decollement
#       +-------------+
#                      \
#                       \ ramp
#                        \
#                         \
#
# using the geometry of Hubbard et al. (2014)
#
#   Hubbard et al., Structure and seismic hazard of the 
#   Ventura Avenue anticline and Ventura fault, California: 
#   Prospect for large, multisegment ruptures in the western
#   Transverse Ranges. BSSA, 104.3, 1070-1087 (2014).

selfdir=$(dirname $0)

WDIR=$selfdir/8_1

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

# dip angle of decollement: 8
FLT=faults/ventura-8.flt.2d

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli
30e3 30e3
# time interval
3.15e11
# number of patches
`grep -v "#" $FLT | wc -l`
# n  Vpl           x2           x3    width      dip
`grep -v "#" $FLT`
# number of frictional patches
`grep -v "#" $FLT | wc -l`
#  n        tau0        mu0        sig          a          b          L         Vo      G/2Vs
`grep -v "#" $FLT | awk '{ \
	depth=$4; \
	tau0=-1; \
	mu0=0.2; \
	sig=100; \
	a=1e-2; \
	b=(depth<10.7e3 && depth>7.3e3)?6e-3:(depth>20e3)?6e-3:1.4e-2; \
	L=1e-2; \
	Vo=1e-6; \
	print NR,tau0,mu0,sig,a,b,L,Vo,5; \
	}'`
# number of observation patches
11
# n   index rate
  1       2    1
  2      10    1
  3      66    1
  4      67    1
  5     100    1
  6     150    1
  7     180    1
  8     200    1
  9     216    1
 10     217    1
 11     300    1
# number of observation points
2
# n name    x2    x3 rate
  1 0001 -20e3     0    1
  2 0002   0e3     0    1
# number of events
0
EOF

time mpirun -n 2 unicycle-ps-ratestate \
	--epsilon 1e-6 \
	--maximum-step 3.15e6 \
        --export-netcdf \
        --event-velocity-threshold 0.1 \
        --export-greens $WDIR \
        $* $WDIR/in.param

