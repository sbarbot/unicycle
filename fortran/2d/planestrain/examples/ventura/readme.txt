
# earthquake cycle in plane strain in a ramp/flat/ramp
#
#   \
#    \ ramp
#     \
#      \  decollement
#       +-------------+
#                      \
#                       \ ramp
#                        \
#                         \
#
# using the geometry of Hubbard et al. (2014)
#
#   Hubbard et al., Structure and seismic hazard of the 
#   Ventura Avenue anticline and Ventura fault, California: 
#   Prospect for large, multisegment ruptures in the western
#   Transverse Ranges. BSSA, 104.3, 1070-1087 (2014).

# create a geometry .grd file for pseudo-3d plotting
grep -v "#" faults/ventura-0.flt.2d | awk '{print $3,$4}' | awk -v n=`grdinfo -C b0_1/log10v.grd | awk '{print $5}'` '{for (i=1;i<=n;i++){print NR,i,-$2/1e3}}' | xyz2grd -R0_1/log10v.grd -G0_1/geometry.grd

# plot the simulation result in pseudo-3d
grdview.sh -H -p -12/1/0.01 -E 160/30 -v 20 -c creep.cpt -t 500 -G 0_1/log10v.grd 0_1/geometry.grd

# create a geometry .grd file for pseudo-3d plotting
grep -v "#" faults/largepatch/ventura-2.flt.2d | awk '{print $3,$4}' | awk -v n=`grdinfo -C 2_1/log10v.grd | awk '{print $5}'` '{for (i=1;i<=n;i++){print NR,i,-$2/1e3}}' | xyz2grd -R2_1/log10v.grd -G2_1/geometry.grd

# plot the simulation result in pseudo-3d
grdview.sh -H -p -12/1/0.01 -E 160/30 -v 20 -c creep.cpt -t 500 -G 2_1/log10v.grd 2_1/geometry.grd
