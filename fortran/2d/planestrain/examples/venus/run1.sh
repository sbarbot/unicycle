#!/bin/bash

set -e
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

selfdir=$(dirname $0)

WDIR=$selfdir/output1
FLT=$WDIR/flt.2d
GDIR=$selfdir/greens

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

if [ ! -e $GDIR ]; then
	echo adding directory $GDIR
	mkdir $GDIR
fi

WIDTH=40
NW=2000
DIP=60

echo "" | awk -v w=$WIDTH -v nw=$NW -v d=$DIP \
	'BEGIN{d2r=atan2(1,0)/90}{
		for (j=1;j<=nw;j++){
			Vpl=1e-9;
			d2=1+cos(d2r*d)-1;
			d3=1+sin(d2r*d)-1;
			printf "%3d %14.7e %14.7e %14.7e %14.7e %f\n",
				j,Vpl,(j-1)*w*d2,1e2+(j-1)*w*d3,w,d
		}
	}' > $FLT

FRICTION=`grep -v "#" $FLT | awk -v nw=$NW 'function min(x,y){return (x<y)?x:y}{ \
	depth=$4;
	V=$2;
	a=1e-2;
	L=5e-2;
	sig=100;
	Vo=1e-6;
	Vs=3e3;
	G=30e3;
	# seismogenic zone (large stress drop)
	if (depth>=5e3 && depth<=20e3){
		if (depth>=10e3 && depth<=15e3){
			# weak zone, low stress drop
			b=a+4e-3;
			L=1e-2;
		} else {
			# strong zone, strong stress drop
			b=a+6e-2;
		}
	} else {
		# velocity-strengthening region
		b=a-4e-3;
	}
	# weak region
	mu0 =(depth>=10e3 && depth<=15e3)?0.1:0.6;
	tau0=-1;
	printf "%3d %16.8e %16.8e %16.8e %16.8e %16.8e %16.8e %12.4e %d\n",
	         NR,  tau0,   mu0,   sig,     a,     b,     L,    Vo, 5}'`

echo "$FRICTION" > $WDIR/friction.dat

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli
30e3 30e3
# time interval
4e11
# number of patches
`grep -v "#" $FLT | wc -l`
# n  Vpl           x2           x3    width      dip
`grep -v "#" $FLT`
# number of frictional patches
`grep -v "#" $FLT | wc -l`
# n tau0   mu0       sig         a         b         L        Vo     G/2Vs
`echo "$FRICTION"`
# number of observation patches
`grep -v "#" $FLT | awk '{if (0 == (NR-1)%4){print $0}}' | wc -l | awk '{print $1+9}'`
# n   index rate
  1      50    1
  2     100    1
  3     150    1
  4     200    1
  5     300    1
  6     350    1
  7     400    1
  8     450    1
  9     550    1
# subsample the fault in space and time
`grep -v "#" $FLT | awk 'BEGIN{i=10}{if (0 == (NR-1)%4){print i,NR,20;i=i+1}}'`
# number of observation points
0
# number of events
0
EOF

if [ -e "$GDIR/greens-0000.grd" ]; then
	IMPORTGREENS="--import-greens $GDIR"
fi

time mpirun -n 2 unicycle-ps-ratestate \
	--export-netcdf \
	--epsilon 1e-6 \
	--maximum-step 3.15e7 \
	--friction-law 1 \
	--export-greens $GDIR \
	--maximum-iterations 10000000 \
	$IMPORTGREENS \
	$* $WDIR/in.param

files=`grep -v "#" $FLT | awk 'BEGIN{i=10}{if (0 == (NR-1)%4){print i; i=i+1}}' | \
	awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`

echo "# exporting to $WDIR/slip-log10v.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $2}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$2,$6}'
done | surface -I4/`echo $yRange | awk '{print $1/1024}'` -R1/$NW/0/$yRange -G$WDIR/slip-log10v.grd

echo "# exporting to $WDIR/slip-tau.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $2}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	cat $i | awk -v i=$index '{print i,$2,$3}'
done | surface -I4/`echo $yRange | awk '{print $1/1024}'` -R1/$NW/0/$yRange -G$WDIR/slip-tau.grd

echo "# exporting to $WDIR/step-log10v.grd"
yRange=`for i in $files; do wc -l $i | awk '{print $1}'; done | minmax -C | awk '{print ($2-1)*20+1}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	grep -v "#" $i | awk -v i=$index '{print i,(NR-1)*20+1,$6}'
done | xyz2grd -I4/20 -R1/$NW/1/$yRange -G$WDIR/step-log10v.grd

echo "# exporting to $WDIR/step-tau.grd"
yRange=`for i in $files; do wc -l $i | awk '{print $1}'; done | minmax -C | awk '{print ($2-1)*20+1}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	grep -v "#" $i | awk -v i=$index '{print i,(NR-1)*20+1,$3}'
done | xyz2grd -I4/20 -R1/$NW/1/$yRange -G$WDIR/step-tau.grd

echo "# exporting to $WDIR/time-log10v.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $1/3.15e7}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	grep -v "#" $i | awk -v i=$index '{print i,$1/3.15e7,$6}'
done | \
	blockmean -I4/`echo $yRange | awk '{print $1/2048}'` -R1/$NW/0/$yRange -E | awk '{print $1,$2,$6}' | \
	surface -I4/`echo $yRange | awk '{print $1/2048}'` -R1/$NW/0/$yRange -G$WDIR/time-log10v.grd

echo "# exporting to $WDIR/time-tau.grd"
yRange=`for i in $files; do tail -n 1 $i | awk '{print $1/3.15e7}'; done | minmax -C | awk '{print $2}'`
for i in $files; do
	index=`echo $(basename $i .dat) | awk -F "-" '{print $3}'`
	grep -v "#" $i | awk -v i=$index '{print i,$1/3.15e7,$3}'
done | \
	blockmean -I4/`echo $yRange | awk '{print $1/2048}'` -R1/$NW/0/$yRange -E | awk '{print $1,$2,$6}' | \
	surface -I4/`echo $yRange | awk '{print $1/2048}'` -R1/$NW/0/$yRange -G$WDIR/time-tau.grd

