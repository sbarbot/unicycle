
!------------------------------------------------------------------------
!> function boxcar
!! computes the boxcar function
!------------------------------------------------------------------------
REAL*8 FUNCTION boxcar(x)
  REAL*8, INTENT(IN) :: x

   IF (ABS(x) .LE. 0.5d0) THEN
      boxcar=1.0d0
   ELSE
      boxcar=0.0d0
   END IF

END FUNCTION boxcar

