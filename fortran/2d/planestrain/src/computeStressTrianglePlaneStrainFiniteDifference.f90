!------------------------------------------------------------------------
!! subroutine computeStressTrianglePlaneStrainFiniteDifference
!! computes the displacement field associated with a triangle volume
!! element using the analytic solution of
!!
!!   Barbot S., Deformation of a Half-Space from Anelastic Strain Confined
!!   in a Tetrahedral Volume, submitted to Bull. Seism. Soc. Am., 2018.
!!
!! and considering the following geometry:
!!
!!              surface
!!      -------------+-------------- E (x2)
!!                   |
!!                   |     + A
!!                   |    /  . 
!!                   |   /     .  
!!                   |  /        .            
!!                   | /           .      
!!                   |/              + B
!!                   /            .
!!                  /|          /  
!!                 / :       .
!!                /  |    /
!!               /   : .
!!              /   /|
!!             / .   :
!!            +      |
!!          C        :
!!                   |
!!                   D (x3)
!!
!!
!! INPUT:
!! x2, x3             east coordinates and depth of the observation point,
!! A, B, C            coordinates of the 3 vertices A = ( xA, zA),
!! eij                source strain component 22, 23 and 33 in the shear zone,
!! G                  rigidity in the half space.
!! nu                 Poisson's ratio the half space.
!!
!! OUTPUT:
!! s22                horizontal stress,
!! s23                shear stress,
!! s33                vertical stress.
!!
!! \author Sylvain Barbot (03/12/18) - original Fortran form
!-----------------------------------------------------------------------
SUBROUTINE computeStressTrianglePlaneStrainFiniteDifference( &
                x2,x3,A,B,C,e22,e23,e33,G,nu,s22,s23,s33)

  IMPLICIT NONE

  REAL*8, INTENT(IN) :: x2,x3
  REAL*8, DIMENSION(2), INTENT(IN) :: A,B,C
  REAL*8, INTENT(IN) :: e22,e23,e33
  REAL*8, INTENT(IN) :: G,nu
  REAL*8, INTENT(OUT) :: s22,s23,s33
    
  REAL*8, PARAMETER :: PI = 3.141592653589793115997963468544185161_8

  ! Heaviside function
  REAL*8, EXTERNAL :: heaviside

  ! normal vectors
  REAL*8, DIMENSION(2) :: nA, nB, nC

  ! moment density
  REAL*8 :: m22,m23,m33

  ! Lame parameter
  REAL*8 :: lambda

  ! displacement gradients
  REAL*8 :: U22, U23, U32, U33

  ! displacement offsets
  REAL*8 :: u2p2,u2p3,u2m2,u2m3
  REAL*8 :: u3p2,u3p3,u3m2,u3m3

  ! position
  REAL*8 :: omega

  ! dimension
  REAL*8 :: l, step

  ! check valid parameters
  IF ((-1._8 .GT. nu) .OR. (0.5_8 .LT. nu)) THEN
     WRITE (0,'("error: -1<=nu<=0.5, nu=",ES9.2E2," given.")') nu
     STOP 1
  END IF

  IF (0 .GT. x3) THEN
     WRITE (0,'("error: observation depth (x3) must be positive")')
     STOP 1
  END IF

  IF ((0 .GT. A(2)) .OR. &
      (0 .GT. B(2)) .OR. &
      (0 .GT. C(2))) THEN
     WRITE (0,'("error: vertex depth must be positive")')
     STOP 1
  END IF

  ! unit vectors
  nA = (/ C(2)-B(2), &
          B(1)-C(1) /) / NORM2(C-B)
  nB = (/ C(2)-A(2), &
          A(1)-C(1) /) / NORM2(C-A)
  nC = (/ B(2)-A(2), &
          A(1)-B(1) /) / NORM2(B-A)
  
  ! check that unit vectors are pointing outward
  IF (DOT_PRODUCT(nA,A-(B+C)/2) .GT. 0) THEN
      nA=-nA
  END IF
  IF (DOT_PRODUCT(nB,B-(A+C)/2) .GT. 0) THEN
      nB=-nB
  END IF
  IF (DOT_PRODUCT(nC,C-(A+B)/2) .GT. 0) THEN
      nC=-nC
  END IF

  ! Lame parameter
  lambda=2*G*nu/(1-2*nu)

  ! length scale
  l=MINVAL( &
      (/ SQRT((A(1)-B(1))**2+(A(2)-B(2))**2), &
         SQRT((A(1)-C(1))**2+(A(2)-C(2))**2), &
         SQRT((B(1)-C(1))**2+(B(2)-C(2))**2) /))

  step=1e-6

  CALL computeDisplacementTrianglePlaneStrain( &
      x2+l*step,x3,A,B,C,e22,e23,e33,nu,u2p2,u3p2)
  CALL computeDisplacementTrianglePlaneStrain( &
      x2-l*step,x3,A,B,C,e22,e23,e33,nu,u2m2,u3m2)

  CALL computeDisplacementTrianglePlaneStrain( &
      x2,x3+l*step,A,B,C,e22,e23,e33,nu,u2p3,u3p3)
  CALL computeDisplacementTrianglePlaneStrain( &
      x2,x3       ,A,B,C,e22,e23,e33,nu,u2m3,u3m3)

  ! strain components
  U22=(u2p2-u2m2)/(2*l*step)
  U23=(u2p3-u2m3)/(  l*step)
  U32=(u3p2-u3m2)/(2*l*step)
  U33=(u3p3-u3m3)/(  l*step)

  ! remove anelastic strain
  omega=heaviside(((A(1)+B(1))/2-x2)*nC(1)+((A(2)+B(2))/2-x3)*nC(2)) &
       *heaviside(((B(1)+C(1))/2-x2)*nA(1)+((B(2)+C(2))/2-x3)*nA(2)) &
       *heaviside(((C(1)+A(1))/2-x2)*nB(1)+((C(2)+A(2))/2-x3)*nB(2))

  U22=U22-omega*e22
  U23=U23-omega*e23
  U32=U32-omega*e23
  U33=U33-omega*e33

  ! stress components
  s22=lambda*(U22+U33)+2*G*U22
  s23=G*(U23+U32)
  s33=lambda*(U22+U33)+2*G*U33

END SUBROUTINE computeStressTrianglePlaneStrainFiniteDifference



