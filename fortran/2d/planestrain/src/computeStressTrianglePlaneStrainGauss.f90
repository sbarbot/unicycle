!------------------------------------------------------------------------
!! subroutine computeStressTrianglePlaneStrainGauss
!! computes the stress field associated due to a triangle volume element
!! using the Gauss-Legendre quadrature with various orders depending on 
!! the distance from the circumcenter, following
!!
!!   Barbot S., Deformation of a Half-Space from Anelastic Strain Confined
!!   in a Tetrahedral Volume, submitted to Bull. Seism. Soc. Am., 2018.
!!
!! and considering the following geometry:
!!
!!              surface
!!      -------------+-------------- E (x2)
!!                   |
!!                   |     + A
!!                   |    /  . 
!!                   |   /     .  
!!                   |  /        .            
!!                   | /           .      
!!                   |/              + B
!!                   /            .
!!                  /|          /  
!!                 / :       .
!!                /  |    /
!!               /   : .
!!              /   /|
!!             / .   :
!!            +      |
!!          C        :
!!                   |
!!                   D (x3)
!!
!!
!! INPUT:
!! x2, x3             east coordinates and depth of the observation point,
!! A, B, C            coordinates of the 3 vertices A = ( xA, zA),
!! eij                source strain component 22, 23 and 33 in the shear zone,
!! G                  rigidity in the half space.
!! nu                 Poisson's ratio the half space.
!!
!! OUTPUT:
!! s22                horizontal stress,
!! s23                shear stress,
!! s33                vertical stress.
!!
!! \author Sylvain Barbot (04/10/18) - original Fortran form
!-----------------------------------------------------------------------
SUBROUTINE computeStressTrianglePlaneStrainGauss( &
                x2,x3,A,B,C,e22,e23,e33,G,nu,s22,s23,s33)

  IMPLICIT NONE

  REAL*8, INTENT(IN) :: x2,x3
  REAL*8, DIMENSION(2), INTENT(IN) :: A,B,C
  REAL*8, INTENT(IN) :: e22,e23,e33
  REAL*8, INTENT(IN) :: G,nu
  REAL*8, INTENT(OUT) :: s22,s23,s33
    
  REAL*8, PARAMETER :: PI = 3.141592653589793115997963468544185161_8

  ! Gauss-Legendre quadrature positions and weights
  INCLUDE 'gauss-3.inc'
  INCLUDE 'gauss-7.inc'
  INCLUDE 'gauss-15.inc'
  INCLUDE 'gauss-450.inc'

  ! Heaviside function
  REAL*8, EXTERNAL :: heaviside

  ! counter, bound
  INTEGER :: k,n

  ! normal vectors
  REAL*8, DIMENSION(2) :: nA,nB,nC

  ! circumcenter
  REAL*8, DIMENSION(2) :: O

  ! circumcenter radius
  REAL*8 :: r

  ! relative source-receiver distance
  REAL*8 :: rho

  ! isotropic strain
  REAL*8 :: ekk

  ! moment density
  REAL*8 :: m22,m23,m33

  ! Lame parameter
  REAL*8 :: lambda

  ! displacement gradients
  REAL*8 :: U22,U23,U32,U33

  ! position
  REAL*8 :: omega

  ! quadrature variables
  REAL*8 :: xk,wk

  ! check valid parameters
  IF ((-1._8 .GT. nu) .OR. (0.5_8 .LT. nu)) THEN
     WRITE (0,'("error: -1<=nu<=0.5, nu=",ES9.2E2," given.")') nu
     STOP 1
  END IF

  IF (0 .GT. x3) THEN
     WRITE (0,'("error: observation depth (x3) must be positive")')
     STOP 1
  END IF

  IF ((0 .GT. A(2)) .OR. &
      (0 .GT. B(2)) .OR. &
      (0 .GT. C(2))) THEN
     WRITE (0,'("error: vertex depth must be positive")')
     STOP 1
  END IF

  ! unit vectors
  nA = (/ C(2)-B(2), &
          B(1)-C(1) /) / NORM2(C-B)
  nB = (/ C(2)-A(2), &
          A(1)-C(1) /) / NORM2(C-A)
  nC = (/ B(2)-A(2), &
          A(1)-B(1) /) / NORM2(B-A)
  
  ! check that unit vectors are pointing outward
  IF (DOT_PRODUCT(nA,A-(B+C)/2) .GT. 0) THEN
      nA=-nA
  END IF
  IF (DOT_PRODUCT(nB,B-(A+C)/2) .GT. 0) THEN
      nB=-nB
  END IF
  IF (DOT_PRODUCT(nC,C-(A+B)/2) .GT. 0) THEN
      nC=-nC
  END IF

  ! Lame parameter (normalized by G)
  lambda=2*nu/(1.d0-2.d0*nu)

  ! circumcenter of triangle
  O=((B+C)+nA*(nB(2)*(B(1)-A(1))-nB(1)*(B(2)-A(2)))/(nA(2)*nB(1)-nA(1)*nB(2)))/2

  ! circumcircle radius
  r=NORM2(O-A)

  ! isotropic strain
  ekk=e22+e33

  ! moment density (normalized by G)
  m22=lambda*ekk+2*e22
  m23=2*e23;
  m33=lambda*ekk+2*e33

  ! strain components, numerical solution
  U22=0.d0
  U32=0.d0
  U23=0.d0
  U33=0.d0

  ! relative source-receiver distance
  rho=SQRT((x2-O(1))**2+(x3-O(2))**2)/r

  ! choose integration order based on distance from circumcenter
  IF (rho .LE. 1.75d0) THEN
     ! Gauss-Legendre quadrature with 450 points
     DO k=1,450
        xk=p450(k)
        wk=w450(k)

        U22=U22+wk*IU22(xk)
        U32=U32+wk*IU32(xk)
        U23=U23+wk*IU23(xk)
        U33=U33+wk*IU33(xk)
     END DO

  ELSE IF (rho .LE. 10.0d0) THEN
     ! Gauss-Legendre quadrature with 15 points
     DO k=1,15
        xk=p15(k)
        wk=w15(k)

        u22=u22+wk*IU22(xk)
        u23=u23+wk*IU23(xk)
        u32=u32+wk*IU32(xk)
        u33=u33+wk*IU33(xk)
     END DO
  ELSE IF (rho .LE. 20.0d0) THEN
     ! Gauss-Legendre quadrature with 7 points
     DO k=1,7
        xk=p7(k)
        wk=w7(k)

        u22=u22+wk*IU22(xk)
        u23=u23+wk*IU23(xk)
        u32=u32+wk*IU32(xk)
        u33=u33+wk*IU33(xk)
     END DO
  ELSE
     ! Gauss-Legendre quadrature with 3 points
     DO k=1,3
        xk=p3(k)
        wk=w3(k)

        u22=u22+wk*IU22(xk)
        u23=u23+wk*IU23(xk)
        u32=u32+wk*IU32(xk)
        u33=u33+wk*IU33(xk)
     END DO

  END IF

  ! remove anelastic strain
  omega=heaviside(((A(1)+B(1))/2-x2)*nC(1)+((A(2)+B(2))/2-x3)*nC(2)) &
       *heaviside(((B(1)+C(1))/2-x2)*nA(1)+((B(2)+C(2))/2-x3)*nA(2)) &
       *heaviside(((C(1)+A(1))/2-x2)*nB(1)+((C(2)+A(2))/2-x3)*nB(2))

  U22=U22-omega*e22
  U23=U23-omega*e23
  U32=U32-omega*e23
  U33=U33-omega*e33

  ! stress components
  s22=G*(lambda*(U22+U33)+2*U22)
  s23=G*(U23+U32)
  s33=G*(lambda*(U22+U33)+2*U33)

CONTAINS

  ! linear combinations of Green's functions
  REAL*8 FUNCTION IU22(t)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: t

    IU22=(m22*nC(1)+m23*nC(2))*NORM2(B-A)/2*G222(y(t,A(1),B(1)),y(t,A(2),B(2))) &
        +(m23*nC(1)+m33*nC(2))*NORM2(B-A)/2*G322(y(t,A(1),B(1)),y(t,A(2),B(2))) &
        +(m22*nA(1)+m23*nA(2))*NORM2(C-B)/2*G222(y(t,B(1),C(1)),y(t,B(2),C(2))) &
        +(m23*nA(1)+m33*nA(2))*NORM2(C-B)/2*G322(y(t,B(1),C(1)),y(t,B(2),C(2))) &
        +(m22*nB(1)+m23*nB(2))*NORM2(A-C)/2*G222(y(t,C(1),A(1)),y(t,C(2),A(2))) &
        +(m23*nB(1)+m33*nB(2))*NORM2(A-C)/2*G322(y(t,C(1),A(1)),y(t,C(2),A(2)))

  END FUNCTION IU22

  REAL*8 FUNCTION IU23(t)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: t

    IU23=(m22*nC(1)+m23*nC(2))*NORM2(B-A)/2*G223(y(t,A(1),B(1)),y(t,A(2),B(2))) &
        +(m23*nC(1)+m33*nC(2))*NORM2(B-A)/2*G323(y(t,A(1),B(1)),y(t,A(2),B(2))) &
        +(m22*nA(1)+m23*nA(2))*NORM2(C-B)/2*G223(y(t,B(1),C(1)),y(t,B(2),C(2))) &
        +(m23*nA(1)+m33*nA(2))*NORM2(C-B)/2*G323(y(t,B(1),C(1)),y(t,B(2),C(2))) &
        +(m22*nB(1)+m23*nB(2))*NORM2(A-C)/2*G223(y(t,C(1),A(1)),y(t,C(2),A(2))) &
        +(m23*nB(1)+m33*nB(2))*NORM2(A-C)/2*G323(y(t,C(1),A(1)),y(t,C(2),A(2)))

  END FUNCTION IU23

  REAL*8 FUNCTION IU32(t)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: t

    IU32=(m22*nC(1)+m23*nC(2))*NORM2(B-A)/2*G232(y(t,A(1),B(1)),y(t,A(2),B(2))) &
        +(m23*nC(1)+m33*nC(2))*NORM2(B-A)/2*G332(y(t,A(1),B(1)),y(t,A(2),B(2))) &
        +(m22*nA(1)+m23*nA(2))*NORM2(C-B)/2*G232(y(t,B(1),C(1)),y(t,B(2),C(2))) &
        +(m23*nA(1)+m33*nA(2))*NORM2(C-B)/2*G332(y(t,B(1),C(1)),y(t,B(2),C(2))) &
        +(m22*nB(1)+m23*nB(2))*NORM2(A-C)/2*G232(y(t,C(1),A(1)),y(t,C(2),A(2))) &
        +(m23*nB(1)+m33*nB(2))*NORM2(A-C)/2*G332(y(t,C(1),A(1)),y(t,C(2),A(2)))

  END FUNCTION IU32

  REAL*8 FUNCTION IU33(t)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: t

    IU33=(m22*nC(1)+m23*nC(2))*NORM2(B-A)/2*G233(y(t,A(1),B(1)),y(t,A(2),B(2))) &
        +(m23*nC(1)+m33*nC(2))*NORM2(B-A)/2*G333(y(t,A(1),B(1)),y(t,A(2),B(2))) &
        +(m22*nA(1)+m23*nA(2))*NORM2(C-B)/2*G233(y(t,B(1),C(1)),y(t,B(2),C(2))) &
        +(m23*nA(1)+m33*nA(2))*NORM2(C-B)/2*G333(y(t,B(1),C(1)),y(t,B(2),C(2))) &
        +(m22*nB(1)+m23*nB(2))*NORM2(A-C)/2*G233(y(t,C(1),A(1)),y(t,C(2),A(2))) &
        +(m23*nB(1)+m33*nB(2))*NORM2(A-C)/2*G333(y(t,C(1),A(1)),y(t,C(2),A(2)))

  END FUNCTION IU33

  ! interpolate from a to b
  REAL*8 FUNCTION y(t,a,b)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: t,a,b

    y=(a+b)/2+t*(b-a)/2

  END FUNCTION y

  !-----------------------------------------------------------------
  !> function G222
  !! Green's function G222 for an elastic half space in plane strain
  !! corresponding to the displacement gradient U22 due to a line
  !! force in the x2 direction.
  !-----------------------------------------------------------------
  REAL*8 FUNCTION G222(y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y2,y3

    G222=-1.d0/(2*pi*(1.d0-nu))*(x2-y2)*( &
            1.d0/4.d0*(3.d0-4*nu)/r1(y2,y3)**2 &
           +1.d0/4.d0*(8.d0*nu**2-12.d0*nu+5.d0)/r2(y2,y3)**2 &
           -1.d0/2.d0*(x3-y3)**2/r1(y2,y3)**4 &
           -1.d0/2.d0*((3.d0-4.d0*nu)*(x3+y3)**2+2*y3*(x3+y3)-2*y3**2)/r2(y2,y3)**4 &
           +4.d0*y3*x3*(x3+y3)**2/r2(y2,y3)**6 &
         )

  END FUNCTION G222

  !---------------------------------------------------------------
  !> function G223
  !! Green's function G223 for an elastic half space in plane strain
  !! corresponding to the displacement in the x2 direction due to a
  !! line force in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G223(y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y2,y3

    G223= -1d0/(2d0*pi*(1d0-nu))*( &
           1d0/4d0*(3d0-4d0*nu)*(x3-y3)/r1(y2,y3)**2 &
          +1d0/4d0*(8*nu**2-12d0*nu+5d0)*(x3+y3)/r2(y2,y3)**2 &
          +1d0/2d0*(x3-y3)/r1(y2,y3)**2 &
          -1d0/2d0*(x3-y3)**2/r1(y2,y3)**4*(x3-y3) &
          +1d0/2d0*((3d0-4d0*nu)*(x3+y3)+y3)/r2(y2,y3)**2 &
          -1d0/2d0*((3d0-4d0*nu)*(x3+y3)**2+2*y3*(x3+y3)-2*y3**2)/r2(y2,y3)**4*(x3+y3) &
          -y3*(x3+y3)**2/r2(y2,y3)**4 &
          -2d0*y3*x3*(x3+y3)/r2(y2,y3)**4 &
          +4d0*y3*x3*(x3+y3)**2/r2(y2,y3)**6*(x3+y3) &
    )

  END FUNCTION G223

  !---------------------------------------------------------------
  !> function G232
  !! Green's function G232 for an elastic half space in plane strain
  !! corresponding to the displacement in the x2 direction due to a
  !! line force in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G232(y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y2,y3

    G232=1d0/(2d0*pi*(1d0-nu))*( &
       (1d0-2d0*nu)*(1d0-nu)*(x3+y3)/r2(y2,y3)**2 &
       +1d0/4d0*(x3-y3)/r1(y2,y3)**2 &
       -1d0/2d0*(x3-y3)*(x2-y2)/r1(y2,y3)**4 *(x2-y2) &
       +1d0/4d0*(3-4*nu)*(x3-y3)/r2(y2,y3)**2 &
       -1d0/2d0*(3-4*nu)*(x3-y3)*(x2-y2)/r2(y2,y3)**4*(x2-y2) &
       -y3*x3*(x3+y3)/r2(y2,y3)**4 &
       +2d0*y3*x3*(x2-y2)*(x3+y3)/r2(y2,y3)**6*(2*x2-2*y2) &
    )

  END FUNCTION G232

  !---------------------------------------------------------------
  !> function G233
  !! Green's function G233 for an elastic half space in plane strain
  !! corresponding to the displacement in the x2 direction due to a
  !! line force in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G233(y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y2,y3

    G233=1d0/(2*pi*(1d0-nu))*(x2-y2)*( &
       -(1d0-2d0*nu)*(1d0-nu)/r2(y2,y3)**2 &
       +1d0/4d0/r1(y2,y3)**2 &
       -1d0/2d0*(x3-y3)/r1(y2,y3)**4*(x3-y3) &
       +1d0/4d0*(3-4*nu)/r2(y2,y3)**2 &
       -1d0/2d0*(3-4*nu)*(x3-y3)/r2(y2,y3)**4*(x3+y3) &
       -y3*(x3+y3)/r2(y2,y3)**4 &
       -y3*x3/r2(y2,y3)**4 &
       +4d0*y3*x3*(x3+y3)/r2(y2,y3)**6*(x3+y3) &
    )

  END FUNCTION G233

  !---------------------------------------------------------------
  !> function G322
  !! Green's function G322 for an elastic half space in plane strain
  !! corresponding to the displacement in the x2 direction due to a
  !! line force in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G322(y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y2,y3

    G322=1d0/(2*pi*(1d0-nu))*( &
       -(1d0-2d0*nu)*(1d0-nu)*(x3+y3)/r2(y2,y3)**2 &
       +1d0/4d0*(x3-y3)/r1(y2,y3)**2 &
       -1d0/2d0*(x3-y3)*(x2-y2)/r1(y2,y3)**4*(x2-y2) &
       +1d0/4d0*(3d0-4d0*nu)*(x3-y3)/r2(y2,y3)**2 &
       -1d0/2d0*(3d0-4d0*nu)*(x3-y3)*(x2-y2)/r2(y2,y3)**4*(x2-y2) &
       +y3*x3*(x3+y3)/r2(y2,y3)**4 &
       -4d0*y3*x3*(x2-y2)*(x3+y3)/r2(y2,y3)**6*(x2-y2) &
    )

  END FUNCTION G322

  !---------------------------------------------------------------
  !> function G323
  !! Green's function G323 for an elastic half space in plane strain
  !! corresponding to the displacement in the x2 direction due to a
  !! line force in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G323(y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y2,y3

    G323=1d0/(2*pi*(1d0-nu))*(x2-y2)*( &
       (1d0-2*nu)*(1d0-nu)/r2(y2,y3)**2 &
       +1d0/4d0/r1(y2,y3)**2 &
       -1d0/2d0*(x3-y3)/r1(y2,y3)**4*(x3-y3) &
       +1d0/4d0*(3d0-4d0*nu)/r2(y2,y3)**2 &
       -1d0/2d0*(3d0-4d0*nu)*(x3-y3)/r2(y2,y3)**4*(x3+y3) &
       +y3*(x3+y3)/r2(y2,y3)**4 &
       +y3*x3/r2(y2,y3)**4 &
       -4d0*y3*x3*(x3+y3)/r2(y2,y3)**6*(x3+y3) &
    )

  END FUNCTION G323

  !---------------------------------------------------------------
  !> function G332
  !! Green's function G332 for an elastic half space in plane strain
  !! corresponding to the displacement in the x2 direction due to a
  !! line force in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G332(y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y2,y3

    G332=1d0/(2d0*pi*(1d0-nu))*(x2-y2)*( &
       -1d0/4d0*(3d0-4d0*nu)/r1(y2,y3)**2 &
       -1d0/4d0*(8d0*nu**2-12d0*nu+5d0)/r2(y2,y3)**2 &
       -1d0/2d0/r1(y2,y3)**2 &
       +1d0/2d0*(x2-y2)**2/r1(y2,y3)**4 &
       -1d0/2d0*(3d0-4d0*nu)/r2(y2,y3)**2 &
       -1d0/2d0*(0*2*y2*x3-(3d0-4d0*nu)*(x2-y2)**2)/r2(y2,y3)**4 &  ! writing 0* reduces the residuals by 8 orders of magnitude
       -3d0/2d0*2*y3*x3/r2(y2,y3)**4 &                              ! writing 3* reduces the residuals by 1 order of magnitude
       +4d0*y3*x3*(x2-y2)**2/r2(y2,y3)**6 &
    )

  END FUNCTION G332

  !---------------------------------------------------------------
  !> function G333
  !! Green's function G333 for an elastic half space in plane strain
  !! corresponding to the displacement in the x2 direction due to a
  !! line force in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G333(y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y2,y3

    G333= 1d0/(2*pi*(1-nu))*( &
    -1d0/4d0*(3-4*nu)*(x3-y3)/r1(y2,y3)**2 &
    -1d0/4d0*(8*nu**2-12*nu+5)*(x3+y3)/r2(y2,y3)**2 &
    +1d0/2d0*(x2-y2)**2/r1(y2,y3)**4*(x3-y3) &
    +1d0/2d0*y2/r2(y2,y3)**2 &
    -1d0/2d0*(2*y2*x3-(3-4*nu)*(x2-y2)**2)/r2(y2,y3)**4*(x3+y3) &
    -y3*(x2-y2)**2/r2(y2,y3)**4 &
    +4d0*y3*x3*(x2-y2)**2/r2(y2,y3)**6*(x3+y3) &
    )

  END FUNCTION G333

  ! Radii
  REAL*8 FUNCTION r1(y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y2,y3

    r1=SQRT((x2-y2)**2+(x3-y3)**2)

  END FUNCTION r1

  REAL*8 FUNCTION r2(y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y2,y3

    r2=SQRT((x2-y2)**2+(x3+y3)**2)

  END FUNCTION r2

END SUBROUTINE computeStressTrianglePlaneStrainGauss



