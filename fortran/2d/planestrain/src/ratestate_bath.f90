!-----------------------------------------------------------------------
!> program RateState-Bath simulates evolution of fault slip in condition of 
!! plane strain under the radiation damping approximation in non-isothermal
!! conditions, approximating the thermal diffusion with a bath temperature.
!!
!! \mainpage
!! 
!! The time evolution is evaluated numerically using the 4th/5th order
!! Runge-Kutta method with adaptive time steps. The state vector is 
!! as follows:
!!
!!    / P1 1       \   +-----------------------+
!!    | .          |   |                       |
!!    | P1 dPatch  |   |                       |
!!    | .          |   |    nPatch * dPatch    |
!!    | .          |   |                       |
!!    | Pn 1       |   |                       |
!!    | .          |   |                       |
!!    \ Pn dPatch  /   +-----------------------+
!!
!! where nPatch is the number of patches and dPatch is the degrees of 
!! freedom per patch. 
!!
!! For every patch, we have the following items in the state vector
!!
!!   /   td    \  1
!!   |   sd    |  .
!!   !  theta* |  .
!!   !   v*    |  .
!!   \   T     /  dPatch
!!
!! where td is the local traction in the dip direction, sd is the slip 
!! in the dip direction, v* is the logarithm of the norm of the 
!! velocity vector (v*=log10(V)), and theta* is the logarithm of the state 
!! variable (theta*=log10(theta)) in the rate and state friction framework,
!! and T is the evolving temperature on the fault patch.
!!
!! References:<br>
!!
!!   Barbot S., "Modulation of fault strength during the seismic cycle
!!   by grain-size evolution around contact junctions", Tectonophysics,
!!   765, 129-145, doi:j.tecto.2019.05.004, 2019
!!
!! \author Sylvain Barbot (2019-2025).
!----------------------------------------------------------------------
PROGRAM ratestate_bath

#define BATH 1
#include "macros.h90"

#ifdef NETCDF
  USE exportnetcdf
#endif
  USE planestrain
  USE greens_ps_bath
  USE rk
  USE mpi_f08
  USE types_ps_bath

  IMPLICIT NONE

  REAL*8, PARAMETER :: DEG2RAD = 0.01745329251994329547437168059786927_8

  ! MPI rank and size
  INTEGER :: rank,csize

  ! error flag
  INTEGER :: ierr

#ifdef NETCDF
  ! netcdf file and variable
  INTEGER :: ncid,y_varid,z_varid,ncCount
#endif

  CHARACTER(256) :: filename

  ! maximum velocity
  REAL*8 :: vMax,vMaxAll

  ! maximum temperature
  REAL*8 :: tMax,tMaxAll

  ! moment rate
  REAL*8 :: momentRate,momentRateAll

  ! scaling factor
  REAL*8, PARAMETER :: lg10=LOG(1.d1)

  ! state vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: y
  ! rate of change of state vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: dydt,yscal
  ! temporary variables
  REAL*8, DIMENSION(:), ALLOCATABLE :: ytmp,ytmp1,ytmp2,ytmp3

  ! slip velocity vector and strain rate tensor array (temporary space)
  REAL*8, DIMENSION(:), ALLOCATABLE :: v

  ! slip velocity vector and strain rate tensor array (temporary space)
  REAL*8, DIMENSION(:), ALLOCATABLE :: vAll

  ! traction vector and stress tensor array (temporary space)
  REAL*8, DIMENSION(:), ALLOCATABLE :: t

  ! displacement and kinematics vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: d,u,dAll

  ! Green's function
  REAL*8, DIMENSION(:,:), ALLOCATABLE :: G,O,Of,Ol

  ! time
  REAL*8 :: time,t0
  ! time step
  REAL*8 :: dt_try,dt_next,dt_done
  ! steps
  INTEGER :: i,j

  ! type of friction law (default)
  INTEGER :: frictionLawType=1

  ! event catalogue
  TYPE(CATALOGUE_STRUCT) :: catalogue

  ! maximum number of time steps (default)
  INTEGER :: maximumIterations=1000000

  ! layout for parallelism
  TYPE(LAYOUT_STRUCT) :: layout

  ! model parameters
  TYPE(SIMULATION_STRUCT) :: in

  ! initialization
  CALL MPI_INIT(ierr)
  CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
  CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

  ! start time
  time=0.d0

  ! initial tentative time step
  dt_next=1d-3

  ! retrieve input parameters from command line
  CALL init(in)
  CALL FLUSH(STDOUT)

  IF (in%isDryRun .AND. 0 .EQ. rank) THEN
     PRINT '("dry run: abort calculation")'
  END IF
  IF (in%isDryRun .OR. in%isVersion .OR. in%isHelp) THEN
     CALL MPI_FINALIZE(ierr)
     STOP
  END IF

  ! calculate basis vectors
  CALL initGeometry(in)

  ! describe data layout for parallelism
  CALL initParallelism(in,layout)

#ifdef NETCDF
  IF (in%isImportGreens) THEN
     CALL initG(in,layout,G)
     IF (0 .EQ. rank) THEN
        PRINT '("# import Green''s function.")'
     END IF
     CALL importGreensNetcdf(G)
  ELSE
     ! calculate the stress interaction matrix
     IF (0 .EQ. rank) THEN
        PRINT '("# computing Green''s functions.")'
     END IF
     CALL buildG(in,layout,G)
     IF (in%isExportGreens) THEN
        IF (0 .EQ. rank) THEN
           PRINT '("# exporting Green''s functions to ",a)',TRIM(in%greensFunctionDirectory)
        END IF
        CALL exportGreensNetcdf(G)
     END IF
  END IF
#else
  ! calculate the stress interaction matrix
  IF (0 .EQ. rank) THEN
     PRINT '("# computing Green''s functions.")'
  END IF
  CALL buildG(in,layout,G)
#endif

  CALL buildO(in,layout,O,Of,Ol)
  DEALLOCATE(Of,Ol)

  IF (0 .EQ. rank) THEN
     PRINT 2000
  END IF

  ! velocity vector and strain rate tensor array (t=G*vAll)
  ALLOCATE(u(layout%listVelocityN(1+rank)), &
           v(layout%listVelocityN(1+rank)), &
           vAll(SUM(layout%listVelocityN)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the velocity and strain rate vector"

  ! traction vector and stress tensor array (t=G*v)
  ALLOCATE(t(layout%listForceN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the traction and stress vector"

  ! displacement vector (d=O*v)
  ALLOCATE(d   (in%nObservationPoint*DISPLACEMENT_VECTOR_DGF), &
           dAll(in%nObservationPoint*DISPLACEMENT_VECTOR_DGF),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the displacement vector"

  ALLOCATE(y(layout%listStateN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the state vector"

  ALLOCATE(dydt(layout%listStateN(1+rank)), &
           yscal(layout%listStateN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the state vectors"

  ALLOCATE(ytmp (layout%listStateN(1+rank)), &
           ytmp1(layout%listStateN(1+rank)), &
           ytmp2(layout%listStateN(1+rank)), &
           ytmp3(layout%listStateN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the RKQS work space"

  ! allocate buffer from rk module
#ifdef ODE45
  ALLOCATE(buffer(layout%listStateN(1+rank),5),SOURCE=0.0d0,STAT=ierr)
#else
  ALLOCATE(buffer(layout%listStateN(1+rank),3),SOURCE=0.0d0,STAT=ierr)
#endif
  IF (ierr>0) STOP "could not allocate the buffer work space"

  IF (0 .EQ. rank) THEN
     OPEN (UNIT=FPTIME,FILE=in%timeFilename,IOSTAT=ierr,FORM="FORMATTED")
     IF (ierr>0) THEN
        WRITE_DEBUG_INFO(102)
        WRITE (STDERR,'("error: unable to access ",a)') TRIM(in%timefilename)
        STOP 1
     END IF
  END IF

  ! initialize the y vector
  IF (0 .EQ. rank) THEN
     PRINT '("# initialize state vector.")'
  END IF
  CALL initStateVector(layout%listStateN(1+rank),y,in)
  IF (0 .EQ. rank) THEN
     PRINT 2000
  END IF

#ifdef NETCDF
  ! initialize netcdf output
  IF (in%isExportNetcdf) THEN
     CALL initnc()
  END IF
#endif

  ! gather maximum velocity
  CALL MPI_REDUCE(vMax,vMaxAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

  ! gather maximum temperature
  CALL MPI_REDUCE(tMax,tMaxAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

  ! gather moment rate
  CALL MPI_REDUCE(momentRate,momentRateAll,1,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

  ! initialize output
  IF (0 .EQ. rank) THEN
     WRITE(STDOUT,'("# export to ",a)') TRIM(in%wdir)
     PRINT 2000
     WRITE(STDOUT,'("#       n               time                 dt       vMax       tMax")')
     WRITE(STDOUT,'(I9.9,ES19.12E2,ES19.12E2,ES11.4E2,ES11.4E2)') 0,time,dt_next,vMaxAll,tMaxAll
     WRITE(FPTIME,'("#               time                 dt               vMax         Moment-rate       tMax")')
  END IF

  ! initialize observation patch
  DO j=1,in%nObservationState
     IF ((in%observationState(1,j) .GE. layout%listOffset(rank+1)) .AND. &
         (in%observationState(1,j) .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN

        in%observationState(3,j)=100+j
        WRITE (filename,'(a,"/patch-",I8.8,"-",I8.8,".dat")') TRIM(in%wdir),j,in%observationState(1,j)
        OPEN (UNIT=in%observationState(3,j), &
              FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
        IF (ierr>0) THEN
           WRITE_DEBUG_INFO(102)
           WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
           STOP 1
        END IF
     END IF
  END DO

  ! initialize observation points
  IF (0 .EQ. rank) THEN
     DO j=1,in%nObservationPoint
        in%observationPoint(j)%file=1000+j
        WRITE (filename,'(a,"/opts-",a,".dat")') TRIM(in%wdir),TRIM(in%observationPoint(j)%name)
        OPEN (UNIT=in%observationPoint(j)%file, &
              FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
        WRITE (in%observationPoint(j)%file,'("#                time                 u2                 u3")')
        IF (ierr>0) THEN
           WRITE_DEBUG_INFO(102)
           WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
           STOP 1
        END IF
     END DO
  END IF

  IF (in%isEventCatalogue) THEN
     ALLOCATE(catalogue%y(layout%listStateN(1+rank)),STAT=ierr)
     IF (0/=ierr) STOP "could not allocate catalogue"

     catalogue%isEventOngoing=.FALSE.
     catalogue%index=0

     IF (0 .EQ. rank) THEN
        catalogue%id=FPCATALOGUE
        OPEN (UNIT=FPCATALOGUE,FILE=TRIM(in%wdir)//"/catalogue.dat", &
              IOSTAT=ierr,FORM="FORMATTED")
        WRITE(catalogue%id,'("#   n    ", &
                             "iStart      iEnd             ", &
                             "tStart               tEnd        ", &
                             "maxVel       aveSlip       maxSlip    stressDrop")')
        CALL FLUSH(catalogue%id)
     END IF

  END IF

  in%events%current=0
  in%events%next=1

  ! main loop
#ifdef ODE23
  CALL odefun(layout%listStateN(1+rank),time,y,dydt)
#endif
  DO i=1,maximumIterations

     ! perturbations in pressure and shear stress
     IF (in%events%next .LE. in%events%n) THEN
        IF (time .GE. in%events%event(in%events%next)%time) THEN
           IF (0 .EQ. rank) THEN
              WRITE(STDOUT,'(I9.9,ES19.12E2,ES19.12E2,X,"**********"," # event",I3,"/",I3)') &
                    i,time,dt_done,in%events%next,in%events%n
           END IF

           ! increment event counters
           in%events%current=in%events%next
           in%events%next   =in%events%next+1

           ! re-initialize tentative time step
           dt_next=1d-3
        END IF
     END IF

#ifdef ODE45
     CALL odefun(layout%listStateN(1+rank),time,y,dydt)
#endif

     CALL export()
     CALL exportPoints()

#ifdef NETCDF
     IF (0 .EQ. rank) THEN
        IF (in%isExportNetcdf) THEN
           IF (0 .EQ. MOD(i,20)) THEN
              CALL exportnc(in%nPatch)
           END IF
        END IF
     END IF
#endif

     dt_try=dt_next

     IF (in%events%next .LE. in%events%n) THEN
        dt_try=MIN(dt_try,in%events%event(in%events%next)%time-time)
     END IF

     yscal(:)=abs(y(:))+abs(dt_try*dydt(:))+TINY

     t0=time
#ifdef ODE45
     CALL rungeKutta(layout%listStateN(1+rank),t0,y,dydt, &
               yscal,ytmp1,ytmp2,ytmp3,dt_try,dt_done,dt_next,odefun,timeStep45)
#else
     CALL rungeKutta(layout%listStateN(1+rank),t0,y,dydt, &
               yscal,ytmp1,ytmp2,ytmp3,dt_try,dt_done,dt_next,odefun,timeStep23)
#endif

     time=time+dt_done

     IF (in%isEventCatalogue) THEN
        CALL buildCatalogue(catalogue,i,time)
     END IF

     ! end calculation
     IF (in%interval .LE. time) THEN
        EXIT
     END IF
   
  END DO

  IF (0 .EQ. rank) THEN
     PRINT '(I9.9," time steps.")', i
  END IF

  IF (0 .EQ. rank) THEN
     CLOSE(FPTIME)
  END IF

  ! close observation patch files
  DO j=1,in%nObservationState
     IF ((in%observationState(1,j) .GE. layout%listOffset(rank+1)) .AND. &
         (in%observationState(1,j) .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN
        CLOSE(in%observationState(3,j))
     END IF
  END DO

  ! close the observation points
  IF (0 .EQ. rank) THEN
     DO j=1,in%nObservationPoint
        CLOSE(in%observationPoint(j)%file)
     END DO
  END IF

#ifdef NETCDF
  ! close the netcdf file
  IF (0 .EQ. rank) THEN
     IF (in%isExportNetcdf) THEN
        CALL closeNetcdfUnlimited(ncid,y_varid,z_varid,ncCount)
     END IF
  END IF
#endif

  IF (in%isEventCatalogue) THEN
     IF (0 .EQ. rank) THEN
        CLOSE(catalogue%id)
     END IF
  END IF

  DEALLOCATE(y,dydt,yscal)
  DEALLOCATE(ytmp,ytmp1,ytmp2,ytmp3)
  DEALLOCATE(buffer)
  DEALLOCATE(G,v,vAll,t)
  DEALLOCATE(layout%listForceN)
  DEALLOCATE(layout%listVelocityN,layout%listVelocityOffset)
  DEALLOCATE(layout%listStateN,layout%listStateOffset)
  DEALLOCATE(layout%elementStateIndex)
  DEALLOCATE(layout%listElements,layout%listOffset)
  DEALLOCATE(O,d,u,dAll)

  CALL MPI_FINALIZE(ierr)

2000 FORMAT ("# ----------------------------------------------------------------------------")
     
CONTAINS

  !-----------------------------------------------------------------------
  !> subroutine buildCatalogue
  ! export event catalogue based on a velocity threshold
  !----------------------------------------------------------------------
  SUBROUTINE buildCatalogue(catalogue,i,t)

    IMPLICIT NONE

    TYPE(CATALOGUE_STRUCT), INTENT(INOUT) :: catalogue
    INTEGER, INTENT(IN) :: i
    REAL*8, INTENT(IN) :: t

    REAL*8 :: maximumSlip,stressDrop,averageSlip
    REAL*8 :: all
    REAL*8 :: vMaxAll

    INTEGER :: l

    ! get the maximum velocity
    CALL MPI_ALLREDUCE(vMax,vMaxAll,1,MPI_REAL8,MPI_MAX,MPI_COMM_WORLD,ierr)

    IF (catalogue%isEventOngoing) THEN
       IF (vMaxAll .LT. catalogue%vEnd) THEN

          ! close current event
          catalogue%iEnd=i
          catalogue%tEnd=t
          catalogue%y=y-catalogue%y
          catalogue%isEventOngoing=.FALSE.

          ! maximum velocity
          catalogue%maximumVelocity=MAX(catalogue%maximumVelocity,vMaxAll)

          ! maximum slip
          maximumSlip=MAXVAL(catalogue%y(1+STATE_VECTOR_SLIP_DIP:SIZE(y):STATE_VECTOR_DGF_PATCH))
          CALL MPI_ALLREDUCE(maximumSlip,all,1,MPI_REAL8,MPI_MAX,MPI_COMM_WORLD,ierr)
          maximumSlip=all

          ! stress drop
          stressDrop=SUM(ABS(catalogue%y(1+STATE_VECTOR_SLIP_DIP:SIZE(y):STATE_VECTOR_DGF_PATCH)))
          CALL MPI_ALLREDUCE(stressDrop,all,1,MPI_REAL8,MPI_SUM,MPI_COMM_WORLD,ierr)
          stressDrop=-SUM(ABS(catalogue%y(1+STATE_VECTOR_SLIP_DIP:SIZE(y):STATE_VECTOR_DGF_PATCH)) &
                         *catalogue%y(1+STATE_VECTOR_TRACTION_DIP:SIZE(y):STATE_VECTOR_DGF_PATCH)) &
                     /all

          ! average slip (variance/mean slip)
          averageSlip=SUM(ABS(catalogue%y(1+STATE_VECTOR_SLIP_DIP:SIZE(y):STATE_VECTOR_DGF_PATCH)))
          CALL MPI_ALLREDUCE(averageSlip,all,1,MPI_REAL8,MPI_SUM,MPI_COMM_WORLD,ierr)
          averageSlip=SUM(catalogue%y(1+STATE_VECTOR_SLIP_DIP:SIZE(y):STATE_VECTOR_DGF_PATCH)**2)/all

          IF (0 .EQ. rank) THEN
             ! write information to file
             WRITE(catalogue%id,'(I5.5,X,I9.9,X,I9.9,2ES19.12E2,4ES14.6E2)') &
                   catalogue%index, &
                   catalogue%iStart,catalogue%iEnd, &
                   catalogue%tStart,catalogue%tEnd, &
                   catalogue%maximumVelocity, &
                   averageSlip, &
                   maximumSlip, &
                   stressDrop
             CALL FLUSH(catalogue%id)
          END IF
       ELSE
          ! continue current event
          catalogue%maximumVelocity=MAX(catalogue%maximumVelocity,vMaxAll)
       END IF
    ELSE
       IF (vMaxAll .GE. catalogue%vStart) THEN
          ! start new event
          catalogue%index=catalogue%index+1
          catalogue%iStart=i
          catalogue%tStart=t
          catalogue%y=y
          catalogue%isEventOngoing=.TRUE.
       END IF
    END IF

  END SUBROUTINE buildCatalogue

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportGreensNetcdf
  ! export the Greens function by rank
  !----------------------------------------------------------------------
  SUBROUTINE exportGreensNetcdf(M)
    REAL*8, DIMENSION(:,:), INTENT(IN) :: M

    REAL*8, DIMENSION(:), ALLOCATABLE :: x,y

    INTEGER :: i,ierr
    CHARACTER(LEN=256) :: filename

    ALLOCATE(x(SIZE(M,1)),y(SIZE(M,2)),STAT=ierr)
    IF (ierr/=0) STOP "could not allocate netcdf coordinate for Greens function"

    ! loop over all patch elements
    DO i=1,SIZE(M,1)
       x(i)=REAL(i,8)
    END DO
    DO i=1,SIZE(M,2)
       y(i)=REAL(i,8)
    END DO

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/greens-",I4.4,".grd")') TRIM(in%greensFunctionDirectory),rank
    CALL writeNetcdf(filename,SIZE(M,1),x,SIZE(M,2),y,M,1)

    DEALLOCATE(x,y)

  END SUBROUTINE exportGreensNetcdf
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine importGreensNetcdf
  ! import the Greens function by rank
  !----------------------------------------------------------------------
  SUBROUTINE importGreensNetcdf(M)
    REAL*8, DIMENSION(:,:), INTENT(OUT) :: M

    INTEGER :: ierr
    CHARACTER(LEN=256) :: filename

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/greens-",I4.4,".grd")') TRIM(in%greensFunctionDirectory),rank
    CALL readNetcdf(filename,SIZE(M,1),SIZE(M,2),M)

  END SUBROUTINE importGreensNetcdf
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine initnc
  ! initializes the coordinates of netcdf files
  !----------------------------------------------------------------------
  SUBROUTINE initnc()

    REAL*8, DIMENSION(:), ALLOCATABLE :: x

    INTEGER :: i,ierr
    CHARACTER(LEN=256) :: filename

    IF (0 .EQ. rank) THEN

       ! initialize the number of exports
       ncCount = 0

       ALLOCATE(x(in%nPatch),STAT=ierr)
       IF (ierr/=0) STOP "could not allocate netcdf coordinate"

       ! loop over all patch elements
       DO i=1,in%nPatch
          x(i)=REAL(i,8)
       END DO

       ! netcdf file is compatible with GMT
       WRITE (filename,'(a,"/log10v.grd")') TRIM(in%wdir)
       CALL openNetcdfUnlimited(filename,in%nPatch,x,ncid,y_varid,z_varid)

       DEALLOCATE(x)

    END IF

  END SUBROUTINE initnc
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportnc
  ! export time series of log10(v)
  !----------------------------------------------------------------------
  SUBROUTINE exportnc(n)
    INTEGER, INTENT(IN) :: n

    REAL*4, DIMENSION(n) :: z

    INTEGER :: j,l,ierr
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    ! update the export count
    ncCount=ncCount+1

    ! loop over all patch elements
    DO j=1,in%nPatch
       ! dip slip
       z(j)=REAL(LOG10(in%patch%s(j)%Vl+vAll(DGF_PATCH*(j-1)+1)),4)
    END DO

    CALL writeNetcdfUnlimited(ncid,y_varid,z_varid,ncCount,n,z)

    ! flush every so often
    IF (0 .EQ. MOD(ncCount,50)) THEN
       CALL flushNetcdfUnlimited(ncid,y_varid,ncCount)
    END IF

  END SUBROUTINE exportnc
#endif

  !-----------------------------------------------------------------------
  !> subroutine exportPoints
  ! export observation points
  !----------------------------------------------------------------------
  SUBROUTINE exportPoints()

    IMPLICIT NONE

    INTEGER :: j,k,l,ierr
    INTEGER :: elementIndex
    CHARACTER(1024) :: formatString
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    !-----------------------------------------------------------------
    ! step 1/4 - gather the kinematics from the state vector
    !-----------------------------------------------------------------

    ! element index in d vector
    k=1
    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO j=1,SIZE(layout%elementIndex)
       elementIndex=layout%elementIndex(j)
       patch=in%patch%s(elementIndex)

       ! dip slip
       u(k)=y(l+STATE_VECTOR_SLIP_DIP)

       l=l+in%dPatch

       k=k+layout%elementVelocityDGF(j)
    END DO

    !-----------------------------------------------------------------
    ! step 2/4 - calculate the rate of traction and rate of stress
    !-----------------------------------------------------------------

#ifdef __BLAS__
    ! use the BLAS library to compute the matrix vector product
    CALL DGEMV("T",SIZE(O,1),SIZE(O,2), &
                1._8,O,SIZE(O,1),u,1,0.d0,d,1)
#else
    ! slower, Fortran intrinsic
    d=MATMUL(TRANSPOSE(O),u)
#endif

    !-----------------------------------------------------------------
    ! step 3/4 - master thread adds the contribution of all elements
    !-----------------------------------------------------------------

    CALL MPI_REDUCE(d,dAll,in%nObservationPoint*DISPLACEMENT_VECTOR_DGF, &
                    MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

    !-----------------------------------------------------------------
    ! step 4/4 - master thread writes to disk
    !-----------------------------------------------------------------

    IF (0 .EQ. rank) THEN
       formatString="(ES21.14E2"
       DO j=1,DISPLACEMENT_VECTOR_DGF
          formatString=TRIM(formatString)//",X,ES19.12E2"
       END DO
       formatString=TRIM(formatString)//")"

       ! element index in d vector
       k=1
       DO j=1,in%nObservationPoint
          IF (0 .EQ. MOD(i-1,in%observationPoint(j)%rate)) THEN
             WRITE (in%observationPoint(j)%file,TRIM(formatString)) time,dAll(k:k+DISPLACEMENT_VECTOR_DGF-1)
          END IF
          k=k+DISPLACEMENT_VECTOR_DGF
       END DO
    END IF

  END SUBROUTINE exportPoints

  !-----------------------------------------------------------------------
  !> subroutine export
  ! write the state variables and their rate of change for fault patches.
  !----------------------------------------------------------------------
  SUBROUTINE export()

    IMPLICIT NONE

    ! degrees of freedom
    INTEGER :: dgf

    ! counters
    INTEGER :: j,k

    ! index in state vector
    INTEGER :: index

    ! format string
    CHARACTER(1024) :: formatString

    ! gather maximum velocity
    CALL MPI_REDUCE(vMax,vMaxAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

    ! gather maximum temperature
    CALL MPI_REDUCE(tMax,tMaxAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

    ! gather moment-rate
    CALL MPI_REDUCE(momentRate,momentRateAll,1,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

    ! export observation state
    DO j=1,in%nObservationState
       IF ((in%observationState(1,j) .GE. layout%listOffset(rank+1)) .AND. &
           (in%observationState(1,j) .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN

          ! check observation state sampling rate
          IF (0 .EQ. MOD(i-1,in%observationState(2,j))) THEN
             dgf=in%dPatch

             formatString="(ES21.14E2"
             DO k=1,dgf
                formatString=TRIM(formatString)//",X,ES20.12E3,X,ES20.12E3"
             END DO
             formatString=TRIM(formatString)//")"

             index=layout%elementStateIndex(in%observationState(1,j)-layout%listOffset(rank+1)+1)-dgf
             WRITE (in%observationState(3,j),TRIM(formatString)) time, &
                       y(index+1:index+dgf), &
                    dydt(index+1:index+dgf)
          END IF
       END IF
    END DO

    IF (0 .EQ. rank) THEN
       WRITE(FPTIME,'(ES20.14E2,ES19.12E2,ES19.12E2,ES20.12E2,ES11.4E2)') time,dt_done,vMaxAll,momentRateAll,tMaxAll
       IF (0 .EQ. MOD(i,50)) THEN
          WRITE(STDOUT,'(I9.9,ES19.12E2,ES19.12E2,ES11.4E2,ES11.4E2)') i,time,dt_done,vMaxAll,tMaxAll
          CALL FLUSH(STDOUT)
       END IF
    END IF

  END SUBROUTINE export

  !-----------------------------------------------------------------------
  !> subroutine initStateVector
  ! initialize the state vector
  !
  ! INPUT:
  ! @param n - number of state elements own by current thread
  ! @param y - the state vector (segment owned by currect thread)
  !
  !----------------------------------------------------------------------
  SUBROUTINE initStateVector(n,y,in)
    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: n
    REAL*8, INTENT(OUT)    :: y(n)
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in

    INTEGER :: i,l,ierr
    INTEGER :: elementIndex
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    ! initial velocity (m/s)
    REAL*8 :: Vinit

    ! zero out state vector
    y=0d0

    ! maximum velocity
    vMax=0._8

    ! maximum temperature
    tMax=0._8

    ! moment-rate
    momentRate=0._8

    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementIndex=layout%elementIndex(i)
       patch=in%patch%s(elementIndex)

       ! dip slip
       y(l+STATE_VECTOR_SLIP_DIP) = 0.d0

       ! traction in dip direction
       IF (0 .GT. patch%tau0) THEN

          ! set initial velocity to local loading rate
          Vinit = 0.98d0*patch%Vl

          SELECT CASE(frictionLawType)
          CASE(1)
             ! multiplicative form of rate-state friction (Barbot, 2018)
             y(l+STATE_VECTOR_TRACTION_DIP) = patch%mu0*patch%sig*exp((patch%a-patch%b)/patch%mu0*log(patch%Vl/patch%Vo))
          CASE(2:3)
             ! additive form of rate-state friction (Chester, 1994; Barbot, 2019)
             y(l+STATE_VECTOR_TRACTION_DIP) = patch%sig*(patch%mu0+(patch%a-patch%b)*log(patch%Vl/patch%Vo))
          CASE DEFAULT
             WRITE (0,'("unhandled option ", a, " (this is a bug)")') frictionLawType
             WRITE_DEBUG_INFO(100)
             STOP 3
          END SELECT
       ELSE
          ! set initial shear stress from user input
          y(l+STATE_VECTOR_TRACTION_DIP) = patch%tau0

          ! set initial velocity corresponding to shear stress
          ! with state variable at steady state Vl
          SELECT CASE(frictionLawType)
          CASE(1)
             ! multiplicative form of rate-state friction (Barbot, 2019)
             Vinit = patch%Vo*exp(patch%b/patch%a*log(patch%Vl/patch%Vo)) &
                             *exp(patch%mu0/patch%a*log(patch%tau0/(patch%mu0*patch%sig)))
          CASE(2:3)
             ! additive form of rate-state friction (Chester, 1994; Barbot, 2019)
             Vinit = patch%Vo*exp(patch%b/patch%a*log(patch%Vl/patch%Vo)) &
                             *exp((patch%tau0-patch%mu0*patch%sig)/(patch%a*patch%sig))
          CASE DEFAULT
             WRITE (0,'("unhandled option ", a, " (this is a bug)")') frictionLawType
             WRITE_DEBUG_INFO(100)
             STOP 3
          END SELECT

       END IF

       ! traction in normal direction
       y(l+STATE_VECTOR_TRACTION_NORMAL) = 0.d0

       ! state variable log10(theta)
       y(l+STATE_VECTOR_STATE_1) = log(patch%L/patch%Vl)/lg10

       ! maximum velocity
       vMax=MAX(Vinit,vMax)

       ! moment-rate
       momentRate=momentRate+(patch%Vl*(0.98d0-1.d0))*in%mu*in%patch%width(elementIndex)

       ! slip velocity log10(V)
       y(l+STATE_VECTOR_VELOCITY) = log(Vinit)/lg10

       ! initial temperature
       y(l+STATE_VECTOR_TEMPERATURE) = patch%To

       ! maximum velocity
       tMax=MAX(patch%To,tMax)

       l=l+in%dPatch
    END DO

  END SUBROUTINE initStateVector

  !-----------------------------------------------------------------------
  !> subroutine odefun
  ! evalutes the derivative of the state vector
  !
  ! @param n - number of state elements own by current thread
  ! @param m - degrees of freedom
  !
  ! DESCRIPTION:
  !   1- extract slip velocity and strain rate from state vector
  !   2- calculate the rate of traction and rate of stress
  !   3- calculate the rate of remaining state variables
  !----------------------------------------------------------------------
  SUBROUTINE odefun(n,time,y,dydt)
    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: n
    REAL*8, INTENT(IN)    :: time
    REAL*8, INTENT(IN)    :: y(n)
    REAL*8, INTENT(INOUT) :: dydt(n)

    INTEGER :: i,j,k,l,ierr
    INTEGER :: elementIndex
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    ! boxcar function
    REAL*8, EXTERNAL :: boxcar

    ! event
    TYPE(EVENT_STRUCT) :: event

    ! amplitude of event perturbation
    REAL*8 :: pert

    ! friction
    REAL*8 :: friction

    ! take-off angle
    REAL*8 :: beta

    ! traction components in the dip direction
    REAL*8 :: td

    ! scalar rate of shear traction
    REAL*8 :: dtau

    ! slip velocity in the dip direction
    REAL*8 :: velocity

    ! modifier for the arcsinh form of rate-state friction
    REAL*8 :: reg

    ! normal stress
    REAL*8 :: sigma

    ! zero out rate of state vector
    dydt=0._8

    ! maximum velocity
    vMax=0._8

    ! maximum temperature
    tMax=0._8

    ! initialize moment-rate
    momentRate=0._8

    !--------------------------------------------------------------------
    ! step 1/3 - extract slip velocity and strain rate from state vector
    !--------------------------------------------------------------------

    ! element index in v vector
    k=1
    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementIndex=layout%elementIndex(i)

       patch=in%patch%s(elementIndex)

       ! slip velocity
       velocity=EXP(y(l+STATE_VECTOR_VELOCITY)*lg10)

       ! maximum velocity
       vMax=MAX(velocity,vMax)

       ! maximum velocity
       tMax=MAX(y(l+STATE_VECTOR_TEMPERATURE),tMax)

       ! update state vector (rate of slip components)
       dydt(l+STATE_VECTOR_SLIP_DIP)=velocity

       v(k)=velocity-patch%Vl

       l=l+in%dPatch

       k=k+layout%elementVelocityDGF(i)
    END DO

    ! all threads gather velocity from all threads
    CALL MPI_ALLGATHERV(v,layout%listVelocityN(1+rank),MPI_REAL8, &
                        vAll,layout%listVelocityN,layout%listVelocityOffset,MPI_REAL8, &
                        MPI_COMM_WORLD,ierr)

    !-----------------------------------------------------------------
    ! step 2/3 - calculate the rate of traction and rate of stress
    !-----------------------------------------------------------------

#ifdef __BLAS__
    ! use the BLAS library to compute the matrix vector product
    ! use CALL atl_f77wrap_dgemv to call the atlas fortran library
    CALL DGEMV("T",SIZE(G,1),SIZE(G,2), &
                1d0,G,SIZE(G,1),vAll,1,0d0,t,1)
#else
    ! slower, Fortran intrinsic
    t=MATMUL(TRANSPOSE(G),vAll)
#endif

    !-----------------------------------------------------------------
    ! step 3/3 - calculate the rate of remaining state variables
    !-----------------------------------------------------------------

    ! stress perturbation
    IF (in%events%current .GT. 0) THEN
       event=in%events%event(in%events%current)
    END IF

    ! element index in t vector
    j=1
    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementIndex=layout%elementIndex(i)

       patch=in%patch%s(elementIndex)

       ! take-off angle
       beta=in%patch%beta(elementIndex)

       ! stress perturbation
       IF (in%events%current .GT. 0) THEN
          pert=boxcar(-0.5d0+(time-event%time)/event%rise(elementIndex))/event%rise(elementIndex)
          t(j+TRACTION_VECTOR_DIP   )=t(j+TRACTION_VECTOR_DIP   )+pert*event%dtau(elementIndex)
          t(j+TRACTION_VECTOR_NORMAL)=t(j+TRACTION_VECTOR_NORMAL)-pert*event%dsig(elementIndex)
       END IF
       
       ! slip velocity
       velocity=exp(y(l+STATE_VECTOR_VELOCITY)*lg10)

       ! moment-rate
       momentRate=momentRate+(velocity-patch%Vl)*in%mu*in%patch%width(elementIndex)

       ! rate of state
       SELECT CASE(frictionLawType)
       CASE(1:2)
          ! equivalent of aging law in isothermal condition (Barbot, 2019)
          dydt(l+STATE_VECTOR_STATE_1)=( &
                  EXP(-patch%H/in%R*(1.d0/y(l+STATE_VECTOR_TEMPERATURE)-1.d0/patch%To) &
                      -y(l+STATE_VECTOR_STATE_1)*lg10)-velocity/patch%L)/lg10
       CASE(3)
          ! equivalent of slip law in isothermal condition (Chester, 1994) (TODO)
          dydt(l+STATE_VECTOR_STATE_1)=( &
                  EXP(-patch%H/in%R*(1.d0/y(l+STATE_VECTOR_TEMPERATURE)-1.d0/patch%To) &
                      -y(l+STATE_VECTOR_STATE_1)*lg10)-velocity/patch%L)/lg10
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug)")') frictionLawType
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT

       ! rate of temperature
       dydt(l+STATE_VECTOR_TEMPERATURE)= &
               -patch%DW2*(y(l+STATE_VECTOR_TEMPERATURE)-patch%Tb) &
               +y(l+STATE_VECTOR_TRACTION_DIP)*velocity/patch%wRhoC

       ! scalar rate of shear traction
       dtau=COS(beta)*t(j+TRACTION_VECTOR_DIP)+SIN(beta)*t(j+TRACTION_VECTOR_NORMAL)
       
       ! normal stress
       IF (0 .EQ. beta) THEN
          sigma=patch%sig-y(l+STATE_VECTOR_TRACTION_NORMAL)
       ELSE
          sigma=patch%sig
       END IF

       SELECT CASE(frictionLawType)
       CASE(1)
          ! multiplicative form of rate-state friction (Barbot, 2019)
          IF (0 .EQ. beta) THEN
             friction=patch%mu0*exp((patch%a*log(velocity/patch%Vo) &
                                    +patch%b*(y(l+STATE_VECTOR_STATE_1)*lg10+log(patch%Vo/patch%L)) &
                                    +patch%a*patch%Q/in%R*(1d0/y(l+STATE_VECTOR_TEMPERATURE)-1d0/patch%To) &
                                    )/patch%mu0 )
          ELSE
             friction=0._8
          END IF

          ! acceleration
          dydt(l+STATE_VECTOR_VELOCITY)= &
               (dtau + ( patch%a*patch%Q/in%R*dydt(l+STATE_VECTOR_TEMPERATURE)/y(l+STATE_VECTOR_TEMPERATURE)**2 &
                        -patch%b*dydt(l+STATE_VECTOR_STATE_1)*lg10)*y(l+STATE_VECTOR_TRACTION_DIP)/patch%mu0 &
                        +friction*t(j+TRACTION_VECTOR_NORMAL)) / &
               (patch%a*y(l+STATE_VECTOR_TRACTION_DIP)/patch%mu0+patch%damping*velocity) / lg10
       CASE(2:3)
          ! additive form of rate-state friction (Chester, 1994; Barbot, 2019)
          IF (0 .EQ. beta) THEN
             friction=patch%mu0+patch%a*(log(velocity/patch%Vo) &
                                        +patch%Q/in%R*(1d0/y(l+STATE_VECTOR_TEMPERATURE)+1e0/patch%To)) &
                               +patch%b*log(exp(y(l+STATE_VECTOR_STATE_1)*lg10)/patch%L*patch%Vo)
          ELSE
             friction=0.d0
          END IF

          ! acceleration
          dydt(l+STATE_VECTOR_VELOCITY)= &
                  (dtau-(patch%b*dydt(l+STATE_VECTOR_STATE_1)*lg10 &
                        +patch%a*patch%Q/in%R*dydt(l+STATE_VECTOR_TEMPERATURE)/(y(l+STATE_VECTOR_TEMPERATURE)**2))*sigma &
                  +friction*t(j+TRACTION_VECTOR_NORMAL)) / &
                  (patch%a*sigma+patch%damping*velocity) / lg10
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug)")') frictionLawType
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT

       ! rate of traction in the dip direction
       dydt(l+STATE_VECTOR_TRACTION_DIP)=dtau-patch%damping*velocity*dydt(l+STATE_VECTOR_VELOCITY)*lg10

       ! rate of traction in the normal direction
       dydt(l+STATE_VECTOR_TRACTION_NORMAL)=t(j+TRACTION_VECTOR_NORMAL)

       l=l+in%dPatch

       j=j+layout%elementForceDGF(i)
    END DO

  END SUBROUTINE odefun

  !-----------------------------------------------------------------------
  !> subroutine initParallelism()
  !! initialize variables describe the data layout
  !! for parallelism.
  !!
  !! OUTPUT:
  !! layout    - list of receiver type and type index
  !-----------------------------------------------------------------------
  SUBROUTINE initParallelism(in,layout)
    IMPLICIT NONE
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in
    TYPE(LAYOUT_STRUCT), INTENT(OUT) :: layout

    INTEGER :: i,j,k,n,remainder,cumulativeIndex
    INTEGER :: rank,csize,ierr
    INTEGER :: nElements,nColumns
    INTEGER :: buffer

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

    ! total number of elements
    nElements=in%patch%ns

    ! list of number of elements in thread
    ALLOCATE(layout%listElements(csize), &
             layout%listOffset(csize),STAT=ierr)
    IF (ierr>0) STOP "could not allocate the list"

    remainder=nElements-INT(nElements/csize)*csize
    IF (0 .LT. remainder) THEN
       layout%listElements(1:(csize-remainder))      =INT(nElements/csize)
       layout%listElements((csize-remainder+1):csize)=INT(nElements/csize)+1
    ELSE
       layout%listElements(1:csize)=INT(nElements/csize)
    END IF

    ! element start index in thread
    j=0
    k=0
    DO i=1,csize
       j=k+1
       k=k+layout%listElements(i)
       layout%listOffset(i)=j
    END DO

    ALLOCATE(layout%elementType       (layout%listElements(1+rank)), &
             layout%elementIndex      (layout%listElements(1+rank)), &
             layout%elementStateIndex (layout%listElements(1+rank)), &
             layout%elementVelocityDGF(layout%listElements(1+rank)), &
             layout%elementStateDGF   (layout%listElements(1+rank)), &
             layout%elementForceDGF   (layout%listElements(1+rank)),STAT=ierr)
    IF (ierr>0) STOP "could not allocate the layout elements"

    j=1
    cumulativeIndex=0
    DO i=1,in%patch%ns
       IF ((i .GE. layout%listOffset(1+rank)) .AND. &
           (i .LT. (layout%listOffset(1+rank)+layout%listElements(1+rank)))) THEN
          layout%elementType(j)=FLAG_PATCH
          layout%elementIndex(j)=i
          layout%elementStateIndex(j)=cumulativeIndex+STATE_VECTOR_DGF_PATCH
          cumulativeIndex=layout%elementStateIndex(j)
          layout%elementVelocityDGF(j)=DGF_PATCH
          layout%elementStateDGF(j)=in%dPatch
          layout%elementForceDGF(j)=DGF_VECTOR
          j=j+1
       END IF
    END DO

    ALLOCATE(layout%listVelocityN(csize), &
             layout%listVelocityOffset(csize), &
             layout%listStateN(csize), &
             layout%listStateOffset(csize), &
             layout%listForceN(csize), &
             STAT=ierr)
    IF (ierr>0) STOP "could not allocate the size list"

    ! share number of elements in threads
    CALL MPI_ALLGATHER(SUM(layout%elementVelocityDGF),1,MPI_INTEGER,layout%listVelocityN,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
    CALL MPI_ALLGATHER(SUM(layout%elementStateDGF),   1,MPI_INTEGER,layout%listStateN,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
    CALL MPI_ALLGATHER(SUM(layout%elementForceDGF),   1,MPI_INTEGER,layout%listForceN,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)

    j=0
    k=0
    DO i=1,csize
       j=k+1
       k=k+layout%listVelocityN(i)
       layout%listVelocityOffset(i)=j-1
    END DO

    j=0
    k=0
    DO i=1,csize
       j=k+1
       k=k+layout%listStateN(i)
       layout%listStateOffset(i)=j-1
    END DO

  END SUBROUTINE initParallelism

  !-----------------------------------------------------------------------
  !> subroutine initGeometry
  ! initializes the position and local reference system vectors
  !
  ! INPUT:
  ! @param in      - input parameters data structure
  !-----------------------------------------------------------------------
  SUBROUTINE initGeometry(in)
    USE planestrain
    USE types_ps_bath
    TYPE(SIMULATION_STRUCT), INTENT(INOUT) :: in
  
    IF (0 .LT. in%patch%ns) THEN
       CALL computeReferenceSystemPlaneStrain( &
                in%patch%ns, &
                in%patch%x, &
                in%patch%width, &
                in%patch%dip, &
                in%patch%sv, &
                in%patch%dv, &
                in%patch%nv, &
                in%patch%xc)
    END IF

  END SUBROUTINE initGeometry

  !---------------------------------------------------------------------
  !> subroutine init
  !! reads simulation parameters from the standard input and initialize
  !! model parameters.
  !!
  !! INPUT:
  !! @param unit - the unit number used to read input data
  !!
  !! OUTPUT:
  !! @param in
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !---------------------------------------------------------------------
  SUBROUTINE init(in)
    USE types_ps_bath
    USE getopt_m
  
    TYPE(SIMULATION_STRUCT), INTENT(OUT) :: in
  
    CHARACTER :: ch
    CHARACTER(512) :: dataline
    CHARACTER(256) :: filename
    INTEGER :: iunit,noptions
!$  INTEGER :: omp_get_num_procs,omp_get_max_threads
    TYPE(OPTION_S) :: opts(11)
  
    INTEGER :: k,e,ierr,i,rank,size,position
    INTEGER, PARAMETER :: psize=512
    INTEGER :: dummy
    CHARACTER, DIMENSION(psize) :: packed
  
    REAL*8 :: d_diff,w_diff,w_heat,rhoc
  
    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,size,ierr)
  
    ! define long options, such as --dry-run
    ! parse the command line for options
    opts( 1)=OPTION_S("version",.FALSE.,CHAR(21))
    opts( 2)=OPTION_S("dry-run",.FALSE.,CHAR(22))
    opts( 3)=OPTION_S("epsilon",.TRUE.,'e')
    opts( 4)=OPTION_S("export-greens",.TRUE.,'g')
    opts( 5)=OPTION_S("export-netcdf",.FALSE.,'n')
    opts( 6)=OPTION_S("friction-law",.TRUE.,'f')
    opts( 7)=OPTION_S("import-greens",.TRUE.,'p')
    opts( 8)=OPTION_S("maximum-step",.TRUE.,'m')
    opts( 9)=OPTION_S("maximum-iterations",.TRUE.,'i')
    opts(10)=OPTION_S("event-velocity-threshold",.TRUE.,'v')
    opts(11)=OPTION_S("help",.FALSE.,'h')
  
    noptions=0;
    DO
       ch=getopt("he:f:g:i:m:np:v:",opts)
       SELECT CASE(ch)
       CASE(CHAR(0))
          EXIT
       CASE(CHAR(21))
          ! option version
          in%isVersion=.TRUE.
       CASE(CHAR(22))
          ! option dry-run
          in%isDryRun=.TRUE.
       CASE('e')
          ! numerical accuracy (variable epsilon sits in the ode45 module)
          READ(optarg,*) epsilon
          noptions=noptions+1
       CASE('f')
          ! type of friction law
          READ(optarg,*) frictionLawType
          noptions=noptions+1
       CASE('g')
          ! export Greens functions to netcdf file
          READ(optarg,'(a)') in%greensFunctionDirectory
          in%isExportGreens=.TRUE.
          noptions=noptions+1
       CASE('i')
          ! maximum number of iterations
          READ(optarg,*) maximumIterations
          noptions=noptions+1
       CASE('m')
          ! maximum time step (variable maximumTimeStep sits in the ode45 module)
          READ(optarg,*) maximumTimeStep
          noptions=noptions+1
       CASE('n')
          ! export in netcdf format
          in%isExportNetcdf=.TRUE.
       CASE('p')
          ! import Greens functions from netcdf file
          READ(optarg,'(a)') in%greensFunctionDirectory
          in%isImportGreens=.TRUE.
          noptions=noptions+1
       CASE('h')
          ! option help
          in%isHelp=.TRUE.
       CASE('v')
          ! velocity threshold for slip events
          READ(optarg,*) catalogue%vStart
          catalogue%vEnd=0.1d0*catalogue%vStart
          in%isEventCatalogue=.TRUE.
          noptions=noptions+1
       CASE('?')
          WRITE_DEBUG_INFO(100)
          in%isHelp=.TRUE.
          EXIT
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug)")') optopt
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT
       noptions=noptions+1
    END DO
  
    IF (in%isVersion) THEN
       CALL printversion()
       ! abort parameter input
       STOP
    END IF
  
    IF (in%isHelp) THEN
       CALL printhelp()
       ! abort parameter input
       STOP
    END IF
  
    in%nPatch=0
    in%nVolume=0
    ! minimum number of dynamic variables for patches
    in%dPatch=STATE_VECTOR_DGF_PATCH
    in%patch%ns=0
    in%rectangle%ns=0
    in%triangle%ns=0
    
    IF (0 .EQ. rank) THEN
       PRINT 2000
       PRINT '("# RATESTATE")'
       PRINT '("# quasi-dynamic rupture simulation in plane")'
       PRINT '("# strain, non-isothermal conditions.")'
       SELECT CASE(frictionLawType)
       CASE(1)
          PRINT '("# friction law: multiplicative form with thermally activated aging law (Barbot, 2019)")'
       CASE(2)
          PRINT '("# friction law: additive form with thermally activated aging law (Barbot, 2019)")'
       CASE(3)
          PRINT '("# friction law: additive form with thermally activated slip law (Chester, 1994)")'
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug)")') frictionLawType
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT
       PRINT '("# numerical accuracy: ",ES15.4)', epsilon
       PRINT '("# maximum iterations: ",I15)', maximumIterations
       PRINT '("# maximum time step: ",ES16.4)', maximumTimeStep
       PRINT '("# number of threads: ",I16)', csize
       IF (in%isEventCatalogue) THEN
          PRINT '("# event velocity threshold:",ES10.2)', catalogue%vStart
       END IF
       IF (in%isExportNetcdf) THEN
          PRINT '("# export velocity to netcdf:      yes")'
       ELSE
          PRINT '("# export velocity to netcdf:       no")'
       END IF
       IF (in%isImportGreens) THEN
          WRITE (STDOUT,'("# import greens function:         yes (",a,")")') TRIM(in%greensFunctionDirectory)
       ELSE
          IF (in%isExportGreens) THEN
             WRITE (STDOUT,'("# export greens function:         yes (",a,")")') TRIM(in%greensFunctionDirectory)
          END IF
       END IF
!$     PRINT '("#     * parallel OpenMP implementation with ",I3.3,"/",I3.3," threads")', &
!$                omp_get_max_threads(),omp_get_num_procs()
       PRINT 2000
  
       IF (noptions .LT. COMMAND_ARGUMENT_COUNT()) THEN
          ! read from input file
          iunit=25
          CALL GET_COMMAND_ARGUMENT(noptions+1,filename)
          OPEN (UNIT=iunit,FILE=filename,IOSTAT=ierr)
       ELSE
          ! get input parameters from standard input
          iunit=5
       END IF
  
       PRINT '("# output directory")'
       CALL getdata(iunit,dataline)
       READ (dataline,'(a)') in%wdir
       PRINT '(2X,a)', TRIM(in%wdir)
  
       in%timeFilename=TRIM(in%wdir)//"/time.dat"
  
       ! test write permissions on output directory
       OPEN (UNIT=FPTIME,FILE=in%timeFilename,POSITION="APPEND",&
               IOSTAT=ierr,FORM="FORMATTED")
       IF (ierr>0) THEN
          WRITE_DEBUG_INFO(102)
          WRITE (STDERR,'("error: unable to access ",a)') TRIM(in%timefilename)
          STOP 1
       END IF
       CLOSE(FPTIME)
     
       PRINT '("# rigidity, Lame parameter, universal gas constant")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%mu, in%lambda, in%R
       PRINT '(3ES9.2E1)', in%mu, in%lambda, in%R
  
       IF (0 .GT. in%mu) THEN
          WRITE_DEBUG_INFO(-1)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("input error: shear modulus must be positive")')
          STOP 2
       END IF
       
       IF (-1._8 .GT. (in%lambda/2._8/(in%lambda+in%mu))) THEN
          WRITE_DEBUG_INFO(-1)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("input error: Poisson''s ratio must be greater than -1.")')
          STOP 2
       END IF
       IF (0.5_8 .LT. (in%lambda/2._8/(in%lambda+in%mu))) THEN
          WRITE_DEBUG_INFO(-1)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("input error: Poisson''s ratio must be lower than 0.5.")')
          STOP 2
       END IF

       IF (0 .GE. in%R) THEN
          WRITE_DEBUG_INFO(200)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("error in input file: R must be positive.")')
          STOP 1
       END IF

       PRINT '("# time interval")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%interval
       PRINT '(ES20.12E2)', in%interval
  
       IF (in%interval .LE. 0d0) THEN
          WRITE (STDERR,'("**** error **** ")')
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("simulation time must be positive. exiting.")')
          STOP 1
       END IF
  
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !                   P A T C H E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT '("# number of patches")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%patch%ns
       PRINT '(I5)', in%patch%ns
       IF (in%patch%ns .GT. 0) THEN
          ALLOCATE(in%patch%s(in%patch%ns), &
                   in%patch%x(3,in%patch%ns), &
                   in%patch%xc(3,in%patch%ns), &
                   in%patch%width(in%patch%ns), &
                   in%patch%dip(in%patch%ns), &
                   in%patch%beta(in%patch%ns), &
                   in%patch%sv(3,in%patch%ns), &
                   in%patch%dv(3,in%patch%ns), &
                   in%patch%nv(3,in%patch%ns),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the patch list"
          PRINT 2000
          PRINT '("#  n        Vl       x2       x3   width     dip    beta")'
          PRINT 2000
          DO k=1,in%patch%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                  in%patch%s(k)%Vl, &
                  in%patch%x(2,k), &
                  in%patch%x(3,k), &
                  in%patch%width(k), &
                  in%patch%dip(k), &
                  in%patch%beta(k)
   
             PRINT '(I4,ES10.2E2,2ES9.2E1,3ES8.2E1)',i, &
                  in%patch%s(k)%Vl, &
                  in%patch%x(2,k), &
                  in%patch%x(3,k), &
                  in%patch%width(k), &
                  in%patch%dip(k), &
                  in%patch%beta(k)
                
             ! convert to radians
             in%patch%dip(k)=in%patch%dip(k)*DEG2RAD     
             in%patch%beta(k)=in%patch%beta(k)*DEG2RAD

             IF (i .NE. k) THEN
                WRITE (STDERR,'("invalid patch definition")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
             IF (in%patch%width(k) .LE. 0d0) THEN
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: patch width must be positive.")')
                STOP 1
             END IF
                
          END DO
   
          ! export the patches
          filename=TRIM(in%wdir)//"/rfaults.flt.2d"
          OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          WRITE (FPOUT,'("#  n        Vl            x2            x3   width     dip  beta")')
          DO k=1,in%patch%ns
             WRITE (FPOUT,'(I4,ES10.2E2,2ES14.7E1,3ES8.2E1)') k, &
               in%patch%s(k)%Vl, &
               in%patch%x(2,k), &
               in%patch%x(3,k), &
               in%patch%width(k), &
               in%patch%dip(k), &
               in%patch%beta(k)
          END DO
          CLOSE(FPOUT)
                
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          !        F R I C T I O N   P R O P E R T I E S
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          PRINT 2000
          PRINT '("# number of frictional patches")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) dummy
          PRINT '(I5)', dummy
          IF (dummy .NE. in%patch%ns) THEN
             WRITE_DEBUG_INFO(-1)
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE(STDERR,'("input error: all patches require frictional properties")')
             STOP 2
          END IF
          PRINT '("#  n     tau0      mu0      sig        a        b        L       Vo  G/(2Vs)")'
          PRINT 2000
          DO k=1,in%patch%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*) i, &
                   in%patch%s(k)%tau0, &
                   in%patch%s(k)%mu0, &
                   in%patch%s(k)%sig, &
                   in%patch%s(k)%a, &
                   in%patch%s(k)%b, &
                   in%patch%s(k)%L, &
                   in%patch%s(k)%Vo, &
                   in%patch%s(k)%damping
   
             PRINT '(I4.4,8ES9.2E1)',i, &
                  in%patch%s(k)%tau0, &
                  in%patch%s(k)%mu0, &
                  in%patch%s(k)%sig, &
                  in%patch%s(k)%a, &
                  in%patch%s(k)%b, &
                  in%patch%s(k)%L, &
                  in%patch%s(k)%Vo, &
                  in%patch%s(k)%damping
                
             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'("invalid friction property definition for patch")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO
   
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          !   F A U L T   T H E R M A L   P R O P E R T I E S
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          PRINT 2000
          PRINT '("# number of thermal properties")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) dummy
          PRINT '(I5)', dummy
          IF (dummy .NE. in%patch%ns) THEN
             WRITE_DEBUG_INFO(-1)
             WRITE(STDERR,'("input error: all patches require frictional properties")')
             STOP 2
          END IF
          PRINT '("#  n        Q        H        D        W        w     rhoc       Tb")'
          PRINT 2000
          DO k=1,in%patch%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                   in%patch%s(k)%Q, &
                   in%patch%s(k)%H, &
                   d_diff,w_diff,w_heat,rhoc, &
                   in%patch%s(k)%Tb

             PRINT '(I4.4,7ES9.3E1)',i, &
                   in%patch%s(k)%Q, &
                   in%patch%s(k)%H, &
                   d_diff,w_diff,w_heat,rhoc, &
                   in%patch%s(k)%Tb
                
             ! lumped parameters
             in%patch%s(k)%wRhoC=w_heat*rhoc
             in%patch%s(k)%DW2=d_diff/w_diff**2

             ! steady-state temperature
             in%patch%s(k)%To=in%patch%s(k)%Tb+in%patch%s(k)%mu0*in%patch%s(k)%sig*in%patch%s(k)%Vl &
                     /in%patch%s(k)%wRhoC/in%patch%s(k)%DW2
   
             IF (i .ne. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'("invalid thermal friction property for patch")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO
   
       END IF
          
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !       O B S E R V A T I O N   P A T C H E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of observation patches")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%nObservationState
       PRINT '(I5)', in%nObservationState
       IF (0 .LT. in%nObservationState) THEN
          ALLOCATE(in%observationState(3,in%nObservationState),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the observation patches"
          PRINT 2000
          PRINT '("#  n      i rate")'
          PRINT 2000
          DO k=1,in%nObservationState
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i,in%observationState(1:2,k)

             PRINT '(I4,X,I6,X,I4)',i,in%observationState(1:2,k)

             IF (0 .GE. in%observationState(2,k)) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: sampling rate must be a strictly positive integer")')
                STOP 1
             END IF

             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO
       END IF

       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !        O B S E R V A T I O N   P O I N T S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of observation points")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%nObservationPoint
       PRINT '(I5)', in%nObservationPoint
       IF (0 .LT. in%nObservationPoint) THEN
          ALLOCATE(in%observationPoint(in%nObservationPoint),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the observation points"
          PRINT 2000
          PRINT '("# n name       x2       x3 rate")'
          PRINT 2000
          DO k=1,in%nObservationPoint
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                     in%observationPoint(k)%name, &
                     in%observationPoint(k)%x(2), &
                     in%observationPoint(k)%x(3), &
                     in%observationPoint(k)%rate

             ! set along-strike coordinate to zero for 2d solutions
             in%observationPoint(k)%x(1)=0._8

             PRINT '(I3.3,X,a4,2ES9.2E1,X,I4)',i, &
                     in%observationPoint(k)%name, &
                     in%observationPoint(k)%x(2), &
                     in%observationPoint(k)%x(3), &
                     in%observationPoint(k)%rate

             IF (0 .GE. in%observationPoint(k)%rate) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: sampling rate must be a strictly positive integer")')
                STOP 1
             END IF

             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO

          filename=TRIM(in%wdir)//"/opts.dat"
          OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          WRITE (FPOUT,'("#      x2       x3 name rate")')
          DO k=1,in%nObservationPoint
             WRITE (FPOUT,'(2ES9.2E1,X,a4,X,I4)') &
                     in%observationPoint(k)%x(2), &
                     in%observationPoint(k)%x(3), &
                     in%observationPoint(k)%name, &
                     in%observationPoint(k)%rate
          END DO
          CLOSE(FPOUT)

       END IF

       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !                    E V E N T S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of events")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%events%n
       PRINT '(I5)', in%events%n
       IF (0 .LT. in%events%n) THEN
          ALLOCATE(in%events%event(in%events%n),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the events"

          DO e=1,in%events%n

             IF (1 .NE. e) THEN
                PRINT '("# time of next event")'
                CALL getdata(iunit,dataline)
                READ (dataline,*) in%events%event(e)%time
                PRINT '(ES9.2E1)', in%events%event(e)%time
             ELSE
                in%events%event(e)%time=0._8
             END IF

             PRINT 2000
             PRINT '("# number of stress perturbation patches")'
             CALL getdata(iunit,dataline)
             READ  (dataline,*) dummy
             PRINT '(I5)', dummy
             IF (dummy .NE. in%patch%ns) THEN
                WRITE_DEBUG_INFO(-1)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE(STDERR,'("input error: all patches require stress perturbations")')
                STOP 2
             END IF

             ALLOCATE(in%events%event(e)%dtau(in%patch%ns), &
                      in%events%event(e)%dsig(in%patch%ns), &
                      in%events%event(e)%rise(in%patch%ns), STAT=ierr)
             IF (ierr>0) STOP "could not allocate the event stress perturbations"

             PRINT '("# n     dtau     dsig    rise-time")'
             PRINT 2000
             DO k=1,in%patch%ns
                CALL getdata(iunit,dataline)
                READ (dataline,*,IOSTAT=ierr) i, &
                      in%events%event(e)%dtau(k), &
                      in%events%event(e)%dsig(k), &
                      in%events%event(e)%rise(k)

                PRINT '(I3.3,3ES9.2E1)',i, &
                     in%events%event(e)%dtau(k), &
                     in%events%event(e)%dsig(k), &
                     in%events%event(e)%rise(k)

                IF (i .NE. k) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("invalid stress perturbation for patch")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   WRITE (STDERR,'("error in input file: unexpected index")')
                   STOP 1
                END IF
             END DO
          END DO

       END IF

       ! test the presence of dislocations
       IF ((in%patch%ns .EQ. 0) .AND. &
           (in%interval .LE. 0d0)) THEN
   
          WRITE_DEBUG_INFO(300)
          WRITE (STDERR,'("nothing to do. exiting.")')
          STOP 1
       END IF
   
       PRINT 2000
       ! flush standard output
       CALL FLUSH(6)      

       position=0
       CALL MPI_PACK(in%interval,         1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%mu,               1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%lambda,           1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%R,                1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%patch%ns,         1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%events%n,         1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nObservationState,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nObservationPoint,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)

       position=0
       CALL MPI_PACK(in%wdir,256,MPI_CHARACTER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)

       ! send the patches (geometry and friction properties) 
       DO i=1,in%patch%ns
          position=0
          CALL MPI_PACK(in%patch%s(i)%Vl,     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%x(2,i),      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%x(3,i),      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%width(i),    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%dip(i),      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%beta(i),     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%tau0,   1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%mu0,    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%sig,    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%a,      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%b,      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%L,      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%Vo,     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%damping,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%Q      ,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%H      ,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%wRhoC  ,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%DW2    ,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%Tb     ,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%To     ,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the observation patches
       DO i=1,in%nObservationState
          position=0
          CALL MPI_PACK(in%observationState(:,i),3,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the observation points
       DO i=1,in%nObservationPoint
          position=0
          CALL MPI_PACK(in%observationPoint(i)%name,10,MPI_CHARACTER,packed,psize,position,MPI_COMM_WORLD,ierr)
          DO k=1,3
             CALL MPI_PACK(in%observationPoint(i)%x(k),1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          END DO
          CALL MPI_PACK(in%observationPoint(i)%rate,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the events
       DO e=1,in%events%n
          position=0
          CALL MPI_PACK(in%events%event(e)%time,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          DO i=1,in%patch%ns
             position=0
             CALL MPI_PACK(in%events%event(e)%dtau(i),1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%events%event(e)%dsig(i),1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%events%event(e)%rise(i),1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          END DO
       END DO

    ELSE ! IF 0 .NE. rank

       !------------------------------------------------------------------
       ! S L A V E S
       !------------------------------------------------------------------

       position=0
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%interval,         1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%mu,               1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%lambda,           1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%R,                1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%patch%ns,         1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%events%n,         1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nObservationState,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nObservationPoint,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)

       position=0
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%wdir,256,MPI_CHARACTER,MPI_COMM_WORLD,ierr)

       IF (0 .LT. in%patch%ns) &
                    ALLOCATE(in%patch%s(in%patch%ns), &
                             in%patch%x(3,in%patch%ns), &
                             in%patch%xc(3,in%patch%ns), &
                             in%patch%width(in%patch%ns), &
                             in%patch%dip(in%patch%ns), &
                             in%patch%beta(in%patch%ns), &
                             in%patch%sv(3,in%patch%ns), &
                             in%patch%dv(3,in%patch%ns), &
                             in%patch%nv(3,in%patch%ns), &
                             STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for patches"

       DO i=1,in%patch%ns
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%Vl,     1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%x(2,i),      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%x(3,i),      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%width(i),    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%dip(i),      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%beta(i),     1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%tau0,   1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%mu0,    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%sig,    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%a,      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%b,      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%L,      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%Vo,     1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%damping,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%Q      ,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%H      ,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%wRhoC  ,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%DW2    ,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%Tb     ,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%To     ,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
       END DO

       IF (0 .LT. in%nObservationState) &
                    ALLOCATE(in%observationState(3,in%nObservationState), &
                             STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for observation patches"

       DO i=1,in%nObservationState
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%observationState(:,i),3,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       END DO

       IF (0 .LT. in%nObservationPoint) &
                    ALLOCATE(in%observationPoint(in%nObservationPoint), &
                             STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for observation points"

       DO i=1,in%nObservationPoint
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%observationPoint(i)%name,10,MPI_CHARACTER,MPI_COMM_WORLD,ierr)
          DO k=1,3
             CALL MPI_UNPACK(packed,psize,position,in%observationPoint(i)%x(k),1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          END DO
          CALL MPI_UNPACK(packed,psize,position,in%observationPoint(i)%rate,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       END DO

       IF (0 .LT. in%events%n) ALLOCATE(in%events%event(in%events%n),STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for events"

       DO e=1,in%events%n
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%events%event(e)%time,1,MPI_REAL8,MPI_COMM_WORLD,ierr)

          ALLOCATE(in%events%event(e)%dtau(in%patch%ns), &
                   in%events%event(e)%dsig(in%patch%ns), &
                   in%events%event(e)%rise(in%patch%ns),STAT=ierr)
          IF (ierr>0) STOP "slave could not allocate memory for events"

          DO i=1,in%patch%ns
             position=0
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%events%event(e)%dtau(i),1,MPI_REAL8,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%events%event(e)%dsig(i),1,MPI_REAL8,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%events%event(e)%rise(i),1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          END DO
       END DO

    END IF ! master or slaves

    in%nPatch=in%patch%ns

2000 FORMAT ("# ----------------------------------------------------------------------------")
     
  END SUBROUTINE init

  !-----------------------------------------------
  !> subroutine printhelp
  !! displays a help message with master thread.
  !-----------------------------------------------
  SUBROUTINE printhelp()
  
    INTEGER :: rank,size,ierr
    
    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,size,ierr)
    
    IF (0.EQ.rank) THEN
       PRINT '("usage:")'
       PRINT '("")'
       PRINT '("mpirun -n 2 unicycle-ps-ratestate-bath [-h] [--dry-run] [--help] [--epsilon 1e-6] [--export-netcdf] [filename]")'
       PRINT '("")'
       PRINT '("options:")'
       PRINT '("   -h                        prints this message and aborts calculation")'
       PRINT '("   --dry-run                 abort calculation, only output geometry")'
       PRINT '("   --export-netcdf           export the kinematics to a netcdf file")'
       PRINT '("   --help                    prints this message and aborts calculation")'
       PRINT '("   --version                 print version number and exit")'
       PRINT '("   --epsilon                 set the numerical accuracy [1E-6]")'
       PRINT '("   --export-greens wdir      export the Greens function to file")'
       PRINT '("   --event-velocity-theshold cut-off velocity for event catalogue")'
       PRINT '("   --friction-law          type of friction/evolution law [1]")'
       PRINT '("       1: multiplicative   multiplicative form, thermally actived aging law (Barbot, 2019)")'
       PRINT '("       2: additive         additive form, thermally activated aging law (Barbot, 2019)")'
       PRINT '("       3: additive         additive form, thermally activated slip law (Chester, 1994)")'
       PRINT '("   --import-greens wdir      import the Greens function from file")'
       PRINT '("   --maximum-iterations      set the maximum time step [1000000]")'
       PRINT '("   --maximum-step            set the maximum time step [none]")'
       PRINT '("")'
       PRINT '("description:")'
       PRINT '("   simulates elasto-dynamics on faults in plane strain")'
       PRINT '("   following the radiation-damping approximation using")'
       PRINT '("   the integral method in non-isothermal conditions.")'
       PRINT '("")'
       PRINT '("   if filename is not provided, reads from standard input.")'
       PRINT '("")'
       PRINT '("see also: ""man unicycle""")'
       PRINT '("")'
       CALL FLUSH(6)
    END IF
    
  END SUBROUTINE printhelp
  
  !-----------------------------------------------
  !> subroutine printversion
  !! displays code version with master thread.
  !-----------------------------------------------
  SUBROUTINE printversion()
    
    INTEGER :: rank,size,ierr
    
    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,size,ierr)
    
    IF (0.EQ.rank) THEN
       PRINT '("unicycle-ps-ratestate-bath version 1.0.0, compiled on ",a)', __DATE__
       PRINT '("")'
       CALL FLUSH(6)
    END IF
  
  END SUBROUTINE printversion
    
END PROGRAM ratestate_bath

