!-----------------------------------------------------------------------
! Copyright 2017-2025 Sylvain Barbot
!
! This file is part of UNICYCLE
!
! UNICYCLE is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! UNICYCLE is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with UNICYCLE.  If not, see <http://www.gnu.org/licenses/>.
!-----------------------------------------------------------------------

#include "macros.h90"

MODULE types_ps_thermobaric

  IMPLICIT NONE

#ifdef THERMOBARIC
  TYPE FLOW_STRUCT
      SEQUENCE
      REAL*8 :: Vo,c0,n,Q,To,zeta
  END TYPE FLOW_STRUCT

  TYPE HEALING_STRUCT
      SEQUENCE
      REAL*8 :: f0,p,q,H,To
  END TYPE HEALING_STRUCT

  TYPE ROCK_STRUCT
      SEQUENCE
      REAL*8 :: mu0,d0,sigma0,alpha,beta,lambda
      INTEGER :: nFlow,nHealing
      TYPE(FLOW_STRUCT), DIMENSION(:), ALLOCATABLE :: flow
      TYPE(HEALING_STRUCT), DIMENSION(:), ALLOCATABLE :: healing
      CHARACTER(80) :: name
  END TYPE ROCK_STRUCT
#endif

  TYPE CATALOGUE_STRUCT
     SEQUENCE
     ! event counter
     INTEGER :: index
     ! start and end indices
     INTEGER :: iStart,iEnd
     ! threshold velocities
     REAL*8 :: vStart,vEnd
     ! event start and end times
     REAL*8 :: tStart,tEnd
     ! peak velocity during event
     REAL*8 :: maximumVelocity
     ! state vector
     REAL*8, DIMENSION(:), ALLOCATABLE :: y
     ! catalogue file ID
     INTEGER :: id
     ! current event flag
     LOGICAL :: isEventOngoing
  END TYPE CATALOGUE_STRUCT

  TYPE EVENT_STRUCT
     SEQUENCE
     ! time of event
     REAL*8 :: time
     ! shear stress perturbation
     REAL*8, DIMENSION(:), ALLOCATABLE :: dtau
     ! confining pressure perturbation
     REAL*8, DIMENSION(:), ALLOCATABLE :: dsig
     ! rise time of perturbation
     REAL*8, DIMENSION(:), ALLOCATABLE :: rise
  END TYPE EVENT_STRUCT

  TYPE EVENT_CLASS
     SEQUENCE
     ! number of events
     INTEGER :: n
     ! index of current and next event
     INTEGER :: current,next
     ! stress perturbations
     TYPE(EVENT_STRUCT), DIMENSION(:), ALLOCATABLE :: event
  END TYPE EVENT_CLASS

  TYPE PATCH_ELEMENT_STRUCT
     SEQUENCE
     REAL*8 :: Vl
     REAL*8 :: tau0,sig,h
#ifdef BATH
     REAL*8 :: wRhoC,DW2,Tb
#endif
#ifdef THERMOBARIC
     INTEGER :: rockType
#endif
  END TYPE PATCH_ELEMENT_STRUCT

  TYPE PATCH_STRUCT
     SEQUENCE
     INTEGER :: ns
     TYPE(PATCH_ELEMENT_STRUCT), DIMENSION(:), ALLOCATABLE :: s
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: sv,dv,nv
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: x,xc
     REAL*8, DIMENSION(:), ALLOCATABLE :: width,dip
     ! take-off angle
     REAL*8, DIMENSION(:), ALLOCATABLE :: beta
  END TYPE PATCH_STRUCT

  TYPE VOLUME_ELEMENT_STRUCT
     SEQUENCE
     ! initial stress
     REAL*8 :: sII

     ! background strain rate
     REAL*8 :: e22,e23,e33

     ! transient (Kelvin) linear properties
     REAL*8 :: Gk,gammadot0k

     ! steady-state (Maxwell) linear properties
     REAL*8 :: Gm,gammadot0m

     ! transient (Kelvin) nonlinear properties
     REAL*8 :: nGk,ngammadot0k,npowerk

     ! steady-state (Maxwell) nonlinear properties
     REAL*8 :: nGm,ngammadot0m,npowerm

  END TYPE VOLUME_ELEMENT_STRUCT

  TYPE RECTANGLE_VOLUME_STRUCT
     SEQUENCE
     INTEGER :: ns

     ! number of linear Maxwell volumes
     INTEGER :: nMaxwell

     ! number of linear Kelvin volumes
     INTEGER :: nKelvin

     ! number of nonlinear Kelvin volumes
     INTEGER :: nNonlinearKelvin

     ! number of nonlinear Maxwell volumes
     INTEGER :: nNonlinearMaxwell

     ! array of strain volume properties
     TYPE(VOLUME_ELEMENT_STRUCT), DIMENSION(:), ALLOCATABLE :: s

     ! strike, dip, and normal vectors
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: sv,dv,nv

     ! upper left central position and center position
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: x,xc

     ! dimension and orientation
     REAL*8, DIMENSION(:), ALLOCATABLE :: thickness,width,dip

  END TYPE RECTANGLE_VOLUME_STRUCT

  TYPE TRIANGLE_VOLUME_STRUCT
     SEQUENCE
     INTEGER :: ns,nVe

     ! number of linear Maxwell volumes
     INTEGER :: nMaxwell

     ! number of linear Kelvin volumes
     INTEGER :: nKelvin

     ! number of nonlinear Kelvin volumes
     INTEGER :: nNonlinearKelvin

     ! number of nonlinear Maxwell volumes
     INTEGER :: nNonlinearMaxwell

     ! array of strain volume properties
     TYPE(VOLUME_ELEMENT_STRUCT), DIMENSION(:), ALLOCATABLE :: s

     ! center position
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: xc

     ! triangulation point index
     INTEGER, DIMENSION(:), ALLOCATABLE :: i1,i2,i3

     ! vertex position
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: v

  END TYPE TRIANGLE_VOLUME_STRUCT

  TYPE OBSERVATION_POINT_STRUCT
     SEQUENCE
     INTEGER :: file
     REAL*8, DIMENSION(3) :: x
     CHARACTER(LEN=10) :: name
     INTEGER :: rate
  END TYPE OBSERVATION_POINT_STRUCT

  TYPE :: SIMULATION_STRUCT
     ! rigidity
     REAL*8 :: mu

     ! Lame parameter
     REAL*8 :: lambda

#ifdef THERMOBARIC
     ! radiation damping term
     REAL*8 :: damping
#endif

#ifdef BATH
     ! universal gas constant
     REAL*8 :: R
#endif

#ifdef THERMOBARIC
     ! number of rock types
     INTEGER :: nRock

     ! list of rock types
     TYPE(ROCK_STRUCT), DIMENSION(:), ALLOCATABLE :: rock
#endif

     ! simulation time
     REAL*8 :: interval

     ! number of patches (rectangular and triangular)
     INTEGER :: nPatch

     ! number of degrees of freedom for faults
     INTEGER :: dPatch

     ! number of volume elements
     INTEGER :: nVolume,nRectangle,nTriangle

     ! number of degrees of freedom for volume elements
     INTEGER :: dVolume

     ! rectangular patches
     TYPE(PATCH_STRUCT) :: patch

     ! rectangle volume elements
     TYPE(RECTANGLE_VOLUME_STRUCT) :: rectangle

     ! triangle volume elements
     TYPE(TRIANGLE_VOLUME_STRUCT) :: triangle

     ! output directory
     CHARACTER(256) :: wdir

     ! Greens function directory
     CHARACTER(256) :: greensFunctionDirectory

     ! filenames
     CHARACTER(256) :: timeFilename

     ! number of observation states
     INTEGER :: nObservationState

     ! observation patches
     INTEGER, DIMENSION(:,:), ALLOCATABLE :: observationState

     ! number of observation points
     INTEGER :: nObservationPoint

     ! observation points
     TYPE(OBSERVATION_POINT_STRUCT), DIMENSION(:), ALLOCATABLE :: observationPoint

     ! observation points name
     CHARACTER(LEN=4), DIMENSION(:), ALLOCATABLE :: observationPointName

     ! perturbation events
     TYPE(EVENT_CLASS) :: events

     ! other options
     LOGICAL :: isDryRun=.FALSE.
     LOGICAL :: isExportGreens=.FALSE.
     LOGICAL :: isExportNetcdf=.FALSE.
     LOGICAL :: isExportState=.FALSE.
     LOGICAL :: isExportSurface=.FALSE.
     LOGICAL :: isExportVolume=.FALSE.
     LOGICAL :: isImportGreens=.FALSE.
     LOGICAL :: isImportState=.FALSE.
     LOGICAL :: isEventCatalogue=.FALSE.
     LOGICAL :: isHelp=.FALSE.
     LOGICAL :: isVersion=.FALSE.
     INTEGER :: netcdfExportRate=20

  END TYPE SIMULATION_STRUCT

  TYPE LAYOUT_STRUCT
     ! list of number of elements in threads
     INTEGER, DIMENSION(:), ALLOCATABLE :: listElements,listOffset

     ! type of elements (FLAG_PATCH, FLAG_RECTANGLE_VOLUME,FLAG_TRIANGLE_VOLUME)
     INTEGER, DIMENSION(:), ALLOCATABLE :: elementType

     ! index of elements
     INTEGER, DIMENSION(:), ALLOCATABLE :: elementIndex

     ! index of state indices
     INTEGER, DIMENSION(:), ALLOCATABLE :: elementStateIndex

     ! degrees of freedom for elements in velocity vector
     !   1 (dip slip) for patches
     INTEGER, DIMENSION(:), ALLOCATABLE :: elementVelocityDGF

     ! degrees of freedom for elements in state vector:
     !   5 (dip and normal traction, dip slip, velocity, state) for patches
     INTEGER, DIMENSION(:), ALLOCATABLE :: elementStateDGF

     ! degrees of freedom for elements in Greens function
     !   2 (dip and normal traction) for patches
     INTEGER, DIMENSION(:), ALLOCATABLE :: elementForceDGF

     ! number of traction and stress element for each thread and array position
     INTEGER, DIMENSION(:), ALLOCATABLE :: listForceN

     ! number of velocity and strain rate components for each thread and array position
     INTEGER, DIMENSION(:), ALLOCATABLE :: listVelocityN,listVelocityOffset

     ! number of state parameters for each thread and array position
     INTEGER, DIMENSION(:), ALLOCATABLE :: listStateN,listStateOffset

  END TYPE LAYOUT_STRUCT

END MODULE types_ps_thermobaric

