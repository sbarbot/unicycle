!-----------------------------------------------------------------------
!> program ViscousCycles simulates evolution of fault slip in a 
!! viscoelastic medium in condition of plane strain under the 
!! radiation damping approximation.
!!
!! \mainpage
!! 
!! The Green's function for traction and stress interaction amongst the
!! dislocations and volume elements has the following layout
!!
!!       / KK  KL  KT \
!!       |            |
!!   G = | LK  LL  LT |
!!       |            |
!!       \ TK  TL  TT /
!!
!! where
!!
!!   KK is the matrix for traction on faults due to fault slip
!!   KL is the matrix for traction on faults due to rectangle volume strain
!!   KT is the matrix for traction on faults due to triangle volume strain
!!
!!   LK is the matrix for stress in rectangle volumes due to fault slip
!!   LL is the matrix for stress in rectangle volumes due to strain in rectangle volumes
!!   LT is the matrix for stress in rectangle volumes due to strain in triangle volumes
!!
!!   TK is the matrix for stress in triangle volumes due to fault slip
!!   TL is the matrix for stress in triangle volumes due to strain in rectangle volumes
!!   TT is the matrix for stress in triangle volumes due to strain in triangle volumes
!!
!! The volumes have three strain directions: e22, e23, and e33. The stress/traction
!! interaction matrices can be expanded into
!!
!!        / LL2222  LL2223  LL2233 \
!!        |                        |
!!   LL = | LL2322  LL2323  LL2333 |
!!        |                        |
!!        \ LL3322  LL3323  LL3333 /
!!
!!        /                        \
!!   KL = | KLd22   KLd23   KLd33  |
!!        \                        /
!!
!!        / LK22s \
!!        |       |
!!   LK = | LK23s |
!!        |       |
!!        \ LK33d /
!!
!! The time evolution is evaluated numerically using the 4th/5th order
!! Runge-Kutta method with adaptive time steps. The state vector is 
!! as follows:
!!
!!
!!    / P1 1       \   +------------------------+  +---------------------+
!!    | .          |   |                        |  |                     |
!!    | P1 dPatch  |   |                        |  |                     |
!!    | .          |   |    nPatch * dPatch     |  |   nPatch * dPatch   |
!!    | .          |   |                        |  |                     |
!!    | Pn 1       |   |                        |  |                     |
!!    | .          |   |                        |  |                     |
!!    | Pn dPatch  |   +------------------------+  +---------------------+
!!    |            |                          
!!    | R1 1       |   +------------------------+  +---------------------+
!!    | .          |   |                        |  |                     |
!!    | .          |   |  nRectangle * dVolume  |  |                     |
!!    | .          |   |                        |  |                     |
!!    | R1 dVolume |   +------------------------+  |                     |
!!    |            |                               |  nVolume * dVolume  |
!!    | Tn 1       |   +------------------------+  |                     |
!!    | .          |   |                        |  |                     |
!!    | .          |   |   nTriangle * dVolume  |  |                     |
!!    | .          |   |                        |  |                     |
!!    \ Tn dVolume /   +------------------------+  +---------------------+
!! 
!!
!! where nPatch and nVolume are the number of patches and the number 
!! of volume elements, respectively, dPatch and dVolume are the 
!! degrees of freedom for patches and volumes, and nRectangle and
!! nTriangle are the number of rectangle and triangle volume element,
!! respectively, with nVolume = nRectangle + nTriangle.
!!
!! For every patch, we have the following items in the state vector
!!
!!   /   td    \  1
!!   |   tn    |  .
!!   |   sd    |  .
!!   !  theta* |  .
!!   \   v*    /  dPatch
!!
!! where td and tn are the local traction in the dip and normal direction, 
!! sd is the slip in the dip direction, v* is the logarithm of the norm of the 
!! velocity vector (v*=log10(V)), and theta* is the logarithm of the state 
!! variable (theta*=log10(theta)) in the rate and state friction framework.
!!
!! For each every volume element, we have the following items in the state vector
!!
!!   /  e22   \  1
!!   |  e23   |  . 
!!   |  e33   |  . 
!!   |  s22   |  .
!!   |  s23   |  .
!!   \  s33   /  dVolume
!!
!! where s22, s32 and s33 are the three independent components of the local stress 
!! tensor in plane strain, and e22, e23 and e33 are the three independent components
!! of the cumulative anelastic strain tensor.
!!
!! References:<br>
!!
!!   Barbot S., "Frictional and structural controls of seismic super-cycles at the 
!!   Japan trench", Earth Planet Space, 10.1186/s40623-020-01185-3, 2020.
!!
!!   Barbot S., "Deformation of a Half-Space from Anelastic Strain Confined
!!   in a Tetrahedral Volume", Bull. Seism. Soc. Am., 10.1785/0120180058, 2018.
!!
!! \author Sylvain Barbot (2017-2025).
!----------------------------------------------------------------------
PROGRAM viscouscycles

#include "macros.h90"

#ifdef NETCDF
  USE exportnetcdf
#endif
  USE planestrain
  USE greens_ps
  USE rk
  USE mpi_f08
  USE types_ps

  IMPLICIT NONE

  TYPE(SIMULATION_STRUCT) :: iparam

  REAL*8, PARAMETER :: pi=3.141592653589793d0
  REAL*8, PARAMETER :: DEG2RAD = 0.01745329251994329547437168059786927_8

  ! MPI rank and size
  INTEGER :: rank,csize

  ! error flag
  INTEGER :: ierr

#ifdef NETCDF
  ! netcdf file and variable
  INTEGER :: ncid,y_varid,z_varid,ncCount
#endif

  CHARACTER(256) :: filename

  ! maximum strain rate, maximum velocity
  REAL*8 :: eMax,vMax,eMaxAll,vMaxAll

  ! moment rate
  REAL*8 :: momentRate, momentRateAll

  ! scaling factor
  REAL*8, PARAMETER :: lg10=LOG(1.d1)

  ! state vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: y
  ! rate of change of state vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: dydt,yscal
  ! temporary variables
  REAL*8, DIMENSION(:), ALLOCATABLE :: ytmp,ytmp1,ytmp2,ytmp3

  ! slip velocity vector and strain rate tensor array (temporary space)
  REAL*8, DIMENSION(:), ALLOCATABLE :: v

  ! slip velocity vector and strain rate tensor array (temporary space)
  REAL*8, DIMENSION(:), ALLOCATABLE :: vAll

  ! traction vector and stress tensor array (temporary space)
  REAL*8, DIMENSION(:), ALLOCATABLE :: t

  ! displacement and kinematics vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: d,u,dAll,dfAll,dlAll

  ! Green's function
  REAL*8, DIMENSION(:,:), ALLOCATABLE :: G,O,Of,Ol

  ! time
  REAL*8 :: time,t0
  ! time step
  REAL*8 :: dt_try,dt_next,dt_done
  ! time steps
  INTEGER :: i,j

  ! type of friction law (default)
  INTEGER :: frictionLawType=1

  ! type of evolution law (default)
  INTEGER :: evolutionLawType=1

  ! maximum number of time steps (default)
  INTEGER :: maximumIterations=1000000

  ! layout for parallelism
  TYPE(LAYOUT_STRUCT) :: layout

  ! model parameters
  TYPE(SIMULATION_STRUCT) :: in

  ! initialization
  CALL MPI_INIT(ierr)
  CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
  CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

  ! start time
  time=0.0d0

  ! initial tentative time step
  dt_next=1.0d-3

  ! retrieve input parameters from command line
  CALL init(in)
  CALL FLUSH(STDOUT)

  IF (in%isdryrun .AND. 0 .EQ. rank) THEN
     PRINT '("dry run: abort calculation")'
  END IF
  IF (in%isdryrun .OR. in%isversion .OR. in%ishelp) THEN
     CALL MPI_FINALIZE(ierr)
     STOP
  END IF

  ! calculate basis vectors
  CALL initGeometry(in)

  ! describe data layout for parallelism
  CALL initParallelism(in,layout)

#ifdef NETCDF
  IF (in%isImportGreens) THEN
     CALL initG(in,layout,G)
     IF (0 .EQ. rank) THEN
        PRINT '("# import Green''s function.")'
     END IF
     CALL importGreensNetcdf(G)
  ELSE
     ! calculate the stress interaction matrix
     IF (0 .EQ. rank) THEN
        PRINT '("# computing Green''s functions.")'
     END IF
     CALL buildG(in,layout,G)
     IF (in%isExportGreens) THEN
        CALL exportGreensNetcdf(G)
     END IF
  END IF
#else
  IF (0 .EQ. rank) THEN
     PRINT '("# computing Green''s functions.")'
  END IF
  CALL buildG(in,layout,G)
#endif

  ! calculate the displacement matrix
  CALL buildO(in,layout,O,Of,Ol)

  ! synchronize threads before writing to standard output
  CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)

  IF (0 .EQ. rank) THEN
     PRINT 2000
  END IF

  ! velocity vector and strain rate tensor array (t=G*vAll)
  ALLOCATE(u(layout%listVelocityN(1+rank)), &
           v(layout%listVelocityN(1+rank)), &
           vAll(SUM(layout%listVelocityN)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the velocity and strain rate vector"

  ! traction vector and stress tensor array (t=G*v)
  ALLOCATE(t(layout%listForceN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the traction and stress vector"

  ! displacement vector (d=O*v)
  ALLOCATE(d    (in%nObservationPoint*DISPLACEMENT_VECTOR_DGF), &
           dAll (in%nObservationPoint*DISPLACEMENT_VECTOR_DGF), &
           dfAll(in%nObservationPoint*DISPLACEMENT_VECTOR_DGF), &
           dlAll(in%nObservationPoint*DISPLACEMENT_VECTOR_DGF),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the displacement vector"

  ALLOCATE(y(layout%listStateN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the state vector"

  ALLOCATE(dydt(layout%listStateN(1+rank)), &
           yscal(layout%listStateN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the state vectors"

  ALLOCATE(ytmp (layout%listStateN(1+rank)), &
           ytmp1(layout%listStateN(1+rank)), &
           ytmp2(layout%listStateN(1+rank)), &
           ytmp3(layout%listStateN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the RKQS work space"

  ! allocate buffer from rk module
#ifdef ODE45
  ALLOCATE(buffer(layout%listStateN(1+rank),5),SOURCE=0.0d0,STAT=ierr)
#else
  ALLOCATE(buffer(layout%listStateN(1+rank),3),SOURCE=0.0d0,STAT=ierr)
#endif
  IF (ierr>0) STOP "could not allocate the buffer work space"

  IF (0 .EQ. rank) THEN
     OPEN (UNIT=FPTIME,FILE=in%timeFilename,IOSTAT=ierr,FORM="FORMATTED")
     IF (ierr>0) THEN
        WRITE_DEBUG_INFO(102)
        WRITE (STDERR,'("error: unable to access ",a)') TRIM(in%timefilename)
        STOP 1
     END IF
  END IF

  ! initialize the y vector
  IF (0 .EQ. rank) THEN
     PRINT '("# initialize state vector.")'
  END IF

  CALL initStateVector(layout%listStateN(1+rank),y,in)

  IF (0 .EQ. rank) THEN
     PRINT 2000
  END IF

#ifdef NETCDF
  ! initialize netcdf output
  IF (in%isExportNetcdf) THEN
     CALL initnc()
  END IF
#endif

  ! gather maximum velocity
  CALL MPI_REDUCE(vMax,vMaxAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

  ! gather moment rate
  CALL MPI_REDUCE(momentRate,momentRateAll,1,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

  ! gather maximum strain-rate
  CALL MPI_REDUCE(eMax,eMaxAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

  ! initialize output
  IF (0 .EQ. rank) THEN
     WRITE(STDOUT,'("# export to ",a)') TRIM(in%wdir)
     PRINT 2000
     WRITE(STDOUT,'("#       n               time                 dt       vMax       eMax")')
     WRITE(STDOUT,'(I9.9,ES19.12E2,ES19.12E2,2ES11.4E2)') 0,time,dt_next,vMaxAll,eMaxAll
     WRITE(FPTIME,'("#               time                 dt               vMax         Moment-rate                eMax")')
  END IF

  ! initialize observation states
  DO j=1,in%nObservationState
     IF ((in%observationState(1,j) .GE. layout%listOffset(rank+1)) .AND. &
         (in%observationState(1,j) .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN

        SELECT CASE(layout%elementType(in%observationState(1,j)-layout%listOffset(rank+1)+1))
        CASE (FLAG_PATCH)
           WRITE (filename,'(a,"/patch-",I8.8,"-",I8.8,".dat")') TRIM(in%wdir), &
                   in%observationState(3,j), in%observationState(1,j)
           in%observationState(4,j)=100+j
           OPEN (UNIT=in%observationState(4,j), &
                 FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
           WRITE (in%observationState(4,j), &
                 '("#           time (s),        dip slip (m),        dip traction,     normal traction, ", &
                   "       log10(theta),     log10(velocity),            velocity,   d dip traction/dt,", &
                   "d normal traction/dt,        d theta / dt,    acceleration / v")')
           IF (ierr>0) THEN
              WRITE_DEBUG_INFO(102)
              WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
              STOP 1
           END IF
        CASE (FLAG_RECTANGLE_VOLUME,FLAG_TRIANGLE_VOLUME)
           WRITE (filename,'(a,"/volume-",I8.8,"-",I8.8,".dat")') TRIM(in%wdir), &
                   in%observationState(3,j), in%observationState(1,j)-in%nPatch
           in%observationState(4,j)=100+j
           OPEN (UNIT=in%observationState(4,j), &
                 FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
           WRITE (in%observationState(4,j), &
                 '("#            time (s),                 e22,                 e23,                 e33, ", &
                   "                s22,                 s23,                 s33,                ek22,", &
                   "                ek23,                ek33")')
           IF (ierr>0) THEN
              WRITE_DEBUG_INFO(102)
              WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
              STOP 1
           END IF
        CASE DEFAULT
           WRITE (STDERR,'("wrong case: this is a bug.")')
           WRITE_DEBUG_INFO(-1)
           STOP 2
        END SELECT

     END IF
  END DO

  ! initialize observation points
  IF (0 .EQ. rank) THEN
     DO j=1,in%nObservationPoint
        in%observationPoint(j)%file=1000+j
        WRITE (filename,'(a,"/opts-",a,".dat")') TRIM(in%wdir),TRIM(in%observationPoint(j)%name)
        OPEN (UNIT=in%observationPoint(j)%file, &
              FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
        IF (ierr>0) THEN
           WRITE_DEBUG_INFO(102)
           WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
           STOP 1
        END IF
        WRITE (in%observationPoint(j)%file, &
                '("#               time            u2 (all)            u3 (all)", &
                  "          u2 (fault)          u3 (fault)        u2 (viscous)        u3 (viscous)")')
     END DO
  END IF

  ! main loop
#ifdef ODE23
  CALL odefun(layout%listStateN(1+rank),time,y,dydt)
#endif
  DO i=1,maximumIterations

#ifdef ODE45
     CALL odefun(layout%listStateN(1+rank),time,y,dydt)
#endif

     CALL export()
     CALL exportPoints()

     IF (0 .EQ. MOD(i,100)) THEN
        IF (in%isexportsurface) THEN
           CALL exportSurface()
        END IF
        IF (in%isexportvolume) THEN
           CALL exportVolume()
        END IF
     END IF 

#ifdef NETCDF
     IF (0 .EQ. rank) THEN
        IF (in%isExportNetcdf) THEN
           IF (0 .EQ. MOD(i,20)) THEN
              CALL exportnc(in%nPatch)
           END IF
        END IF
     END IF
#endif

     dt_try=dt_next
     yscal(:)=abs(y(:))+abs(dt_try*dydt(:))+TINY

     t0=time
#ifdef ODE45
     CALL rungeKutta(layout%listStateN(1+rank),t0,y,dydt, &
               yscal,ytmp1,ytmp2,ytmp3,dt_try,dt_done,dt_next,odefun,timeStep45)
#else
     CALL rungeKutta(layout%listStateN(1+rank),t0,y,dydt, &
               yscal,ytmp1,ytmp2,ytmp3,dt_try,dt_done,dt_next,odefun,timeStep23)
#endif

     time=time+dt_done

     ! end calculation
     IF (in%interval .LE. time) THEN
        EXIT
     END IF
   
  END DO

  IF (0 .EQ. rank) THEN
     PRINT '(I9.9," time steps.")', i
  END IF

  IF (0 .EQ. rank) THEN
     CLOSE(FPTIME)
  END IF

  ! close observation patch files
  DO j=1,in%nObservationState
     IF ((in%observationState(1,j) .GE. layout%listOffset(rank+1)) .AND. &
         (in%observationState(1,j) .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN
        CLOSE(in%observationState(4,j))
     END IF
  END DO

  ! close the observation points
  IF (0 .EQ. rank) THEN
     DO j=1,in%nObservationPoint
        CLOSE(in%observationPoint(j)%file)
     END DO
  END IF

#ifdef NETCDF
  ! close the netcdf file
  IF (0 .EQ. rank) THEN
     IF (in%isExportNetcdf) THEN
        CALL closeNetcdfUnlimited(ncid,y_varid,z_varid,ncCount)
     END IF
  END IF
#endif

  DEALLOCATE(y,dydt,yscal)
  DEALLOCATE(ytmp,ytmp1,ytmp2,ytmp3)
  DEALLOCATE(buffer)
  DEALLOCATE(G,v,vAll,t)
  DEALLOCATE(layout%listForceN)
  DEALLOCATE(layout%listVelocityN,layout%listVelocityOffset)
  DEALLOCATE(layout%listStateN,layout%listStateOffset)
  DEALLOCATE(layout%elementStateIndex)
  DEALLOCATE(layout%listElements,layout%listOffset)
  DEALLOCATE(O,Of,Ol,d,u,dAll,dfAll,dlAll)

  CALL MPI_FINALIZE(ierr)

2000 FORMAT ("# ----------------------------------------------------------------------------")
     
CONTAINS

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportGreensNetcdf
  ! initializes the coordinates of netcdf files
  !----------------------------------------------------------------------
  SUBROUTINE exportGreensNetcdf(M)
    REAL*8, INTENT(IN), DIMENSION(:,:) :: M

    REAL*8, DIMENSION(:), ALLOCATABLE :: x,y

    INTEGER :: i,ierr
    CHARACTER(LEN=256) :: filename

    ALLOCATE(x(SIZE(M,1)),y(SIZE(M,2)),STAT=ierr)
    IF (ierr/=0) STOP "could not allocate netcdf coordinate for Greens function"

    ! coordinates
    DO i=1,SIZE(M,1)
       x(i)=REAL(i,8)
    END DO
    DO i=1,SIZE(M,2)
       y(i)=REAL(i,8)
    END DO

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/greens-",I4.4,".grd")') TRIM(in%greensFunctionDirectory),rank
    CALL writeNetcdf(filename,SIZE(M,1),x,SIZE(M,2),y,M,1)

    DEALLOCATE(x,y)

  END SUBROUTINE exportGreensNetcdf
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine importGreensNetcdf
  ! import the Greens function by rank
  !----------------------------------------------------------------------
  SUBROUTINE importGreensNetcdf(M)
    REAL*8, DIMENSION(:,:), INTENT(OUT) :: M

    INTEGER :: ierr
    CHARACTER(LEN=256) :: filename

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/greens-",I4.4,".grd")') TRIM(in%greensFunctionDirectory),rank
    CALL readNetcdf(filename,SIZE(M,1),SIZE(M,2),M)

  END SUBROUTINE importGreensNetcdf
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine initnc
  ! initializes the coordinates of netcdf files
  !----------------------------------------------------------------------
  SUBROUTINE initnc()

    REAL*8, DIMENSION(:), ALLOCATABLE :: x

    INTEGER :: i,ierr
    CHARACTER(LEN=256) :: filename

    IF (0 .EQ. rank) THEN

       ! initialize the number of exports
       ncCount = 0

       ALLOCATE(x(in%nPatch),STAT=ierr)
       IF (ierr/=0) STOP "could not allocate netcdf coordinate"

       ! loop over all patch elements
       DO i=1,in%nPatch
          x(i)=REAL(i,8)
       END DO

       ! netcdf file is compatible with GMT
       WRITE (filename,'(a,"/log10v.grd")') TRIM(in%wdir)
       CALL openNetcdfUnlimited(filename,in%nPatch,x,ncid,y_varid,z_varid)

       DEALLOCATE(x)

    END IF

  END SUBROUTINE initnc
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportnc
  ! export time series of log10(v)
  !----------------------------------------------------------------------
  SUBROUTINE exportnc(n)
    INTEGER, INTENT(IN) :: n

    REAL*4, DIMENSION(n) :: z

    INTEGER :: j,l,ierr
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    ! update the export count
    ncCount=ncCount+1

    ! loop over all patch elements
    DO j=1,in%nPatch
       ! dip slip
       z(j)=REAL(LOG10(in%patch%s(j)%Vl+vAll(DGF_PATCH*(j-1)+1)),4)
    END DO

    CALL writeNetcdfUnlimited(ncid,y_varid,z_varid,ncCount,n,z)

    ! flush every so often
    IF (0 .EQ. MOD(ncCount,50)) THEN
       CALL flushNetcdfUnlimited(ncid,y_varid,ncCount)
    END IF

  END SUBROUTINE exportnc
#endif

  !-----------------------------------------------------------------------
  !> subroutine exportSurface
  !! export the instantaneous plastic strain-rate in GMT compatible .xy
  !! format. 
  !----------------------------------------------------------------------
  SUBROUTINE exportSurface()

    CHARACTER(1024) :: filename
    INTEGER :: k,ierr
    INTEGER, PARAMETER :: iunit=30
    REAL*8 :: velocity

    IF (0 .LT. in%patch%ns) THEN
       WRITE(filename,'(a,"/surface-",I6.6,".xy")') TRIM(in%wdir),i

       OPEN (UNIT=iunit,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
       IF (ierr>0) THEN
          WRITE_DEBUG_INFO(102)
          WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
          STOP 1
       END IF
       DO k=1,in%patch%ns
          velocity=LOG10(ABS(vAll((k-1)*DGF_PATCH+1)+in%patch%s(k)%Vl))
          IF (0d0 .LE. velocity) THEN
             WRITE (iunit,'("> -Z",ES17.10E3)') velocity
          ELSE
             WRITE (iunit,'("> -Z",ES18.10E3)') velocity
          END IF
          WRITE (iunit,'(2ES20.12E2)') in%patch%x(2,k),in%patch%x(3,k)                                       
          WRITE (iunit,'(2ES20.12E2)') in%patch%x(2,k)+COS(in%patch%dip(k))*in%patch%width(k), &
                                       in%patch%x(3,k)+SIN(in%patch%dip(k))*in%patch%width(k)
       END DO
       CLOSE(iunit)
    END IF

  END SUBROUTINE exportSurface

  !-----------------------------------------------------------------------
  !> subroutine exportVolume
  !! export the instantaneous plastic strain-rate in GMT compatible .xy
  !! format. 
  !----------------------------------------------------------------------
  SUBROUTINE exportVolume()

    CHARACTER(1024) :: filename
    INTEGER :: k,ierr
    INTEGER, PARAMETER :: iunit=30
    REAL*8 :: e22,e23,e33,ekk,eII

    IF (0 .LT. in%rectangle%ns) THEN
       WRITE(filename,'(a,"/rectangle-",I6.6,".xy")') TRIM(in%wdir),i

       OPEN (UNIT=iunit,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
       IF (ierr>0) THEN
          WRITE_DEBUG_INFO(102)
          WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
          STOP 1
       END IF
       DO k=1,in%rectangle%ns
          e22=vAll(in%patch%ns*DGF_PATCH+(k-1)*DGF_VOLUME+1)+in%rectangle%s(k)%e22
          e23=vAll(in%patch%ns*DGF_PATCH+(k-1)*DGF_VOLUME+2)+in%rectangle%s(k)%e23
          e33=vAll(in%patch%ns*DGF_PATCH+(k-1)*DGF_VOLUME+2)+in%rectangle%s(k)%e33
          ekk=e22+e33
          eII=LOG10(SQRT((e22-ekk/2)**2+2*e23**2+(e33-ekk/2)**2))
          IF (0d0 .LE. eII) THEN
             WRITE (iunit,'("> -Z",ES17.10E3," # ",3ES19.10E3)') eII,e22,e23,e33
          ELSE
             WRITE (iunit,'("> -Z",ES18.10E3," # ",3ES19.10E3)') eII,e22,e23,e33
          END IF
          WRITE (iunit,'(2ES20.12E2)') in%rectangle%x(2,k)-in%rectangle%thickness(k)/2,in%rectangle%x(3,k)
          WRITE (iunit,'(2ES20.12E2)') in%rectangle%x(2,k)+in%rectangle%thickness(k)/2,in%rectangle%x(3,k)
          WRITE (iunit,'(2ES20.12E2)') in%rectangle%x(2,k)+in%rectangle%thickness(k)/2,in%rectangle%x(3,k)+in%rectangle%width(k)
          WRITE (iunit,'(2ES20.12E2)') in%rectangle%x(2,k)-in%rectangle%thickness(k)/2,in%rectangle%x(3,k)+in%rectangle%width(k)
       END DO
       CLOSE(iunit)
    END IF

    IF (0 .LT. in%triangle%ns) THEN
       WRITE(filename,'(a,"/triangle-",I6.6,".xy")') TRIM(in%wdir),i

       OPEN (UNIT=iunit,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
       IF (ierr>0) THEN
          WRITE_DEBUG_INFO(102)
          WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
          STOP 1
       END IF
       DO k=1,in%triangle%ns
          e22=vAll(in%patch%ns*DGF_PATCH+in%rectangle%ns*DGF_VOLUME+(k-1)*DGF_VOLUME+1)+in%triangle%s(k)%e22
          e23=vAll(in%patch%ns*DGF_PATCH+in%rectangle%ns*DGF_VOLUME+(k-1)*DGF_VOLUME+2)+in%triangle%s(k)%e23
          e33=vAll(in%patch%ns*DGF_PATCH+in%rectangle%ns*DGF_VOLUME+(k-1)*DGF_VOLUME+3)+in%triangle%s(k)%e33
          ekk=e22+e33
          eII=LOG10(SQRT((e22-ekk/2)**2+2*e23**2+(e33-ekk/2)**2))
          IF (0d0 .LE. eII) THEN
             WRITE (iunit,'("> -Z",ES17.10E3," # ",3ES19.10E3)') eII,e22,e23,e33
          ELSE
             WRITE (iunit,'("> -Z",ES18.10E3," # ",3ES19.10E3)') eII,e22,e23,e33
          END IF
          WRITE (iunit,'(2ES20.12E2)') in%triangle%v(2,in%triangle%i1(k)),in%triangle%v(3,in%triangle%i1(k))
          WRITE (iunit,'(2ES20.12E2)') in%triangle%v(2,in%triangle%i2(k)),in%triangle%v(3,in%triangle%i2(k))
          WRITE (iunit,'(2ES20.12E2)') in%triangle%v(2,in%triangle%i3(k)),in%triangle%v(3,in%triangle%i3(k))
       END DO
       CLOSE(iunit)
    END IF

  END SUBROUTINE exportVolume

  !-----------------------------------------------------------------------
  !> subroutine exportPoints
  ! export observation points
  !----------------------------------------------------------------------
  SUBROUTINE exportPoints()

    IMPLICIT NONE

    INTEGER :: j,k,l,ierr
    INTEGER :: elementIndex,elementType
    CHARACTER(2014) :: formatString
    TYPE(PATCH_ELEMENT_STRUCT) :: patch
    TYPE(VOLUME_ELEMENT_STRUCT) :: volume

    !-----------------------------------------------------------------
    ! step 1/3 - gather the kinematics from the state vector
    !-----------------------------------------------------------------

    ! element index in d vector
    k=1
    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO j=1,SIZE(layout%elementIndex)
       elementType= layout%elementType(j)
       elementIndex=layout%elementIndex(j)

       SELECT CASE (elementType)
       CASE (FLAG_PATCH)

          !patch=in%patch%s(elementIndex)

          ! strike slip
          u(k)=y(l+STATE_VECTOR_SLIP_DIP)

          l=l+in%dPatch

       CASE (FLAG_RECTANGLE_VOLUME,FLAG_TRIANGLE_VOLUME)

          !volume=in%rectangle%s(elementIndex)

          ! strain components e22, e23 and e33
          u(k:k+layout%elementVelocityDGF(j)-1)=(/ &
                     y(l+STATE_VECTOR_E22), &
                     y(l+STATE_VECTOR_E23), &
                     y(l+STATE_VECTOR_E33) /)

          l=l+in%dVolume

       CASE DEFAULT
          WRITE(STDERR,'("wrong case: this is a bug.")')
          WRITE_DEBUG_INFO(-1)
          STOP 2
       END SELECT

       k=k+layout%elementVelocityDGF(j)
    END DO

    !-----------------------------------------------------------------
    ! step 2/3 - calculate the rate of traction and rate of stress
    !            master thread adds the contribution of all elemets
    !-----------------------------------------------------------------

#ifdef __BLAS__
    ! use the BLAS library to compute the matrix vector product
    CALL DGEMV("T",SIZE(O,1),SIZE(O,2), &
                1._8,O,SIZE(O,1),u,1,0.d0,d,1)
#else
    ! slower, Fortran intrinsic
    d=MATMUL(TRANSPOSE(O),u)
#endif

    CALL MPI_REDUCE(d,dAll,in%nObservationPoint*DISPLACEMENT_VECTOR_DGF, &
                    MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

#ifdef __BLAS__
    ! use the BLAS library to compute the matrix vector product
    CALL DGEMV("T",SIZE(Of,1),SIZE(Of,2), &
                1._8,Of,SIZE(Of,1),u,1,0.d0,d,1)
#else
    ! slower, Fortran intrinsic
    d=MATMUL(TRANSPOSE(Of),u)
#endif

    CALL MPI_REDUCE(d,dfAll,in%nObservationPoint*DISPLACEMENT_VECTOR_DGF, &
                    MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

#ifdef __BLAS__
    ! use the BLAS library to compute the matrix vector product
    CALL DGEMV("T",SIZE(Ol,1),SIZE(Ol,2), &
                1._8,Ol,SIZE(Ol,1),u,1,0.d0,d,1)
#else
    ! slower, Fortran intrinsic
    d=MATMUL(TRANSPOSE(Ol),u)
#endif

    CALL MPI_REDUCE(d,dlAll,in%nObservationPoint*DISPLACEMENT_VECTOR_DGF, &
                    MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

    !-----------------------------------------------------------------
    ! step 3/3 - master thread writes to disk
    !-----------------------------------------------------------------

    IF (0 .EQ. rank) THEN
       formatString="(ES20.14E2"
       DO j=1,DISPLACEMENT_VECTOR_DGF
          formatString=TRIM(formatString)//",X,ES19.12E2,X,ES19.12E2,X,ES19.12E2"
       END DO
       formatString=TRIM(formatString)//")"

       ! element index in d vector
       k=1
       DO j=1,in%nObservationPoint
          IF (0 .EQ. MOD(i-1,in%observationPoint(j)%rate)) THEN
             WRITE (in%observationPoint(j)%file,TRIM(formatString)) &
                  time, &
                  dAll (k:k+DISPLACEMENT_VECTOR_DGF-1), &
                  dfAll(k:k+DISPLACEMENT_VECTOR_DGF-1), &
                  dlAll(k:k+DISPLACEMENT_VECTOR_DGF-1)
          END IF
          k=k+DISPLACEMENT_VECTOR_DGF
       END DO
    END IF

  END SUBROUTINE exportPoints

  !-----------------------------------------------------------------------
  !> subroutine export
  ! write the state variables of elements, either patch or volume, and 
  ! other information.
  !----------------------------------------------------------------------
  SUBROUTINE export()

    IMPLICIT NONE

    ! degrees of freedom
    INTEGER :: dgf

    ! counters
    INTEGER :: j,k

    ! index in state vector
    INTEGER :: index

    ! format string
    CHARACTER(1024) :: formatString

    ! gather maximum velocity
    CALL MPI_REDUCE(vMax,vMaxAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

    ! gather moment-rate
    CALL MPI_REDUCE(momentRate,momentRateAll,1,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

    ! gather maximum strain-rate
    CALL MPI_REDUCE(eMax,eMaxAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

    ! export observation state
    DO j=1,in%nObservationState
       IF ((in%observationState(1,j) .GE. layout%listOffset(rank+1)) .AND. &
           (in%observationState(1,j) .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN

          IF (0 .EQ. MOD(i-1,in%observationState(2,j))) THEN
             SELECT CASE(layout%elementType(in%observationState(1,j)-layout%listOffset(rank+1)+1))
             CASE (FLAG_PATCH)
                dgf=in%dPatch
             CASE (FLAG_RECTANGLE_VOLUME,FLAG_TRIANGLE_VOLUME)
                dgf=in%dVolume
             CASE DEFAULT
                WRITE(STDERR,'("wrong case: this is a bug.")')
                WRITE_DEBUG_INFO(-1)
                STOP 2
             END SELECT

             formatString="(ES20.14E2"
             DO k=1,dgf
                formatString=TRIM(formatString)//",X,ES20.12E3,X,ES20.12E3"
             END DO
             formatString=TRIM(formatString)//")"

             index=layout%elementStateIndex(in%observationState(1,j)-layout%listOffset(rank+1)+1)-dgf
             WRITE (in%observationState(4,j),TRIM(formatString)) time, &
                       y(index+1:index+dgf), &
                    dydt(index+1:index+dgf)
          END IF
          IF (0 .EQ. MOD(i-1,500*in%observationState(2,j))) THEN
             CALL FLUSH(in%observationState(4,j))
          END IF
       END IF
    END DO

    IF (0 .EQ. rank) THEN
       WRITE(FPTIME,'(ES20.14E2,ES19.12E2,ES19.12E2,2ES20.12E2)') time,dt_done,vMaxAll,momentRateAll,eMaxAll
       IF (0 .EQ. MOD(i,50)) THEN
          WRITE(STDOUT,'(I9.9,ES19.12E2,ES19.12E2,2ES11.4E2)') i,time,dt_done,vMaxAll,eMaxAll
          CALL FLUSH(STDOUT)
       END IF
    END IF

  END SUBROUTINE export

  !-----------------------------------------------------------------------
  !> subroutine initStateVector
  ! initialize the state vector
  !
  ! INPUT:
  ! @param n - number of state elements own by current thread
  ! @param y - the state vector (segment owned by currect thread)
  !
  !----------------------------------------------------------------------
  SUBROUTINE initStateVector(n,y,in)

    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: n
    REAL*8, INTENT(OUT)    :: y(n)
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in

    INTEGER :: i,l,ierr
    INTEGER :: elementType,elementIndex
    TYPE(PATCH_ELEMENT_STRUCT) :: patch
    TYPE(VOLUME_ELEMENT_STRUCT) :: volume

    ! norm of deviatoric strain tensor
    REAL*8 :: eps

    ! norm of deviatoric stress tensor
    REAL*8 :: tau

    ! isotropic strain
    REAL*8 :: ekk

    ! deviatoric strain components
    REAL*8 :: e22p,e33p

    ! stress components
    REAL*8 :: s22,s23,s33

    ! initialize state vector to zero
    y=0d0

    ! maximum velocity
    vMax=0._8

    ! maximum strain-rate
    eMax=0._8

    ! moment-rate
    momentRate=0._8

    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementType= layout%elementType(i)
       elementIndex=layout%elementIndex(i)

       SELECT CASE (elementType)
       CASE (FLAG_PATCH)

          patch=in%patch%s(elementIndex)

          ! dip slip
          y(l+STATE_VECTOR_SLIP_DIP) = 0d0

          ! traction in dip direction
          IF (0 .GT. patch%tau0) THEN
             SELECT CASE(frictionLawType)
             CASE(1)
                ! multiplicative form of rate-state friction (Barbot, 2018)
                y(l+STATE_VECTOR_TRACTION_DIP) = patch%mu0*patch%sig*exp((patch%a-patch%b)/patch%mu0*log(patch%Vl/patch%Vo)) &
                                                         +patch%damping*patch%Vl
             CASE(2)
                ! additive form of rate-state friction (Ruina, 1983)
                y(l+STATE_VECTOR_TRACTION_DIP) = patch%sig*(patch%mu0+(patch%a-patch%b)*log(patch%Vl/patch%Vo)) &
                                                         +patch%damping*patch%Vl
             CASE(3)
                ! arcsinh form of rate-state friction (Rice & Benzion, 1996)
                y(l+STATE_VECTOR_TRACTION_DIP) = &
                       patch%a*patch%sig*ASINH(patch%Vl/2/patch%Vo*exp((patch%mu0+patch%b*log(patch%Vo/patch%Vl))/patch%a)) &
                                                         +patch%damping*patch%Vl
             CASE DEFAULT
                WRITE (0,'("unhandled option ", a, " (this is a bug")') frictionLawType
                WRITE_DEBUG_INFO(100)
                STOP 3
             END SELECT
          ELSE
             y(l+STATE_VECTOR_TRACTION_DIP) = patch%tau0
          END IF

          ! traction in normal direction
          y(l+STATE_VECTOR_TRACTION_NORMAL) = 0d0

          ! state variable log(theta Vo / L)
          y(l+STATE_VECTOR_STATE_1) = log(patch%L/patch%Vl)/lg10

          ! maximum velocity
          vMax=MAX(patch%Vl,vMax)

          ! moment-rate
          momentRate=momentRate+((0.98d0-1.0d0)*patch%Vl)*in%mu*in%patch%width(elementIndex)

          ! slip velocity log(V/Vo)
          y(l+STATE_VECTOR_VELOCITY) = log(patch%Vl*0.98d0)/lg10

          l=l+in%dPatch

       CASE (FLAG_RECTANGLE_VOLUME,FLAG_TRIANGLE_VOLUME)

          IF (elementType .EQ. FLAG_RECTANGLE_VOLUME) THEN
             volume=in%rectangle%s(elementIndex)
          ELSE
             volume=in%triangle%s(elementIndex)
          END IF

          ! strain rate from nonlinear viscosity in Maxwell element
          ekk=volume%e22+volume%e33
          e22p=volume%e22-ekk/2
          e33p=volume%e33-ekk/2
          eps=SQRT((e22p**2+2._8*volume%e23**2+e33p**2)/2._8)

          ! stress components s22, s23 and s33
          IF (0._8 .GT. volume%sII) THEN
             IF ((0d0 .LT. eps) .AND. (0d0 .LT. volume%ngammadot0m)) THEN
                tau=(eps/volume%ngammadot0m)**(1d0/volume%npowerm)
                s22 = tau*e22p/eps
                s23 = tau*volume%e23/eps
                s33 = tau*e33p/eps
             ELSE
                s22 = 0._8
                s23 = 0._8
                s33 = 0._8
             END IF
          ELSE
             IF (0d0 .LT. eps) THEN
                s22 = volume%sII*volume%e22/eps
                s23 = volume%sII*volume%e23/eps
                s33 = volume%sII*volume%e33/eps
             ELSE
                s22 = 0._8
                s23 = 0._8
                s33 = 0._8
             END IF
          END IF

          ! maximum velocity
          eMax=MAX(eps,eMax)

          ! stress components s22, s23 and s33
          y(l+STATE_VECTOR_S22) = s22
          y(l+STATE_VECTOR_S23) = s23
          y(l+STATE_VECTOR_S33) = s33

          ! strain components e22, e23 and e33
          y(l+STATE_VECTOR_E22) = 0._8
          y(l+STATE_VECTOR_E23) = 0._8
          y(l+STATE_VECTOR_E33) = 0._8

          ! initial Kelvin strain
          y(l+STATE_VECTOR_EK22) = s22/(2._8*volume%nGk)
          y(l+STATE_VECTOR_EK23) = s23/(2._8*volume%nGk)
          y(l+STATE_VECTOR_EK33) = s33/(2._8*volume%nGk)
          
          l=l+in%dVolume

       CASE DEFAULT
          WRITE(STDERR,'("wrong case: this is a bug.")')
          WRITE_DEBUG_INFO(-1)
          STOP 2
       END SELECT

    END DO

  END SUBROUTINE initStateVector

  !-----------------------------------------------------------------------
  !> subroutine odefun
  ! evalutes the derivative of the state vector
  !
  ! @param n - number of state elements own by current thread
  ! @param m - degrees of freedom
  !
  ! DESCRIPTION:
  !   1- extract slip velocity and strain rate from state vector
  !   2- calculate the rate of traction and rate of stress
  !   3- calculate the rate of remaining state variables
  !----------------------------------------------------------------------
  SUBROUTINE odefun(n,time,y,dydt)

    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: n
    REAL*8, INTENT(IN)    :: time
    REAL*8, INTENT(IN)    :: y(n)
    REAL*8, INTENT(INOUT) :: dydt(n)

    INTEGER :: i,j,k,l,ierr
    INTEGER :: elementType,elementIndex
    TYPE(PATCH_ELEMENT_STRUCT) :: patch
    TYPE(VOLUME_ELEMENT_STRUCT) :: volume

    ! friction
    REAL*8 :: friction

    ! take-off angle
    REAL*8 :: beta

    ! isotropic strain
    REAL*8 :: ekk

    ! isotropic stress
    REAL*8 :: p

    ! anelastic strain components
    REAL*8 :: e22,e23,e33

    ! Kelvin strain rate
    REAL*8 :: ek22,ek23,ek33

    ! stress components
    REAL*8 :: s11,s22,s23,s33

    ! Kelvin internal stress components
    REAL*8 :: sk22,sk23,sk33

    ! deviatoric stress components
    REAL*8 :: s11p,s22p,s33p

    ! norm of strain tensor
    REAL*8 :: eII

    ! norm of Kelvin strain-rate
    REAL*8 :: ek

    ! norm of stress tensor
    REAL*8 :: sII

    ! norm of Kelvin effective stress
    REAL*8 :: q

    ! scalar rate of shear traction
    REAL*8 :: dtau

    ! slip velocity in the dip direction
    REAL*8 :: velocity

    ! modifier for the arcsinh form of rate-state friction
    REAL*8 :: reg

    ! normal stress
    REAL*8 :: sigma

    ! Poisson s ratio
    REAL*8 :: nu

    nu = in%lambda/(in%lambda+in%mu)/2._8

    ! zero out rate of state vector
    dydt=0d0

    ! maximum strain rate
    eMax=0._8

    ! maximum velocity
    vMax=0._8

    ! initialize moment-rate
    momentRate=0._8

    !--------------------------------------------------------------------
    ! step 1/3 - extract slip velocity and strain rate from state vector
    !--------------------------------------------------------------------

    ! element index in v vector
    k=1
    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementType= layout%elementType(i)
       elementIndex=layout%elementIndex(i)

       SELECT CASE (elementType)
       CASE (FLAG_PATCH)
          ! v(k:k+layout%elementVelocityDGF(i)-1) = slip velocity

          patch=in%patch%s(elementIndex)

          ! take-off angle
          beta=in%patch%beta(elementIndex)

          ! slip velocity
          velocity=EXP(y(l+STATE_VECTOR_VELOCITY)*lg10)

          ! update state vector (rate of slip)
          dydt(l+STATE_VECTOR_SLIP_DIP)=velocity

          v(k)=velocity-patch%Vl

          l=l+in%dPatch

       CASE (FLAG_RECTANGLE_VOLUME,FLAG_TRIANGLE_VOLUME)
          ! v(k:k+layout%elementVelocityDGF(i)-1)= strain rate

          IF (elementType .EQ. FLAG_RECTANGLE_VOLUME) THEN
             volume=in%rectangle%s(elementIndex)
          ELSE
             volume=in%triangle%s(elementIndex)
          END IF

          ! stress components
          s22=y(l+STATE_VECTOR_S22)
          s23=y(l+STATE_VECTOR_S23)
          s33=y(l+STATE_VECTOR_S33)

          ! plane strain
          s11=nu*(s22+s33)

          ! isotropic stress
          p=(s11+s22+s33)/3._8

          ! deviatoric stress
          s11p=s11-p
          s22p=s22-p
          s33p=s33-p

          ! deviatoric stress
          sII=SQRT((s11p**2+s22p**2+2._8*s23**2+s33p**2)/2._8)

          ! strain rate from nonlinear viscosity in Maxwell element
          eII=volume%ngammadot0m*sII**(volume%npowerm-1)

          ! anelastic strain rate components
          e22=eII*s22p
          e23=eII*s23
          e33=eII*s33p
 
          ! Kelvin strain
          ek22=y(l+STATE_VECTOR_EK22)
          ek23=y(l+STATE_VECTOR_EK23)
          ek33=y(l+STATE_VECTOR_EK33)

          ! isotropic component
          ekk=(ek22+ek33)/3._8

          ! internal stress
          sk22=2._8*volume%nGk*(ek22-ekk)
          sk23=2._8*volume%nGk*(ek23    )
          sk33=2._8*volume%nGk*(ek33-ekk)

          ! norm of effective Kelvin stress
          q=SQRT(((s22p-sk22)**2+2._8*(s23-sk23)**2+(s33p-sk33)**2)/2._8)

          ! strain rate from nonlinear viscosity in Kelvin element
          ek=volume%ngammadot0k*q**(volume%npowerk-1)

          ! Kelvin strain-rate component
          ek22=ek*(s22p-sk22)
          ek23=ek*(s23 -sk23)
          ek33=ek*(s33p-sk33)

          ! update state vector (Kelvin strain)
          dydt(l+STATE_VECTOR_EK22)=ek22
          dydt(l+STATE_VECTOR_EK23)=ek23
          dydt(l+STATE_VECTOR_EK33)=ek33

          ! total strain-rate in Burgers assembly
          e22=e22+ek22
          e23=e23+ek23
          e33=e33+ek33

          ! update state vector (total anelastic strain)
          dydt(l+STATE_VECTOR_E22)=e22
          dydt(l+STATE_VECTOR_E23)=e23
          dydt(l+STATE_VECTOR_E33)=e33

          ! maximum strain rate
          eMax=MAX(eMax,SQRT((e22**2+2._8*e23**2+e33**2)/2._8))

          v(k:k+layout%elementVelocityDGF(i)-1)=(/ &
                  (e22-volume%e22), &
                  (e23-volume%e23), &
                  (e33-volume%e33) /)

          l=l+in%dVolume

       CASE DEFAULT
          WRITE(STDERR,'("wrong case: this is a bug.")')
          WRITE_DEBUG_INFO(-1)
          STOP 2
       END SELECT

       k=k+layout%elementVelocityDGF(i)
    END DO

    ! all threads gather velocity from all threads
    CALL MPI_ALLGATHERV(v,layout%listVelocityN(1+rank),MPI_REAL8, &
                        vAll,layout%listVelocityN,layout%listVelocityOffset,MPI_REAL8, &
                        MPI_COMM_WORLD,ierr)

    !-----------------------------------------------------------------
    ! step 2/3 - calculate the rate of traction and rate of stress
    !-----------------------------------------------------------------

#ifdef __BLAS__
    ! use the BLAS library to compute the matrix vector product
    CALL DGEMV("T",SIZE(G,1),SIZE(G,2), &
                1d0,G,SIZE(G,1),vAll,1,0d0,t,1)
#else
    ! slower, Fortran intrinsic
    t=MATMUL(TRANSPOSE(G),vAll)
#endif

    !-----------------------------------------------------------------
    ! step 3/3 - calculate the rate of remaining state variables
    !-----------------------------------------------------------------

    ! element index in t vector
    j=1
    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementType= layout%elementType(i)
       elementIndex=layout%elementIndex(i)
       
       SELECT CASE (elementType)
       CASE (FLAG_PATCH)

          patch=in%patch%s(elementIndex)

          ! slip velocity
          velocity=EXP(y(l+STATE_VECTOR_VELOCITY)*lg10)

          ! moment-rate
          momentRate=momentRate+(velocity-patch%Vl)*in%mu*in%patch%width(elementIndex)

          ! maximum velocity
          vMax=MAX(velocity,vMax)

          ! rate of state
          SELECT CASE(evolutionLawType)
          CASE(1)
             ! isothermal, isobaric aging law
             dydt(l+STATE_VECTOR_STATE_1)=(EXP(-y(l+STATE_VECTOR_STATE_1)*lg10)-velocity/patch%L)/lg10
          CASE(2)
             ! isothermal, isobaric slip law
             dydt(l+STATE_VECTOR_STATE_1)=(-velocity/patch%L*(LOG(velocity/patch%L)+y(l+STATE_VECTOR_STATE_1)*lg10))
          CASE DEFAULT
             WRITE (0,'("unhandled option ", a, " (this is a bug")') evolutionLawType
             WRITE_DEBUG_INFO(100)
             STOP 3
          END SELECT

          ! scalar rate of shear traction
          dtau=COS(beta)*t(j+TRACTION_VECTOR_DIP)+SIN(beta)*t(j+TRACTION_VECTOR_NORMAL)
       
          ! normal stress
          IF (0 .EQ. beta) THEN
             sigma=patch%sig-y(l+STATE_VECTOR_TRACTION_NORMAL)
          ELSE
             sigma=patch%sig
          END IF


          SELECT CASE(frictionLawType)
          CASE(1)
             ! multiplicative form of rate-state friction (Barbot, 2019)
             IF (0 .EQ. beta) THEN
                friction=patch%mu0*exp(patch%a/patch%mu0*log(velocity/patch%Vo) &
                                      +patch%b/patch%mu0*(y(l+STATE_VECTOR_STATE_1)*lg10+log(patch%Vo/patch%L)))
             ELSE
                friction=0._8
             END IF


             ! acceleration
             dydt(l+STATE_VECTOR_VELOCITY)= &
                  (dtau-patch%b*y(l+STATE_VECTOR_TRACTION_DIP)/patch%mu0*dydt(l+STATE_VECTOR_STATE_1)*lg10 &
                                             +friction*t(j+TRACTION_VECTOR_NORMAL)) / &
                  (patch%a*y(l+STATE_VECTOR_TRACTION_DIP)/patch%mu0+patch%damping*velocity) / lg10
          CASE(2)
             ! additive form of rate-state friction (Ruina, 1983)
             IF (0 .EQ. beta) THEN
                friction=patch%mu0+patch%a*log(velocity/patch%Vo) &
                                  +patch%b*log(exp(y(l+STATE_VECTOR_STATE_1)*lg10)/patch%L*patch%Vo)
             ELSE
                friction=0.d0
             END IF


             ! acceleration
             dydt(l+STATE_VECTOR_VELOCITY)=(dtau-patch%b*sigma*dydt(l+STATE_VECTOR_STATE_1)*lg10 &
                                             +friction*t(j+TRACTION_VECTOR_NORMAL)) / &
                                           (patch%a*sigma+patch%damping*velocity) / lg10
          CASE(3)
             ! arcsinh form of rate-state friction (Rice & Benzion, 1996)
             reg=2.0d0*patch%Vo/velocity*exp(-(patch%mu0+patch%b*(y(STATE_VECTOR_STATE_1)*lg10-log(patch%L/patch%Vo)))/patch%a)
   
             IF (0 .EQ. beta) THEN
                friction=patch%a*ASINH(1._8/reg)
             ELSE
                friction=0.d0
             END IF


             reg=1d0/SQRT(1._8+reg**2)
             dydt(l+STATE_VECTOR_VELOCITY)= &
                     (dtau-patch%b*patch%sig*dydt(l+STATE_VECTOR_STATE_1)*lg10*reg &
                                             +friction*t(j+TRACTION_VECTOR_NORMAL)) / &
                     (patch%a*patch%sig*reg+patch%damping*velocity) / lg10
          CASE DEFAULT
             WRITE (0,'("unhandled option ", a, " (this is a bug")') frictionLawType
             WRITE_DEBUG_INFO(100)
             STOP 3
          END SELECT

          ! rate of shear traction
          dydt(l+STATE_VECTOR_TRACTION_DIP)=dtau-patch%damping*velocity*dydt(l+STATE_VECTOR_VELOCITY)*lg10

          ! rate of normal traction
          dydt(l+STATE_VECTOR_TRACTION_NORMAL)=t(j+TRACTION_VECTOR_NORMAL)

          l=l+in%dPatch

       CASE (FLAG_RECTANGLE_VOLUME,FLAG_TRIANGLE_VOLUME)

          IF (elementType .EQ. FLAG_RECTANGLE_VOLUME) THEN
             volume=in%rectangle%s(elementIndex)
          ELSE
             volume=in%triangle%s(elementIndex)
          END IF

          ! stress rate
          dydt(l+STATE_VECTOR_S22)=t(j+TRACTION_VECTOR_S22)
          dydt(l+STATE_VECTOR_S23)=t(j+TRACTION_VECTOR_S23)
          dydt(l+STATE_VECTOR_S33)=t(j+TRACTION_VECTOR_S33)

          l=l+in%dVolume

       CASE DEFAULT
          WRITE(STDERR,'("wrong case: this is a bug.")')
          WRITE_DEBUG_INFO(-1)
          STOP 2
       END SELECT

       j=j+layout%elementForceDGF(i)

    END DO

  END SUBROUTINE odefun

  !-----------------------------------------------------------------------
  !> subroutine initParallelism()
  !! initialize variables describe the data layout
  !! for parallelism.
  !!
  !! OUTPUT:
  !! layout    - list of receiver type and type index
  !-----------------------------------------------------------------------
  SUBROUTINE initParallelism(in,layout)
    IMPLICIT NONE
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in
    TYPE(LAYOUT_STRUCT), INTENT(OUT) :: layout

    ! MPI rank and size
    INTEGER :: rank,csize

    ! error flag
    INTEGER :: ierr

    INTEGER :: i,j,k,n,remainder,cumulativeIndex
    INTEGER :: nElements,nColumns
    INTEGER :: buffer

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

    ! total number of elements
    nElements=in%patch%ns+in%rectangle%ns+in%triangle%ns

    ! list of number of elements in thread
    ALLOCATE(layout%listElements(csize), &
             layout%listOffset(csize),STAT=ierr)
    IF (ierr>0) STOP "could not allocate the list"

    remainder=nElements-INT(nElements/csize)*csize
    IF (0 .LT. remainder) THEN
       layout%listElements(1:(csize-remainder))      =INT(nElements/csize)
       layout%listElements((csize-remainder+1):csize)=INT(nElements/csize)+1
    ELSE
       layout%listElements(1:csize)=INT(nElements/csize)
    END IF

    ! element start index in thread
    j=0
    k=0
    DO i=1,csize
       j=k+1
       k=k+layout%listElements(i)
       layout%listOffset(i)=j
    END DO

    ALLOCATE(layout%elementType       (layout%listElements(1+rank)), &
             layout%elementIndex      (layout%listElements(1+rank)), &
             layout%elementStateIndex (layout%listElements(1+rank)), &
             layout%elementVelocityDGF(layout%listElements(1+rank)), &
             layout%elementStateDGF   (layout%listElements(1+rank)), &
             layout%elementForceDGF   (layout%listElements(1+rank)),STAT=ierr)
    IF (ierr>0) STOP "could not allocate the layout elements"

    j=1
    cumulativeIndex=0
    DO i=1,in%patch%ns
       IF ((i .GE. layout%listOffset(1+rank)) .AND. &
           (i .LT. (layout%listOffset(1+rank)+layout%listElements(1+rank)))) THEN
          layout%elementType(j)=FLAG_PATCH
          layout%elementIndex(j)=i
          layout%elementStateIndex(j)=cumulativeIndex+STATE_VECTOR_DGF_PATCH
          cumulativeIndex=layout%elementStateIndex(j)
          layout%elementVelocityDGF(j)=DGF_PATCH
          layout%elementStateDGF(j)=in%dPatch
          layout%elementForceDGF(j)=DGF_VECTOR
          j=j+1
       END IF
    END DO

    DO i=1,in%rectangle%ns
       IF (((i+in%patch%ns) .GE. layout%listOffset(1+rank)) .AND. &
           ((i+in%patch%ns) .LT. (layout%listOffset(1+rank)+layout%listElements(1+rank)))) THEN
          layout%elementType(j)=FLAG_RECTANGLE_VOLUME
          layout%elementIndex(j)=i
          layout%elementStateIndex(j)=cumulativeIndex+STATE_VECTOR_DGF_VOLUME
          cumulativeIndex=layout%elementStateIndex(j)
          layout%elementVelocityDGF(j)=DGF_VOLUME
          layout%elementStateDGF(j)=in%dVolume
          layout%elementForceDGF(j)=DGF_TENSOR
          j=j+1
       END IF
    END DO
    DO i=1,in%triangle%ns
       IF (((i+in%patch%ns+in%rectangle%ns) .GE. layout%listOffset(1+rank)) .AND. &
           ((i+in%patch%ns+in%rectangle%ns) .LT. (layout%listOffset(1+rank)+layout%listElements(1+rank)))) THEN
          layout%elementType(j)=FLAG_TRIANGLE_VOLUME
          layout%elementIndex(j)=i
          layout%elementStateIndex(j)=cumulativeIndex+STATE_VECTOR_DGF_VOLUME
          cumulativeIndex=layout%elementStateIndex(j)
          layout%elementVelocityDGF(j)=DGF_VOLUME
          layout%elementStateDGF(j)=in%dVolume
          layout%elementForceDGF(j)=DGF_TENSOR
          j=j+1
       END IF
    END DO

    ALLOCATE(layout%listVelocityN(csize), &
             layout%listVelocityOffset(csize), &
             layout%listStateN(csize), &
             layout%listStateOffset(csize), &
             layout%listForceN(csize), &
             STAT=ierr)
    IF (ierr>0) STOP "could not allocate the size list"

    ! share number of elements in threads
    CALL MPI_ALLGATHER(SUM(layout%elementVelocityDGF),1,MPI_INTEGER,layout%listVelocityN,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
    CALL MPI_ALLGATHER(SUM(layout%elementStateDGF),   1,MPI_INTEGER,layout%listStateN,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
    CALL MPI_ALLGATHER(SUM(layout%elementForceDGF),   1,MPI_INTEGER,layout%listForceN,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)

    j=0
    k=0
    DO i=1,csize
       j=k+1
       k=k+layout%listVelocityN(i)
       layout%listVelocityOffset(i)=j-1
    END DO

    j=0
    k=0
    DO i=1,csize
       j=k+1
       k=k+layout%listStateN(i)
       layout%listStateOffset(i)=j-1
    END DO

  END SUBROUTINE initParallelism

  !-----------------------------------------------------------------------
  !> subroutine initGeometry
  ! initializes the position and local reference system vectors
  !
  ! INPUT:
  ! @param in      - input parameters data structure
  !-----------------------------------------------------------------------
  SUBROUTINE initGeometry(in)
    USE planestrain
    USE types_ps
    TYPE(SIMULATION_STRUCT), INTENT(INOUT) :: in
  
    IF (0 .LT. in%patch%ns) THEN
       CALL computeReferenceSystemPlaneStrain( &
                in%patch%ns, &
                in%patch%x, &
                in%patch%width, &
                in%patch%dip, &
                in%patch%sv, &
                in%patch%dv, &
                in%patch%nv, &
                in%patch%xc)
    END IF

    IF (0 .LT. in%rectangle%ns) THEN
       CALL computeReferenceSystemPlaneStrain( &
                in%rectangle%ns, &
                in%rectangle%x, &
                in%rectangle%width, &
                in%rectangle%dip, &
                in%rectangle%sv, &
                in%rectangle%dv, &
                in%rectangle%nv, &
                in%rectangle%xc)
    END IF

  END SUBROUTINE initGeometry

  !---------------------------------------------------------------------
  !> subroutine init
  !! reads simulation parameters from the standard input and initialize
  !! model parameters.
  !!
  !! INPUT:
  !! @param unit - the unit number used to read input data
  !!
  !! OUTPUT:
  !! @param in
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !---------------------------------------------------------------------
  SUBROUTINE init(in)
    USE types_ps
    USE getopt_m
  
    TYPE(SIMULATION_STRUCT), INTENT(OUT) :: in
  
    CHARACTER :: ch
    CHARACTER(512) :: dataline
    CHARACTER(256) :: filename
    INTEGER :: iunit,noptions
!$  INTEGER :: omp_get_num_procs,omp_get_max_threads
    TYPE(OPTION_S) :: opts(13)
  
    INTEGER :: k,ierr,i,rank,size,position
    INTEGER :: maxVertexIndex=-1
    INTEGER, PARAMETER :: psize=1024
    INTEGER :: dummy
    CHARACTER, DIMENSION(psize) :: packed

    INTEGER :: nObservationPatch,nObservationVolume
    INTEGER, DIMENSION(:,:), ALLOCATABLE :: observationPatch,observationVolume
  
    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,size,ierr)
  
    ! define long options, such as --dry-run
    ! parse the command line for options
    opts( 1)=OPTION_S("version",.FALSE.,CHAR(21))
    opts( 2)=OPTION_S("dry-run",.FALSE.,CHAR(22))
    opts( 3)=OPTION_S("epsilon",.TRUE.,'e')
    opts( 4)=OPTION_S("export-greens",.TRUE.,'g')
    opts( 5)=OPTION_S("export-netcdf",.FALSE.,'n')
    opts( 6)=OPTION_S("export-surface",.FALSE.,'l')
    opts( 7)=OPTION_S("export-volume",.FALSE.,'v')
    opts( 8)=OPTION_S("friction-law",.TRUE.,'f')
    opts( 9)=OPTION_S("evolution-law",.TRUE.,'j')
    opts(10)=OPTION_S("import-greens",.TRUE.,'p')
    opts(11)=OPTION_S("maximum-step",.TRUE.,'m')
    opts(12)=OPTION_S("maximum-iterations",.TRUE.,'i')
    opts(13)=OPTION_S("help",.FALSE.,'h')
  
    noptions=0;
    DO
       ch=getopt("he:f:g:i:j:m:n",opts)
       SELECT CASE(ch)
       CASE(CHAR(0))
          EXIT
       CASE(CHAR(21))
          ! option version
          in%isversion=.TRUE.
       CASE(CHAR(22))
          ! option dry-run
          in%isdryrun=.TRUE.
       CASE('e')
          ! numerical accuracy (variable epsilon sits in the ode45 module)
          READ(optarg,*) epsilon
          noptions=noptions+1
       CASE('f')
          ! type of friction law
          READ(optarg,*) frictionLawType
          noptions=noptions+1
       CASE('g')
          ! export Greens functions to netcdf file
          READ(optarg,'(a)') in%greensFunctionDirectory
          in%isExportGreens=.TRUE.
          noptions=noptions+1
       CASE('i')
          ! maximum iterations
          READ(optarg,*) maximumIterations
          noptions=noptions+1
       CASE('j')
          ! type of evolution law
          READ(optarg,*) evolutionLawType
          noptions=noptions+1
       CASE('m')
          ! maximum time step (variable maximumTimeStep sits in the ode45 module)
          READ(optarg,*) maximumTimeStep
          noptions=noptions+1
       CASE('n')
          ! export in netcdf format
          in%isExportNetcdf=.TRUE.
       CASE('l')
          ! export surface elements in GMT-compatible .xy files
          in%isexportsurface=.TRUE.
       CASE('v')
          ! export volume elements in GMT-compatible .xy files
          in%isexportvolume=.TRUE.
       CASE('p')
          ! import Greens functions from netcdf file
          READ(optarg,'(a)') in%greensFunctionDirectory
          in%isImportGreens=.TRUE.
          noptions=noptions+1
       CASE('h')
          ! option help
          in%ishelp=.TRUE.
       CASE('?')
          WRITE_DEBUG_INFO(100)
          in%ishelp=.TRUE.
          EXIT
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug")') optopt
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT
       noptions=noptions+1
    END DO
  
    IF (in%isversion) THEN
       CALL printversion()
       ! abort parameter input
       CALL MPI_FINALIZE(ierr)
       STOP
    END IF
  
    IF (in%ishelp) THEN
       CALL printhelp()
       ! abort parameter input
       CALL MPI_FINALIZE(ierr)
       STOP
    END IF
  
    ! number of fault patches
    in%nPatch=0
    ! number of dynamic variables for patches
    in%dPatch=STATE_VECTOR_DGF_PATCH
    ! number of volume elements
    in%nVolume=0
    ! number of dynamic variables for volume elements (s22,s23,s33,e22,e23,e33)
    in%dVolume=STATE_VECTOR_DGF_VOLUME
    in%patch%ns=0
    ! number of rectangle volumes
    in%rectangle%ns=0
    ! number of triangle volumes
    in%triangle%ns=0
    ! number of vertices
    in%triangle%nVe=0
  
    IF (0 .EQ. rank) THEN
       PRINT 2000
       PRINT '("# VISCOUSCYCLES")'
       PRINT '("# quasi-dynamic earthquake simulation in viscoelastic medium")'
       PRINT '("# in condition of in-plane strain with the radiation damping")'
       PRINT '("# approximation.")'
       SELECT CASE(frictionLawType)
       CASE(1)
          PRINT '("# friction law: multiplicative form of rate-state friction (Barbot, 2018)")'
       CASE(2)
          PRINT '("# friction law: additive form of rate-state friction (Ruina, 1983)")'
       CASE(3)
          PRINT '("# friction law: arcsinh form of rate-state friction (Rice & Benzion, 1996)")'
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug")') frictionLawType
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT
       SELECT CASE(evolutionLawType)
       CASE(1)
          PRINT '("# evolution law: isothermal, isobaric additive form (Ruina, 1983)")'
       CASE(2)
          PRINT '("# evolution law: isothermal, isobaric multiplicative form (Ruina, 1983)")'
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug")') evolutionLawType
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT
       PRINT '("# numerical accuracy: ",ES11.4)', epsilon
       PRINT '("# maximum iterations: ",I11)', maximumIterations
       PRINT '("# maximum time step: ",ES12.4)', maximumTimeStep
       PRINT '("# number of threads: ",I12)', csize
       IF (in%isExportNetcdf) THEN
          PRINT '("# export velocity to netcdf:  yes")'
       ELSE
          PRINT '("# export velocity to netcdf:   no")'
       END IF
       IF (in%isExportVolume) THEN
          PRINT '("# export plastic strain-rate to .xy files:  yes")'
       ELSE
          PRINT '("# export plastic strain-rate to .xy files:   no")'
       END IF
       IF (in%isImportGreens) THEN
          WRITE (STDOUT,'("# import greens function:         yes (",a,")")') TRIM(in%greensFunctionDirectory)
       ELSE
          IF (in%isExportGreens) THEN
             WRITE (STDOUT,'("# export greens function:         yes (",a,")")') TRIM(in%greensFunctionDirectory)
          END IF
       END IF
!$     PRINT '("#     * parallel OpenMP implementation with ",I3.3,"/",I3.3," threads")', &
!$                omp_get_max_threads(),omp_get_num_procs()
       PRINT 2000
  
       IF (noptions .LT. COMMAND_ARGUMENT_COUNT()) THEN
          ! read from input file
          iunit=25
          CALL GET_COMMAND_ARGUMENT(noptions+1,filename)
          OPEN (UNIT=iunit,FILE=filename,IOSTAT=ierr)
       ELSE
          ! get input parameters from standard input
          iunit=5
       END IF
  
       PRINT '("# output directory")'
       CALL getdata(iunit,dataline)
       READ (dataline,'(a)') in%wdir
       PRINT '(2X,a)', TRIM(in%wdir)
  
       in%timeFilename=TRIM(in%wdir)//"/time.dat"
  
       ! test write permissions on output directory
       OPEN (UNIT=FPTIME,FILE=in%timeFilename,POSITION="APPEND",&
               IOSTAT=ierr,FORM="FORMATTED")
       IF (ierr>0) THEN
          WRITE_DEBUG_INFO(102)
          WRITE (STDERR,'("error: unable to access ",a)') TRIM(in%timefilename)
          STOP 1
       END IF
       CLOSE(FPTIME)
     
       PRINT '("# rigidity, Lame parameter")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%mu,in%lambda
       PRINT '(2ES9.2E1)', in%mu,in%lambda
  
       IF (0 .GT. in%mu) THEN
          WRITE_DEBUG_INFO(-1)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("input error: shear modulus must be positive")')
          STOP 2
       END IF
  
       PRINT '("# time interval")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%interval
       PRINT '(ES20.12E2)', in%interval
  
       IF (in%interval .LE. 0d0) THEN
          WRITE (STDERR,'("**** error **** ")')
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("simulation time must be positive. exiting.")')
          STOP 1
       END IF
  
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !                   P A T C H E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT '("# number of patches")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%patch%ns
       PRINT '(I5)', in%patch%ns
       IF (in%patch%ns .GT. 0) THEN
          ALLOCATE(in%patch%s(in%patch%ns), &
                   in%patch%x(3,in%patch%ns), &
                   in%patch%xc(3,in%patch%ns), &
                   in%patch%width(in%patch%ns), &
                   in%patch%dip(in%patch%ns), &
                   in%patch%beta(in%patch%ns), &
                   in%patch%sv(3,in%patch%ns), &
                   in%patch%dv(3,in%patch%ns), &
                   in%patch%nv(3,in%patch%ns),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the patch list"
          PRINT 2000
          PRINT '("#  n        Vl            x2            x3   width     dip      beta")'
          PRINT 2000
          DO k=1,in%patch%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                  in%patch%s(k)%Vl, &
                  in%patch%x(2,k), &
                  in%patch%x(3,k), &
                  in%patch%width(k), &
                  in%patch%dip(k), &
                  in%patch%beta(k)
   
             PRINT '(I4,ES10.2E2,2ES14.7E1,2ES8.2E1,ES10.4E1)',i, &
                  in%patch%s(k)%Vl, &
                  in%patch%x(2,k), &
                  in%patch%x(3,k), &
                  in%patch%width(k), &
                  in%patch%dip(k), &
                  in%patch%beta(k)
                
             ! convert to radians
             in%patch%dip(k)=in%patch%dip(k)*DEG2RAD     
             in%patch%beta(k)=in%patch%beta(k)*DEG2RAD

             IF (i .NE. k) THEN
                WRITE (STDERR,'("invalid patch definition")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
             IF (in%patch%width(k) .LE. 0d0) THEN
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: patch width must be positive.")')
                STOP 1
             END IF

          END DO
   
          ! export the patches
          filename=TRIM(in%wdir)//"/rfaults.flt.2d"
          OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          WRITE (FPOUT,'("#  n        Vl            x2            x3   width     dip      beta")')
          DO k=1,in%patch%ns
             WRITE (FPOUT,'(I4,ES10.2E2,2ES14.7E1,2ES8.2E1,ES10.4E1)') k, &
               in%patch%s(k)%Vl, &
               in%patch%x(2,k), &
               in%patch%x(3,k), &
               in%patch%width(k), &
               in%patch%dip(k), &
               in%patch%beta(k)
          END DO
          CLOSE(FPOUT)
                
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          !        F R I C T I O N   P R O P E R T I E S
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          PRINT 2000
          PRINT '("# number of frictional patches")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) dummy
          PRINT '(I5)', dummy
          IF (dummy .NE. in%patch%ns) THEN
             WRITE_DEBUG_INFO(-1)
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE(STDERR,'("input error: all patches require frictional properties")')
             STOP 2
          END IF
          PRINT '("#  n     tau0      mu0      sig        a        b        L       Vo  G/(2Vs)")'
          PRINT 2000
          DO k=1,in%patch%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                   in%patch%s(k)%tau0, &
                   in%patch%s(k)%mu0, &
                   in%patch%s(k)%sig, &
                   in%patch%s(k)%a, &
                   in%patch%s(k)%b, &
                   in%patch%s(k)%L, &
                   in%patch%s(k)%Vo, &
                   in%patch%s(k)%damping
   
             PRINT '(I4,8ES9.2E1)',i, &
                  in%patch%s(k)%tau0, &
                  in%patch%s(k)%mu0, &
                  in%patch%s(k)%sig, &
                  in%patch%s(k)%a, &
                  in%patch%s(k)%b, &
                  in%patch%s(k)%L, &
                  in%patch%s(k)%Vo, &
                  in%patch%s(k)%damping
                
             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'("invalid friction property definition for patch")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO
   
       END IF
          
       ! - - - - - - - - - - - - - - - - - - - - - - - - - - -
       !  R E C T A N G L E   V O L U M E S   E L E M E N T S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT '("# number of rectangle volume elements")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%rectangle%ns
       PRINT '(I5)', in%rectangle%ns
       IF (in%rectangle%ns .GT. 0) THEN
          ALLOCATE(in%rectangle%s(in%rectangle%ns), &
                   in%rectangle%x(3,in%rectangle%ns), &
                   in%rectangle%xc(3,in%rectangle%ns), &
                   in%rectangle%width(in%rectangle%ns), &
                   in%rectangle%thickness(in%rectangle%ns), &
                   in%rectangle%dip(in%rectangle%ns), &
                   in%rectangle%sv(3,in%rectangle%ns), &
                   in%rectangle%dv(3,in%rectangle%ns), &
                   in%rectangle%nv(3,in%rectangle%ns),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the volume element list"
          PRINT 2000
          PRINT '("#  n       e22       e23       e33       x2       x3   ", &
                & "width thickness  dip")'
          PRINT 2000
          DO k=1,in%rectangle%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                  in%rectangle%s(k)%e22, &
                  in%rectangle%s(k)%e23, &
                  in%rectangle%s(k)%e33, &
                  in%rectangle%x(2,k), &
                  in%rectangle%x(3,k), &
                  in%rectangle%width(k), &
                  in%rectangle%thickness(k), &
                  in%rectangle%dip(k)
   
             PRINT '(I4.4,3ES10.2E2,2ES9.2E1,2ES8.2E1,f7.1)',i, &
                  in%rectangle%s(k)%e22, &
                  in%rectangle%s(k)%e23, &
                  in%rectangle%s(k)%e33, &
                  in%rectangle%x(2,k), &
                  in%rectangle%x(3,k), &
                  in%rectangle%width(k), &
                  in%rectangle%thickness(k), &
                  in%rectangle%dip(k)
                
             ! convert to radians
             in%rectangle%dip(k)=in%rectangle%dip(k)*DEG2RAD     

             IF (0 .GT. in%rectangle%x(3,k)) THEN
                WRITE_DEBUG_INFO(-1)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("input error: depth must be positive")')
                STOP 2
             END IF

             IF (i .NE. k) THEN
                WRITE (STDERR,'("invalid volume element definition")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("input error: unexpected index")')
                STOP 1
             END IF
             IF (MIN(in%rectangle%width(k),in%rectangle%thickness(k)) .LE. 0d0) THEN
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("input error: volume element dimension must be positive.")')
                STOP 1
             END IF
                
          END DO
   
          ! export the rectangle volume elements
          filename=TRIM(in%wdir)//"/rvolume.shz.2d"
          OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          WRITE (FPOUT,'("#  n       e22       e23       e33       x2       x3   ", &
             & "width thickness  dip")')
          DO k=1,in%rectangle%ns
             WRITE (FPOUT,'(I4.4,3ES10.2E2,2ES9.2E1,2ES8.2E1,f7.1)') k, &
               in%rectangle%s(k)%e22, &
               in%rectangle%s(k)%e23, &
               in%rectangle%s(k)%e33, &
               in%rectangle%x(2,k), &
               in%rectangle%x(3,k), &
               in%rectangle%width(k), &
               in%rectangle%thickness(k), &
               in%rectangle%dip(k)
          END DO
          CLOSE(FPOUT)
                
          ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
          !   N O N L I N E A R   M A X W E L L   P R O P E R T I E S
          ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
          PRINT 2000
          PRINT '("# number of nonlinear Maxwell volume elements")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) in%rectangle%nNonlinearMaxwell
          PRINT '(I5)', in%rectangle%nNonlinearMaxwell
          IF (0 .NE. in%rectangle%nNonlinearMaxwell) THEN
             IF (in%rectangle%ns .NE. in%rectangle%nNonlinearMaxwell) THEN
                WRITE_DEBUG_INFO(-1)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE(STDERR,'("input error: nonlinear Maxwell properties ", &
                             & "are for none or all volume elements")')
                STOP 2
             END IF

             PRINT '("#  n      sII   gammadot0m         n")'
             PRINT 2000
             DO k=1,in%rectangle%ns
                CALL getdata(iunit,dataline)
                READ (dataline,*,IOSTAT=ierr) i, &
                      in%rectangle%s(k)%sII, &
                      in%rectangle%s(k)%ngammadot0m, &
                      in%rectangle%s(k)%npowerm
   
                PRINT '(I4.4,ES9.2E1,ES13.6E2,ES10.4E1)',i, &
                      in%rectangle%s(k)%sII, &
                      in%rectangle%s(k)%ngammadot0m, &
                      in%rectangle%s(k)%npowerm
                
                ! set Kelvin properties to default values (no transient)
                in%rectangle%s(k)%ngammadot0k=0._8
                in%rectangle%s(k)%nGk=in%mu
                in%rectangle%s(k)%npowerk=1._8
   
                IF (i .NE. k) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("invalid property definition for volume element")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   WRITE (STDERR,'("error in input file: unexpected index")')
                   STOP 1
                END IF
             END DO

          END IF
   
          ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
          !   N O N L I N E A R   K E L V I N   P R O P E R T I E S
          ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
          PRINT 2000
          PRINT '("# number of nonlinear Kelvin volume elements")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) in%rectangle%nNonlinearKelvin
          PRINT '(I5)', in%rectangle%nNonlinearKelvin
          IF (0 .NE. in%rectangle%nNonlinearKelvin) THEN
             IF (in%rectangle%ns .NE. in%rectangle%nNonlinearKelvin) THEN
                WRITE_DEBUG_INFO(-1)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE(STDERR,'("input error: nonlinear Kelvin properties ", &
                             & "are for none or all volume elements")')
                STOP 2
             END IF

             PRINT '("#  n   gammadot0k           Gk         n")'
             PRINT 2000
             DO k=1,in%rectangle%ns
                CALL getdata(iunit,dataline)
                READ (dataline,*,IOSTAT=ierr) i, &
                      in%rectangle%s(k)%ngammadot0k, &
                      in%rectangle%s(k)%nGk, &
                      in%rectangle%s(k)%npowerk
   
                PRINT '(I4.4,2ES13.6E2,ES10.4E1)',i, &
                      in%rectangle%s(k)%ngammadot0k, &
                      in%rectangle%s(k)%nGk, &
                      in%rectangle%s(k)%npowerk
                
                IF (i .NE. k) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("invalid property definition for volume element")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   WRITE (STDERR,'("error in input file: unexpected index")')
                   STOP 1
                END IF
             END DO

          END IF
   
       END IF
          
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !   T R I A N G L E   V O L U M E   E L E M E N T S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT '("# number of triangle volume elements")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%triangle%ns
       PRINT '(I5)', in%triangle%ns

       IF (in%triangle%ns .GT. 0) THEN
          ALLOCATE(in%triangle%s(in%triangle%ns), &
                   in%triangle%i1(in%triangle%ns), &
                   in%triangle%i2(in%triangle%ns), &
                   in%triangle%i3(in%triangle%ns), &
                   in%triangle%xc(3,in%triangle%ns),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the triangle volume list"
          PRINT 2000
          PRINT '("#   n       e22       e23       e33        i1        i2        i3")'
          PRINT 2000
          DO k=1,in%triangle%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                  in%triangle%s(k)%e22, &
                  in%triangle%s(k)%e23, &
                  in%triangle%s(k)%e33, &
                  in%triangle%i1(k), &
                  in%triangle%i2(k), &
                  in%triangle%i3(k)
   
             PRINT '(I5,3ES10.2E2,3I10)',i, &
                  in%triangle%s(k)%e22, &
                  in%triangle%s(k)%e23, &
                  in%triangle%s(k)%e33, &
                  in%triangle%i1(k), &
                  in%triangle%i2(k), &
                  in%triangle%i3(k)
                
             ! check range of vertex index
             IF (in%triangle%i1(k) .GT. maxVertexIndex) THEN
                maxVertexIndex=in%triangle%i1(k)
             END IF
             IF (in%triangle%i2(k) .GT. maxVertexIndex) THEN
                maxVertexIndex=in%triangle%i2(k)
             END IF
             IF (in%triangle%i3(k) .GT. maxVertexIndex) THEN
                maxVertexIndex=in%triangle%i3(k)
             END IF
             IF ((0 .GT. in%triangle%i1(k)) .OR. &
                 (0 .GT. in%triangle%i2(k)) .OR. &
                 (0 .GT. in%triangle%i3(k))) THEN
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: negative index")')
                STOP 1
             END IF

             IF (i .ne. k) THEN
                WRITE (STDERR,'("error in input file: unexpected index")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("invalid triangular patch definition ")')
                STOP 1
             END IF

          END DO
                
          PRINT '("# number of vertices")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) in%triangle%nVe
          PRINT '(I5)', in%triangle%nVe
          IF (maxVertexIndex .GT. in%triangle%nVe) THEN
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE (STDERR,'("error in input file: not enough vertices")')
             STOP 1
          END IF
          IF (in%triangle%nVe .GT. 0) THEN
             ALLOCATE(in%triangle%v(3,in%triangle%nVe),STAT=ierr)
             IF (ierr>0) STOP "could not allocate the list of vertices"
             PRINT 2000
             PRINT '("#  n           x2           x3")'
             PRINT 2000
             DO k=1,in%triangle%nVe
                CALL getdata(iunit,dataline)
                READ (dataline,*,IOSTAT=ierr) i, &
                      in%triangle%v(2,k), &
                      in%triangle%v(3,k)
   
                PRINT '(I4,2ES13.6E1)',i, &
                     in%triangle%v(2,k), &
                     in%triangle%v(3,k)
                
                IF (i .ne. k) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("invalid vertex definition ")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   WRITE (STDERR,'("error in input file: unexpected index")')
                   STOP 1
                END IF
             END DO
                
          END IF
   
          ! compute the centroid of each triangle volume
          DO k=1,in%triangle%ns
             in%triangle%xc(:,k)=(in%triangle%v(:,in%triangle%i1(k)) &
                                 +in%triangle%v(:,in%triangle%i2(k)) &
                                 +in%triangle%v(:,in%triangle%i3(k)))/3.0d0
          END DO
   
          ! export the triangle volume elements
          filename=TRIM(in%wdir)//"/volume.trv"
          OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          WRITE (FPOUT,'("#   n       e22       e23       e33        i1        i2        i3")')
          DO k=1,in%triangle%ns
             WRITE (FPOUT,'(I5,3ES10.2E2,3I10)') k, &
               in%triangle%s(k)%e22, &
               in%triangle%s(k)%e23, &
               in%triangle%s(k)%e33, &
               in%triangle%i1(k), &
               in%triangle%i2(k), &
               in%triangle%i3(k)
          END DO
          CLOSE(FPOUT)
                
          filename=TRIM(in%wdir)//"/volume.ned"
          OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          WRITE (FPOUT,'("#  n           x2           x3")')
          DO k=1,in%triangle%nVe
             WRITE (FPOUT,'(I4,2ES13.6E1)') k, &
                  in%triangle%v(2,k), &
                  in%triangle%v(3,k)
          END DO
          CLOSE(FPOUT)
                
          ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
          !   N O N L I N E A R   M A X W E L L   P R O P E R T I E S
          ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
          PRINT 2000
          PRINT '("# number of nonlinear Maxwell volume elements")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) in%triangle%nNonlinearMaxwell
          PRINT '(I5)', in%triangle%nNonlinearMaxwell
          IF (0 .NE. in%triangle%nNonlinearMaxwell) THEN
             IF (in%triangle%ns .NE. in%triangle%nNonlinearMaxwell) THEN
                WRITE_DEBUG_INFO(-1)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE(STDERR,'("input error: nonlinear Maxwell properties ", &
                             & "are for none or all triangle volume elements")')
                STOP 2
             END IF

             PRINT '("#  n      sII   gammadot0m         n")'
             PRINT 2000
             DO k=1,in%triangle%ns
                CALL getdata(iunit,dataline)
                READ (dataline,*,IOSTAT=ierr) i, &
                      in%triangle%s(k)%sII, &
                      in%triangle%s(k)%ngammadot0m, &
                      in%triangle%s(k)%npowerm
   
                PRINT '(I4.4,ES9.2E1,ES13.6E2,ES10.4E1)',i, &
                      in%triangle%s(k)%sII, &
                      in%triangle%s(k)%ngammadot0m, &
                      in%triangle%s(k)%npowerm
                
                ! set Kelvin properties to default values (no transient)
                in%triangle%s(k)%ngammadot0k=0._8
                in%triangle%s(k)%nGk=in%mu
                in%triangle%s(k)%npowerk=1._8

                IF (i .NE. k) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("invalid property definition for triangle volume element")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   WRITE (STDERR,'("error in input file: unexpected index")')
                   STOP 1
                END IF
             END DO

          END IF
   
          ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
          !   N O N L I N E A R   K E L V I N   P R O P E R T I E S
          ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
          PRINT 2000
          PRINT '("# number of nonlinear Kelvin volume elements")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) in%triangle%nNonlinearKelvin
          PRINT '(I5)', in%triangle%nNonlinearKelvin
          IF (0 .NE. in%triangle%nNonlinearKelvin) THEN
             IF (in%triangle%ns .NE. in%triangle%nNonlinearKelvin) THEN
                WRITE_DEBUG_INFO(-1)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE(STDERR,'("input error: nonlinear Kelvin properties ", &
                             & "are for none or all triangle volume elements")')
                STOP 2
             END IF

             PRINT '("#  n   gammadot0k           Gk         n")'
             PRINT 2000
             DO k=1,in%triangle%ns
                CALL getdata(iunit,dataline)
                READ (dataline,*,IOSTAT=ierr) i, &
                      in%triangle%s(k)%ngammadot0k, &
                      in%triangle%s(k)%nGk, &
                      in%triangle%s(k)%npowerk
   
                PRINT '(I4.4,2ES13.6E2,ES10.4E1)',i, &
                      in%triangle%s(k)%ngammadot0k, &
                      in%triangle%s(k)%nGk, &
                      in%triangle%s(k)%npowerk
                
                IF (i .NE. k) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("invalid property definition for triangle volume element")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   WRITE (STDERR,'("error in input file: unexpected index")')
                   STOP 1
                END IF
             END DO

          END IF
   
       END IF
          
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !       O B S E R V A T I O N   P A T C H E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of observation patches")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) nObservationPatch
       PRINT '(I5)', nObservationPatch
       IF (0 .LT. nObservationPatch) THEN
          ALLOCATE(observationPatch(2,nObservationPatch),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the observation patches"
          PRINT 2000
          PRINT '("#  n      i rate")'
          PRINT 2000
          DO k=1,nObservationPatch
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i,observationPatch(:,k)

             PRINT '(I4,X,I6,X,I4)',i,observationPatch(:,k)

             IF (0 .GE. observationPatch(2,k)) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: sampling rate must be a strictly positive integer")')
                STOP 1
             END IF
             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO
       END IF

       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !       O B S E R V A T I O N   V O L U M E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of observation volumes")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) nObservationVolume
       PRINT '(I5)', nObservationVolume
       IF (0 .LT. nObservationVolume) THEN
          ALLOCATE(observationVolume(2,nObservationVolume),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the observation volumes"
          PRINT 2000
          PRINT '("#  n      i rate")'
          PRINT 2000
          DO k=1,nObservationVolume
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i,observationVolume(:,k)

             PRINT '(I4.4,X,I6,X,I4)',i,observationVolume(:,k)

             IF (0 .GE. observationVolume(2,k)) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: sampling rate must be strictly positive.")')
                STOP 1
             END IF

             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO
       END IF

       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !        O B S E R V A T I O N   P O I N T S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of observation points")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%nObservationPoint
       PRINT '(I5)', in%nObservationPoint
       IF (0 .LT. in%nObservationPoint) THEN
          ALLOCATE(in%observationPoint(in%nObservationPoint),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the observation points"
          PRINT 2000
          PRINT '("# n name       x2       x3 rate")'
          PRINT 2000
          DO k=1,in%nObservationPoint
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                     in%observationPoint(k)%name, &
                     in%observationPoint(k)%x(2), &
                     in%observationPoint(k)%x(3), &
                     in%observationPoint(k)%rate

             ! set along-strike coordinate to zero for 2d solutions
             in%observationPoint(k)%x(1)=0._8

             PRINT '(I3.3,X,a4,2ES9.2E1,X,I4)',i, &
                     in%observationPoint(k)%name, &
                     in%observationPoint(k)%x(2), &
                     in%observationPoint(k)%x(3), &
                     in%observationPoint(k)%rate

             IF (0 .GE. in%observationPoint(k)%rate) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: sampling rate must be a strictly positive integer")')
                STOP 1
             END IF

             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO

          filename=TRIM(in%wdir)//"/opts.dat"
          OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          WRITE (FPOUT,'("#      x2       x3 name rate")')
          DO k=1,in%nObservationPoint
             WRITE (FPOUT,'(2ES9.2E1,X,a4,X,I4)') &
                     in%observationPoint(k)%x(2), &
                     in%observationPoint(k)%x(3), &
                     in%observationPoint(k)%name, &
                     in%observationPoint(k)%rate
          END DO
          CLOSE(FPOUT)

       END IF

       ! test the presence of dislocations
       IF ((in%patch%ns .EQ. 0) .AND. &
           (in%interval .LE. 0d0)) THEN
   
          WRITE_DEBUG_INFO(300)
          WRITE (STDERR,'("nothing to do. exiting.")')
          STOP 1
       END IF
   
       PRINT 2000
       ! flush standard output
       CALL FLUSH(6)

       in%nObservationState=nObservationPatch+nObservationVolume
       ALLOCATE(in%observationState(4,in%nObservationState))
       j=1
       DO i=1,nObservationPatch
          in%observationState(1,j)=observationPatch(1,i)
          in%observationState(2,j)=observationPatch(2,i)
          in%observationState(3,j)=i
          j=j+1
       END DO

       DO i=1,nObservationVolume
          in%observationState(1,j)=observationVolume(1,i)+in%patch%ns
          in%observationState(2,j)=observationVolume(2,i)
          in%observationState(3,j)=i
          j=j+1
       END DO
       IF (ALLOCATED(observationPatch)) DEALLOCATE(observationPatch)
       IF (ALLOCATED(observationVolume)) DEALLOCATE(observationVolume)

       position=0
       CALL MPI_PACK(in%interval,          1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%mu,                1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%lambda,            1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%patch%ns,          1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%rectangle%ns,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%triangle%ns, 1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%triangle%nVe,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nObservationState, 1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nObservationPoint, 1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)

       position=0
       CALL MPI_PACK(in%wdir,256,MPI_CHARACTER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)

       ! send the patches (geometry and friction properties) 
       DO i=1,in%patch%ns
          position=0
          CALL MPI_PACK(in%patch%s(i)%Vl,     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%x(2,i),      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%x(3,i),      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%width(i),    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%dip(i),      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%beta(i),     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%tau0,   1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%mu0,    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%sig,    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%a,      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%b,      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%L,      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%Vo,     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%patch%s(i)%damping,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the rectangle volume elements
       DO i=1,in%rectangle%ns
          position=0
          CALL MPI_PACK(in%rectangle%s(i)%sII,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%e22,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%e23,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%e33,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%x(2,i),          1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%x(3,i),          1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%width(i),        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%thickness(i),    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%dip(i),          1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%Gk,         1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%gammadot0k, 1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%gammadot0m, 1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%nGk,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%ngammadot0k,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%npowerk,    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%ngammadot0m,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%npowerm,    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the triangle volume elements
       DO i=1,in%triangle%ns
          position=0
          CALL MPI_PACK(in%triangle%s(i)%sII,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%e22,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%e23,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%e33,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%i1(i),           1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%i2(i),           1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%i3(i),           1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%xc(2,i),         1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%xc(3,i),         1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%Gk,         1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%gammadot0k, 1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%gammadot0m, 1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%nGk,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%ngammadot0k,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%npowerk,    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%ngammadot0m,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%npowerm,    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the triangle vertices
       IF (0 .NE. in%triangle%ns) THEN
          DO i=1,in%triangle%nVe
             position=0
             CALL MPI_PACK(in%triangle%v(:,i),3,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          END DO
       END IF

       ! send the observation state
       DO i=1,in%nObservationState
          position=0
          CALL MPI_PACK(in%observationState(:,i),4,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the observation points
       DO i=1,in%nObservationPoint
          position=0
          CALL MPI_PACK(in%observationPoint(i)%name,10,MPI_CHARACTER,packed,psize,position,MPI_COMM_WORLD,ierr)
          DO k=1,3
             CALL MPI_PACK(in%observationPoint(i)%x(k),1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          END DO
          CALL MPI_PACK(in%observationPoint(i)%rate,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

    ELSE ! IF 0 .NE. rank

       !------------------------------------------------------------------
       ! S L A V E S
       !------------------------------------------------------------------

       position=0
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%interval,          1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%mu,                1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%lambda,            1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%patch%ns,          1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%rectangle%ns,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%triangle%ns, 1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%triangle%nVe,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nObservationState, 1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nObservationPoint, 1,MPI_INTEGER,MPI_COMM_WORLD,ierr)

       position=0
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%wdir,256,MPI_CHARACTER,MPI_COMM_WORLD,ierr)

       IF (0 .LT. in%patch%ns) &
                    ALLOCATE(in%patch%s(in%patch%ns), &
                             in%patch%x(3,in%patch%ns), &
                             in%patch%xc(3,in%patch%ns), &
                             in%patch%width(in%patch%ns), &
                             in%patch%dip(in%patch%ns), &
                             in%patch%beta(in%patch%ns), &
                             in%patch%sv(3,in%patch%ns), &
                             in%patch%dv(3,in%patch%ns), &
                             in%patch%nv(3,in%patch%ns), &
                             STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for patches"

       DO i=1,in%patch%ns
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%Vl,     1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%x(2,i),      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%x(3,i),      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%width(i),    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%dip(i),      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%beta(i),     1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%tau0,   1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%mu0,    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%sig,    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%a,      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%b,      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%L,      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%Vo,     1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%patch%s(i)%damping,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
       END DO

       IF (0 .LT. in%rectangle%ns) &
                    ALLOCATE(in%rectangle%s(in%rectangle%ns), &
                             in%rectangle%x(3,in%rectangle%ns), &
                             in%rectangle%xc(3,in%rectangle%ns), &
                             in%rectangle%width(in%rectangle%ns), &
                             in%rectangle%thickness(in%rectangle%ns), &
                             in%rectangle%dip(in%rectangle%ns), &
                             in%rectangle%sv(3,in%rectangle%ns), &
                             in%rectangle%dv(3,in%rectangle%ns), &
                             in%rectangle%nv(3,in%rectangle%ns),STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for rectangle volume elements"

       DO i=1,in%rectangle%ns
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%sII,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%e22,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%e23,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%e33,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%x(2,i),          1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%x(3,i),          1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%width(i),        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%thickness(i),    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%dip(i),          1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%Gk,         1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%gammadot0k, 1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%gammadot0m, 1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%nGk,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%ngammadot0k,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%npowerk,    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%ngammadot0m,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%npowerm,    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
       END DO

       IF (0 .LT. in%triangle%ns) &
                    ALLOCATE(in%triangle%s(in%triangle%ns), &
                             in%triangle%i1(in%triangle%ns), &
                             in%triangle%i2(in%triangle%ns), &
                             in%triangle%i3(in%triangle%ns), &
                             in%triangle%xc(3,in%triangle%ns))
       IF (ierr>0) STOP "slave could not allocate memory for triangle volume elements"

       IF (0 .LT. in%triangle%nVe) ALLOCATE(in%triangle%v(3,in%triangle%nVe), STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory"

       DO i=1,in%triangle%ns
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%sII,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%e22,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%e23,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%e33,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%i1(i),           1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%i2(i),           1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%i3(i),           1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%xc(2,i),         1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%xc(3,i),         1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%Gk,         1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%gammadot0k, 1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%gammadot0m, 1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%nGk,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%ngammadot0k,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%npowerk,    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%ngammadot0m,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%npowerm,    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
       END DO

       ! receive triangle vertices
       DO i=1,in%triangle%nVe
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%v(:,i),3,MPI_REAL8,MPI_COMM_WORLD,ierr)
       END DO

       IF (0 .LT. in%nObservationState) &
                    ALLOCATE(in%observationState(4,in%nObservationState), &
                             STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for observation states"

       DO i=1,in%nObservationState
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%observationState(:,i),4,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       END DO

       IF (0 .LT. in%nObservationPoint) &
                    ALLOCATE(in%observationPoint(in%nObservationPoint), &
                             STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for observation points"

       DO i=1,in%nObservationPoint
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%observationPoint(i)%name,10,MPI_CHARACTER,MPI_COMM_WORLD,ierr)
          DO k=1,3
             CALL MPI_UNPACK(packed,psize,position,in%observationPoint(i)%x(k),1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          END DO
          CALL MPI_UNPACK(packed,psize,position,in%observationPoint(i)%rate,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       END DO

    END IF ! master or slaves

    in%nPatch=in%patch%ns
    in%nVolume=in%rectangle%ns+in%triangle%ns

2000 FORMAT ("# ----------------------------------------------------------------------------")
     
  END SUBROUTINE init

  !-----------------------------------------------
  !> subroutine printhelp
  !! displays a help message with master thread.
  !-----------------------------------------------
  SUBROUTINE printhelp()
  
    INTEGER :: rank,size,ierr
    
    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,size,ierr)
    
    IF (0 .EQ. rank) THEN
       PRINT '("usage:")'
       PRINT '("")'
       PRINT '("mpirun -n 2 unicycle-ps-viscouscycles [-h] [--dry-run] [--help] [--epsilon 1e-6] [filename]")'
       PRINT '("")'
       PRINT '("options:")'
       PRINT '("   -h                      prints this message and aborts calculation")'
       PRINT '("   --dry-run               abort calculation, only output geometry")'
       PRINT '("   --export-netcdf         export the kinematics to a netcdf file")'
       PRINT '("   --export-volume         export volume elements in GMT compatible .xy files")'
       PRINT '("   --help                  prints this message and aborts calculation")'
       PRINT '("   --version               print version number and exit")'
       PRINT '("   --epsilon               set the numerical accuracy [1E-6]")'
       PRINT '("   --export-greens wdir    export the Greens function to file")'
       PRINT '("   --friction-law          type of friction law [1]")'
       PRINT '("         1: multiplicative form of rate-state friction (Barbot, 2018)")'
       PRINT '("         2: additive       form of rate-state friction (Ruina, 1983)")'
       PRINT '("         3: arcsinh        form of rate-state friction (Rice & Benzion, 1996)")'
       PRINT '("   --evolution-law         type of evolution law [1]")'
       PRINT '("       1: aging law        isothermal, isobaric additive form (Ruina, 1983)")'
       PRINT '("       2: slip law         isothermal, isobaric multiplicative form (Ruina, 1983)")'
       PRINT '("   --import-greens wdir    import the Greens function from file")'
       PRINT '("   --maximum-iterations    set the maximum time step [1000000]")'
       PRINT '("   --maximum-step          set the maximum time step [none]")'
       PRINT '("")'
       PRINT '("description:")'
       PRINT '("   simulates elasto-dynamics on faults in plane strain")'
       PRINT '("   in viscoelastic media with the radiation-damping approximation")'
       PRINT '("   using the integral method.")'
       PRINT '("")'
       PRINT '("   if filename is not provided, reads from standard input.")'
       PRINT '("")'
       PRINT '("see also: ""man unicycle""")'
       PRINT '("")'
       PRINT  '("                            _                  _")'
       PRINT  '("   _O        _   _  __   _ (_)  ___ _   _  ___| | ___")'
       PRINT  '("   (\`      | | | ||   \| || | / __| | | |/ __| |/ _ \")'
       PRINT  '("   |>       | |_| || |\ '' || || (__| |_| | (__| |  __/")'
       PRINT  '("   ,|.       \___/ |_| \__||_| \___|\__, |\___|_|\___|")'
       PRINT  '("   `-''                              |___/")'
       PRINT '("")'
       CALL FLUSH(6)
    END IF
    
  END SUBROUTINE printhelp
  
  !-----------------------------------------------
  !> subroutine printversion
  !! displays code version with master thread.
  !-----------------------------------------------
  SUBROUTINE printversion()
    
    INTEGER :: rank,size,ierr
    
    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,size,ierr)
    
    IF (0 .EQ. rank) THEN
       PRINT '("unicycle-ps-viscouscycles version 1.0.1, compiled on ",a)', __DATE__
       PRINT '("")'
       CALL FLUSH(6)
    END IF
  
  END SUBROUTINE printversion
    
END PROGRAM viscouscycles

