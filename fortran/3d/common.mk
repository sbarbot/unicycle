
OBJRS=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, types_3d.o getdata.o \
     cuboid.o computeDisplacementVerticalCuboidMixedQuad.o computeStressVerticalCuboidMixedQuad.o \
     comdun.o mod_dtrigreen.o stuart97.o dc3d.o okada92.o \
     tetrahedron.o gauss.o \
     computeStressVerticalCuboidSourceImageGauss.o \
     computeStressTetrahedronSourceImageGauss.o \
     computeDisplacementTetrahedronGauss.o \
     computeStressVerticalCuboidGauss.o \
     computeStressTetrahedronGauss.o \
     exportxyz_rfaults.o exportxyz_tfaults.o exportxyz_cuboids.o exportxyz_tetrahedron.o \
     exportvtk_tetrahedron.o exportvtk_rfaults.o exportvtk_tfaults.o exportvtk_cuboids.o \
     DispInHalfSpace.o StrainInHalfSpace.o nikkhoo15.o greens_3d.o ratestate.o )

OBJTB=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, \
      types_3d_thermobaric.o getdata.o \
      cuboid.o computeDisplacementVerticalCuboidMixedQuad.o computeStressVerticalCuboidMixedQuad.o \
      comdun.o mod_dtrigreen.o stuart97.o dc3d.o okada92.o \
      tetrahedron.o gauss.o \
      computeStressVerticalCuboidSourceImageGauss.o \
      computeStressTetrahedronSourceImageGauss.o \
      computeDisplacementTetrahedronGauss.o \
      computeStressVerticalCuboidGauss.o \
      computeStressTetrahedronGauss.o \
      exportxyz_rfaults_thermobaric.o exportxyz_tfaults_thermobaric.o \
      exportvtk_rfaults_thermobaric.o exportvtk_tfaults_thermobaric.o \
      DispInHalfSpace.o StrainInHalfSpace.o nikkhoo15.o greens_3d_thermobaric.o rootbisection.o thermobaric.o )

OBJVC=$(SRC)/macros.h90 $(patsubst %,$(DST)/%, \
      types_3d.o getdata.o \
      cuboid.o computeDisplacementVerticalCuboidMixedQuad.o computeStressVerticalCuboidMixedQuad.o \
      computeStressVerticalCuboidSourceImageGauss.o \
      exportvtk_rfaults.o exportvtk_tfaults.o exportvtk_cuboids.o exportvtk_tetrahedron.o \
      computeStressVerticalCuboidGauss.o computeStressTetrahedronGauss.o \
      computeStressTetrahedronSourceImageGauss.o \
      computeDisplacementTetrahedronGauss.o tetrahedron.o \
      exportxyz_rfaults.o exportxyz_tfaults.o exportxyz_cuboids.o exportxyz_tetrahedron.o \
      comdun.o mod_dtrigreen.o stuart97.o dc3d.o okada92.o gauss.o \
      DispInHalfSpace.o StrainInHalfSpace.o nikkhoo15.o greens_3d.o viscouscycles.o )

LIB=$(patsubst %,$(LIBDST)/%, \
      getopt_m.o exportnetcdf.o rk.o )

$(shell mkdir -p $(DST))
$(shell mkdir -p $(LIBDST))

$(DST)/%.o:$(SRC)/%.c
	$(COMPILE.c) $^ -o $(DST)/$*.o

$(DST)/%.o: $(SRC)/%.f $(SRC)/macros.h90
	$(COMPILE.f) $(filter-out $(SRC)/macros.h90,$^) -o $(DST)/$*.o

$(DST)/%.o: $(SRC)/%.f90 $(SRC)/macros.h90
	$(COMPILE.f) $(filter-out $(SRC)/macros.h90,$^) -o $(DST)/$*.o -J $(DST)

$(LIBDST)/%.o: $(LIBSRC)/%.f90
	$(COMPILE.f) $^ -o $(LIBDST)/$*.o -J $(LIBDST)

all: lib \
	unicycle-3d-ratestate \
	unicycle-3d-viscouscycles \
	unicycle-3d-thermobaric

unicycle-3d-ratestate: $(filter-out $(SRC)/macros.h90,$(OBJRS)) $(LIB)
	$(LINK.f) -o $(DST)/$@ $^ $(LIBS)

unicycle-3d-viscouscycles: $(filter-out $(SRC)/macros.h90,$(OBJVC)) $(LIB)
	$(LINK.f) -o $(DST)/$@ $^ $(LIBS)

unicycle-3d-thermobaric: $(filter-out $(SRC)/macros.h90,$(OBJTB)) $(LIB)
	$(LINK.f) -o $(DST)/$@ $^ $(LIBS)

lib: $(LIB)

clean:
	$(RM) $(DST)/*.o $(DST)/*.mod

.NOTPARALLEL:

