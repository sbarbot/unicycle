#!/bin/bash

set -e
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

# Benchmark problem BP4 is for a three-dimensional (3D) extension of BP1 to a problem in
# a whole-space (quasi-dynamic approximation is still assumed), although some parameters
# are changed to make the computations more feasible. The model size, resolution, initial and
# boundary conditions, and model output are designed specically for 3D problems.

selfdir=$(dirname $0)

WDIR=$selfdir/output-bp4-500-light
FLT=$WDIR/flt.3d

GDIR=$selfdir/greens-32-500

if [ ! -e $GDIR ]; then
	echo adding directory $GDIR
	mkdir $GDIR
fi

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

NY=240
NZ=160
DX=500

echo "" | awk -v nl=$NY -v nw=$NZ -v dx=$DX 'BEGIN{k=1;pi=atan2(1.0,0.0)*2.0}{
       	x1o=0;
       	x2o=0;
       	x3o=300e3;
       	len=dx;
       	width=dx;
	strike=90;
       	dip=90;
	rake=180;
	s1=cos(pi*strike/180.0)
	s2=sin(pi*strike/180.0)
	s3=0
	d1=1-sin(pi*strike/180.0)*cos(pi*dip/180.0)-1
	d2=1+cos(pi*strike/180.0)*cos(pi*dip/180.0)-1
	d3=1+sin(pi*dip/180.0)-1
	for (j=-nw/2;j<(nw/2-1);j++){
		for (i=-nl/2;i<(nl/2-1);i++){
			x1=x1o+i*s1*len+j*d1*width;
			x2=x2o+i*s2*len+j*d2*width;
       	        	x3=x3o+i*s3*len+j*d3*width;
       	        	printf "%05d %e %16.11e %16.11e %16.11e %f %f %f %f %f\n", k,1e-9,x1,x2,x3,len,width,strike,dip,rake;
			k=k+1;
		}
       	}
}' > $FLT

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli (MPa)
32.03812032e3 32.03812032e3
# time interval (s)
4.725e10
# number of rectangular patches
`grep -v "#" $FLT | wc -l`
# n  Vl      x1    x2   x3 length width strike dip rake
`grep -v "#" $FLT`
# number of frictional patches
`grep -v "#" $FLT | wc -l`
# n tau0 mu0 sig    a     b    L   Vo G/2Vs
`grep -v "#" $FLT | awk -v dx=$DX '
	function abs(x){return (x>0)?x:-x};
	function max(x,y){return (x>y)?x:y};
	function boxcar(x){return (x>=-0.5 && x<=0.5)?1:0};
	function heavi(x){return (x>0)?1:0};
	function ramp(x){return x*boxcar(x-0.5)+heavi(x-1)};
	function asinh(x){return log(x+sqrt(1+x^2))};
	function sinh(x){return (exp(x)-exp(-x))/2};
	{ 
	x2=$4;
	x3=$5-300e3;
	mu0=0.6;
	sig=50;
	a0=0.0065;
	amax=0.025;
	a=a0+ramp(max(abs(x3)-15e3,abs(x2)-30e3)/3e3)*(amax-a0);
	b=0.013;
	Vo=1e-6;
	L=4e-2;
	rho=2670;
	Vs=3464;
	G=rho*Vs^2/1e6;
	damping=G/Vs/2;
	Vl=1e-9;
	tau0=a*sig*asinh(Vl/Vo/2*exp((mu0+b*log(Vo/Vl))/a));
	# stress perturbation
	if (1==boxcar((x2-(30e3-1.5e3-6e3))/12e3)*boxcar((x3-(15e3-1.5e3-6e3))/12e3)){
		tau0=a*sig*asinh(1e-3/Vo/2*exp((mu0+b*log(Vo/Vl))/a));
	}
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,damping;
	}'`
# number of triangular patches
0
# number of observation patches
14
# n   index rate
`cat <<EOF | awk -v nl=$NY -v nw=$NZ -v dx=$DX '{i=int($1/dx)+nl/2+1;j=int($2/dx)+nw/2+1;print NR,i+j*nw+1,10}'
-36.0e3 +0.00e3
-22.5e3 -7.50e3
-16.5e3 -12.0e3
-16.5e3 +0.00e3
-16.5e3 +12.0e3
+0.00e3 -21.0e3
+0.00e3 -12.0e3
+0.00e3 +0.00e3
+0.00e3 +12.0e3
+0.00e3 +21.0e3
+16.5e3 -12.0e3
+16.5e3 +0.00e3
+16.5e3 +12.0e3
+36.0e3 +0.00e3
EOF`
# number of observation points
0
# number of events
0
EOF

# load Greens function if they exist
if [ -e $GDIR/greens-0000.grd ]; then
	IMPORT_GREENS="--import-greens $GDIR"
fi
 
time mpirun -n 32 unicycle-3d-ratestate \
	$* \
	--epsilon 1e-4 \
	--friction-law 3 \
	--maximum-step 3.15e7 \
	--maximum-iterations 1000000 \
	$WDIR/in.param

