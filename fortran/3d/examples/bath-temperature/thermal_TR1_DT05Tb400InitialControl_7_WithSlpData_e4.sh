#!/bin/bash

set -e
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

# simulates thermal instabilities with velocity-strengthening properties.

selfdir=$(dirname $0)
WDIR=$selfdir/TR1_DT05Tb400InitialControl_ix7_e_4WithSlpData
GDIR=$selfdir/greens
FLT=$WDIR/flt.3d
GPS=$selfdir/gps/unavco_m.xy

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

if [ ! -e $GDIR ]; then
	echo adding directory $GDIR
	mkdir $GDIR
fi

NX=225
NZ=80
DX=200
FW=1e3

# list of patches forming a vertical profile
VER=`echo "" | awk -v nz=$NZ -v nx=$NX \
	'BEGIN{i=nx/2}{
		for (j=1;j<=nz;j++){ 
			printf "%d \n",i+(j-1)*nx
		}
	}'`

# list of patches forming a horizontal profile centered on the thermally weakening patch
HOR=`echo "" | awk -v nx=$NX -v nz=$NZ -v dx=$DX -v fw=$FW \
	'BEGIN{j=int(((17e3+fw/2-10e3)/dx))}{
		for (i=1;i<=nx;i++){ 
			printf "%d\n",i+(j)*nx                 
		};
	}'`

# list of surface patches with subsampling
ALL=`echo "" | awk -v nz=$NZ -v nx=$NX \
	'{
		for (j=1;j<=nz;j=j+2){ 
			for (i=1;i<=nx;i=i+2){
				printf "%d \n",i+(j-1)*nx
			}
		}
	}'`

# fault geometry
echo "" | awk -v nx=$NX -v nz=$NZ -v dx=$DX 'BEGIN{k=1;pi=atan2(1.0,0.0)*2.0}{
       	x1o=+1.058439e+04;
       	x2o=-5.997553e+03;
       	x3o=10e3;
       	len=dx;
       	width=dx;
	strike=-39.0;
       	dip=90;
	rake=180;
	s1=cos(pi*strike/180.0)
	s2=sin(pi*strike/180.0)
	s3=0
	d1=1-sin(pi*strike/180.0)*cos(pi*dip/180.0)-1
	d2=1+cos(pi*strike/180.0)*cos(pi*dip/180.0)-1
	d3=1+sin(pi*dip/180.0)-1
	Vl=1e-9;
       	for (j=0;j<nz;j++){
		for (i=0;i<nx;i++){
			x1=x1o+i*s1*len+j*d1*width;
			x2=x2o+i*s2*len+j*d2*width;
       	        	x3=x3o+i*s3*len+j*d3*width;
       	        	printf "%05d %e %16.11e %16.11e %16.11e %f %f %f %f %f\n", k,Vl,x1,x2,x3,len,width,strike,dip,rake;
       	        	k=k+1;
			}
       	}
}' > $FLT

# input file
cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# Lame parameter (MPa), rigidity (MPa), universal gas constant
40e3 40e3 8.314
# time interval (s)
1e10
# number of rectangular patches
`grep -v "#" $FLT | wc -l`
# n   Vl     x1    x2   x3 length width strike dip rake
`grep -v "#" $FLT`
# number of frictional properties
`grep -v "#" $FLT | wc -l`
# n tau0 mu0 sig    a     b    L   Vo G/2Vs
`grep -v "#" $FLT | awk -v dx=$DX -v nx=$NX -v nz=$NZ '{ 
	x1=$3;
	x2=$4;
	x3=$5; 
	tau0=-1;
	mu0=0.5449;
	sig=19.92;
	a=1.02e-2;
	b=6e-3;
        L=3.1e-3;
	Vo=1e-6;
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,5;\
	}'`
# number of thermal properties
`grep -v "#" $FLT | wc -l`
# n        Q        H        D        W        w     rhoc       Tb
`grep -v "#" $FLT | awk -v dx=$DX -v nx=$NX -v nz=$NZ -v fw=$FW 'BEGIN{pi=atan2(1,0)*2}{ 
	x1=$3;
	x2=$4;
	x3=$5; 
	strike=$8/180*pi;
	l=x1*cos(strike)+x2*sin(strike)-12e3;
	Q=90.03e3;
	H=59.75e3;
	D=7.43e-7;
	W=1.70;
	w=((l>(15e3)) && 
	   (l<(30e3)) && 
	   (x3>=(17e3)) && 
	   (x3<(17e3+fw)))?1.4e-4:1e-0;
	rhoc=2.6;
	Tb=400+273.15;
	printf "%06d %e %e %e %e %e %e %e\n", NR,Q,H,D,W,w,rhoc,Tb;
	}'`
# number of triangular patches
0
# number of observation patches
`for i in 1; do echo "$VER"; echo "$HOR"; echo "$ALL"; done | wc -l | awk '{print $1+1}'`
# n   index rate
`for i in 1; do echo "$VER"; echo "$HOR"; echo "$ALL"; done | \
	awk -v nx=$NX -v nz=$NZ -v dx=$DX -v fw=$FW \
	'{print NR,$1,10}END{printf "%d %d 1\n",NR+1,(int((17e3+fw/2-10e3)/dx))*nx+nx/2}'`
# number of observation points
`grep -v "#" $GPS | wc -l`
# n name x1 x2 x3
`grep -v "#" $GPS | awk '{print NR,$3,$2,$1,0}'`
# number of events
0
EOF

# load Greens function if they exist
if [ -e $GDIR/greens-0000.grd ]; then
	IMPORT_GREENS="--import-greens $GDIR"
fi
 
#time mpirun -n 16 unicycle-3d-ratestate-bath \
#	--epsilon 1e-4 \
#	--friction-law 1 \
#	--maximum-step 3.15e7 \
#	--maximum-iterations 1000000 \
#	--export-greens $GDIR \
# 	$IMPORT_GREENS \
#	$WDIR/in.param

ecatalogue.sh -b 1e-8/1e-8 $WDIR/time.dat > $WDIR/ecatalogue-1e-8.dat
exit
echo "# exporting to $WDIR/slip-temp-ver.grd"
files=`echo "$VER" | awk -v d=$WDIR '{printf "%s/patch-*-%08d.dat ",d,$1}'`
yRange=`for i in $files; do tail -n 1 $i | awk '{print -$2}'; done | minmax -C | awk '{print $2}'`
c=0
for i in $files; do
        c=`echo $c | awk '{print $1+1}'`
        grep -v "#" $i | awk -v i=$c '{print i,-$2,$9-273.15}'
done | surface -I0.125/`echo $yRange | awk '{print $1/16384}'` -R1/$NZ/0/$yRange -G$WDIR/slip-temp-ver.grd

echo "# exporting to $WDIR/slip-log10v-ver.grd"
files=`echo "$VER" | awk -v d=$WDIR '{printf "%s/patch-*-%08d.dat ",d,$1}'`
yRange=`for i in $files; do tail -n 1 $i | awk '{print -$2}'; done | minmax -C | awk '{print $2}'`
c=0

for i in $files; do
        c=`echo $c | awk '{print $1+1}'`
        grep -v "#" $i | awk -v i=$c '{print i,-$2,$8}'
done | surface -I0.125/`echo $yRange | awk '{print $1/16384}'` -R1/$NZ/0/$yRange -G$WDIR/slip-log10v-ver.grd

echo "# exporting to $WDIR/slip-temp-hor.grd"
files=`echo "$HOR" | awk -v d=$WDIR '{printf "%s/patch-*-%08d.dat ",d,$1}'`
yRange=`for i in $files; do tail -n 1 $i | awk '{print -$2}'; done | minmax -C | awk '{print $2}'`
c=0
for i in $files; do
        c=`echo $c | awk '{print $1+1}'`
        grep -v "#" $i | awk -v i=$c '{print i,-$2,$9-273.15}'
done | surface -I0.125/`echo $yRange | awk '{print $1/16384}'` -R1/$NX/0/$yRange -G$WDIR/slip-temp-hor.grd

echo "# exporting to $WDIR/slip-log10v-hor.grd"
files=`echo "$HOR" | awk -v d=$WDIR '{printf "%s/patch-*-%08d.dat ",d,$1}'`
yRange=`for i in $files; do tail -n 1 $i | awk '{print -$2}'; done | minmax -C | awk '{print $2}'`
c=0
for i in $files; do
        c=`echo $c | awk '{print $1+1}'`
        grep -v "#" $i | awk -v i=$c '{print i,-$2,$8}'
done | surface -I0.125/`echo $yRange | awk '{print $1/16384}'` -R1/$NX/0/$yRange -G$WDIR/slip-log10v-hor.grd

echo "# exporting to $WDIR/step-temp-ver.grd"
files=`echo "$VER" | awk -v d=$WDIR '{printf "%s/patch-*-%08d.dat ",d,$1}'`
xRange=`echo "$VER" | wc -l | awk '{print $1}'`
yRange=`for i in $files; do wc -l $i | awk '{print $1}'; done | minmax -C | awk '{print ($2-1)*10+1}'`
c=0
for i in $files; do
        c=`echo $c | awk '{print $1+1}'`
        grep -v "#" $i | awk -v i=$c '{print i,(NR-1)*10+1,$9-273.15}'
done | xyz2grd -I1/10 -R1/$xRange/1/$yRange -G$WDIR/step-temp-ver.grd

echo "# exporting to $WDIR/step-log10v-ver.grd"
files=`echo "$VER" | awk -v d=$WDIR '{printf "%s/patch-*-%08d.dat ",d,$1}'`
xRange=`echo "$VER" | wc -l | awk '{print $1}'`
yRange=`for i in $files; do wc -l $i | awk '{print $1}'; done | minmax -C | awk '{print ($2-1)*10+1}'`
c=0
for i in $files; do
        c=`echo $c | awk '{print $1+1}'`
        grep -v "#" $i | awk -v i=$c '{print i,(NR-1)*10+1,$8}'
done | xyz2grd -I1/10 -R1/$xRange/1/$yRange -G$WDIR/step-log10v-ver.grd

echo "# exporting to $WDIR/step-temp-hor.grd"
files=`echo "$HOR" | awk -v d=$WDIR '{printf "%s/patch-*-%08d.dat ",d,$1}'`
xRange=`echo "$HOR" | wc -l | awk '{print $1}'`
yRange=`for i in $files; do wc -l $i | awk '{print $1}'; done | minmax -C | awk '{print ($2-1)*10+1}'`
c=0
for i in $files; do
        c=`echo $c | awk '{print $1+1}'`
        grep -v "#" $i | awk -v i=$c '{print i,(NR-1)*10+1,$9-273.15}'
done | xyz2grd -I1/10 -R1/$xRange/1/$yRange -G$WDIR/step-temp-hor.grd

echo "# exporting to $WDIR/step-log10v-hor.grd"
files=`echo "$HOR" | awk -v d=$WDIR '{printf "%s/patch-*-%08d.dat ",d,$1}'`
xRange=`echo "$HOR" | wc -l | awk '{print $1}'`
yRange=`for i in $files; do wc -l $i | awk '{print $1}'; done | minmax -C | awk '{print ($2-1)*10+1}'`
c=0
for i in $files; do
        c=`echo $c | awk '{print $1+1}'`
        grep -v "#" $i | awk -v i=$c '{print i,(NR-1)*10+1,$8}'
done | xyz2grd -I1/10 -R1/$xRange/1/$yRange -G$WDIR/step-log10v-hor.grd

echo "# exporting to $WDIR/time-temp-ver.grd"
files=`echo "$VER" | awk -v d=$WDIR '{printf "%s/patch-*-%08d.dat ",d,$1}'`
yRange=`for i in $files; do tail -n 1 $i | awk '{print $1/3.15e7}'; done | minmax -C | awk '{print $2}'`
c=0
for i in $files; do
        c=`echo $c | awk '{print $1+1}'`
        grep -v "#" $i | awk -v i=$c '{print i,$1/3.15e7,$9-273.15}'
done | \
blockmean -I1/`echo $yRange | awk '{print $1/16384}'` -R1/$NZ/0/$yRange -E | awk '{print $1,$2,$6}' | \
surface -I1/`echo $yRange | awk '{print $1/16384}'` -R1/$NZ/0/$yRange -G$WDIR/time-temp-ver.grd

echo "# exporting to $WDIR/time-log10v-ver.grd"
files=`echo "$VER" | awk -v d=$WDIR '{printf "%s/patch-*-%08d.dat ",d,$1}'`
yRange=`for i in $files; do tail -n 1 $i | awk '{print $1/3.15e7}'; done | minmax -C | awk '{print $2}'`
c=0
for i in $files; do
        c=`echo $c | awk '{print $1+1}'`
        grep -v "#" $i | awk -v i=$c '{print i,$1/3.15e7,$8}'
done | \
blockmean -I0.125/`echo $yRange | awk '{print $1/16384}'` -R1/$NZ/0/$yRange -E | awk '{print $1,$2,$6}' | \
surface -I0.125/`echo $yRange | awk '{print $1/16384}'` -R1/$NZ/0/$yRange -G$WDIR/time-log10v-ver.grd

echo "# exporting to $WDIR/time-temp-hor.grd"
files=`echo "$HOR" | awk -v d=$WDIR '{printf "%s/patch-*-%08d.dat ",d,$1}'`
yRange=`for i in $files; do tail -n 1 $i | awk '{print $1/3.15e7}'; done | minmax -C | awk '{print $2}'`
c=0
for i in $files; do
        c=`echo $c | awk '{print $1+1}'`
        grep -v "#" $i | awk -v i=$c '{print i,$1/3.15e7,$9-273.15}'
done | \
blockmean -I0.125/`echo $yRange | awk '{print $1/16384}'` -R1/$NX/0/$yRange -E | awk '{print $1,$2,$6}' | \
surface -I0.125/`echo $yRange | awk '{print $1/16384}'` -R1/$NX/0/$yRange -G$WDIR/time-temp-hor.grd

echo "# exporting to $WDIR/time-log10v-hor.grd"
files=`echo "$HOR" | awk -v d=$WDIR '{printf "%s/patch-*-%08d.dat ",d,$1}'`
yRange=`for i in $files; do tail -n 1 $i | awk '{print $1/3.15e7}'; done | minmax -C | awk '{print $2}'`
c=0
for i in $files; do
        c=`echo $c | awk '{print $1+1}'`
        grep -v "#" $i | awk -v i=$c '{print i,$1/3.15e7,$8}'
done | \
blockmean -I0.125/`echo $yRange | awk '{print $1/16384}'` -R1/$NX/0/$yRange -E | awk '{print $1,$2,$6}' | \
surface -I0.125/`echo $yRange | awk '{print $1/16384}'` -R1/$NX/0/$yRange -G$WDIR/time-log10v-hor.grd


