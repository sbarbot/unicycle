#!/bin/bash

set -e
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

# Explore the dynamics of a 5km-width velocity-weakening region on a vertical
# strike-slip fault as a function of the characteristic weakening distance L.
# The simulations produce in turn creep, long-term slow-slip events, short-term
# slow-slip events, very low-frequency earthquakes, bilateral ruptures, 
# unilateral ruptures, and partial ruptures sequences of increasing complexity
# with decreasing L, corresponding to an increase in the Ruina number Ru=R/h*.

#PBS -N 3d-batch1-ruina
#PBS -q q16
#PBS -l nodes=1:ppn=16
#PBS -l walltime=20:00:00:00
#PBS -V

module load unicycle
module load relax

selfdir=$(dirname $0)

if [ "$PBS_O_WORKDIR" != "" ]; then
	echo "MPI version:" `which mpirun`
	echo working directory: $PBS_O_WORKDIR
	cd $PBS_O_WORKDIR
	selfdir=$PBS_O_WORKDIR
	echo host: `hostname`
	echo time: `date`
	NPROCS=`wc -l < $PBS_NODEFILE`
	echo number of cpus: $NPROCS
	echo processors:
	echo `cat $PBS_NODEFILE`
fi

GDIR=$selfdir/greens

if [ ! -e $GDIR ]; then
	echo adding directory $GDIR
	mkdir $GDIR
fi

N=128
DX=100

VER=`echo "" | awk -v nw=$N -v nl=$N \
	'BEGIN{i=nl/2}{
		for (j=1;j<=nw;j++){ 
			printf "%d \n",i+(j-1)*nl
		}
	}'`

HOR=`echo "" | awk -v nl=$N -v nw=$N \
	'BEGIN{j=nw/2}{
		for (i=1;i<=nl;i++){ 
			printf "%d\n",i+(j-1)*nl
		};
	}'`


for i in 2.5E-2 2.4E-2 2.3E-2 2.25E-2 2.24E-2 2.23E-2 2.22E-2 2.21E-2 2.2E-2 2.1E-2 2.0E-2 1.9E-2 1.8E-2 1.7E-2 1.6E-2 1.5E-2 1.45E-2 1.4E-2 1.35E-2 1.3E-2 1.25E-2 1.2E-2 1.15E-2 1.125E-2 1.1125E-2 1.1E-2 1.075E-2 1.05E-2 1.025E-2 1.0125E-2 1.0E-2 9.0E-3 8.9E-3 8.8E-3 8.7E-3 8.69E-3 8.68E-3 8.67E-3 8.66E-3 8.65E-3 8.64E-3 8.63E-3 8.62E-3 8.61E-3 8.6E-3 8.5E-3 8.4E-3 8.3E-3 8.2E-3 8.1E-3 8.0E-3 7.9E-3 7.8E-3 7.7E-3 7.6E-3 7.5E-3 7.49E-3 7.48E-3 7.47E-3 7.46E-3 7.45E-3 7.44E-3 7.42E-3 7.41E-3 7.4E-3 7.39E-3 7.38E-3 7.37E-3 7.36E-3 7.35E-3 7.34E-3 7.32E-3 7.31E-3 7.3E-3 7.2E-3 7.1E-3 7.0E-3 6.0E-3 5.0E-3 4.0E-3 3.0E-3 2.0E-3 1.0E-3; do

	WDIR=$selfdir/output-L$i
	FLT=$WDIR/flt.3d

	if [ ! -e $WDIR ]; then
		echo adding directory $WDIR
		mkdir $WDIR
	else
		continue
	fi

	echo "" | awk -v N=$N -v dx=$DX 'BEGIN{k=1;pi=atan2(1.0,0.0)*2.0}{
        	x1o=0;
        	x2o=0;
        	x3o=0;
        	len=dx;
        	width=dx;
		strike=0;
        	dip=90;
		rake=0;
		s1=cos(pi*strike/180.0)
		s2=sin(pi*strike/180.0)
		s3=0
		d1=1-sin(pi*strike/180.0)*cos(pi*dip/180.0)-1
		d2=1+cos(pi*strike/180.0)*cos(pi*dip/180.0)-1
		d3=1+sin(pi*dip/180.0)-1
        	for (j=0;j<N;j++){
			for (i=0;i<N;i++){
				x1=x1o+i*s1*len+j*d1*width;
				x2=x2o+i*s2*len+j*d2*width;
        	        	x3=x3o+i*s3*len+j*d3*width;
        	        	printf "%05d %e %16.11e %16.11e %16.11e %f %f %f %f %f\n", k,1e-09,x1,x2,x3,len,width,strike,dip,rake;
        	        	k=k+1;
			}
        	}
	}' > $FLT

	cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# elastic moduli (MPa)
30e3 30e3
# time interval (s)
5e10
# number of rectangular patches
`grep -v "#" $FLT | wc -l`
# n  Vpl     x1    x2   x3 length width strike dip rake
`grep -v "#" $FLT`
# number of frictional patches
`grep -v "#" $FLT | wc -l`
# n mu0 sig    a     b    L   Vo G/2Vs
`grep -v "#" $FLT | awk -v L=$i -v dx=$DX -v n=$N '{ 
	x1=$3;
	x3=$5; 
	tau0=-1;
	mu0=0.6;
	sig=100;
	a=1.0e-2;
	b=((x1>(dx*n*2/6)) && 
	   (x1<(dx*n*4/6)) && 
	   (x3>(dx*n*2/6)) && 
	   (x3<(dx*n*4/6)))?a+4e-3:a-4e-3;
	Vo=1e-6;
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,5;\
	}'`
# number of triangular patches
0
# number of observation patches
`for i in 1; do echo "$VER"; echo "$HOR"; done | wc -l | awk '{print $1+1}'`
# n   index rate
`for i in 1; do echo "$VER"; echo "$HOR"; done | awk '{print NR,$1,20}END{print NR+1,8128,1}'`
# number of observation points
0
# number of events
0
EOF

	# load Greens function if they exist
	if [ -e $GDIR/greens-0000.grd ]; then
		IMPORT_GREENS="--import-greens $GDIR"
	fi
 
	time mpirun -n 16 unicycle-3d-ratestate \
		--friction-law 2 \
		--maximum-step 3.15e7 \
		--maximum-iterations 1000000 \
		--export-greens $GDIR \
 		$IMPORT_GREENS \
		$WDIR/in.param

	echo "# exporting to $WDIR/slip-log10v-ver.grd"
	files=`echo "$VER" | awk 'BEGIN{i=1}{if (0 == (NR-1)%1){print i; i=i+1}}' | \
		awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`
	yRange=`for i in $files; do tail -n 1 $i | awk '{print $2}'; done | minmax -C | awk '{print $2}'`
	c=0
	for i in $files; do
		c=`echo $c | awk '{print $1+1}'`
		grep -v "#" $i | awk -v i=$c '{print i,$2,$8}'
	done | surface -I1/`echo $yRange | awk '{print $1/2048}'` -R1/$N/0/$yRange -G$WDIR/slip-log10v-ver.grd

	echo "# exporting to $WDIR/slip-log10v-hor.grd"
	files=`echo "$HOR" | awk 'BEGIN{i=1}{if (0 == (NR-1)%1){print i; i=i+1}}' | \
		awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`
	yRange=`for i in $files; do tail -n 1 $i | awk '{print $2}'; done | minmax -C | awk '{print $2}'`
	c=0
	for i in $files; do
		c=`echo $c | awk '{print $1+1}'`
		grep -v "#" $i | awk -v i=$c '{print i,$2,$8}'
	done | surface -I1/`echo $yRange | awk '{print $1/2048}'` -R1/$N/0/$yRange -G$WDIR/slip-log10v-hor.grd

	echo "# exporting to $WDIR/log10v-ver.grd"
	files=`echo "$VER" | awk 'BEGIN{i=1}{if (0 == (NR-1)%1){print i; i=i+1}}' | \
		awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`
	xRange=`echo "$VER" | wc -l | awk '{print $1}'`
	yRange=`for i in $files; do wc -l $i | awk '{print $1}'; done | minmax -C | awk '{print ($2-1)*20+1}'`
	c=0
	for i in $files; do
		c=`echo $c | awk '{print $1+1}'`
		grep -v "#" $i | awk -v i=$c '{print i,(NR-1)*20+1,$8}'
	done | xyz2grd -I1/20 -R1/$xRange/1/$yRange -G$WDIR/log10v-ver.grd

	echo "# exporting to $WDIR/log10v-hor.grd"
	files=`echo "$HOR" | awk 'BEGIN{i=1}{if (0 == (NR-1)%1){print i; i=i+1}}' | \
		awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`
	xRange=`echo "$HOR" | wc -l | awk '{print $1}'`
	yRange=`for i in $files; do wc -l $i | awk '{print $1}'; done | minmax -C | awk '{print ($2-1)*20+1}'`
	c=0
	for i in $files; do
		c=`echo $c | awk '{print $1+1}'`
		grep -v "#" $i | awk -v i=$c '{print i,(NR-1)*20+1,$8}'
	done | xyz2grd -I1/20 -R1/$xRange/1/$yRange -G$WDIR/log10v-hor.grd

	echo "# exporting to $WDIR/time-log10v-ver.grd"
	files=`echo "$VER" | awk 'BEGIN{i=1}{if (0 == (NR-1)%1){print i; i=i+1}}' | \
		awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`
	yRange=`for i in $files; do tail -n 1 $i | awk '{print $1/3.15e7}'; done | minmax -C | awk '{print $2}'`
	c=0
	for i in $files; do
		c=`echo $c | awk '{print $1+1}'`
		grep -v "#" $i | awk -v i=$c '{print i,$1/3.15e7,$8}'
	done | \
	blockmean -I1/`echo $yRange | awk '{print $1/2048}'` -R1/$N/0/$yRange -E | awk '{print $1,$2,$6}' | \
	surface -I1/`echo $yRange | awk '{print $1/2048}'` -R1/$N/0/$yRange -G$WDIR/time-log10v-ver.grd

	echo "# exporting to $WDIR/time-log10v-hor.grd"
	files=`echo "$HOR" | awk 'BEGIN{i=1}{if (0 == (NR-1)%1){print i; i=i+1}}' | \
		awk -v d=$WDIR '{printf "%s/patch-%08d* ",d,$1}'`
	yRange=`for i in $files; do tail -n 1 $i | awk '{print $1/3.15e7}'; done | minmax -C | awk '{print $2}'`
	c=0
	for i in $files; do
		c=`echo $c | awk '{print $1+1}'`
		grep -v "#" $i | awk -v i=$c '{print i,$1/3.15e7,$8}'
	done | \
	blockmean -I1/`echo $yRange | awk '{print $1/2048}'` -R1/$N/0/$yRange -E | awk '{print $1,$2,$6}' | \
	surface -I1/`echo $yRange | awk '{print $1/2048}'` -R1/$N/0/$yRange -G$WDIR/time-log10v-hor.grd

done

