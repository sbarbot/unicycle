#!/bin/bash -e

trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

# seismic cycles in an elastic half-space for a 30º-dipping
# thrust fault. 

selfdir=$(dirname $0)

WDIR="$selfdir/test-results/output2"
FLT="$WDIR/flt.3d"

if [ ! -e $WDIR ]; then
	echo "# adding directory $WDIR"
	mkdir -p "$WDIR"
fi

# fault dimension
NY=60
NZ=40
DX=1000

echo "" | awk -v nl="$NY" -v nw="$NZ" -v dx="$DX" 'BEGIN{k=1;pi=atan2(1.0,0.0)*2.0}{
       	x1o=24.5e3;
       	x2o=0;
       	x3o=0;
       	len=dx;
       	width=dx;
	strike=90;
       	dip=30;
	rake=89;
	s1=cos(pi*strike/180.0)
	s2=sin(pi*strike/180.0)
	s3=0
	d1=1-sin(pi*strike/180.0)*cos(pi*dip/180.0)-1
	d2=1+cos(pi*strike/180.0)*cos(pi*dip/180.0)-1
	d3=1+sin(pi*dip/180.0)-1
	for (j=0;j<nw;j++){
		for (i=-nl/2;i<(nl/2);i++){
			x1=x1o+i*s1*len+j*d1*width;
			x2=x2o+i*s2*len+j*d2*width;
       	        	x3=x3o+i*s3*len+j*d3*width;
       	        	printf "%05d %e %16.11e %16.11e %16.11e %f %f %f %f %f\n", k,1e-9,x1,x2,x3,len,width,strike,dip,rake;
			k=k+1;
		}
       	}
}' > $FLT

cat <<EOF > "$WDIR/in.param"
# output directory
$WDIR
# Lame parameter, rigidity (MPa)
30e3 30e3
# time interval (s)
1.5e10
# number of rectangular patches
$(grep -cv "#" $FLT)
# n  Vl      x1    x2   x3 length width strike dip rake
$(grep -v "#" $FLT)
# number of frictional patches
$(grep -cv "#" $FLT)
# n tau0 mu0 sig    a     b    L   Vo G/2Vs
$(grep -v "#" $FLT | awk -v dx=$DX '
	function abs(x){return (x>0)?x:-x};
	function max(x,y){return (x>y)?x:y};
	function boxcar(x){return (x>=-0.5 && x<=0.5)?1:0};
	function heavi(x){return (x>0)?1:0};
	function ramp(x){return x*boxcar(x-0.5)+heavi(x-1)};
	function asinh(x){return log(x+sqrt(1+x^2))};
	function sinh(x){return (exp(x)-exp(-x))/2};
	{ 
	x2=$4+dx/2;
	x3=$5+dx/2;
	mu0=0.6;
	sig=50;
	a0=0.0065;
	amax=0.025;
	a=a0+ramp(max(abs(x3)-10e3,abs(x2)-15e3)/3e3)*(amax-a0);
	b=0.013;
	Vo=1e-6;
	L=5e-2;
	rho=2670;
	Vs=3464;
	G=rho*Vs^2/1e6;
	damping=G/Vs/2;
	Vl=1e-9;
	tau0=-1
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,damping;
	}')
# number of triangular patches
0
# number of observation patches
0
# number of observation profiles
2
# n   n  offset stride rate
1 $NY  $(echo $NY 5e3 $DX | awk '{print int($2/$3)*$1}') 1 50
2 $NZ  $(echo $NY | awk '{print int($1/2)}') $NY 50
# number of observation images
1
# n  nl  nw offset stride rate
  1 $NY $NZ      0      1  100
# number of observation points
0
# number of perturbations
0
EOF

mpirun -n 2 --allow-run-as-root unicycle-3d-ratestate \
	$* \
	--epsilon 1e-6 \
	--friction-law 1 \
	--maximum-step 3.15e7 \
	--maximum-iterations 100000 \
	--export-greens "$WDIR" \
	--export-netcdf \
	$WDIR/in.param

ls -al $WDIR/{time.dat,rfaults.flt,patch-rectangle.vtp}
ls -al $WDIR/{greens-0000.grd,image-01-000100-log10v.grd}
ls -al $WDIR/{log10v.001.grd,log10v.002.grd}

gnuplot <<EOF
set term dumb
set logscale y
set xlabel "Time (year)"
set ylabel "Velocity (m/s)"
plot '$WDIR/time.dat' using (\$1/3.15e7):3 with line title 'Velocity (m/s)'
EOF

echo "# test successful" | tee "$WDIR/report.xml"

