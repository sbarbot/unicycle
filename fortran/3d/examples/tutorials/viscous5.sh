#!/bin/bash -e

trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

# seismic cycle in a 3d viscoelastic medium for the case of
# a 30.5 dipping thrust fault. The viscoelastic substrate
# uses a tetrahedral mesh to capture the curvature of the Moho.

selfdir=$(dirname $0)

WDIR=$selfdir/viscous5
FLT=$WDIR/flt.3d
TET=$WDIR/shz.tet
NED=$WDIR/shz.ned

if [ ! -e $WDIR ]; then
	echo adding directory $WDIR
	mkdir $WDIR
fi

# fault dimension
NY=60
NZ=40
DX=1000

# ductile domain
SX=12
SY=16
SZ=3
SDX=5000

echo "" | awk -v nl="$NY" -v nw="$NZ" -v dx="$DX" 'BEGIN{k=1;pi=atan2(1.0,0.0)*2.0}{
       	x1o=27.5e3;
       	x2o=0;
       	x3o=0;
       	len=dx;
       	width=dx;
	strike=90;
	dip=30.5;
	rake=89;
	s1=cos(pi*strike/180.0)
	s2=sin(pi*strike/180.0)
	s3=0
	d1=-sin(pi*strike/180.0)*cos(pi*dip/180.0)
	d2=+cos(pi*strike/180.0)*cos(pi*dip/180.0)
	d3=+sin(pi*dip/180.0)
	for (j=0;j<nw;j++){
		for (i=-nl/2;i<(nl/2);i++){
			x1=x1o+i*s1*len+j*d1*width;
			x2=x2o+i*s2*len+j*d2*width;
       	        	x3=x3o+i*s3*len+j*d3*width;
       	        	printf "%05d %e %16.11e %16.11e %16.11e %f %f %f %f %f\n", k,1e-9,x1,x2,x3,len,width,strike,dip,rake;
			k=k+1;
		}
       	}
}' > $FLT

echo "" | awk -v nl="$SX" -v nt="$SY" -v nw="$SZ" '
	function n(a,b,c){return a+(b+c*(nt+1))*(nl+1)};
	BEGIN{l=1;pi=atan2(1.0,0.0)*2.0}{
	e11=-1e-15;
	e12=0;
	e13=0;
	e22=0;
	e23=0;
	e33=+1e-15;
	for (k=0;k<nw;k++){
		for (j=0;j<nt;j++){
			for (i=0;i<nl;i++){
				m=n(i,j,k)+1;
printf "%05d %e %e %e %e %e %e %d %d %d %d\n",l+0,e11,e12,e13,e22,e23,e33,m+n(0,0,0),m+n(0,1,0),m+n(0,1,1),m+n(1,1,0);
printf "%05d %e %e %e %e %e %e %d %d %d %d\n",l+1,e11,e12,e13,e22,e23,e33,m+n(0,0,0),m+n(1,1,0),m+n(1,0,0),m+n(1,1,1);
printf "%05d %e %e %e %e %e %e %d %d %d %d\n",l+2,e11,e12,e13,e22,e23,e33,m+n(0,1,1),m+n(1,1,0),m+n(1,1,1),m+n(0,0,0);
printf "%05d %e %e %e %e %e %e %d %d %d %d\n",l+3,e11,e12,e13,e22,e23,e33,m+n(0,0,0),m+n(0,1,1),m+n(0,0,1),m+n(1,0,1);
printf "%05d %e %e %e %e %e %e %d %d %d %d\n",l+4,e11,e12,e13,e22,e23,e33,m+n(0,0,0),m+n(1,0,0),m+n(1,0,1),m+n(1,1,1);
printf "%05d %e %e %e %e %e %e %d %d %d %d\n",l+5,e11,e12,e13,e22,e23,e33,m+n(0,0,0),m+n(0,1,1),m+n(1,1,1),m+n(1,0,1);
				l=l+6;
			}
       		}
	}
}' > $TET

echo "" | awk -v nl="$SX" -v nt="$SY" -v nw="$SZ" -v dx="$SDX" 'BEGIN{l=1;pi=atan2(1.0,0.0)*2.0}{
       	x1o=0;
       	x2o=0;
       	x3o=20e3;
       	len=dx;
       	thickness=dx;
       	width=dx;
	strike=90;
       	dip=90;
	s1=cos(pi*strike/180.0)
	s2=sin(pi*strike/180.0)
	s3=0
	d1=-sin(pi*strike/180.0)*cos(pi*dip/180.0)
	d2=+cos(pi*strike/180.0)*cos(pi*dip/180.0)
	d3=+sin(pi*dip/180.0)
	n1=-sin(pi*strike/180.0)*sin(pi*dip/180.0)
	n2=+cos(pi*strike/180.0)*sin(pi*dip/180.0)
	n3=-cos(pi*dip/180.0)
	for (k=0;k<=nw;k++){
		for (j=-nt/2;j<=(nt/2);j++){
			for (i=-nl/2;i<=(nl/2);i++){
				x1=x1o+i*s1*len+j*n1*thickness+k*d1*width;
				x2=x2o+i*s2*len+j*n2*thickness+k*d2*width;
				x3=x3o+i*s3*len+j*n3*thickness+k*d3*width+5e3*(1-exp(-0.5*(x1/20e3)^2));
       		        	printf "%05d %e %e %e\n",l,x1,x2,x3;
				l=l+1;
			}
       		}
	}
}' > $NED

cat <<EOF > $WDIR/in.param
# output directory
$WDIR
# Lame parameter, rigidity (MPa)
30e3 30e3
# time interval (s)
1.5e10
# number of rectangle surface elements
$(grep -cv "#" $FLT)
# n  Vl      x1    x2   x3 length width strike dip rake
$(grep -v "#" $FLT)
# number of frictional patches
$(grep -cv "#" $FLT)
# n tau0 mu0 sig    a     b    L   Vo G/2Vs
$(grep -v "#" $FLT | awk -v dx="$DX" '
	function abs(x){return (x>0)?x:-x};
	function max(x,y){return (x>y)?x:y};
	function boxcar(x){return (x>=-0.5 && x<=0.5)?1:0};
	function heavi(x){return (x>0)?1:0};
	function ramp(x){return x*boxcar(x-0.5)+heavi(x-1)};
	function asinh(x){return log(x+sqrt(1+x^2))};
	function sinh(x){return (exp(x)-exp(-x))/2};
	{ 
	x2=$4+dx/2;
	x3=$5+dx/2;
	mu0=0.6;
	sig=50;
	a0=0.0065;
	amax=0.025;
	a=a0+ramp(max(abs(x3)-10e3,abs(x2)-15e3)/3e3)*(amax-a0);
	b=0.013;
	Vo=1e-6;
	L=5e-2;
	rho=2670;
	Vs=3464;
	G=rho*Vs^2/1e6;
	damping=G/Vs/2;
	Vl=1e-9;
	tau0=-1
	printf "%06d %f %f %f %e %e %e %e %f\n", NR,tau0,mu0,sig,a,b,L,Vo,damping;
	}')
# number of triangle surface elements
0
# number of cuboid volume elements
0
# number of tetrahedron volume elements
$(grep -cv "#" $TET)
# n  e11   e12 e13 e22 e23 e33 i1 i2 i3 i4
$(grep -v "#" $TET)
# number of vertices
$(grep -cv "#" $NED)
# n   x1   x2   x3
$(grep -v "#" $NED)
# number of nonlinear Maxwell volume elements
$(grep -cv "#" $TET)
# n sII gammadot0m n
$(grep -v "#" $TET | awk '{print NR,-1,1e-12,1}')
# number of nonlinear Kelvin volume elements
0
# number of observation patches
7
# n   index rate
$(cat <<EOS | awk -v nl="$NY" -v nw="$NZ" -v dx="$DX" \
	'{i=int($1/dx)+nl/2+1;j=int($2/dx)+1;print NR,i+(j-1)*nl,10}'
 +0.0e3  0.0e3
-10.0e3  0.0e3
+10.0e3  0.0e3
+0.00e3  5.0e3
-10.0e3  5.0e3
+10.0e3  5.0e3
 +0.0e3 15.0e3
EOS
)
# number of observation volumes
0
# number of observation profiles
2
# n   n  offset stride rate
  1 $NY  $(echo $NY 5e3 $DX | awk '{print int($2/$3)*$1}') 1 50
  2 $NZ  $(echo $NY | awk '{print int($1/2)}') $NY 50
# number of observation images
1
# n  nl  nw offset stride rate
  1 $NY $NZ      0      1  100
# number of observation points
$(echo "" | awk '
	function abs(x){return (x>0)?x:-x}
	function sgn(x){return (x>=0)?1:-1}
	function stretch(x){return sgn(x)*abs(x)^1.5/100e3^0.5}
	BEGIN{n1=21;n2=21;dx=10e3}{
	print n1*n2;
	for (i2=0;i2<n2;i2++){
		for (i1=0;i1<n1;i1++){
			printf "%d %04d %f %f 0\n",1+i1+i2*n1,1+i1+i2*n1,stretch((i1-int(n1/2))*dx+10),(i2-int(n2/2))*dx;
		}
	}
}')
# number of perturbations
0
EOF

# load Greens function if they exist
if [ -e $WDIR/greens-0000.grd ]; then
	IMPORT_GREENS="--import-greens $WDIR"
fi
 
time mpirun -n 2 unicycle-3d-viscouscycles \
	$* \
	--epsilon 1e-6 \
	--friction-law 1 \
	--maximum-step 3.15e6 \
	--maximum-iterations 1000000 \
	--export-greens $WDIR \
	--export-netcdf \
 	$IMPORT_GREENS \
	$WDIR/in.param

