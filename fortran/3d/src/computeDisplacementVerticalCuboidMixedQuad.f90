!------------------------------------------------------------------------
!! subroutine computeDisplacementVerticalCuboidMixedQuad
!! computes the displacement field associated with vertical cuboid volume
!! elements using the either the double-exponential numerical integration or
!! the Gauss-Legendre quadrature depending on the distance from the cuboid,
!! as in 
!!
!!   Barbot S., "Deformation of a Half-Space from Anelastic Strain Confined
!!   in a Tetrahedral Volume", Bull. Seism. Soc. Am., 10.1785/0120180058, 2018.
!!
!! and considering the following geometry:
!!
!!                     N (x1)
!!                    /
!!                   /| strike (theta)
!!       q1,q2,q3 ->@-------------------------+   E (x2)
!!                  |                         | w    +
!!                  :                         | i   /
!!                  |                         | d  / s
!!                  :                         | t / s
!!                  |                         | h/ e
!!                  :                         | / n
!!                  +-------------------------+  k
!!                  :        l e n g t h      / c
!!                  |                        / i
!!                  :                       / h
!!                  |                      / t
!!                  :                     /
!!                  |                    +
!!                  Z (x3)
!!
!!
!! INPUT:
!! x1, x2, x3         north, east, and depth coordinates of the observation points
!! q1, q2, q3         north, east and depth coordinates of the cuboid volume
!! L, T, W            length, thickness and width of the cuboid volume
!! theta (radians)    strike angle from north (from x1) of the cuboid volume
!! eijp               source strain component ij in the cuboid volume
!!                    in the system of reference tied to the cuboid
!! nu                 Poisson's ratio in the half space
!!
!! OUTPUT:
!! u1                 displacement component in the north direction
!! u2                 displacement component in the east  direction
!! u3                 displacement component in the down  direction
!!
!! \author Sylvain Barbot (09/24/18) - original Fortran form
!-----------------------------------------------------------------------
SUBROUTINE computeDisplacementVerticalCuboidMixedQuad( &
                        x1,x2,x3,q1,q2,q3,L,T,W,theta, &
                        e11p,e12p,e13p,e22p,e23p,e33p,nu, &
                        u1,u2,u3)

  IMPLICIT NONE

  REAL*8, INTENT(IN) :: x1,x2,x3
  REAL*8, INTENT(IN) :: q1,q2,q3
  REAL*8, INTENT(IN) :: L,T,W,theta
  REAL*8, INTENT(IN) :: e11p,e12p,e13p,e22p,e23p,e33p
  REAL*8, INTENT(IN) :: nu
  REAL*8, INTENT(OUT) :: u1,u2,u3

  REAL*8, PARAMETER :: PI = 3.141592653589793115997963468544185161_8

  REAL*8, DIMENSION(15), PARAMETER :: sk = (/ -0.98799251802048542848956571858661258114697281712376d0, &
                                              -0.9372733924007059043077589477102094712439962735153d0, &
                                              -0.84820658341042721620064832077421685136625617473699d0, &
                                              -0.7244177313601700474161860546139380096308992945841d0, &
                                              -0.57097217260853884753722673725391064123838639628275d0, &
                                              -0.3941513470775633698972073709810454683627527761587d0, &
                                              -0.20119409399743452230062830339459620781283645446264d0, &
                                               0d0, &
                                               0.20119409399743452230062830339459620781283645446264d0, &
                                               0.3941513470775633698972073709810454683627527761587d0, &
                                               0.57097217260853884753722673725391064123838639628275d0, &
                                               0.7244177313601700474161860546139380096308992945841d0, &
                                               0.84820658341042721620064832077421685136625617473699d0, &
                                               0.93727339240070590430775894771020947124399627351531d0, &
                                               0.98799251802048542848956571858661258114697281712376d0 /)

  REAL*8, DIMENSION(15), PARAMETER :: gk = (/  0.03075324199611726835462839357720441772174814483343d0, &
                                               0.07036604748810812470926741645066733846670803275433d0, &
                                               0.1071592204671719350118695466858693034155437157581d0, &
                                               0.13957067792615431444780479451102832252085027531551d0, &
                                               0.16626920581699393355320086048120881113090018009841d0, &
                                               0.1861610000155622110268005618664228245062260122779d0, &
                                               0.19843148532711157645611832644383932481869255995754d0, &
                                               0.20257824192556127288062019996751931483866215800948d0, &
                                               0.19843148532711157645611832644383932481869255995754d0, &
                                               0.1861610000155622110268005618664228245062260122779d0, &
                                               0.16626920581699393355320086048120881113090018009841d0, &
                                               0.13957067792615431444780479451102832252085027531551d0, &
                                               0.1071592204671719350118695466858693034155437157581d0, &
                                               0.070366047488108124709267416450667338466708032754331d0, &
                                               0.03075324199611726835462839357720441772174814483343d0 /)

  ! trace
  REAL*8 :: ekk

  ! moment density
  REAL*8 :: m11,m12,m13,m22,m23,m33

  ! Lame parameter
  REAL*8 :: lambda

  ! numerical integration coordinates and weight
  REAL*8 :: xk,wk,xj,wj

  ! double integration step
  REAL*8 :: h

  ! counter, bound
  INTEGER :: j,k,n

  ! center
  REAL*8, DIMENSION(3) :: O

  ! radius
  REAL*8 :: r

  ! coordinates in primed reference system
  REAL*8 :: x1p, x2p

  ! dummy variable
  REAL*8 :: t1

  ! check valid parameters
  IF ((-1._8 .GT. nu) .OR. (0.5_8 .LT. nu)) THEN
     WRITE (0,'("error: -1<=nu<=0.5, nu=",ES9.2E2," given.")') nu
     STOP 1
  END IF

  IF (0 .GT. x3) THEN
     WRITE (0,'("error: observation depth (x3) must be positive")')
     STOP 1
  END IF

  ! Lame parameter
  lambda=2*nu/(1._8-2*nu)

  ! trace
  ekk=e11p+e22p+e33p

  ! moment density
  m11=2*e11p+lambda*ekk
  m12=2*e12p
  m13=2*e13p
  m22=2*e22p+lambda*ekk
  m23=2*e23p
  m33=2*e33p+lambda*ekk

  ! rotate observation points to the shear-zone-centric system of coordinates
  x1p= (x1-q1)*DCOS(theta)+(x2-q2)*DSIN(theta)
  x2p=-(x1-q1)*DSIN(theta)+(x2-q2)*DCOS(theta)

  ! center of cuboid
  O=(/ q1+L/2*DCOS(theta), q2+L/2*DSIN(theta), q3+W/2 /)

  ! radius
  r=MAX(L/2,W/2,SQRT(W*L))*SQRT(2._8)

  ! initialize displacement
  u1=0._8
  u2=0._8
  u3=0._8

  ! choose integration method based on distance from circumcenter
  IF (SQRT((x1-O(1))**2+(x2-O(2))**2+(x3-O(3))**2) .LE. 1.1d0*r) THEN

     h=0.01
     n=INT(1d0/h*3.5d0)

     ! use the double-exponential method
     DO k=-n,n
        wk=(0.5*h*PI*COSH(k*h))/(COSH(0.5*PI*SINH(k*h)))**2
        xk=TANH(0.5*PI*SINH(k*h))

        DO j=-n,n
           wj=(0.5*h*PI*COSH(j*h))/(COSH(0.5*PI*SINH(j*h)))**2
           xj=TANH(0.5*PI*SINH(j*h))

           u1=u1+wj*wk*IU1(xj,xk)
           u2=u2+wj*wk*IU2(xj,xk)
           u3=u3+wj*wk*IU3(xj,xk)
           
        END DO
     END DO

  ELSE

     ! use the Gauss-Legendre quadrature
     DO k=1,15
        wk=gk(k)
        xk=sk(k)

        DO j=1,15
           wj=gk(j)
           xj=sk(j)

           u1=u1+wj*wk*IU1(xj,xk)
           u2=u2+wj*wk*IU2(xj,xk)
           u3=u3+wj*wk*IU3(xj,xk)

        END DO
     END DO

  END IF

  ! rotate displacement field to reference system of coordinates
  t1=u1*DCOS(theta)-u2*DSIN(theta)
  u2=u1*DSIN(theta)+u2*DCOS(theta)
  u1=t1

CONTAINS

  !---------------------------------------------------------------
  !> function r1
  !! distance from source
  !---------------------------------------------------------------
  REAL*8 FUNCTION r1(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    r1=SQRT((x1p-y1)**2+(x2p-y2)**2+(x3-y3)**2)

  END FUNCTION r1

  !---------------------------------------------------------------
  !> function r2
  !! distance from image
  !---------------------------------------------------------------
  REAL*8 FUNCTION r2(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    r2=SQRT((x1p-y1)**2+(x2p-y2)**2+(x3+y3)**2)

  END FUNCTION r2

  !---------------------------------------------------------------
  !> function G11
  !! Green's function G11 for an elastic half space in 3d
  !! corresponding to the displacement in the x1 direction due to a
  !! point force in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G11(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8 :: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)

    G11=1._8/(16._8*PI*(1._8-nu))*( &
            (3._8-4._8*nu)/lr1+1._8/lr2+(x1p-y1)**2/lr1**3 &
            +(3._8-4._8*nu)*(x1p-y1)**2/lr2**3+2*x3*y3*(lr2**2-3._8*(x1p-y1)**2)/lr2**5 &
            +4._8*(1._8-2*nu)*(1._8-nu)*(lr2**2-(x1p-y1)**2+lr2*(x3+y3))/(lr2*(lr2+x3+y3)**2) &
            )

  END FUNCTION G11

  !---------------------------------------------------------------
  !> function G12
  !! Green's function G12 for an elastic half space in 3d
  !! corresponding to the displacement in the x2 direction due to a
  !! point force in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G12(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8 :: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)

    G12=(x1p-y1)*(x2p-y2)/(16._8*PI*(1._8-nu))*( &
            1._8/lr1**3+(3._8-4._8*nu)/lr2**3-6._8*x3*y3/lr2**5 &
            -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)**2) &
            )

  END FUNCTION G12

  !---------------------------------------------------------------
  !> function G13
  !! Green's function G13 for an elastic half space in 3d
  !! corresponding to the displacement in the x3 direction due to a
  !! point force in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G13(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8 :: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)

    G13=(x1p-y1)/(16._8*PI*(1._8-nu))*( &
            (x3-y3)/lr1**3+(3._8-4._8*nu)*(x3-y3)/lr2**3 &
            -6._8*x3*y3*(x3+y3)/lr2**5+4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)) &
            )

  END FUNCTION G13

  !---------------------------------------------------------------
  !> function G21
  !! Green's function G21 for an elastic half space in 3d
  !! corresponding to the displacement in the x1 direction due to a
  !! point force in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G21(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8 :: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)

    G21=(x1p-y1)*(x2p-y2)/(16._8*PI*(1._8-nu))*( &
            1._8/lr1**3+(3._8-4._8*nu)/lr2**3-6._8*x3*y3/lr2**5 &
            -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)**2) &
            )

  END FUNCTION G21

  !---------------------------------------------------------------
  !> function G22
  !! Green's function G22 for an elastic half space in 3d
  !! corresponding to the displacement in the x2 direction due to a
  !! point force in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G22(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8 :: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)

    G22=1._8/(16._8*PI*(1._8-nu))*( &
            (3._8-4._8*nu)/lr1+1._8/lr2+(x2p-y2)**2/lr1**3 &
            +(3._8-4._8*nu)*(x2p-y2)**2/lr2**3+2*x3*y3*(lr2**2-3._8*(x2p-y2)**2)/lr2**5 &
            +4._8*(1._8-2*nu)*(1._8-nu)*(lr2**2-(x2p-y2)**2+lr2*(x3+y3))/(lr2*(lr2+x3+y3)**2) &
            )

  END FUNCTION G22

  !---------------------------------------------------------------
  !> function G23
  !! Green's function G23 for an elastic half space in 3d
  !! corresponding to the displacement in the x3 direction due to a
  !! point force in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G23(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8 :: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)

    G23=(x2p-y2)/(16._8*PI*(1._8-nu))*( &
            (x3-y3)/lr1**3+(3._8-4._8*nu)*(x3-y3)/lr2**3 &
            -6._8*x3*y3*(x3+y3)/lr2**5+4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)) &
            )

  END FUNCTION G23

  !---------------------------------------------------------------
  !> function G31
  !! Green's function G31 for an elastic half space in 3d
  !! corresponding to the displacement in the x1 direction due to a
  !! point force in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G31(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8 :: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)

    G31=(x1p-y1)/(16._8*PI*(1._8-nu))*( &
            (x3-y3)/lr1**3+(3._8-4._8*nu)*(x3-y3)/lr2**3 &
            +6._8*x3*y3*(x3+y3)/lr2**5-4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)) &
            )

  END FUNCTION G31

  !---------------------------------------------------------------
  !> function G32
  !! Green's function G32 for an elastic half space in 3d
  !! corresponding to the displacement in the x2 direction due to a
  !! point force in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G32(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8 :: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)

    G32=(x2p-y2)/(16._8*PI*(1._8-nu))*( &
            (x3-y3)/lr1**3+(3._8-4._8*nu)*(x3-y3)/lr2**3 &
            +6._8*x3*y3*(x3+y3)/lr2**5-4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)) &
            )

  END FUNCTION G32

  !---------------------------------------------------------------
  !> function G33
  !! Green's function G33 for an elastic half space in 3d
  !! corresponding to the displacement in the x3 direction due to a
  !! point force in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G33(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8 :: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)

    G33=1._8/(16._8*PI*(1._8-nu))*( &
            (3._8-4._8*nu)/lr1+(5._8-12._8*nu+8._8*nu**2)/lr2+(x3-y3)**2/lr1**3 &
            +6._8*x3*y3*(x3+y3)**2/lr2**5+((3._8-4._8*nu)*(x3+y3)**2-2*x3*y3)/lr2**3 &
            )

  END FUNCTION G33

  !---------------------------------------------------------------
  !> function IU1
  !! function IU1 is the integrand for displacement component u1
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU1(x,y)
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU1=0._8

    IF (0._8 .NE. m11) THEN
       IU1=IU1+m11*T*W/4._8*(G11(L,x*T/2,(1+y)*W/2+q3)-G11(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU1=IU1+m12*T*W/4._8*(G21(L,x*T/2,(1+y)*W/2+q3)-G21(0._8,x*T/2,(1+y)*W/2+q3)) &
              +m12*L*W/4._8*(G11((1+x)*L/2,T/2,(1+y)*W/2+q3)-G11((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU1=IU1+m13*T*W/4._8*(G31(L,x*T/2,(1+y)*W/2+q3)-G31(0._8,x*T/2,(1+y)*W/2+q3)) &
              +m13*L*T/4._8*(G11((1+x)*L/2,y*T/2,W+q3)-G11((1+x)*L/2,y*T/2,q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU1=IU1+m22*L*W/4._8*(G21((1+x)*L/2,T/2,(1+y)*W/2+q3)-G21((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU1=IU1+m23*L*T/4._8*(G21((1+x)*L/2,y*T/2,W+q3)-G21((1+x)*L/2,y*T/2,q3)) &
              +m23*L*W/4._8*(G31((1+x)*L/2,T/2,(1+y)*W/2+q3)-G31((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU1=IU1+m33*L*T/4._8*(G31((1+x)*L/2,y*T/2,W+q3)-G31((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU1

  !---------------------------------------------------------------
  !> function IU2
  !! function IU2 is the integrand for displacement component u2
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU2(x,y)
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU2=0._8

    IF (0._8 .NE. m11) THEN
       IU2=IU2+m11*T*W/4._8*(G12(L,x*T/2,(1+y)*W/2+q3)-G12(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU2=IU2+m12*T*W/4._8*(G22(L,x*T/2,(1+y)*W/2+q3)-G22(0._8,x*T/2,(1+y)*W/2+q3)) &
              +m12*L*W/4._8*(G12((1+x)*L/2,T/2,(1+y)*W/2+q3)-G12((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU2=IU2+m13*T*W/4._8*(G32(L,x*T/2,(1+y)*W/2+q3)-G32(0._8,x*T/2,(1+y)*W/2+q3)) &
              +m13*L*T/4._8*(G12((1+x)*L/2,y*T/2,W+q3)-G12((1+x)*L/2,y*T/2,q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU2=IU2+m22*L*W/4._8*(G22((1+x)*L/2,T/2,(1+y)*W/2+q3)-G22((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU2=IU2+m23*L*T/4._8*(G22((1+x)*L/2,y*T/2,W+q3)-G22((1+x)*L/2,y*T/2,q3)) &
              +m23*L*W/4._8*(G32((1+x)*L/2,T/2,(1+y)*W/2+q3)-G32((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU2=IU2+m33*L*T/4._8*(G32((1+x)*L/2,y*T/2,W+q3)-G32((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU2

  !---------------------------------------------------------------
  !> function IU3
  !! function IU3 is the integrand for displacement component u3
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU3(x,y)
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU3=0._8

    IF (0._8 .NE. m11) THEN
       IU3=IU3+m11*T*W/4._8*(G13(L,x*T/2,(1+y)*W/2+q3)-G13(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU3=IU3+m12*T*W/4._8*(G23(L,x*T/2,(1+y)*W/2+q3)-G23(0._8,x*T/2,(1+y)*W/2+q3)) &
              +m12*L*W/4._8*(G13((1+x)*L/2,T/2,(1+y)*W/2+q3)-G13((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU3=IU3+m13*L*T/4._8*(G13((1+x)*L/2,y*T/2,W+q3)-G13((1+x)*L/2,y*T/2,q3)) &
              +m13*T*W/4._8*(G33(L,x*T/2,(1+y)*W/2+q3)-G33(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU3=IU3+m22*L*W/4._8*(G23((1+x)*L/2,T/2,(1+y)*W/2+q3)-G23((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU3=IU3+m23*L*T/4._8*(G23((1+x)*L/2,y*T/2,W+q3)-G23((1+x)*L/2,y*T/2,q3)) &
              +m23*L*W/4._8*(G33((1+x)*L/2,T/2,(1+y)*W/2+q3)-G33((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU3=IU3+m33*L*T/4._8*(G33((1+x)*L/2,y*T/2,W+q3)-G33((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU3

END SUBROUTINE computeDisplacementVerticalCuboidMixedQuad

