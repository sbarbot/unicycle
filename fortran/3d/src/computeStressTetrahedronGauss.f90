!------------------------------------------------------------------------
!! subroutine computeStressTetrahedronGauss
!! computes the stress field associated with a tetrahedron volume element
!! using the Gauss-Legendre quadrature with various orders depending on
!! the source-receiver distance, as in 
!!
!!   Barbot S., "Deformation of a Half-Space from Anelastic Strain Confined
!!   in a Tetrahedral Volume", Bull. Seism. Soc. Am., 10.1785/0120180058, 2018.
!!
!! and considering the following geometry:
!!
!!                      / North (x1)
!!                     /
!!        surface     /
!!      -------------+-------------- East (x2)
!!                  /|
!!                 / |     + A
!!                /  |    /  .
!!                   |   /     .
!!                   |  /        .
!!                   | /           .
!!                   |/              + B
!!                   /            .  |
!!                  /|          /    |
!!                 / :       .       |
!!                /  |    /          |
!!               /   : .             |
!!              /   /|               |
!!             / .   :               |
!!            +------|---------------+
!!          C        :                 D
!!                   |
!!                   Down (x3)
!!
!!
!! INPUT:
!! x1, x2, x3     north, east, and depth coordinates of the observation points
!! A, B, C, D     north, east, and depth coordinates of the tetrahedron vertices,
!! eijp           source strain component ij in the tetrahedron volume
!! G, nu          rigidity and Poisson's ratio in the half space
!!
!! OUTPUT:
!! sij            stress components in the north, east, depth reference system.
!!
!! \author Sylvain Barbot (06/07/22) - original Fortran form
!-----------------------------------------------------------------------
SUBROUTINE computeStressTetrahedronGauss( &
                        x1,x2,x3,A,B,C,D, &
                        e11p,e12p,e13p,e22p,e23p,e33p,G,nu, &
                        s11,s12,s13,s22,s23,s33)

  IMPLICIT NONE

  REAL*8, INTENT(IN) :: x1,x2,x3
  REAL*8, DIMENSION(3), INTENT(IN) :: A, B, C, D
  REAL*8, INTENT(IN) :: e11p,e12p,e13p,e22p,e23p,e33p
  REAL*8, INTENT(IN) :: G,nu
  REAL*8, INTENT(OUT) :: s11,s12,s13,s22,s23,s33

  REAL*8, PARAMETER :: PI = 3.141592653589793115997963468544185161_8

  ! Gauss-Legendre quadrature positions and weights
  INCLUDE 'gauss-3.inc'
  INCLUDE 'gauss-7.inc'
  INCLUDE 'gauss-15.inc'
  INCLUDE 'gauss-40.inc'

  ! trace
  REAL*8 :: ekk

  ! moment density
  REAL*8 :: m11,m12,m13,m22,m23,m33

  ! displacement gradient
  REAL*8 :: u11,u12,u13,u21,u22,u23,u31,u32,u33

  ! strain components in unprimed coordinate system
  REAL*8 :: e11,e12,e13,e22,e23,e33

  ! triangle surface areas
  REAL*8 :: ABC,BCD,CDA,DAB

  ! normal vectors
  REAL*8, DIMENSION(3) :: nA,nB,nC,nD

  ! mask
  REAL*8 :: omega

  ! Lame parameter
  REAL*8 :: lambda

  ! numerical integration coordinates and weight
  REAL*8 :: xk,wk,xj,wj

  ! counters
  INTEGER :: j,k

  ! centroid
  REAL*8, DIMENSION(3) :: O

  ! temporary vector
  REAL*8, DIMENSION(3) :: v

  ! length scale
  REAL*8 :: r

  ! relative source-receiver distance
  REAL*8 :: rho

  ! check valid parameters
  IF ((-1._8 .GT. nu) .OR. (0.5_8 .LT. nu)) THEN
     WRITE (0,'("error: -1<=nu<=0.5, nu=",ES9.2E2," given.")') nu
     STOP 1
  END IF

  IF (0 .GT. x3) THEN
     WRITE (0,'("error: observation depth (x3) must be positive")')
     STOP 1
  END IF

  IF (0 .GT. MIN(A(3),B(3),C(3),D(3))) THEN
     WRITE (0,'("error: source depth must be positive")')
     STOP 1
  END IF

  ! Lame parameter (normalized by rigidity)
  lambda=2*nu/(1._8-2*nu)

  ! trace
  ekk=e11p+e22p+e33p

  ! unit normal vectors
  CALL cross(C-B,D-B,nA) ! BCD
  CALL cross(D-C,A-C,nB) ! CDA
  CALL cross(A-D,B-D,nC) ! DAB
  CALL cross(B-A,C-A,nD) ! ABC

  nA=nA/NORM2(nA)
  nB=nB/NORM2(nB)
  nC=nC/NORM2(nC)
  nD=nD/NORM2(nD)
  
  ! check that unit vectors are pointing outward
  IF (0 .LT. DOT_PRODUCT(nA,A-(B+C+D)/3)) THEN
      nA=-nA
  END IF
  IF (0 .LT. DOT_PRODUCT(nB,B-(A+C+D)/3)) THEN
      nB=-nB
  END IF
  IF (0 .LT. DOT_PRODUCT(nC,C-(A+B+D)/3)) THEN
      nC=-nC
  END IF
  IF (0 .LT. DOT_PRODUCT(nD,D-(A+B+C)/3)) THEN
      nD=-nD
  END IF

  ! area of triangle ABC
  CALL cross(C-A,B-A,v)
  ABC=NORM2(v)/2
  ! area of triangle BCD
  CALL cross(D-B,C-B,v)
  BCD=NORM2(v)/2
  ! area of triangle CDA
  CALL cross(A-C,D-C,v)
  CDA=NORM2(v)/2
  ! area of triangle DAB
  CALL cross(B-D,A-D,v)
  DAB=NORM2(v)/2

  ! moment density (normalized by rigidity)
  m11=2*e11p+lambda*ekk
  m12=2*e12p
  m13=2*e13p
  m22=2*e22p+lambda*ekk
  m23=2*e23p
  m33=2*e33p+lambda*ekk

  ! initialize displacement gradients
  u11=0.d0; u12=0.d0; u13=0.d0
  u21=0.d0; u22=0.d0; u23=0.d0
  u31=0.d0; u32=0.d0; u33=0.d0

  ! centroid
  O=(A+B+C+D)/4._8

  ! length scale
  r=MAX(NORM2(A-O),NORM2(B-O),NORM2(C-O),NORM2(D-O))

  ! relative source-receiver distance
  rho=SQRT((x1-O(1))**2+(x2-O(2))**2+(x3-O(3))**2)/r

  ! choose integration method based on distance from centroid
  IF (rho .LE. 1.2d0) THEN
     ! Gauss-Legendre quadrature with 40 points
     DO k=1,40
        xk=p40(k)
        wk=w40(k)

        DO j=1,40
           xj=p40(j)
           wj=w40(j)

           ! derivatives of the u1 component
           u11=u11+wj*wk*(1-xk)*IU11(xj,xk)
           u12=u12+wj*wk*(1-xk)*IU12(xj,xk)
           u13=u13+wj*wk*(1-xk)*IU13(xj,xk)
           
           ! derivatives of the u2 component
           u21=u21+wj*wk*(1-xk)*IU21(xj,xk)
           u22=u22+wj*wk*(1-xk)*IU22(xj,xk)
           u23=u23+wj*wk*(1-xk)*IU23(xj,xk)
           
           ! derivatives of the u3 component
           u31=u31+wj*wk*(1-xk)*IU31(xj,xk)
           u32=u32+wj*wk*(1-xk)*IU32(xj,xk)
           u33=u33+wj*wk*(1-xk)*IU33(xj,xk)
           
        END DO
     END DO

  ELSE IF (rho .LE. 10.0d0) THEN
     ! Gauss-Legendre quadrature with 15 points
     DO k=1,15
        xk=p15(k)
        wk=w15(k)

        DO j=1,15
           xj=p15(j)
           wj=w15(j)

           ! derivatives of the u1 component
           u11=u11+wj*wk*(1-xk)*IU11(xj,xk)
           u12=u12+wj*wk*(1-xk)*IU12(xj,xk)
           u13=u13+wj*wk*(1-xk)*IU13(xj,xk)

           ! derivatives of the u2 component
           u21=u21+wj*wk*(1-xk)*IU21(xj,xk)
           u22=u22+wj*wk*(1-xk)*IU22(xj,xk)
           u23=u23+wj*wk*(1-xk)*IU23(xj,xk)

           ! derivatives of the u3 component
           u31=u31+wj*wk*(1-xk)*IU31(xj,xk)
           u32=u32+wj*wk*(1-xk)*IU32(xj,xk)
           u33=u33+wj*wk*(1-xk)*IU33(xj,xk)

        END DO
     END DO
  ELSE IF (rho .LE. 20.0d0) THEN
     ! Gauss-Legendre quadrature with 7 points
     DO k=1,7
        xk=p7(k)
        wk=w7(k)

        DO j=1,7
           xj=p7(j)
           wj=w7(j)

           ! derivatives of the u1 component
           u11=u11+wj*wk*(1-xk)*IU11(xj,xk)
           u12=u12+wj*wk*(1-xk)*IU12(xj,xk)
           u13=u13+wj*wk*(1-xk)*IU13(xj,xk)

           ! derivatives of the u2 component
           u21=u21+wj*wk*(1-xk)*IU21(xj,xk)
           u22=u22+wj*wk*(1-xk)*IU22(xj,xk)
           u23=u23+wj*wk*(1-xk)*IU23(xj,xk)

           ! derivatives of the u3 component
           u31=u31+wj*wk*(1-xk)*IU31(xj,xk)
           u32=u32+wj*wk*(1-xk)*IU32(xj,xk)
           u33=u33+wj*wk*(1-xk)*IU33(xj,xk)

        END DO
     END DO
  ELSE
     ! Gauss-Legendre quadrature with 3 points
     DO k=1,3
        xk=p3(k)
        wk=w3(k)

        DO j=1,3
           xj=p3(j)
           wj=w3(j)

           ! derivatives of the u1 component
           u11=u11+wj*wk*(1-xk)*IU11(xj,xk)
           u12=u12+wj*wk*(1-xk)*IU12(xj,xk)
           u13=u13+wj*wk*(1-xk)*IU13(xj,xk)

           ! derivatives of the u2 component
           u21=u21+wj*wk*(1-xk)*IU21(xj,xk)
           u22=u22+wj*wk*(1-xk)*IU22(xj,xk)
           u23=u23+wj*wk*(1-xk)*IU23(xj,xk)

           ! derivatives of the u3 component
           u31=u31+wj*wk*(1-xk)*IU31(xj,xk)
           u32=u32+wj*wk*(1-xk)*IU32(xj,xk)
           u33=u33+wj*wk*(1-xk)*IU33(xj,xk)

        END DO
     END DO
  END IF

  ! one inside the tetrahedron, zero outside
  omega=heaviside(((A(1)+B(1)+C(1))/3-x1)*nD(1)+((A(2)+B(2)+C(2))/3-x2)*nD(2)+((A(3)+B(3)+C(3))/3-x3)*nD(3)) &
       *heaviside(((B(1)+C(1)+D(1))/3-x1)*nA(1)+((B(2)+C(2)+D(2))/3-x2)*nA(2)+((B(3)+C(3)+D(3))/3-x3)*nA(3)) &
       *heaviside(((C(1)+D(1)+A(1))/3-x1)*nB(1)+((C(2)+D(2)+A(2))/3-x2)*nB(2)+((C(3)+D(3)+A(3))/3-x3)*nB(3)) &
       *heaviside(((D(1)+A(1)+B(1))/3-x1)*nC(1)+((D(2)+A(2)+B(2))/3-x2)*nC(2)+((D(3)+A(3)+B(3))/3-x3)*nC(3))

  ! elastic strain (total strain minus anelastic eigenstrain)
  e11= u11       -omega*e11p
  e12=(u12+u21)/2-omega*e12p
  e13=(u13+u31)/2-omega*e13p
  e22= u22       -omega*e22p
  e23=(u23+u32)/2-omega*e23p
  e33= u33       -omega*e33p

  ! trace
  ekk=e11+e22+e33

  ! stress components
  s11=G*(2*e11+lambda*ekk)
  s12=G*(2*e12)
  s13=G*(2*e13)
  s22=G*(2*e22+lambda*ekk)
  s23=G*(2*e23)
  s33=G*(2*e33+lambda*ekk)

CONTAINS

  !--------------------------------------------------
  !> subroutine circumcenter
  !! compute the circumsphere center of a tetrahedron
  !--------------------------------------------------
  SUBROUTINE circumcenter(A,B,C,D,O)
    IMPLICIT NONE
    REAL*8, DIMENSION(3), INTENT(IN) :: A,B,C,D
    REAL*8, DIMENSION(3), INTENT(OUT) :: O

    REAL*8 :: det,As,Bs,Cs,Ds

    det=A(1)*( B(2)*(C(3)-D(3))+B(3)*(D(2)-C(2))-D(2)*C(3)+C(2)*D(3) ) &
       +A(2)*( B(1)*(D(3)-C(3))+B(3)*(C(1)-D(1))+D(1)*C(3)-D(3)*C(1) ) &
       +A(3)*( B(1)*(C(2)-D(2))+B(2)*(D(1)-C(1))-D(1)*C(2)+D(2)*C(1) ) &
       +B(1)*(-C(2)*D(3)+C(3)*D(2) ) &
       +B(2)*( C(1)*D(3)-C(3)*D(1) ) &
       +B(3)*(-C(1)*D(2)+C(2)*D(1) )

    As=SUM(A*A)
    Bs=SUM(B*B)
    Cs=SUM(C*C)
    Ds=SUM(D*D)

    O(1)=(A(2)*B(3)*Cs-A(2)*B(3)*Ds-A(2)*Bs*C(3) &
         +A(2)*Bs*D(3)+A(2)*C(3)*Ds-A(2)*Cs*D(3) &
         -A(3)*B(2)*Cs+A(3)*B(2)*Ds+A(3)*Bs*C(2) &
         -A(3)*Bs*D(2)-A(3)*C(2)*Ds+A(3)*Cs*D(2) &
         +As*B(2)*C(3)-As*B(2)*D(3)-As*B(3)*C(2) &
         +As*B(3)*D(2)+As*C(2)*D(3)-As*C(3)*D(2) &
         -B(2)*C(3)*Ds+B(2)*Cs*D(3)+B(3)*C(2)*Ds &
         -B(3)*Cs*D(2)-Bs*C(2)*D(3)+Bs*C(3)*D(2))/det/2
    O(2)=(-A(1)*B(3)*Cs+A(1)*B(3)*Ds+A(1)*Bs*C(3) &
         -A(1)*Bs*D(3)-A(1)*C(3)*Ds+A(1)*Cs*D(3) &
         +A(3)*B(1)*Cs-A(3)*B(1)*Ds-A(3)*Bs*C(1) &
         +A(3)*Bs*D(1)+A(3)*C(1)*Ds-A(3)*Cs*D(1) &
         -As*B(1)*C(3)+As*B(1)*D(3)+As*B(3)*C(1) &
         -As*B(3)*D(1)-As*C(1)*D(3)+As*C(3)*D(1) &
         +B(1)*C(3)*Ds-B(1)*Cs*D(3)-B(3)*C(1)*Ds &
         +B(3)*Cs*D(1)+Bs*C(1)*D(3)-Bs*C(3)*D(1))/det/2
    O(3)=(A(1)*B(2)*Cs-A(1)*B(2)*Ds-A(1)*Bs*C(2) &
         +A(1)*Bs*D(2)+A(1)*C(2)*Ds-A(1)*Cs*D(2) &
         -A(2)*B(1)*Cs+A(2)*B(1)*Ds+A(2)*Bs*C(1) &
         -A(2)*Bs*D(1)-A(2)*C(1)*Ds+A(2)*Cs*D(1) &
         +As*B(1)*C(2)-As*B(1)*D(2)-As*B(2)*C(1) &
         +As*B(2)*D(1)+As*C(1)*D(2)-As*C(2)*D(1) &
         -B(1)*C(2)*Ds+B(1)*Cs*D(2)+B(2)*C(1)*Ds &
         -B(2)*Cs*D(1)-Bs*C(1)*D(2)+Bs*C(2)*D(1))/det/2

  END SUBROUTINE circumcenter

  !-----------------------------------------------
  !> subroutine cross
  !! compute the cross product between two vectors
  !-----------------------------------------------
  SUBROUTINE cross(u,v,w)
    REAL*8, DIMENSION(3), INTENT(IN) :: u,v
    REAL*8, DIMENSION(3), INTENT(OUT) :: w

    w(1)=u(2)*v(3)-u(3)*v(2)
    w(2)=u(3)*v(1)-u(1)*v(3)
    w(3)=u(1)*v(2)-u(2)*v(1)

  END SUBROUTINE cross

  !---------------------------------------------------------------
  !> function y
  !! parameterized surface integral over a triangle
  !---------------------------------------------------------------
  REAL*8 FUNCTION y(u,v,A,B,C)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: u,v,A,B,C

    y=A*(1-u)*(1-v)/4+B*(1+u)*(1-v)/4+C*(1+v)/2

  END FUNCTION y

  !---------------------------------------------------------------
  !> function r1
  !! distance from source
  !---------------------------------------------------------------
  REAL*8 FUNCTION r1(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    r1=SQRT((x1-y1)**2+(x2-y2)**2+(x3-y3)**2)

  END FUNCTION r1

  !---------------------------------------------------------------
  !> function r2
  !! distance from image
  !---------------------------------------------------------------
  REAL*8 FUNCTION r2(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    r2=SQRT((x1-y1)**2+(x2-y2)**2+(x3+y3)**2)

  END FUNCTION r2

  !---------------------------------------------------------------
  !> function G11d1
  !! Green's function G11d1 for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,1 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G11d1(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G11d1=1._8/(16._8*PI*(1._8-nu))*(x1-y1)*( &
       -(3._8-4._8*nu)/lr1**3 &
       -1._8/lr2**3 &
       +(2*lr1**2-3._8*(x1-y1)**2)/lr1**5 &
       +(3._8-4._8*nu)*(2*lr2**2-3._8*(x1-y1)**2)/lr2**5 &
       -6._8*y3*x3*(3._8*lr2**2-5._8*(x1-y1)**2)/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)**2) &
       -8._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)**2) &
           +4._8*(1._8-2*nu)*(1._8-nu)*(x1-y1)**2/(lr2**3*(lr2+x3+y3)**2) &
           +8._8*(1._8-2*nu)*(1._8-nu)*(x1-y1)**2/(lr2**2*(lr2+x3+y3)**3) &
       )

  END FUNCTION G11d1

  !---------------------------------------------------------------
  !> function G11d2
  !! Green's function G11d2 for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,2 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G11d2(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G11d2=1._8/(16._8*PI*(1._8-nu))*(x2-y2)*( &
       -(3._8-4._8*nu)/lr1**3 &
       -1._8/lr2**3 &
       -3._8*(x1-y1)**2/lr1**5 &
       -3._8*(3-4*nu)*(x1-y1)**2/lr2**5 &
       -6._8*y3*x3*(lr2**2-5._8*(x1-y1)**2)/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)**2) &
       +4._8*(1._8-2*nu)*(1._8-nu)*(x1-y1)**2*(3._8*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**3) &
    )

  END FUNCTION G11d2

  !---------------------------------------------------------------
  !> function G11d3
  !! Green's function G11d3 for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,3 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G11d3(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
    
    G11d3=1._8/(16._8*PI*(1._8-nu))*( &
       -(3._8-4._8*nu)*(x3-y3)/lr1**3 &
       -(x3+y3)/lr2**3 &
       -3._8*(x1-y1)**2*(x3-y3)/lr1**5 &
       -3._8*(3._8-4*nu)*(x1-y1)**2*(x3+y3)/lr2**5 &
       +2*y3*(lr2**2-3._8*x3*(x3+y3))/lr2**5 &
       -6._8*y3*(x1-y1)**2*(lr2**2-5._8*x3*(x3+y3))/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)) &
       +4._8*(1._8-2*nu)*(1._8-nu)*(x1-y1)**2*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G11d3

  !---------------------------------------------------------------
  !> function G21d1
  !! Green's function G21d1 for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,1 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G21d1(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G21d1=1._8/(16._8*PI*(1._8-nu))*(x2-y2)*( &
    +(lr1**2-3._8*(x1-y1)**2)/lr1**5 &
    +(3._8-4._8*nu)*(lr2**2-3._8*(x1-y1)**2)/lr2**5 &
    -6._8*y3*x3*(lr2**2-5._8*(x1-y1)**2)/lr2**7 &
    -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)**2) &
    +4._8*(1._8-2*nu)*(1._8-nu)*(x1-y1)**2*(3._8*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**3) &
    )

  END FUNCTION G21d1

  !---------------------------------------------------------------
  !> function G21d2
  !! Green's function G21d2 for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,2 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G21d2(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G21d2=1._8/(16._8*PI*(1._8-nu))*(x1-y1)*( &
       +(lr1**2-3._8*(x2-y2)**2)/lr1**5 &
       +(3._8-4._8*nu)*(lr2**2-3._8*(x2-y2)**2)/lr2**5 &
       -6._8*y3*x3*(lr2**2-5._8*(x2-y2)**2)/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)**2) &
       +4._8*(1._8-2*nu)*(1._8-nu)*(x2-y2)**2*(3._8*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**3) &
    )

  END FUNCTION G21d2

  !---------------------------------------------------------------
  !> function G21d3
  !! Green's function G21d3 for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,3 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G21d3(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G21d3=1._8/(16._8*PI*(1._8-nu))*(x1-y1)*(x2-y2)*( &
       -3._8*(x3-y3)/lr1**5 &
       -3._8*(3._8-4._8*nu)*(x3+y3)/lr2**5 &
       -6._8*y3*(lr2**2-5._8*x3*(x3+y3))/lr2**7 &
       +4._8*(1._8-2*nu)*(1._8-nu)*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G21d3

  !---------------------------------------------------------------
  !> function G31d1
  !! Green's function G31d1 for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,1 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G31d1(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
    
    G31d1=1._8/(16._8*PI*(1._8-nu))*( &
       +(x3-y3)*(lr1**2-3._8*(x1-y1)**2)/lr1**5 &
       +(3._8-4._8*nu)*(x3-y3)*(lr2**2-3*(x1-y1)**2)/lr2**5 &
       +6._8*y3*x3*(x3+y3)*(lr2**2-5*(x1-y1)**2)/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)) &
       +4._8*(1._8-2*nu)*(1._8-nu)*(x1-y1)**2*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G31d1

  !---------------------------------------------------------------
  !> function G31d2
  !! Green's function G31d2 for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,2 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G31d2(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G31d2=1._8/(16._8*PI*(1._8-nu))*(x1-y1)*(x2-y2)*( &
       -3._8*(x3-y3)/lr1**5 &
       -3._8*(3._8-4._8*nu)*(x3-y3)/lr2**5 &
       -30._8*y3*x3*(x3+y3)/lr2**7 &
       +4._8*(1._8-2*nu)*(1._8-nu)*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G31d2

  !---------------------------------------------------------------
  !> function G31d3
  !! Green's function G31d3 for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,3 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G31d3(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G31d3=1._8/(16._8*PI*(1._8-nu))*(x1-y1)*( &
       +(lr1**2-3*(x3-y3)**2)/lr1**5 &
       +(3._8-4._8*nu)*(lr2**2-3._8*(x3**2-y3**2))/lr2**5 &
       +6._8*y3*(2*x3+y3)/lr2**5 &
       -30._8*y3*x3*(x3+y3)**2/lr2**7 &
       +4._8*(1._8-2*nu)*(1-nu)/lr2**3 &
    )

  END FUNCTION G31d3

  !---------------------------------------------------------------
  !> function G12d1
  !! Green's function G12d1 for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,1 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G12d1(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G12d1=1._8/(16._8*PI*(1._8-nu))*(x2-y2)*( &
       +(lr1**2-3*(x1-y1)**2)/lr1**5 &
       +(3._8-4*nu)*(lr2**2-3*(x1-y1)**2)/lr2**5 &
       -6._8*y3*x3*(lr2**2-5*(x1-y1)**2)/lr2**7 &
       -4._8*(1._8-nu)*(1._8-2*nu)/(lr2*(lr2+x3+y3)**2) &
       +4._8*(1._8-nu)*(1._8-2*nu)*(x1-y1)**2*(3._8*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**3) &
    )

  END FUNCTION G12d1

  !---------------------------------------------------------------
  !> function G12d2
  !! Green's function G12d2 for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,2 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G12d2(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G12d2=1._8/(16._8*PI*(1._8-nu))*(x1-y1)*( &
       +(lr1**2-3._8*(x2-y2)**2)/lr1**5 &
       +(3._8-4._8*nu)*(lr2**2-3._8*(x2-y2)**2)/lr2**5 &
       -6._8*y3*x3*(lr2**2-5._8*(x2-y2)**2)/lr2**7 &
       -4._8*(1._8-nu)*(1._8-2*nu)/(lr2*(lr2+x3+y3)**2) &
       +4._8*(1._8-nu)*(1._8-2*nu)*(x2-y2)**2*(3*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**3) &
    )

  END FUNCTION G12d2

  !---------------------------------------------------------------
  !> function G12d3
  !! Green's function G12d3 for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,3 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G12d3(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G12d3=1._8/(16._8*PI*(1._8-nu))*(x1-y1)*(x2-y2)*( &
    -3._8*(x3-y3)/lr1**5 &
    -3._8*(3._8-4._8*nu)*(x3+y3)/lr2**5 &
    -6._8*y3*(lr2**2-5*x3*(x3+y3))/lr2**7 &
    +4._8*(1._8-2*nu)*(1._8-nu)*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G12d3

  !---------------------------------------------------------------
  !> function G22d1
  !! Green's function G22d1 for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,1 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G22d1(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G22d1=1._8/(16._8*PI*(1._8-nu))*(x1-y1)*( &
       -(3._8-4._8*nu)/lr1**3 &
       -1._8/lr2**3 &
       -3._8*(x2-y2)**2/lr1**5 &
       -3._8*(3._8-4._8*nu)*(x2-y2)**2/lr2**5 &
       -6._8*y3*x3*(lr2**2-5._8*(x2-y2)**2)/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)**2) &
       +4._8*(1._8-2*nu)*(1._8-nu)*(x2-y2)**2*(3._8*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**3) &
    )

  END FUNCTION G22d1

  !---------------------------------------------------------------
  !> function G22d2
  !! Green's function G22d2 for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,2 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G22d2(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G22d2=1._8/(16._8*PI*(1._8-nu))*(x2-y2)*( &
       -(3._8-4._8*nu)/lr1**3 &
       -1._8/lr2**3 &
       +(2*lr1**2-3._8*(x2-y2)**2)/lr1**5 &
       +(3._8-4._8*nu)*(2*lr2**2-3._8*(x2-y2)**2)/lr2**5 &
       -6._8*y3*x3*(3._8*lr2**2-5*(x2-y2)**2)/lr2**7 &
       -12._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)**2) &
       +4._8*(1._8-2*nu)*(1._8-nu)*(x2-y2)**2*(3._8*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**3) &
    )

  END FUNCTION G22d2

  !---------------------------------------------------------------
  !> function G22d3
  !! Green's function G22d3 for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,3 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G22d3(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G22d3=1._8/(16._8*PI*(1._8-nu))*( &
       -(3._8-4._8*nu)*(x3-y3)/lr1**3 &
       -(x3+y3)/lr2**3 &
       -3._8*(x2-y2)**2*(x3-y3)/lr1**5 &
       -3._8*(3-4*nu)*(x2-y2)**2*(x3+y3)/lr2**5 &
       +2*y3*(lr2**2-3*x3*(x3+y3))/lr2**5 &
       -6._8*y3*(x2-y2)**2*(lr2**2-5*x3*(x3+y3))/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)) &
       +4._8*(1._8-2*nu)*(1._8-nu)*(x2-y2)**2*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G22d3

  !---------------------------------------------------------------
  !> function G32d1
  !! Green's function G32d1 for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,1 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G32d1(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G32d1=1._8/(16._8*PI*(1._8-nu))*(x1-y1)*(x2-y2)*( &
       -3._8*(x3-y3)/lr1**5 &
       -3._8*(3._8-4._8*nu)*(x3-y3)/lr2**5 &
       -30._8*y3*x3*(x3+y3)/lr2**7 &
       +4._8*(1._8-2*nu)*(1._8-nu)*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G32d1

  !---------------------------------------------------------------
  !> function G32d2
  !! Green's function G32d2 for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,2 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G32d2(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G32d2=1._8/(16._8*PI*(1._8-nu))*( &
       +(x3-y3)*(lr1**2-3*(x2-y2)**2)/lr1**5 &
       +(3._8-4._8*nu)*(x3-y3)*(lr2**2-3*(x2-y2)**2)/lr2**5 &
       +6._8*y3*x3*(x3+y3)*(lr2**2-5*(x2-y2)**2)/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)) &
       +4._8*(1._8-2*nu)*(1._8-nu)*(x2-y2)**2*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G32d2

  !---------------------------------------------------------------
  !> function G32d3
  !! Green's function G32d3 for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,3 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G32d3(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G32d3=1._8/(16._8*PI*(1._8-nu))*(x2-y2)*( &
       +(lr1**2-3*(x3-y3)**2)/lr1**5 &
       +(3._8-4._8*nu)*(lr2**2-3*(x3**2-y3**2))/lr2**5 &
       +6._8*y3*(2*x3+y3)/lr2**5 &
       -30._8*y3*x3*(x3+y3)**2/lr2**7 &
       +4._8*(1._8-2*nu)*(1-nu)/lr2**3 &
    )

  END FUNCTION G32d3

  !---------------------------------------------------------------
  !> function G13d1
  !! Green's function G13d1 for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,1 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G13d1(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G13d1=1._8/(16._8*PI*(1._8-nu))*( &
       +(x3-y3)*(lr1**2-3._8*(x1-y1)**2)/lr1**5 &
       +(3._8-4*nu)*(x3-y3)*(lr2**2-3*(x1-y1)**2)/lr2**5 &
       -6._8*y3*x3*(x3+y3)*(lr2**2-5*(x1-y1)**2)/lr2**7 &
       +4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)) &
       -4._8*(1._8-2*nu)*(1._8-nu)*(x1-y1)**2*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G13d1

  !---------------------------------------------------------------
  !> function G13d2
  !! Green's function G13d2 for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,2 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G13d2(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G13d2=1._8/(16._8*PI*(1._8-nu))*(x1-y1)*(x2-y2)*( &
       -3._8*(x3-y3)/lr1**5 &
       -3._8*(3._8-4._8*nu)*(x3-y3)/lr2**5 &
       +30._8*y3*x3*(x3+y3)/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G13d2

  !---------------------------------------------------------------
  !> function G13d3
  !! Green's function G13d3 for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,3 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G13d3(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G13d3=1._8/(16._8*PI*(1._8-nu))*(x1-y1)*( &
       +(lr1**2-3*(x3-y3)**2)/lr1**5 &
       +(3._8-4*nu)*(lr2**2-3*(x3**2-y3**2))/lr2**5 &
       -6._8*y3*(2*x3+y3)/lr2**5 &
       +30._8*y3*x3*(x3+y3)**2/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/lr2**3 &
    )

  END FUNCTION G13d3

  !---------------------------------------------------------------
  !> function G23d1
  !! Green's function G23d1 for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,1 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G23d1(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G23d1=1._8/(16._8*PI*(1._8-nu))*(x1-y1)*(x2-y2)*( &
       -3._8*(x3-y3)/lr1**5 &
       -3._8*(3._8-4._8*nu)*(x3-y3)/lr2**5 &
       +30._8*y3*x3*(x3+y3)/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G23d1

  !---------------------------------------------------------------
  !> function G23d2
  !! Green's function G23d2 for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,2 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G23d2(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G23d2=1._8/(16._8*PI*(1._8-nu))*( &
       +(x3-y3)*(lr1**2-3*(x2-y2)**2)/lr1**5 &
       +(3._8-4._8*nu)*(x3-y3)*(lr2**2-3*(x2-y2)**2)/lr2**5 &
       -6._8*y3*x3*(x3+y3)*(lr2**2-5*(x2-y2)**2)/lr2**7 &
       +4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)) &
       -4._8*(1._8-2*nu)*(1._8-nu)*(x2-y2)**2*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G23d2

  !---------------------------------------------------------------
  !> function G23d3
  !! Green's function G23d3 for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,3 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G23d3(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G23d3=1._8/(16*PI*(1._8-nu))*(x2-y2)*( &
       +(lr1**2-3*(x3-y3)**2)/lr1**5 &
       +(3._8-4._8*nu)*(lr2**2-3._8*(x3**2-y3**2))/lr2**5 &
       -6._8*y3*(2*x3+y3)/lr2**5 &
       +30._8*y3*x3*(x3+y3)**2/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/lr2**3 &
    )

  END FUNCTION G23d3

  !---------------------------------------------------------------
  !> function G33d1
  !! Green's function G33d1 for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,1 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G33d1(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G33d1=1._8/(16._8*PI*(1._8-nu))*(x1-y1)*( &
       -(3._8-4._8*nu)/lr1**3 &
       -(5._8-12._8*nu+8._8*nu**2)/lr2**3 &
       -3._8*(x3-y3)**2/lr1**5 &
       -30._8*y3*x3*(x3+y3)**2/lr2**7 &
       -3._8*(3-4*nu)*(x3+y3)**2/lr2**5 &
       +6._8*y3*x3/lr2**5 &
    )

  END FUNCTION G33d1

  !---------------------------------------------------------------
  !> function G33d2
  !! Green's function G33d2 for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,2 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G33d2(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G33d2=1._8/(16._8*PI*(1._8-nu))*(x2-y2)*( &
       -(3._8-4._8*nu)/lr1**3 &
       -(5._8-12._8*nu+8._8*nu**2)/lr2**3 &
       -3._8*(x3-y3)**2/lr1**5 &
       -30._8*y3*x3*(x3+y3)**2/lr2**7 &
       -3._8*(3-4*nu)*(x3+y3)**2/lr2**5 &
       +6._8*y3*x3/lr2**5 &
    )

  END FUNCTION G33d2

  !---------------------------------------------------------------
  !> function G33d3
  !! Green's function G33d3 for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,3 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G33d3(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)

    G33d3=1._8/(16._8*PI*(1._8-nu))*( &
       -(3._8-4._8*nu)*(x3-y3)/lr1**3 &
       -(5._8-12._8*nu+8._8*nu**2)*(x3+y3)/lr2**3 &
       +(x3-y3)*(2*lr1**2-3*(x3-y3)**2)/lr1**5 &
       +6._8*y3*(x3+y3)**2/lr2**5 &
       +6._8*y3*x3*(x3+y3)*(2*lr2**2-5*(x3+y3)**2)/lr2**7 &
       +(3._8-4._8*nu)*(x3+y3)*(2*lr2**2-3*(x3+y3)**2)/lr2**5 &
       -2*y3*(lr2**2-3*x3*(x3+y3))/lr2**5 &
    )

  END FUNCTION G33d3

  !---------------------------------------------------------------
  ! function IU11 is the integrand for displacement gradient u1,1
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU11(u,v)
    IMPLICIT NONE
     REAL*8, INTENT(IN) :: u,v

     ! initiate displacement gradient
     IU11=0._8

     IF (0 .NE. m11*nD(1)+m12*nD(2)+m13*nD(3)) THEN
         IU11=IU11+ABC/4*(m11*nD(1)+m12*nD(2)+m13*nD(3))*G11d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
     END IF
     IF (0 .NE. m12*nD(1)+m22*nD(2)+m23*nD(3)) THEN
         IU11=IU11+ABC/4*(m12*nD(1)+m22*nD(2)+m23*nD(3))*G21d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
     END IF
     IF (0 .NE. m13*nD(1)+m23*nD(2)+m33*nD(3)) THEN
         IU11=IU11+ABC/4*(m13*nD(1)+m23*nD(2)+m33*nD(3))*G31d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
     END IF
     IF (0 .NE. m11*nA(1)+m12*nA(2)+m13*nA(3)) THEN
         IU11=IU11+BCD/4*(m11*nA(1)+m12*nA(2)+m13*nA(3))*G11d1(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
     END IF
     IF (0 .NE. m12*nA(1)+m22*nA(2)+m23*nA(3)) THEN
         IU11=IU11+BCD/4*(m12*nA(1)+m22*nA(2)+m23*nA(3))*G21d1(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
     END IF
     IF (0 .NE. m13*nA(1)+m23*nA(2)+m33*nA(3)) THEN
         IU11=IU11+BCD/4*(m13*nA(1)+m23*nA(2)+m33*nA(3))*G31d1(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
     END IF
     IF (0 .NE. m11*nB(1)+m12*nB(2)+m13*nB(3)) THEN
         IU11=IU11+CDA/4*(m11*nB(1)+m12*nB(2)+m13*nB(3))*G11d1(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
     END IF
     IF (0 .NE. m12*nB(1)+m22*nB(2)+m23*nB(3)) THEN
         IU11=IU11+CDA/4*(m12*nB(1)+m22*nB(2)+m23*nB(3))*G21d1(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
     END IF
     IF (0 .NE. m13*nB(1)+m23*nB(2)+m33*nB(3)) THEN
         IU11=IU11+CDA/4*(m13*nB(1)+m23*nB(2)+m33*nB(3))*G31d1(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
     END IF
     IF (0 .NE. m11*nC(1)+m12*nC(2)+m13*nC(3)) THEN
         IU11=IU11+DAB/4*(m11*nC(1)+m12*nC(2)+m13*nC(3))*G11d1(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
     END IF
     IF (0 .NE. m12*nC(1)+m22*nC(2)+m23*nC(3)) THEN
         IU11=IU11+DAB/4*(m12*nC(1)+m22*nC(2)+m23*nC(3))*G21d1(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
     END IF
     IF (0 .NE. m13*nC(1)+m23*nC(2)+m33*nC(3)) THEN
         IU11=IU11+DAB/4*(m13*nC(1)+m23*nC(2)+m33*nC(3))*G31d1(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
     END IF

  END FUNCTION IU11

  !---------------------------------------------------------------
  ! function IU12 is the integrand for displacement gradient u1,2
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU12(u,v)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: u,v

    ! initialize displacement gradient
    IU12=0._8

    IF (0 .NE. m11*nD(1)+m12*nD(2)+m13*nD(3)) THEN
        IU12=IU12+ABC/4*(m11*nD(1)+m12*nD(2)+m13*nD(3))*G11d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
    END IF
    IF (0 .NE. m12*nD(1)+m22*nD(2)+m23*nD(3)) THEN
        IU12=IU12+ABC/4*(m12*nD(1)+m22*nD(2)+m23*nD(3))*G21d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
    END IF
    IF (0 .NE. m13*nD(1)+m23*nD(2)+m33*nD(3)) THEN
        IU12=IU12+ABC/4*(m13*nD(1)+m23*nD(2)+m33*nD(3))*G31d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
    END IF
    IF (0 .NE. m11*nA(1)+m12*nA(2)+m13*nA(3)) THEN
        IU12=IU12+BCD/4*(m11*nA(1)+m12*nA(2)+m13*nA(3))*G11d2(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
    END IF
    IF (0 .NE. m12*nA(1)+m22*nA(2)+m23*nA(3)) THEN
        IU12=IU12+BCD/4*(m12*nA(1)+m22*nA(2)+m23*nA(3))*G21d2(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
    END IF
    IF (0 .NE. m13*nA(1)+m23*nA(2)+m33*nA(3)) THEN
        IU12=IU12+BCD/4*(m13*nA(1)+m23*nA(2)+m33*nA(3))*G31d2(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
    END IF
    IF (0 .NE. m11*nB(1)+m12*nB(2)+m13*nB(3)) THEN
        IU12=IU12+CDA/4*(m11*nB(1)+m12*nB(2)+m13*nB(3))*G11d2(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
    END IF
    IF (0 .NE. m12*nB(1)+m22*nB(2)+m23*nB(3)) THEN
        IU12=IU12+CDA/4*(m12*nB(1)+m22*nB(2)+m23*nB(3))*G21d2(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
    END IF
    IF (0 .NE. m13*nB(1)+m23*nB(2)+m33*nB(3)) THEN
        IU12=IU12+CDA/4*(m13*nB(1)+m23*nB(2)+m33*nB(3))*G31d2(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
    END IF
    IF (0 .NE. m11*nC(1)+m12*nC(2)+m13*nC(3)) THEN
        IU12=IU12+DAB/4*(m11*nC(1)+m12*nC(2)+m13*nC(3))*G11d2(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
    END IF
    IF (0 .NE. m12*nC(1)+m22*nC(2)+m23*nC(3)) THEN
        IU12=IU12+DAB/4*(m12*nC(1)+m22*nC(2)+m23*nC(3))*G21d2(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
    END IF
    IF (0 .NE. m13*nC(1)+m23*nC(2)+m33*nC(3)) THEN
        IU12=IU12+DAB/4*(m13*nC(1)+m23*nC(2)+m33*nC(3))*G31d2(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
    END IF

  END FUNCTION IU12

  !---------------------------------------------------------------
  ! function IU13 is the integrand for displacement gradient u1,3
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU13(u,v)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: u,v

    ! initiate displacement gradient
    IU13=0._8

    IF (0 .NE. m11*nD(1)+m12*nD(2)+m13*nD(3)) THEN
        IU13=IU13+ABC/4*(m11*nD(1)+m12*nD(2)+m13*nD(3))*G11d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
    END IF
    IF (0 .NE. m12*nD(1)+m22*nD(2)+m23*nD(3)) THEN
        IU13=IU13+ABC/4*(m12*nD(1)+m22*nD(2)+m23*nD(3))*G21d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
    END IF
    IF (0 .NE. m13*nD(1)+m23*nD(2)+m33*nD(3)) THEN
        IU13=IU13+ABC/4*(m13*nD(1)+m23*nD(2)+m33*nD(3))*G31d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
    END IF
    IF (0 .NE. m11*nA(1)+m12*nA(2)+m13*nA(3)) THEN
        IU13=IU13+BCD/4*(m11*nA(1)+m12*nA(2)+m13*nA(3))*G11d3(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
    END IF
    IF (0 .NE. m12*nA(1)+m22*nA(2)+m23*nA(3)) THEN
        IU13=IU13+BCD/4*(m12*nA(1)+m22*nA(2)+m23*nA(3))*G21d3(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
    END IF
    IF (0 .NE. m13*nA(1)+m23*nA(2)+m33*nA(3)) THEN
        IU13=IU13+BCD/4*(m13*nA(1)+m23*nA(2)+m33*nA(3))*G31d3(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
    END IF
    IF (0 .NE. m11*nB(1)+m12*nB(2)+m13*nB(3)) THEN
        IU13=IU13+CDA/4*(m11*nB(1)+m12*nB(2)+m13*nB(3))*G11d3(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
    END IF
    IF (0 .NE. m12*nB(1)+m22*nB(2)+m23*nB(3)) THEN
        IU13=IU13+CDA/4*(m12*nB(1)+m22*nB(2)+m23*nB(3))*G21d3(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
    END IF
    IF (0 .NE. m13*nB(1)+m23*nB(2)+m33*nB(3)) THEN
        IU13=IU13+CDA/4*(m13*nB(1)+m23*nB(2)+m33*nB(3))*G31d3(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
    END IF
    IF (0 .NE. m11*nC(1)+m12*nC(2)+m13*nC(3)) THEN
        IU13=IU13+DAB/4*(m11*nC(1)+m12*nC(2)+m13*nC(3))*G11d3(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
    END IF
    IF (0 .NE. m12*nC(1)+m22*nC(2)+m23*nC(3)) THEN
        IU13=IU13+DAB/4*(m12*nC(1)+m22*nC(2)+m23*nC(3))*G21d3(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
    END IF
    IF (0 .NE. m13*nC(1)+m23*nC(2)+m33*nC(3)) THEN
        IU13=IU13+DAB/4*(m13*nC(1)+m23*nC(2)+m33*nC(3))*G31d3(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
    END IF

  END FUNCTION IU13

  !---------------------------------------------------------------
  ! function IU21 is the integrand for displacement gradient u2,1
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU21(u,v)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: u,v

    ! initialize displacement gradient
    IU21=0._8

    IF (0 .NE. m11*nD(1)+m12*nD(2)+m13*nD(3)) THEN
        IU21=IU21+ABC/4*(m11*nD(1)+m12*nD(2)+m13*nD(3))*G12d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
    END IF
    IF (0 .NE. m12*nD(1)+m22*nD(2)+m23*nD(3)) THEN
        IU21=IU21+ABC/4*(m12*nD(1)+m22*nD(2)+m23*nD(3))*G22d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
    END IF
    IF (0 .NE. m13*nD(1)+m23*nD(2)+m33*nD(3)) THEN
        IU21=IU21+ABC/4*(m13*nD(1)+m23*nD(2)+m33*nD(3))*G32d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
    END IF
    IF (0 .NE. m11*nA(1)+m12*nA(2)+m13*nA(3)) THEN
        IU21=IU21+BCD/4*(m11*nA(1)+m12*nA(2)+m13*nA(3))*G12d1(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
    END IF
    IF (0 .NE. m12*nA(1)+m22*nA(2)+m23*nA(3)) THEN
        IU21=IU21+BCD/4*(m12*nA(1)+m22*nA(2)+m23*nA(3))*G22d1(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
    END IF
    IF (0 .NE. m13*nA(1)+m23*nA(2)+m33*nA(3)) THEN
        IU21=IU21+BCD/4*(m13*nA(1)+m23*nA(2)+m33*nA(3))*G32d1(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
    END IF
    IF (0 .NE. m11*nB(1)+m12*nB(2)+m13*nB(3)) THEN
        IU21=IU21+CDA/4*(m11*nB(1)+m12*nB(2)+m13*nB(3))*G12d1(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
    END IF
    IF (0 .NE. m12*nB(1)+m22*nB(2)+m23*nB(3)) THEN
        IU21=IU21+CDA/4*(m12*nB(1)+m22*nB(2)+m23*nB(3))*G22d1(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
    END IF
    IF (0 .NE. m13*nB(1)+m23*nB(2)+m33*nB(3)) THEN
        IU21=IU21+CDA/4*(m13*nB(1)+m23*nB(2)+m33*nB(3))*G32d1(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
    END IF
    IF (0 .NE. m11*nC(1)+m12*nC(2)+m13*nC(3)) THEN
        IU21=IU21+DAB/4*(m11*nC(1)+m12*nC(2)+m13*nC(3))*G12d1(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
    END IF
    IF (0 .NE. m12*nC(1)+m22*nC(2)+m23*nC(3)) THEN
        IU21=IU21+DAB/4*(m12*nC(1)+m22*nC(2)+m23*nC(3))*G22d1(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
    END IF
    IF (0 .NE. m13*nC(1)+m23*nC(2)+m33*nC(3)) THEN
        IU21=IU21+DAB/4*(m13*nC(1)+m23*nC(2)+m33*nC(3))*G32d1(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
    END IF

  END FUNCTION IU21

  !---------------------------------------------------------------
  ! function IU22 is the integrand for displacement gradient u2,2
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU22(u,v)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: u,v

    ! initiate displacement gradient
    IU22=0._8

    IF (0 .NE. m11*nD(1)+m12*nD(2)+m13*nD(3)) THEN
        IU22=IU22+ABC/4*(m11*nD(1)+m12*nD(2)+m13*nD(3))*G12d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
    END IF
    IF (0 .NE. m12*nD(1)+m22*nD(2)+m23*nD(3)) THEN
        IU22=IU22+ABC/4*(m12*nD(1)+m22*nD(2)+m23*nD(3))*G22d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
    END IF
    IF (0 .NE. m13*nD(1)+m23*nD(2)+m33*nD(3)) THEN
        IU22=IU22+ABC/4*(m13*nD(1)+m23*nD(2)+m33*nD(3))*G32d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
    END IF
    IF (0 .NE. m11*nA(1)+m12*nA(2)+m13*nA(3)) THEN
        IU22=IU22+BCD/4*(m11*nA(1)+m12*nA(2)+m13*nA(3))*G12d2(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
    END IF
    IF (0 .NE. m12*nA(1)+m22*nA(2)+m23*nA(3)) THEN
        IU22=IU22+BCD/4*(m12*nA(1)+m22*nA(2)+m23*nA(3))*G22d2(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
    END IF
    IF (0 .NE. m13*nA(1)+m23*nA(2)+m33*nA(3)) THEN
        IU22=IU22+BCD/4*(m13*nA(1)+m23*nA(2)+m33*nA(3))*G32d2(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
    END IF
    IF (0 .NE. m11*nB(1)+m12*nB(2)+m13*nB(3)) THEN
        IU22=IU22+CDA/4*(m11*nB(1)+m12*nB(2)+m13*nB(3))*G12d2(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
    END IF
    IF (0 .NE. m12*nB(1)+m22*nB(2)+m23*nB(3)) THEN
        IU22=IU22+CDA/4*(m12*nB(1)+m22*nB(2)+m23*nB(3))*G22d2(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
    END IF
    IF (0 .NE. m13*nB(1)+m23*nB(2)+m33*nB(3)) THEN
        IU22=IU22+CDA/4*(m13*nB(1)+m23*nB(2)+m33*nB(3))*G32d2(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
    END IF
    IF (0 .NE. m11*nC(1)+m12*nC(2)+m13*nC(3)) THEN
        IU22=IU22+DAB/4*(m11*nC(1)+m12*nC(2)+m13*nC(3))*G12d2(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
    END IF
    IF (0 .NE. m12*nC(1)+m22*nC(2)+m23*nC(3)) THEN
        IU22=IU22+DAB/4*(m12*nC(1)+m22*nC(2)+m23*nC(3))*G22d2(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
    END IF
    IF (0 .NE. m13*nC(1)+m23*nC(2)+m33*nC(3)) THEN
        IU22=IU22+DAB/4*(m13*nC(1)+m23*nC(2)+m33*nC(3))*G32d2(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
    END IF

  END FUNCTION IU22

  !---------------------------------------------------------------
  ! function IU23 is the integrand for displacement gradient u2,3
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU23(u,v)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: u,v

    ! initialize displacement gradient
    IU23=0._8

    IF (0 .NE. m11*nD(1)+m12*nD(2)+m13*nD(3)) THEN
        IU23=IU23+ABC/4*(m11*nD(1)+m12*nD(2)+m13*nD(3))*G12d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
    END IF
    IF (0 .NE. m12*nD(1)+m22*nD(2)+m23*nD(3)) THEN
        IU23=IU23+ABC/4*(m12*nD(1)+m22*nD(2)+m23*nD(3))*G22d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
    END IF
    IF (0 .NE. m13*nD(1)+m23*nD(2)+m33*nD(3)) THEN
        IU23=IU23+ABC/4*(m13*nD(1)+m23*nD(2)+m33*nD(3))*G32d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
    END IF
    IF (0 .NE. m11*nA(1)+m12*nA(2)+m13*nA(3)) THEN
        IU23=IU23+BCD/4*(m11*nA(1)+m12*nA(2)+m13*nA(3))*G12d3(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
    END IF
    IF (0 .NE. m12*nA(1)+m22*nA(2)+m23*nA(3)) THEN
        IU23=IU23+BCD/4*(m12*nA(1)+m22*nA(2)+m23*nA(3))*G22d3(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
    END IF
    IF (0 .NE. m13*nA(1)+m23*nA(2)+m33*nA(3)) THEN
        IU23=IU23+BCD/4*(m13*nA(1)+m23*nA(2)+m33*nA(3))*G32d3(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
    END IF
    IF (0 .NE. m11*nB(1)+m12*nB(2)+m13*nB(3)) THEN
        IU23=IU23+CDA/4*(m11*nB(1)+m12*nB(2)+m13*nB(3))*G12d3(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
    END IF
    IF (0 .NE. m12*nB(1)+m22*nB(2)+m23*nB(3)) THEN
        IU23=IU23+CDA/4*(m12*nB(1)+m22*nB(2)+m23*nB(3))*G22d3(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
    END IF
    IF (0 .NE. m13*nB(1)+m23*nB(2)+m33*nB(3)) THEN
        IU23=IU23+CDA/4*(m13*nB(1)+m23*nB(2)+m33*nB(3))*G32d3(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
    END IF
    IF (0 .NE. m11*nC(1)+m12*nC(2)+m13*nC(3)) THEN
        IU23=IU23+DAB/4*(m11*nC(1)+m12*nC(2)+m13*nC(3))*G12d3(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
    END IF
    IF (0 .NE. m12*nC(1)+m22*nC(2)+m23*nC(3)) THEN
        IU23=IU23+DAB/4*(m12*nC(1)+m22*nC(2)+m23*nC(3))*G22d3(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
    END IF
    IF (0 .NE. m13*nC(1)+m23*nC(2)+m33*nC(3)) THEN
        IU23=IU23+DAB/4*(m13*nC(1)+m23*nC(2)+m33*nC(3))*G32d3(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
    END IF

  END FUNCTION IU23

  !---------------------------------------------------------------
  ! function IU31 is the integrand for displacement gradient u3,1
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU31(u,v)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: u,v

    ! initialize displacement gradient
    IU31=0._8
    
    IF (0 .NE. m11*nD(1)+m12*nD(2)+m13*nD(3)) THEN
        IU31=IU31+ABC/4*(m11*nD(1)+m12*nD(2)+m13*nD(3))*G13d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
    END IF
    IF (0 .NE. m12*nD(1)+m22*nD(2)+m23*nD(3)) THEN
        IU31=IU31+ABC/4*(m12*nD(1)+m22*nD(2)+m23*nD(3))*G23d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
    END IF
    IF (0 .NE. m13*nD(1)+m23*nD(2)+m33*nD(3)) THEN
        IU31=IU31+ABC/4*(m13*nD(1)+m23*nD(2)+m33*nD(3))*G33d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
    END IF
    IF (0 .NE. m11*nA(1)+m12*nA(2)+m13*nA(3)) THEN
        IU31=IU31+BCD/4*(m11*nA(1)+m12*nA(2)+m13*nA(3))*G13d1(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
    END IF
    IF (0 .NE. m12*nA(1)+m22*nA(2)+m23*nA(3)) THEN
        IU31=IU31+BCD/4*(m12*nA(1)+m22*nA(2)+m23*nA(3))*G23d1(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
    END IF
    IF (0 .NE. m13*nA(1)+m23*nA(2)+m33*nA(3)) THEN
        IU31=IU31+BCD/4*(m13*nA(1)+m23*nA(2)+m33*nA(3))*G33d1(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
    END IF
    IF (0 .NE. m11*nB(1)+m12*nB(2)+m13*nB(3)) THEN
        IU31=IU31+CDA/4*(m11*nB(1)+m12*nB(2)+m13*nB(3))*G13d1(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
    END IF
    IF (0 .NE. m12*nB(1)+m22*nB(2)+m23*nB(3)) THEN
        IU31=IU31+CDA/4*(m12*nB(1)+m22*nB(2)+m23*nB(3))*G23d1(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
    END IF
    IF (0 .NE. m13*nB(1)+m23*nB(2)+m33*nB(3)) THEN
        IU31=IU31+CDA/4*(m13*nB(1)+m23*nB(2)+m33*nB(3))*G33d1(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
    END IF
    IF (0 .NE. m11*nC(1)+m12*nC(2)+m13*nC(3)) THEN
        IU31=IU31+DAB/4*(m11*nC(1)+m12*nC(2)+m13*nC(3))*G13d1(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
    END IF
    IF (0 .NE. m12*nC(1)+m22*nC(2)+m23*nC(3)) THEN
        IU31=IU31+DAB/4*(m12*nC(1)+m22*nC(2)+m23*nC(3))*G23d1(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
    END IF
    IF (0 .NE. m13*nC(1)+m23*nC(2)+m33*nC(3)) THEN
        IU31=IU31+DAB/4*(m13*nC(1)+m23*nC(2)+m33*nC(3))*G33d1(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
    END IF

  END FUNCTION IU31

  !---------------------------------------------------------------
  ! function IU32 is the integrand for displacement gradient u3,2
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU32(u,v)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: u,v

    ! initialize displacement gradient
    IU32=0._8

    IF (0 .NE. m11*nD(1)+m12*nD(2)+m13*nD(3)) THEN
        IU32=IU32+ABC/4*(m11*nD(1)+m12*nD(2)+m13*nD(3))*G13d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
    END IF
    IF (0 .NE. m12*nD(1)+m22*nD(2)+m23*nD(3)) THEN
        IU32=IU32+ABC/4*(m12*nD(1)+m22*nD(2)+m23*nD(3))*G23d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
    END IF
    IF (0 .NE. m13*nD(1)+m23*nD(2)+m33*nD(3)) THEN
        IU32=IU32+ABC/4*(m13*nD(1)+m23*nD(2)+m33*nD(3))*G33d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
    END IF
    IF (0 .NE. m11*nA(1)+m12*nA(2)+m13*nA(3)) THEN
        IU32=IU32+BCD/4*(m11*nA(1)+m12*nA(2)+m13*nA(3))*G13d2(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
    END IF
    IF (0 .NE. m12*nA(1)+m22*nA(2)+m23*nA(3)) THEN
        IU32=IU32+BCD/4*(m12*nA(1)+m22*nA(2)+m23*nA(3))*G23d2(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
    END IF
    IF (0 .NE. m13*nA(1)+m23*nA(2)+m33*nA(3)) THEN
        IU32=IU32+BCD/4*(m13*nA(1)+m23*nA(2)+m33*nA(3))*G33d2(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
    END IF
    IF (0 .NE. m11*nB(1)+m12*nB(2)+m13*nB(3)) THEN
        IU32=IU32+CDA/4*(m11*nB(1)+m12*nB(2)+m13*nB(3))*G13d2(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
    END IF
    IF (0 .NE. m12*nB(1)+m22*nB(2)+m23*nB(3)) THEN
        IU32=IU32+CDA/4*(m12*nB(1)+m22*nB(2)+m23*nB(3))*G23d2(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
    END IF
    IF (0 .NE. m13*nB(1)+m23*nB(2)+m33*nB(3)) THEN
        IU32=IU32+CDA/4*(m13*nB(1)+m23*nB(2)+m33*nB(3))*G33d2(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
    END IF
    IF (0 .NE. m11*nC(1)+m12*nC(2)+m13*nC(3)) THEN
        IU32=IU32+DAB/4*(m11*nC(1)+m12*nC(2)+m13*nC(3))*G13d2(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
    END IF
    IF (0 .NE. m12*nC(1)+m22*nC(2)+m23*nC(3)) THEN
        IU32=IU32+DAB/4*(m12*nC(1)+m22*nC(2)+m23*nC(3))*G23d2(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
    END IF
    IF (0 .NE. m13*nC(1)+m23*nC(2)+m33*nC(3)) THEN
        IU32=IU32+DAB/4*(m13*nC(1)+m23*nC(2)+m33*nC(3))*G33d2(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
    END IF

  END FUNCTION IU32

  !---------------------------------------------------------------
  ! function IU33 is the integrand for displacement gradient u3,3
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU33(u,v)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: u,v

    ! initialize displacement gradient
    IU33=0._8

    IF (0 .NE. m11*nD(1)+m12*nD(2)+m13*nD(3)) THEN
        IU33=IU33+ABC/4*(m11*nD(1)+m12*nD(2)+m13*nD(3))*G13d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
    END IF
    IF (0 .NE. m12*nD(1)+m22*nD(2)+m23*nD(3)) THEN
        IU33=IU33+ABC/4*(m12*nD(1)+m22*nD(2)+m23*nD(3))*G23d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
    END IF
    IF (0 .NE. m13*nD(1)+m23*nD(2)+m33*nD(3)) THEN
        IU33=IU33+ABC/4*(m13*nD(1)+m23*nD(2)+m33*nD(3))*G33d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)))
    END IF
    IF (0 .NE. m11*nA(1)+m12*nA(2)+m13*nA(3)) THEN
        IU33=IU33+BCD/4*(m11*nA(1)+m12*nA(2)+m13*nA(3))*G13d3(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
    END IF
    IF (0 .NE. m12*nA(1)+m22*nA(2)+m23*nA(3)) THEN
        IU33=IU33+BCD/4*(m12*nA(1)+m22*nA(2)+m23*nA(3))*G23d3(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
    END IF
    IF (0 .NE. m13*nA(1)+m23*nA(2)+m33*nA(3)) THEN
        IU33=IU33+BCD/4*(m13*nA(1)+m23*nA(2)+m33*nA(3))*G33d3(y(u,v,B(1),C(1),D(1)),y(u,v,B(2),C(2),D(2)),y(u,v,B(3),C(3),D(3)))
    END IF
    IF (0 .NE. m11*nB(1)+m12*nB(2)+m13*nB(3)) THEN
        IU33=IU33+CDA/4*(m11*nB(1)+m12*nB(2)+m13*nB(3))*G13d3(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
    END IF
    IF (0 .NE. m12*nB(1)+m22*nB(2)+m23*nB(3)) THEN
        IU33=IU33+CDA/4*(m12*nB(1)+m22*nB(2)+m23*nB(3))*G23d3(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
    END IF
    IF (0 .NE. m13*nB(1)+m23*nB(2)+m33*nB(3)) THEN
        IU33=IU33+CDA/4*(m13*nB(1)+m23*nB(2)+m33*nB(3))*G33d3(y(u,v,C(1),D(1),A(1)),y(u,v,C(2),D(2),A(2)),y(u,v,C(3),D(3),A(3)))
    END IF
    IF (0 .NE. m11*nC(1)+m12*nC(2)+m13*nC(3)) THEN
        IU33=IU33+DAB/4*(m11*nC(1)+m12*nC(2)+m13*nC(3))*G13d3(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
    END IF
    IF (0 .NE. m12*nC(1)+m22*nC(2)+m23*nC(3)) THEN
        IU33=IU33+DAB/4*(m12*nC(1)+m22*nC(2)+m23*nC(3))*G23d3(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
    END IF
    IF (0 .NE. m13*nC(1)+m23*nC(2)+m33*nC(3)) THEN
        IU33=IU33+DAB/4*(m13*nC(1)+m23*nC(2)+m33*nC(3))*G33d3(y(u,v,D(1),A(1),B(1)),y(u,v,D(2),A(2),B(2)),y(u,v,D(3),A(3),B(3)))
    END IF

  END FUNCTION IU33

  !------------------------------------------------------------------------
  !> function heaviside
  !! computes the Heaviside function
  !------------------------------------------------------------------------
  REAL*8 FUNCTION heaviside(x)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x

     IF (0 .LT. x) THEN
        heaviside=1._8
     ELSE
        heaviside=0._8
     END IF

  END FUNCTION heaviside

END SUBROUTINE computeStressTetrahedronGauss

