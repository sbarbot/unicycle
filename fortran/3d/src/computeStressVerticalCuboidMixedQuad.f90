!------------------------------------------------------------------------
!! subroutine computeStressVerticalCuboidMixedQuad
!! computes the stress field associated with vertical cuboid volume element
!! using the either the double-exponential numerical integration or the
!! Gauss-Legendre quadrature depending on the distance from the cuboid,
!! as in 
!!
!!   Barbot S., "Deformation of a Half-Space from Anelastic Strain Confined
!!   in a Tetrahedral Volume", Bull. Seism. Soc. Am., 10.1785/0120180058, 2018.
!!
!! and considering the following geometry:
!!
!!                     N (x1)
!!                    /
!!                   /| strike (theta)
!!       q1,q2,q3 ->@-------------------------+
!!                  |                         |     E (x2)
!!                  :                       w |      +
!!                  |                       i |     / 
!!                  :                       d |    / s
!!                  |                       t |   / s
!!                  :                       h |  / e
!!                  |                         | / n
!!                  +-------------------------+  k
!!                  :       l e n g t h       / c
!!                  |                        / i
!!                  :                       / h
!!                  |                      / t
!!                  :                     /
!!                  |                    +
!!                  Z (x3)
!!
!!
!! INPUT:
!! x1, x2, x3         north, east, and depth coordinates of the observation points
!! q1, q2, q3         north, east and depth coordinates of the cuboid volume
!! L, T, W            length, thickness and width of the cuboid volume
!! theta (radians)    strike angle from north (from x1) of the cuboid volume
!! epsvij             source strain component ij in the cuboid volume
!!                    in the system of reference tied to the cuboid volume
!! nu                 Poisson's ratio in the half space
!!
!! OUTPUT:
!! sij                stress components in the north, east, depth reference system.
!!
!! \author Sylvain Barbot (09/27/18) - original Fortran form
!-----------------------------------------------------------------------
SUBROUTINE computeStressVerticalCuboidMixedQuad( &
                        x1,x2,x3,q1,q2,q3,L,T,W,theta, &
                        e11p,e12p,e13p,e22p,e23p,e33p,G,nu, &
                        s11,s12,s13,s22,s23,s33)

  IMPLICIT NONE

  REAL*8, INTENT(IN) :: x1,x2,x3
  REAL*8, INTENT(IN) :: q1,q2,q3
  REAL*8, INTENT(IN) :: L,T,W,theta
  REAL*8, INTENT(IN) :: e11p,e12p,e13p,e22p,e23p,e33p
  REAL*8, INTENT(IN) :: G,nu
  REAL*8, INTENT(OUT) :: s11,s12,s13,s22,s23,s33

  REAL*8, PARAMETER :: PI = 3.141592653589793115997963468544185161_8

  REAL*8, DIMENSION(15), PARAMETER :: sk = (/ -0.98799251802048542848956571858661258114697281712376d0, &
                                              -0.9372733924007059043077589477102094712439962735153d0, &
                                              -0.84820658341042721620064832077421685136625617473699d0, &
                                              -0.7244177313601700474161860546139380096308992945841d0, &
                                              -0.57097217260853884753722673725391064123838639628275d0, &
                                              -0.3941513470775633698972073709810454683627527761587d0, &
                                              -0.20119409399743452230062830339459620781283645446264d0, &
                                               0d0, &
                                               0.20119409399743452230062830339459620781283645446264d0, &
                                               0.3941513470775633698972073709810454683627527761587d0, &
                                               0.57097217260853884753722673725391064123838639628275d0, &
                                               0.7244177313601700474161860546139380096308992945841d0, &
                                               0.84820658341042721620064832077421685136625617473699d0, &
                                               0.93727339240070590430775894771020947124399627351531d0, &
                                               0.98799251802048542848956571858661258114697281712376d0 /)

  REAL*8, DIMENSION(15), PARAMETER :: gk = (/  0.03075324199611726835462839357720441772174814483343d0, &
                                               0.07036604748810812470926741645066733846670803275433d0, &
                                               0.1071592204671719350118695466858693034155437157581d0, &
                                               0.13957067792615431444780479451102832252085027531551d0, &
                                               0.16626920581699393355320086048120881113090018009841d0, &
                                               0.1861610000155622110268005618664228245062260122779d0, &
                                               0.19843148532711157645611832644383932481869255995754d0, &
                                               0.20257824192556127288062019996751931483866215800948d0, &
                                               0.19843148532711157645611832644383932481869255995754d0, &
                                               0.1861610000155622110268005618664228245062260122779d0, &
                                               0.16626920581699393355320086048120881113090018009841d0, &
                                               0.13957067792615431444780479451102832252085027531551d0, &
                                               0.1071592204671719350118695466858693034155437157581d0, &
                                               0.070366047488108124709267416450667338466708032754331d0, &
                                               0.03075324199611726835462839357720441772174814483343d0 /)

  ! trace
  REAL*8 :: ekk

  ! moment density
  REAL*8 :: m11,m12,m13,m22,m23,m33

  ! displacement gradient
  REAL*8 :: u11,u12,u13,u21,u22,u23,u31,u32,u33

  ! strain components in unprimed coordinate system
  REAL*8 :: e11,e12,e13,e22,e23,e33

  ! eigenstrain in unprimed coordinate system
  REAL*8 :: eps11,eps12,eps13,eps22,eps23,eps33

  ! strain in primed coordinate system
  REAL*8 :: v11p,v12p,v13p,v22p,v23p,v33p

  ! position in primed coordinate system
  REAL*8 :: x1p,x2p

  ! Lame parameter
  REAL*8 :: lambda

  ! numerical integration coordinates and weight
  REAL*8 :: xk,wk,xj,wj

  ! double integration step
  REAL*8 :: h

  ! counters, bound
  INTEGER :: j,k,n

  ! pseudo center
  REAL*8, DIMENSION(3) :: O

  ! pseudo radius
  REAL*8 :: r

  ! check valid parameters
  IF ((-1._8 .GT. nu) .OR. (0.5_8 .LT. nu)) THEN
     WRITE (0,'("error: -1<=nu<=0.5, nu=",ES9.2E2," given.")') nu
     STOP 1
  END IF

  IF (0 .GT. x3) THEN
     WRITE (0,'("error: observation depth (x3) must be positive")')
     STOP 1
  END IF

  ! Lame parameter
  lambda=2*nu/(1._8-2*nu)

  ! trace
  ekk=e11p+e22p+e33p

  ! moment density
  m11=2*e11p+lambda*ekk
  m12=2*e12p
  m13=2*e13p
  m22=2*e22p+lambda*ekk
  m23=2*e23p
  m33=2*e33p+lambda*ekk

  ! center of cuboid
  O=(/ q1+L/2*DCOS(theta), q2+L/2*DSIN(theta), q3+W/2 /)

  ! radius
  r=MAX(L/2,W/2,SQRT(W*L))*SQRT(2._8)

  ! rotate observation points to the volume-element-centric system of coordinates
  x1p= (x1-q1)*DCOS(theta)+(x2-q2)*DSIN(theta)
  x2p=-(x1-q1)*DSIN(theta)+(x2-q2)*DCOS(theta)

  ! numerical solution
  u11=0.d0; u12=0.d0; u13=0.d0
  u21=0.d0; u22=0.d0; u23=0.d0
  u31=0.d0; u32=0.d0; u33=0.d0

  ! choose integration method based on distance from circumcenter
  IF (SQRT((x1-O(1))**2+(x2-O(2))**2+(x3-O(3))**2) .LE. 1.1d0*r) THEN

     h=0.01
     n=INT(1d0/h*3.5d0)

     ! use the double-exponential method
     DO k=-n,n
        wk=(0.5*h*PI*COSH(k*h))/(COSH(0.5*PI*SINH(k*h)))**2
        xk=TANH(0.5*PI*SINH(k*h))

        DO j=-n,n
           wj=(0.5*h*PI*COSH(j*h))/(COSH(0.5*PI*SINH(j*h)))**2
           xj=TANH(0.5*PI*SINH(j*h))

           ! derivatives of the u1 component
           u11=u11+wj*wk*IU11(xj,xk)
           u12=u12+wj*wk*IU12(xj,xk)
           u13=u13+wj*wk*IU13(xj,xk)
           
           ! derivatives of the u2 component
           u21=u21+wj*wk*IU21(xj,xk)
           u22=u22+wj*wk*IU22(xj,xk)
           u23=u23+wj*wk*IU23(xj,xk)
           
           ! derivatives of the u3 component
           u31=u31+wj*wk*IU31(xj,xk)
           u32=u32+wj*wk*IU32(xj,xk)
           u33=u33+wj*wk*IU33(xj,xk)
           
        END DO
     END DO

  ELSE

     ! use the Gauss-Legendre quadrature
     DO k=1,15
        wk=gk(k)
        xk=sk(k)

        DO j=1,15
           wj=gk(j)
           xj=sk(j)

           ! derivatives of the u1 component
           u11=u11+wj*wk*IU11(xj,xk)
           u12=u12+wj*wk*IU12(xj,xk)
           u13=u13+wj*wk*IU13(xj,xk)

           ! derivatives of the u2 component
           u21=u21+wj*wk*IU21(xj,xk)
           u22=u22+wj*wk*IU22(xj,xk)
           u23=u23+wj*wk*IU23(xj,xk)

           ! derivatives of the u3 component
           u31=u31+wj*wk*IU31(xj,xk)
           u32=u32+wj*wk*IU32(xj,xk)
           u33=u33+wj*wk*IU33(xj,xk)

        END DO
     END DO

  END IF

  ! strain in the reference system tied to the volume element (primed)
  v11p= u11
  v12p=(u12+u21)/2
  v13p=(u13+u31)/2
  v22p= u22
  v23p=(u23+u32)/2
  v33p= u33

  ! rotate strain field to reference (unprimed) system of coordinates
  e11=(DCOS(theta)*v11p-DSIN(theta)*v12p)*DCOS(theta)-(DCOS(theta)*v12p-DSIN(theta)*v22p)*DSIN(theta)
  e12=(DCOS(theta)*v11p-DSIN(theta)*v12p)*DSIN(theta)+(DCOS(theta)*v12p-DSIN(theta)*v22p)*DCOS(theta)
  e13= DCOS(theta)*v13p-DSIN(theta)*v23p
  e22=(DSIN(theta)*v11p+DCOS(theta)*v12p)*DSIN(theta)+(DSIN(theta)*v12p+DCOS(theta)*v22p)*DCOS(theta)
  e23= DSIN(theta)*v13p+DCOS(theta)*v23p
  e33=v33p
 
  ! rotate anelastic strain field to reference (unprimed) system of coordinates
  eps11=(DCOS(theta)*e11p-DSIN(theta)*e12p)*DCOS(theta)-(DCOS(theta)*e12p-DSIN(theta)*e22p)*DSIN(theta)
  eps12=(DCOS(theta)*e11p-DSIN(theta)*e12p)*DSIN(theta)+(DCOS(theta)*e12p-DSIN(theta)*e22p)*DCOS(theta)
  eps13= DCOS(theta)*e13p-DSIN(theta)*e23p
  eps22=(DSIN(theta)*e11p+DCOS(theta)*e12p)*DSIN(theta)+(DSIN(theta)*e12p+DCOS(theta)*e22p)*DCOS(theta)
  eps23= DSIN(theta)*e13p+DCOS(theta)*e23p
  eps33=e33p

  ! remove anelastic eigenstrain
  e11=e11-eps11*S(x1p/L)*Omega(x2p/T)*S((x3-q3)/W)
  e12=e12-eps12*S(x1p/L)*Omega(x2p/T)*S((x3-q3)/W)
  e13=e13-eps13*S(x1p/L)*Omega(x2p/T)*S((x3-q3)/W)
  e22=e22-eps22*S(x1p/L)*Omega(x2p/T)*S((x3-q3)/W)
  e23=e23-eps23*S(x1p/L)*Omega(x2p/T)*S((x3-q3)/W)
  e33=e33-eps33*S(x1p/L)*Omega(x2p/T)*S((x3-q3)/W)

  ! trace
  ekk=e11+e22+e33

  ! stress components
  s11=G*(2*e11+lambda*ekk)
  s12=G*(2*e12)
  s13=G*(2*e13)
  s22=G*(2*e22+lambda*ekk)
  s23=G*(2*e23)
  s33=G*(2*e33+lambda*ekk)

CONTAINS

  !---------------------------------------------------------------
  !> function r1
  !! distance from source
  !---------------------------------------------------------------
  REAL*8 FUNCTION r1(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    r1=SQRT((x1p-y1)**2+(x2p-y2)**2+(x3-y3)**2)

  END FUNCTION r1

  !---------------------------------------------------------------
  !> function r2
  !! distance from image
  !---------------------------------------------------------------
  REAL*8 FUNCTION r2(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    r2=SQRT((x1p-y1)**2+(x2p-y2)**2+(x3+y3)**2)

  END FUNCTION r2

  !---------------------------------------------------------------
  !> function G11d1
  !! Green's function G11d1 for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,1 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G11d1(y1,y2,y3) 
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G11d1=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*( &
       -(3._8-4._8*nu)/lr1**3 &
       -1._8/lr2**3 &
       +(2*lr1**2-3._8*(x1p-y1)**2)/lr1**5 &
       +(3._8-4._8*nu)*(2*lr2**2-3._8*(x1p-y1)**2)/lr2**5 &
       -6._8*y3*x3*(3._8*lr2**2-5._8*(x1p-y1)**2)/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)**2) &
       -8._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)**2) &
           +4._8*(1._8-2*nu)*(1._8-nu)*(x1p-y1)**2/(lr2**3*(lr2+x3+y3)**2) &
           +8._8*(1._8-2*nu)*(1._8-nu)*(x1p-y1)**2/(lr2**2*(lr2+x3+y3)**3) &
       )

  END FUNCTION G11d1

  !---------------------------------------------------------------
  !> function G11d2
  !! Green's function G11d2 for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,2 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G11d2(y1,y2,y3) 
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G11d2=1._8/(16._8*PI*(1._8-nu))*(x2p-y2)*( &
       -(3._8-4._8*nu)/lr1**3 &
       -1._8/lr2**3 &
       -3._8*(x1p-y1)**2/lr1**5 &
       -3._8*(3-4*nu)*(x1p-y1)**2/lr2**5 &
       -6._8*y3*x3*(lr2**2-5._8*(x1p-y1)**2)/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)**2) &
       +4._8*(1._8-2*nu)*(1._8-nu)*(x1p-y1)**2*(3._8*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**3) &
    )

  END FUNCTION G11d2

  !---------------------------------------------------------------
  !> function G11d3
  !! Green's function G11d3 for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,3 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G11d3(y1,y2,y3) 
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
    
    G11d3=1._8/(16._8*PI*(1._8-nu))*( &
       -(3._8-4._8*nu)*(x3-y3)/lr1**3 &
       -(x3+y3)/lr2**3 &
       -3._8*(x1p-y1)**2*(x3-y3)/lr1**5 &
       -3._8*(3._8-4*nu)*(x1p-y1)**2*(x3+y3)/lr2**5 &
       +2*y3*(lr2**2-3._8*x3*(x3+y3))/lr2**5 &
       -6._8*y3*(x1p-y1)**2*(lr2**2-5._8*x3*(x3+y3))/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)) &
       +4._8*(1._8-2*nu)*(1._8-nu)*(x1p-y1)**2*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G11d3

  !---------------------------------------------------------------
  !> function G21d1
  !! Green's function G21d1 for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,1 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G21d1(y1,y2,y3) 
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G21d1=1._8/(16._8*PI*(1._8-nu))*(x2p-y2)*( &
    +(lr1**2-3._8*(x1p-y1)**2)/lr1**5 &
    +(3._8-4._8*nu)*(lr2**2-3._8*(x1p-y1)**2)/lr2**5 &
    -6._8*y3*x3*(lr2**2-5._8*(x1p-y1)**2)/lr2**7 &
    -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)**2) &
    +4._8*(1._8-2*nu)*(1._8-nu)*(x1p-y1)**2*(3._8*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**3) &
    )

  END FUNCTION G21d1

  !---------------------------------------------------------------
  !> function G21d2
  !! Green's function G21d2 for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,2 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G21d2(y1,y2,y3) 
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G21d2=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*( &
       +(lr1**2-3._8*(x2p-y2)**2)/lr1**5 &
       +(3._8-4._8*nu)*(lr2**2-3._8*(x2p-y2)**2)/lr2**5 &
       -6._8*y3*x3*(lr2**2-5._8*(x2p-y2)**2)/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)**2) &
       +4._8*(1._8-2*nu)*(1._8-nu)*(x2p-y2)**2*(3._8*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**3) &
    )

  END FUNCTION G21d2

  !---------------------------------------------------------------
  !> function G21d3
  !! Green's function G21d3 for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,3 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G21d3(y1,y2,y3) 
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G21d3=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*(x2p-y2)*( &
       -3._8*(x3-y3)/lr1**5 &
       -3._8*(3._8-4._8*nu)*(x3+y3)/lr2**5 &
       -6._8*y3*(lr2**2-5._8*x3*(x3+y3))/lr2**7 &
       +4._8*(1._8-2*nu)*(1._8-nu)*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G21d3

  !---------------------------------------------------------------
  !> function G31d1
  !! Green's function G31d1 for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,1 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G31d1(y1,y2,y3) 
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
    
    G31d1=1._8/(16._8*PI*(1._8-nu))*( &
       +(x3-y3)*(lr1**2-3._8*(x1p-y1)**2)/lr1**5 &
       +(3._8-4._8*nu)*(x3-y3)*(lr2**2-3*(x1p-y1)**2)/lr2**5 &
       +6._8*y3*x3*(x3+y3)*(lr2**2-5*(x1p-y1)**2)/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)) &
       +4._8*(1._8-2*nu)*(1._8-nu)*(x1p-y1)**2*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G31d1

  !---------------------------------------------------------------
  !> function G31d2
  !! Green's function G31d2 for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,2 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G31d2(y1,y2,y3) 
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G31d2=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*(x2p-y2)*( &
       -3._8*(x3-y3)/lr1**5 &
       -3._8*(3._8-4._8*nu)*(x3-y3)/lr2**5 &
       -30._8*y3*x3*(x3+y3)/lr2**7 &
       +4._8*(1._8-2*nu)*(1._8-nu)*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G31d2

  !---------------------------------------------------------------
  !> function G31d3
  !! Green's function G31d3 for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,3 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G31d3(y1,y2,y3) 
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G31d3=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*( &
       +(lr1**2-3*(x3-y3)**2)/lr1**5 &
       +(3._8-4._8*nu)*(lr2**2-3._8*(x3**2-y3**2))/lr2**5 &
       +6._8*y3*(2*x3+y3)/lr2**5 &
       -30._8*y3*x3*(x3+y3)**2/lr2**7 &
       +4._8*(1._8-2*nu)*(1-nu)/lr2**3 &
    )

  END FUNCTION G31d3

  !---------------------------------------------------------------
  !> function G12d1
  !! Green's function G12d1 for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,1 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G12d1(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G12d1=1._8/(16._8*PI*(1._8-nu))*(x2p-y2)*( &
       +(lr1**2-3*(x1p-y1)**2)/lr1**5 &
       +(3._8-4*nu)*(lr2**2-3*(x1p-y1)**2)/lr2**5 &
       -6._8*y3*x3*(lr2**2-5*(x1p-y1)**2)/lr2**7 &
       -4._8*(1._8-nu)*(1._8-2*nu)/(lr2*(lr2+x3+y3)**2) &
       +4._8*(1._8-nu)*(1._8-2*nu)*(x1p-y1)**2*(3._8*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**3) &
    )

  END FUNCTION G12d1

  !---------------------------------------------------------------
  !> function G12d2
  !! Green's function G12d2 for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,2 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G12d2(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G12d2=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*( &
       +(lr1**2-3._8*(x2p-y2)**2)/lr1**5 &
       +(3._8-4._8*nu)*(lr2**2-3._8*(x2p-y2)**2)/lr2**5 &
       -6._8*y3*x3*(lr2**2-5._8*(x2p-y2)**2)/lr2**7 &
       -4._8*(1._8-nu)*(1._8-2*nu)/(lr2*(lr2+x3+y3)**2) &
       +4._8*(1._8-nu)*(1._8-2*nu)*(x2p-y2)**2*(3*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**3) &
    )

  END FUNCTION G12d2

  !---------------------------------------------------------------
  !> function G12d3
  !! Green's function G12d3 for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,3 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G12d3(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G12d3=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*(x2p-y2)*( &
    -3._8*(x3-y3)/lr1**5 &
    -3._8*(3._8-4._8*nu)*(x3+y3)/lr2**5 &
    -6._8*y3*(lr2**2-5*x3*(x3+y3))/lr2**7 &
    +4._8*(1._8-2*nu)*(1._8-nu)*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G12d3

  !---------------------------------------------------------------
  !> function G22d1
  !! Green's function G22d1 for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,1 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G22d1(y1,y2,y3) 
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G22d1=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*( &
       -(3._8-4._8*nu)/lr1**3 &
       -1._8/lr2**3 &
       -3._8*(x2p-y2)**2/lr1**5 &
       -3._8*(3._8-4._8*nu)*(x2p-y2)**2/lr2**5 &
       -6._8*y3*x3*(lr2**2-5._8*(x2p-y2)**2)/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)**2) &
       +4._8*(1._8-2*nu)*(1._8-nu)*(x2p-y2)**2*(3._8*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**3) &
    )

  END FUNCTION G22d1

  !---------------------------------------------------------------
  !> function G22d2
  !! Green's function G22d2 for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,2 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G22d2(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G22d2=1._8/(16._8*PI*(1._8-nu))*(x2p-y2)*( &
       -(3._8-4._8*nu)/lr1**3 &
       -1._8/lr2**3 &
       +(2*lr1**2-3._8*(x2p-y2)**2)/lr1**5 &
       +(3._8-4._8*nu)*(2*lr2**2-3._8*(x2p-y2)**2)/lr2**5 &
       -6._8*y3*x3*(3._8*lr2**2-5*(x2p-y2)**2)/lr2**7 &
       -12._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)**2) &
       +4._8*(1._8-2*nu)*(1._8-nu)*(x2p-y2)**2*(3._8*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**3) &
    )

  END FUNCTION G22d2

  !---------------------------------------------------------------
  !> function G22d3
  !! Green's function G22d3 for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,3 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G22d3(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G22d3=1._8/(16._8*PI*(1._8-nu))*( &
       -(3._8-4._8*nu)*(x3-y3)/lr1**3 &
       -(x3+y3)/lr2**3 &
       -3._8*(x2p-y2)**2*(x3-y3)/lr1**5 &
       -3._8*(3-4*nu)*(x2p-y2)**2*(x3+y3)/lr2**5 &
       +2*y3*(lr2**2-3*x3*(x3+y3))/lr2**5 &
       -6._8*y3*(x2p-y2)**2*(lr2**2-5*x3*(x3+y3))/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)) &
       +4._8*(1._8-2*nu)*(1._8-nu)*(x2p-y2)**2*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G22d3

  !---------------------------------------------------------------
  !> function G32d1
  !! Green's function G32d1 for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,1 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G32d1(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G32d1=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*(x2p-y2)*( &
       -3._8*(x3-y3)/lr1**5 &
       -3._8*(3._8-4._8*nu)*(x3-y3)/lr2**5 &
       -30._8*y3*x3*(x3+y3)/lr2**7 &
       +4._8*(1._8-2*nu)*(1._8-nu)*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G32d1

  !---------------------------------------------------------------
  !> function G32d2
  !! Green's function G32d2 for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,2 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G32d2(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G32d2=1._8/(16._8*PI*(1._8-nu))*( &
       +(x3-y3)*(lr1**2-3*(x2p-y2)**2)/lr1**5 &
       +(3._8-4._8*nu)*(x3-y3)*(lr2**2-3*(x2p-y2)**2)/lr2**5 &
       +6._8*y3*x3*(x3+y3)*(lr2**2-5*(x2p-y2)**2)/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)) &
       +4._8*(1._8-2*nu)*(1._8-nu)*(x2p-y2)**2*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G32d2

  !---------------------------------------------------------------
  !> function G32d3
  !! Green's function G32d3 for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,3 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G32d3(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G32d3=1._8/(16._8*PI*(1._8-nu))*(x2p-y2)*( &
       +(lr1**2-3*(x3-y3)**2)/lr1**5 &
       +(3._8-4._8*nu)*(lr2**2-3*(x3**2-y3**2))/lr2**5 &
       +6._8*y3*(2*x3+y3)/lr2**5 &
       -30._8*y3*x3*(x3+y3)**2/lr2**7 &
       +4._8*(1._8-2*nu)*(1-nu)/lr2**3 &
    )

  END FUNCTION G32d3

  !---------------------------------------------------------------
  !> function G13d1
  !! Green's function G13d1 for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,1 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G13d1(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G13d1=1._8/(16._8*PI*(1._8-nu))*( &
       +(x3-y3)*(lr1**2-3._8*(x1p-y1)**2)/lr1**5 &
       +(3._8-4*nu)*(x3-y3)*(lr2**2-3*(x1p-y1)**2)/lr2**5 &
       -6._8*y3*x3*(x3+y3)*(lr2**2-5*(x1p-y1)**2)/lr2**7 &
       +4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)) &
       -4._8*(1._8-2*nu)*(1._8-nu)*(x1p-y1)**2*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G13d1

  !---------------------------------------------------------------
  !> function G13d2
  !! Green's function G13d2 for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,2 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G13d2(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G13d2=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*(x2p-y2)*( &
       -3._8*(x3-y3)/lr1**5 &
       -3._8*(3._8-4._8*nu)*(x3-y3)/lr2**5 &
       +30._8*y3*x3*(x3+y3)/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G13d2

  !---------------------------------------------------------------
  !> function G13d3
  !! Green's function G13d3 for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,3 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G13d3(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G13d3=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*( &
       +(lr1**2-3*(x3-y3)**2)/lr1**5 &
       +(3._8-4*nu)*(lr2**2-3*(x3**2-y3**2))/lr2**5 &
       -6._8*y3*(2*x3+y3)/lr2**5 &
       +30._8*y3*x3*(x3+y3)**2/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/lr2**3 &
    )

  END FUNCTION G13d3

  !---------------------------------------------------------------
  !> function G23d1
  !! Green's function G23d1 for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,1 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G23d1(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G23d1=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*(x2p-y2)*( &
       -3._8*(x3-y3)/lr1**5 &
       -3._8*(3._8-4._8*nu)*(x3-y3)/lr2**5 &
       +30._8*y3*x3*(x3+y3)/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G23d1

  !---------------------------------------------------------------
  !> function G23d2
  !! Green's function G23d2 for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,2 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G23d2(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G23d2=1._8/(16._8*PI*(1._8-nu))*( &
       +(x3-y3)*(lr1**2-3*(x2p-y2)**2)/lr1**5 &
       +(3._8-4._8*nu)*(x3-y3)*(lr2**2-3*(x2p-y2)**2)/lr2**5 &
       -6._8*y3*x3*(x3+y3)*(lr2**2-5*(x2p-y2)**2)/lr2**7 &
       +4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)) &
       -4._8*(1._8-2*nu)*(1._8-nu)*(x2p-y2)**2*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G23d2

  !---------------------------------------------------------------
  !> function G23d3
  !! Green's function G23d3 for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,3 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G23d3(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G23d3=1._8/(16*PI*(1._8-nu))*(x2p-y2)*( &
       +(lr1**2-3*(x3-y3)**2)/lr1**5 &
       +(3._8-4._8*nu)*(lr2**2-3._8*(x3**2-y3**2))/lr2**5 &
       -6._8*y3*(2*x3+y3)/lr2**5 &
       +30._8*y3*x3*(x3+y3)**2/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/lr2**3 &
    )

  END FUNCTION G23d3

  !---------------------------------------------------------------
  !> function G33d1
  !! Green's function G33d1 for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,1 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G33d1(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G33d1=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*( &
       -(3._8-4._8*nu)/lr1**3 &
       -(5._8-12._8*nu+8._8*nu**2)/lr2**3 &
       -3._8*(x3-y3)**2/lr1**5 &
       -30._8*y3*x3*(x3+y3)**2/lr2**7 &
       -3._8*(3-4*nu)*(x3+y3)**2/lr2**5 &
       +6._8*y3*x3/lr2**5 &
    )

  END FUNCTION G33d1

  !---------------------------------------------------------------
  !> function G33d2
  !! Green's function G33d2 for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,2 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G33d2(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)
          
    G33d2=1._8/(16._8*PI*(1._8-nu))*(x2p-y2)*( &
       -(3._8-4._8*nu)/lr1**3 &
       -(5._8-12._8*nu+8._8*nu**2)/lr2**3 &
       -3._8*(x3-y3)**2/lr1**5 &
       -30._8*y3*x3*(x3+y3)**2/lr2**7 &
       -3._8*(3-4*nu)*(x3+y3)**2/lr2**5 &
       +6._8*y3*x3/lr2**5 &
    )

  END FUNCTION G33d2

  !---------------------------------------------------------------
  !> function G33d3
  !! Green's function G33d3 for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,3 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G33d3(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1, lr2

    lr1=r1(y1,y2,y3)
    lr2=r2(y1,y2,y3)

    G33d3=1._8/(16._8*PI*(1._8-nu))*( &
       -(3._8-4._8*nu)*(x3-y3)/lr1**3 &
       -(5._8-12._8*nu+8._8*nu**2)*(x3+y3)/lr2**3 &
       +(x3-y3)*(2*lr1**2-3*(x3-y3)**2)/lr1**5 &
       +6._8*y3*(x3+y3)**2/lr2**5 &
       +6._8*y3*x3*(x3+y3)*(2*lr2**2-5*(x3+y3)**2)/lr2**7 &
       +(3._8-4._8*nu)*(x3+y3)*(2*lr2**2-3*(x3+y3)**2)/lr2**5 &
       -2*y3*(lr2**2-3*x3*(x3+y3))/lr2**5 &
    )

  END FUNCTION G33d3

  ! function IU11 is the integrand for displacement component u1
  REAL*8 FUNCTION IU11(x,y)
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU11=0._8

    IF (0._8 .NE. m11) THEN
       IU11=IU11+m11*T*W/4*(G11d1(L,x*T/2,(1+y)*W/2+q3)-G11d1(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU11=IU11+m12*T*W/4*(G21d1(L,x*T/2,(1+y)*W/2+q3)-G21d1(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m12*L*W/4*(G11d1((1+x)*L/2,T/2,(1+y)*W/2+q3)-G11d1((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU11=IU11+m13*T*W/4*(G31d1(L,x*T/2,(1+y)*W/2+q3)-G31d1(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m13*L*T/4*(G11d1((1+x)*L/2,y*T/2,W+q3)-G11d1((1+x)*L/2,y*T/2,q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU11=IU11+m22*L*W/4*(G21d1((1+x)*L/2,T/2,(1+y)*W/2+q3)-G21d1((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU11=IU11+m23*L*T/4*(G21d1((1+x)*L/2,y*T/2,W+q3)-G21d1((1+x)*L/2,y*T/2,q3)) &
                +m23*L*W/4*(G31d1((1+x)*L/2,T/2,(1+y)*W/2+q3)-G31d1((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU11=IU11+m33*L*T/4*(G31d1((1+x)*L/2,y*T/2,W+q3)-G31d1((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU11

  ! function IU12 is the integrand for displacement gradient u1,2
  REAL*8 FUNCTION IU12(x,y)
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU12=0._8

    IF (0._8 .NE. m11) THEN
       IU12=IU12+m11*T*W/4*(G11d2(L,x*T/2,(1+y)*W/2+q3)-G11d2(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU12=IU12+m12*T*W/4*(G21d2(L,x*T/2,(1+y)*W/2+q3)-G21d2(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m12*L*W/4*(G11d2((1+x)*L/2,T/2,(1+y)*W/2+q3)-G11d2((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU12=IU12+m13*T*W/4*(G31d2(L,x*T/2,(1+y)*W/2+q3)-G31d2(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m13*L*T/4*(G11d2((1+x)*L/2,y*T/2,W+q3)-G11d2((1+x)*L/2,y*T/2,q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU12=IU12+m22*L*W/4*(G21d2((1+x)*L/2,T/2,(1+y)*W/2+q3)-G21d2((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU12=IU12+m23*L*T/4*(G21d2((1+x)*L/2,y*T/2,W+q3)-G21d2((1+x)*L/2,y*T/2,q3)) &
                +m23*L*W/4*(G31d2((1+x)*L/2,T/2,(1+y)*W/2+q3)-G31d2((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU12=IU12+m33*L*T/4*(G31d2((1+x)*L/2,y*T/2,W+q3)-G31d2((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU12

  ! function IU13 is the integrand for displacement gradient u1,3
  REAL*8 FUNCTION IU13(x,y)
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU13=0._8

    IF (0._8 .NE. m11) THEN
       IU13=IU13+m11*T*W/4*(G11d3(L,x*T/2,(1+y)*W/2+q3)-G11d3(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU13=IU13+m12*T*W/4*(G21d3(L,x*T/2,(1+y)*W/2+q3)-G21d3(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m12*L*W/4*(G11d3((1+x)*L/2,T/2,(1+y)*W/2+q3)-G11d3((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU13=IU13+m13*T*W/4*(G31d3(L,x*T/2,(1+y)*W/2+q3)-G31d3(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m13*L*T/4*(G11d3((1+x)*L/2,y*T/2,W+q3)-G11d3((1+x)*L/2,y*T/2,q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU13=IU13+m22*L*W/4*(G21d3((1+x)*L/2,T/2,(1+y)*W/2+q3)-G21d3((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU13=IU13+m23*L*T/4*(G21d3((1+x)*L/2,y*T/2,W+q3)-G21d3((1+x)*L/2,y*T/2,q3)) &
                +m23*L*W/4*(G31d3((1+x)*L/2,T/2,(1+y)*W/2+q3)-G31d3((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU13=IU13+m33*L*T/4*(G31d3((1+x)*L/2,y*T/2,W+q3)-G31d3((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU13

  ! function IU21 is the integrand for displacement gradient u2,1
  REAL*8 FUNCTION IU21(x,y)
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU21=0._8

    IF (0._8 .NE. m11) THEN
       IU21=IU21+m11*T*W/4*(G12d1(L,x*T/2,(1+y)*W/2+q3)-G12d1(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU21=IU21+m12*T*W/4*(G22d1(L,x*T/2,(1+y)*W/2+q3)-G22d1(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m12*L*W/4*(G12d1((1+x)*L/2,T/2,(1+y)*W/2+q3)-G12d1((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU21=IU21+m13*T*W/4*(G32d1(L,x*T/2,(1+y)*W/2+q3)-G32d1(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m13*L*T/4*(G12d1((1+x)*L/2,y*T/2,W+q3)-G12d1((1+x)*L/2,y*T/2,q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU21=IU21+m22*L*W/4*(G22d1((1+x)*L/2,T/2,(1+y)*W/2+q3)-G22d1((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU21=IU21+m23*L*T/4*(G22d1((1+x)*L/2,y*T/2,W+q3)-G22d1((1+x)*L/2,y*T/2,q3)) &
                +m23*L*W/4*(G32d1((1+x)*L/2,T/2,(1+y)*W/2+q3)-G32d1((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU21=IU21+m33*L*T/4*(G32d1((1+x)*L/2,y*T/2,W+q3)-G32d1((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU21

  ! function IU22 is the integrand for displacement gradient u2,2
  REAL*8 FUNCTION IU22(x,y)
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU22=0._8

    IF (0._8 .NE. m11) THEN
       IU22=IU22+m11*T*W/4*(G12d2(L,x*T/2,(1+y)*W/2+q3)-G12d2(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU22=IU22+m12*T*W/4*(G22d2(L,x*T/2,(1+y)*W/2+q3)-G22d2(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m12*L*W/4*(G12d2((1+x)*L/2,T/2,(1+y)*W/2+q3)-G12d2((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU22=IU22+m13*T*W/4*(G32d2(L,x*T/2,(1+y)*W/2+q3)-G32d2(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m13*L*T/4*(G12d2((1+x)*L/2,y*T/2,W+q3)-G12d2((1+x)*L/2,y*T/2,q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU22=IU22+m22*L*W/4*(G22d2((1+x)*L/2,T/2,(1+y)*W/2+q3)-G22d2((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU22=IU22+m23*L*T/4*(G22d2((1+x)*L/2,y*T/2,W+q3)-G22d2((1+x)*L/2,y*T/2,q3)) &
                +m23*L*W/4*(G32d2((1+x)*L/2,T/2,(1+y)*W/2+q3)-G32d2((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU22=IU22+m33*L*T/4*(G32d2((1+x)*L/2,y*T/2,W+q3)-G32d2((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU22

  ! function IU23 is the integrand for displacement gradient u2,3
  REAL*8 FUNCTION IU23(x,y)
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU23=0._8

    IF (0._8 .NE. m11) THEN
       IU23=IU23+m11*T*W/4*(G12d3(L,x*T/2,(1+y)*W/2+q3)-G12d3(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU23=IU23+m12*T*W/4*(G22d3(L,x*T/2,(1+y)*W/2+q3)-G22d3(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m12*L*W/4*(G12d3((1+x)*L/2,T/2,(1+y)*W/2+q3)-G12d3((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU23=IU23+m13*T*W/4*(G32d3(L,x*T/2,(1+y)*W/2+q3)-G32d3(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m13*L*T/4*(G12d3((1+x)*L/2,y*T/2,W+q3)-G12d3((1+x)*L/2,y*T/2,q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU23=IU23+m22*L*W/4*(G22d3((1+x)*L/2,T/2,(1+y)*W/2+q3)-G22d3((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU23=IU23+m23*L*T/4*(G22d3((1+x)*L/2,y*T/2,W+q3)-G22d3((1+x)*L/2,y*T/2,q3)) &
                +m23*L*W/4*(G32d3((1+x)*L/2,T/2,(1+y)*W/2+q3)-G32d3((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU23=IU23+m33*L*T/4*(G32d3((1+x)*L/2,y*T/2,W+q3)-G32d3((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU23

  ! function IU31 is the integrand for displacement gradient u3,1
  REAL*8 FUNCTION IU31(x,y)
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU31=0._8

    IF (0._8 .NE. m11) THEN
       IU31=IU31+m11*T*W/4*(G13d1(L,x*T/2,(1+y)*W/2+q3)-G13d1(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU31=IU31+m12*T*W/4*(G23d1(L,x*T/2,(1+y)*W/2+q3)-G23d1(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m12*L*W/4*(G13d1((1+x)*L/2,T/2,(1+y)*W/2+q3)-G13d1((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU31=IU31+m13*L*T/4*(G13d1((1+x)*L/2,y*T/2,W+q3)-G13d1((1+x)*L/2,y*T/2,q3)) &
                +m13*T*W/4*(G33d1(L,x*T/2,(1+y)*W/2+q3)-G33d1(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU31=IU31+m22*L*W/4*(G23d1((1+x)*L/2,T/2,(1+y)*W/2+q3)-G23d1((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU31=IU31+m23*L*T/4*(G23d1((1+x)*L/2,y*T/2,W+q3)-G23d1((1+x)*L/2,y*T/2,q3)) &
                +m23*L*W/4*(G33d1((1+x)*L/2,T/2,(1+y)*W/2+q3)-G33d1((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU31=IU31+m33*L*T/4*(G33d1((1+x)*L/2,y*T/2,W+q3)-G33d1((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU31

  ! function IU32 is the integrand for displacement component u3,2
  REAL*8 FUNCTION IU32(x,y)
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU32=0._8

    IF (0._8 .NE. m11) THEN
       IU32=IU32+m11*T*W/4*(G13d2(L,x*T/2,(1+y)*W/2+q3)-G13d2(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU32=IU32+m12*T*W/4*(G23d2(L,x*T/2,(1+y)*W/2+q3)-G23d2(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m12*L*W/4*(G13d2((1+x)*L/2,T/2,(1+y)*W/2+q3)-G13d2((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU32=IU32+m13*L*T/4*(G13d2((1+x)*L/2,y*T/2,W+q3)-G13d2((1+x)*L/2,y*T/2,q3)) &
                +m13*T*W/4*(G33d2(L,x*T/2,(1+y)*W/2+q3)-G33d2(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU32=IU32+m22*L*W/4*(G23d2((1+x)*L/2,T/2,(1+y)*W/2+q3)-G23d2((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU32=IU32+m23*L*T/4*(G23d2((1+x)*L/2,y*T/2,W+q3)-G23d2((1+x)*L/2,y*T/2,q3)) &
                +m23*L*W/4*(G33d2((1+x)*L/2,T/2,(1+y)*W/2+q3)-G33d2((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU32=IU32+m33*L*T/4*(G33d2((1+x)*L/2,y*T/2,W+q3)-G33d2((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU32

  ! function IU33 is the integrand for displacement gradient u3,3
  REAL*8 FUNCTION IU33(x,y)
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU33=0._8

    IF (0._8 .NE. m11) THEN
       IU33=IU33+m11*T*W/4*(G13d3(L,x*T/2,(1+y)*W/2+q3)-G13d3(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU33=IU33+m12*T*W/4*(G23d3(L,x*T/2,(1+y)*W/2+q3)-G23d3(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m12*L*W/4*(G13d3((1+x)*L/2,T/2,(1+y)*W/2+q3)-G13d3((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU33=IU33+m13*L*T/4*(G13d3((1+x)*L/2,y*T/2,W+q3)-G13d3((1+x)*L/2,y*T/2,q3)) &
                +m13*T*W/4*(G33d3(L,x*T/2,(1+y)*W/2+q3)-G33d3(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU33=IU33+m22*L*W/4*(G23d3((1+x)*L/2,T/2,(1+y)*W/2+q3)-G23d3((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU33=IU33+m23*L*T/4*(G23d3((1+x)*L/2,y*T/2,W+q3)-G23d3((1+x)*L/2,y*T/2,q3)) &
                +m23*L*W/4*(G33d3((1+x)*L/2,T/2,(1+y)*W/2+q3)-G33d3((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU33=IU33+m33*L*T/4*(G33d3((1+x)*L/2,y*T/2,W+q3)-G33d3((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU33

  !------------------------------------------------------------------------
  !> function Omega(x)
  !! evaluates the boxcar function
  !------------------------------------------------------------------------
  REAL*8 FUNCTION omega(x)
    REAL*8, INTENT(IN) :: x

    omega=heaviside(x+0.5_8)-heaviside(x-0.5_8)

  END FUNCTION omega

  !------------------------------------------------------------------------
  !> function S(x)
  !! evalutes the shifted boxcar function
  !------------------------------------------------------------------------
  REAL*8 FUNCTION s(x)
    REAL*8, INTENT(IN) :: x

    s=omega(x-0.5_8)

  END FUNCTION s

  !------------------------------------------------------------------------
  !> function heaviside
  !! computes the Heaviside function
  !------------------------------------------------------------------------
  REAL*8 FUNCTION heaviside(x)
    REAL*8, INTENT(IN) :: x

     IF (0 .LT. x) THEN
        heaviside=1.0_8
     ELSE
        heaviside=0.0_8
     END IF

  END FUNCTION heaviside

END SUBROUTINE computeStressVerticalCuboidMixedQuad
