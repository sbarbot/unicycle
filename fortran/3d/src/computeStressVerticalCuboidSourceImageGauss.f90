!------------------------------------------------------------------------
!! subroutine computeStressVerticalCuboidSourceImageGauss
!! computes the stress field associated with vertical cuboid volume element
!! using the Gauss-Legendre quadrature with various orders depending on
!! the source-receiver distance, as in 
!!
!!   Barbot S., "Deformation of a Half-Space from Anelastic Strain Confined
!!   in a Tetrahedral Volume", Bull. Seism. Soc. Am., 10.1785/0120180058, 2018.
!!
!! and considering the following geometry:
!!
!!                     N (x1)
!!                    /
!!                   /| strike (theta)
!!       q1,q2,q3 ->@-------------------------+
!!                  |                         |     E (x2)
!!                  :                       w |      +
!!                  |                       i |     / 
!!                  :                       d |    / s
!!                  |                       t |   / s
!!                  :                       h |  / e
!!                  |                         | / n
!!                  +-------------------------+  k
!!                  :       l e n g t h       / c
!!                  |                        / i
!!                  :                       / h
!!                  |                      / t
!!                  :                     /
!!                  |                    +
!!                  Z (x3)
!!
!!
!! INPUT:
!! x1, x2, x3         north, east, and depth coordinates of the observation points
!! q1, q2, q3         north, east and depth coordinates of the cuboid volume
!! L, T, W            length, thickness and width of the cuboid volume
!! theta (radians)    strike angle from north (from x1) of the cuboid volume
!! eijp               source strain component ij in the cuboid volume
!!                    in the system of reference tied to the cuboid volume
!! G,nu               rigidity and Poisson's ratio in the half space
!!
!! OUTPUT:
!! sij                stress components in the north, east, depth reference system.
!!
!! \author Sylvain Barbot (09/27/18) - original Fortran form
!-----------------------------------------------------------------------
SUBROUTINE computeStressVerticalCuboidSourceImageGauss( &
                        x1,x2,x3,q1,q2,q3,L,T,W,theta, &
                        e11p,e12p,e13p,e22p,e23p,e33p,G,nu, &
                        s11,s12,s13,s22,s23,s33)

  IMPLICIT NONE

  REAL*8, INTENT(IN) :: x1,x2,x3
  REAL*8, INTENT(IN) :: q1,q2,q3
  REAL*8, INTENT(IN) :: L,T,W,theta
  REAL*8, INTENT(IN) :: e11p,e12p,e13p,e22p,e23p,e33p
  REAL*8, INTENT(IN) :: G,nu
  REAL*8, INTENT(OUT) :: s11,s12,s13,s22,s23,s33

  REAL*8, PARAMETER :: PI = 3.141592653589793115997963468544185161_8

  ! Gauss-Legendre quadrature positions and weights
  INCLUDE 'gauss-3.inc'
  INCLUDE 'gauss-7.inc'
  INCLUDE 'gauss-15.inc'
  INCLUDE 'gauss-40.inc'

  ! trace
  REAL*8 :: ekk

  ! moment density
  REAL*8 :: m11,m12,m13,m22,m23,m33

  ! displacement gradient
  REAL*8 :: u11,u12,u13,u21,u22,u23,u31,u32,u33

  ! strain components in unprimed coordinate system
  REAL*8 :: e11,e12,e13,e22,e23,e33

  ! eigenstrain in unprimed coordinate system
  REAL*8 :: eps11,eps12,eps13,eps22,eps23,eps33

  ! strain in primed coordinate system
  REAL*8 :: v11p,v12p,v13p,v22p,v23p,v33p

  ! position in primed coordinate system
  REAL*8 :: x1p,x2p

  ! Lame parameter
  REAL*8 :: lambda

  ! numerical integration coordinates and weight
  REAL*8 :: xk,wk,xj,wj

  ! counters, bound
  INTEGER :: j,k

  ! pseudo center
  REAL*8, DIMENSION(3) :: O

  ! pseudo radius
  REAL*8 :: r

  ! relative source-receiver distance
  REAL*8 :: rho

  ! check valid parameters
  IF ((-1._8 .GT. nu) .OR. (0.5_8 .LT. nu)) THEN
     WRITE (0,'("error: -1<=nu<=0.5, nu=",ES9.2E2," given.")') nu
     STOP 1
  END IF

  IF (0 .GT. x3) THEN
     WRITE (0,'("error: observation depth (x3) must be positive")')
     STOP 1
  END IF

  ! Lame parameter (normalized by rigidity)
  lambda=2*nu/(1._8-2*nu)

  ! trace
  ekk=e11p+e22p+e33p

  ! moment density (normalized by rigidity)
  m11=2*e11p+lambda*ekk
  m12=2*e12p
  m13=2*e13p
  m22=2*e22p+lambda*ekk
  m23=2*e23p
  m33=2*e33p+lambda*ekk

  ! center of cuboid
  O=(/ q1+L/2*DCOS(theta), q2+L/2*DSIN(theta), q3+W/2 /)

  ! radius
  r=MAX(L/2,W/2,SQRT(W*L))*SQRT(2._8)

  ! rotate observation points to the volume-element-centric system of coordinates
  x1p= (x1-q1)*DCOS(theta)+(x2-q2)*DSIN(theta)
  x2p=-(x1-q1)*DSIN(theta)+(x2-q2)*DCOS(theta)

  ! numerical solution
  u11=0.d0; u12=0.d0; u13=0.d0
  u21=0.d0; u22=0.d0; u23=0.d0
  u31=0.d0; u32=0.d0; u33=0.d0

  ! relative source-receiver distance
  rho=SQRT((x1-O(1))**2+(x2-O(2))**2+(x3-O(3))**2)/r

  ! choose integration method based on distance from circumcenter
  IF (rho .LE. 1.1d0) THEN
     ! Gauss-Legendre quadrature with 40 points
     DO k=1,40
        xk=p40(k)
        wk=w40(k)

        DO j=1,40
           xj=p40(j)
           wj=w40(j)

           ! derivatives of the u1 component
           u11=u11+wj*wk*IU11s(xj,xk)
           u12=u12+wj*wk*IU12s(xj,xk)
           u13=u13+wj*wk*IU13s(xj,xk)
           
           ! derivatives of the u2 component
           u21=u21+wj*wk*IU21s(xj,xk)
           u22=u22+wj*wk*IU22s(xj,xk)
           u23=u23+wj*wk*IU23s(xj,xk)
           
           ! derivatives of the u3 component
           u31=u31+wj*wk*IU31s(xj,xk)
           u32=u32+wj*wk*IU32s(xj,xk)
           u33=u33+wj*wk*IU33s(xj,xk)
           
        END DO
     END DO

  ELSE IF (rho .LE. 10.0d0) THEN
     ! Gauss-Legendre quadrature with 15 points
     DO k=1,15
        xk=p15(k)
        wk=w15(k)

        DO j=1,15
           xj=p15(j)
           wj=w15(j)

           ! derivatives of the u1 component
           u11=u11+wj*wk*IU11s(xj,xk)
           u12=u12+wj*wk*IU12s(xj,xk)
           u13=u13+wj*wk*IU13s(xj,xk)

           ! derivatives of the u2 component
           u21=u21+wj*wk*IU21s(xj,xk)
           u22=u22+wj*wk*IU22s(xj,xk)
           u23=u23+wj*wk*IU23s(xj,xk)

           ! derivatives of the u3 component
           u31=u31+wj*wk*IU31s(xj,xk)
           u32=u32+wj*wk*IU32s(xj,xk)
           u33=u33+wj*wk*IU33s(xj,xk)

        END DO
     END DO
  ELSE IF (rho .LE. 20.0d0) THEN
     ! Gauss-Legendre quadrature with 7 points
     DO k=1,7
        xk=p7(k)
        wk=w7(k)

        DO j=1,7
           xj=p7(j)
           wj=w7(j)

           ! derivatives of the u1 component
           u11=u11+wj*wk*IU11s(xj,xk)
           u12=u12+wj*wk*IU12s(xj,xk)
           u13=u13+wj*wk*IU13s(xj,xk)

           ! derivatives of the u2 component
           u21=u21+wj*wk*IU21s(xj,xk)
           u22=u22+wj*wk*IU22s(xj,xk)
           u23=u23+wj*wk*IU23s(xj,xk)

           ! derivatives of the u3 component
           u31=u31+wj*wk*IU31s(xj,xk)
           u32=u32+wj*wk*IU32s(xj,xk)
           u33=u33+wj*wk*IU33s(xj,xk)

        END DO
     END DO
  ELSE
     ! Gauss-Legendre quadrature with 3 points
     DO k=1,3
        xk=p3(k)
        wk=w3(k)

        DO j=1,3
           xj=p3(j)
           wj=w3(j)

           ! derivatives of the u1 component
           u11=u11+wj*wk*IU11s(xj,xk)
           u12=u12+wj*wk*IU12s(xj,xk)
           u13=u13+wj*wk*IU13s(xj,xk)

           ! derivatives of the u2 component
           u21=u21+wj*wk*IU21s(xj,xk)
           u22=u22+wj*wk*IU22s(xj,xk)
           u23=u23+wj*wk*IU23s(xj,xk)

           ! derivatives of the u3 component
           u31=u31+wj*wk*IU31s(xj,xk)
           u32=u32+wj*wk*IU32s(xj,xk)
           u33=u33+wj*wk*IU33s(xj,xk)

        END DO
     END DO
  END IF

  ! Gauss-Legendre quadrature with 3 points for the image
  DO k=1,3
     xk=p3(k)
     wk=w3(k)

     DO j=1,3
        xj=p3(j)
        wj=w3(j)

        ! derivatives of the u1 component
        u11=u11+wj*wk*IU11i(xj,xk)
        u12=u12+wj*wk*IU12i(xj,xk)
        u13=u13+wj*wk*IU13i(xj,xk)

        ! derivatives of the u2 component
        u21=u21+wj*wk*IU21i(xj,xk)
        u22=u22+wj*wk*IU22i(xj,xk)
        u23=u23+wj*wk*IU23i(xj,xk)

        ! derivatives of the u3 component
        u31=u31+wj*wk*IU31i(xj,xk)
        u32=u32+wj*wk*IU32i(xj,xk)
        u33=u33+wj*wk*IU33i(xj,xk)

     END DO
  END DO

  ! strain in the reference system tied to the volume element (primed)
  v11p= u11
  v12p=(u12+u21)/2
  v13p=(u13+u31)/2
  v22p= u22
  v23p=(u23+u32)/2
  v33p= u33

  ! rotate strain field to reference (unprimed) system of coordinates
  e11=(DCOS(theta)*v11p-DSIN(theta)*v12p)*DCOS(theta)-(DCOS(theta)*v12p-DSIN(theta)*v22p)*DSIN(theta)
  e12=(DCOS(theta)*v11p-DSIN(theta)*v12p)*DSIN(theta)+(DCOS(theta)*v12p-DSIN(theta)*v22p)*DCOS(theta)
  e13= DCOS(theta)*v13p-DSIN(theta)*v23p
  e22=(DSIN(theta)*v11p+DCOS(theta)*v12p)*DSIN(theta)+(DSIN(theta)*v12p+DCOS(theta)*v22p)*DCOS(theta)
  e23= DSIN(theta)*v13p+DCOS(theta)*v23p
  e33=v33p
 
  ! rotate anelastic strain field to reference (unprimed) system of coordinates
  eps11=(DCOS(theta)*e11p-DSIN(theta)*e12p)*DCOS(theta)-(DCOS(theta)*e12p-DSIN(theta)*e22p)*DSIN(theta)
  eps12=(DCOS(theta)*e11p-DSIN(theta)*e12p)*DSIN(theta)+(DCOS(theta)*e12p-DSIN(theta)*e22p)*DCOS(theta)
  eps13= DCOS(theta)*e13p-DSIN(theta)*e23p
  eps22=(DSIN(theta)*e11p+DCOS(theta)*e12p)*DSIN(theta)+(DSIN(theta)*e12p+DCOS(theta)*e22p)*DCOS(theta)
  eps23= DSIN(theta)*e13p+DCOS(theta)*e23p
  eps33=e33p

  ! remove anelastic eigenstrain
  e11=e11-eps11*S(x1p/L)*Omega(x2p/T)*S((x3-q3)/W)
  e12=e12-eps12*S(x1p/L)*Omega(x2p/T)*S((x3-q3)/W)
  e13=e13-eps13*S(x1p/L)*Omega(x2p/T)*S((x3-q3)/W)
  e22=e22-eps22*S(x1p/L)*Omega(x2p/T)*S((x3-q3)/W)
  e23=e23-eps23*S(x1p/L)*Omega(x2p/T)*S((x3-q3)/W)
  e33=e33-eps33*S(x1p/L)*Omega(x2p/T)*S((x3-q3)/W)

  ! trace
  ekk=e11+e22+e33

  ! stress components
  s11=G*(2*e11+lambda*ekk)
  s12=G*(2*e12)
  s13=G*(2*e13)
  s22=G*(2*e22+lambda*ekk)
  s23=G*(2*e23)
  s33=G*(2*e33+lambda*ekk)

CONTAINS

  !---------------------------------------------------------------
  !> function r1
  !! distance from source
  !---------------------------------------------------------------
  REAL*8 FUNCTION r1(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    r1=SQRT((x1p-y1)**2+(x2p-y2)**2+(x3-y3)**2)

  END FUNCTION r1

  !---------------------------------------------------------------
  !> function r2
  !! distance from image
  !---------------------------------------------------------------
  REAL*8 FUNCTION r2(y1,y2,y3)
    REAL*8, INTENT(IN) :: y1,y2,y3

    r2=SQRT((x1p-y1)**2+(x2p-y2)**2+(x3+y3)**2)

  END FUNCTION r2

  !---------------------------------------------------------------
  !> function G11d1s
  !! Green's function G11d1s for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,1 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G11d1s(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G11d1s=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*( &
       -(3._8-4._8*nu)/lr1**3 &
       +(2*lr1**2-3._8*(x1p-y1)**2)/lr1**5 &
       )
          
  END FUNCTION G11d1s

  !---------------------------------------------------------------
  !> function G11d2s
  !! Green's function G11d2s for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,2 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G11d2s(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G11d2s=1._8/(16._8*PI*(1._8-nu))*(x2p-y2)*( &
       -(3._8-4._8*nu)/lr1**3 &
       -3._8*(x1p-y1)**2/lr1**5 &
    )

  END FUNCTION G11d2s

  !---------------------------------------------------------------
  !> function G11d3s
  !! Green's function G11d3s for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,3 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G11d3s(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G11d3s=1._8/(16._8*PI*(1._8-nu))*( &
       -(3._8-4._8*nu)*(x3-y3)/lr1**3 &
       -3._8*(x1p-y1)**2*(x3-y3)/lr1**5 &
    )

  END FUNCTION G11d3s

  !---------------------------------------------------------------
  !> function G21d1s
  !! Green's function G21d1s for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,1 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G21d1s(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)
          
    G21d1s=1._8/(16._8*PI*(1._8-nu))*(x2p-y2)*( &
    +(lr1**2-3._8*(x1p-y1)**2)/lr1**5 &
    )

  END FUNCTION G21d1s

  !---------------------------------------------------------------
  !> function G21d2s
  !! Green's function G21d2s for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,2 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G21d2s(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G21d2s=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*( &
       +(lr1**2-3._8*(x2p-y2)**2)/lr1**5 &
    )

  END FUNCTION G21d2s

  !---------------------------------------------------------------
  !> function G21d3s
  !! Green's function G21d3s for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,3 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G21d3s(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G21d3s=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*(x2p-y2)*( &
       -3._8*(x3-y3)/lr1**5 &
    )

  END FUNCTION G21d3s

  !---------------------------------------------------------------
  !> function G31d1s
  !! Green's function G31d1s for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,1 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G31d1s(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G31d1s=1._8/(16._8*PI*(1._8-nu))*( &
       +(x3-y3)*(lr1**2-3._8*(x1p-y1)**2)/lr1**5 &
    )

  END FUNCTION G31d1s

  !---------------------------------------------------------------
  !> function G31d2s
  !! Green's function G31d2s for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,2 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G31d2s(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G31d2s=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*(x2p-y2)*( &
       -3._8*(x3-y3)/lr1**5 &
    )

  END FUNCTION G31d2s

  !---------------------------------------------------------------
  !> function G31d3s
  !! Green's function G31d3s for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,3 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G31d3s(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G31d3s=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*( &
       +(lr1**2-3*(x3-y3)**2)/lr1**5 &
    )

  END FUNCTION G31d3s

  !---------------------------------------------------------------
  !> function G12d1s
  !! Green's function G12d1s for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,1 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G12d1s(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G12d1s=1._8/(16._8*PI*(1._8-nu))*(x2p-y2)*( &
       +(lr1**2-3*(x1p-y1)**2)/lr1**5 &
    )

  END FUNCTION G12d1s

  !---------------------------------------------------------------
  !> function G12d2s
  !! Green's function G12d2s for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,2 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G12d2s(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G12d2s=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*( &
       +(lr1**2-3._8*(x2p-y2)**2)/lr1**5 &
    )

  END FUNCTION G12d2s

  !---------------------------------------------------------------
  !> function G12d3s
  !! Green's function G12d3s for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,3 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G12d3s(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G12d3s=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*(x2p-y2)*( &
    -3._8*(x3-y3)/lr1**5 &
    )

  END FUNCTION G12d3s

  !---------------------------------------------------------------
  !> function G22d1s
  !! Green's function G22d1s for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,1 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G22d1s(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G22d1s=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*( &
       -(3._8-4._8*nu)/lr1**3 &
       -3._8*(x2p-y2)**2/lr1**5 &
    )

  END FUNCTION G22d1s

  !---------------------------------------------------------------
  !> function G22d2s
  !! Green's function G22d2s for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,2 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G22d2s(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G22d2s=1._8/(16._8*PI*(1._8-nu))*(x2p-y2)*( &
       -(3._8-4._8*nu)/lr1**3 &
       +(2*lr1**2-3._8*(x2p-y2)**2)/lr1**5 &
    )

  END FUNCTION G22d2s

  !---------------------------------------------------------------
  !> function G22d3s
  !! Green's function G22d3s for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,3 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G22d3s(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G22d3s=1._8/(16._8*PI*(1._8-nu))*( &
       -(3._8-4._8*nu)*(x3-y3)/lr1**3 &
       -3._8*(x2p-y2)**2*(x3-y3)/lr1**5 &
    )

  END FUNCTION G22d3s

  !---------------------------------------------------------------
  !> function G32d1s
  !! Green's function G32d1s for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,1 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G32d1s(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G32d1s=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*(x2p-y2)*( &
       -3._8*(x3-y3)/lr1**5 &
    )

  END FUNCTION G32d1s

  !---------------------------------------------------------------
  !> function G32d2s
  !! Green's function G32d2s for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,2 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G32d2s(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G32d2s=1._8/(16._8*PI*(1._8-nu))*( &
       +(x3-y3)*(lr1**2-3*(x2p-y2)**2)/lr1**5 &
    )

  END FUNCTION G32d2s

  !---------------------------------------------------------------
  !> function G32d3s
  !! Green's function G32d3s for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,3 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G32d3s(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G32d3s=1._8/(16._8*PI*(1._8-nu))*(x2p-y2)*( &
       +(lr1**2-3*(x3-y3)**2)/lr1**5 &
    )

  END FUNCTION G32d3s

  !---------------------------------------------------------------
  !> function G13d1s
  !! Green's function G13d1s for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,1 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G13d1s(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G13d1s=1._8/(16._8*PI*(1._8-nu))*( &
       +(x3-y3)*(lr1**2-3._8*(x1p-y1)**2)/lr1**5 &
    )

  END FUNCTION G13d1s

  !---------------------------------------------------------------
  !> function G13d2s
  !! Green's function G13d2s for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,2 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G13d2s(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G13d2s=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*(x2p-y2)*( &
       -3._8*(x3-y3)/lr1**5 &
    )

  END FUNCTION G13d2s

  !---------------------------------------------------------------
  !> function G13d3s
  !! Green's function G13d3s for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,3 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G13d3s(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)
          
    G13d3s=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*( &
       +(lr1**2-3*(x3-y3)**2)/lr1**5 &
    )

  END FUNCTION G13d3s

  !---------------------------------------------------------------
  !> function G23d1s
  !! Green's function G23d1s for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,1 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G23d1s(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G23d1s=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*(x2p-y2)*( &
       -3._8*(x3-y3)/lr1**5 &
    )

  END FUNCTION G23d1s

  !---------------------------------------------------------------
  !> function G23d2s
  !! Green's function G23d2s for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,2 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G23d2s(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G23d2s=1._8/(16._8*PI*(1._8-nu))*( &
       +(x3-y3)*(lr1**2-3*(x2p-y2)**2)/lr1**5 &
    )

  END FUNCTION G23d2s

  !---------------------------------------------------------------
  !> function G23d3s
  !! Green's function G23d3s for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,3 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G23d3s(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G23d3s=1._8/(16*PI*(1._8-nu))*(x2p-y2)*( &
       +(lr1**2-3*(x3-y3)**2)/lr1**5 &
    )

  END FUNCTION G23d3s

  !---------------------------------------------------------------
  !> function G33d1s
  !! Green's function G33d1s for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,1 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G33d1s(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G33d1s=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*( &
       -(3._8-4._8*nu)/lr1**3 &
       -3._8*(x3-y3)**2/lr1**5 &
    )

  END FUNCTION G33d1s

  !---------------------------------------------------------------
  !> function G33d2s
  !! Green's function G33d2s for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,2 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G33d2s(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G33d2s=1._8/(16._8*PI*(1._8-nu))*(x2p-y2)*( &
       -(3._8-4._8*nu)/lr1**3 &
       -3._8*(x3-y3)**2/lr1**5 &
    )

  END FUNCTION G33d2s

  !---------------------------------------------------------------
  !> function G33d3s
  !! Green's function G33d3s for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,3 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G33d3s(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr1

    lr1=r1(y1,y2,y3)

    G33d3s=1._8/(16._8*PI*(1._8-nu))*( &
       -(3._8-4._8*nu)*(x3-y3)/lr1**3 &
       +(x3-y3)*(2*lr1**2-3*(x3-y3)**2)/lr1**5 &
    )

  END FUNCTION G33d3s

  !---------------------------------------------------------------
  !> function G11d1i
  !! Green's function G11d1i for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,1 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G11d1i(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
          
    G11d1i=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*( &
       -1._8/lr2**3 &
       +(3._8-4._8*nu)*(2*lr2**2-3._8*(x1p-y1)**2)/lr2**5 &
       -6._8*y3*x3*(3._8*lr2**2-5._8*(x1p-y1)**2)/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)**2) &
       -8._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)**2) &
           +4._8*(1._8-2*nu)*(1._8-nu)*(x1p-y1)**2/(lr2**3*(lr2+x3+y3)**2) &
           +8._8*(1._8-2*nu)*(1._8-nu)*(x1p-y1)**2/(lr2**2*(lr2+x3+y3)**3) &
       )

  END FUNCTION G11d1i

  !---------------------------------------------------------------
  !> function G11d2i
  !! Green's function G11d2i for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,2 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G11d2i(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
          
    G11d2i=1._8/(16._8*PI*(1._8-nu))*(x2p-y2)*( &
       -1._8/lr2**3 &
       -3._8*(3-4*nu)*(x1p-y1)**2/lr2**5 &
       -6._8*y3*x3*(lr2**2-5._8*(x1p-y1)**2)/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)**2) &
       +4._8*(1._8-2*nu)*(1._8-nu)*(x1p-y1)**2*(3._8*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**3) &
    )

  END FUNCTION G11d2i

  !---------------------------------------------------------------
  !> function G11d3i
  !! Green's function G11d3i for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,3 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G11d3i(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
    
    G11d3i=1._8/(16._8*PI*(1._8-nu))*( &
       -(x3+y3)/lr2**3 &
       -3._8*(3._8-4*nu)*(x1p-y1)**2*(x3+y3)/lr2**5 &
       +2*y3*(lr2**2-3._8*x3*(x3+y3))/lr2**5 &
       -6._8*y3*(x1p-y1)**2*(lr2**2-5._8*x3*(x3+y3))/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)) &
       +4._8*(1._8-2*nu)*(1._8-nu)*(x1p-y1)**2*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G11d3i

  !---------------------------------------------------------------
  !> function G21d1i
  !! Green's function G21d1i for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,1 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G21d1i(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
          
    G21d1i=1._8/(16._8*PI*(1._8-nu))*(x2p-y2)*( &
    +(3._8-4._8*nu)*(lr2**2-3._8*(x1p-y1)**2)/lr2**5 &
    -6._8*y3*x3*(lr2**2-5._8*(x1p-y1)**2)/lr2**7 &
    -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)**2) &
    +4._8*(1._8-2*nu)*(1._8-nu)*(x1p-y1)**2*(3._8*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**3) &
    )

  END FUNCTION G21d1i

  !---------------------------------------------------------------
  !> function G21d2i
  !! Green's function G21d2i for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,2 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G21d2i(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
          
    G21d2i=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*( &
       +(3._8-4._8*nu)*(lr2**2-3._8*(x2p-y2)**2)/lr2**5 &
       -6._8*y3*x3*(lr2**2-5._8*(x2p-y2)**2)/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)**2) &
       +4._8*(1._8-2*nu)*(1._8-nu)*(x2p-y2)**2*(3._8*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**3) &
    )

  END FUNCTION G21d2i

  !---------------------------------------------------------------
  !> function G21d3i
  !! Green's function G21d3i for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,3 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G21d3i(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
          
    G21d3i=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*(x2p-y2)*( &
       -3._8*(3._8-4._8*nu)*(x3+y3)/lr2**5 &
       -6._8*y3*(lr2**2-5._8*x3*(x3+y3))/lr2**7 &
       +4._8*(1._8-2*nu)*(1._8-nu)*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G21d3i

  !---------------------------------------------------------------
  !> function G31d1i
  !! Green's function G31d1i for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,1 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G31d1i(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
    
    G31d1i=1._8/(16._8*PI*(1._8-nu))*( &
       +(3._8-4._8*nu)*(x3-y3)*(lr2**2-3*(x1p-y1)**2)/lr2**5 &
       +6._8*y3*x3*(x3+y3)*(lr2**2-5*(x1p-y1)**2)/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)) &
       +4._8*(1._8-2*nu)*(1._8-nu)*(x1p-y1)**2*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G31d1i

  !---------------------------------------------------------------
  !> function G31d2i
  !! Green's function G31d2i for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,2 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G31d2i(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
          
    G31d2i=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*(x2p-y2)*( &
       -3._8*(3._8-4._8*nu)*(x3-y3)/lr2**5 &
       -30._8*y3*x3*(x3+y3)/lr2**7 &
       +4._8*(1._8-2*nu)*(1._8-nu)*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G31d2i

  !---------------------------------------------------------------
  !> function G31d3i
  !! Green's function G31d3i for an elastic half space in 3d
  !! corresponding to the displacement gradient u1,3 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G31d3i(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
          
    G31d3i=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*( &
       +(3._8-4._8*nu)*(lr2**2-3._8*(x3**2-y3**2))/lr2**5 &
       +6._8*y3*(2*x3+y3)/lr2**5 &
       -30._8*y3*x3*(x3+y3)**2/lr2**7 &
       +4._8*(1._8-2*nu)*(1-nu)/lr2**3 &
    )

  END FUNCTION G31d3i

  !---------------------------------------------------------------
  !> function G12d1i
  !! Green's function G12d1i for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,1 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G12d1i(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
          
    G12d1i=1._8/(16._8*PI*(1._8-nu))*(x2p-y2)*( &
       +(3._8-4*nu)*(lr2**2-3*(x1p-y1)**2)/lr2**5 &
       -6._8*y3*x3*(lr2**2-5*(x1p-y1)**2)/lr2**7 &
       -4._8*(1._8-nu)*(1._8-2*nu)/(lr2*(lr2+x3+y3)**2) &
       +4._8*(1._8-nu)*(1._8-2*nu)*(x1p-y1)**2*(3._8*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**3) &
    )

  END FUNCTION G12d1i

  !---------------------------------------------------------------
  !> function G12d2i
  !! Green's function G12d2i for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,2 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G12d2i(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
          
    G12d2i=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*( &
       +(3._8-4._8*nu)*(lr2**2-3._8*(x2p-y2)**2)/lr2**5 &
       -6._8*y3*x3*(lr2**2-5._8*(x2p-y2)**2)/lr2**7 &
       -4._8*(1._8-nu)*(1._8-2*nu)/(lr2*(lr2+x3+y3)**2) &
       +4._8*(1._8-nu)*(1._8-2*nu)*(x2p-y2)**2*(3*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**3) &
    )

  END FUNCTION G12d2i

  !---------------------------------------------------------------
  !> function G12d3i
  !! Green's function G12d3 for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,3 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G12d3i(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
          
    G12d3i=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*(x2p-y2)*( &
    -3._8*(3._8-4._8*nu)*(x3+y3)/lr2**5 &
    -6._8*y3*(lr2**2-5*x3*(x3+y3))/lr2**7 &
    +4._8*(1._8-2*nu)*(1._8-nu)*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G12d3i

  !---------------------------------------------------------------
  !> function G22d1i
  !! Green's function G22d1i for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,1 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G22d1i(y1,y2,y3) 
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
          
    G22d1i=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*( &
       -1._8/lr2**3 &
       -3._8*(3._8-4._8*nu)*(x2p-y2)**2/lr2**5 &
       -6._8*y3*x3*(lr2**2-5._8*(x2p-y2)**2)/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)**2) &
       +4._8*(1._8-2*nu)*(1._8-nu)*(x2p-y2)**2*(3._8*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**3) &
    )

  END FUNCTION G22d1i

  !---------------------------------------------------------------
  !> function G22d2i
  !! Green's function G22d2i for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,2 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G22d2i(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
          
    G22d2i=1._8/(16._8*PI*(1._8-nu))*(x2p-y2)*( &
       -1._8/lr2**3 &
       +(3._8-4._8*nu)*(2*lr2**2-3._8*(x2p-y2)**2)/lr2**5 &
       -6._8*y3*x3*(3._8*lr2**2-5*(x2p-y2)**2)/lr2**7 &
       -12._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)**2) &
       +4._8*(1._8-2*nu)*(1._8-nu)*(x2p-y2)**2*(3._8*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**3) &
    )

  END FUNCTION G22d2i

  !---------------------------------------------------------------
  !> function G22d3i
  !! Green's function G22d3i for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,3 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G22d3i(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
          
    G22d3i=1._8/(16._8*PI*(1._8-nu))*( &
       -(x3+y3)/lr2**3 &
       -3._8*(3-4*nu)*(x2p-y2)**2*(x3+y3)/lr2**5 &
       +2*y3*(lr2**2-3*x3*(x3+y3))/lr2**5 &
       -6._8*y3*(x2p-y2)**2*(lr2**2-5*x3*(x3+y3))/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)) &
       +4._8*(1._8-2*nu)*(1._8-nu)*(x2p-y2)**2*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G22d3i

  !---------------------------------------------------------------
  !> function G32d1i
  !! Green's function G32d1i for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,1 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G32d1i(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
          
    G32d1i=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*(x2p-y2)*( &
       -3._8*(3._8-4._8*nu)*(x3-y3)/lr2**5 &
       -30._8*y3*x3*(x3+y3)/lr2**7 &
       +4._8*(1._8-2*nu)*(1._8-nu)*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G32d1i

  !---------------------------------------------------------------
  !> function G32d2i
  !! Green's function G32d2i for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,2 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G32d2i(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
          
    G32d2i=1._8/(16._8*PI*(1._8-nu))*( &
       +(3._8-4._8*nu)*(x3-y3)*(lr2**2-3*(x2p-y2)**2)/lr2**5 &
       +6._8*y3*x3*(x3+y3)*(lr2**2-5*(x2p-y2)**2)/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)) &
       +4._8*(1._8-2*nu)*(1._8-nu)*(x2p-y2)**2*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G32d2i

  !---------------------------------------------------------------
  !> function G32d3i
  !! Green's function G32d3i for an elastic half space in 3d
  !! corresponding to the displacement gradient u2,3 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G32d3i(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
          
    G32d3i=1._8/(16._8*PI*(1._8-nu))*(x2p-y2)*( &
       +(3._8-4._8*nu)*(lr2**2-3*(x3**2-y3**2))/lr2**5 &
       +6._8*y3*(2*x3+y3)/lr2**5 &
       -30._8*y3*x3*(x3+y3)**2/lr2**7 &
       +4._8*(1._8-2*nu)*(1-nu)/lr2**3 &
    )

  END FUNCTION G32d3i

  !---------------------------------------------------------------
  !> function G13d1i
  !! Green's function G13d1i for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,1 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G13d1i(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
          
    G13d1i=1._8/(16._8*PI*(1._8-nu))*( &
       +(3._8-4*nu)*(x3-y3)*(lr2**2-3*(x1p-y1)**2)/lr2**5 &
       -6._8*y3*x3*(x3+y3)*(lr2**2-5*(x1p-y1)**2)/lr2**7 &
       +4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)) &
       -4._8*(1._8-2*nu)*(1._8-nu)*(x1p-y1)**2*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G13d1i

  !---------------------------------------------------------------
  !> function G13d2i
  !! Green's function G13d2i for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,2 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G13d2i(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
          
    G13d2i=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*(x2p-y2)*( &
       -3._8*(3._8-4._8*nu)*(x3-y3)/lr2**5 &
       +30._8*y3*x3*(x3+y3)/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G13d2i

  !---------------------------------------------------------------
  !> function G13d3i
  !! Green's function G13d3i for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,3 due to a
  !! point force pointing in the x1 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G13d3i(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
          
    G13d3i=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*( &
       +(3._8-4*nu)*(lr2**2-3*(x3**2-y3**2))/lr2**5 &
       -6._8*y3*(2*x3+y3)/lr2**5 &
       +30._8*y3*x3*(x3+y3)**2/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/lr2**3 &
    )

  END FUNCTION G13d3i

  !---------------------------------------------------------------
  !> function G23d1i
  !! Green's function G23d1i for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,1 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G23d1i(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
          
    G23d1i=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*(x2p-y2)*( &
       -3._8*(3._8-4._8*nu)*(x3-y3)/lr2**5 &
       +30._8*y3*x3*(x3+y3)/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G23d1i

  !---------------------------------------------------------------
  !> function G23d2i
  !! Green's function G23d2i for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,2 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G23d2i(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
          
    G23d2i=1._8/(16._8*PI*(1._8-nu))*( &
       +(3._8-4._8*nu)*(x3-y3)*(lr2**2-3*(x2p-y2)**2)/lr2**5 &
       -6._8*y3*x3*(x3+y3)*(lr2**2-5*(x2p-y2)**2)/lr2**7 &
       +4._8*(1._8-2*nu)*(1._8-nu)/(lr2*(lr2+x3+y3)) &
       -4._8*(1._8-2*nu)*(1._8-nu)*(x2p-y2)**2*(2*lr2+x3+y3)/(lr2**3*(lr2+x3+y3)**2) &
    )

  END FUNCTION G23d2i

  !---------------------------------------------------------------
  !> function G23d3i
  !! Green's function G23d3i for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,3 due to a
  !! point force pointing in the x2 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G23d3i(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
          
    G23d3i=1._8/(16*PI*(1._8-nu))*(x2p-y2)*( &
       +(3._8-4._8*nu)*(lr2**2-3._8*(x3**2-y3**2))/lr2**5 &
       -6._8*y3*(2*x3+y3)/lr2**5 &
       +30._8*y3*x3*(x3+y3)**2/lr2**7 &
       -4._8*(1._8-2*nu)*(1._8-nu)/lr2**3 &
    )

  END FUNCTION G23d3i

  !---------------------------------------------------------------
  !> function G33d1i
  !! Green's function G33d1i for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,1 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G33d1i(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
          
    G33d1i=1._8/(16._8*PI*(1._8-nu))*(x1p-y1)*( &
       -(5._8-12._8*nu+8._8*nu**2)/lr2**3 &
       -30._8*y3*x3*(x3+y3)**2/lr2**7 &
       -3._8*(3-4*nu)*(x3+y3)**2/lr2**5 &
       +6._8*y3*x3/lr2**5 &
    )

  END FUNCTION G33d1i

  !---------------------------------------------------------------
  !> function G33d2i
  !! Green's function G33d2i for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,2 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G33d2i(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)
          
    G33d2i=1._8/(16._8*PI*(1._8-nu))*(x2p-y2)*( &
       -(5._8-12._8*nu+8._8*nu**2)/lr2**3 &
       -30._8*y3*x3*(x3+y3)**2/lr2**7 &
       -3._8*(3-4*nu)*(x3+y3)**2/lr2**5 &
       +6._8*y3*x3/lr2**5 &
    )

  END FUNCTION G33d2i

  !---------------------------------------------------------------
  !> function G33d3i
  !! Green's function G33d3i for an elastic half space in 3d
  !! corresponding to the displacement gradient u3,3 due to a
  !! point force pointing in the x3 direction.
  !---------------------------------------------------------------
  REAL*8 FUNCTION G33d3i(y1,y2,y3)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: y1,y2,y3

    REAL*8:: lr2

    lr2=r2(y1,y2,y3)

    G33d3i=1._8/(16._8*PI*(1._8-nu))*( &
       -(5._8-12._8*nu+8._8*nu**2)*(x3+y3)/lr2**3 &
       +6._8*y3*(x3+y3)**2/lr2**5 &
       +6._8*y3*x3*(x3+y3)*(2*lr2**2-5*(x3+y3)**2)/lr2**7 &
       +(3._8-4._8*nu)*(x3+y3)*(2*lr2**2-3*(x3+y3)**2)/lr2**5 &
       -2*y3*(lr2**2-3*x3*(x3+y3))/lr2**5 &
    )

  END FUNCTION G33d3i

  !---------------------------------------------------------------
  ! function IU11s is the integrand for displacement component u1
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU11s(x,y)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU11s=0._8

    IF (0._8 .NE. m11) THEN
       IU11s=IU11s+m11*T*W/4*(G11d1s(L,x*T/2,(1+y)*W/2+q3)-G11d1s(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU11s=IU11s+m12*T*W/4*(G21d1s(L,x*T/2,(1+y)*W/2+q3)-G21d1s(0._8,x*T/2,(1+y)*W/2+q3)) &
                  +m12*L*W/4*(G11d1s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G11d1s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU11s=IU11s+m13*T*W/4*(G31d1s(L,x*T/2,(1+y)*W/2+q3)-G31d1s(0._8,x*T/2,(1+y)*W/2+q3)) &
                  +m13*L*T/4*(G11d1s((1+x)*L/2,y*T/2,W+q3)-G11d1s((1+x)*L/2,y*T/2,q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU11s=IU11s+m22*L*W/4*(G21d1s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G21d1s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU11s=IU11s+m23*L*T/4*(G21d1s((1+x)*L/2,y*T/2,W+q3)-G21d1s((1+x)*L/2,y*T/2,q3)) &
                  +m23*L*W/4*(G31d1s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G31d1s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU11s=IU11s+m33*L*T/4*(G31d1s((1+x)*L/2,y*T/2,W+q3)-G31d1s((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU11s

  !---------------------------------------------------------------
  ! function IU12s is the integrand for displacement gradient u1,2
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU12s(x,y)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU12s=0._8

    IF (0._8 .NE. m11) THEN
       IU12s=IU12s+m11*T*W/4*(G11d2s(L,x*T/2,(1+y)*W/2+q3)-G11d2s(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU12s=IU12s+m12*T*W/4*(G21d2s(L,x*T/2,(1+y)*W/2+q3)-G21d2s(0._8,x*T/2,(1+y)*W/2+q3)) &
                  +m12*L*W/4*(G11d2s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G11d2s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU12s=IU12s+m13*T*W/4*(G31d2s(L,x*T/2,(1+y)*W/2+q3)-G31d2s(0._8,x*T/2,(1+y)*W/2+q3)) &
                  +m13*L*T/4*(G11d2s((1+x)*L/2,y*T/2,W+q3)-G11d2s((1+x)*L/2,y*T/2,q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU12s=IU12s+m22*L*W/4*(G21d2s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G21d2s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU12s=IU12s+m23*L*T/4*(G21d2s((1+x)*L/2,y*T/2,W+q3)-G21d2s((1+x)*L/2,y*T/2,q3)) &
                  +m23*L*W/4*(G31d2s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G31d2s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU12s=IU12s+m33*L*T/4*(G31d2s((1+x)*L/2,y*T/2,W+q3)-G31d2s((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU12s

  !---------------------------------------------------------------
  ! function IU13s is the integrand for displacement gradient u1,3
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU13s(x,y)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU13s=0._8

    IF (0._8 .NE. m11) THEN
       IU13s=IU13s+m11*T*W/4*(G11d3s(L,x*T/2,(1+y)*W/2+q3)-G11d3s(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU13s=IU13s+m12*T*W/4*(G21d3s(L,x*T/2,(1+y)*W/2+q3)-G21d3s(0._8,x*T/2,(1+y)*W/2+q3)) &
                  +m12*L*W/4*(G11d3s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G11d3s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU13s=IU13s+m13*T*W/4*(G31d3s(L,x*T/2,(1+y)*W/2+q3)-G31d3s(0._8,x*T/2,(1+y)*W/2+q3)) &
                  +m13*L*T/4*(G11d3s((1+x)*L/2,y*T/2,W+q3)-G11d3s((1+x)*L/2,y*T/2,q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU13s=IU13s+m22*L*W/4*(G21d3s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G21d3s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU13s=IU13s+m23*L*T/4*(G21d3s((1+x)*L/2,y*T/2,W+q3)-G21d3s((1+x)*L/2,y*T/2,q3)) &
                  +m23*L*W/4*(G31d3s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G31d3s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU13s=IU13s+m33*L*T/4*(G31d3s((1+x)*L/2,y*T/2,W+q3)-G31d3s((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU13s

  !---------------------------------------------------------------
  ! function IU21s is the integrand for displacement gradient u2,1
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU21s(x,y)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU21s=0._8

    IF (0._8 .NE. m11) THEN
       IU21s=IU21s+m11*T*W/4*(G12d1s(L,x*T/2,(1+y)*W/2+q3)-G12d1s(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU21s=IU21s+m12*T*W/4*(G22d1s(L,x*T/2,(1+y)*W/2+q3)-G22d1s(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m12*L*W/4*(G12d1s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G12d1s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU21s=IU21s+m13*T*W/4*(G32d1s(L,x*T/2,(1+y)*W/2+q3)-G32d1s(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m13*L*T/4*(G12d1s((1+x)*L/2,y*T/2,W+q3)-G12d1s((1+x)*L/2,y*T/2,q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU21s=IU21s+m22*L*W/4*(G22d1s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G22d1s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU21s=IU21s+m23*L*T/4*(G22d1s((1+x)*L/2,y*T/2,W+q3)-G22d1s((1+x)*L/2,y*T/2,q3)) &
                +m23*L*W/4*(G32d1s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G32d1s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU21s=IU21s+m33*L*T/4*(G32d1s((1+x)*L/2,y*T/2,W+q3)-G32d1s((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU21s

  !---------------------------------------------------------------
  ! function IU22s is the integrand for displacement gradient u2,2
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU22s(x,y)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU22s=0._8

    IF (0._8 .NE. m11) THEN
       IU22s=IU22s+m11*T*W/4*(G12d2s(L,x*T/2,(1+y)*W/2+q3)-G12d2s(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU22s=IU22s+m12*T*W/4*(G22d2s(L,x*T/2,(1+y)*W/2+q3)-G22d2s(0._8,x*T/2,(1+y)*W/2+q3)) &
                  +m12*L*W/4*(G12d2s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G12d2s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU22s=IU22s+m13*T*W/4*(G32d2s(L,x*T/2,(1+y)*W/2+q3)-G32d2s(0._8,x*T/2,(1+y)*W/2+q3)) &
                  +m13*L*T/4*(G12d2s((1+x)*L/2,y*T/2,W+q3)-G12d2s((1+x)*L/2,y*T/2,q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU22s=IU22s+m22*L*W/4*(G22d2s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G22d2s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU22s=IU22s+m23*L*T/4*(G22d2s((1+x)*L/2,y*T/2,W+q3)-G22d2s((1+x)*L/2,y*T/2,q3)) &
                  +m23*L*W/4*(G32d2s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G32d2s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU22s=IU22s+m33*L*T/4*(G32d2s((1+x)*L/2,y*T/2,W+q3)-G32d2s((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU22s

  !---------------------------------------------------------------
  ! function IU23s is the integrand for displacement gradient u2,3
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU23s(x,y)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU23s=0._8

    IF (0._8 .NE. m11) THEN
       IU23s=IU23s+m11*T*W/4*(G12d3s(L,x*T/2,(1+y)*W/2+q3)-G12d3s(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU23s=IU23s+m12*T*W/4*(G22d3s(L,x*T/2,(1+y)*W/2+q3)-G22d3s(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m12*L*W/4*(G12d3s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G12d3s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU23s=IU23s+m13*T*W/4*(G32d3s(L,x*T/2,(1+y)*W/2+q3)-G32d3s(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m13*L*T/4*(G12d3s((1+x)*L/2,y*T/2,W+q3)-G12d3s((1+x)*L/2,y*T/2,q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU23s=IU23s+m22*L*W/4*(G22d3s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G22d3s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU23s=IU23s+m23*L*T/4*(G22d3s((1+x)*L/2,y*T/2,W+q3)-G22d3s((1+x)*L/2,y*T/2,q3)) &
                +m23*L*W/4*(G32d3s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G32d3s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU23s=IU23s+m33*L*T/4*(G32d3s((1+x)*L/2,y*T/2,W+q3)-G32d3s((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU23s

  !---------------------------------------------------------------
  ! function IU31s is the integrand for displacement gradient u3,1
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU31s(x,y)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU31s=0._8

    IF (0._8 .NE. m11) THEN
       IU31s=IU31s+m11*T*W/4*(G13d1s(L,x*T/2,(1+y)*W/2+q3)-G13d1s(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU31s=IU31s+m12*T*W/4*(G23d1s(L,x*T/2,(1+y)*W/2+q3)-G23d1s(0._8,x*T/2,(1+y)*W/2+q3)) &
                  +m12*L*W/4*(G13d1s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G13d1s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU31s=IU31s+m13*L*T/4*(G13d1s((1+x)*L/2,y*T/2,W+q3)-G13d1s((1+x)*L/2,y*T/2,q3)) &
                  +m13*T*W/4*(G33d1s(L,x*T/2,(1+y)*W/2+q3)-G33d1s(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU31s=IU31s+m22*L*W/4*(G23d1s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G23d1s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU31s=IU31s+m23*L*T/4*(G23d1s((1+x)*L/2,y*T/2,W+q3)-G23d1s((1+x)*L/2,y*T/2,q3)) &
                  +m23*L*W/4*(G33d1s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G33d1s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU31s=IU31s+m33*L*T/4*(G33d1s((1+x)*L/2,y*T/2,W+q3)-G33d1s((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU31s

  !---------------------------------------------------------------
  ! function IU32s is the integrand for displacement component u3,2
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU32s(x,y)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU32s=0._8

    IF (0._8 .NE. m11) THEN
       IU32s=IU32s+m11*T*W/4*(G13d2s(L,x*T/2,(1+y)*W/2+q3)-G13d2s(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU32s=IU32s+m12*T*W/4*(G23d2s(L,x*T/2,(1+y)*W/2+q3)-G23d2s(0._8,x*T/2,(1+y)*W/2+q3)) &
                  +m12*L*W/4*(G13d2s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G13d2s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU32s=IU32s+m13*L*T/4*(G13d2s((1+x)*L/2,y*T/2,W+q3)-G13d2s((1+x)*L/2,y*T/2,q3)) &
                  +m13*T*W/4*(G33d2s(L,x*T/2,(1+y)*W/2+q3)-G33d2s(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU32s=IU32s+m22*L*W/4*(G23d2s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G23d2s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU32s=IU32s+m23*L*T/4*(G23d2s((1+x)*L/2,y*T/2,W+q3)-G23d2s((1+x)*L/2,y*T/2,q3)) &
                  +m23*L*W/4*(G33d2s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G33d2s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU32s=IU32s+m33*L*T/4*(G33d2s((1+x)*L/2,y*T/2,W+q3)-G33d2s((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU32s

  !---------------------------------------------------------------
  ! function IU33s is the integrand for displacement gradient u3,3
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU33s(x,y)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU33s=0._8

    IF (0._8 .NE. m11) THEN
       IU33s=IU33s+m11*T*W/4*(G13d3s(L,x*T/2,(1+y)*W/2+q3)-G13d3s(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU33s=IU33s+m12*T*W/4*(G23d3s(L,x*T/2,(1+y)*W/2+q3)-G23d3s(0._8,x*T/2,(1+y)*W/2+q3)) &
                  +m12*L*W/4*(G13d3s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G13d3s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU33s=IU33s+m13*L*T/4*(G13d3s((1+x)*L/2,y*T/2,W+q3)-G13d3s((1+x)*L/2,y*T/2,q3)) &
                  +m13*T*W/4*(G33d3s(L,x*T/2,(1+y)*W/2+q3)-G33d3s(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU33s=IU33s+m22*L*W/4*(G23d3s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G23d3s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU33s=IU33s+m23*L*T/4*(G23d3s((1+x)*L/2,y*T/2,W+q3)-G23d3s((1+x)*L/2,y*T/2,q3)) &
                  +m23*L*W/4*(G33d3s((1+x)*L/2,T/2,(1+y)*W/2+q3)-G33d3s((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU33s=IU33s+m33*L*T/4*(G33d3s((1+x)*L/2,y*T/2,W+q3)-G33d3s((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU33s

  !---------------------------------------------------------------
  ! function IU11i is the integrand for displacement component u1
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU11i(x,y)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU11i=0._8

    IF (0._8 .NE. m11) THEN
       IU11i=IU11i+m11*T*W/4*(G11d1i(L,x*T/2,(1+y)*W/2+q3)-G11d1i(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU11i=IU11i+m12*T*W/4*(G21d1i(L,x*T/2,(1+y)*W/2+q3)-G21d1i(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m12*L*W/4*(G11d1i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G11d1i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU11i=IU11i+m13*T*W/4*(G31d1i(L,x*T/2,(1+y)*W/2+q3)-G31d1i(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m13*L*T/4*(G11d1i((1+x)*L/2,y*T/2,W+q3)-G11d1i((1+x)*L/2,y*T/2,q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU11i=IU11i+m22*L*W/4*(G21d1i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G21d1i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU11i=IU11i+m23*L*T/4*(G21d1i((1+x)*L/2,y*T/2,W+q3)-G21d1i((1+x)*L/2,y*T/2,q3)) &
                +m23*L*W/4*(G31d1i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G31d1i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU11i=IU11i+m33*L*T/4*(G31d1i((1+x)*L/2,y*T/2,W+q3)-G31d1i((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU11i

  !---------------------------------------------------------------
  ! function IU12i is the integrand for displacement gradient u1,2
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU12i(x,y)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU12i=0._8

    IF (0._8 .NE. m11) THEN
       IU12i=IU12i+m11*T*W/4*(G11d2i(L,x*T/2,(1+y)*W/2+q3)-G11d2i(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU12i=IU12i+m12*T*W/4*(G21d2i(L,x*T/2,(1+y)*W/2+q3)-G21d2i(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m12*L*W/4*(G11d2i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G11d2i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU12i=IU12i+m13*T*W/4*(G31d2i(L,x*T/2,(1+y)*W/2+q3)-G31d2i(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m13*L*T/4*(G11d2i((1+x)*L/2,y*T/2,W+q3)-G11d2i((1+x)*L/2,y*T/2,q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU12i=IU12i+m22*L*W/4*(G21d2i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G21d2i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU12i=IU12i+m23*L*T/4*(G21d2i((1+x)*L/2,y*T/2,W+q3)-G21d2i((1+x)*L/2,y*T/2,q3)) &
                +m23*L*W/4*(G31d2i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G31d2i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU12i=IU12i+m33*L*T/4*(G31d2i((1+x)*L/2,y*T/2,W+q3)-G31d2i((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU12i

  !---------------------------------------------------------------
  ! function IU13i is the integrand for displacement gradient u1,3
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU13i(x,y)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU13i=0._8

    IF (0._8 .NE. m11) THEN
       IU13i=IU13i+m11*T*W/4*(G11d3i(L,x*T/2,(1+y)*W/2+q3)-G11d3i(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU13i=IU13i+m12*T*W/4*(G21d3i(L,x*T/2,(1+y)*W/2+q3)-G21d3i(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m12*L*W/4*(G11d3i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G11d3i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU13i=IU13i+m13*T*W/4*(G31d3i(L,x*T/2,(1+y)*W/2+q3)-G31d3i(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m13*L*T/4*(G11d3i((1+x)*L/2,y*T/2,W+q3)-G11d3i((1+x)*L/2,y*T/2,q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU13i=IU13i+m22*L*W/4*(G21d3i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G21d3i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU13i=IU13i+m23*L*T/4*(G21d3i((1+x)*L/2,y*T/2,W+q3)-G21d3i((1+x)*L/2,y*T/2,q3)) &
                +m23*L*W/4*(G31d3i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G31d3i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU13i=IU13i+m33*L*T/4*(G31d3i((1+x)*L/2,y*T/2,W+q3)-G31d3i((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU13i

  !---------------------------------------------------------------
  ! function IU21i is the integrand for displacement gradient u2,1
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU21i(x,y)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU21i=0._8

    IF (0._8 .NE. m11) THEN
       IU21i=IU21i+m11*T*W/4*(G12d1i(L,x*T/2,(1+y)*W/2+q3)-G12d1i(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU21i=IU21i+m12*T*W/4*(G22d1i(L,x*T/2,(1+y)*W/2+q3)-G22d1i(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m12*L*W/4*(G12d1i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G12d1i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU21i=IU21i+m13*T*W/4*(G32d1i(L,x*T/2,(1+y)*W/2+q3)-G32d1i(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m13*L*T/4*(G12d1i((1+x)*L/2,y*T/2,W+q3)-G12d1i((1+x)*L/2,y*T/2,q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU21i=IU21i+m22*L*W/4*(G22d1i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G22d1i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU21i=IU21i+m23*L*T/4*(G22d1i((1+x)*L/2,y*T/2,W+q3)-G22d1i((1+x)*L/2,y*T/2,q3)) &
                +m23*L*W/4*(G32d1i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G32d1i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU21i=IU21i+m33*L*T/4*(G32d1i((1+x)*L/2,y*T/2,W+q3)-G32d1i((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU21i

  !---------------------------------------------------------------
  ! function IU22i is the integrand for displacement gradient u2,2
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU22i(x,y)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU22i=0._8

    IF (0._8 .NE. m11) THEN
       IU22i=IU22i+m11*T*W/4*(G12d2i(L,x*T/2,(1+y)*W/2+q3)-G12d2i(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU22i=IU22i+m12*T*W/4*(G22d2i(L,x*T/2,(1+y)*W/2+q3)-G22d2i(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m12*L*W/4*(G12d2i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G12d2i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU22i=IU22i+m13*T*W/4*(G32d2i(L,x*T/2,(1+y)*W/2+q3)-G32d2i(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m13*L*T/4*(G12d2i((1+x)*L/2,y*T/2,W+q3)-G12d2i((1+x)*L/2,y*T/2,q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU22i=IU22i+m22*L*W/4*(G22d2i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G22d2i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU22i=IU22i+m23*L*T/4*(G22d2i((1+x)*L/2,y*T/2,W+q3)-G22d2i((1+x)*L/2,y*T/2,q3)) &
                +m23*L*W/4*(G32d2i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G32d2i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU22i=IU22i+m33*L*T/4*(G32d2i((1+x)*L/2,y*T/2,W+q3)-G32d2i((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU22i

  !---------------------------------------------------------------
  ! function IU23i is the integrand for displacement gradient u2,3
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU23i(x,y)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU23i=0._8

    IF (0._8 .NE. m11) THEN
       IU23i=IU23i+m11*T*W/4*(G12d3i(L,x*T/2,(1+y)*W/2+q3)-G12d3i(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU23i=IU23i+m12*T*W/4*(G22d3i(L,x*T/2,(1+y)*W/2+q3)-G22d3i(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m12*L*W/4*(G12d3i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G12d3i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU23i=IU23i+m13*T*W/4*(G32d3i(L,x*T/2,(1+y)*W/2+q3)-G32d3i(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m13*L*T/4*(G12d3i((1+x)*L/2,y*T/2,W+q3)-G12d3i((1+x)*L/2,y*T/2,q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU23i=IU23i+m22*L*W/4*(G22d3i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G22d3i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU23i=IU23i+m23*L*T/4*(G22d3i((1+x)*L/2,y*T/2,W+q3)-G22d3i((1+x)*L/2,y*T/2,q3)) &
                +m23*L*W/4*(G32d3i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G32d3i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU23i=IU23i+m33*L*T/4*(G32d3i((1+x)*L/2,y*T/2,W+q3)-G32d3i((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU23i

  !---------------------------------------------------------------
  ! function IU31i is the integrand for displacement gradient u3,1
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU31i(x,y)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU31i=0._8

    IF (0._8 .NE. m11) THEN
       IU31i=IU31i+m11*T*W/4*(G13d1i(L,x*T/2,(1+y)*W/2+q3)-G13d1i(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU31i=IU31i+m12*T*W/4*(G23d1i(L,x*T/2,(1+y)*W/2+q3)-G23d1i(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m12*L*W/4*(G13d1i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G13d1i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU31i=IU31i+m13*L*T/4*(G13d1i((1+x)*L/2,y*T/2,W+q3)-G13d1i((1+x)*L/2,y*T/2,q3)) &
                +m13*T*W/4*(G33d1i(L,x*T/2,(1+y)*W/2+q3)-G33d1i(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU31i=IU31i+m22*L*W/4*(G23d1i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G23d1i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU31i=IU31i+m23*L*T/4*(G23d1i((1+x)*L/2,y*T/2,W+q3)-G23d1i((1+x)*L/2,y*T/2,q3)) &
                +m23*L*W/4*(G33d1i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G33d1i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU31i=IU31i+m33*L*T/4*(G33d1i((1+x)*L/2,y*T/2,W+q3)-G33d1i((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU31i

  !---------------------------------------------------------------
  ! function IU32i is the integrand for displacement component u3,2
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU32i(x,y)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU32i=0._8

    IF (0._8 .NE. m11) THEN
       IU32i=IU32i+m11*T*W/4*(G13d2i(L,x*T/2,(1+y)*W/2+q3)-G13d2i(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU32i=IU32i+m12*T*W/4*(G23d2i(L,x*T/2,(1+y)*W/2+q3)-G23d2i(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m12*L*W/4*(G13d2i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G13d2i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU32i=IU32i+m13*L*T/4*(G13d2i((1+x)*L/2,y*T/2,W+q3)-G13d2i((1+x)*L/2,y*T/2,q3)) &
                +m13*T*W/4*(G33d2i(L,x*T/2,(1+y)*W/2+q3)-G33d2i(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU32i=IU32i+m22*L*W/4*(G23d2i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G23d2i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU32i=IU32i+m23*L*T/4*(G23d2i((1+x)*L/2,y*T/2,W+q3)-G23d2i((1+x)*L/2,y*T/2,q3)) &
                +m23*L*W/4*(G33d2i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G33d2i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU32i=IU32i+m33*L*T/4*(G33d2i((1+x)*L/2,y*T/2,W+q3)-G33d2i((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU32i

  !---------------------------------------------------------------
  ! function IU33i is the integrand for displacement gradient u3,3
  !---------------------------------------------------------------
  REAL*8 FUNCTION IU33i(x,y)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: x,y

    ! initialize displacement
    IU33i=0._8

    IF (0._8 .NE. m11) THEN
       IU33i=IU33i+m11*T*W/4*(G13d3i(L,x*T/2,(1+y)*W/2+q3)-G13d3i(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m12) THEN
       IU33i=IU33i+m12*T*W/4*(G23d3i(L,x*T/2,(1+y)*W/2+q3)-G23d3i(0._8,x*T/2,(1+y)*W/2+q3)) &
                +m12*L*W/4*(G13d3i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G13d3i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m13) THEN
       IU33i=IU33i+m13*L*T/4*(G13d3i((1+x)*L/2,y*T/2,W+q3)-G13d3i((1+x)*L/2,y*T/2,q3)) &
                +m13*T*W/4*(G33d3i(L,x*T/2,(1+y)*W/2+q3)-G33d3i(0._8,x*T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m22) THEN
       IU33i=IU33i+m22*L*W/4*(G23d3i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G23d3i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m23) THEN
       IU33i=IU33i+m23*L*T/4*(G23d3i((1+x)*L/2,y*T/2,W+q3)-G23d3i((1+x)*L/2,y*T/2,q3)) &
                +m23*L*W/4*(G33d3i((1+x)*L/2,T/2,(1+y)*W/2+q3)-G33d3i((1+x)*L/2,-T/2,(1+y)*W/2+q3))
    END IF
    IF (0._8 .NE. m33) THEN
       IU33i=IU33i+m33*L*T/4*(G33d3i((1+x)*L/2,y*T/2,W+q3)-G33d3i((1+x)*L/2,y*T/2,q3))
    END IF

  END FUNCTION IU33i

  !------------------------------------------------------------------------
  !> function Omega(x)
  !! evaluates the boxcar function
  !------------------------------------------------------------------------
  REAL*8 FUNCTION omega(x)
    REAL*8, INTENT(IN) :: x

    omega=heaviside(x+0.5_8)-heaviside(x-0.5_8)

  END FUNCTION omega

  !------------------------------------------------------------------------
  !> function S(x)
  !! evalutes the shifted boxcar function
  !------------------------------------------------------------------------
  REAL*8 FUNCTION s(x)
    REAL*8, INTENT(IN) :: x

    s=omega(x-0.5_8)

  END FUNCTION s

  !------------------------------------------------------------------------
  !> function heaviside
  !! computes the Heaviside function
  !------------------------------------------------------------------------
  REAL*8 FUNCTION heaviside(x)
    REAL*8, INTENT(IN) :: x

     IF (0 .LT. x) THEN
        heaviside=1.0_8
     ELSE
        heaviside=0.0_8
     END IF

  END FUNCTION heaviside

END SUBROUTINE computeStressVerticalCuboidSourceImageGauss
