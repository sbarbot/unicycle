
#include "macros.h90"

!------------------------------------------------------------------
!> subroutine ExportVTK_tetrahedron
!! creates a .vtp file (in the VTK PolyData XML format) containing
!! the tetrahedron volume elements.
!!
!! \author Sylvain Barbot 06/07/22 - Tokyo, Japan - original form
!------------------------------------------------------------------
SUBROUTINE exportvtk_tetrahedron(ns,i1,i2,i3,i4,nve,ve,volume,id)
  USE types_3d
  INTEGER, INTENT(IN) :: ns
  INTEGER, DIMENSION(ns), INTENT(IN) :: i1,i2,i3,i4
  INTEGER, INTENT(IN) :: nve
  REAL*8, DIMENSION(3,nve), INTENT(IN) :: ve
  TYPE(VOLUME_ELEMENT_STRUCT), DIMENSION(ns), INTENT(IN) :: volume
  INTEGER, INTENT(IN) :: id

  INTEGER :: k
  CHARACTER :: q

  ! double-quote character
  q=char(34)

  WRITE (id,'("<?xml version=",a,"1.0",a,"?>")') q,q
  WRITE (id,'("<VTKFile type=",a,"PolyData",a," version=",a,"0.1",a,">")') q,q,q,q
  WRITE (id,'("  <PolyData>")')

  DO k=1,ns
     WRITE (id,'("    <Piece NumberOfPoints=",a,"4",a," NumberOfPolys=",a,"4",a,">")') q,q,q,q
     WRITE (id,'("      <Points>")')
     WRITE (id,'("        <DataArray type=",a,"Float32",a, &
                         & " Name=",a,"Points",a, &
                         & " NumberOfComponents=",a,"3",a, &
                         & " format=",a,"ascii",a,">")') q,q,q,q,q,q,q,q
     WRITE (id,'(12ES15.6)') ve(:,i1(k)),ve(:,i2(k)),ve(:,i3(k)),ve(:,i4(k))
     WRITE (id,'("        </DataArray>")')
     WRITE (id,'("      </Points>")')
     WRITE (id,'("      <PointData>")')

     WRITE (id,'("        <DataArray type=",a,"Float32",a, &
                         & " Name=",a,"gammadot0m",a, &
                         & " NumberOfComponents=",a,"1",a, &
                         & " format=",a,"ascii",a," >")') q,q,q,q,q,q,q,q
     WRITE (id,'(4ES15.6)') volume(k)%gammadot0m,volume(k)%gammadot0m,volume(k)%gammadot0m,volume(k)%gammadot0m
     WRITE (id,'("        </DataArray>")')
     WRITE (id,'("        <DataArray type=",a,"Float32",a, &
                         & " Name=",a,"nonlinear Maxwell n",a, &
                         & " NumberOfComponents=",a,"1",a, &
                         & " format=",a,"ascii",a," >")') q,q,q,q,q,q,q,q
     WRITE (id,'(4ES15.6)') volume(k)%npowerm,volume(k)%npowerm,volume(k)%npowerm,volume(k)%npowerm
     WRITE (id,'("        </DataArray>")')
     WRITE (id,'("        <DataArray type=",a,"Float32",a, &
                         & " Name=",a,"e11",a, &
                         & " NumberOfComponents=",a,"1",a, &
                         & " format=",a,"ascii",a,">")') q,q,q,q,q,q,q,q
     WRITE (id,'(4ES15.6)') volume(k)%e11,volume(k)%e11,volume(k)%e11,volume(k)%e11
     WRITE (id,'("        </DataArray>")')
     WRITE (id,'("        <DataArray type=",a,"Float32",a, &
                         & " Name=",a,"e12",a, &
                         & " NumberOfComponents=",a,"1",a, &
                         & " format=",a,"ascii",a,">")') q,q,q,q,q,q,q,q
     WRITE (id,'(4ES15.6)') volume(k)%e12,volume(k)%e12,volume(k)%e12,volume(k)%e12
     WRITE (id,'("        </DataArray>")')
     WRITE (id,'("        <DataArray type=",a,"Float32",a, &
                         & " Name=",a,"e13",a, &
                         & " NumberOfComponents=",a,"1",a, &
                         & " format=",a,"ascii",a,">")') q,q,q,q,q,q,q,q
     WRITE (id,'(4ES15.6)') volume(k)%e13,volume(k)%e13,volume(k)%e13,volume(k)%e13
     WRITE (id,'("        </DataArray>")')
     WRITE (id,'("        <DataArray type=",a,"Float32",a, &
                         & " Name=",a,"e22",a, &
                         & " NumberOfComponents=",a,"1",a, &
                         & " format=",a,"ascii",a,">")') q,q,q,q,q,q,q,q
     WRITE (id,'(4ES15.6)') volume(k)%e22,volume(k)%e22,volume(k)%e22,volume(k)%e22
     WRITE (id,'("        </DataArray>")')
     WRITE (id,'("        <DataArray type=",a,"Float32",a, &
                         & " Name=",a,"e23",a, &
                         & " NumberOfComponents=",a,"1",a, &
                         & " format=",a,"ascii",a,">")') q,q,q,q,q,q,q,q
     WRITE (id,'(4ES15.6)') volume(k)%e23,volume(k)%e23,volume(k)%e23,volume(k)%e23
     WRITE (id,'("        </DataArray>")')
     WRITE (id,'("        <DataArray type=",a,"Float32",a, &
                         & " Name=",a,"e33",a, &
                         & " NumberOfComponents=",a,"1",a, &
                         & " format=",a,"ascii",a,">")') q,q,q,q,q,q,q,q
     WRITE (id,'(4ES15.6)') volume(k)%e33,volume(k)%e33,volume(k)%e33,volume(k)%e33
     WRITE (id,'("        </DataArray>")')

     WRITE (id,'("      </PointData>")')
     WRITE (id,'("      <Polys>")')
     WRITE (id,'("        <DataArray type=",a,"Int32",a, &
                            & " Name=",a,"connectivity",a, &
                            & " format=",a,"ascii",a, &
                            & " RangeMin=",a,"0",a, &
                            & " RangeMax=",a,"7",a,">")') q,q,q,q,q,q,q,q,q,q
     WRITE (id,'("0 1 2 0")')
     WRITE (id,'("0 1 3 0")')
     WRITE (id,'("1 2 3 1")')
     WRITE (id,'("2 0 3 2")')
     WRITE (id,'("        </DataArray>")')
     WRITE (id,'("        <DataArray type=",a,"Int32",a, &
                                 & " Name=",a,"offsets",a, &
                                 & " format=",a,"ascii",a, &
                                 & " RangeMin=",a,"12",a, &
                                 & " RangeMax=",a,"12",a,">")') q,q,q,q,q,q,q,q,q,q
     WRITE (id,'("          4 8 12 16")')
     WRITE (id,'("        </DataArray>")')
     WRITE (id,'("      </Polys>")')
     WRITE (id,'("    </Piece>")')
  END DO

  WRITE (id,'("  </PolyData>")')
  WRITE (id,'("</VTKFile>")')

END SUBROUTINE exportvtk_tetrahedron

!------------------------------------------------------------------
!> subroutine ExportVTK_tetrahedron_field
!! creates a .vtp file (in the VTK PolyData XML format) containing
!! the tetrahedron volume elements.
!!
!! \author Sylvain Barbot 06/07/22 - Tokyo, Japan - original form
!------------------------------------------------------------------
SUBROUTINE exportvtk_tetrahedron_field(ns,i1,i2,i3,i4,nve,ve,id,field)
  USE types_3d
  INTEGER, INTENT(IN) :: ns
  INTEGER, DIMENSION(ns), INTENT(IN) :: i1,i2,i3,i4
  INTEGER, INTENT(IN) :: nve
  REAL*8, DIMENSION(3,nve), INTENT(IN) :: ve
  INTEGER, INTENT(IN) :: id
  REAL*8, DIMENSION(DGF_VOLUME,ns), INTENT(IN) :: field

  REAL*8 :: eps
  INTEGER :: k
  CHARACTER :: q

  ! double-quote character
  q=char(34)

  WRITE (id,'("<?xml version=",a,"1.0",a,"?>")') q,q
  WRITE (id,'("<VTKFile type=",a,"PolyData",a," version=",a,"0.1",a,">")') q,q,q,q
  WRITE (id,'("  <PolyData>")')

  DO k=1,ns
     WRITE (id,'("    <Piece NumberOfPoints=",a,"4",a," NumberOfPolys=",a,"4",a,">")') q,q,q,q
     WRITE (id,'("      <Points>")')
     WRITE (id,'("        <DataArray type=",a,"Float32",a, &
                         & " Name=",a,"Points",a, &
                         & " NumberOfComponents=",a,"3",a, &
                         & " format=",a,"ascii",a,">")') q,q,q,q,q,q,q,q
     WRITE (id,'(12ES15.6)') ve(:,i1(k)),ve(:,i2(k)),ve(:,i3(k)),ve(:,i4(k))
     WRITE (id,'("        </DataArray>")')
     WRITE (id,'("      </Points>")')
     WRITE (id,'("      <PointData>")')

     eps=LOG(SQRT((field(1,k)**2 &
                +2*field(2,k)**2 &
                +2*field(3,k)**2 &
                  +field(4,k)**2 &
                +2*field(5,k)**2 &
                  +field(6,k)**2)/2._8))/LOG(10._8)

     WRITE (id,'("        <DataArray type=",a,"Float32",a, &
                         & " Name=",a,"norm",a, &
                         & " NumberOfComponents=",a,"1",a, &
                         & " format=",a,"ascii",a," >")') q,q,q,q,q,q,q,q

     WRITE (id,'(8ES15.6)') eps,eps,eps,eps, &
                            eps,eps,eps,eps
   
     WRITE (id,'("        </DataArray>")')

     WRITE (id,'("      </PointData>")')
     WRITE (id,'("      <Polys>")')
     WRITE (id,'("        <DataArray type=",a,"Int32",a, &
                            & " Name=",a,"connectivity",a, &
                            & " format=",a,"ascii",a, &
                            & " RangeMin=",a,"0",a, &
                            & " RangeMax=",a,"7",a,">")') q,q,q,q,q,q,q,q,q,q
     WRITE (id,'("0 1 2 0")')
     WRITE (id,'("0 1 3 0")')
     WRITE (id,'("1 2 3 1")')
     WRITE (id,'("2 0 3 2")')
     WRITE (id,'("        </DataArray>")')
     WRITE (id,'("        <DataArray type=",a,"Int32",a, &
                                 & " Name=",a,"offsets",a, &
                                 & " format=",a,"ascii",a, &
                                 & " RangeMin=",a,"12",a, &
                                 & " RangeMax=",a,"12",a,">")') q,q,q,q,q,q,q,q,q,q
     WRITE (id,'("          4 8 12 16")')
     WRITE (id,'("        </DataArray>")')
     WRITE (id,'("      </Polys>")')
     WRITE (id,'("    </Piece>")')
  END DO

  WRITE (id,'("  </PolyData>")')
  WRITE (id,'("</VTKFile>")')

END SUBROUTINE exportvtk_tetrahedron_field

