
#include "macros.h90"

!------------------------------------------------------------------
!> subroutine ExportVTK_TFaults
!! creates a .vtp file (in the VTK PolyData XML format) containing
!! the triangular faults. The faults are characterized with a set
!! of subsegments (triangles) each associated with a slip vector. 
!!
!! \author sylvain barbot 06/24/09 - original form
!------------------------------------------------------------------
SUBROUTINE exportvtk_tfaults_thermobaric(ns,i1,i2,i3,nVe,v,patch,id)
  USE types_3d_thermobaric
  INTEGER, INTENT(IN) :: ns
  INTEGER, DIMENSION(ns), INTENT(IN) :: i1,i2,i3
  INTEGER, INTENT(IN) :: nVe
  REAL*8, DIMENSION(3,nVe), INTENT(IN) :: v
  TYPE(PATCH_ELEMENT_STRUCT), DIMENSION(ns), INTENT(IN) :: patch
  INTEGER, INTENT(IN) :: id

  INTEGER :: k
  CHARACTER :: q

  REAL*8 :: norm
         
  REAL*8, DIMENSION(:,:), ALLOCATABLE :: A,B,C
  REAL*8, DIMENSION(:,:), ALLOCATABLE :: sv,dv,nv

  ! double-quote character
  q=char(34)

  ALLOCATE(A(3,ns),B(3,ns),C(3,ns),STAT=iostatus)
  IF (0 /= iostatus) STOP "could not allocation the vertices"

  ALLOCATE(sv(3,ns),dv(3,ns),nv(3,ns),STAT=iostatus)
  IF (0 /= iostatus) STOP "could not allocation the unit vectors"

  ! vertices
  DO j=1,ns
     A(:,j)=v(:,i1(j))
     B(:,j)=v(:,i2(j))
     C(:,j)=v(:,i3(j))
  END DO

  ! normal vector (B-A) x (C-A)
  DO j=1,ns
     nv(1,j)=(B(2,j)-A(2,j))*(C(3,j)-A(3,j))-(B(3,j)-A(3,j))*(C(2,j)-A(2,j))
     nv(2,j)=(B(3,j)-A(3,j))*(C(1,j)-A(1,j))-(B(1,j)-A(1,j))*(C(3,j)-A(3,j))
     nv(3,j)=(B(1,j)-A(1,j))*(C(2,j)-A(2,j))-(B(2,j)-A(2,j))*(C(1,j)-A(1,j))

     norm=SQRT(nv(1,j)**2+nv(2,j)**2+nv(3,j)**2)
     nv(:,j)=nv(:,j)/norm

     ! choose upward-pointing normal vectors
     IF (0 .LT. nv(3,j)) THEN
        nv(:,j)=-nv(:,j)
     END IF

     ! strike-direction vector
     sv(1,j)= COS(ATAN2(nv(1,j),nv(2,j)))
     sv(2,j)=-SIN(ATAN2(nv(1,j),nv(2,j)))
     sv(3,j)=0._8
                 
     ! dip-direction vector dv = nv x sv
     dv(1,j)=nv(2,j)*sv(3,j)-nv(3,j)*sv(2,j)
     dv(2,j)=nv(3,j)*sv(1,j)-nv(1,j)*sv(3,j)
     dv(3,j)=nv(1,j)*sv(2,j)-nv(2,j)*sv(1,j)

  END DO

  WRITE (id,'("<?xml version=",a,"1.0",a,"?>")') q,q
  WRITE (id,'("<VTKFile type=",a,"PolyData",a," version=",a,"0.1",a,">")') q,q,q,q
  WRITE (id,'("  <PolyData>")')

  DO k=1,ns

     WRITE (id,'("    <Piece NumberOfPoints=",a,"3",a," NumberOfPolys=",a,"1",a,">")') q,q,q,q
     WRITE (id,'("      <Points>")')
     WRITE (id,'("        <DataArray type=",a,"Float32",a, &
                         & " Name=",a,"Fault Patch",a, &
                         & " NumberOfComponents=",a,"3",a, &
                         & " format=",a,"ascii",a,">")') q,q,q,q,q,q,q,q

     ! fault edge coordinates
     WRITE (id,'(9ES12.3)') &
                   A(1,k),A(2,k),A(3,k), &
                   B(1,k),B(2,k),B(3,k), &
                   C(1,k),C(2,k),C(3,k)

     WRITE (id,'("        </DataArray>")')
     WRITE (id,'("      </Points>")')
     WRITE (id,'("      <Polys>")')
     WRITE (id,'("        <DataArray type=",a,"Int32",a, &
                            & " Name=",a,"connectivity",a, &
                            & " format=",a,"ascii",a, &
                            & " RangeMin=",a,"0",a, &
                            & " RangeMax=",a,"2",a,">")') q,q,q,q,q,q,q,q,q,q
     WRITE (id,'("0 1 2")')
     WRITE (id,'("        </DataArray>")')
     WRITE (id,'("        <DataArray type=",a,"Int32",a, &
                                 & " Name=",a,"offsets",a, &
                                 & " format=",a,"ascii",a, &
                                 & " RangeMin=",a,"3",a, &
                                 & " RangeMax=",a,"3",a,">")') q,q,q,q,q,q,q,q,q,q
     WRITE (id,'("          3")')
     WRITE (id,'("        </DataArray>")')
     WRITE (id,'("      </Polys>")')

     WRITE (id,'("      <CellData Normals=",a,"Normals",a,">")') q,q

     WRITE (id,'("        <DataArray type=",a,"Float32",a, &
                               & " Name=",a,"Vl",a, &
                               & " NumberOfComponents=",a,"1",a, &
                               & " format=",a,"ascii",a,">")') q,q,q,q,q,q,q,q
     WRITE (id,'(1ES11.2)') patch(k)%Vl
     WRITE (id,'("        </DataArray>")')

     WRITE (id,'("        <DataArray type=",a,"Float32",a, &
                               & " Name=",a,"rake",a, &
                               & " NumberOfComponents=",a,"1",a, &
                               & " format=",a,"ascii",a,">")') q,q,q,q,q,q,q,q
     WRITE (id,'(1ES11.2)') patch(k)%rake
     WRITE (id,'("        </DataArray>")')

     WRITE (id,'("        <DataArray type=",a,"Float32",a, &
                               & " Name=",a,"sig",a, &
                               & " NumberOfComponents=",a,"1",a, &
                               & " format=",a,"ascii",a,">")') q,q,q,q,q,q,q,q
     WRITE (id,'(1ES11.2)') patch(k)%sig
     WRITE (id,'("        </DataArray>")')
   
     WRITE (id,'("      </CellData>")')

     WRITE (id,'("    </Piece>")')

  END DO

  WRITE (id,'("  </PolyData>")')
  WRITE (id,'("</VTKFile>")')

  DEALLOCATE(A,B,C)
  DEALLOCATE(sv,dv,nv)

END SUBROUTINE exportvtk_tfaults_thermobaric

!------------------------------------------------------------------
!> subroutine ExportVTK_TFaults_field
!! creates a .vtp file (in the VTK PolyData XML format) containing
!! the triangular faults. The faults are characterized with a set
!! of subsegments (triangles) each associated with a slip vector. 
!!
!! \author sylvain barbot 06/24/09 - original form
!------------------------------------------------------------------
SUBROUTINE exportvtk_tfaults_field(ns,i1,i2,i3,nVe,v,id,field)
  INTEGER, INTENT(IN) :: ns
  INTEGER, DIMENSION(ns), INTENT(IN) :: i1,i2,i3
  INTEGER, INTENT(IN) :: nVe
  REAL*8, DIMENSION(3,nVe), INTENT(IN) :: v
  INTEGER, INTENT(IN) :: id
  REAL*8, DIMENSION(DGF_PATCH,ns), INTENT(IN) :: field

  INTEGER :: k
  CHARACTER :: q

  REAL*8 :: norm
         
  REAL*8, DIMENSION(:,:), ALLOCATABLE :: A,B,C
  REAL*8, DIMENSION(:,:), ALLOCATABLE :: sv,dv,nv

  ! double-quote character
  q=char(34)

  ALLOCATE(A(3,ns),B(3,ns),C(3,ns),STAT=iostatus)
  IF (0 /= iostatus) STOP "could not allocation the vertices"

  ALLOCATE(sv(3,ns),dv(3,ns),nv(3,ns),STAT=iostatus)
  IF (0 /= iostatus) STOP "could not allocation the unit vectors"

  ! vertices
  DO j=1,ns
     A(:,j)=v(:,i1(j))
     B(:,j)=v(:,i2(j))
     C(:,j)=v(:,i3(j))
  END DO

  ! normal vector (B-A) x (C-A)
  DO j=1,ns
     nv(1,j)=(B(2,j)-A(2,j))*(C(3,j)-A(3,j))-(B(3,j)-A(3,j))*(C(2,j)-A(2,j))
     nv(2,j)=(B(3,j)-A(3,j))*(C(1,j)-A(1,j))-(B(1,j)-A(1,j))*(C(3,j)-A(3,j))
     nv(3,j)=(B(1,j)-A(1,j))*(C(2,j)-A(2,j))-(B(2,j)-A(2,j))*(C(1,j)-A(1,j))

     norm=SQRT(nv(1,j)**2+nv(2,j)**2+nv(3,j)**2)
     nv(:,j)=nv(:,j)/norm

     ! choose upward-pointing normal vectors
     IF (0 .LT. nv(3,j)) THEN
        nv(:,j)=-nv(:,j)
     END IF

     ! strike-direction vector
     sv(1,j)= COS(ATAN2(nv(1,j),nv(2,j)))
     sv(2,j)=-SIN(ATAN2(nv(1,j),nv(2,j)))
     sv(3,j)=0._8
                 
     ! dip-direction vector dv = nv x sv
     dv(1,j)=nv(2,j)*sv(3,j)-nv(3,j)*sv(2,j)
     dv(2,j)=nv(3,j)*sv(1,j)-nv(1,j)*sv(3,j)
     dv(3,j)=nv(1,j)*sv(2,j)-nv(2,j)*sv(1,j)

  END DO

  WRITE (id,'("<?xml version=",a,"1.0",a,"?>")') q,q
  WRITE (id,'("<VTKFile type=",a,"PolyData",a," version=",a,"0.1",a,">")') q,q,q,q
  WRITE (id,'("  <PolyData>")')

  DO k=1,ns

     WRITE (id,'("    <Piece NumberOfPoints=",a,"3",a," NumberOfPolys=",a,"1",a,">")') q,q,q,q
     WRITE (id,'("      <Points>")')
     WRITE (id,'("        <DataArray type=",a,"Float32",a, &
                         & " Name=",a,"Fault Patch",a, &
                         & " NumberOfComponents=",a,"3",a, &
                         & " format=",a,"ascii",a,">")') q,q,q,q,q,q,q,q

     ! fault edge coordinates
     WRITE (id,'(9ES12.3)') &
                   A(1,k),A(2,k),A(3,k), &
                   B(1,k),B(2,k),B(3,k), &
                   C(1,k),C(2,k),C(3,k)

     WRITE (id,'("        </DataArray>")')
     WRITE (id,'("      </Points>")')
     WRITE (id,'("      <Polys>")')
     WRITE (id,'("        <DataArray type=",a,"Int32",a, &
                            & " Name=",a,"connectivity",a, &
                            & " format=",a,"ascii",a, &
                            & " RangeMin=",a,"0",a, &
                            & " RangeMax=",a,"2",a,">")') q,q,q,q,q,q,q,q,q,q
     WRITE (id,'("0 1 2")')
     WRITE (id,'("        </DataArray>")')
     WRITE (id,'("        <DataArray type=",a,"Int32",a, &
                                 & " Name=",a,"offsets",a, &
                                 & " format=",a,"ascii",a, &
                                 & " RangeMin=",a,"3",a, &
                                 & " RangeMax=",a,"3",a,">")') q,q,q,q,q,q,q,q,q,q
     WRITE (id,'("          3")')
     WRITE (id,'("        </DataArray>")')
     WRITE (id,'("      </Polys>")')

     WRITE (id,'("      <CellData Normals=",a,"Normals",a,">")') q,q

     ! normal of vector
     norm=SQRT(field(1,k)**2+field(2,k)**2)

     WRITE (id,'("        <DataArray type=",a,"Float32",a, &
                               & " Name=",a,"norm",a, &
                               & " NumberOfComponents=",a,"1",a, &
                               & " format=",a,"ascii",a,">")') q,q,q,q,q,q,q,q
     WRITE (id,'(1ES11.2)') norm
     WRITE (id,'("        </DataArray>")')

     WRITE (id,'("      </CellData>")')

     WRITE (id,'("    </Piece>")')

  END DO

  WRITE (id,'("  </PolyData>")')
  WRITE (id,'("</VTKFile>")')

  DEALLOCATE(A,B,C)
  DEALLOCATE(sv,dv,nv)

END SUBROUTINE exportvtk_tfaults_field

