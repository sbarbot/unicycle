
#include "macros.h90"

!------------------------------------------------------------------
!> subroutine ExportXYZ_cuboids
!! creates a GMT-compatible ASCII .xyz file containing
!! the cuboid volume elements.
!!
!! GMT/Unicycle conversion
!!  x /  x2
!!  y /  x1
!!  z / -x3
!!
!! \author Sylvain Barbot 08/08/22 - Shibuya, Japan
!------------------------------------------------------------------
SUBROUTINE exportxyz_cuboids(ns,x,length,thickness,width,strike,dip,volume,id)
  USE types_3d
  INTEGER, INTENT(IN) :: ns
  REAL*8, DIMENSION(3,ns), INTENT(IN) :: x
  REAL*8, DIMENSION(ns), INTENT(IN) :: length
  REAL*8, DIMENSION(ns), INTENT(IN) :: thickness
  REAL*8, DIMENSION(ns), INTENT(IN) :: width
  REAL*8, DIMENSION(ns), INTENT(IN) :: strike
  REAL*8, DIMENSION(ns), INTENT(IN) :: dip
  TYPE(VOLUME_ELEMENT_STRUCT), DIMENSION(ns), INTENT(IN) :: volume
  INTEGER, INTENT(IN) :: id

  INTEGER :: k

  REAL*8 :: x1,x2,x3,cstrike,sstrike,cdip,sdip,L,T,W
  REAL*8, DIMENSION(3) :: sv,dv,nv
  REAL*8, DIMENSION(3) :: A,B,C,D,E,F,G,H
  REAL*8 :: ekk,eps

  DO k=1,ns

     ! volume dimension
     L=length(k)
     W=width(k)
     T=thickness(k)

     ! fault top-corner position
     x1=x(1,k)
     x2=x(2,k)
     x3=x(3,k)

     cstrike=COS(strike(k))
     sstrike=SIN(strike(k))
     cdip=COS(dip(k))
     sdip=SIN(dip(k))
 
     ! strike-slip unit direction
     sv(1)=cstrike
     sv(2)=sstrike
     sv(3)=0._8

     ! dip-slip unit direction
     dv(1)=+sstrike*cdip
     dv(2)=-cstrike*cdip
     dv(3)=-sdip

     ! surface normal vector components
     nv(1)=-sdip*sstrike
     nv(2)=+sdip*cstrike
     nv(3)=-cdip

     A=(/ x2                -nv(2)*T/2.d0,x1                -nv(1)*T/2.d0,-(x3                -nv(3)*T/2.d0) /)
     B=(/ x2        +sv(2)*L-nv(2)*T/2.d0,x1        +sv(1)*L-nv(1)*T/2.d0,-(x3        +sv(3)*L-nv(3)*T/2.d0) /)
     C=(/ x2-dv(2)*W+sv(2)*L-nv(2)*T/2.d0,x1-dv(1)*W+sv(1)*L-nv(1)*T/2.d0,-(x3-dv(3)*W+sv(3)*L-nv(3)*T/2.d0) /)
     D=(/ x2-dv(2)*W        -nv(2)*T/2.d0,x1-dv(1)*W        -nv(1)*T/2.d0,-(x3-dv(3)*W        -nv(3)*T/2.d0) /)

     E=(/ x2                +nv(2)*T/2.d0,x1                +nv(1)*T/2.d0,-(x3                +nv(3)*T/2.d0) /)
     F=(/ x2        +sv(2)*L+nv(2)*T/2.d0,x1        +sv(1)*L+nv(1)*T/2.d0,-(x3        +sv(3)*L+nv(3)*T/2.d0) /)
     G=(/ x2-dv(2)*W+sv(2)*L+nv(2)*T/2.d0,x1-dv(1)*W+sv(1)*L+nv(1)*T/2.d0,-(x3-dv(3)*W+sv(3)*L+nv(3)*T/2.d0) /)
     H=(/ x2-dv(2)*W        +nv(2)*T/2.d0,x1-dv(1)*W        +nv(1)*T/2.d0,-(x3-dv(3)*W        +nv(3)*T/2.d0) /)

     ! isotropic strain-rate
     ekk=volume(k)%e11+volume(k)%e22+volume(k)%e33
     
     ! norm of strain-rate
     eps=SQRT((volume(k)%e11-ekk/3d0)**2 &
             +volume(k)%e12**2 &
             +volume(k)%e13**2 &
             +(volume(k)%e22-ekk/3d0)**2 &
             +volume(k)%e23**2 &
             +(volume(k)%e33-ekk/3d0)**2)

     ! cuboid edge coordinates
     IF (0d0 .LE. volume(k)%ngammadot0m) THEN
        WRITE (id,'("> -Z",ES17.10E3,2ES19.9E3)') volume(k)%ngammadot0m,volume(k)%npowerm,eps
     ELSE
        WRITE (id,'("> -Z",ES18.10E3,2ES19.9E3)') volume(k)%ngammadot0m,volume(k)%npowerm,eps
     END IF
     WRITE (id,'(3ES16.7)') A
     WRITE (id,'(3ES16.7)') B
     WRITE (id,'(3ES16.7)') C
     WRITE (id,'(3ES16.7)') D

     IF (0d0 .LE. volume(k)%ngammadot0m) THEN
        WRITE (id,'("> -Z",ES17.10E3,2ES19.9E3)') volume(k)%ngammadot0m,volume(k)%npowerm,eps
     ELSE
        WRITE (id,'("> -Z",ES18.10E3,2ES19.9E3)') volume(k)%ngammadot0m,volume(k)%npowerm,eps
     END IF
     WRITE (id,'(3ES16.7)') A
     WRITE (id,'(3ES16.7)') B
     WRITE (id,'(3ES16.7)') F
     WRITE (id,'(3ES16.7)') E


     IF (0d0 .LE. volume(k)%ngammadot0m) THEN
        WRITE (id,'("> -Z",ES17.10E3,2ES19.9E3)') volume(k)%ngammadot0m,volume(k)%npowerm,eps
     ELSE
        WRITE (id,'("> -Z",ES18.10E3,2ES19.9E3)') volume(k)%ngammadot0m,volume(k)%npowerm,eps
     END IF
     WRITE (id,'(3ES16.7)') E
     WRITE (id,'(3ES16.7)') F
     WRITE (id,'(3ES16.7)') G
     WRITE (id,'(3ES16.7)') H

     IF (0d0 .LE. volume(k)%ngammadot0m) THEN
        WRITE (id,'("> -Z",ES17.10E3,2ES19.9E3)') volume(k)%ngammadot0m,volume(k)%npowerm,eps
     ELSE
        WRITE (id,'("> -Z",ES18.10E3,2ES19.9E3)') volume(k)%ngammadot0m,volume(k)%npowerm,eps
     END IF
     WRITE (id,'(3ES16.7)') D
     WRITE (id,'(3ES16.7)') C
     WRITE (id,'(3ES16.7)') G
     WRITE (id,'(3ES16.7)') H

     IF (0d0 .LE. volume(k)%ngammadot0m) THEN
        WRITE (id,'("> -Z",ES17.10E3,2ES19.9E3)') volume(k)%ngammadot0m,volume(k)%npowerm,eps
     ELSE
        WRITE (id,'("> -Z",ES18.10E3,2ES19.9E3)') volume(k)%ngammadot0m,volume(k)%npowerm,eps
     END IF
     WRITE (id,'(3ES16.7)') A
     WRITE (id,'(3ES16.7)') E
     WRITE (id,'(3ES16.7)') H
     WRITE (id,'(3ES16.7)') D

     IF (0d0 .LE. volume(k)%ngammadot0m) THEN
        WRITE (id,'("> -Z",ES17.10E3,2ES19.9E3)') volume(k)%ngammadot0m,volume(k)%npowerm,eps
     ELSE
        WRITE (id,'("> -Z",ES18.10E3,2ES19.9E3)') volume(k)%ngammadot0m,volume(k)%npowerm,eps
     END IF
     WRITE (id,'(3ES16.7)') B
     WRITE (id,'(3ES16.7)') F
     WRITE (id,'(3ES16.7)') G
     WRITE (id,'(3ES16.7)') C
  END DO

END SUBROUTINE exportxyz_cuboids

!------------------------------------------------------------------
!> subroutine ExportVTK_cuboids_field
!! creates a .vtp file (in the VTK PolyData XML format) containing
!! the cuboid volume elements.
!!
!! GMT/Unicycle conversion
!!  x /  x2
!!  y /  x1
!!  z / -x3
!!
!! \author Sylvain Barbot 08/08/22 - Yoyogi-Uehara, Tokyo, Japan
!------------------------------------------------------------------
SUBROUTINE exportxyz_cuboids_field(ns,x,length,thickness,width,strike,dip,id,field)
  USE types_3d
  INTEGER, INTENT(IN) :: ns
  REAL*8, DIMENSION(3,ns), INTENT(IN) :: x
  REAL*8, DIMENSION(ns), INTENT(IN) :: length
  REAL*8, DIMENSION(ns), INTENT(IN) :: thickness
  REAL*8, DIMENSION(ns), INTENT(IN) :: width
  REAL*8, DIMENSION(ns), INTENT(IN) :: strike
  REAL*8, DIMENSION(ns), INTENT(IN) :: dip
  INTEGER, INTENT(IN) :: id
  REAL*8, DIMENSION(DGF_VOLUME,ns), INTENT(IN) :: field

  INTEGER :: k

  REAL*8 :: x1,x2,x3,cstrike,sstrike,cdip,sdip,L,T,W,eps
  REAL*8, DIMENSION(3) :: sv,dv,nv
  REAL*8, DIMENSION(3) :: A,B,C,D,E,F,G,H

  DO k=1,ns

     ! volume dimension
     L=length(k)
     W=width(k)
     T=thickness(k)

     ! fault top-corner position
     x1=x(1,k)
     x2=x(2,k)
     x3=x(3,k)

     cstrike=COS(strike(k))
     sstrike=SIN(strike(k))
     cdip=COS(dip(k))
     sdip=SIN(dip(k))
 
     ! strike-slip unit direction
     sv(1)=cstrike
     sv(2)=sstrike
     sv(3)=0._8

     ! dip-slip unit direction
     dv(1)=+sstrike*cdip
     dv(2)=-cstrike*cdip
     dv(3)=-sdip

     ! surface normal vector components
     nv(1)=-sdip*sstrike
     nv(2)=+sdip*cstrike
     nv(3)=-cdip

     A=(/ x2                -nv(2)*T/2.d0,x1                -nv(1)*T/2.d0,-(x3                -nv(3)*T/2.d0) /)
     B=(/ x2        +sv(2)*L-nv(2)*T/2.d0,x1        +sv(1)*L-nv(1)*T/2.d0,-(x3        +sv(3)*L-nv(3)*T/2.d0) /)
     C=(/ x2-dv(2)*W+sv(2)*L-nv(2)*T/2.d0,x1-dv(1)*W+sv(1)*L-nv(1)*T/2.d0,-(x3-dv(3)*W+sv(3)*L-nv(3)*T/2.d0) /)
     D=(/ x2-dv(2)*W        -nv(2)*T/2.d0,x1-dv(1)*W        -nv(1)*T/2.d0,-(x3-dv(3)*W        -nv(3)*T/2.d0) /)

     E=(/ x2                +nv(2)*T/2.d0,x1                +nv(1)*T/2.d0,-(x3                +nv(3)*T/2.d0) /)
     F=(/ x2        +sv(2)*L+nv(2)*T/2.d0,x1        +sv(1)*L+nv(1)*T/2.d0,-(x3        +sv(3)*L+nv(3)*T/2.d0) /)
     G=(/ x2-dv(2)*W+sv(2)*L+nv(2)*T/2.d0,x1-dv(1)*W+sv(1)*L+nv(1)*T/2.d0,-(x3-dv(3)*W+sv(3)*L+nv(3)*T/2.d0) /)
     H=(/ x2-dv(2)*W        +nv(2)*T/2.d0,x1-dv(1)*W        +nv(1)*T/2.d0,-(x3-dv(3)*W        +nv(3)*T/2.d0) /)

     ! norm of tensor
     eps=LOG(SQRT((field(1,k)**2 &
                +2*field(2,k)**2 &
                +2*field(3,k)**2 &
                  +field(4,k)**2 &
                +2*field(5,k)**2 &
                  +field(6,k)**2)/2._8))/LOG(10._8)

     ! fault edge coordinates
     IF (0d0 .LE. eps) THEN
        WRITE (id,'("> -Z",ES17.10E3)') eps
     ELSE
        WRITE (id,'("> -Z",ES18.10E3)') eps
     END IF
     WRITE (id,'(3ES16.7)') A
     WRITE (id,'(3ES16.7)') B
     WRITE (id,'(3ES16.7)') C
     WRITE (id,'(3ES16.7)') D

     IF (0d0 .LE. eps) THEN
        WRITE (id,'("> -Z",ES17.10E3)') eps
     ELSE
        WRITE (id,'("> -Z",ES18.10E3)') eps
     END IF
     WRITE (id,'(3ES16.7)') A
     WRITE (id,'(3ES16.7)') B
     WRITE (id,'(3ES16.7)') F
     WRITE (id,'(3ES16.7)') E

     IF (0d0 .LE. eps) THEN
        WRITE (id,'("> -Z",ES17.10E3)') eps
     ELSE
        WRITE (id,'("> -Z",ES18.10E3)') eps
     END IF
     WRITE (id,'(3ES16.7)') E
     WRITE (id,'(3ES16.7)') F
     WRITE (id,'(3ES16.7)') G
     WRITE (id,'(3ES16.7)') H

     IF (0d0 .LE. eps) THEN
        WRITE (id,'("> -Z",ES17.10E3)') eps
     ELSE
        WRITE (id,'("> -Z",ES18.10E3)') eps
     END IF
     WRITE (id,'(3ES16.7)') D
     WRITE (id,'(3ES16.7)') C
     WRITE (id,'(3ES16.7)') G
     WRITE (id,'(3ES16.7)') H

  END DO

END SUBROUTINE exportxyz_cuboids_field

