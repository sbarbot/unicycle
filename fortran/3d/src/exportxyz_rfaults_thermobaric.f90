
#include "macros.h90"

!------------------------------------------------------------------
!> subroutine ExportXYZ_RFaults_thermobaric
!! creates a GMT-compatible ASCII .xyz file  containing
!! the rectangular faults. The faults are characterized with a set
!! of subsegments (rectangles) each associated with a slip vector. 
!!
!! GMT/Unicycle conversion
!!  x /  x2
!!  y /  x1
!!  z / -x3
!!
!! \author sylvain barbot 08/08/22 - Yoyogi-Uehara, Tokyo, Japan
!------------------------------------------------------------------
SUBROUTINE exportxyz_rfaults_thermobaric(ns,x,length,width,strike,dip,patch,id)
  USE types_3d_thermobaric
  INTEGER, INTENT(IN) :: ns
  REAL*8, DIMENSION(3,ns), INTENT(IN) :: x
  REAL*8, DIMENSION(ns), INTENT(IN) :: length
  REAL*8, DIMENSION(ns), INTENT(IN) :: width
  REAL*8, DIMENSION(ns), INTENT(IN) :: strike
  REAL*8, DIMENSION(ns), INTENT(IN) :: dip
  TYPE(PATCH_ELEMENT_STRUCT), DIMENSION(ns), INTENT(IN) :: patch
  INTEGER, INTENT(IN) :: id

  INTEGER :: k

  REAL*8 :: x1,x2,x3,cstrike,sstrike,cdip,sdip,L,W
  REAL*8, DIMENSION(3) :: s,d,n

  WRITE (id,'("# -ZVl a b L sig for each fault patch")')
  DO k=1,ns

     ! fault dimension
     L=length(k)
     W=width(k)

     ! fault top-corner position
     x1=x(1,k)
     x2=x(2,k)
     x3=x(3,k)

     cstrike=COS(strike(k))
     sstrike=SIN(strike(k))
     cdip=COS(dip(k))
     sdip=SIN(dip(k))
 
     ! strike-slip unit direction
     s(1)=cstrike
     s(2)=sstrike
     s(3)=0._8

     ! dip-slip unit direction
     d(1)=+sstrike*cdip
     d(2)=-cstrike*cdip
     d(3)=-sdip

     ! surface normal vector components
     n(1)=-sdip*sstrike
     n(2)=+sdip*cstrike
     n(3)=-cdip

     ! fault edge coordinates
     IF (0d0 .LE. patch(k)%Vl) THEN
        WRITE (id,'("> -Z",ES17.10E3,2ES17.9E3,I2)') patch(k)%Vl,patch(k)%tau0,patch(k)%sig,patch(k)%rockType
     ELSE
        WRITE (id,'("> -Z",ES18.10E3,2ES17.9E3,I2)') patch(k)%Vl,patch(k)%tau0,patch(k)%sig,patch(k)%rockType
     END IF
     WRITE (id,'(3ES16.8)') x2              ,x1              ,-(x3              )
     WRITE (id,'(3ES16.8)') x2       +s(2)*L,x1       +s(1)*L,-(x3       +s(3)*L)
     WRITE (id,'(3ES16.8)') x2-d(2)*W+s(2)*L,x1-d(1)*W+s(1)*L,-(x3-d(3)*W+s(3)*L)
     WRITE (id,'(3ES16.8)') x2-d(2)*W       ,x1-d(1)*W       ,-(x3-d(3)*W       )

  END DO

END SUBROUTINE exportxyz_rfaults_thermobaric

!------------------------------------------------------------------
!> subroutine ExportXYZ_RFaults_field
!! creates a .vtp file (in the VTK PolyData XML format) containing
!! the rectangular faults. The faults are characterized with a set
!! of subsegments (rectangles) each associated with a slip vector. 
!!
!! GMT/Unicycle conversion
!!  x /  x2
!!  y /  x1
!!  z / -x3
!!
!! \author sylvain barbot 08/08/22 - Yoyogi-Uehara, Tokyo, Japan
!------------------------------------------------------------------
SUBROUTINE exportxyz_rfaults_field(ns,x,length,width,strike,dip,id,field)
  INTEGER, INTENT(IN) :: ns
  REAL*8, DIMENSION(3,ns), INTENT(IN) :: x
  REAL*8, DIMENSION(ns), INTENT(IN) :: length
  REAL*8, DIMENSION(ns), INTENT(IN) :: width
  REAL*8, DIMENSION(ns), INTENT(IN) :: strike
  REAL*8, DIMENSION(ns), INTENT(IN) :: dip
  INTEGER, INTENT(IN) :: id
  REAL*8, DIMENSION(DGF_PATCH,ns), INTENT(IN) :: field

  INTEGER :: k
  CHARACTER :: q

  REAL*8 :: x1,x2,x3,cstrike,sstrike,cdip,sdip,L,W,eps
         
  REAL*8, DIMENSION(3) :: s,d,n

  ! double-quote character
  q=char(34)

  DO k=1,ns

     ! fault dimension
     L=length(k)
     W=width(k)

     ! fault top-corner position
     x1=x(1,k)
     x2=x(2,k)
     x3=x(3,k)

     cstrike=COS(strike(k))
     sstrike=SIN(strike(k))
     cdip=COS(dip(k))
     sdip=SIN(dip(k))
 
     ! strike-slip unit direction
     s(1)=cstrike
     s(2)=sstrike
     s(3)=0._8

     ! dip-slip unit direction
     d(1)=+sstrike*cdip
     d(2)=-cstrike*cdip
     d(3)=-sdip

     ! surface normal vector components
     n(1)=-sdip*sstrike
     n(2)=+sdip*cstrike
     n(3)=-cdip

     ! normal of vector
     eps=LOG(SQRT(field(1,k)**2+field(2,k)**2))/LOG(10._8)
     WRITE (id,'(1ES15.6)') eps

     ! fault edge coordinates
     IF (0d0 .LE. eps) THEN
        WRITE (id,'("> -Z",ES17.10E3)') eps
     ELSE
        WRITE (id,'("> -Z",ES18.10E3)') eps
     END IF
     WRITE (id,'(3ES16.8)') x2              ,x1              ,-(x3              )
     WRITE (id,'(3ES16.8)') x2       +s(2)*L,x1       +s(1)*L,-(x3       +s(3)*L)
     WRITE (id,'(3ES16.8)') x2-d(2)*W+s(2)*L,x1-d(1)*W+s(1)*L,-(x3-d(3)*W+s(3)*L)
     WRITE (id,'(3ES16.8)') x2-d(2)*W       ,x1-d(1)*W       ,-(x3-d(3)*W       )

  END DO

END SUBROUTINE exportxyz_rfaults_field

