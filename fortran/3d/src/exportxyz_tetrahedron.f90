
#include "macros.h90"

!------------------------------------------------------------------
!> subroutine ExportXYZ_tetrahedron
!! creates a GMT-compatible ASCII .xyz file containing
!! the tetrahedron volume elements.
!!
!! GMT/Unicycle conversion
!!  x /  x2
!!  y /  x1
!!  z / -x3
!!
!! \author Sylvain Barbot 08/08/22 - Shibuya, Tokyo, Japan
!------------------------------------------------------------------
SUBROUTINE exportxyz_tetrahedron(ns,i1,i2,i3,i4,nve,ve,volume,id)
  USE types_3d
  INTEGER, INTENT(IN) :: ns
  INTEGER, DIMENSION(ns), INTENT(IN) :: i1,i2,i3,i4
  INTEGER, INTENT(IN) :: nve
  REAL*8, DIMENSION(3,nve), INTENT(IN) :: ve
  TYPE(VOLUME_ELEMENT_STRUCT), DIMENSION(ns), INTENT(IN) :: volume
  INTEGER, INTENT(IN) :: id

  INTEGER :: k
  REAL*8 :: ekk,eps

  DO k=1,ns

     ! isotropic strain-rate
     ekk=volume(k)%e11+volume(k)%e22+volume(k)%e33
     
     ! norm of strain-rate
     eps=SQRT((volume(k)%e11-ekk/3d0)**2 &
             +volume(k)%e12**2 &
             +volume(k)%e13**2 &
             +(volume(k)%e22-ekk/3d0)**2 &
             +volume(k)%e23**2 &
             +(volume(k)%e33-ekk/3d0)**2)

     IF (0d0 .LE. volume(k)%ngammadot0m) THEN
        WRITE (id,'("> -Z",ES17.10E3,2ES19.9E3)') volume(k)%ngammadot0m,volume(k)%npowerm,eps
     ELSE
        WRITE (id,'("> -Z",ES18.10E3,2ES19.9E3)') volume(k)%ngammadot0m,volume(k)%npowerm,eps
     END IF
     WRITE (id,'(3ES16.7)') ve(2,i1(k)),ve(1,i1(k)),-ve(3,i1(k))
     WRITE (id,'(3ES16.7)') ve(2,i2(k)),ve(1,i2(k)),-ve(3,i2(k))
     WRITE (id,'(3ES16.7)') ve(2,i3(k)),ve(1,i3(k)),-ve(3,i3(k))

     IF (0d0 .LE. volume(k)%ngammadot0m) THEN
        WRITE (id,'("> -Z",ES17.10E3,2ES19.9E3)') volume(k)%ngammadot0m,volume(k)%npowerm,eps
     ELSE
        WRITE (id,'("> -Z",ES18.10E3,2ES19.9E3)') volume(k)%ngammadot0m,volume(k)%npowerm,eps
     END IF
     WRITE (id,'(3ES16.7)') ve(2,i1(k)),ve(1,i1(k)),-ve(3,i1(k))
     WRITE (id,'(3ES16.7)') ve(2,i2(k)),ve(1,i2(k)),-ve(3,i2(k))
     WRITE (id,'(3ES16.7)') ve(2,i4(k)),ve(1,i4(k)),-ve(3,i4(k))

     IF (0d0 .LE. volume(k)%ngammadot0m) THEN
        WRITE (id,'("> -Z",ES17.10E3,2ES19.9E3)') volume(k)%ngammadot0m,volume(k)%npowerm,eps
     ELSE
        WRITE (id,'("> -Z",ES18.10E3,2ES19.9E3)') volume(k)%ngammadot0m,volume(k)%npowerm,eps
     END IF
     WRITE (id,'(3ES16.7)') ve(2,i2(k)),ve(1,i2(k)),-ve(3,i2(k))
     WRITE (id,'(3ES16.7)') ve(2,i3(k)),ve(1,i3(k)),-ve(3,i3(k))
     WRITE (id,'(3ES16.7)') ve(2,i4(k)),ve(1,i4(k)),-ve(3,i4(k))

     IF (0d0 .LE. volume(k)%ngammadot0m) THEN
        WRITE (id,'("> -Z",ES17.10E3,2ES19.9E3)') volume(k)%ngammadot0m,volume(k)%npowerm,eps
     ELSE
        WRITE (id,'("> -Z",ES18.10E3,2ES19.9E3)') volume(k)%ngammadot0m,volume(k)%npowerm,eps
     END IF
     WRITE (id,'(3ES16.7)') ve(2,i3(k)),ve(1,i3(k)),-ve(3,i3(k))
     WRITE (id,'(3ES16.7)') ve(2,i1(k)),ve(1,i1(k)),-ve(3,i1(k))
     WRITE (id,'(3ES16.7)') ve(2,i4(k)),ve(1,i4(k)),-ve(3,i4(k))

  END DO

END SUBROUTINE exportxyz_tetrahedron

!------------------------------------------------------------------
!> subroutine ExportVTK_tetrahedron_field
!! creates a .vtp file (in the VTK PolyData XML format) containing
!! the tetrahedron volume elements.
!!
!! GMT/Unicycle conversion
!!  x /  x2
!!  y /  x1
!!  z / -x3
!!
!! \author Sylvain Barbot 06/07/22 - Tokyo, Japan - original form
!------------------------------------------------------------------
SUBROUTINE exportxyz_tetrahedron_field(ns,i1,i2,i3,i4,nve,ve,id,field)
  USE types_3d
  INTEGER, INTENT(IN) :: ns
  INTEGER, DIMENSION(ns), INTENT(IN) :: i1,i2,i3,i4
  INTEGER, INTENT(IN) :: nve
  REAL*8, DIMENSION(3,nve), INTENT(IN) :: ve
  INTEGER, INTENT(IN) :: id
  REAL*8, DIMENSION(DGF_VOLUME,ns), INTENT(IN) :: field

  REAL*8 :: eps
  INTEGER :: k

  DO k=1,ns
     eps=LOG(SQRT((field(1,k)**2 &
                +2*field(2,k)**2 &
                +2*field(3,k)**2 &
                  +field(4,k)**2 &
                +2*field(5,k)**2 &
                  +field(6,k)**2)/2._8))/LOG(10._8)

     IF (0d0 .LE. eps) THEN
        WRITE (id,'("> -Z",ES17.10E3)') eps
     ELSE
        WRITE (id,'("> -Z",ES18.10E3)') eps
     END IF
     WRITE (id,'(3ES16.7)') ve(2,i1(k)),ve(1,i1(k)),-ve(3,i1(k))
     WRITE (id,'(3ES16.7)') ve(2,i2(k)),ve(1,i2(k)),-ve(3,i2(k))
     WRITE (id,'(3ES16.7)') ve(2,i3(k)),ve(1,i3(k)),-ve(3,i3(k))

     IF (0d0 .LE. eps) THEN
        WRITE (id,'("> -Z",ES17.10E3)') eps
     ELSE
        WRITE (id,'("> -Z",ES18.10E3)') eps
     END IF
     WRITE (id,'(3ES16.7)') ve(2,i1(k)),ve(1,i1(k)),-ve(3,i1(k))
     WRITE (id,'(3ES16.7)') ve(2,i2(k)),ve(1,i2(k)),-ve(3,i2(k))
     WRITE (id,'(3ES16.7)') ve(2,i4(k)),ve(1,i4(k)),-ve(3,i4(k))

     IF (0d0 .LE. eps) THEN
        WRITE (id,'("> -Z",ES17.10E3)') eps
     ELSE
        WRITE (id,'("> -Z",ES18.10E3)') eps
     END IF
     WRITE (id,'(3ES16.7)') ve(2,i2(k)),ve(1,i2(k)),-ve(3,i2(k))
     WRITE (id,'(3ES16.7)') ve(2,i3(k)),ve(1,i3(k)),-ve(3,i3(k))
     WRITE (id,'(3ES16.7)') ve(2,i4(k)),ve(1,i4(k)),-ve(3,i4(k))

     IF (0d0 .LE. eps) THEN
        WRITE (id,'("> -Z",ES17.10E3)') eps
     ELSE
        WRITE (id,'("> -Z",ES18.10E3)') eps
     END IF
     WRITE (id,'(3ES16.7)') ve(2,i3(k)),ve(1,i3(k)),-ve(3,i3(k))
     WRITE (id,'(3ES16.7)') ve(2,i1(k)),ve(1,i1(k)),-ve(3,i1(k))
     WRITE (id,'(3ES16.7)') ve(2,i4(k)),ve(1,i4(k)),-ve(3,i4(k))

  END DO

END SUBROUTINE exportxyz_tetrahedron_field

