
#include "macros.h90"

!------------------------------------------------------------------
!> subroutine ExportXYZ_TFaults
!! creates a GMT-compatible ASCII .xyz file containing
!! the triangular faults. The faults are characterized with a set
!! of subsegments (triangles) each associated with a slip vector. 
!!
!! GMT/Unicycle conversion
!!  x /  x2
!!  y /  x1
!!  z / -x3
!!
!! \author sylvain barbot 08/08/22 - Shibuya, Japan
!------------------------------------------------------------------
SUBROUTINE exportxyz_tfaults_thermobaric(ns,i1,i2,i3,nVe,v,patch,id)
  USE types_3d_thermobaric
  INTEGER, INTENT(IN) :: ns
  INTEGER, DIMENSION(ns), INTENT(IN) :: i1,i2,i3
  INTEGER, INTENT(IN) :: nVe
  REAL*8, DIMENSION(3,nVe), INTENT(IN) :: v
  TYPE(PATCH_ELEMENT_STRUCT), DIMENSION(ns), INTENT(IN) :: patch
  INTEGER, INTENT(IN) :: id

  INTEGER :: k

  REAL*8 :: norm
         
  REAL*8, DIMENSION(:,:), ALLOCATABLE :: A,B,C
  REAL*8, DIMENSION(:,:), ALLOCATABLE :: sv,dv,nv

  ALLOCATE(A(3,ns),B(3,ns),C(3,ns),STAT=iostatus)
  IF (0 /= iostatus) STOP "could not allocation the vertices"

  ALLOCATE(sv(3,ns),dv(3,ns),nv(3,ns),STAT=iostatus)
  IF (0 /= iostatus) STOP "could not allocation the unit vectors"

  ! vertices
  DO j=1,ns
     A(:,j)=v(:,i1(j))
     B(:,j)=v(:,i2(j))
     C(:,j)=v(:,i3(j))
  END DO

  ! normal vector (B-A) x (C-A)
  DO j=1,ns
     nv(1,j)=(B(2,j)-A(2,j))*(C(3,j)-A(3,j))-(B(3,j)-A(3,j))*(C(2,j)-A(2,j))
     nv(2,j)=(B(3,j)-A(3,j))*(C(1,j)-A(1,j))-(B(1,j)-A(1,j))*(C(3,j)-A(3,j))
     nv(3,j)=(B(1,j)-A(1,j))*(C(2,j)-A(2,j))-(B(2,j)-A(2,j))*(C(1,j)-A(1,j))

     norm=SQRT(nv(1,j)**2+nv(2,j)**2+nv(3,j)**2)
     nv(:,j)=nv(:,j)/norm

     ! choose upward-pointing normal vectors
     IF (0 .LT. nv(3,j)) THEN
        nv(:,j)=-nv(:,j)
     END IF

     ! strike-direction vector
     sv(1,j)= COS(ATAN2(nv(1,j),nv(2,j)))
     sv(2,j)=-SIN(ATAN2(nv(1,j),nv(2,j)))
     sv(3,j)=0._8
                 
     ! dip-direction vector dv = nv x sv
     dv(1,j)=nv(2,j)*sv(3,j)-nv(3,j)*sv(2,j)
     dv(2,j)=nv(3,j)*sv(1,j)-nv(1,j)*sv(3,j)
     dv(3,j)=nv(1,j)*sv(2,j)-nv(2,j)*sv(1,j)

  END DO

  WRITE (id,'("# -ZVl a b L sig for each fault patch")')
  DO k=1,ns

     IF (0d0 .LE. patch(k)%Vl) THEN
        WRITE (id,'("> -Z",ES17.10E3,2ES19.9E3,I2)') patch(k)%Vl,patch(k)%tau0,patch(k)%sig,patch(k)%rockType
     ELSE
        WRITE (id,'("> -Z",ES18.10E3,2ES19.9E3,I2)') patch(k)%Vl,patch(k)%tau0,patch(k)%sig,patch(k)%rockType
     END IF

     ! fault edge coordinates
     WRITE (id,'(3ES16.7)') A(2,k),A(1,k),-A(3,k)
     WRITE (id,'(3ES16.7)') B(2,k),B(1,k),-B(3,k)
     WRITE (id,'(3ES16.7)') C(2,k),C(1,k),-C(3,k)

  END DO

  DEALLOCATE(A,B,C)
  DEALLOCATE(sv,dv,nv)

END SUBROUTINE exportxyz_tfaults_thermobaric

!------------------------------------------------------------------
!> subroutine ExportXYZ_TFaults_field
!! creates a GMT-compatible ASCII .xyz file containing
!! the triangular faults. The faults are characterized with a set
!! of subsegments (triangles) each associated with a slip vector. 
!!
!! GMT/Unicycle conversion
!!  x /  x2
!!  y /  x1
!!  z / -x3
!!
!! \author sylvain barbot 08/08/22 - Shibuya, Japan
!------------------------------------------------------------------
SUBROUTINE exportxyz_tfaults_field(ns,i1,i2,i3,nVe,v,id,field)
  INTEGER, INTENT(IN) :: ns
  INTEGER, DIMENSION(ns), INTENT(IN) :: i1,i2,i3
  INTEGER, INTENT(IN) :: nVe
  REAL*8, DIMENSION(3,nVe), INTENT(IN) :: v
  INTEGER, INTENT(IN) :: id
  REAL*8, DIMENSION(DGF_PATCH,ns), INTENT(IN) :: field

  INTEGER :: k
  REAL*8 :: norm
         
  REAL*8, DIMENSION(:,:), ALLOCATABLE :: A,B,C
  REAL*8, DIMENSION(:,:), ALLOCATABLE :: sv,dv,nv

  ALLOCATE(A(3,ns),B(3,ns),C(3,ns),STAT=iostatus)
  IF (0 /= iostatus) STOP "could not allocation the vertices"

  ALLOCATE(sv(3,ns),dv(3,ns),nv(3,ns),STAT=iostatus)
  IF (0 /= iostatus) STOP "could not allocation the unit vectors"

  ! vertices
  DO j=1,ns
     A(:,j)=v(:,i1(j))
     B(:,j)=v(:,i2(j))
     C(:,j)=v(:,i3(j))
  END DO

  ! normal vector (B-A) x (C-A)
  DO j=1,ns
     nv(1,j)=(B(2,j)-A(2,j))*(C(3,j)-A(3,j))-(B(3,j)-A(3,j))*(C(2,j)-A(2,j))
     nv(2,j)=(B(3,j)-A(3,j))*(C(1,j)-A(1,j))-(B(1,j)-A(1,j))*(C(3,j)-A(3,j))
     nv(3,j)=(B(1,j)-A(1,j))*(C(2,j)-A(2,j))-(B(2,j)-A(2,j))*(C(1,j)-A(1,j))

     norm=SQRT(nv(1,j)**2+nv(2,j)**2+nv(3,j)**2)
     nv(:,j)=nv(:,j)/norm

     ! choose upward-pointing normal vectors
     IF (0 .LT. nv(3,j)) THEN
        nv(:,j)=-nv(:,j)
     END IF

     ! strike-direction vector
     sv(1,j)= COS(ATAN2(nv(1,j),nv(2,j)))
     sv(2,j)=-SIN(ATAN2(nv(1,j),nv(2,j)))
     sv(3,j)=0._8
                 
     ! dip-direction vector dv = nv x sv
     dv(1,j)=nv(2,j)*sv(3,j)-nv(3,j)*sv(2,j)
     dv(2,j)=nv(3,j)*sv(1,j)-nv(1,j)*sv(3,j)
     dv(3,j)=nv(1,j)*sv(2,j)-nv(2,j)*sv(1,j)

  END DO

  DO k=1,ns

     ! normal of vector
     norm=SQRT(field(1,k)**2+field(2,k)**2)

     IF (0d0 .LE. norm) THEN
        WRITE (id,'("> -Z",ES17.10E3,2ES19.9E3)') norm,field(1,k),field(2,k)
     ELSE
        WRITE (id,'("> -Z",ES18.10E3,2ES19.9E3)') norm,field(1,k),field(2,k)
     END IF

     ! fault edge coordinates
     WRITE (id,'(3ES16.7)') A(2,k),A(1,k),-A(3,k)
     WRITE (id,'(3ES16.7)') B(2,k),B(1,k),-B(3,k)
     WRITE (id,'(3ES16.7)') C(2,k),C(1,k),-C(3,k)

  END DO

  DEALLOCATE(A,B,C)
  DEALLOCATE(sv,dv,nv)

END SUBROUTINE exportxyz_tfaults_field

