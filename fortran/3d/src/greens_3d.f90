!-----------------------------------------------------------------------
! Copyright 2017 Sylvain Barbot
!
! This file is part of UNICYCLE
!
! UNICYCLE is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! UNICYCLE is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with UNICYCLE.  If not, see <http://www.gnu.org/licenses/>.
!-----------------------------------------------------------------------

#include "macros.h90"

MODULE greens_3d

  USE mpi_f08

  IMPLICIT NONE

  PUBLIC

  !------------------------------------------------------------------------
  !! The Green's function for traction and stress interaction amongst 
  !! triangular and rectangular dislocations, and volume elements has the 
  !! following layout
  !!
  !!       / KK KT  KL \
  !!       |           |
  !!   G = | TK TT  TL |
  !!       |           |
  !!       \ LK LT  LL /
  !!
  !! where
  !!
  !!   KK is the matrix for traction on rectangular faults due to rectangular fault slip
  !!   KT is the matrix for traction on rectangular faults due to triangular  fault slip
  !!   TK is the matrix for traction on triangular  faults due to rectangular fault slip
  !!   TT is the matrix for traction on triangular  faults due to triangular  fault slip
  !!
  !!   KL is the matrix for traction on rectangular faults due to strain in volumes
  !!   TL is the matrix for traction on triangular  faults due to strain in volumes
  !!
  !!   LK is the matrix for stress in volumes due to rectangular fault slip
  !!   LT is the matrix for stress in volumes due to triangular  fault slip
  !!   LL is the matrix for stress in volumes due to strain in volumes
  !!
  !! The functions provided in this module compute the matrices KK, KT, KL
  !! TK, TT, TL, LK, LT, LL separately so they can be subsequently combined.
  !------------------------------------------------------------------------


CONTAINS

  !-----------------------------------------------------------------------
  !> subroutine initG
  !! initiate the stress interaction matrix G
  !----------------------------------------------------------------------
  SUBROUTINE initG(in,layout,G)
    USE types_3d
    IMPLICIT NONE
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in
    TYPE(LAYOUT_STRUCT), INTENT(IN) :: layout
    REAL*8, DIMENSION(:,:), ALLOCATABLE, INTENT(OUT) :: G

    INTEGER :: ierr,rank,csize
    INTEGER :: m,n

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

    ! number of columns
    n=in%rectangle%ns*DGF_PATCH &
     +in%triangle%ns*DGF_PATCH &
     +in%cuboid%ns*DGF_VOLUME &
     +in%tetrahedron%ns*DGF_VOLUME

    ! number of rows in current thread
    m=layout%listForceN(1+rank)

    ALLOCATE(G(n,m),STAT=ierr)
    IF (0 /= ierr) STOP "could not allocate the Greens function matrix"

  END SUBROUTINE initG

  !-----------------------------------------------------------------------
  !> subroutine initO
  !! initiate the displacement Green's function O
  !----------------------------------------------------------------------
  SUBROUTINE initO(in,layout,O,Of,Ol)
    USE types_3d
    IMPLICIT NONE
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in
    TYPE(LAYOUT_STRUCT), INTENT(IN) :: layout
    REAL*8, DIMENSION(:,:), ALLOCATABLE, INTENT(OUT) :: O,Of,Ol

    INTEGER :: ierr,rank,csize
    INTEGER :: m,n

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

    ! number of columns in current thread
    n=layout%listVelocityN(rank+1)

    ! number of rows
    m=in%nObservationPoint*DISPLACEMENT_VECTOR_DGF

    ALLOCATE(O(n,m),Of(n,m),Ol(n,m),STAT=ierr)
    IF (0 /= ierr) STOP "could not allocate the displacement kernels"

    ! zero out partial contribution matrices
    O=0._8
    Of=0._8
    Ol=0._8

  END SUBROUTINE initO

  !-----------------------------------------------------------------------
  !> subroutine buildG
  !! Builds the stress interaction matrix G
  !----------------------------------------------------------------------
  SUBROUTINE buildG(in,layout,G)
    USE nikkhoo15
    USE okada92
    USE cuboid
    USE tetrahedron
    USE gauss
    USE types_3d
    IMPLICIT NONE
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in
    TYPE(LAYOUT_STRUCT), INTENT(IN) :: layout
    REAL*8, DIMENSION(:,:), ALLOCATABLE, INTENT(OUT) :: G

    INTEGER :: ierr,rank,csize
    INTEGER :: elementType,elementIndex

    INTEGER :: i,j,k,m,n,p,q,r

    ! traction kernels
    REAL*8, DIMENSION(:,:,:), ALLOCATABLE :: KR,KT,KC,KE

    ! stress kernels
    REAL*8, DIMENSION(:,:,:), ALLOCATABLE :: LR,LT,LC,LE
  
    ! identity matrix
    REAL*8, DIMENSION(6,6), PARAMETER :: &
               eye=RESHAPE( (/ 1._8,0._8,0._8,0._8,0._8,0._8, &
                               0._8,1._8,0._8,0._8,0._8,0._8, &
                               0._8,0._8,1._8,0._8,0._8,0._8, &
                               0._8,0._8,0._8,1._8,0._8,0._8, &
                               0._8,0._8,0._8,0._8,1._8,0._8, &
                               0._8,0._8,0._8,0._8,0._8,1._8 /), (/ 6,6 /))
  
    ! order of integration
    INTEGER, PARAMETER :: order=2

    ! 1d integration points (non-dimensional) and weights
    REAL*8, DIMENSION(order) :: u,w

    ! 2d integration points and weights
    REAL*8, DIMENSION(3,order,order,order) :: x3
    REAL*8, DIMENSION(order,order,order) :: w3

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

    ! number of columns
    n=in%rectangle%ns*DGF_PATCH+ &
      in%triangle%ns*DGF_PATCH+ &
      in%cuboid%ns*DGF_VOLUME+ &
      in%tetrahedron%ns*DGF_VOLUME

    ! number of rows in current thread
    m=layout%listForceN(1+rank)

    ALLOCATE(G(n,m),STAT=ierr)

    ! three types of traction sources
    ALLOCATE(KR(DGF_PATCH,in%rectangle%ns,DGF_VECTOR), &
             KT(DGF_PATCH,in%triangle%ns,DGF_VECTOR), &
             KC(DGF_VOLUME,in%cuboid%ns,DGF_VECTOR), &
             KE(DGF_VOLUME,in%tetrahedron%ns,DGF_VECTOR),STAT=ierr)
    IF (0 /= ierr) STOP "could not allocate the traction kernels"
      
    ! three types of stress sources
    ALLOCATE(LR(DGF_PATCH,in%rectangle%ns,DGF_TENSOR), &
             LT(DGF_PATCH,in%triangle%ns,DGF_TENSOR), &
             LC(DGF_VOLUME,in%cuboid%ns,DGF_TENSOR), &
             LE(DGF_VOLUME,in%tetrahedron%ns,DGF_TENSOR),STAT=ierr)
    IF (0 /= ierr) STOP "could not allocate the kernels"
      
    ! initialize integration coefficients
    CALL gaussRule(order,u,w)

    ! loop over elements owned by current thread
    k=1
    DO i=1,SIZE(layout%elementIndex)
       elementType= layout%elementType(i)
       elementIndex=layout%elementIndex(i)

       SELECT CASE (elementType)
       CASE (FLAG_RECTANGLE)
          ! G(:,k:k+layout%elementForceDGF(i)-1) = Green's function
          CALL tractionRows(in%rectangle%xc(:,elementIndex), &
                         in%rectangle%sv(:,elementIndex), &
                         in%rectangle%dv(:,elementIndex), &
                         in%rectangle%nv(:,elementIndex), &
                         layout%elementForceDGF(i),n,G(:,k))
       CASE (FLAG_TRIANGLE)
          ! G(:,k:k+layout%elementForceDGF(i)-1)= Green's function
          CALL tractionRows(in%triangle%xc(:,elementIndex), &
                         in%triangle%sv(:,elementIndex), &
                         in%triangle%dv(:,elementIndex), &
                         in%triangle%nv(:,elementIndex), &
                         layout%elementForceDGF(i),n,G(:,k))
       CASE (FLAG_CUBOID)
          ! 3d integration points
          x3=RESHAPE((/ ((((in%cuboid%xc(j,elementIndex) &
                           +u(p)*in%cuboid%sv(j,elementIndex)*in%cuboid%length(elementIndex)/2 &
                           +u(q)*in%cuboid%nv(j,elementIndex)*in%cuboid%thickness(elementIndex)/2 &
                           -u(r)*in%cuboid%dv(j,elementIndex)*in%cuboid%width(elementIndex)/2, &
                           j=1,3),p=1,order),q=1,order),r=1,order) /),SHAPE(x3))
          ! 3d integration weights
          w3=RESHAPE((/ (((w(p)*w(q)*w(r)/8._8, p=1,order),q=1,order),r=1,order) /),SHAPE(w3))

          ! G(:,k:k+layout%elementForceDGF(i)-1)= Green's function
          CALL stressRows(order**3,x3,w3, &
                         in%cuboid%sv(:,elementIndex), &
                         in%cuboid%dv(:,elementIndex), &
                         in%cuboid%nv(:,elementIndex), &
                         layout%elementForceDGF(i),n,G(:,k))
       CASE (FLAG_TETRAHEDRON)
          ! 3d integration points
          x3=RESHAPE((/ ((((tmap(u(p),u(q),u(r), &
                  in%tetrahedron%v(j,in%tetrahedron%i1(elementIndex)), &
                  in%tetrahedron%v(j,in%tetrahedron%i2(elementIndex)), &
                  in%tetrahedron%v(j,in%tetrahedron%i3(elementIndex)), &
                  in%tetrahedron%v(j,in%tetrahedron%i4(elementIndex))), &
                           j=1,3),p=1,order),q=1,order),r=1,order) /),SHAPE(x3))
          ! 3d integration weights
          w3=RESHAPE((/ (((w(p)*w(q)*w(r)*(1._8-u(p))*(1._8-u(r))**2*6._8/64._8, p=1,order),q=1,order),r=1,order) /),SHAPE(w3))

          ! G(:,k:k+layout%elementForceDGF(i)-1)= Green's function
          CALL stressRows(order**3,x3,w3, &
                         in%tetrahedron%sv(:,elementIndex), &
                         in%tetrahedron%dv(:,elementIndex), &
                         in%tetrahedron%nv(:,elementIndex), &
                         layout%elementForceDGF(i),n,G(:,k))
       END SELECT

       k=k+layout%elementForceDGF(i)
    END DO

    DEALLOCATE(KR,KT,KC,KE)
    DEALLOCATE(LR,LT,LC,LE)
      
  CONTAINS

    !-----------------------------------------------------------------------
    !> subroutine tractionRows
    !! evaluates all the relevant rows for traction change due to slip (strike
    !! slip and dip slip) in rectangular and triangular patches, and strain
    !! (11, 12, 13, 22, 23, 33) in volume elements.
    !!
    !! \author Sylvain Barbot (03/08/2017)
    !----------------------------------------------------------------------
    SUBROUTINE tractionRows(xc,sv,dv,nv,m,n,rows)
      IMPLICIT NONE
      REAL*8, DIMENSION(3), INTENT(IN) :: xc,sv,dv,nv
      INTEGER, INTENT(IN) :: m,n
      REAL*8, DIMENSION(n*m), INTENT(OUT) :: rows
  
      INTEGER :: j
  
      ! rectangular patches
      DO j=1,DGF_PATCH
         CALL computeTractionKernelsOkada92(xc,sv,dv,nv, &
                 in%rectangle%ns,in%rectangle%x, &
                 in%rectangle%length,in%rectangle%width, &
                 in%rectangle%strike,in%rectangle%dip, &
                 eye(j,1),eye(j,2),eye(j,3),in%mu,in%lambda, &
                 KR(j,:,1),KR(j,:,2),KR(j,:,3))
      END DO
  
      ! triangular patches
      IF (0 .NE. in%triangle%ns) THEN
         DO j=1,DGF_PATCH
            CALL computeTractionKernelsNikkhoo15(xc,sv,dv,nv, &
                    in%triangle%ns,in%triangle%i1,in%triangle%i2,in%triangle%i3, &
                    in%triangle%nVe,in%triangle%v, &
                    eye(j,1),eye(j,2),eye(j,3),in%mu,in%lambda, &
                    KT(j,:,1),KT(j,:,2),KT(j,:,3))
         END DO
      END IF
  
      ! cuboid volume elements
      IF (0 .NE. in%cuboid%ns) THEN
         DO j=1,DGF_VOLUME
            CALL computeTractionKernelsVerticalCuboid(xc,sv,dv,nv, &
                    in%cuboid%ns,in%cuboid%x, &
                    in%cuboid%length,in%cuboid%thickness,in%cuboid%width, &
                    in%cuboid%strike, &
                    eye(j,1),eye(j,2),eye(j,3),eye(j,4),eye(j,5),eye(j,6),in%mu,in%lambda, &
                    KC(j,:,1),KC(j,:,2),KC(j,:,3))
         END DO
      END IF

      ! tetrahedron volume elements
      IF (0 .NE. in%tetrahedron%ns) THEN
         DO j=1,DGF_VOLUME
            CALL computeTractionKernelsTetrahedron(xc,sv,dv,nv, &
                    in%tetrahedron%ns, &
                    in%tetrahedron%i1, &
                    in%tetrahedron%i2, &
                    in%tetrahedron%i3, &
                    in%tetrahedron%i4, &
                    in%tetrahedron%nVe, &
                    in%tetrahedron%v, &
                    eye(j,1),eye(j,2),eye(j,3),eye(j,4),eye(j,5),eye(j,6),in%mu,in%lambda, &
                    KE(j,:,1),KE(j,:,2),KE(j,:,3))
         END DO
      END IF
  
      ! concatenate kernels
      rows=(/ KR(:,:,1),KT(:,:,1),KC(:,:,1),KE(:,:,1), &
              KR(:,:,2),KT(:,:,2),KC(:,:,2),KE(:,:,2), &
              KR(:,:,3),KT(:,:,3),KC(:,:,3),KE(:,:,3) /)
  
    END SUBROUTINE tractionRows
  
    !-----------------------------------------------------------------------
    !> subroutine stressRows
    !! evaluates all the relevant rows for stress change due to slip (strike
    !! slip and dip slip) in rectangular and triangular patches, and strain
    !! (11, 12, 13, 22, 23, 33) in volume elements.
    !!
    !! \author Sylvain Barbot (03/08/2017)
    !----------------------------------------------------------------------
    SUBROUTINE stressRows(p,x,w,sv,dv,nv,m,n,rows)
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: p
      REAL*8, DIMENSION(3,p), INTENT(IN) :: x
      REAL*8, DIMENSION(p), INTENT(IN) :: w
      REAL*8, DIMENSION(3), INTENT(IN) :: sv,dv,nv
      INTEGER, INTENT(IN) :: m,n
      REAL*8, DIMENSION(n*m), INTENT(OUT) :: rows
  
      INTEGER :: j
  
      ! rectangle patches
      DO j=1,DGF_PATCH
         CALL computeStressKernelsOkada92(p,x,w,sv,dv,nv, &
                 in%rectangle%ns,in%rectangle%x, &
                 in%rectangle%length,in%rectangle%width, &
                 in%rectangle%strike,in%rectangle%dip, &
                 eye(j,1),eye(j,2),eye(j,3),in%mu,in%lambda, &
                 LR(j,:,1),LR(j,:,2),LR(j,:,3),LR(j,:,4),LR(j,:,5),LR(j,:,6))
      END DO
  
      ! triangle patches
      DO j=1,DGF_PATCH
         CALL computeStressKernelsNikkhoo15(p,x,w,sv,dv,nv, &
                 in%triangle%ns,in%triangle%i1,in%triangle%i2,in%triangle%i3, &
                 in%triangle%nVe,in%triangle%v, &
                 eye(j,1),eye(j,2),eye(j,3),in%mu,in%lambda, &
                 LT(j,:,1),LT(j,:,2),LT(j,:,3),LT(j,:,4),LT(j,:,5),LT(j,:,6))
      END DO
  
      ! cuboid volume elements
      DO j=1,DGF_VOLUME
         CALL computeStressKernelsVerticalCuboid(p,x,w,sv,dv,nv, &
                 in%cuboid%ns,in%cuboid%x, &
                 in%cuboid%length,in%cuboid%thickness,in%cuboid%width, &
                 in%cuboid%strike, &
                 eye(j,1),eye(j,2),eye(j,3),eye(j,4),eye(j,5),eye(j,6),in%mu,in%lambda, &
                 LC(j,:,1),LC(j,:,2),LC(j,:,3),LC(j,:,4),LC(j,:,5),LC(j,:,6))
      END DO
  
      ! tetrahedron volume elements
      DO j=1,DGF_VOLUME
         CALL computeStressKernelsTetrahedron(p,x,w,sv,dv,nv, &
                 in%tetrahedron%ns, &
                 in%tetrahedron%i1, &
                 in%tetrahedron%i2, &
                 in%tetrahedron%i3, &
                 in%tetrahedron%i4, &
                 in%tetrahedron%nVe, &
                 in%tetrahedron%v, &
                 eye(j,1),eye(j,2),eye(j,3),eye(j,4),eye(j,5),eye(j,6),in%mu,in%lambda, &
                 LE(j,:,1),LE(j,:,2),LE(j,:,3),LE(j,:,4),LE(j,:,5),LE(j,:,6))
      END DO
  
      ! concatenate kernels
      rows=(/ LR(:,:,1),LT(:,:,1),LC(:,:,1),LE(:,:,1), &
              LR(:,:,2),LT(:,:,2),LC(:,:,2),LE(:,:,2), &
              LR(:,:,3),LT(:,:,3),LC(:,:,3),LE(:,:,3), &
              LR(:,:,4),LT(:,:,4),LC(:,:,4),LE(:,:,4), &
              LR(:,:,5),LT(:,:,5),LC(:,:,5),LE(:,:,5), &
              LR(:,:,6),LT(:,:,6),LC(:,:,6),LE(:,:,6) /)
  
    END SUBROUTINE stressRows
  
  END SUBROUTINE buildG
  
  !-----------------------------------------------------------------------
  !> subroutine buildO
  !! Builds the displacement matrix O following the layout below
  !!
  !!
  !!     /         **  \   +----------------------------------------+
  !!     |         **  |   |                                        |
  !!     |         **  |   |   nObservation                         |
  !! O = |         **  |   | (      *       )  * (  varying size  ) |
  !!     |         **  |   |   DISPLACEMENT                         |
  !!     |         **  |   |   _VECTOR_DGF                          |
  !!     |         **  |   |                                        |
  !!     \         **  /   +----------------------------------------+
  !!
  !! where each thread owns a few columns of O, marked schematically ****.
  !! The number of columns depends of the type of element owned by the
  !! current thread.
  !!
  !! The layout was chosen to minimize the MPI communication assuming
  !! that 
  !!
  !!   nObservation*DISPLACEMENT_VECTOR_DGF < (nPatch*dPatch+nVolume*dVolume).
  !!
  !! \author Sylvain Barbot (06/28/2017)
  !----------------------------------------------------------------------
  SUBROUTINE buildO(in,layout,O,Of,Ol)
    USE nikkhoo15
    USE okada92
    USE cuboid
    USE types_3d

    IMPLICIT NONE

    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in
    TYPE(LAYOUT_STRUCT), INTENT(IN) :: layout
    REAL*8, DIMENSION(:,:), ALLOCATABLE, INTENT(OUT) :: O,Of,Ol

    INTEGER :: ierr,rank,csize
    INTEGER :: i,j,k,l,n,m,p
    INTEGER :: elementIndex,elementType
    REAL*8 :: nu

    ! principal strain
    REAL*8, DIMENSION(3) :: sv, dv, nv

    ! principal strain tensor
    REAL*8, DIMENSION(6,6) :: pst

    ! identity matrix
    REAL*8, DIMENSION(6,6), PARAMETER :: &
               eye=RESHAPE( (/ 1._8,0._8,0._8,0._8,0._8,0._8, &
                               0._8,1._8,0._8,0._8,0._8,0._8, &
                               0._8,0._8,1._8,0._8,0._8,0._8, &
                               0._8,0._8,0._8,1._8,0._8,0._8, &
                               0._8,0._8,0._8,0._8,1._8,0._8, &
                               0._8,0._8,0._8,0._8,0._8,1._8 /), (/ 6,6 /))

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

    ! Poisson's ratio
    nu=in%lambda/(in%lambda+in%mu)/2.d0

    ! number of columns in current thread
    n=layout%listVelocityN(rank+1)

    ! number of rows
    m=in%nObservationPoint*DISPLACEMENT_VECTOR_DGF

    ALLOCATE(O(n,m),Of(n,m),Ol(n,m),STAT=ierr)
    IF (0 /= ierr) STOP "could not allocate the displacement kernels"

    ! zero out partial contribution matrices
    Of=0._8
    Ol=0._8

    ! initiate counter for displacement vector
    p=1
    ! loop over observation points
    DO k=1,in%nObservationPoint

       ! initiate counter for source kinematics
       l=1

       ! loop over elements owned by current thread
       DO i=1,SIZE(layout%elementIndex)
          elementType= layout%elementType(i)
          elementIndex=layout%elementIndex(i)

          SELECT CASE (elementType)
          CASE (FLAG_RECTANGLE)

             ! fault patches
             DO j=1,DGF_PATCH
                CALL computeDisplacementOkada92( &
                        in%observationPoint(k)%x(1), &
                        in%observationPoint(k)%x(2), &
                        in%observationPoint(k)%x(3), &
                        in%rectangle%x(1,elementIndex), &
                        in%rectangle%x(2,elementIndex), &
                        in%rectangle%x(3,elementIndex), &
                        in%rectangle%length(elementIndex), &
                        in%rectangle%width(elementIndex), &
                        in%rectangle%strike(elementIndex), &
                        in%rectangle%dip(elementIndex), &
                        eye(j,1),eye(j,2),eye(j,3), &
                        in%mu,in%lambda, &
                        O(l+j-1,p),O(l+j-1,p+1),O(l+j-1,p+2))

                ! contribution from faulting alone
                Of(l+j-1,p  )=O(l+j-1,p  )
                Of(l+j-1,p+1)=O(l+j-1,p+1)
                Of(l+j-1,p+2)=O(l+j-1,p+2)
             END DO

             l=l+DGF_PATCH

          CASE (FLAG_TRIANGLE)

             ! fault patches
             DO j=1,DGF_PATCH
                CALL computeDisplacementNikkhoo15( &
                        in%observationPoint(k)%x(1), &
                        in%observationPoint(k)%x(2), &
                        in%observationPoint(k)%x(3), &
                        in%triangle%v(:,in%triangle%i1(elementIndex)), &
                        in%triangle%v(:,in%triangle%i2(elementIndex)), &
                        in%triangle%v(:,in%triangle%i3(elementIndex)), &
                        eye(j,1),eye(j,2),eye(j,3), &
                        in%mu,in%lambda, &
                        O(l+j-1,p),O(l+j-1,p+1),O(l+j-1,p+2))

                ! contribution from faulting alone
                Of(l+j-1,p  )=O(l+j-1,p  )
                Of(l+j-1,p+1)=O(l+j-1,p+1)
                Of(l+j-1,p+2)=O(l+j-1,p+2)
             END DO

             l=l+DGF_PATCH

          CASE (FLAG_CUBOID)

             ! cuboid volume elements
             DO j=1,DGF_VOLUME
                CALL computeDisplacementVerticalCuboid( &
                        in%observationPoint(k)%x(1), &
                        in%observationPoint(k)%x(2), &
                        in%observationPoint(k)%x(3), &
                        in%cuboid%x(1,elementIndex), &
                        in%cuboid%x(2,elementIndex), &
                        in%cuboid%x(3,elementIndex), &
                        in%cuboid%length(elementIndex), &
                        in%cuboid%thickness(elementIndex), &
                        in%cuboid%width(elementIndex), &
                        in%cuboid%strike(elementIndex), &
                        eye(1,j),eye(2,j),eye(3,j), &
                        eye(4,j),eye(5,j),eye(6,j), &
                        in%mu,nu, &
                        O(l+j-1,p),O(l+j-1,p+1),O(l+j-1,p+2))

                ! contribution from flow alone
                Ol(l+j-1,p  )=O(l+j-1,p  )
                Ol(l+j-1,p+1)=O(l+j-1,p+1)
                Ol(l+j-1,p+2)=O(l+j-1,p+2)
             END DO

             l=l+DGF_VOLUME

          CASE (FLAG_TETRAHEDRON)

             ! principal direction
             sv=in%tetrahedron%sv(:,k)
             nv=in%tetrahedron%nv(:,k)
             dv=in%tetrahedron%dv(:,k)

             ! strain tensor in coordinate system of stress tensor
             pst=RESHAPE( (/ sv(1)*sv(1),               sv(1)*sv(2),               sv(1)*sv(3), sv(2)*sv(2),               sv(2)*sv(3), sv(3)*sv(3), &
                             sv(1)*nv(1), (sv(1)*nv(2)+sv(2)*nv(1)), (sv(1)*nv(3)+sv(3)*nv(1)), sv(2)*nv(2), (sv(3)*nv(2)+sv(2)*nv(3)), sv(3)*nv(3), &
                            -sv(1)*dv(1),-(sv(1)*dv(2)+sv(2)*dv(1)),-(sv(1)*dv(3)+sv(3)*dv(1)),-sv(2)*dv(2),-(sv(3)*dv(2)+sv(2)*dv(3)),-sv(3)*dv(3), &
                             nv(1)*nv(1),               nv(1)*nv(2),               nv(1)*nv(3), nv(2)*nv(2),               nv(2)*nv(3), nv(3)*nv(3), &
                            -nv(1)*dv(1),-(nv(1)*dv(2)+nv(2)*dv(1)),-(nv(1)*dv(3)+nv(3)*dv(1)),-nv(2)*dv(2),-(nv(3)*dv(2)+nv(2)*dv(3)),-nv(3)*dv(3), &
                             dv(1)*dv(1),               dv(1)*dv(2),               dv(1)*dv(3), dv(2)*dv(2),               dv(2)*dv(3), dv(3)*dv(3) &
                          /), (/ 6,6 /))

             ! tetrahedron volume elements
             DO j=1,DGF_VOLUME
                CALL computeDisplacementTetrahedronGauss( &
                        in%observationPoint(k)%x(1), &
                        in%observationPoint(k)%x(2), &
                        in%observationPoint(k)%x(3), &
                        in%tetrahedron%v(:,in%tetrahedron%i1(elementIndex)), &
                        in%tetrahedron%v(:,in%tetrahedron%i2(elementIndex)), &
                        in%tetrahedron%v(:,in%tetrahedron%i3(elementIndex)), &
                        in%tetrahedron%v(:,in%tetrahedron%i4(elementIndex)), &
                        pst(1,j),pst(2,j),pst(3,j), &
                        pst(4,j),pst(5,j),pst(6,j), &
                        nu, &
                        O(l+j-1,p),O(l+j-1,p+1),O(l+j-1,p+2))

                ! contribution from flow alone
                Ol(l+j-1,p  )=O(l+j-1,p  )
                Ol(l+j-1,p+1)=O(l+j-1,p+1)
                Ol(l+j-1,p+2)=O(l+j-1,p+2)
             END DO

             l=l+DGF_VOLUME

          CASE DEFAULT
             WRITE(STDERR,'("wrong case: this is a bug.")')
             WRITE_DEBUG_INFO(-1)
             STOP 2
          END SELECT

       END DO ! elements owned by current thread

       p=p+DISPLACEMENT_VECTOR_DGF

    END DO ! observation points
  
  END SUBROUTINE buildO
  
END MODULE greens_3d

