!> program Unicycle (unified cycle of earthquakes) simulates evolution
!! of fault slip and distributed strain using the integral method under
!! the radiation damping approximation.
!!
!! \mainpage
!! 
!! The Green's function for traction and stress interaction amongst 
!! triangle and rectangle dislocations has the following layout
!!
!! \verbatim
!!
!!       / RR RT \
!!   G = |       |
!!       \ TR TT /
!!
!! \endverbatim
!!
!! where
!!
!!   RR is the matrix for traction on rectangle faults due to rectangle fault slip
!!   RT is the matrix for traction on rectangle faults due to triangle  fault slip
!!   TR is the matrix for traction on triangle  faults due to rectangle fault slip
!!   TT is the matrix for traction on triangle  faults due to triangle  fault slip
!!
!! All faults (rectangle or triangle) have two slip directions:
!! strike slip and dip slip. The interaction matrices become
!!
!! \verbatim
!!
!!        / RRss  RRsd \        / RTss  RRsd \
!!   RR = |            |,  RT = |            |,  
!!        \ RRds  RRdd /        \ RRds  RRdd /
!!
!!        / TRss  TRsd \        / TTss  TTsd \
!!   TR = |            |,  TT = |            |
!!        \ TRds  TRdd /        \ TTds  TTdd /
!!
!! \endverbatim
!!
!! The time evolution is evaluated numerically using the 4th/5th order
!! Runge-Kutta method with adaptive time steps. The state vector is 
!! as follows:
!!
!! \verbatim
!!
!!    / R1 1       \   +-----------------------+  +-----------------+
!!    | .          |   |                       |  |                 |
!!    | R1 dPatch  |   |                       |  |                 |
!!    | .          |   |  nRectangle * dPatch  |  |                 |
!!    | .          |   |                       |  |                 |
!!    | Rn 1       |   |                       |  |                 |
!!    | .          |   |                       |  |                 |
!!    | Rn dPatch  |   +-----------------------+  |                 |
!!    |            |                              | nPatch * dPatch |
!!    | T1 1       |   +-----------------------+  |                 |
!!    | .          |   |                       |  |                 |
!!    | T1 dPatch  |   |                       |  |                 |
!!    | .          |   |  nTriangle * dPatch   |  |                 |
!!    | .          |   |                       |  |                 |
!!    | Tn 1       |   |                       |  |                 |
!!    | .          |   |                       |  |                 |
!!    \ Tn dPatch  /   +-----------------------+  +-----------------+
!!
!! \endverbatim
!!
!! where nRectangle and nTriangle are the number of rectangle and
!! triangle patches, respectively, and dPatch is the degrees of 
!! freedom for patches.
!!
!! For every rectangle or triangle patch, we have the following 
!! items in the state vector
!!
!! \verbatim
!!
!!   /  ss   \  1
!!   |  sd   |  .
!!   |  ts   |  .
!!   |  td   |  .
!!   |  tn   |  .
!!   ! theta*|  .
!!   \  v*   /  dPatch
!!
!! \endverbatim
!!
!! where ts, td, and td are the local traction in the strike, dip, and
!! normal directions, ss and sd are the total slip in the strike and dip 
!! directions, v*=log10(v) is the logarithm of the norm of the velocity,
!! and theta*=log10(theta) is the logarithm of the state variable in the 
!! rate and state friction framework.
!!
!! References:<br>
!!
!!   Barbot S., Slow-slip, slow earthquakes, period-two cycles, full and
!!   partial ruptures, and deterministic chaos in a single asperity fault.
!!   Tectonophysics, 768, p.228171, 2019.
!!
!! \author Sylvain Barbot (2017-2025).
!----------------------------------------------------------------------
PROGRAM ratestate

#include "macros.h90"

#ifndef ODE45
#ifndef ODE23
#define ODE45
#endif
#else
#ifdef ODE23
  ERROR: either ODE45 or ODE23 should be defined, but not both
#endif
#endif

#ifdef NETCDF
  USE exportnetcdf
#endif
  USE greens_3d
  USE rk
  USE mpi_f08
  USE types_3d

  IMPLICIT NONE

  REAL*8, PARAMETER :: DEG2RAD = 0.01745329251994329547437168059786927_8

  ! MPI rank and size
  INTEGER :: rank,csize

  ! error flag
  INTEGER :: ierr

  CHARACTER(512) :: filename
  CHARACTER(512) :: importStateDir

#ifdef NETCDF
  ! check file
  LOGICAL :: fileExist,fileExistAll
#endif

  ! maximum velocity in velocity-weakening region
  REAL*8 :: vMaxWeakening,vMaxWeakeningAll

  ! moment rate
  REAL*8 :: momentRateWeakening,momentRateWeakeningAll

  ! maximum velocity
  REAL*8 :: vMax,vMaxAll

  ! moment rate
  REAL*8 :: momentRate, momentRateAll

  ! scaling factor
  REAL*8, PARAMETER :: lg10=LOG(1.d1)

  ! state vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: y
  ! rate of change of state vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: dydt,yscal
  ! temporary variables
  REAL*8, DIMENSION(:), ALLOCATABLE :: ytmp,ytmp1,ytmp2,ytmp3

  ! slip velocity vector and strain rate tensor array (temporary space)
  REAL*8, DIMENSION(:), ALLOCATABLE :: v

  ! slip velocity vector and strain rate tensor array (temporary space)
  REAL*8, DIMENSION(:), ALLOCATABLE :: vAll

  ! vector array (strike and dip components of slip)
  REAL*4, DIMENSION(:), ALLOCATABLE :: vector,vectorAll

  ! traction vector and stress tensor array (temporary space)
  REAL*8, DIMENSION(:), ALLOCATABLE :: t

  ! displacement and kinematics vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: d,u,dAll

  ! Green's functions
  REAL*8, DIMENSION(:,:), ALLOCATABLE :: G,O,Of,Ol

  ! time
  REAL*8 :: time,t0
  ! time step
  REAL*8 :: dt_try,dt_next,dt_done
  ! time steps
  INTEGER :: i,j

  ! type of friction law (default)
  INTEGER :: frictionLawType=1

  ! type of evolution law (default)
  INTEGER :: evolutionLawType=1

  ! maximum number of time steps (default)
  INTEGER :: maximumIterations=1000000

  ! verbosity
  INTEGER :: verbose=2

  ! rate of NETCDF export (default)
  INTEGER :: exportSlipDistributionRate=-1

  ! layout for parallelism
  TYPE(LAYOUT_STRUCT) :: layout

  ! model parameters
  TYPE(SIMULATION_STRUCT) :: in

  ! initialization
  CALL MPI_INIT(ierr)
  CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
  CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

  ! start time
  time=0.d0

  ! initial tentative time step
  dt_next=1.0d-3

  ! retrieve input parameters from command line
  CALL init(in)
  CALL FLUSH(STDOUT)

  ! export geometrical layout information
  CALL exportGeometry(in)

  IF (in%isdryrun .AND. 0 .EQ. rank) THEN
     PRINT '("dry run: abort calculation")'
  END IF
  IF (in%isdryrun .OR. in%isversion .OR. in%ishelp) THEN
     CALL MPI_FINALIZE(ierr)
     STOP
  END IF

  ! calculate basis vectors
  CALL initGeometry(in)

  ! describe data layout for parallelism
  CALL initParallelism(in,layout)

#ifdef NETCDF
  WRITE (filename,'(a,"/greens-",I4.4,".grd")') TRIM(in%greensFunctionDirectory),rank
  INQUIRE(FILE=filename,EXIST=fileExist)
  CALL MPI_ALLREDUCE(fileExist,fileExistAll,1,MPI_LOGICAL,MPI_LAND,MPI_COMM_WORLD,ierr)
  IF (in%isImportGreens .AND. fileExistAll) THEN
     CALL initG(in,layout,G)
     IF (0 .EQ. rank) THEN
        PRINT '("# import Green''s function.")'
     END IF
     CALL importGreensNetcdf(G)
  ELSE
     ! calculate the stress interaction matrix
     IF (0 .EQ. rank) THEN
        PRINT '("# computing Green''s functions.")'
     END IF
     CALL buildG(in,layout,G)
     IF (in%isExportGreens) THEN
        IF (0 .EQ. rank) THEN
           PRINT '("# exporting Green''s functions to ",a)',TRIM(in%greensFunctionDirectory)
        END IF
        CALL exportGreensNetcdf(G)
     END IF
  END IF
#else
  ! calculate the stress interaction matrix
  IF (0 .EQ. rank) THEN
     PRINT '("# computing Green''s functions.")'
  END IF
  CALL buildG(in,layout,G)
#endif

  CALL buildO(in,layout,O,Of,Ol)
  DEALLOCATE(Of,Ol)

  ! synchronize threads before writing to standard output
  CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)

  IF (0 .EQ. rank) THEN
     PRINT 2000
  END IF

  ! velocity vector and strain rate tensor array (t=G*vAll)
  ALLOCATE(u(layout%listVelocityN(1+rank)), &
           v(layout%listVelocityN(1+rank)), &
           vAll(SUM(layout%listVelocityN)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the velocity and strain rate vector"

  IF (0 .LT. exportSlipDistributionRate) THEN
     ALLOCATE(vector(layout%listVelocityN(1+rank)),STAT=ierr)
     IF (ierr>0) STOP "could not allocate the two-component vector"
     IF (0 .EQ. rank) THEN
        ALLOCATE(vectorAll(SUM(layout%listVelocityN)),STAT=ierr)
        IF (ierr>0) STOP "could not allocate the two-component vector"
     END IF
  END IF

  ! traction vector and stress tensor array (t=G*v)
  ALLOCATE(t(layout%listForceN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the traction and stress vector"

  ! displacement vector (d=O*v)
  ALLOCATE(d   (in%nObservationPoint*DISPLACEMENT_VECTOR_DGF), &
           dAll(in%nObservationPoint*DISPLACEMENT_VECTOR_DGF),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the displacement vector"

  ! state vector
  ALLOCATE(y(layout%listStateN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the state vector"

  ! rate of state vector
  ALLOCATE(dydt(layout%listStateN(1+rank)), &
           yscal(layout%listStateN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the state vectors"

  ALLOCATE(ytmp (layout%listStateN(1+rank)), &
           ytmp1(layout%listStateN(1+rank)), &
           ytmp2(layout%listStateN(1+rank)), &
           ytmp3(layout%listStateN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the RKQS work space"

  ! allocate buffer from rk module
#ifdef ODE45
  ALLOCATE(buffer(layout%listStateN(1+rank),5),SOURCE=0.0d0,STAT=ierr)
#else
  ALLOCATE(buffer(layout%listStateN(1+rank),3),SOURCE=0.0d0,STAT=ierr)
#endif
  IF (ierr>0) STOP "could not allocate the buffer work space"

  IF (0 .EQ. rank) THEN
     in%timeFilename=TRIM(in%wdir)//"/time-weakening.dat"
     OPEN (UNIT=FPTIMEWEAKENING,FILE=in%timeFilename,IOSTAT=ierr,FORM="FORMATTED")
     IF (ierr>0) THEN
        WRITE_DEBUG_INFO(102)
        WRITE (STDERR,'("error: unable to access ",a)') TRIM(in%timefilename)
        STOP 1
     END IF

     in%timeFilename=TRIM(in%wdir)//"/time.dat"
     OPEN (UNIT=FPTIME,FILE=in%timeFilename,IOSTAT=ierr,FORM="FORMATTED")
     IF (ierr>0) THEN
        WRITE_DEBUG_INFO(102)
        WRITE (STDERR,'("error: unable to access ",a)') TRIM(in%timefilename)
        STOP 1
     END IF
  END IF

  ! initialize the y vector
  IF (0 .EQ. rank) THEN
     PRINT '("# initialize state vector.")'
  END IF
  CALL initStateVector(layout%listStateN(1+rank),y,in)
  IF (0 .EQ. rank) THEN
     PRINT 2000
  END IF

#ifdef NETCDF
  ! initialize netcdf output
  IF (in%isExportNetcdf) THEN
    IF (0 .EQ. rank) THEN

       DO i=1,in%nObservationProfiles
          WRITE (in%observationProfileVelocity(i)%filename,'(a,"/log10v.",I3.3,".grd")') TRIM(in%wdir),i
          CALL initNetcdf(in%observationProfileVelocity(i))
       END DO

       IF (in%isExportSlip) THEN
          DO i=1,in%nObservationProfiles
             WRITE (in%observationProfileSlip(i)%filename,'(a,"/slip.",I3.3,".grd")') TRIM(in%wdir),i
             CALL initNetcdf(in%observationProfileSlip(i))
          END DO
       END IF

       IF (in%isExportStress) THEN
          DO i=1,in%nObservationProfiles
             WRITE (in%observationProfileStress(i)%filename,'(a,"/stress.",I3.3,".grd")') TRIM(in%wdir),i
             CALL initNetcdf(in%observationProfileStress(i))
          END DO
       END IF
    END IF
  END IF
#endif

  ! gather maximum velocity in velocity-weakening region
  CALL MPI_REDUCE(vMaxWeakening,vMaxWeakeningAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

  ! gather moment rate in velocity-weakening region
  CALL MPI_REDUCE(momentRateWeakening,momentRateWeakeningAll,1,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

  ! gather maximum velocity
  CALL MPI_REDUCE(vMax,vMaxAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

  ! gather moment rate
  CALL MPI_REDUCE(momentRate,momentRateAll,1,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

  ! initialize output
  IF (0 .EQ. rank) THEN
     WRITE(STDOUT,'("# export to ",a)') TRIM(in%wdir)
     PRINT 2000
     WRITE(STDOUT,'("#       n               time                 dt       vMax")')
     WRITE(STDOUT,'(I9.9,ES19.12E2,ES19.12E2,ES11.4E2)') 0,time,dt_next,vMaxAll
     WRITE(FPTIME,'("#               time                 dt               vMax         Moment-rate")')
     WRITE(FPTIMEWEAKENING,'("# velocity and moment-rate in velocity-weakening region")')
     WRITE(FPTIMEWEAKENING,'("#               time                 dt               vMax         Moment-rate")')
  END IF

  ! initialize observation patch
  DO j=1,in%nObservationState
     IF ((in%observationState(1,j) .GE. layout%listOffset(rank+1)) .AND. &
         (in%observationState(1,j) .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN

        in%observationState(3,j)=100+j
        WRITE (filename,'(a,"/patch-",I8.8,"-",I8.8,".dat")') TRIM(in%wdir),j,in%observationState(1,j)
        OPEN (UNIT=in%observationState(3,j), &
              FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
        IF (ierr>0) THEN
           WRITE_DEBUG_INFO(102)
           WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
           STOP 1
        END IF
        WRITE (in%observationState(3,j), &
              '("#           time (s),", &
              & "     strike slip (m),        dip slip (m),", &
              & "     strike traction,        dip traction,     normal traction,", &
              & "        log10(theta),     log10(velocity),", &
              & "     strike velocity,        dip velocity,", &
              & "d strike traction/dt,   d dip traction/dt,", &
              & "d normal traction/dt,        d theta / dt,    acceleration / v")')
     END IF
  END DO

  ! initialize observation points
  IF (0 .EQ. rank) THEN
     DO j=1,in%nObservationPoint
        in%observationPoint(j)%file=100000+j
        WRITE (filename,'(a,"/opts-",a,".dat")') TRIM(in%wdir),TRIM(in%observationPoint(j)%name)
        OPEN (UNIT=in%observationPoint(j)%file, &
              FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
        IF (ierr>0) THEN
           WRITE_DEBUG_INFO(102)
           WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
           STOP 1
        END IF
        WRITE (in%observationPoint(j)%file,'("#               time                    ", &
                                           & "u1                    u2                    u3")')
     END DO
  END IF

  ! main loop
#ifdef ODE23
  CALL odefun(layout%listStateN(1+rank),time,y,dydt)
#endif
  DO i=1,maximumIterations

#ifdef ODE45
     CALL odefun(layout%listStateN(1+rank),time,y,dydt)
#endif

     CALL export()
     CALL exportPoints()

     IF (0 .LT. exportSlipDistributionRate) THEN
        IF (0 .EQ. MOD(i-1,exportSlipDistributionRate)) THEN
           CALL exportAsciiSlipDistribution()
        END IF
     END IF

#ifdef NETCDF
     IF (in%isExportNetcdf) THEN

        IF (0 .EQ. rank) THEN
           DO j=1,in%nObservationProfiles
              IF (0 .EQ. MOD(i,in%observationProfileVelocity(j)%rate)) THEN
                 CALL exportNetcdfVelocity(in%observationProfileVelocity(j))
              END IF
           END DO
        END IF

        IF (in%isExportSlip) THEN
           DO j=1,in%nObservationProfiles
              IF (0 .EQ. MOD(i,in%observationProfileSlip(j)%rate)) THEN
                 CALL exportNetcdfSlip(in%observationProfileSlip(j))
              END IF
           END DO
        END IF

        IF (in%isExportStress) THEN
           DO j=1,in%nObservationProfiles
              IF (0 .EQ. MOD(i,in%observationProfileStress(j)%rate)) THEN
                 CALL exportNetcdfStress(in%observationProfileStress(j))
              END IF
           END DO
        END IF
     END IF
#endif

#ifdef NETCDF
     IF (0 .EQ. rank) THEN
        IF (in%isExportNetcdf) THEN
           DO j=1,in%nObservationImages
              IF (0 .EQ. MOD(i,in%observationImageVelocity(j)%rate)) THEN
                 CALL exportImageNetcdfVelocity(in%observationImageVelocity(j),j)
                 CALL exportImageNetcdfVelocityStrike(in%observationImageVelocityStrike(j),j)
                 CALL exportImageNetcdfVelocityDip(in%observationImageVelocityDip(j),j)
              END IF
           END DO
        END IF
     END IF
#endif

     dt_try=dt_next
     yscal(:)=abs(y(:))+abs(dt_try*dydt(:))+TINY

     t0=time
#ifdef ODE45
     CALL rungeKutta(layout%listStateN(1+rank),t0,y,dydt, &
               yscal,ytmp1,ytmp2,ytmp3,dt_try,dt_done,dt_next,odefun,timeStep45)
#else
     CALL rungeKutta(layout%listStateN(1+rank),t0,y,dydt, &
               yscal,ytmp1,ytmp2,ytmp3,dt_try,dt_done,dt_next,odefun,timeStep23)
#endif

     time=time+dt_done

     IF (in%isExportState) THEN
        IF (0 .EQ. MOD(i,5000)) THEN
           IF (0.EQ. rank) PRINT '("# exporting state")'
           CALL exportState()
        END IF
     END IF

     ! end calculation
     IF (in%interval .LE. time) THEN
        EXIT
     END IF
   
  END DO

  IF (0 .EQ. rank) THEN
     PRINT '(I9.9," time steps.")', i
  END IF

  IF (0 .EQ. rank) THEN
     CLOSE(FPTIMEWEAKENING)
     CLOSE(FPTIME)
  END IF

  ! close observation state files
  DO j=1,in%nObservationState
     IF ((in%observationState(1,j) .GE. layout%listOffset(rank+1)) .AND. &
         (in%observationState(1,j) .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN
        CLOSE(in%observationState(3,j))
     END IF
  END DO

  ! close the observation points
  IF (0 .EQ. rank) THEN
     DO j=1,in%nObservationPoint
        CLOSE(in%observationPoint(j)%file)
     END DO
  END IF

#ifdef NETCDF
  IF (0 .EQ. rank) THEN
     IF (in%isExportnetcdf) THEN
        DO j=1,in%nObservationProfiles
           CALL closeNetcdfUnlimited(in%observationProfileVelocity(j)%ncid, &
                                     in%observationProfileVelocity(j)%y_varid, &
                                     in%observationProfileVelocity(j)%z_varid, &
                                     in%observationProfileVelocity(j)%ncCount)
        END DO
        IF (in%isExportSlip) THEN
           DO j=1,in%nObservationProfiles
              CALL closeNetcdfUnlimited(in%observationProfileSlip(j)%ncid, &
                                        in%observationProfileSlip(j)%y_varid, &
                                        in%observationProfileSlip(j)%z_varid, &
                                        in%observationProfileSlip(j)%ncCount)
           END DO
        END IF
        IF (in%isExportStress) THEN
           DO j=1,in%nObservationProfiles
              CALL closeNetcdfUnlimited(in%observationProfileStress(j)%ncid, &
                                        in%observationProfileStress(j)%y_varid, &
                                        in%observationProfileStress(j)%z_varid, &
                                        in%observationProfileStress(j)%ncCount)
           END DO
        END IF
     END IF
  END IF
#endif

  DEALLOCATE(y,dydt,yscal)
  DEALLOCATE(ytmp,ytmp1,ytmp2,ytmp3)
  DEALLOCATE(buffer)
  DEALLOCATE(G,v,vAll,t)
#ifdef NETCDF
  IF (ALLOCATED(in%observationProfileVelocity)) DEALLOCATE(in%observationProfileVelocity)
  IF (ALLOCATED(in%observationProfileSlip)) DEALLOCATE(in%observationProfileSlip)
  IF (ALLOCATED(in%observationProfileStress)) DEALLOCATE(in%observationProfileStress)

  IF (ALLOCATED(in%observationImageVelocity)) DEALLOCATE(in%observationImageVelocity)
  IF (ALLOCATED(in%observationImageVelocityStrike)) DEALLOCATE(in%observationImageVelocityStrike)
  IF (ALLOCATED(in%observationImageVelocityDip)) DEALLOCATE(in%observationImageVelocityDip)
#endif
  DEALLOCATE(layout%listForceN)
  DEALLOCATE(layout%listVelocityN,layout%listVelocityOffset)
  DEALLOCATE(layout%listStateN,layout%listStateOffset)
  DEALLOCATE(layout%elementStateIndex)
  DEALLOCATE(layout%listElements,layout%listOffset)
  DEALLOCATE(O,d,u,dAll)
  IF (0 .LT. exportSlipDistributionRate) THEN
     DEALLOCATE(vector)
     IF (0 .EQ. rank) DEALLOCATE(vectorAll)
  END IF

  CALL MPI_FINALIZE(ierr)

2000 FORMAT ("# ----------------------------------------------------------------------------")
     
CONTAINS

  !-----------------------------------------------------------------------
  !> subroutine exportState
  ! export state vector to disk to allow restart
  !----------------------------------------------------------------------
  SUBROUTINE exportState()
    WRITE (filename,'(a,"/state-",I4.4,".ode")') TRIM(in%wdir),rank
    OPEN(FPSTATE,FILE=filename,FORM="unformatted")
    WRITE(FPSTATE) time
    WRITE(FPSTATE) y
    CLOSE(FPSTATE)
  END SUBROUTINE exportState

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportGreensNetcdf
  ! export the Greens function by rank
  !----------------------------------------------------------------------
  SUBROUTINE exportGreensNetcdf(M)
    REAL*8, DIMENSION(:,:), INTENT(IN) :: M

    REAL*8, DIMENSION(:), ALLOCATABLE :: x,y

    INTEGER :: i,ierr
    CHARACTER(LEN=256) :: filename

    ALLOCATE(x(SIZE(M,1)),y(SIZE(M,2)),STAT=ierr)
    IF (ierr/=0) STOP "could not allocate netcdf coordinate for Greens function"

    ! loop over all patch elements
    DO i=1,SIZE(M,1)
       x(i)=REAL(i,8)
    END DO
    DO i=1,SIZE(M,2)
       y(i)=REAL(i,8)
    END DO

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/greens-",I4.4,".grd")') TRIM(in%greensFunctionDirectory),rank
    CALL writeNetcdf(filename,SIZE(M,1),x,SIZE(M,2),y,M,1)

    DEALLOCATE(x,y)

  END SUBROUTINE exportGreensNetcdf
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine importGreensNetcdf
  ! import the Greens function by rank
  !----------------------------------------------------------------------
  SUBROUTINE importGreensNetcdf(M)
    REAL*8, DIMENSION(:,:), INTENT(OUT) :: M

    CHARACTER(LEN=256) :: filename

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/greens-",I4.4,".grd")') TRIM(in%greensFunctionDirectory),rank
    CALL readNetcdf(filename,SIZE(M,1),SIZE(M,2),M)

  END SUBROUTINE importGreensNetcdf
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine initNetcdf
  ! initializes the coordinates of netcdf files
  !----------------------------------------------------------------------
  SUBROUTINE initNetcdf(profile)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: profile

    REAL*8, DIMENSION(:), ALLOCATABLE :: x

    INTEGER :: i,ierr

    ! initialize the number of exports
    profile%ncCount=0

    ALLOCATE(x(profile%n),STAT=ierr)
    IF (ierr/=0) STOP "could not allocate netcdf coordinate"

    ! loop over all patch elements
    DO i=1,profile%n
       x(i)=REAL(i,8)
    END DO

    ! netcdf file is compatible with GMT
    CALL openNetcdfUnlimited( &
             profile%filename, &
             profile%n, &
             x, &
             profile%ncid, &
             profile%y_varid, &
             profile%z_varid)

    DEALLOCATE(x)

  END SUBROUTINE initNetcdf
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportNetcdfVelocity
  ! export time series of log10(v)
  !----------------------------------------------------------------------
  SUBROUTINE exportNetcdfVelocity(profile)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: profile

    REAL*4, DIMENSION(profile%n) :: z

    INTEGER :: j,i
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    ! update the export count
    profile%ncCount=profile%ncCount+1

    ! loop over patch elements by stride
    DO j=1,profile%n
       i=1+profile%offset+(j-1)*profile%stride
       IF (in%rectangle%ns .GE. i) THEN
          patch=in%rectangle%s(i)
       ELSE
          patch=in%triangle%s(i)
       END IF
       ! norm of slip rate vector
       z(j)=REAL(LOG10(SQRT( &
               (patch%Vl*COS(patch%rake)+vAll(1+DGF_PATCH*(i-1)))**2 &
              +(patch%Vl*SIN(patch%rake)+vAll(2+DGF_PATCH*(i-1)))**2 &
               )),4)
    END DO

    CALL writeNetcdfUnlimited(profile%ncid,profile%y_varid,profile%z_varid,profile%ncCount,profile%n,z)

    ! flush every so often
    IF (0 .EQ. MOD(profile%ncCount,50)) THEN
       CALL flushNetcdfUnlimited(profile%ncid,profile%y_varid,profile%ncCount)
    END IF

  END SUBROUTINE exportNetcdfVelocity

  !-----------------------------------------------------------------------
  !> subroutine exportNetcdfSlip
  ! export time series of cumulative slip
  !----------------------------------------------------------------------
  SUBROUTINE exportNetcdfSlip(profile)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: profile

    REAL*4, DIMENSION(profile%n) :: z,zAll

    INTEGER :: j,i,c,l

    ! number of sample per thread and offset per thread
    INTEGER, DIMENSION(csize) :: cAll,oAll

    ! initialize number of profile element in current thread
    c=0

    ! loop over patch elements by stride
    DO j=1,profile%n
       i=1+profile%offset+(j-1)*profile%stride

       ! test whether element is owned by current thread
       IF ((i .GE. layout%listOffset(rank+1)) .AND. &
           (i .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN

          ! state vector index
          l=layout%elementStateIndex(i-layout%listOffset(rank+1))+1

          ! update count for current thread
          c=c+1

          ! norm of cumulative slip vector
          z(c)=REAL(SQRT(y(l+STATE_VECTOR_SLIP_STRIKE)**2+y(l+STATE_VECTOR_SLIP_DIP)**2),4)
       END IF
    END DO

    ! share dimension of all threads among all threads
    CALL MPI_ALLGATHER(c,1,MPI_INTEGER,cAll,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)

    ! thread offset
    oAll(2:csize)=cAll(1:(csize-1))
    oAll(1)=0

    ! master thread gathers data from all threads
    CALL MPI_GATHERV(z,c,MPI_REAL4, &
                     zAll,cAll,oAll,MPI_REAL4, &
                     0,MPI_COMM_WORLD,ierr)

    IF (0 .EQ. rank) THEN
       ! update the export count
       profile%ncCount=profile%ncCount+1

       CALL writeNetcdfUnlimited(profile%ncid,profile%y_varid,profile%z_varid,profile%ncCount,profile%n,zAll)

       ! flush every so often
       IF (0 .EQ. MOD(profile%ncCount,50)) THEN
          CALL flushNetcdfUnlimited(profile%ncid,profile%y_varid,profile%ncCount)
       END IF

    END IF

  END SUBROUTINE exportNetcdfSlip

  !-----------------------------------------------------------------------
  !> subroutine exportNetcdfStress
  ! export time series of instantaneous shear stress
  !----------------------------------------------------------------------
  SUBROUTINE exportNetcdfStress(profile)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: profile

    REAL*4, DIMENSION(profile%n) :: z,zAll

    INTEGER :: j,i,c,l

    ! number of sample per thread and offset per thread
    INTEGER, DIMENSION(csize) :: cAll,oAll

    ! initialize number of profile element in current thread
    c=0

    ! loop over patch elements by stride
    DO j=1,profile%n
       i=1+profile%offset+(j-1)*profile%stride

       ! test whether element is owned by current thread
       IF ((i .GE. layout%listOffset(rank+1)) .AND. &
           (i .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN

          ! state vector index
          l=layout%elementStateIndex(i-layout%listOffset(rank+1))+1

          ! update count for current thread
          c=c+1

          ! norm of cumulative shear traction vector
          z(c)=REAL(SQRT(y(l+STATE_VECTOR_TRACTION_STRIKE)**2+y(l+STATE_VECTOR_TRACTION_DIP)**2),4)
       END IF
    END DO

    ! share dimension of all threads among all threads
    CALL MPI_ALLGATHER(c,1,MPI_INTEGER,cAll,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)

    ! thread offset
    oAll(2:csize)=cAll(1:(csize-1))
    oAll(1)=0

    ! master thread gathers data from all threads
    CALL MPI_GATHERV(z,c,MPI_REAL4, &
                     zAll,cAll,oAll,MPI_REAL4, &
                     0,MPI_COMM_WORLD,ierr)

    IF (0 .EQ. rank) THEN
       ! update the export count
       profile%ncCount=profile%ncCount+1

       CALL writeNetcdfUnlimited(profile%ncid,profile%y_varid,profile%z_varid,profile%ncCount,profile%n,zAll)

       ! flush every so often
       IF (0 .EQ. MOD(profile%ncCount,50)) THEN
          CALL flushNetcdfUnlimited(profile%ncid,profile%y_varid,profile%ncCount)
       END IF

    END IF

  END SUBROUTINE exportNetcdfStress
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportImageNetcdfVelocity
  ! export an image of patches by strides
  !----------------------------------------------------------------------
  SUBROUTINE exportImageNetcdfVelocity(image,number)
    TYPE(IMAGE_STRUCT), INTENT(IN) :: image
    INTEGER, INTENT(IN) :: number

    REAL*8, DIMENSION(image%nl) :: x
    REAL*8, DIMENSION(image%nw) :: y
    REAL*8, DIMENSION(image%nl,image%nw) :: z

    INTEGER :: j,k,index
    CHARACTER(LEN=256) :: filename
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    ! loop over all patch elements
    DO j=1,image%nl
       x(j)=REAL(j,8)
    END DO
    DO k=1,image%nw
       y(k)=REAL(k,8)
    END DO

    ! loop over patch elements by stride
    DO k=1,image%nw
       DO j=1,image%nl
          index=1+image%offset+(j-1)*image%stride+(k-1)*image%stride*image%nl
          IF (in%rectangle%ns .GE. index) THEN
             patch=in%rectangle%s(index)
          ELSE
             patch=in%triangle%s(index)
          END IF
          ! norm of slip rate vector
          z(j,k)=LOG10(SQRT( &
                  (patch%Vl*COS(patch%rake)+vAll(1+DGF_PATCH*(index-1)))**2 &
                 +(patch%Vl*SIN(patch%rake)+vAll(2+DGF_PATCH*(index-1)))**2 &
                  ))
       END DO
    END DO

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/image-",I2.2,"-",I6.6,"-log10v.grd")') TRIM(in%wdir),number,i
    CALL writeNetcdf(filename,image%nl,x,image%nw,y,z,1)

  END SUBROUTINE exportImageNetcdfVelocity

  !-----------------------------------------------------------------------
  !> subroutine exportImageNetcdfVelocityStrike
  ! export an image of patches by strides
  !----------------------------------------------------------------------
  SUBROUTINE exportImageNetcdfVelocityStrike(image,number)
    TYPE(IMAGE_STRUCT), INTENT(IN) :: image
    INTEGER, INTENT(IN) :: number

    REAL*8, DIMENSION(image%nl) :: x
    REAL*8, DIMENSION(image%nw) :: y
    REAL*8, DIMENSION(image%nl,image%nw) :: z

    INTEGER :: j,k,index
    CHARACTER(LEN=256) :: filename
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    ! loop over all patch elements
    DO j=1,image%nl
       x(j)=REAL(j,8)
    END DO
    DO k=1,image%nw
       y(k)=REAL(k,8)
    END DO

    ! loop over patch elements by stride
    DO k=1,image%nw
       DO j=1,image%nl
          index=1+image%offset+(j-1)*image%stride+(k-1)*image%stride*image%nl
          IF (in%rectangle%ns .GE. index) THEN
             patch=in%rectangle%s(index)
          ELSE
             patch=in%triangle%s(index)
          END IF
          ! strike-parallel component of slip distribution
          z(j,k)=patch%Vl*COS(patch%rake)+vAll(1+DGF_PATCH*(index-1))
       END DO
    END DO

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/image-",I2.2,"-",I6.6,"-v-strike.grd")') TRIM(in%wdir),number,i
    CALL writeNetcdf(filename,image%nl,x,image%nw,y,z,1)

  END SUBROUTINE exportImageNetcdfVelocityStrike

  !-----------------------------------------------------------------------
  !> subroutine exportImageNetcdfVelocityDip
  ! export an image of patches by strides
  !----------------------------------------------------------------------
  SUBROUTINE exportImageNetcdfVelocityDip(image,number)
    TYPE(IMAGE_STRUCT), INTENT(IN) :: image
    INTEGER, INTENT(IN) :: number

    REAL*8, DIMENSION(image%nl) :: x
    REAL*8, DIMENSION(image%nw) :: y
    REAL*8, DIMENSION(image%nl,image%nw) :: z

    INTEGER :: j,k,index
    CHARACTER(LEN=256) :: filename
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    ! loop over all patch elements
    DO j=1,image%nl
       x(j)=REAL(j,8)
    END DO
    DO k=1,image%nw
       y(k)=REAL(k,8)
    END DO

    ! loop over patch elements by stride
    DO k=1,image%nw
       DO j=1,image%nl
          index=1+image%offset+(j-1)*image%stride+(k-1)*image%stride*image%nl
          IF (in%rectangle%ns .GE. index) THEN
             patch=in%rectangle%s(index)
          ELSE
             patch=in%triangle%s(index)
          END IF
          ! dip-parallel component of slip distribution
          z(j,k)=patch%Vl*SIN(patch%rake)+vAll(2+DGF_PATCH*(index-1))
       END DO
    END DO

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/image-",I2.2,"-",I6.6,"-v-dip.grd")') TRIM(in%wdir),number,i
    CALL writeNetcdf(filename,image%nl,x,image%nw,y,z,1)

  END SUBROUTINE exportImageNetcdfVelocityDip
#endif

  !-----------------------------------------------------------------------
  !> subroutine exportAsciiSlipDistribution
  ! export cumulative fault slip to ASCII file
  !----------------------------------------------------------------------
  SUBROUTINE exportAsciiSlipDistribution()

    INTEGER :: k,l

    ! loop over elements owned by current thread
    DO k=1,SIZE(layout%elementIndex)
       ! element index in state vector
       l=layout%elementStateIndex(k)-in%dPatch+1

       ! cumulative slip
       vector(2*(k-1)+1)=REAL(y(l+STATE_VECTOR_SLIP_STRIKE),4)
       vector(2*(k-1)+2)=REAL(y(l+STATE_VECTOR_SLIP_DIP),4)
    END DO

    ! all threads gather velocity from all threads
    CALL MPI_GATHERV(vector,layout%listVelocityN(1+rank),MPI_REAL4, &
                     vectorAll,layout%listVelocityN,layout%listVelocityOffset,MPI_REAL4, &
                     0,MPI_COMM_WORLD,ierr)

    ! export to ASCII
    IF (0 .EQ. rank) THEN
          ! export the fault patches
          WRITE (filename,'(a,"/slip-distribution-",I8.8,".dat")') TRIM(in%wdir),i
          OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          WRITE (FPOUT,'("# strike-slip, dip-slip")')
          DO k=1,in%npatch
             WRITE (FPOUT,'(2ES18.10E2)') vectorAll(2*(k-1)+1), vectorAll(2*(k-1)+2)
          END DO
          CLOSE(FPOUT)
    END IF

  END SUBROUTINE exportAsciiSlipDistribution

  !-----------------------------------------------------------------------
  !> subroutine exportGeometry
  ! export geometry of fault patches
  !----------------------------------------------------------------------
  SUBROUTINE exportGeometry(in)
    IMPLICIT NONE
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in

    CHARACTER(512) :: filename

    IF (0 .EQ. rank) THEN
       IF (0 .NE. in%rectangle%ns) THEN
          WRITE (filename,'(a,"/patch-rectangle.vtp")') TRIM(in%wdir)
          OPEN (UNIT=FPVTP,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          CALL exportvtk_rfaults(in%rectangle%ns, &
                                 in%rectangle%x, &
                                 in%rectangle%length, &
                                 in%rectangle%width, &
                                 in%rectangle%strike, &
                                 in%rectangle%dip, &
                                 in%rectangle%s, &
                                 FPVTP)
          CLOSE(FPVTP)

          WRITE (filename,'(a,"/patch-rectangle.xyz")') TRIM(in%wdir)
          OPEN (UNIT=FPVTP,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          CALL exportxyz_rfaults(in%rectangle%ns, &
                                 in%rectangle%x, &
                                 in%rectangle%length, &
                                 in%rectangle%width, &
                                 in%rectangle%strike, &
                                 in%rectangle%dip, &
                                 in%rectangle%s, &
                                 FPVTP)
          CLOSE(FPVTP)

       END IF

       IF (0 .NE. in%triangle%ns) THEN
          WRITE (filename,'(a,"/patch-triangle.vtp")') TRIM(in%wdir)
          OPEN (UNIT=FPVTP,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          CALL exportvtk_tfaults(in%triangle%ns, &
                                 in%triangle%i1, &
                                 in%triangle%i2, &
                                 in%triangle%i3, &
                                 in%triangle%nVe, &
                                 in%triangle%v, &
                                 in%triangle%s, &
                                 FPVTP)
          CLOSE(FPVTP)
       END IF
    END IF

  END SUBROUTINE exportGeometry

  !-----------------------------------------------------------------------
  !> subroutine exportPoints
  ! export observation points
  !----------------------------------------------------------------------
  SUBROUTINE exportPoints()

    IMPLICIT NONE

    INTEGER :: i,k,l,ierr
    INTEGER :: elementIndex,elementType
    CHARACTER(1024) :: formatString
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    !-----------------------------------------------------------------
    ! step 1/4 - gather the kinematics from the state vector
    !-----------------------------------------------------------------

    ! element index in d vector
    k=1
    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementIndex=layout%elementIndex(i)
       elementType= layout%elementType(i)

       SELECT CASE (elementType)
       CASE (FLAG_RECTANGLE)
             patch=in%rectangle%s(elementIndex)
       CASE (FLAG_TRIANGLE)
             patch=in%triangle%s(elementIndex)
       CASE DEFAULT
          WRITE(STDERR,'("wrong case: this is a bug.")')
          WRITE_DEBUG_INFO(-1)
          STOP 2
       END SELECT

       ! strike slip and dip slip
       u(k:k+layout%elementVelocityDGF(i)-1)= (/ &
               y(l+STATE_VECTOR_SLIP_STRIKE), &
               y(l+STATE_VECTOR_SLIP_DIP) /)

       l=l+in%dPatch

       k=k+layout%elementVelocityDGF(i)
    END DO

    !-----------------------------------------------------------------
    ! step 2/4 - calculate the rate of traction and rate of stress
    !-----------------------------------------------------------------

#ifdef __BLAS__
    ! use the BLAS library to compute the matrix vector product
    CALL DGEMV("T",SIZE(O,1),SIZE(O,2), &
                1._8,O,SIZE(O,1),u,1,0.d0,d,1)
#else
    ! slower, Fortran intrinsic
    d=MATMUL(TRANSPOSE(O),u)
#endif

    !-----------------------------------------------------------------
    ! step 3/4 - master thread adds the contribution of all elements
    !-----------------------------------------------------------------

    CALL MPI_REDUCE(d,dAll,in%nObservationPoint*DISPLACEMENT_VECTOR_DGF, &
                    MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

    !-----------------------------------------------------------------
    ! step 4/4 - master thread writes to disk
    !-----------------------------------------------------------------

    IF (0 .EQ. rank) THEN
       formatString="(ES20.14E2"
       DO i=1,DISPLACEMENT_VECTOR_DGF
          formatString=TRIM(formatString)//",X,ES21.14E2"
       END DO
       formatString=TRIM(formatString)//")"

       ! element index in d vector
       k=1
       DO i=1,in%nObservationPoint
          WRITE (in%observationPoint(i)%file,TRIM(formatString)) time,dAll(k:k+DISPLACEMENT_VECTOR_DGF-1)
          k=k+DISPLACEMENT_VECTOR_DGF
       END DO
    END IF

  END SUBROUTINE exportPoints

  !-----------------------------------------------------------------------
  !> subroutine export
  ! write the state variables of patch elements, and other information.
  !----------------------------------------------------------------------
  SUBROUTINE export()

    ! degrees of freedom
    INTEGER :: dgf

    ! counters
    INTEGER :: j,k

    ! index in state vector
    INTEGER :: index

    ! format string
    CHARACTER(1024) :: formatString

    ! gather maximum velocity in velocity-weakening region
    CALL MPI_REDUCE(vMaxWeakening,vMaxWeakeningAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

    ! gather moment-rate in velocity-weakening region
    CALL MPI_REDUCE(momentRateWeakening,momentRateWeakeningAll,1,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

    ! gather maximum velocity
    CALL MPI_REDUCE(vMax,vMaxAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

    ! gather moment-rate
    CALL MPI_REDUCE(momentRate,momentRateAll,1,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

    ! export observation state
    DO j=1,in%nObservationState
       IF ((in%observationState(1,j) .GE. layout%listOffset(rank+1)) .AND. &
           (in%observationState(1,j) .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN

          ! check observation state sampling rate
          IF (0 .EQ. MOD(i-1,in%observationState(2,j))) THEN
             dgf=in%dPatch

             formatString="(ES20.14E2"
             DO k=1,dgf
                formatString=TRIM(formatString)//",X,ES20.12E3,X,ES20.12E3"
             END DO
             formatString=TRIM(formatString)//")"

             index=layout%elementStateIndex(in%observationState(1,j)-layout%listOffset(rank+1)+1)-dgf
             WRITE (in%observationState(3,j),TRIM(formatString)) time, &
                       y(index+1:index+dgf), &
                    dydt(index+1:index+dgf)
          END IF
       END IF
    END DO

    IF (0 .EQ. rank) THEN
       WRITE(FPTIMEWEAKENING,'(ES20.14E2,ES19.12E2,ES19.12E2,ES20.12E2)') time,dt_done,vMaxWeakeningAll,momentRateWeakeningAll
       WRITE(FPTIME,'(ES20.14E2,ES19.12E2,ES19.12E2,ES20.12E2)') time,dt_done,vMaxAll,momentRateAll
       IF (0 .EQ. MOD(i,50)) THEN
          WRITE(STDOUT,'(I9.9,ES19.12E2,ES19.12E2,ES11.4E2)') i,time,dt_done,vMaxAll
          CALL FLUSH(STDOUT)
          CALL FLUSH(FPTIMEWEAKENING)
          CALL FLUSH(FPTIME)
       END IF
    END IF

  END SUBROUTINE export

  !-----------------------------------------------------------------------
  !> subroutine initStateVector
  ! initialize the state vector
  !
  ! INPUT:
  ! @param n - number of state elements own by current thread
  ! @param y - the state vector (segment owned by currect thread)
  !
  !----------------------------------------------------------------------
  SUBROUTINE initStateVector(n,y,in)
    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: n
    REAL*8, INTENT(OUT)    :: y(n)
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in

    INTEGER :: i,l
    INTEGER :: elementType,elementIndex
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    ! velocity perturbation
    REAL*8, PARAMETER :: modifier = 0.99d0

    ! initial stress
    REAL*8 :: tau

    ! initial velocity
    REAL*8 :: Vinit

    ! patch area
    REAL*8 :: area

    ! zero out state vector
    y=0._8

    ! maximum velocity
    vMaxWeakening=0._8

    ! moment-rate
    momentRateWeakening=0._8

    ! maximum velocity
    vMax=0._8

    ! moment-rate
    momentRate=0._8

    ! restart
    IF (in%isImportState) THEN
       WRITE (filename,'(a,"/state-",I4.4,".ode")') TRIM(in%wdir),rank
       IF (0 .EQ. rank) WRITE (STDOUT,'("# load state vector from ",a)') TRIM(filename)
       OPEN(FPSTATE,FILE=filename,FORM="unformatted")
       READ(FPSTATE) time
       READ(FPSTATE) y
       CLOSE(FPSTATE)

       ! element index in state vector
       l=1
       ! loop over elements owned by current thread
       DO i=1,SIZE(layout%elementIndex)
          elementType= layout%elementType(i)
          elementIndex=layout%elementIndex(i)

          SELECT CASE (elementType)
          CASE (FLAG_RECTANGLE)
                patch=in%rectangle%s(elementIndex)
                area=in%rectangle%length(elementIndex)*in%rectangle%width(elementIndex)
          CASE (FLAG_TRIANGLE)
                patch=in%triangle%s(elementIndex)
                area=in%triangle%area(elementIndex)
          CASE DEFAULT
             WRITE(STDERR,'("wrong case: this is a bug.")')
             WRITE_DEBUG_INFO(-1)
             STOP 2
          END SELECT

          Vinit=DEXP(y(l+STATE_VECTOR_VELOCITY)*lg10)

          ! maximum velocity
          vMax=MAX(Vinit,vMax)

          ! moment-rate
          momentRate=momentRate+(Vinit-patch%Vl)*in%mu*area

          l=l+in%dPatch
       END DO

       RETURN
    END IF

    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementType= layout%elementType(i)
       elementIndex=layout%elementIndex(i)

       SELECT CASE (elementType)
       CASE (FLAG_RECTANGLE)
             patch=in%rectangle%s(elementIndex)
             area=in%rectangle%length(elementIndex)*in%rectangle%width(elementIndex)
       CASE (FLAG_TRIANGLE)
             patch=in%triangle%s(elementIndex)
             area=in%triangle%area(elementIndex)
       CASE DEFAULT
          WRITE(STDERR,'("wrong case: this is a bug.")')
          WRITE_DEBUG_INFO(-1)
          STOP 2
       END SELECT

       ! strike slip
       y(l+STATE_VECTOR_SLIP_STRIKE) = 0._8

       ! dip slip
       y(l+STATE_VECTOR_SLIP_DIP) = 0._8

       ! traction
       IF (0 .GT. patch%tau0) THEN

          ! initial velocity
          Vinit=modifier*patch%Vl

          SELECT CASE(frictionLawType)
          CASE(1)
             ! multiplicative form of rate-state friction (Barbot, 2019)
             tau = patch%mu0*patch%sig*exp((patch%a-patch%b)/patch%mu0*log(patch%Vl/patch%Vo))
          CASE(2)
             ! additive form of rate-state friction (Ruina, 1983)
             tau = patch%sig*(patch%mu0+(patch%a-patch%b)*log(patch%Vl/patch%Vo))
          CASE(3)
             ! arcsinh form of rate-state friction (Rice & Benzion, 1996)
             tau = patch%a*patch%sig*ASINH(patch%Vl/2/patch%Vo*exp((patch%mu0+patch%b*log(patch%Vo/patch%Vl))/patch%a))
          CASE(4)
             ! cut-off velocity form of rate-state friction (Okubo, 1989)
             tau = patch%sig*(patch%mu0-patch%a*log(1._8+patch%Vo/patch%Vl)+patch%b*log(1._8+patch%V2/patch%Vl))
          CASE DEFAULT
             WRITE (0,'("unhandled option ", a, " (this is a bug)")') frictionLawType
             WRITE_DEBUG_INFO(100)
             STOP 3
          END SELECT
       ELSE
          tau = patch%tau0
          SELECT CASE(frictionLawType)
          CASE(1)
             ! multiplicative form of rate-state friction (Barbot, 2019)
             Vinit=patch%Vo*(tau/(patch%mu0*patch%sig))**(patch%mu0/patch%a)*(patch%L/patch%Vl)**(-patch%b/patch%a)
          CASE(2)
             ! additive form of rate-state friction (Ruina, 1983)
             Vinit=patch%Vo*EXP((tau/patch%sig-patch%mu0-patch%b*LOG(patch%Vo/patch%Vl))/patch%a)
          CASE(3)
             ! arcsinh form of rate-state friction (Rice & Benzion, 1996)
             Vinit=2*patch%Vo*SINH(tau/(patch%a*patch%sig))*EXP(-((patch%mu0+patch%b*LOG(patch%Vo/Patch%Vl))/patch%a))
          CASE DEFAULT
             WRITE (0,'("unhandled option ", a, " (this is a bug)")') frictionLawType
             WRITE_DEBUG_INFO(100)
             STOP 3
          END SELECT
       END IF

       ! traction in strike direction
       y(l+STATE_VECTOR_TRACTION_STRIKE) = tau*COS(patch%rake)

       ! traction in dip direction
       y(l+STATE_VECTOR_TRACTION_DIP) = tau*SIN(patch%rake)

       ! traction in normal direction
       y(l+STATE_VECTOR_TRACTION_NORMAL) = 0._8

       ! state variable log10(theta)
       y(l+STATE_VECTOR_STATE_1) = LOG(patch%L/patch%Vl)/lg10

       ! maximum velocity and moment-rate in velocity-weakening region
       IF (patch%a.LT.patch%b) THEN
          vMaxWeakening=MAX(patch%Vl,vMaxWeakening)
          momentRateWeakening=momentRateWeakening+Vinit*in%mu*area
       END IF

       ! maximum velocity
       vMax=MAX(patch%Vl,vMax)

       ! moment-rate
       momentRate=momentRate+(Vinit-patch%Vl)*in%mu*area

       ! slip velocity log10(V)
       y(l+STATE_VECTOR_VELOCITY) = LOG(Vinit)/lg10

       l=l+in%dPatch

    END DO

  END SUBROUTINE initStateVector

  !-----------------------------------------------------------------------
  !> subroutine odefun
  ! evalutes the derivative of the state vector
  !
  ! @param n - number of state elements own by current thread
  ! @param m - degrees of freedom
  !
  ! DESCRIPTION:
  !   1- extract slip velocity and strain rate from state vector
  !   2- calculate the rate of traction and rate of stress
  !   3- calculate the rate of remaining state variables
  !----------------------------------------------------------------------
  SUBROUTINE odefun(n,time,y,dydt)
    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: n
    REAL*8, INTENT(IN)    :: time
    REAL*8, INTENT(IN)    :: y(n)
    REAL*8, INTENT(INOUT) :: dydt(n)

    INTEGER :: i,j,k,l,ierr
    INTEGER :: elementType,elementIndex
    TYPE(PATCH_ELEMENT_STRUCT) :: patch
    REAL*8 :: correction

    ! traction components in the strike and dip directions
    REAL*8 :: ts, td

    ! scalar rate of shear traction
    REAL*8 :: dtau

    ! velocity scalar
    REAL*8 :: velocity

    ! slip velocity in the strike and dip directions
    REAL*8 :: vs,vd

    ! rake of traction and velocity
    REAL*8 :: rake

    ! friction
    REAL*8 :: friction

    ! state variable theta
    REAL*8 :: theta

    ! normal stress
    REAL*8 :: sigma

    ! modifier for the arcsinh form of rate-state friction
    REAL*8 :: reg

    ! patch area
    REAL*8 :: area

    ! maximum velocity in velocity-weakening region
    vMaxWeakening=0._8

    ! moment-rate in velocity-weakening region
    momentRateWeakening=0._8

    ! maximum velocity
    vMax=0._8

    ! initialize moment-rate
    momentRate=0._8

    ! initialize rate of state vector
    dydt=0._8

    !--------------------------------------------------------------------
    ! step 1/3 - extract slip velocity and strain rate from state vector
    !--------------------------------------------------------------------

    ! element index in v vector
    k=1
    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementType= layout%elementType(i)
       elementIndex=layout%elementIndex(i)

       SELECT CASE (elementType)
       CASE (FLAG_RECTANGLE)
          patch=in%rectangle%s(elementIndex)
          area=in%rectangle%length(elementIndex)*in%rectangle%width(elementIndex)
       CASE (FLAG_TRIANGLE)
          patch=in%triangle%s(elementIndex)
          area=in%triangle%area(elementIndex)
       CASE DEFAULT
          WRITE(STDERR,'("wrong case: this is a bug.")')
          WRITE_DEBUG_INFO(-1)
          STOP 2
       END SELECT

       ! traction and rake
       ts=y(l+STATE_VECTOR_TRACTION_STRIKE)
       td=y(l+STATE_VECTOR_TRACTION_DIP)
       rake=ATAN2(td,ts)

       ! slip velocity
       velocity=DEXP(y(l+STATE_VECTOR_VELOCITY)*lg10)
       vs=velocity*COS(rake)
       vd=velocity*SIN(rake)

       ! maximum velocity and moment-rate in velocity-weakening region
       IF (patch%a.LT.patch%b) THEN
          vMaxWeakening=MAX(velocity,vMaxWeakening)
          momentRateWeakening=momentRateWeakening+velocity*in%mu*area
       END IF

       ! maximum velocity
       vMax=MAX(velocity,vMax)

       ! moment-rate
       momentRate=momentRate+(velocity-patch%Vl)*in%mu*area

       ! update state vector (rate of slip components)
       dydt(l+STATE_VECTOR_SLIP_STRIKE)=vs
       dydt(l+STATE_VECTOR_SLIP_DIP   )=vd

       ! v(k:k+layout%elementVelocityDGF(i)-1) = slip velocity
       v(k:k+layout%elementVelocityDGF(i)-1)=(/ &
                     vs-patch%Vl*COS(patch%rake), &
                     vd-patch%Vl*SIN(patch%rake) /)

       l=l+in%dPatch

       k=k+layout%elementVelocityDGF(i)
    END DO

    ! all threads gather velocity from all threads
    CALL MPI_ALLGATHERV(v,layout%listVelocityN(1+rank),MPI_REAL8, &
                        vAll,layout%listVelocityN,layout%listVelocityOffset,MPI_REAL8, &
                        MPI_COMM_WORLD,ierr)

    !-----------------------------------------------------------------
    ! step 2/3 - calculate the rate of traction and rate of stress
    !-----------------------------------------------------------------

#ifdef __BLAS__
    ! use the BLAS library to compute the matrix vector product
    CALL DGEMV("T",SIZE(G,1),SIZE(G,2), &
                1._8,G,SIZE(G,1),vAll,1,0.d0,t,1)
#else
    ! slower, Fortran intrinsic
    t=MATMUL(TRANSPOSE(G),vAll)
#endif

    !-----------------------------------------------------------------
    ! step 3/3 - calculate the rate of remaining state variables
    !-----------------------------------------------------------------

    ! element index in t vector
    j=1
    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementType= layout%elementType(i)
       elementIndex=layout%elementIndex(i)

       SELECT CASE (elementType)
       CASE (FLAG_RECTANGLE)
          patch=in%rectangle%s(elementIndex)
       CASE (FLAG_TRIANGLE)
          patch=in%triangle%s(elementIndex)
       CASE DEFAULT
          WRITE(STDERR,'("wrong case: this is a bug.")')
          WRITE_DEBUG_INFO(-1)
          STOP 2
       END SELECT

       ! traction and rake
       ts=y(l+STATE_VECTOR_TRACTION_STRIKE)
       td=y(l+STATE_VECTOR_TRACTION_DIP)
       rake=ATAN2(td,ts)

       ! slip velocity
       velocity=DEXP(y(l+STATE_VECTOR_VELOCITY)*lg10)

       ! rate of state
       SELECT CASE(evolutionLawType)
       CASE(1)
          ! isothermal, isobaric aging law
          dydt(l+STATE_VECTOR_STATE_1)=(EXP(-y(l+STATE_VECTOR_STATE_1)*lg10)-velocity/patch%L)/lg10
       CASE(2)
          ! isothermal, isobaric slip law
          dydt(l+STATE_VECTOR_STATE_1)=(-velocity/patch%L*(LOG(velocity/patch%L)+y(l+STATE_VECTOR_STATE_1)*lg10))
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug)")') evolutionLawType
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT

       ! scalar rate of shear traction
       dtau=t(j+TRACTION_VECTOR_STRIKE)*COS(rake) &
           +t(j+TRACTION_VECTOR_DIP)   *SIN(rake)
          
       ! normal stress
       !sigma=patch%sig-y(l+STATE_VECTOR_TRACTION_NORMAL)
       sigma=patch%sig

       SELECT CASE(frictionLawType)
       CASE(1)
          ! multiplicative form of rate-state friction (Barbot, 2019)
          friction=patch%mu0*EXP(patch%a/patch%mu0*LOG(velocity/patch%Vo) &
                                +patch%b/patch%mu0*(y(l+STATE_VECTOR_STATE_1)*lg10+LOG(patch%Vo/patch%L)))

          ! acceleration (1/V dV/dt) / log(10)
          !                               +friction*t(j+TRACTION_VECTOR_NORMAL)) / &
          dydt(l+STATE_VECTOR_VELOCITY)= &
               (dtau-patch%b*sigma*friction/patch%mu0*dydt(l+STATE_VECTOR_STATE_1)*lg10 &
                                                                               ) / &
               (patch%a*sigma*friction/patch%mu0+patch%damping*velocity) / lg10
       CASE(2)
          ! additive form of rate-state friction (Ruina, 1983)
          friction=patch%mu0+patch%a*LOG(velocity/patch%Vo) &
                            +patch%b*LOG(EXP(y(l+STATE_VECTOR_STATE_1)*lg10)/patch%L*patch%Vo)

          ! acceleration
          dydt(l+STATE_VECTOR_VELOCITY)= &
               (dtau-patch%b*sigma*dydt(l+STATE_VECTOR_STATE_1)*lg10 &
                                          +friction*t(j+TRACTION_VECTOR_NORMAL)) / &
               (patch%a*sigma+patch%damping*velocity) / lg10
       CASE(3)
          ! arcsinh form of rate-state friction (Rice & Benzion, 1996)
          reg=2.0d0*patch%Vo/velocity*EXP(-(patch%mu0+patch%b*(y(STATE_VECTOR_STATE_1)*lg10-LOG(patch%L/patch%Vo)))/patch%a)

          friction=patch%a*ASINH(1._8/reg)

          reg=1.0d0/SQRT(1._8+reg**2)

          ! acceleration
          dydt(l+STATE_VECTOR_VELOCITY)= &
                  (dtau-patch%b*sigma*dydt(l+STATE_VECTOR_STATE_1)*lg10*reg &
                                          +friction*t(j+TRACTION_VECTOR_NORMAL)) / &
                  (patch%a*sigma*reg+patch%damping*velocity) / lg10
       CASE(4)
          ! cut-off velocity form of rate-state friction (Okubo, 1989)
          friction=patch%mu0-patch%a*LOG(1._8+patch%Vo/velocity) &
                            +patch%b*LOG(1._8+exp(y(l+STATE_VECTOR_STATE_1)*lg10)/patch%L*patch%V2)

          ! state variable
          theta=EXP(y(l+STATE_VECTOR_STATE_1)*lg10)

          ! acceleration
          dydt(l+STATE_VECTOR_VELOCITY)= &
               (dtau-patch%b*sigma*dydt(l+STATE_VECTOR_STATE_1)*lg10/(1._8+patch%L/patch%V2/theta) &
                                          +friction*t(j+TRACTION_VECTOR_NORMAL)) / &
               (patch%a*sigma/(1._8+velocity/patch%Vo)+patch%damping*velocity) / lg10
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug)")') frictionLawType
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT

       ! correction
       correction=patch%damping*velocity*dydt(l+STATE_VECTOR_VELOCITY)*lg10

       ! traction rate
       dydt(l+STATE_VECTOR_TRACTION_STRIKE)=t(j+TRACTION_VECTOR_STRIKE)-correction*COS(rake)
       dydt(l+STATE_VECTOR_TRACTION_DIP   )=t(j+TRACTION_VECTOR_DIP   )-correction*SIN(rake)
       dydt(l+STATE_VECTOR_TRACTION_NORMAL)=t(j+TRACTION_VECTOR_NORMAL)

       l=l+in%dPatch

       j=j+layout%elementForceDGF(i)
    END DO

  END SUBROUTINE odefun

  !-----------------------------------------------------------------------
  !> subroutine initParallelism()
  !! initialize variables describe the data layout
  !! for parallelism.
  !!
  !! OUTPUT:
  !! layout    - list of receiver type and type index
  !-----------------------------------------------------------------------
  SUBROUTINE initParallelism(in,layout)
    IMPLICIT NONE
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in
    TYPE(LAYOUT_STRUCT), INTENT(OUT) :: layout

    INTEGER :: i,j,k,ierr,remainder
    INTEGER :: cumulativeIndex,cumulativeVelocityIndex
    INTEGER :: rank,csize
    INTEGER :: nElements

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

    ! list of number of elements in thread
    ALLOCATE(layout%listElements(csize), &
             layout%listOffset(csize),STAT=ierr)
    IF (ierr>0) STOP "could not allocate the list"

    ! total number of elements
    nElements=in%rectangle%ns+in%triangle%ns

    remainder=nElements-INT(nElements/csize)*csize
    IF (0 .LT. remainder) THEN
       layout%listElements(1:(csize-remainder))      =INT(nElements/csize)
       layout%listElements((csize-remainder+1):csize)=INT(nElements/csize)+1
    ELSE
       layout%listElements(1:csize)=INT(nElements/csize)
    END IF

    ! element start index in thread
    j=0
    k=0
    DO i=1,csize
       j=k+1
       k=k+layout%listElements(i)
       layout%listOffset(i)=j
    END DO

    ALLOCATE(layout%elementType       (layout%listElements(1+rank)), &
             layout%elementIndex      (layout%listElements(1+rank)), &
             layout%elementStateIndex (layout%listElements(1+rank)), &
             layout%elementVelocityDGF(layout%listElements(1+rank)), &
             layout%elementVelocityIndex(layout%listElements(1+rank)), &
             layout%elementStateDGF   (layout%listElements(1+rank)), &
             layout%elementForceDGF   (layout%listElements(1+rank)),STAT=ierr)
    IF (ierr>0) STOP "could not allocate the layout elements"

    j=1
    cumulativeIndex=0
    cumulativeVelocityIndex=0
    DO i=1,in%rectangle%ns
       IF ((i .GE. layout%listOffset(1+rank)) .AND. &
           (i .LT. (layout%listOffset(1+rank)+layout%listElements(1+rank)))) THEN
          layout%elementType(j)=FLAG_RECTANGLE
          layout%elementIndex(j)=i
          layout%elementStateIndex(j)=cumulativeIndex+STATE_VECTOR_DGF_PATCH
          cumulativeIndex=layout%elementStateIndex(j)
          layout%elementVelocityDGF(j)=DGF_PATCH
          layout%elementVelocityIndex(j)=cumulativeVelocityIndex+DGF_PATCH
          cumulativeVelocityIndex=layout%elementVelocityIndex(j)
          layout%elementStateDGF(j)=in%dPatch
          layout%elementForceDGF(j)=DGF_VECTOR
          j=j+1
       END IF
    END DO
    DO i=1,in%triangle%ns
       IF (((i+in%rectangle%ns) .GE. layout%listOffset(1+rank)) .AND. &
           ((i+in%rectangle%ns) .LT. (layout%listOffset(1+rank)+layout%listElements(1+rank)))) THEN
          layout%elementType(j)=FLAG_TRIANGLE
          layout%elementIndex(j)=i
          layout%elementStateIndex(j)=cumulativeIndex+STATE_VECTOR_DGF_PATCH
          cumulativeIndex=layout%elementStateIndex(j)
          layout%elementVelocityDGF(j)=DGF_PATCH
          layout%elementVelocityIndex(j)=cumulativeVelocityIndex+DGF_PATCH
          cumulativeVelocityIndex=layout%elementVelocityIndex(j)
          layout%elementStateDGF(j)=in%dPatch
          layout%elementForceDGF(j)=DGF_VECTOR
          j=j+1
       END IF
    END DO

    ! for MPI_ALLGATHERV
    ALLOCATE(layout%listVelocityN(csize), &
             layout%listVelocityOffset(csize), &
             layout%listStateN(csize), &
             layout%listStateOffset(csize), &
             layout%listForceN(csize), &
             STAT=ierr)
    IF (ierr>0) STOP "could not allocate the size list"

    ! share number of elements in threads
    CALL MPI_ALLGATHER(SUM(layout%elementVelocityDGF),1,MPI_INTEGER,layout%listVelocityN,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
    CALL MPI_ALLGATHER(SUM(layout%elementStateDGF),   1,MPI_INTEGER,layout%listStateN,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
    CALL MPI_ALLGATHER(SUM(layout%elementForceDGF),   1,MPI_INTEGER,layout%listForceN,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)

    j=0
    k=0
    DO i=1,csize
       j=k+1
       k=k+layout%listVelocityN(i)
       layout%listVelocityOffset(i)=j-1
    END DO

    j=0
    k=0
    DO i=1,csize
       j=k+1
       k=k+layout%listStateN(i)
       layout%listStateOffset(i)=j-1
    END DO

  END SUBROUTINE initParallelism


  !-----------------------------------------------------------------------
  !> subroutine initGeometry
  ! initializes the position and local reference system vectors
  !
  ! INPUT:
  ! @param in      - input parameters data structure
  !-----------------------------------------------------------------------
  SUBROUTINE initGeometry(in)
    USE stuart97
    USE okada92
    USE types_3d
    TYPE(SIMULATION_STRUCT), INTENT(INOUT) :: in

    IF (0 .LT. in%rectangle%ns) THEN
       CALL computeReferenceSystemOkada92( &
                in%rectangle%ns, &
                in%rectangle%x, &
                in%rectangle%length, &
                in%rectangle%width, &
                in%rectangle%strike, &
                in%rectangle%dip, &
                in%rectangle%sv, &
                in%rectangle%dv, &
                in%rectangle%nv, &
                in%rectangle%xc)
    END IF

    IF (0 .LT. in%triangle%ns) THEN
       CALL computeReferenceSystemStuart97( &
                in%triangle%nVe, &
                in%triangle%v, &
                in%triangle%ns, &
                in%triangle%i1, &
                in%triangle%i2, &
                in%triangle%i3, &
                in%triangle%sv, &
                in%triangle%dv, &
                in%triangle%nv, &
                in%triangle%xc)
    END IF

  END SUBROUTINE initGeometry

  !---------------------------------------------------------------------
  !> subroutine init
  !! reads simulation parameters from the standard input and initialize
  !! model parameters.
  !!
  !! INPUT:
  !! @param unit - the unit number used to read input data
  !!
  !! OUTPUT:
  !! @param in
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !---------------------------------------------------------------------
  SUBROUTINE init(in)
    USE types_3d
    USE getopt_m

    TYPE(SIMULATION_STRUCT), INTENT(OUT) :: in

    CHARACTER :: ch
    CHARACTER(512) :: dataline
    CHARACTER(256) :: filename
    INTEGER :: iunit,noptions
!$  INTEGER :: omp_get_num_procs,omp_get_max_threads
    TYPE(OPTION_S) :: opts(17)
    REAL*8, DIMENSION(3) :: normal

    INTEGER :: i,k,rank,size,ierr,position
    INTEGER :: maxVertexIndex=-1
    INTEGER, PARAMETER :: psize=1024
    INTEGER :: dummy
    CHARACTER, DIMENSION(psize) :: packed

    TYPE(PATCH_ELEMENT_STRUCT) :: patchMin, patchMax

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,size,ierr)

    ! define long options, such as --dry-run
    ! parse the command line for options
    opts( 1)=OPTION_S("version",.FALSE.,CHAR(21))
    opts( 2)=OPTION_S("dry-run",.FALSE.,CHAR(22))
    opts( 3)=OPTION_S("epsilon",.TRUE.,'e')
    opts( 4)=OPTION_S("export-greens",.TRUE.,'g')
    opts( 5)=OPTION_S("export-netcdf",.FALSE.,'n')
    opts( 6)=OPTION_S("export-slip",.FALSE.,'l')
    opts( 7)=OPTION_S("export-stress",.FALSE.,'t')
    opts( 8)=OPTION_S("export-slip-distribution",.TRUE.,'s')
    opts( 9)=OPTION_S("import-state",.TRUE.,'k')
    opts(10)=OPTION_S("export-state",.FALSE.,'x')
    opts(11)=OPTION_S("friction-law",.TRUE.,'f')
    opts(12)=OPTION_S("evolution-law",.TRUE.,'j')
    opts(13)=OPTION_S("import-greens",.TRUE.,'p')
    opts(14)=OPTION_S("maximum-step",.TRUE.,'m')
    opts(15)=OPTION_S("maximum-iterations",.TRUE.,'i')
    opts(16)=OPTION_S("help",.FALSE.,'h')
    opts(17)=OPTION_S("verbose",.TRUE.,'v')

    noptions=0
    DO
       ch=getopt("he:f:g:i:j:k:lm:np:s:tv:x",opts)
       SELECT CASE(ch)
       CASE(CHAR(0))
          EXIT
       CASE(CHAR(21))
          ! option version
          in%isversion=.TRUE.
       CASE(CHAR(22))
          ! option dry-run
          in%isdryrun=.TRUE.
       CASE('e')
          ! numerical accuracy (variable epsilon sits in the rk module)
          READ(optarg,*) epsilon
          noptions=noptions+1
       CASE('f')
          ! type of friction law
          READ(optarg,*) frictionLawType
          noptions=noptions+1
       CASE('g')
          ! export Greens functions to netcdf file
          READ(optarg,'(a)') in%greensFunctionDirectory
          in%isExportGreens=.TRUE.
          noptions=noptions+1
       CASE('i')
          ! maximum number of iterations
          READ(optarg,*) maximumIterations
          noptions=noptions+1
       CASE('j')
          ! type of evolution law
          READ(optarg,*) evolutionLawType
          noptions=noptions+1
       CASE('l')
          ! export slip in netcdf format
          in%isExportSlip=.TRUE.
       CASE('m')
          ! maximum time step (variable maximumTimeStep sits in the rk module)
          READ(optarg,*) maximumTimeStep
          noptions=noptions+1
       CASE('n')
          ! export in netcdf format
          in%isExportNetcdf=.TRUE.
       CASE('p')
          ! import Greens functions from netcdf file
          READ(optarg,'(a)') in%greensFunctionDirectory
          in%isImportGreens=.TRUE.
          noptions=noptions+1
       CASE('s')
          ! export slip distribution
          READ(optarg,*) exportSlipDistributionRate
          noptions=noptions+1
       CASE('t')
          ! export stress in netcdf format
          in%isExportStress=.TRUE.
       CASE('k')
          ! import state vector
          in%isImportState=.TRUE.
          READ(optarg,'(a)') importStateDir
          noptions=noptions+1
       CASE('x')
          ! export in netcdf format
          in%isExportState=.TRUE.
       CASE('h')
          ! option help
          in%ishelp=.TRUE.
       CASE('v')
          ! verbosity
          READ(optarg,*) verbose
          noptions=noptions+1
       CASE('?')
          WRITE_DEBUG_INFO(100)
          in%ishelp=.TRUE.
          EXIT
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug)")') optopt
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT
       noptions=noptions+1
    END DO

    IF (in%isversion) THEN
       CALL printversion()
       ! abort parameter input
       CALL MPI_FINALIZE(ierr)
       STOP
    END IF

    IF (in%ishelp) THEN
       CALL printhelp()
       ! abort parameter input
       CALL MPI_FINALIZE(ierr)
       STOP
    END IF

    in%nPatch=0
    ! minimum number of dynamic variables for patches
    in%dPatch=STATE_VECTOR_DGF_PATCH
    in%rectangle%ns=0
    in%triangle%ns=0
    in%triangle%nVe=0
    in%nVolume=0
    in%dVolume=STATE_VECTOR_DGF_VOLUME
    in%cuboid%ns=0

    IF (0.EQ.rank) THEN
       PRINT 2000
       PRINT '("# RATESTATE")'
       PRINT '("# quasi-dynamic earthquake simulation in three-dimensional media")'
       PRINT '("# with the integral method.")'
       SELECT CASE(frictionLawType)
       CASE(1)
          PRINT '("# friction law: multiplicative form of rate-state friction (Barbot, 2019)")'
       CASE(2)
          PRINT '("# friction law: additive form of rate-state friction (Ruina, 1983)")'
       CASE(3)
          PRINT '("# friction law: arcsinh form of rate-state friction (Rice & BenZion, 1996)")'
       CASE(4)
          PRINT '("# friction law: rate-state with cut-off velocity (Okubo, 1989)")'
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug)")') frictionLawType
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT
       SELECT CASE(evolutionLawType)
       CASE(1)
          PRINT '("# evolution law: isothermal, isobaric additive form (Ruina, 1983)")'
       CASE(2)
          PRINT '("# evolution law: isothermal, isobaric multiplicative form (Ruina, 1983)")'
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug)")') evolutionLawType
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT
       PRINT '("# numerical accuracy: ",ES11.4)', epsilon
       PRINT '("# maximum iterations: ",I11)', maximumIterations
       PRINT '("# maximum time step: ",ES12.4)', maximumTimeStep
       PRINT '("# number of threads: ",I12)', csize
       IF (in%isExportNetcdf) THEN
          PRINT '("# export velocity to netcdf:      yes")'
       ELSE
          PRINT '("# export velocity to netcdf:       no")'
       END IF
#ifdef NETCDF
       IF (0 .LT. exportSlipDistributionRate) THEN
          PRINT '("# export slip distribution every ",I4," steps")', exportSlipDistributionRate
       END IF
       IF (in%isImportGreens) THEN
          WRITE (STDOUT,'("# import greens function:         yes (",a,")")') TRIM(in%greensFunctionDirectory)
       ELSE IF (in%isExportGreens) THEN
          WRITE (STDOUT,'("# export greens function:         yes (",a,")")') TRIM(in%greensFunctionDirectory)
       END IF
#endif
!$     PRINT '("#     * parallel OpenMP implementation with ",I3.3,"/",I3.3," threads")', &
!$                  omp_get_max_threads(),omp_get_num_procs()
       PRINT 2000

       IF (noptions .LT. COMMAND_ARGUMENT_COUNT()) THEN
          ! read from input file
          iunit=25
          CALL GET_COMMAND_ARGUMENT(noptions+1,filename)
          OPEN (UNIT=iunit,FILE=filename,IOSTAT=ierr)
       ELSE
          ! get input parameters from standard input
          iunit=5
       END IF

       PRINT '("# output directory")'
       CALL getdata(iunit,dataline)
       READ (dataline,'(a)') in%wdir
       PRINT '(2X,a)', TRIM(in%wdir)

       ! test write permissions on output directory
       in%timeFilename=TRIM(in%wdir)//"/time.dat"
       OPEN (UNIT=FPTIME,FILE=in%timeFilename,POSITION="APPEND",&
               IOSTAT=ierr,FORM="FORMATTED")
       IF (ierr>0) THEN
          WRITE_DEBUG_INFO(102)
          WRITE (STDERR,'("error: unable to access ",a)') TRIM(in%timefilename)
          STOP 1
       END IF
       CLOSE(FPTIME)
   
       PRINT '("# elastic moduli (lambda, mu)")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%lambda,in%mu
       PRINT '(2ES9.2E1)', in%lambda,in%mu

       IF (0 .GT. in%mu) THEN
          WRITE_DEBUG_INFO(-1)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("input error: shear modulus must be positive")')
          STOP 2
       END IF

       in%nu=in%lambda/2._8/(in%lambda+in%mu)
       IF (-1._8 .GT. in%nu) THEN
          WRITE_DEBUG_INFO(-1)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("input error: Poisson''s ratio must be greater than -1.")')
          STOP 2
       END IF
       IF (0.5_8 .LT. in%nu) THEN
          WRITE_DEBUG_INFO(-1)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("input error: Poisson''s ratio must be lower than 0.5.")')
          STOP 2
       END IF

       PRINT '("# time interval")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%interval
       PRINT '(ES20.12E2)', in%interval

       IF (in%interval .LE. 0._8) THEN
          WRITE (STDERR,'("**** error **** ")')
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("simulation time must be positive. exiting.")')
          STOP 1
       END IF

       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !       R E C T A N G L E   P A T C H E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT '("# number of rectangle patches")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%rectangle%ns
       PRINT '(I5)', in%rectangle%ns
       IF (in%rectangle%ns .GT. 0) THEN
          ALLOCATE(in%rectangle%s(in%rectangle%ns), &
                   in%rectangle%x(3,in%rectangle%ns), &
                   in%rectangle%xc(3,in%rectangle%ns), &
                   in%rectangle%length(in%rectangle%ns), &
                   in%rectangle%width(in%rectangle%ns), &
                   in%rectangle%strike(in%rectangle%ns), &
                   in%rectangle%dip(in%rectangle%ns), &
                   in%rectangle%sv(3,in%rectangle%ns), &
                   in%rectangle%dv(3,in%rectangle%ns), &
                   in%rectangle%nv(3,in%rectangle%ns),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the rectangle patch list"
          PRINT 2000
          PRINT '("#    n        Vl       x1       x2      x3  length   width strike   dip   rake")'
          PRINT 2000
          DO k=1,in%rectangle%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                  in%rectangle%s(k)%Vl, &
                  in%rectangle%x(1,k), &
                  in%rectangle%x(2,k), &
                  in%rectangle%x(3,k), &
                  in%rectangle%length(k), &
                  in%rectangle%width(k), &
                  in%rectangle%strike(k), &
                  in%rectangle%dip(k), &
                  in%rectangle%s(k)%rake
             in%rectangle%s(k)%opening=0
   
             IF ((2 .LE. verbose) .OR. (4 .GE. i) .OR. (in%rectangle%ns-3 .LE. i)) THEN

                PRINT '(I6,ES10.2E2,2ES9.2E1,3ES8.2E1,f7.1,f6.1,f7.1)',i, &
                     in%rectangle%s(k)%Vl, &
                     in%rectangle%x(1,k), &
                     in%rectangle%x(2,k), &
                     in%rectangle%x(3,k), &
                     in%rectangle%length(k), &
                     in%rectangle%width(k), &
                     in%rectangle%strike(k), &
                     in%rectangle%dip(k), &
                     in%rectangle%s(k)%rake
             END IF

             ! convert to radians
             in%rectangle%strike(k)=in%rectangle%strike(k)*DEG2RAD     
             in%rectangle%dip(k)=in%rectangle%dip(k)*DEG2RAD     
             in%rectangle%s(k)%rake=in%rectangle%s(k)%rake*DEG2RAD     

             IF (i .NE. k) THEN
                WRITE (STDERR,'("invalid rectangle patch definition")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
             IF (MAX(in%rectangle%length(k),in%rectangle%width(k)) .LE. 0._8) THEN
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: patch length and width must be positive.")')
                STOP 1
             END IF
                
          END DO
   
          ! export rectangle patches
          filename=TRIM(in%wdir)//"/rfaults.flt"
          OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          WRITE (FPOUT,'("#    n              Vl              x1              x2              x3    ", &
                       & "length     width strike    dip   rake")')
          DO k=1,in%rectangle%ns
             WRITE (FPOUT,'(I6,4ES16.7E3,2ES10.4E1,f7.2,f7.2,f7.2)') k, &
                  in%rectangle%s(k)%Vl, &
                  in%rectangle%x(1,k), &
                  in%rectangle%x(2,k), &
                  in%rectangle%x(3,k), &
                  in%rectangle%length(k), &
                  in%rectangle%width(k), &
                  in%rectangle%strike(k)/DEG2RAD, &
                  in%rectangle%dip(k)/DEG2RAD, &
                  in%rectangle%s(k)%rake/DEG2RAD
          END DO
          CLOSE(FPOUT)
                
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          !        F R I C T I O N   P R O P E R T I E S
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          PRINT 2000
          PRINT '("# number of frictional rectangle patches")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) dummy
          PRINT '(I5)', dummy
          IF (dummy .NE. in%rectangle%ns) THEN
             WRITE_DEBUG_INFO(-1)
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE(STDERR,'("input error: all rectangle patches require frictional properties")')
             STOP 2
          END IF

          ! parameter range
          patchMin%tau0=HUGE(1.e0)
          patchMin%mu0=HUGE(1.e0)
          patchMin%sig=HUGE(1.e0)
          patchMin%a=HUGE(1.e0)
          patchMin%b=HUGE(1.e0)
          patchMin%L=HUGE(1.e0)
          patchMin%Vo=HUGE(1.e0)
          patchMin%damping=HUGE(1.e0)

          patchMax%tau0=-HUGE(1.e0)
          patchMax%mu0=-HUGE(1.e0)
          patchMax%sig=-HUGE(1.e0)
          patchMax%a=-HUGE(1.e0)
          patchMax%b=-HUGE(1.e0)
          patchMax%L=-HUGE(1.e0)
          patchMax%Vo=-HUGE(1.e0)
          patchMax%damping=-HUGE(1.e0)

          SELECT CASE(frictionLawType)
          CASE(1:3)
             PRINT '("#    n     tau0      mu0      sig        a        b        L       Vo  G/(2Vs)")'
             PRINT 2000
             DO k=1,in%rectangle%ns
                CALL getdata(iunit,dataline)
                READ (dataline,*,IOSTAT=ierr) i, &
                      in%rectangle%s(k)%tau0, &
                      in%rectangle%s(k)%mu0, &
                      in%rectangle%s(k)%sig, &
                      in%rectangle%s(k)%a, &
                      in%rectangle%s(k)%b, &
                      in%rectangle%s(k)%L, &
                      in%rectangle%s(k)%Vo, &
                      in%rectangle%s(k)%damping
   
                IF ((2 .LE. verbose) .OR. (4 .GE. i) .OR. (in%rectangle%ns-3 .LE. i)) THEN

                   PRINT '(I6,8ES9.2E1)',i, &
                        in%rectangle%s(k)%tau0, &
                        in%rectangle%s(k)%mu0, &
                        in%rectangle%s(k)%sig, &
                        in%rectangle%s(k)%a, &
                        in%rectangle%s(k)%b, &
                        in%rectangle%s(k)%L, &
                        in%rectangle%s(k)%Vo, &
                        in%rectangle%s(k)%damping
                END IF
                
                ! parameter range
                patchMin%tau0     =MIN(patchMin%tau0,in%rectangle%s(k)%tau0)
                patchMin%mu0      =MIN(patchMin%mu0,in%rectangle%s(k)%mu0)
                patchMin%sig      =MIN(patchMin%sig,in%rectangle%s(k)%sig)
                patchMin%a        =MIN(patchMin%a,in%rectangle%s(k)%a)
                patchMin%b        =MIN(patchMin%b,in%rectangle%s(k)%b)
                patchMin%L        =MIN(patchMin%L,in%rectangle%s(k)%L)
                patchMin%Vo       =MIN(patchMin%Vo,in%rectangle%s(k)%Vo)
                patchMin%V2       =MIN(patchMin%Vo,in%rectangle%s(k)%V2)
                patchMin%damping  =MIN(patchMin%damping,in%rectangle%s(k)%damping)

                patchMax%tau0     =MAX(patchMax%tau0,in%rectangle%s(k)%tau0)
                patchMax%mu0      =MAX(patchMax%mu0,in%rectangle%s(k)%mu0)
                patchMax%sig      =MAX(patchMax%sig,in%rectangle%s(k)%sig)
                patchMax%a        =MAX(patchMax%a,in%rectangle%s(k)%a)
                patchMax%b        =MAX(patchMax%b,in%rectangle%s(k)%b)
                patchMax%L        =MAX(patchMax%L,in%rectangle%s(k)%L)
                patchMax%Vo       =MAX(patchMax%Vo,in%rectangle%s(k)%Vo)
                patchMax%V2       =MAX(patchMax%Vo,in%rectangle%s(k)%V2)
                patchMax%damping  =MAX(patchMax%damping,in%rectangle%s(k)%damping)

                IF (i .NE. k) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("invalid friction property definition for rectangle patch")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   WRITE (STDERR,'("error in input file: unexpected index")')
                   STOP 1
                END IF
             END DO

             PRINT 2000
             PRINT '("# min:",8ES9.2E1)', &
                  patchMin%tau0, &
                  patchMin%mu0, &
                  patchMin%sig, &
                  patchMin%a, &
                  patchMin%b, &
                  patchMin%L, &
                  patchMin%Vo, &
                  patchMin%damping

             PRINT '("# max:",8ES9.2E1)', &
                  patchMax%tau0, &
                  patchMax%mu0, &
                  patchMax%sig, &
                  patchMax%a, &
                  patchMax%b, &
                  patchMax%L, &
                  patchMax%Vo, &
                  patchMax%damping
             PRINT 2000

          CASE(4)
             PRINT '("#    n     tau0      mu0      sig        a        b        L       V1       V2  G/(2Vs)")'
             PRINT 2000
             DO k=1,in%rectangle%ns
                CALL getdata(iunit,dataline)
                READ (dataline,*,IOSTAT=ierr) i, &
                      in%rectangle%s(k)%tau0, &
                      in%rectangle%s(k)%mu0, &
                      in%rectangle%s(k)%sig, &
                      in%rectangle%s(k)%a, &
                      in%rectangle%s(k)%b, &
                      in%rectangle%s(k)%L, &
                      in%rectangle%s(k)%Vo, &
                      in%rectangle%s(k)%V2, &
                      in%rectangle%s(k)%damping
   
                IF ((2 .LE. verbose) .OR. (4 .GE. i) .OR. (in%rectangle%ns-3 .LE. i)) THEN

                   PRINT '(I6,9ES9.2E1)',i, &
                        in%rectangle%s(k)%tau0, &
                        in%rectangle%s(k)%mu0, &
                        in%rectangle%s(k)%sig, &
                        in%rectangle%s(k)%a, &
                        in%rectangle%s(k)%b, &
                        in%rectangle%s(k)%L, &
                        in%rectangle%s(k)%Vo, &
                        in%rectangle%s(k)%V2, &
                        in%rectangle%s(k)%damping
                END IF

                ! parameter range
                patchMin%tau0     =MIN(patchMin%tau0,in%rectangle%s(k)%tau0)
                patchMin%mu0      =MIN(patchMin%mu0,in%rectangle%s(k)%mu0)
                patchMin%sig      =MIN(patchMin%sig,in%rectangle%s(k)%sig)
                patchMin%a        =MIN(patchMin%a,in%rectangle%s(k)%a)
                patchMin%b        =MIN(patchMin%b,in%rectangle%s(k)%b)
                patchMin%L        =MIN(patchMin%L,in%rectangle%s(k)%L)
                patchMin%Vo       =MIN(patchMin%Vo,in%rectangle%s(k)%Vo)
                patchMin%V2       =MIN(patchMin%Vo,in%rectangle%s(k)%V2)
                patchMin%damping  =MIN(patchMin%damping,in%rectangle%s(k)%damping)

                patchMax%tau0     =MAX(patchMax%tau0,in%rectangle%s(k)%tau0)
                patchMax%mu0      =MAX(patchMax%mu0,in%rectangle%s(k)%mu0)
                patchMax%sig      =MAX(patchMax%sig,in%rectangle%s(k)%sig)
                patchMax%a        =MAX(patchMax%a,in%rectangle%s(k)%a)
                patchMax%b        =MAX(patchMax%b,in%rectangle%s(k)%b)
                patchMax%L        =MAX(patchMax%L,in%rectangle%s(k)%L)
                patchMax%Vo       =MAX(patchMax%Vo,in%rectangle%s(k)%Vo)
                patchMax%V2       =MAX(patchMax%Vo,in%rectangle%s(k)%V2)
                patchMax%damping  =MAX(patchMax%damping,in%rectangle%s(k)%damping)

                IF (i .NE. k) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("invalid friction property definition for rectangle patch")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   WRITE (STDERR,'("error in input file: unexpected index")')
                   STOP 1
                END IF
             END DO

             PRINT 2000
             PRINT '("# min:",9ES9.2E1)', &
                  patchMin%tau0, &
                  patchMin%mu0, &
                  patchMin%sig, &
                  patchMin%a, &
                  patchMin%b, &
                  patchMin%L, &
                  patchMin%Vo, &
                  patchMin%V2, &
                  patchMin%damping

             PRINT '("# max:",9ES9.2E1)', &
                  patchMax%tau0, &
                  patchMax%mu0, &
                  patchMax%sig, &
                  patchMax%a, &
                  patchMax%b, &
                  patchMax%L, &
                  patchMax%Vo, &
                  patchMax%V2, &
                  patchMax%damping
             PRINT 2000

          CASE DEFAULT
             WRITE (0,'("unhandled option ", a, " (this is a bug)")') frictionLawType
             WRITE_DEBUG_INFO(100)
             STOP 3
          END SELECT
   
       END IF
          
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !        T R I A N G L E   P A T C H E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT '("# number of triangle patches")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%triangle%ns
       PRINT '(I5)', in%triangle%ns
       IF (in%triangle%ns .GT. 0) THEN
          ALLOCATE(in%triangle%s(in%triangle%ns), &
                   in%triangle%i1(in%triangle%ns), &
                   in%triangle%i2(in%triangle%ns), &
                   in%triangle%i3(in%triangle%ns), &
                   in%triangle%area(in%triangle%ns), &
                   in%triangle%sv(3,in%triangle%ns), &
                   in%triangle%dv(3,in%triangle%ns), &
                   in%triangle%nv(3,in%triangle%ns), &
                   in%triangle%xc(3,in%triangle%ns),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the triangle patch list"
          PRINT 2000
          PRINT '("#    n       Vl        i1        i2        i3   rake")'
          PRINT 2000
          DO k=1,in%triangle%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                  in%triangle%s(k)%Vl, &
                  in%triangle%i1(k), &
                  in%triangle%i2(k), &
                  in%triangle%i3(k), &
                  in%triangle%s(k)%rake
             in%triangle%s(k)%opening=0
   
             PRINT '(I6,ES9.2E1,3I10,f7.1)',i, &
                  in%triangle%s(k)%Vl, &
                  in%triangle%i1(k), &
                  in%triangle%i2(k), &
                  in%triangle%i3(k), &
                  in%triangle%s(k)%rake
                
             ! convert to radians
             in%triangle%s(k)%rake=in%triangle%s(k)%rake*DEG2RAD     

             ! check range of vertex index
             IF (in%triangle%i1(k) .GT. maxVertexIndex) THEN
                maxVertexIndex=in%triangle%i1(k)
             END IF
             IF (in%triangle%i2(k) .GT. maxVertexIndex) THEN
                maxVertexIndex=in%triangle%i2(k)
             END IF
             IF (in%triangle%i3(k) .GT. maxVertexIndex) THEN
                maxVertexIndex=in%triangle%i3(k)
             END IF
             IF ((0 .GT. in%triangle%i1(k)) .OR. &
                 (0 .GT. in%triangle%i2(k)) .OR. &
                 (0 .GT. in%triangle%i3(k))) THEN
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: negative index")')
                STOP 1
             END IF

             IF (i .NE. k) THEN
                WRITE (STDERR,'("error in input file: unexpected index")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("invalid triangle patch definition ")')
                STOP 1
             END IF
          END DO
                
          PRINT '("# number of vertices")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) in%triangle%nVe
          PRINT '(I5)', in%triangle%nVe
          IF (maxVertexIndex .GT. in%triangle%nVe) THEN
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE (STDERR,'("error in input file: not enough vertices")')
             STOP 1
          END IF
          IF (in%triangle%nVe .GT. 0) THEN
             ALLOCATE(in%triangle%v(3,in%triangle%nVe),STAT=ierr)
             IF (ierr>0) STOP "could not allocate the list of vertices"
             PRINT 2000
             PRINT '("#    n      x1       x2       x3")'
             PRINT 2000
             DO k=1,in%triangle%nVe
                CALL getdata(iunit,dataline)
                READ (dataline,*,IOSTAT=ierr) i, &
                      in%triangle%v(1,k), &
                      in%triangle%v(2,k), &
                      in%triangle%v(3,k)
   
                PRINT '(I3.3,3ES9.2E1)',i, &
                     in%triangle%v(1,k), &
                     in%triangle%v(2,k), &
                     in%triangle%v(3,k)
                
                IF (i .NE. k) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("invalid vertex definition ")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   WRITE (STDERR,'("error in input file: unexpected index")')
                   STOP 1
                END IF
             END DO
                
             ! area of triangle elements
             DO k=1,in%triangle%ns
                ! area
                CALL cross( &
                       in%triangle%v(:,in%triangle%i1(k))-in%triangle%v(:,in%triangle%i2(k)), &
                       in%triangle%v(:,in%triangle%i1(k))-in%triangle%v(:,in%triangle%i3(k)), &
                       normal)
                in%triangle%area(k)=NORM2(normal)/2
             END DO

             ! export the triangle patches
             filename=TRIM(in%wdir)//"/tfaults.tri"
             OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
             IF (ierr>0) THEN
                WRITE_DEBUG_INFO(102)
                WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
                STOP 1
             END IF
             WRITE (FPOUT,'("#    n       Vl        i1        i2        i3   rake")')
             DO k=1,in%triangle%ns
                WRITE (FPOUT,'(I6,ES9.2E1,3I10,f7.1)') k, &
                  in%triangle%s(k)%Vl, &
                  in%triangle%i1(k), &
                  in%triangle%i2(k), &
                  in%triangle%i3(k), &
                  in%triangle%s(k)%rake
             END DO
             CLOSE(FPOUT)
                
             filename=TRIM(in%wdir)//"/tfaults.ned"
             OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
             IF (ierr>0) THEN
                WRITE_DEBUG_INFO(102)
                WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
                STOP 1
             END IF
             WRITE (FPOUT,'("#    n      x1       x2       x3")')
             DO k=1,in%triangle%ns
                WRITE (FPOUT,'(I3.3,3ES9.2E1)') k, &
                     in%triangle%v(1,k), &
                     in%triangle%v(2,k), &
                     in%triangle%v(3,k)
             END DO
             CLOSE(FPOUT)
                
          END IF
   
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          !        F R I C T I O N   P R O P E R T I E S
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          PRINT 2000
          PRINT '("# number of frictional triangle patches")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) dummy
          PRINT '(I5)', dummy
          IF (dummy .NE. in%triangle%ns) THEN
             WRITE_DEBUG_INFO(-1)
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE(STDERR,'("input error: all triangle patches require frictional properties")')
             STOP 2
          END IF

          SELECT CASE(frictionLawType)
          CASE(1:3)
             PRINT '("#    n     tau0      mu0      sig        a        b        L       Vo    2G/Vs")'
             PRINT 2000
             DO k=1,in%triangle%ns
                CALL getdata(iunit,dataline)
                READ (dataline,*,IOSTAT=ierr) i, &
                      in%triangle%s(k)%tau0, &
                      in%triangle%s(k)%mu0, &
                      in%triangle%s(k)%sig, &
                      in%triangle%s(k)%a, &
                      in%triangle%s(k)%b, &
                      in%triangle%s(k)%L, &
                      in%triangle%s(k)%Vo, &
                      in%triangle%s(k)%damping
   
                 PRINT '(I6,8ES9.2E1)',i, &
                     in%triangle%s(k)%tau0, &
                     in%triangle%s(k)%mu0, &
                     in%triangle%s(k)%sig, &
                     in%triangle%s(k)%a, &
                     in%triangle%s(k)%b, &
                     in%triangle%s(k)%L, &
                     in%triangle%s(k)%Vo, &
                     in%triangle%s(k)%damping
                
                IF (i .NE. k) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("invalid friction property definition for triangle patch")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   WRITE (STDERR,'("error in input file: unexpected index")')
                   STOP 1
                END IF
             END DO

          CASE(4)
             PRINT '("#    n     tau0      mu0      sig        a        b        L       V1       V2    2G/Vs")'
             PRINT 2000
             DO k=1,in%triangle%ns
                CALL getdata(iunit,dataline)
                READ (dataline,*,IOSTAT=ierr) i, &
                      in%triangle%s(k)%tau0, &
                      in%triangle%s(k)%mu0, &
                      in%triangle%s(k)%sig, &
                      in%triangle%s(k)%a, &
                      in%triangle%s(k)%b, &
                      in%triangle%s(k)%L, &
                      in%triangle%s(k)%Vo, &
                      in%triangle%s(k)%V2, &
                      in%triangle%s(k)%damping
   
                 PRINT '(I6,9ES9.2E1)',i, &
                     in%triangle%s(k)%tau0, &
                     in%triangle%s(k)%mu0, &
                     in%triangle%s(k)%sig, &
                     in%triangle%s(k)%a, &
                     in%triangle%s(k)%b, &
                     in%triangle%s(k)%L, &
                     in%triangle%s(k)%Vo, &
                     in%triangle%s(k)%V2, &
                     in%triangle%s(k)%damping
                
                IF (i .NE. k) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("invalid friction property definition for triangle patch")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   WRITE (STDERR,'("error in input file: unexpected index")')
                   STOP 1
                END IF
             END DO

          CASE DEFAULT
             WRITE (0,'("unhandled option ", a, " (this is a bug)")') frictionLawType
             WRITE_DEBUG_INFO(100)
             STOP 3
          END SELECT
   
       END IF
          
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !       O B S E R V A T I O N   P A T C H E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of observation patches")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%nObservationState
       PRINT '(I5)', in%nObservationState
       IF (0 .LT. in%nObservationState) THEN
          ALLOCATE(in%observationState(3,in%nObservationState),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the observation patches"
          PRINT 2000
          PRINT '("#   n      i rate")'
          PRINT 2000
          DO k=1,in%nObservationState
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i,in%observationState(1:2,k)

             PRINT '(I5,X,I6,X,I4)', i, in%observationState(1:2,k)

             IF (0 .GE. in%observationState(2,k)) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: invalid subsampling rate")')
                STOP 1
             END IF

             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO
       END IF

#ifdef NETCDF
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !       O B S E R V A T I O N   P R O F I L E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of observation profiles")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%nObservationProfiles
       PRINT '(I5)', in%nObservationProfiles
       IF (0 .LT. in%nObservationProfiles) THEN

          ALLOCATE(in%observationProfileVelocity(in%nObservationProfiles),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the velocity observation profiles"

          IF (in%isExportSlip) THEN
             ALLOCATE(in%observationProfileSlip(in%nObservationProfiles),STAT=ierr)
             IF (ierr>0) STOP "could not allocate the slip observation profiles"
          END IF

          IF (in%isExportStress) THEN
             ALLOCATE(in%observationProfileStress(in%nObservationProfiles),STAT=ierr)
             IF (ierr>0) STOP "could not allocate the stress observation profiles"
          END IF

          PRINT 2000
          PRINT '("#   n    n offset stride rate")'
          PRINT 2000

          DO k=1,in%nObservationProfiles
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                     in%observationProfileVelocity(k)%n, &
                     in%observationProfileVelocity(k)%offset, &
                     in%observationProfileVelocity(k)%stride, &
                     in%observationProfileVelocity(k)%rate

             PRINT '(I5,X,I4,X,I6,X,I6,X,I4)', i, &
                     in%observationProfileVelocity(k)%n, &
                     in%observationProfileVelocity(k)%offset, &
                     in%observationProfileVelocity(k)%stride, &
                     in%observationProfileVelocity(k)%rate

             IF (0 .GE. in%observationProfileVelocity(k)%n .OR. &
                 0 .GE. in%observationProfileVelocity(k)%stride .OR. &
                 0 .GE. in%observationProfileVelocity(k)%rate) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: invalid profile declaration")')
                STOP 1
             END IF

             IF (0 .GT. in%observationProfileVelocity(k)%offset) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: invalid offset in profile declaration")')
                STOP 1
             END IF

             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF

             IF (in%isExportSlip) THEN
                in%observationProfileSlip(k)%n     =in%observationProfileVelocity(k)%n
                in%observationProfileSlip(k)%offset=in%observationProfileVelocity(k)%offset
                in%observationProfileSlip(k)%stride=in%observationProfileVelocity(k)%stride
                in%observationProfileSlip(k)%rate  =in%observationProfileVelocity(k)%rate
             END IF

             IF (in%isExportStress) THEN
                in%observationProfileStress(k)%n     =in%observationProfileVelocity(k)%n
                in%observationProfileStress(k)%offset=in%observationProfileVelocity(k)%offset
                in%observationProfileStress(k)%stride=in%observationProfileVelocity(k)%stride
                in%observationProfileStress(k)%rate  =in%observationProfileVelocity(k)%rate
             END IF
          END DO
       END IF

       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !       O B S E R V A T I O N   I M A G E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of observation images")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%nObservationImages
       PRINT '(I5)', in%nObservationImages
       IF (0 .LT. in%nObservationImages) THEN
          ALLOCATE(in%observationImageVelocity(in%nObservationImages),STAT=ierr)
          ALLOCATE(in%observationImageVelocityStrike(in%nObservationImages),STAT=ierr)
          ALLOCATE(in%observationImageVelocityDip(in%nObservationImages),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the observation profiles"
          PRINT 2000
          PRINT '("#   n   nl   nw offset stride rate")'
          PRINT 2000
          DO k=1,in%nObservationImages
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                     in%observationImageVelocity(k)%nl, &
                     in%observationImageVelocity(k)%nw, &
                     in%observationImageVelocity(k)%offset, &
                     in%observationImageVelocity(k)%stride, &
                     in%observationImageVelocity(k)%rate

             PRINT '(I5,X,I4,X,I4,X,I6,X,I6,X,I4)', i, &
                     in%observationImageVelocity(k)%nl, &
                     in%observationImageVelocity(k)%nw, &
                     in%observationImageVelocity(k)%offset, &
                     in%observationImageVelocity(k)%stride, &
                     in%observationImageVelocity(k)%rate

             IF (0 .GE. in%observationImageVelocity(k)%nl .OR. &
                 0 .GE. in%observationImageVelocity(k)%nw .OR. &
                 0 .GE. in%observationImageVelocity(k)%stride .OR. &
                 0 .GE. in%observationImageVelocity(k)%rate) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: invalid image declaration")')
                STOP 1
             END IF

             IF (0 .GT. in%observationImageVelocity(k)%offset) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: invalid offset for image declaration")')
                STOP 1
             END IF

             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF

             in%observationImageVelocityStrike=in%observationImageVelocity
             in%observationImageVelocityDip=in%observationImageVelocity
          END DO
       END IF
#endif

       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !        O B S E R V A T I O N   P O I N T S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of observation points")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%nObservationPoint
       PRINT '(I5)', in%nObservationPoint
       IF (0 .LT. in%nObservationPoint) THEN
          ALLOCATE(in%observationPoint(in%nObservationPoint),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the observation points"
          PRINT 2000
          PRINT '("# n       name       x1       x2       x3")'
          PRINT 2000
          DO k=1,in%nObservationPoint
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                     in%observationPoint(k)%name, &
                     in%observationPoint(k)%x(1), &
                     in%observationPoint(k)%x(2), &
                     in%observationPoint(k)%x(3)

             PRINT '(I3.3,X,a20,3ES9.2E1)',i, &
                     in%observationPoint(k)%name, &
                     in%observationPoint(k)%x(1), &
                     in%observationPoint(k)%x(2), &
                     in%observationPoint(k)%x(3)

             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO

          ! export list of observation points
          filename=TRIM(in%wdir)//"/opts.dat"
          OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          WRITE (FPOUT,'("# n       name       x1       x2       x3")')
          DO k=1,in%nObservationPoint
             WRITE (FPOUT,'(I3.3,X,a20,3ES9.2E1)') k, &
                     in%observationPoint(k)%name, &
                     in%observationPoint(k)%x(1), &
                     in%observationPoint(k)%x(2), &
                     in%observationPoint(k)%x(3)
          END DO
          CLOSE(FPOUT)

       END IF

       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !                  E V E N T S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT '("# number of events")'
       CALL getdata(iunit,dataline)
       READ (dataline,*) in%ne
       PRINT '(I5)', in%ne
       IF (in%ne .GT. 0) ALLOCATE(in%event(in%ne),STAT=ierr)
       IF (ierr>0) STOP "could not allocate the event list"
       
       DO i=1,in%ne
          IF (1 .NE. i) THEN
             PRINT '("# time of next event")'
             CALL getdata(iunit,dataline)
             READ (dataline,*) in%event(i)%time
             in%event(i)%i=i-1
             PRINT '(ES9.2E1)', in%event(i)%time
   
             IF (in%event(i)%time .LE. in%event(i-1)%time) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'(a,a)') "input file error. ", &
                     "timing of perturbations must increase, quiting."
                STOP 1
             END IF
          ELSE
             in%event(1)%time=0._8
             in%event(1)%i=0
          END IF
   
       END DO
   
       ! test the presence of dislocations for coseismic calculation
       IF ((in%rectangle%ns .EQ. 0) .AND. &
           (in%triangle%ns .EQ. 0) .OR. &
           (in%interval .LE. 0._8)) THEN
   
          WRITE_DEBUG_INFO(300)
          WRITE (STDERR,'("nothing to do. exiting.")')
          STOP 1
       END IF
   
       PRINT 2000
       ! flush standard output
       CALL FLUSH(6)      

       in%nPatch=in%rectangle%ns+in%triangle%ns

       position=0
       CALL MPI_PACK(in%interval,            1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%lambda,              1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%mu,                  1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nu,                  1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%rectangle%ns,        1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%triangle%ns,         1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%triangle%nVe,        1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%ne,                  1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nObservationState,   1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nObservationProfiles,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nObservationPoint,   1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)

       position=0
       CALL MPI_PACK(in%wdir,256,MPI_CHARACTER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)

       ! send the rectangle patches (geometry and friction properties) 
       DO i=1,in%rectangle%ns
          position=0
          CALL MPI_PACK(in%rectangle%s(i)%Vl,     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%x(1,i),      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%x(2,i),      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%x(3,i),      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%length(i),   1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%width(i),    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%strike(i),   1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%dip(i),      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%rake,   1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%opening,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%tau0,   1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%mu0,    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%sig,    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%a,      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%b,      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%L,      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%Vo,     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%V2,     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%damping,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the triangle patches (geometry and friction properties) 
       DO i=1,in%triangle%ns
          position=0
          CALL MPI_PACK(in%triangle%s(i)%Vl,     1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%i1(i),       1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%i2(i),       1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%i3(i),       1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%area(i),     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%rake,   1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%opening,1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%tau0,   1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%mu0,    1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%sig,    1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%a,      1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%b,      1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%L,      1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%Vo,     1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%V2,     1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%damping,1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the triangle vertices
       IF (0 .NE. in%triangle%ns) THEN
          DO i=1,in%triangle%nVe
             position=0
             CALL MPI_PACK(in%triangle%v(1,i),1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%triangle%v(2,i),1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%triangle%v(3,i),1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          END DO
       END IF

       ! send the observation state
       DO i=1,in%nObservationState
          position=0
          CALL MPI_PACK(in%observationState(:,i),3,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       IF (in%isExportSlip) THEN
          DO i=1,in%nObservationProfiles
             position=0
             CALL MPI_PACK(in%observationProfileSlip(i)%n,     1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%observationProfileSlip(i)%offset,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%observationProfileSlip(i)%stride,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%observationProfileSlip(i)%rate,  1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          END DO
       END IF

       IF (in%isExportStress) THEN
          DO i=1,in%nObservationProfiles
             position=0
             CALL MPI_PACK(in%observationProfileStress(i)%n,     1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%observationProfileStress(i)%offset,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%observationProfileStress(i)%stride,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%observationProfileStress(i)%rate,  1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          END DO
       END IF

       ! send the observation points
       DO i=1,in%nObservationPoint
          position=0
          CALL MPI_PACK(in%observationPoint(i)%name,20,MPI_CHARACTER,packed,psize,position,MPI_COMM_WORLD,ierr)
          DO k=1,3
             CALL MPI_PACK(in%observationPoint(i)%x(k),1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          END DO
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the perturbation events
       DO k=1,in%ne
          CALL MPI_PACK(in%event(k)%time,1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%event(k)%i,   1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

    ELSE ! if 0.NE.rank

       !------------------------------------------------------------------
       ! S L A V E S
       !------------------------------------------------------------------

       position=0
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%interval,            1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%lambda,              1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%mu,                  1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nu,                  1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%rectangle%ns,        1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%triangle%ns,         1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%triangle%nVe,        1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%ne,                  1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nObservationState,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nObservationProfiles,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nObservationPoint,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)

       position=0
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%wdir,256,MPI_CHARACTER,MPI_COMM_WORLD,ierr)

       IF (0 .LT. in%rectangle%ns) &
                    ALLOCATE(in%rectangle%s(in%rectangle%ns), &
                             in%rectangle%x(3,in%rectangle%ns), &
                             in%rectangle%xc(3,in%rectangle%ns), &
                             in%rectangle%length(in%rectangle%ns), &
                             in%rectangle%width(in%rectangle%ns), &
                             in%rectangle%strike(in%rectangle%ns), &
                             in%rectangle%dip(in%rectangle%ns), &
                             in%rectangle%sv(3,in%rectangle%ns), &
                             in%rectangle%dv(3,in%rectangle%ns), &
                             in%rectangle%nv(3,in%rectangle%ns), &
                             STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory"

       IF (0 .LT. in%triangle%ns) &
                    ALLOCATE(in%triangle%s(in%triangle%ns), &
                             in%triangle%xc(3,in%triangle%ns), &
                             in%triangle%i1(in%triangle%ns), &
                             in%triangle%i2(in%triangle%ns), &
                             in%triangle%i3(in%triangle%ns), &
                             in%triangle%area(in%triangle%ns), &
                             in%triangle%sv(3,in%triangle%ns), &
                             in%triangle%dv(3,in%triangle%ns), &
                             in%triangle%nv(3,in%triangle%ns),STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory"

       IF (0 .LT. in%triangle%nVe) ALLOCATE(in%triangle%v(3,in%triangle%nVe), STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory"

       IF (0 .LT. in%ne) ALLOCATE(in%event(in%ne),STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory"

       DO i=1,in%rectangle%ns
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%Vl,     1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%x(1,i),      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%x(2,i),      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%x(3,i),      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%length(i),   1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%width(i),    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%strike(i),   1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%dip(i),      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%rake,   1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%opening,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%tau0,   1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%mu0,    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%sig,    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%a,      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%b,      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%L,      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%Vo,     1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%V2,     1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%damping,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
       END DO

       DO i=1,in%triangle%ns
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%Vl,     1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%i1(i),       1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%i2(i),       1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%i3(i),       1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%area(i),     1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%rake,   1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%opening,1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%tau0,   1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%mu0,    1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%sig,    1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%a,      1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%b,      1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%L,      1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%Vo,     1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%V2,     1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%damping,1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       END DO

       IF (0 .NE. in%triangle%ns) THEN
          DO i=1,in%triangle%nVe
             position=0
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%triangle%v(1,i),1,MPI_REAL8,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%triangle%v(2,i),1,MPI_REAL8,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%triangle%v(3,i),1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          END DO
       END IF

       IF (0 .LT. in%nObservationState) &
                    ALLOCATE(in%observationState(3,in%nObservationState),STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for observation states"

       DO i=1,in%nObservationState
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%observationState(:,i),3,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       END DO

       IF (in%isExportSlip) THEN
          ALLOCATE(in%observationProfileSlip(in%nObservationProfiles),STAT=ierr)
          IF (ierr>0) STOP "slave could not allocate memory for slip observation profiles"
          DO i=1,in%nObservationProfiles
             position=0
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%observationProfileSlip(i)%n,     1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%observationProfileSlip(i)%offset,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%observationProfileSlip(i)%stride,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%observationProfileSlip(i)%rate,  1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          END DO
       END IF

       IF (in%isExportStress) THEN
          ALLOCATE(in%observationProfileStress(in%nObservationProfiles),STAT=ierr)
          IF (ierr>0) STOP "slave could not allocate memory for stress observation profiles"
          DO i=1,in%nObservationProfiles
             position=0
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%observationProfileStress(i)%n,     1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%observationProfileStress(i)%offset,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%observationProfileStress(i)%stride,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%observationProfileStress(i)%rate,  1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          END DO
       END IF

       IF (0 .LT. in%nObservationPoint) &
                    ALLOCATE(in%observationPoint(in%nObservationPoint), &
                             STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for observation points"

       DO i=1,in%nObservationPoint
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%observationPoint(i)%name,20,MPI_CHARACTER,MPI_COMM_WORLD,ierr)
          DO k=1,3
             CALL MPI_UNPACK(packed,psize,position,in%observationPoint(i)%x(k),1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          END DO
       END DO

       DO i=1,in%ne
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%event(i)%time,1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%event(i)%i,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       END DO

       CALL FLUSH(6)      

       in%nPatch=in%rectangle%ns+in%triangle%ns

    END IF ! master or slaves

2000 FORMAT ("# ----------------------------------------------------------------------------")
   
  END SUBROUTINE init
   
  !-----------------------------------------------
  !> subroutine cross
  !! compute the cross product between two vectors
  !-----------------------------------------------
  SUBROUTINE cross(u,v,w)
    REAL*8, DIMENSION(3), INTENT(IN) :: u,v
    REAL*8, DIMENSION(3), INTENT(OUT) :: w

    w(1)=u(2)*v(3)-u(3)*v(2)
    w(2)=u(3)*v(1)-u(1)*v(3)
    w(3)=u(1)*v(2)-u(2)*v(1)

  END SUBROUTINE cross

  !-----------------------------------------------
  !> subroutine printhelp
  !! displays a help message with master thread.
  !-----------------------------------------------
  SUBROUTINE printhelp()

    INTEGER :: rank,size,ierr

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,size,ierr)

    IF (0.EQ.rank) THEN
       PRINT '("usage:")'
       PRINT '("")'
       PRINT '("mpirun -n 2 unicycle-3d-ratestate [-h] [--dry-run] [--help] [--epsilon 1e-6] [filename]")'
       PRINT '("                                  [--friction-law i] [--evolution-law i] [--export-netcdf]")'
       PRINT '("                                  [--import-state wdir] [--export-state] [--maximum-step int]")'
       PRINT '("                                  [--export-greens] [--import-greens wdir] [--maximum-iterations int]")'
       PRINT '("")'
       PRINT '("options:")'
       PRINT '("   -h                           prints this message and aborts calculation")'
       PRINT '("   --dry-run                    read input, export geometry, and abort further calculation")'
       PRINT '("   --help                       prints this message and aborts calculation")'
       PRINT '("   --version                    print version number and exit")'
       PRINT '("   --epsilon                    set the numerical accuracy [1E-6]")'
       PRINT '("   --friction-law               type of friction law [1]")'
       PRINT '("       1: multiplicative        form of rate-state friction (Barbot, 2019)")'
       PRINT '("       2: additive              form of rate-state friction (Ruina, 1983)")'
       PRINT '("       3: arcsinh               form of rate-state friction (Rice & Benzion, 1996)")'
       PRINT '("       4: cut-off velocity      form of rate-state friction (Okubo, 1989)")'
       PRINT '("   --evolution-law              type of evolution law [1]")'
       PRINT '("       1: aging law             isothermal, isobaric additive form (Ruina, 1983)")'
       PRINT '("       2: slip law              isothermal, isobaric multiplicative form (Ruina, 1983)")'
       PRINT '("   --export-greens wdir         export the Greens function to file")'
       PRINT '("   --export-netcdf              export results to GMT-compatible netcdf files")'
       PRINT '("   --export-slip                export slip profiles to GMT-compatible netcdf files")'
       PRINT '("   --export-stress              export stress profiles to GMT-compatible netcdf files")'
       PRINT '("   --export-vtp                 export results to Paraview-compatible .vtp files")'
       PRINT '("   --export-slip-distribution r export slip distribution every r time steps")'
       PRINT '("   --import-greens wdir         import the Greens function from file")'
       PRINT '("   --import-state wdir          import the state vector from file in wdir")'
       PRINT '("   --maximum-iterations         set the maximum time step [1000000]")'
       PRINT '("   --maximum-step               set the maximum time step [none]")'
       PRINT '("   --verbose i                  set the verbosity [2]")'
       PRINT '("")'
       PRINT '("description:")'
       PRINT '("   simulates elasto-dynamics on faults in three dimensions")'
       PRINT '("   following the radiation-damping approximation")'
       PRINT '("   using the integral method.")'
       PRINT '("")'
       PRINT '("see also: ""man unicycle""")'
       PRINT '("")'
       PRINT '("           Uni-,,..__sCycle        ")'
       PRINT '("          =QWWWQUAAAKEWW@?`        ")'
       PRINT '("           ?9RWYESSWUT?^           ")'
       PRINT '("                 Q;                ")'
       PRINT '("                .C;                ")'
       PRINT '("                 O;                ")'
       PRINT '("                 M;                ")'
       PRINT '("                 P;                ")'
       PRINT '("                .U;                ")'
       PRINT '("                 T;                ")'
       PRINT '("                 E;                ")'
       PRINT '("                 Q;                ")'
       PRINT '("                 Q;                ")'
       PRINT '("                 Q;                ")'
       PRINT '("               ._Q;_.              ")'
       PRINT '("          _awgQWRRRWWmya,.         ")'
       PRINT '("       .amWT!^-  Q` -~<9Wmw,       ")'
       PRINT '("     .aQD>`     .W;      -9Qw,     ")'
       PRINT '("    _m@(         W;        <$mc    ")'
       PRINT '("   _QD`          W;          4Wc   ")'
       PRINT '("  _QD`           T;           4Wc  ")'
       PRINT '("  dQ`            T;   ___-__   $Q. ")'
       PRINT '(" _QF             T;  .oU!/     ]W[ ")'
       PRINT '(" ]Q[             T;_u!`        -Qk ")'
       PRINT '(" ]Q(            <O!^.           Qk ")'
       PRINT '(" ]Q[         _u?^              .Qk ")'
       PRINT '(" -Qk      _a?.                 ]Q[ ")'
       PRINT '("  4Q, -!!?T!-                 .m@  ")'
       PRINT '("  -Qm,                        jQ(  ")'
       PRINT '("   -Qm,                     .wQ[   ")'
       PRINT '("    -$Qc                   _m@^    ")'
       PRINT '("      ?$ma.              <yQP`     ")'
       PRINT '("        ?VQwa,,.   .__awQ@?`       ")'
       PRINT '("          -??$QRRRWQWBT!~          ")'
       PRINT '("                ---                ")'
       PRINT '("")'
       CALL FLUSH(6)
    END IF

  END SUBROUTINE printhelp

  !-----------------------------------------------
  !> subroutine printversion
  !! displays code version with master thread.
  !-----------------------------------------------
  SUBROUTINE printversion()

    INTEGER :: rank,size,ierr

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,size,ierr)

    IF (0.EQ.rank) THEN
       PRINT '("unicycle-3d-ratestate version 1.0.0, compiled on ",a)', __DATE__
       PRINT '("")'
       CALL FLUSH(6)
    END IF

  END SUBROUTINE printversion

END PROGRAM ratestate




