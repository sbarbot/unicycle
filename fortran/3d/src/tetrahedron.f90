!-----------------------------------------------------------------------
! Copyright (c) 2022-2025 Sylvain Barbot
!
! This file is part of UNICYCLE
!
! UNICYCLE is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! UNICYCLE is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with UNICYCLE.  If not, see <http://www.gnu.org/licenses/>.
!-----------------------------------------------------------------------

#include "macros.h90"

MODULE tetrahedron

  IMPLICIT NONE

  PUBLIC

CONTAINS

  !------------------------------------------------------------------------
  !> subroutine computeReferenceSystemTetrahedron
  !! computes the center position and local reference system tied to the volume
  !!
  !! INPUT:
  !! @param ns
  !! @param x           - upper left coordinate of fault patch (north, east, down)
  !! @param strike      - strike angle
  !! @param L,W         - length and width of the rectangular dislocation
  !!
  !! OUTPUT:
  !! @param sv,dv,nv    - strike, dip and normal vectors of the fault patch
  !! @param xc          - coordinates (north, east, down) of the center
  !!
  !! \author Sylvain Barbot (21/02/17) - original fortran form
  !------------------------------------------------------------------------
  SUBROUTINE computeReferenceSystemTetrahedron(ns,i1,i2,i3,i4,nve,v,sv,dv,nv,xc)
    INTEGER, INTENT(IN) :: ns
    INTEGER, DIMENSION(ns), INTENT(IN) :: i1,i2,i3,i4
    INTEGER, INTENT(IN) :: nve
    REAL*8, DIMENSION(3,nve), INTENT(IN) :: v
    REAL*8, DIMENSION(3,ns), INTENT(OUT) :: sv,dv,nv,xc

    ! counter
    INTEGER :: k

    ! unit vectors in the strike direction
    sv(1,1:ns)=1._8
    sv(2,1:ns)=0._8
    sv(3,1:ns)=0._8
            
    ! unit vectors in the dip direction
    dv(1,1:ns)=0._8
    dv(2,1:ns)=0._8
    dv(3,1:ns)=-1._8
            
    ! unit vectors in the normal direction
    nv(1,1:ns)=0._8
    nv(2,1:ns)=1._8
    nv(3,1:ns)=0._8
            
    ! center of fault patch
    DO k=1,ns
       xc(:,k)=(v(:,i1(k))+v(:,i2(k))+v(:,i3(k))+v(:,i4(k)))/4._8
    END DO
                
                
  END SUBROUTINE computeReferenceSystemTetrahedron

  !---------------------------------------------------------------
  !> function y
  !! parameterized volume integral over a tetrahedron
  !---------------------------------------------------------------
  REAL*8 FUNCTION tmap(u,v,w,A,B,C,D)
    REAL*8, INTENT(IN) :: u,v,w,A,B,C,D

    tmap=A*(1-w)*(1-v)*(1-u)/8+B*(1-w)*(1+u)/4+C*(1-w)*(1+v)*(1-u)/8+D*(1+w)/2

  END FUNCTION tmap

  !------------------------------------------------------------------------
  !> subroutine computeTractionKernelsTetrahedron
  !! calculates the traction kernels associated a deforming tetrahedron
  !! in an elastic half-space using the analytic solution of
  !!
  !!   Barbot S., "Deformation of a Half-Space from Anelastic Strain Confined
  !!   in a Tetrahedral Volume", Bull. Seism. Soc. Am., 10.1785/0120180058, 2018.
  !!
  !! INPUT:
  !! @param x1,x2,x3    - coordinates of observation points
  !! @param sv,dv,nv    - strike, dip and normal vector of observation points
  !! @param ns          - number of tetrahedron volume elements
  !! @param i1,i2,i3,i4 - index of tetrahedron vertices
  !! @param ns          - number of tetrahedron vertices
  !! @param v           - vertex coordinates
  !! @param e11p,e12p
  !!        e13p,e22p
  !!        e23p,e33p  - strain components in the tetrahedron
  !! @param G,lambda   - rigidity and Lame parameter
  !!
  !! OUTPUT:
  !! ts,td,tn          - traction in the strike, dip and normal directions
  !!
  !! \author Sylvain Barbot (06/08/22) - Tokyo, Japan
  !------------------------------------------------------------------------
  SUBROUTINE computeTractionKernelsTetrahedron( &
                         x,sv,dv,nv, &
                         ns,i1,i2,i3,i4,nve,v, &
                         e11p,e12p,e13p,e22p,e23p,e33p,G,lambda, &
                         ts,td,tn)
    REAL*8, DIMENSION(3), INTENT(IN) :: x
    REAL*8, DIMENSION(3), INTENT(IN) :: sv,dv,nv
    INTEGER, INTENT(IN) :: ns
    INTEGER, DIMENSION(ns), INTENT(IN) :: i1,i2,i3,i4
    INTEGER, INTENT(IN) :: nve
    REAL*8, DIMENSION(3,nve), INTENT(IN) :: v
    REAL*8, INTENT(IN) :: e11p,e12p,e13p,e22p,e23p,e33p
    REAL*8, INTENT(IN) :: G,lambda
    REAL*8, DIMENSION(ns), INTENT(OUT) :: ts,td,tn

    INTEGER :: k
    REAL*8 :: s11,s12,s13,s22,s23,s33
    REAL*8 :: nu

    ! Poisson's ratio
    nu=lambda/(lambda+G)/2._8

    DO k=1,ns
       CALL computeStressTetrahedronGauss( &
              x(1),x(2),x(3), &
              v(:,i1(k)),v(:,i2(k)),v(:,i3(k)),v(:,i4(k)), &
              e11p,e12p,e13p,e22p,e23p,e33p,G,nu, &
              s11,s12,s13,s22,s23,s33)

       ! rotate to receiver system of coordinates
       ts(k)= ( nv(1)*s11+nv(2)*s12+nv(3)*s13 )*sv(1) &
             +( nv(1)*s12+nv(2)*s22+nv(3)*s23 )*sv(2) &
             +( nv(1)*s13+nv(2)*s23+nv(3)*s33 )*sv(3)
       td(k)= ( nv(1)*s11+nv(2)*s12+nv(3)*s13 )*dv(1) &
             +( nv(1)*s12+nv(2)*s22+nv(3)*s23 )*dv(2) &
             +( nv(1)*s13+nv(2)*s23+nv(3)*s33 )*dv(3)
       tn(k)= ( nv(1)*s11+nv(2)*s12+nv(3)*s13 )*nv(1) &
             +( nv(1)*s12+nv(2)*s22+nv(3)*s23 )*nv(2) &
             +( nv(1)*s13+nv(2)*s23+nv(3)*s33 )*nv(3)

    END DO

  END SUBROUTINE computeTractionKernelsTetrahedron

  !------------------------------------------------------------------------
  !> subroutine computeStressKernelsTetrahedron
  !! calculates the traction kernels associated with strain in finite
  !! tetrahedron volumes in an elastic half-space, following
  !!
  !!   Barbot S., "Deformation of a Half-Space from Anelastic Strain Confined
  !!   in a Tetrahedral Volume", Bull. Seism. Soc. Am., 10.1785/0120180058, 2018.
  !!
  !! INPUT:
  !! @param x1,x2,x3    - coordinates of observation points
  !! @param sv,dv,nv    - strike, dip and normal vector of observation points
  !! @param ns          - number of tetrahedron volume elements
  !! @param i1,i2,i3,i4 - index of tetrahedron vertices
  !! @param ns          - number of tetrahedron vertices
  !! @param v           - vertex coordinates
  !! @param e11p,e12p
  !!        e13p,e22p
  !!        e23p,e33p   - strain components in the primed reference system
  !!                      tied to the cuboid
  !! @param G,lambda    - rigidity and Lame parameter
  !!
  !! OUTPUT:
  !! s11,s12,s13,s22,s23,s33 - the stress components in the reference
  !!                           system tied to the volume element.
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeStressKernelsTetrahedron( &
                         p,x,w,sv,dv,nv, &
                         ns,i1,i2,i3,i4,nve,v, &
                         e11p,e12p,e13p,e22p,e23p,e33p,G,lambda, &
                         s11,s12,s13,s22,s23,s33)
    INTEGER, INTENT(IN) :: p
    REAL*8, DIMENSION(3,p), INTENT(IN) :: x
    REAL*8, DIMENSION(p), INTENT(IN) :: w
    REAL*8, DIMENSION(3), INTENT(IN) :: sv,dv,nv
    INTEGER, INTENT(IN) :: ns
    INTEGER, DIMENSION(ns), INTENT(IN) :: i1,i2,i3,i4
    INTEGER, INTENT(IN) :: nve
    REAL*8, DIMENSION(3,nve), INTENT(IN) :: v
    REAL*8, INTENT(IN) :: e11p,e12p,e13p,e22p,e23p,e33p
    REAL*8, INTENT(IN) :: G,lambda
    REAL*8, DIMENSION(ns), INTENT(OUT) :: s11,s12,s13,s22,s23,s33

    INTEGER :: i,j
    REAL*8 :: s11p,s12p,s13p,s22p,s23p,s33p
    REAL*8 :: nu

    ! Poisson's ratio
    nu=lambda/(lambda+G)/2._8

    DO i=1,ns
       ! initialize integral
       s11p=0._8
       s12p=0._8
       s13p=0._8
       s22p=0._8
       s23p=0._8
       s33p=0._8
       ! integrate over volume
       DO j=1,p
          CALL computeStressTetrahedronSourceImageGauss( &
                 x(1,j),x(2,j),x(3,j), &
                 v(:,i1(i)),v(:,i2(i)),v(:,i3(i)),v(:,i4(i)), &
                 e11p,e12p,e13p,e22p,e23p,e33p,G,nu, &
                 s11(i),s12(i),s13(i),s22(i),s23(i),s33(i))

          ! rotate to receiver system of coordinates
          s11p=s11p+w(j)*( ( sv(1)*s11(i)+sv(2)*s12(i)+sv(3)*s13(i) )*sv(1) &
                          +( sv(1)*s12(i)+sv(2)*s22(i)+sv(3)*s23(i) )*sv(2) &
                          +( sv(1)*s13(i)+sv(2)*s23(i)+sv(3)*s33(i) )*sv(3) )
          s12p=s12p+w(j)*( ( sv(1)*s11(i)+sv(2)*s12(i)+sv(3)*s13(i) )*nv(1) &
                          +( sv(1)*s12(i)+sv(2)*s22(i)+sv(3)*s23(i) )*nv(2) &
                          +( sv(1)*s13(i)+sv(2)*s23(i)+sv(3)*s33(i) )*nv(3) )
          s13p=s13p+w(j)*(-( sv(1)*s11(i)+sv(2)*s12(i)+sv(3)*s13(i) )*dv(1) &
                          -( sv(1)*s12(i)+sv(2)*s22(i)+sv(3)*s23(i) )*dv(2) &
                          -( sv(1)*s13(i)+sv(2)*s23(i)+sv(3)*s33(i) )*dv(3) )
          s22p=s22p+w(j)*( ( nv(1)*s11(i)+nv(2)*s12(i)+nv(3)*s13(i) )*nv(1) &
                          +( nv(1)*s12(i)+nv(2)*s22(i)+nv(3)*s23(i) )*nv(2) &
                          +( nv(1)*s13(i)+nv(2)*s23(i)+nv(3)*s33(i) )*nv(3) )
          s23p=s23p+w(j)*(-( nv(1)*s11(i)+nv(2)*s12(i)+nv(3)*s13(i) )*dv(1) &
                          -( nv(1)*s12(i)+nv(2)*s22(i)+nv(3)*s23(i) )*dv(2) &
                          -( nv(1)*s13(i)+nv(2)*s23(i)+nv(3)*s33(i) )*dv(3) )
          s33p=s33p+w(j)*(-(-dv(1)*s11(i)-dv(2)*s12(i)-dv(3)*s13(i) )*dv(1) &
                          -(-dv(1)*s12(i)-dv(2)*s22(i)-dv(3)*s23(i) )*dv(2) &
                          -(-dv(1)*s13(i)-dv(2)*s23(i)-dv(3)*s33(i) )*dv(3) )
       END DO

       s11(i)=s11p
       s12(i)=s12p
       s13(i)=s13p
       s22(i)=s22p
       s23(i)=s23p
       s33(i)=s33p

    END DO

  END SUBROUTINE computeStressKernelsTetrahedron

END MODULE tetrahedron




