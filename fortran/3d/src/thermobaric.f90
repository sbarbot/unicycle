!> program Unicycle (unified cycle of earthquakes) simulates evolution
!! of fault slip and distributed strain using the integral method under
!! the radiation damping approximation.
!!
!! \mainpage
!! 
!! The Green's function for traction and stress interaction amongst 
!! triangle and rectangle dislocations has the following layout
!!
!! \verbatim
!!
!!       / RR RT \
!!   G = |       |
!!       \ TR TT /
!!
!! \endverbatim
!!
!! where
!!
!!   RR is the matrix for traction on rectangle faults due to rectangle fault slip
!!   RT is the matrix for traction on rectangle faults due to triangle  fault slip
!!   TR is the matrix for traction on triangle  faults due to rectangle fault slip
!!   TT is the matrix for traction on triangle  faults due to triangle  fault slip
!!
!! All faults (rectangle or triangle) have two slip directions:
!! strike slip and dip slip. The interaction matrices become
!!
!! \verbatim
!!
!!        / RRss  RRsd \        / RTss  RRsd \
!!   RR = |            |,  RT = |            |,  
!!        \ RRds  RRdd /        \ RRds  RRdd /
!!
!!        / TRss  TRsd \        / TTss  TTsd \
!!   TR = |            |,  TT = |            |
!!        \ TRds  TRdd /        \ TTds  TTdd /
!!
!! \endverbatim
!!
!! The time evolution is evaluated numerically using the 4th/5th order
!! Runge-Kutta method with adaptive time steps. The state vector is 
!! as follows:
!!
!! \verbatim
!!
!!    / R1 1       \   +-----------------------+  +-----------------+
!!    | .          |   |                       |  |                 |
!!    | R1 dPatch  |   |                       |  |                 |
!!    | .          |   |  nRectangle * dPatch  |  |                 |
!!    | .          |   |                       |  |                 |
!!    | Rn 1       |   |                       |  |                 |
!!    | .          |   |                       |  |                 |
!!    | Rn dPatch  |   +-----------------------+  |                 |
!!    |            |                              | nPatch * dPatch |
!!    | T1 1       |   +-----------------------+  |                 |
!!    | .          |   |                       |  |                 |
!!    | T1 dPatch  |   |                       |  |                 |
!!    | .          |   |  nTriangle * dPatch   |  |                 |
!!    | .          |   |                       |  |                 |
!!    | Tn 1       |   |                       |  |                 |
!!    | .          |   |                       |  |                 |
!!    \ Tn dPatch  /   +-----------------------+  +-----------------+
!!
!! \endverbatim
!!
!! where nRectangle and nTriangle are the number of rectangle and
!! triangle patches, respectively, and dPatch is the degrees of 
!! freedom for patches.
!!
!! For every rectangle or triangle patch, we have the following 
!! items in the state vector
!!
!! \verbatim
!!
!!   /  ss   \  1
!!   |  sd   |  .
!!   |  ts   |  .
!!   |  td   |  .
!!   |  tn   |  .
!!   ! theta*|  .
!!   \  v*   /  dPatch
!!
!! \endverbatim
!!
!! where ts, td, and td are the local traction in the strike, dip, and
!! normal directions, ss and sd are the total slip in the strike and dip 
!! directions, v*=log10(v) is the logarithm of the norm of the velocity,
!! and theta*=log10(theta) is the logarithm of the state variable in the 
!! rate and state friction framework.
!!
!! References:<br>
!!
!!   Barbot S., Slow-slip, slow earthquakes, period-two cycles, full and
!!   partial ruptures, and deterministic chaos in a single asperity fault.
!!   Tectonophysics, 768, p.228171, 2019.
!!
!! \author Sylvain Barbot (2017-2025).
!----------------------------------------------------------------------
PROGRAM thermobaric

#include "macros.h90"

#ifndef ODE45
#ifndef ODE23
#define ODE45
#endif
#else
#ifdef ODE23
  ERROR: either ODE45 or ODE23 should be defined, but not both
#endif
#endif

#ifdef NETCDF
  USE exportnetcdf
#endif
  USE greens_3d_thermobaric
  USE rk
  USE mpi_f08
  USE types_3d_thermobaric

  IMPLICIT NONE

  REAL*8, PARAMETER :: DEG2RAD = 0.01745329251994329547437168059786927_8

  ! MPI rank and size
  INTEGER :: rank,csize

  ! error flag
  INTEGER :: ierr

  CHARACTER(512) :: filename
  CHARACTER(512) :: importStateDir

#ifdef NETCDF
  ! check file
  LOGICAL :: fileExist,fileExistAll
#endif

  ! maximum velocity
  REAL*8 :: vMax,vMaxAll

  ! maximum temperature
  REAL*8 :: tMax,tMaxAll

  ! moment rate
  REAL*8 :: momentRate, momentRateAll

  ! scaling factor
  REAL*8, PARAMETER :: lg10=LOG(1.d1)

  ! state vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: y
  ! rate of change of state vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: dydt,yscal
  ! temporary variables
  REAL*8, DIMENSION(:), ALLOCATABLE :: ytmp,ytmp1,ytmp2,ytmp3

  ! slip velocity vector and strain rate tensor array (temporary space)
  REAL*8, DIMENSION(:), ALLOCATABLE :: v

  ! slip velocity vector and strain rate tensor array (temporary space)
  REAL*8, DIMENSION(:), ALLOCATABLE :: vAll

  ! vector array (strike and dip components of slip)
  REAL*4, DIMENSION(:), ALLOCATABLE :: vector,vectorAll

  ! traction vector and stress tensor array (temporary space)
  REAL*8, DIMENSION(:), ALLOCATABLE :: t

  ! displacement and kinematics vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: d,u,dAll

  ! Green's functions
  REAL*8, DIMENSION(:,:), ALLOCATABLE :: G,O,Of,Ol

  ! time
  REAL*8 :: time,t0
  ! time step
  REAL*8 :: dt_try,dt_next,dt_done
  ! time steps
  INTEGER :: i,j

  ! type of evolution law (default)
  INTEGER :: evolutionLaw=1

  ! maximum number of time steps (default)
  INTEGER :: maximumIterations=1000000

  ! verbosity
  INTEGER :: verbose=2

  ! rate of NETCDF export (default)
  INTEGER :: exportSlipDistributionRate=-1

  ! layout for parallelism
  TYPE(LAYOUT_STRUCT) :: layout

  ! model parameters
  TYPE(SIMULATION_STRUCT) :: in

  ! initialization
  CALL MPI_INIT(ierr)
  CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
  CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

  ! start time
  time=0.d0

  ! initial tentative time step
  dt_next=1.0d-3

  ! retrieve input parameters from command line
  CALL init(in)
  CALL FLUSH(STDOUT)

  ! export geometrical layout information
  CALL exportGeometry(in)

  IF (in%isdryrun .AND. 0 .EQ. rank) THEN
     PRINT '("dry run: abort calculation")'
  END IF
  IF (in%isdryrun .OR. in%isversion .OR. in%ishelp) THEN
     CALL MPI_FINALIZE(ierr)
     STOP
  END IF

  ! calculate basis vectors
  CALL initGeometry(in)

  ! describe data layout for parallelism
  CALL initParallelism(in,layout)

#ifdef NETCDF
  WRITE (filename,'(a,"/greens-",I4.4,".grd")') TRIM(in%greensFunctionDirectory),rank
  INQUIRE(FILE=filename,EXIST=fileExist)
  CALL MPI_ALLREDUCE(fileExist,fileExistAll,1,MPI_LOGICAL,MPI_LAND,MPI_COMM_WORLD,ierr)
  IF (in%isImportGreens .AND. fileExistAll) THEN
     CALL initG(in,layout,G)
     IF (0 .EQ. rank) THEN
        PRINT '("# import Green''s function.")'
     END IF
     CALL importGreensNetcdf(G)
  ELSE
     ! calculate the stress interaction matrix
     IF (0 .EQ. rank) THEN
        PRINT '("# computing Green''s functions.")'
     END IF
     CALL buildG(in,layout,G)
     IF (in%isExportGreens) THEN
        IF (0 .EQ. rank) THEN
           PRINT '("# exporting Green''s functions to ",a)',TRIM(in%greensFunctionDirectory)
        END IF
        CALL exportGreensNetcdf(G)
     END IF
  END IF
#else
  ! calculate the stress interaction matrix
  IF (0 .EQ. rank) THEN
     PRINT '("# computing Green''s functions.")'
  END IF
  CALL buildG(in,layout,G)
#endif

  CALL buildO(in,layout,O,Of,Ol)
  DEALLOCATE(Of,Ol)

  ! synchronize threads before writing to standard output
  CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)

  IF (0 .EQ. rank) THEN
     PRINT 2000
  END IF

  ! velocity vector and strain rate tensor array (t=G*vAll)
  ALLOCATE(u(layout%listVelocityN(1+rank)), &
           v(layout%listVelocityN(1+rank)), &
           vAll(SUM(layout%listVelocityN)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the velocity and strain rate vector"

  IF (0 .LT. exportSlipDistributionRate) THEN
     ALLOCATE(vector(layout%listVelocityN(1+rank)),STAT=ierr)
     IF (ierr>0) STOP "could not allocate the two-component vector"
     IF (0 .EQ. rank) THEN
        ALLOCATE(vectorAll(SUM(layout%listVelocityN)),STAT=ierr)
        IF (ierr>0) STOP "could not allocate the two-component vector"
     END IF
  END IF

  ! traction vector and stress tensor array (t=G*v)
  ALLOCATE(t(layout%listForceN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the traction and stress vector"

  ! displacement vector (d=O*v)
  ALLOCATE(d   (in%nObservationPoint*DISPLACEMENT_VECTOR_DGF), &
           dAll(in%nObservationPoint*DISPLACEMENT_VECTOR_DGF),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the displacement vector"

  ! state vector
  ALLOCATE(y(layout%listStateN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the state vector"

  ! rate of state vector
  ALLOCATE(dydt(layout%listStateN(1+rank)), &
           yscal(layout%listStateN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the state vectors"

  ALLOCATE(ytmp (layout%listStateN(1+rank)), &
           ytmp1(layout%listStateN(1+rank)), &
           ytmp2(layout%listStateN(1+rank)), &
           ytmp3(layout%listStateN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the RKQS work space"

  ! allocate buffer from rk module
#ifdef ODE45
  ALLOCATE(buffer(layout%listStateN(1+rank),5),SOURCE=0.0d0,STAT=ierr)
#else
  ALLOCATE(buffer(layout%listStateN(1+rank),3),SOURCE=0.0d0,STAT=ierr)
#endif
  IF (ierr>0) STOP "could not allocate the buffer work space"

  IF (0 .EQ. rank) THEN
     in%timeFilename=TRIM(in%wdir)//"/time-weakening.dat"
     OPEN (UNIT=FPTIMEWEAKENING,FILE=in%timeFilename,IOSTAT=ierr,FORM="FORMATTED")
     IF (ierr>0) THEN
        WRITE_DEBUG_INFO(102)
        WRITE (STDERR,'("error: unable to access ",a)') TRIM(in%timefilename)
        STOP 1
     END IF

     in%timeFilename=TRIM(in%wdir)//"/time.dat"
     OPEN (UNIT=FPTIME,FILE=in%timeFilename,IOSTAT=ierr,FORM="FORMATTED")
     IF (ierr>0) THEN
        WRITE_DEBUG_INFO(102)
        WRITE (STDERR,'("error: unable to access ",a)') TRIM(in%timefilename)
        STOP 1
     END IF
  END IF

  ! initialize the y vector
  IF (0 .EQ. rank) THEN
     PRINT '("# initialize state vector.")'
  END IF
  CALL initStateVector(layout%listStateN(1+rank),y,in)
  IF (0 .EQ. rank) THEN
     PRINT 2000
  END IF

#ifdef NETCDF
  ! initialize netcdf output
  IF (in%isExportNetcdf) THEN
    IF (0 .EQ. rank) THEN

       DO i=1,in%nObservationProfiles
          WRITE (in%observationProfileVelocity(i)%filename,'(a,"/log10v.",I3.3,".grd")') TRIM(in%wdir),i
          CALL initNetcdf(in%observationProfileVelocity(i))
       END DO

       IF (in%isExportSlip) THEN
          DO i=1,in%nObservationProfiles
             WRITE (in%observationProfileSlip(i)%filename,'(a,"/slip.",I3.3,".grd")') TRIM(in%wdir),i
             CALL initNetcdf(in%observationProfileSlip(i))
          END DO
       END IF

       IF (in%isExportStress) THEN
          DO i=1,in%nObservationProfiles
             WRITE (in%observationProfileStress(i)%filename,'(a,"/stress.",I3.3,".grd")') TRIM(in%wdir),i
             CALL initNetcdf(in%observationProfileStress(i))
          END DO
       END IF

       IF (in%isExportTemperature) THEN
          DO i=1,in%nObservationProfiles
             WRITE (in%observationProfileTemperature(i)%filename,'(a,"/temperature.",I3.3,".grd")') TRIM(in%wdir),i
             CALL initNetcdf(in%observationProfileTemperature(i))
          END DO
       END IF
    END IF
  END IF
#endif

  ! gather maximum velocity
  CALL MPI_REDUCE(vMax,vMaxAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

  ! gather maximum temperature
  CALL MPI_REDUCE(tMax,tMaxAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

  ! gather moment rate
  CALL MPI_REDUCE(momentRate,momentRateAll,1,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

  ! initialize output
  IF (0 .EQ. rank) THEN
     WRITE(STDOUT,'("# export to ",a)') TRIM(in%wdir)
     PRINT 2000
     WRITE(STDOUT,'("#       n               time                 dt       vMax       tMax")')
     WRITE(STDOUT,'(I9.9,ES19.12E2,ES19.12E2,ES11.4E2,ES11.4E2)') 0,time,dt_next,vMaxAll,tMaxAll
     WRITE(FPTIME,'("#               time                 dt               vMax         Moment-rate       tMax")')
     WRITE(FPTIMEWEAKENING,'("# velocity and moment-rate in velocity-weakening region")')
     WRITE(FPTIMEWEAKENING,'("#               time                 dt               vMax         Moment-rate       tMax")')
  END IF

  ! initialize observation patch
  DO j=1,in%nObservationState
     IF ((in%observationState(1,j) .GE. layout%listOffset(rank+1)) .AND. &
         (in%observationState(1,j) .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN

        in%observationState(3,j)=100+j
        WRITE (filename,'(a,"/patch-",I8.8,"-",I8.8,".dat")') TRIM(in%wdir),j,in%observationState(1,j)
        OPEN (UNIT=in%observationState(3,j), &
              FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
        IF (ierr>0) THEN
           WRITE_DEBUG_INFO(102)
           WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
           STOP 1
        END IF
        WRITE (in%observationState(3,j), &
              '("#           time (s),", &
              & "     strike slip (m),        dip slip (m),", &
              & "     strike traction,        dip traction,     normal traction,", &
              & "        log10(theta),     log10(velocity),         temperature,", &
              & "     strike velocity,        dip velocity,", &
              & "d strike traction/dt,   d dip traction/dt,d normal traction/dt,", &
              & "        d theta / dt,    acceleration / v,    d temperature/dt")')
     END IF
  END DO

  ! initialize observation points
  IF (0 .EQ. rank) THEN
     DO j=1,in%nObservationPoint
        in%observationPoint(j)%file=100000+j
        WRITE (filename,'(a,"/opts-",a,".dat")') TRIM(in%wdir),TRIM(in%observationPoint(j)%name)
        OPEN (UNIT=in%observationPoint(j)%file, &
              FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
        IF (ierr>0) THEN
           WRITE_DEBUG_INFO(102)
           WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
           STOP 1
        END IF
        WRITE (in%observationPoint(j)%file,'("#               time                    ", &
                                           & "u1                    u2                    u3")')
     END DO
  END IF

  ! main loop
#ifdef ODE23
  CALL odefun(layout%listStateN(1+rank),time,y,dydt)
#endif
  DO i=1,maximumIterations

#ifdef ODE45
     CALL odefun(layout%listStateN(1+rank),time,y,dydt)
#endif

     CALL export()
     CALL exportPoints()

     IF (0 .LT. exportSlipDistributionRate) THEN
        IF (0 .EQ. MOD(i-1,exportSlipDistributionRate)) THEN
           CALL exportAsciiSlipDistribution()
        END IF
     END IF

#ifdef NETCDF
     IF (in%isExportNetcdf) THEN

        IF (0 .EQ. rank) THEN
           DO j=1,in%nObservationProfiles
              IF (0 .EQ. MOD(i,in%observationProfileVelocity(j)%rate)) THEN
                 CALL exportNetcdfVelocity(in%observationProfileVelocity(j))
              END IF
           END DO
        END IF

        IF (in%isExportSlip) THEN
           DO j=1,in%nObservationProfiles
              IF (0 .EQ. MOD(i,in%observationProfileSlip(j)%rate)) THEN
                 CALL exportNetcdfSlip(in%observationProfileSlip(j))
              END IF
           END DO
        END IF

        IF (in%isExportStress) THEN
           DO j=1,in%nObservationProfiles
              IF (0 .EQ. MOD(i,in%observationProfileStress(j)%rate)) THEN
                 CALL exportNetcdfStress(in%observationProfileStress(j))
              END IF
           END DO
        END IF
     END IF
#endif

#ifdef NETCDF
     IF (0 .EQ. rank) THEN
        IF (in%isExportNetcdf) THEN
           DO j=1,in%nObservationImages
              IF (0 .EQ. MOD(i,in%observationImageVelocity(j)%rate)) THEN
                 CALL exportImageNetcdfVelocity(in%observationImageVelocity(j),j)
                 CALL exportImageNetcdfVelocityStrike(in%observationImageVelocityStrike(j),j)
                 CALL exportImageNetcdfVelocityDip(in%observationImageVelocityDip(j),j)
              END IF
           END DO
        END IF
     END IF
#endif

     dt_try=dt_next
     yscal(:)=abs(y(:))+abs(dt_try*dydt(:))+TINY

     t0=time
#ifdef ODE45
     CALL rungeKutta(layout%listStateN(1+rank),t0,y,dydt, &
               yscal,ytmp1,ytmp2,ytmp3,dt_try,dt_done,dt_next,odefun,timeStep45)
#else
     CALL rungeKutta(layout%listStateN(1+rank),t0,y,dydt, &
               yscal,ytmp1,ytmp2,ytmp3,dt_try,dt_done,dt_next,odefun,timeStep23)
#endif

     time=time+dt_done

     IF (in%isExportState) THEN
        IF (0 .EQ. MOD(i,5000)) THEN
           IF (0.EQ. rank) PRINT '("# exporting state")'
           CALL exportState()
        END IF
     END IF

     ! end calculation
     IF (in%interval .LE. time) THEN
        EXIT
     END IF
   
  END DO

  IF (0 .EQ. rank) THEN
     PRINT '(I9.9," time steps.")', i
  END IF

  IF (0 .EQ. rank) THEN
     CLOSE(FPTIMEWEAKENING)
     CLOSE(FPTIME)
  END IF

  ! close observation state files
  DO j=1,in%nObservationState
     IF ((in%observationState(1,j) .GE. layout%listOffset(rank+1)) .AND. &
         (in%observationState(1,j) .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN
        CLOSE(in%observationState(3,j))
     END IF
  END DO

  ! close the observation points
  IF (0 .EQ. rank) THEN
     DO j=1,in%nObservationPoint
        CLOSE(in%observationPoint(j)%file)
     END DO
  END IF

#ifdef NETCDF
  IF (0 .EQ. rank) THEN
     IF (in%isExportnetcdf) THEN
        DO j=1,in%nObservationProfiles
           CALL closeNetcdfUnlimited(in%observationProfileVelocity(j)%ncid, &
                                     in%observationProfileVelocity(j)%y_varid, &
                                     in%observationProfileVelocity(j)%z_varid, &
                                     in%observationProfileVelocity(j)%ncCount)
        END DO
        IF (in%isExportSlip) THEN
           DO j=1,in%nObservationProfiles
              CALL closeNetcdfUnlimited(in%observationProfileSlip(j)%ncid, &
                                        in%observationProfileSlip(j)%y_varid, &
                                        in%observationProfileSlip(j)%z_varid, &
                                        in%observationProfileSlip(j)%ncCount)
           END DO
        END IF
        IF (in%isExportStress) THEN
           DO j=1,in%nObservationProfiles
              CALL closeNetcdfUnlimited(in%observationProfileStress(j)%ncid, &
                                        in%observationProfileStress(j)%y_varid, &
                                        in%observationProfileStress(j)%z_varid, &
                                        in%observationProfileStress(j)%ncCount)
           END DO
        END IF
     END IF
  END IF
#endif

  DEALLOCATE(y,dydt,yscal)
  DEALLOCATE(ytmp,ytmp1,ytmp2,ytmp3)
  DEALLOCATE(buffer)
  DEALLOCATE(G,v,vAll,t)
#ifdef NETCDF
  IF (ALLOCATED(in%observationProfileVelocity)) DEALLOCATE(in%observationProfileVelocity)
  IF (ALLOCATED(in%observationProfileSlip)) DEALLOCATE(in%observationProfileSlip)
  IF (ALLOCATED(in%observationProfileStress)) DEALLOCATE(in%observationProfileStress)

  IF (ALLOCATED(in%observationImageVelocity)) DEALLOCATE(in%observationImageVelocity)
  IF (ALLOCATED(in%observationImageVelocityStrike)) DEALLOCATE(in%observationImageVelocityStrike)
  IF (ALLOCATED(in%observationImageVelocityDip)) DEALLOCATE(in%observationImageVelocityDip)
#endif
  DEALLOCATE(layout%listForceN)
  DEALLOCATE(layout%listVelocityN,layout%listVelocityOffset)
  DEALLOCATE(layout%listStateN,layout%listStateOffset)
  DEALLOCATE(layout%elementStateIndex)
  DEALLOCATE(layout%listElements,layout%listOffset)
  DEALLOCATE(O,d,u,dAll)
  IF (0 .LT. exportSlipDistributionRate) THEN
     DEALLOCATE(vector)
     IF (0 .EQ. rank) DEALLOCATE(vectorAll)
  END IF

  CALL MPI_FINALIZE(ierr)

2000 FORMAT ("# ----------------------------------------------------------------------------")
     
CONTAINS

  !-----------------------------------------------------------------------
  !> subroutine exportState
  ! export state vector to disk to allow restart
  !----------------------------------------------------------------------
  SUBROUTINE exportState()
    WRITE (filename,'(a,"/state-",I4.4,".ode")') TRIM(in%wdir),rank
    OPEN(FPSTATE,FILE=filename,FORM="unformatted")
    WRITE(FPSTATE) time
    WRITE(FPSTATE) y
    CLOSE(FPSTATE)
  END SUBROUTINE exportState

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportGreensNetcdf
  ! export the Greens function by rank
  !----------------------------------------------------------------------
  SUBROUTINE exportGreensNetcdf(M)
    REAL*8, DIMENSION(:,:), INTENT(IN) :: M

    REAL*8, DIMENSION(:), ALLOCATABLE :: x,y

    INTEGER :: i,ierr
    CHARACTER(LEN=256) :: filename

    ALLOCATE(x(SIZE(M,1)),y(SIZE(M,2)),STAT=ierr)
    IF (ierr/=0) STOP "could not allocate netcdf coordinate for Greens function"

    ! loop over all patch elements
    DO i=1,SIZE(M,1)
       x(i)=REAL(i,8)
    END DO
    DO i=1,SIZE(M,2)
       y(i)=REAL(i,8)
    END DO

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/greens-",I4.4,".grd")') TRIM(in%greensFunctionDirectory),rank
    CALL writeNetcdf(filename,SIZE(M,1),x,SIZE(M,2),y,M,1)

    DEALLOCATE(x,y)

  END SUBROUTINE exportGreensNetcdf
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine importGreensNetcdf
  ! import the Greens function by rank
  !----------------------------------------------------------------------
  SUBROUTINE importGreensNetcdf(M)
    REAL*8, DIMENSION(:,:), INTENT(OUT) :: M

    CHARACTER(LEN=256) :: filename

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/greens-",I4.4,".grd")') TRIM(in%greensFunctionDirectory),rank
    CALL readNetcdf(filename,SIZE(M,1),SIZE(M,2),M)

  END SUBROUTINE importGreensNetcdf
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine initNetcdf
  ! initializes the coordinates of netcdf files
  !----------------------------------------------------------------------
  SUBROUTINE initNetcdf(profile)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: profile

    REAL*8, DIMENSION(:), ALLOCATABLE :: x

    INTEGER :: i,ierr

    ! initialize the number of exports
    profile%ncCount=0

    ALLOCATE(x(profile%n),STAT=ierr)
    IF (ierr/=0) STOP "could not allocate netcdf coordinate"

    ! loop over all patch elements
    DO i=1,profile%n
       x(i)=REAL(i,8)
    END DO

    ! netcdf file is compatible with GMT
    CALL openNetcdfUnlimited( &
             profile%filename, &
             profile%n, &
             x, &
             profile%ncid, &
             profile%y_varid, &
             profile%z_varid)

    DEALLOCATE(x)

  END SUBROUTINE initNetcdf
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportNetcdfVelocity
  ! export time series of log10(v)
  !----------------------------------------------------------------------
  SUBROUTINE exportNetcdfVelocity(profile)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: profile

    REAL*4, DIMENSION(profile%n) :: z

    INTEGER :: j,i
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    ! update the export count
    profile%ncCount=profile%ncCount+1

    ! loop over patch elements by stride
    DO j=1,profile%n
       i=1+profile%offset+(j-1)*profile%stride
       IF (in%rectangle%ns .GE. i) THEN
          patch=in%rectangle%s(i)
       ELSE
          patch=in%triangle%s(i)
       END IF
       ! norm of slip rate vector
       z(j)=REAL(LOG10(SQRT( &
               (patch%Vl*COS(patch%rake)+vAll(1+DGF_PATCH*(i-1)))**2 &
              +(patch%Vl*SIN(patch%rake)+vAll(2+DGF_PATCH*(i-1)))**2 &
               )),4)
    END DO

    CALL writeNetcdfUnlimited(profile%ncid,profile%y_varid,profile%z_varid,profile%ncCount,profile%n,z)

    ! flush every so often
    IF (0 .EQ. MOD(profile%ncCount,50)) THEN
       CALL flushNetcdfUnlimited(profile%ncid,profile%y_varid,profile%ncCount)
    END IF


  END SUBROUTINE exportNetcdfVelocity

  !-----------------------------------------------------------------------
  !> subroutine exportNetcdfSlip
  ! export time series of cumulative slip
  !----------------------------------------------------------------------
  SUBROUTINE exportNetcdfSlip(profile)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: profile

    REAL*4, DIMENSION(profile%n) :: z,zAll

    INTEGER :: j,i,c,l

    ! number of sample per thread and offset per thread
    INTEGER, DIMENSION(csize) :: cAll,oAll

    ! initialize number of profile element in current thread
    c=0

    ! loop over patch elements by stride
    DO j=1,profile%n
       i=1+profile%offset+(j-1)*profile%stride

       ! test whether element is owned by current thread
       IF ((i .GE. layout%listOffset(rank+1)) .AND. &
           (i .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN

          ! state vector index
          l=layout%elementStateIndex(i-layout%listOffset(rank+1))+1

          ! update count for current thread
          c=c+1

          ! norm of cumulative slip vector
          z(c)=REAL(SQRT(y(l+STATE_VECTOR_SLIP_STRIKE)**2+y(l+STATE_VECTOR_SLIP_DIP)**2),4)
       END IF
    END DO

    ! share dimension of all threads among all threads
    CALL MPI_ALLGATHER(c,1,MPI_INTEGER,cAll,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)

    ! thread offset
    oAll(2:csize)=cAll(1:(csize-1))
    oAll(1)=0

    ! master thread gathers data from all threads
    CALL MPI_GATHERV(z,c,MPI_REAL4, &
                     zAll,cAll,oAll,MPI_REAL4, &
                     0,MPI_COMM_WORLD,ierr)

    IF (0 .EQ. rank) THEN
       ! update the export count
       profile%ncCount=profile%ncCount+1

       CALL writeNetcdfUnlimited(profile%ncid,profile%y_varid,profile%z_varid,profile%ncCount,profile%n,zAll)

       ! flush every so often
       IF (0 .EQ. MOD(profile%ncCount,50)) THEN
          CALL flushNetcdfUnlimited(profile%ncid,profile%y_varid,profile%ncCount)
       END IF

    END IF

  END SUBROUTINE exportNetcdfSlip

  !-----------------------------------------------------------------------
  !> subroutine exportNetcdfStress
  ! export time series of instantaneous shear stress
  !----------------------------------------------------------------------
  SUBROUTINE exportNetcdfStress(profile)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: profile

    REAL*4, DIMENSION(profile%n) :: z,zAll

    INTEGER :: j,i,c,l

    ! number of sample per thread and offset per thread
    INTEGER, DIMENSION(csize) :: cAll,oAll

    ! initialize number of profile element in current thread
    c=0

    ! loop over patch elements by stride
    DO j=1,profile%n
       i=1+profile%offset+(j-1)*profile%stride

       ! test whether element is owned by current thread
       IF ((i .GE. layout%listOffset(rank+1)) .AND. &
           (i .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN

          ! state vector index
          l=layout%elementStateIndex(i-layout%listOffset(rank+1))+1

          ! update count for current thread
          c=c+1

          ! norm of cumulative shear traction vector
          z(c)=REAL(SQRT(y(l+STATE_VECTOR_TRACTION_STRIKE)**2+y(l+STATE_VECTOR_TRACTION_DIP)**2),4)
       END IF
    END DO

    ! share dimension of all threads among all threads
    CALL MPI_ALLGATHER(c,1,MPI_INTEGER,cAll,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)

    ! thread offset
    oAll(2:csize)=cAll(1:(csize-1))
    oAll(1)=0

    ! master thread gathers data from all threads
    CALL MPI_GATHERV(z,c,MPI_REAL4, &
                     zAll,cAll,oAll,MPI_REAL4, &
                     0,MPI_COMM_WORLD,ierr)

    IF (0 .EQ. rank) THEN
       ! update the export count
       profile%ncCount=profile%ncCount+1

       CALL writeNetcdfUnlimited(profile%ncid,profile%y_varid,profile%z_varid,profile%ncCount,profile%n,zAll)

       ! flush every so often
       IF (0 .EQ. MOD(profile%ncCount,50)) THEN
          CALL flushNetcdfUnlimited(profile%ncid,profile%y_varid,profile%ncCount)
       END IF

    END IF

  END SUBROUTINE exportNetcdfStress
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportImageNetcdfVelocity
  ! export an image of patches by strides
  !----------------------------------------------------------------------
  SUBROUTINE exportImageNetcdfVelocity(image,number)
    TYPE(IMAGE_STRUCT), INTENT(IN) :: image
    INTEGER, INTENT(IN) :: number

    REAL*8, DIMENSION(image%nl) :: x
    REAL*8, DIMENSION(image%nw) :: y
    REAL*8, DIMENSION(image%nl,image%nw) :: z

    INTEGER :: j,k,index
    CHARACTER(LEN=256) :: filename
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    ! loop over all patch elements
    DO j=1,image%nl
       x(j)=REAL(j,8)
    END DO
    DO k=1,image%nw
       y(k)=REAL(k,8)
    END DO

    ! loop over patch elements by stride
    DO k=1,image%nw
       DO j=1,image%nl
          index=1+image%offset+(j-1)*image%stride+(k-1)*image%stride*image%nl
          IF (in%rectangle%ns .GE. index) THEN
             patch=in%rectangle%s(index)
          ELSE
             patch=in%triangle%s(index)
          END IF
          ! norm of slip rate vector
          z(j,k)=LOG10(SQRT( &
                  (patch%Vl*COS(patch%rake)+vAll(1+DGF_PATCH*(index-1)))**2 &
                 +(patch%Vl*SIN(patch%rake)+vAll(2+DGF_PATCH*(index-1)))**2 &
                  ))
       END DO
    END DO

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/image-",I2.2,"-",I6.6,"-log10v.grd")') TRIM(in%wdir),number,i
    CALL writeNetcdf(filename,image%nl,x,image%nw,y,z,1)

  END SUBROUTINE exportImageNetcdfVelocity

  !-----------------------------------------------------------------------
  !> subroutine exportImageNetcdfVelocityStrike
  ! export an image of patches by strides
  !----------------------------------------------------------------------
  SUBROUTINE exportImageNetcdfVelocityStrike(image,number)
    TYPE(IMAGE_STRUCT), INTENT(IN) :: image
    INTEGER, INTENT(IN) :: number

    REAL*8, DIMENSION(image%nl) :: x
    REAL*8, DIMENSION(image%nw) :: y
    REAL*8, DIMENSION(image%nl,image%nw) :: z

    INTEGER :: j,k,index
    CHARACTER(LEN=256) :: filename
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    ! loop over all patch elements
    DO j=1,image%nl
       x(j)=REAL(j,8)
    END DO
    DO k=1,image%nw
       y(k)=REAL(k,8)
    END DO

    ! loop over patch elements by stride
    DO k=1,image%nw
       DO j=1,image%nl
          index=1+image%offset+(j-1)*image%stride+(k-1)*image%stride*image%nl
          IF (in%rectangle%ns .GE. index) THEN
             patch=in%rectangle%s(index)
          ELSE
             patch=in%triangle%s(index)
          END IF
          ! strike-parallel component of slip distribution
          z(j,k)=patch%Vl*COS(patch%rake)+vAll(1+DGF_PATCH*(index-1))
       END DO
    END DO

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/image-",I2.2,"-",I6.6,"-v-strike.grd")') TRIM(in%wdir),number,i
    CALL writeNetcdf(filename,image%nl,x,image%nw,y,z,1)

  END SUBROUTINE exportImageNetcdfVelocityStrike

  !-----------------------------------------------------------------------
  !> subroutine exportImageNetcdfVelocityDip
  ! export an image of patches by strides
  !----------------------------------------------------------------------
  SUBROUTINE exportImageNetcdfVelocityDip(image,number)
    TYPE(IMAGE_STRUCT), INTENT(IN) :: image
    INTEGER, INTENT(IN) :: number

    REAL*8, DIMENSION(image%nl) :: x
    REAL*8, DIMENSION(image%nw) :: y
    REAL*8, DIMENSION(image%nl,image%nw) :: z

    INTEGER :: j,k,index
    CHARACTER(LEN=256) :: filename
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    ! loop over all patch elements
    DO j=1,image%nl
       x(j)=REAL(j,8)
    END DO
    DO k=1,image%nw
       y(k)=REAL(k,8)
    END DO

    ! loop over patch elements by stride
    DO k=1,image%nw
       DO j=1,image%nl
          index=1+image%offset+(j-1)*image%stride+(k-1)*image%stride*image%nl
          IF (in%rectangle%ns .GE. index) THEN
             patch=in%rectangle%s(index)
          ELSE
             patch=in%triangle%s(index)
          END IF
          ! dip-parallel component of slip distribution
          z(j,k)=patch%Vl*SIN(patch%rake)+vAll(2+DGF_PATCH*(index-1))
       END DO
    END DO

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/image-",I2.2,"-",I6.6,"-v-dip.grd")') TRIM(in%wdir),number,i
    CALL writeNetcdf(filename,image%nl,x,image%nw,y,z,1)

  END SUBROUTINE exportImageNetcdfVelocityDip
#endif

  !-----------------------------------------------------------------------
  !> subroutine exportAsciiSlipDistribution
  ! export cumulative fault slip to ASCII file
  !----------------------------------------------------------------------
  SUBROUTINE exportAsciiSlipDistribution()

    INTEGER :: k,l

    ! loop over elements owned by current thread
    DO k=1,SIZE(layout%elementIndex)
       ! element index in state vector
       l=layout%elementStateIndex(k)-in%dPatch+1

       ! cumulative slip
       vector(2*(k-1)+1)=REAL(y(l+STATE_VECTOR_SLIP_STRIKE),4)
       vector(2*(k-1)+2)=REAL(y(l+STATE_VECTOR_SLIP_DIP),4)
    END DO

    ! all threads gather velocity from all threads
    CALL MPI_GATHERV(vector,layout%listVelocityN(1+rank),MPI_REAL4, &
                     vectorAll,layout%listVelocityN,layout%listVelocityOffset,MPI_REAL4, &
                     0,MPI_COMM_WORLD,ierr)

    ! export to ASCII
    IF (0 .EQ. rank) THEN
          ! export the fault patches
          WRITE (filename,'(a,"/slip-distribution-",I8.8,".dat")') TRIM(in%wdir),i
          OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          WRITE (FPOUT,'("# strike-slip, dip-slip")')
          DO k=1,in%npatch
             WRITE (FPOUT,'(2ES18.10E2)') vectorAll(2*(k-1)+1), vectorAll(2*(k-1)+2)
          END DO
          CLOSE(FPOUT)
    END IF

  END SUBROUTINE exportAsciiSlipDistribution

  !-----------------------------------------------------------------------
  !> subroutine exportGeometry
  ! export geometry of fault patches
  !----------------------------------------------------------------------
  SUBROUTINE exportGeometry(in)
    IMPLICIT NONE
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in

    CHARACTER(512) :: filename

    IF (0 .EQ. rank) THEN
       IF (0 .NE. in%rectangle%ns) THEN
          WRITE (filename,'(a,"/patch-rectangle.vtp")') TRIM(in%wdir)
          OPEN (UNIT=FPVTP,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          CALL exportvtk_rfaults_thermobaric(in%rectangle%ns, &
                                 in%rectangle%x, &
                                 in%rectangle%length, &
                                 in%rectangle%width, &
                                 in%rectangle%strike, &
                                 in%rectangle%dip, &
                                 in%rectangle%s, &
                                 FPVTP)
          CLOSE(FPVTP)

          WRITE (filename,'(a,"/patch-rectangle.xyz")') TRIM(in%wdir)
          OPEN (UNIT=FPVTP,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          CALL exportxyz_rfaults_thermobaric(in%rectangle%ns, &
                                 in%rectangle%x, &
                                 in%rectangle%length, &
                                 in%rectangle%width, &
                                 in%rectangle%strike, &
                                 in%rectangle%dip, &
                                 in%rectangle%s, &
                                 FPVTP)
          CLOSE(FPVTP)

       END IF

       IF (0 .NE. in%triangle%ns) THEN
          WRITE (filename,'(a,"/patch-triangle.vtp")') TRIM(in%wdir)
          OPEN (UNIT=FPVTP,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          CALL exportvtk_tfaults_thermobaric(in%triangle%ns, &
                                 in%triangle%i1, &
                                 in%triangle%i2, &
                                 in%triangle%i3, &
                                 in%triangle%nVe, &
                                 in%triangle%v, &
                                 in%triangle%s, &
                                 FPVTP)
          CLOSE(FPVTP)
       END IF
    END IF

  END SUBROUTINE exportGeometry

  !-----------------------------------------------------------------------
  !> subroutine exportPoints
  ! export observation points
  !----------------------------------------------------------------------
  SUBROUTINE exportPoints()

    IMPLICIT NONE

    INTEGER :: i,k,l,ierr
    INTEGER :: elementIndex,elementType
    CHARACTER(1024) :: formatString
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    !-----------------------------------------------------------------
    ! step 1/4 - gather the kinematics from the state vector
    !-----------------------------------------------------------------

    ! element index in d vector
    k=1
    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementIndex=layout%elementIndex(i)
       elementType= layout%elementType(i)

       SELECT CASE (elementType)
       CASE (FLAG_RECTANGLE)
             patch=in%rectangle%s(elementIndex)
       CASE (FLAG_TRIANGLE)
             patch=in%triangle%s(elementIndex)
       CASE DEFAULT
          WRITE(STDERR,'("wrong case: this is a bug.")')
          WRITE_DEBUG_INFO(-1)
          STOP 2
       END SELECT

       ! strike slip and dip slip
       u(k:k+layout%elementVelocityDGF(i)-1)= (/ &
               y(l+STATE_VECTOR_SLIP_STRIKE), &
               y(l+STATE_VECTOR_SLIP_DIP) /)

       l=l+in%dPatch

       k=k+layout%elementVelocityDGF(i)
    END DO

    !-----------------------------------------------------------------
    ! step 2/4 - calculate the rate of traction and rate of stress
    !-----------------------------------------------------------------

#ifdef __BLAS__
    ! use the BLAS library to compute the matrix vector product
    CALL DGEMV("T",SIZE(O,1),SIZE(O,2), &
                1._8,O,SIZE(O,1),u,1,0.d0,d,1)
#else
    ! slower, Fortran intrinsic
    d=MATMUL(TRANSPOSE(O),u)
#endif

    !-----------------------------------------------------------------
    ! step 3/4 - master thread adds the contribution of all elements
    !-----------------------------------------------------------------

    CALL MPI_REDUCE(d,dAll,in%nObservationPoint*DISPLACEMENT_VECTOR_DGF, &
                    MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

    !-----------------------------------------------------------------
    ! step 4/4 - master thread writes to disk
    !-----------------------------------------------------------------

    IF (0 .EQ. rank) THEN
       formatString="(ES20.14E2"
       DO i=1,DISPLACEMENT_VECTOR_DGF
          formatString=TRIM(formatString)//",X,ES21.14E2"
       END DO
       formatString=TRIM(formatString)//")"

       ! element index in d vector
       k=1
       DO i=1,in%nObservationPoint
          WRITE (in%observationPoint(i)%file,TRIM(formatString)) time,dAll(k:k+DISPLACEMENT_VECTOR_DGF-1)
          k=k+DISPLACEMENT_VECTOR_DGF
       END DO
    END IF

  END SUBROUTINE exportPoints

  !-----------------------------------------------------------------------
  !> subroutine export
  ! write the state variables of patch elements, and other information.
  !----------------------------------------------------------------------
  SUBROUTINE export()

    ! degrees of freedom
    INTEGER :: dgf

    ! counters
    INTEGER :: j,k

    ! index in state vector
    INTEGER :: index

    ! format string
    CHARACTER(1024) :: formatString

    ! gather maximum velocity
    CALL MPI_REDUCE(vMax,vMaxAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

    ! gather moment-rate
    CALL MPI_REDUCE(momentRate,momentRateAll,1,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

    ! export observation state
    DO j=1,in%nObservationState
       IF ((in%observationState(1,j) .GE. layout%listOffset(rank+1)) .AND. &
           (in%observationState(1,j) .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN

          ! check observation state sampling rate
          IF (0 .EQ. MOD(i-1,in%observationState(2,j))) THEN
             dgf=in%dPatch

             formatString="(ES20.14E2"
             DO k=1,dgf
                formatString=TRIM(formatString)//",X,ES20.12E3,X,ES20.12E3"
             END DO
             formatString=TRIM(formatString)//")"

             index=layout%elementStateIndex(in%observationState(1,j)-layout%listOffset(rank+1)+1)-dgf
             WRITE (in%observationState(3,j),TRIM(formatString)) time, &
                       y(index+1:index+dgf), &
                    dydt(index+1:index+dgf)
          END IF
       END IF
    END DO

    IF (0 .EQ. rank) THEN
       WRITE(FPTIME,'(ES20.14E2,ES19.12E2,ES19.12E2,ES20.12E2)') time,dt_done,vMaxAll,momentRateAll
       IF (0 .EQ. MOD(i,50)) THEN
          WRITE(STDOUT,'(I9.9,ES19.12E2,ES19.12E2,ES11.4E2,ES11.4E2)') i,time,dt_done,vMaxAll,tMaxAll
          CALL FLUSH(STDOUT)
          CALL FLUSH(FPTIME)
       END IF
    END IF

  END SUBROUTINE export

  !-----------------------------------------------------------------------
  !> subroutine initStateVector
  ! initialize the state vector
  !
  ! INPUT:
  ! @param n - number of state elements own by current thread
  ! @param y - the state vector (segment owned by currect thread)
  !
  !----------------------------------------------------------------------
  SUBROUTINE initStateVector(n,y,in)
    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: n
    REAL*8, INTENT(OUT)    :: y(n)
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in

    INTEGER :: i,l
    INTEGER :: elementType,elementIndex
    TYPE(PATCH_ELEMENT_STRUCT) :: p
    TYPE(ROCK_STRUCT) :: r
    TYPE(HEALING_STRUCT) :: h1,h2
    TYPE(FLOW_STRUCT) :: f1,f2,f3

    ! velocity perturbation
    REAL*8, PARAMETER :: modifier = 0.99d0

    ! initial traction (Pa)
    REAL*8 :: tau0

    ! initial state variable (LOG10(d/do))
    REAL*8 :: Sinit

    ! initial velocity (m/s)
    REAL*8 :: Vinit

    ! initial temperature
    REAL*8 :: Tinit

    ! velocity
    REAL*8 :: V1,V2,V3

    ! patch area
    REAL*8 :: area

    ! zero out state vector
    y=0._8

    ! maximum velocity
    vMax=0._8

    ! maximum temperature
    tMax=0._8

    ! moment-rate
    momentRate=0._8

    ! restart
    IF (in%isImportState) THEN
       WRITE (filename,'(a,"/state-",I4.4,".ode")') TRIM(in%wdir),rank
       IF (0 .EQ. rank) WRITE (STDOUT,'("# load state vector from ",a)') TRIM(filename)
       OPEN(FPSTATE,FILE=filename,FORM="unformatted")
       READ(FPSTATE) time
       READ(FPSTATE) y
       CLOSE(FPSTATE)

       ! element index in state vector
       l=1
       ! loop over elements owned by current thread
       DO i=1,SIZE(layout%elementIndex)
          elementType= layout%elementType(i)
          elementIndex=layout%elementIndex(i)

          SELECT CASE (elementType)
          CASE (FLAG_RECTANGLE)
                p=in%rectangle%s(elementIndex)
                area=in%rectangle%length(elementIndex)*in%rectangle%width(elementIndex)
          CASE (FLAG_TRIANGLE)
                p=in%triangle%s(elementIndex)
                area=in%triangle%area(elementIndex)
          CASE DEFAULT
             WRITE(STDERR,'("wrong case: this is a bug.")')
             WRITE_DEBUG_INFO(-1)
             STOP 2
          END SELECT

          Vinit=DEXP(y(l+STATE_VECTOR_VELOCITY)*lg10)

          ! maximum velocity
          vMax=MAX(Vinit,vMax)

          ! moment-rate
          momentRate=momentRate+(Vinit-p%Vl)*in%mu*area

          l=l+in%dPatch
       END DO

       RETURN
    END IF

    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementType= layout%elementType(i)
       elementIndex=layout%elementIndex(i)

       SELECT CASE (elementType)
       CASE (FLAG_RECTANGLE)
             p=in%rectangle%s(elementIndex)
             area=in%rectangle%length(elementIndex)*in%rectangle%width(elementIndex)
       CASE (FLAG_TRIANGLE)
             p=in%triangle%s(elementIndex)
             area=in%triangle%area(elementIndex)
       CASE DEFAULT
          WRITE(STDERR,'("wrong case: this is a bug.")')
          WRITE_DEBUG_INFO(-1)
          STOP 2
       END SELECT

       ! current rock
       r=in%rock(p%rockType)

       ! strike slip
       y(l+STATE_VECTOR_SLIP_STRIKE) = 0._8

       ! dip slip
       y(l+STATE_VECTOR_SLIP_DIP) = 0._8

       ! steady-state temperature
       Tinit=p%Tb+r%mu0*p%sig*p%Vl/p%wRhoC/p%DW2
   
       ! initial velocity
       Vinit=modifier*p%Vl

       ! initial size of microasperities (LOG10(d/d0)) at Vinit
       SELECT CASE(r%nHealing)
       CASE(1)
          ! log10(dss/do)
          h1=r%healing(1)
          Sinit=1._8/h1%p*(LOG(2*p%h*h1%f0/r%lambda/h1%p/Vinit) &
                          +h1%q*LOG(p%sig/r%sigma0) &
                          -h1%H/in%R*(1._8/Tinit-1._8/h1%To))/lg10
       CASE(2)
          h1=r%healing(1)
          h2=r%healing(2)
          Sinit=LOG(findRoot2( &
                  h1%f0*(r%d0)**h1%p/h1%p*(p%sig/r%sigma0)**h1%q*EXP(-h1%H/in%R*(1._8/Tinit-1._8/h1%To)),-h1%p, &
                  h2%f0*(r%d0)**h2%p/h2%p*(p%sig/r%sigma0)**h2%q*EXP(-h2%H/in%R*(1._8/Tinit-1._8/h2%To)),-h2%p, &
                  r%lambda*Vinit/(2*p%h))/r%d0)/lg10
       CASE DEFAULT
          WRITE (0,'("incorrect healing term ", I2)') r%nHealing
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT

       ! initial traction
       IF (0 .GT. p%tau0) THEN

          ! initial velocity
          Vinit=modifier*p%Vl

          SELECT CASE(r%nFlow)
          CASE(1)
             f1=r%flow(1)
             tau0=(f1%c0+r%mu0*p%sig)*(Vinit/f1%Vo)**(1._8/f1%n) &
                                    *EXP(r%alpha*Sinit*lg10) &
                                    *(p%sig/r%sigma0)**(-r%beta+f1%zeta/f1%n) &
                                    *EXP(-f1%Q/in%R/f1%n*(1._8/Tinit-1._8/f1%To))
          CASE(2)
             f1=r%flow(1)
             f2=r%flow(2)
             tau0=findRoot2( &
                     f1%Vo/(f1%c0+r%mu0*p%sig)**f1%n &
                                 *DEXP(-r%alpha*f1%n*Sinit*lg10) &
                                 *(p%sig/r%sigma0)**(r%beta*f1%n-f1%zeta) &
                                 *DEXP(-f1%Q/in%R*(1._8/Tinit-1._8/f1%To)), f1%n, &
                     f2%Vo/(f2%c0+r%mu0*p%sig)**f2%n &
                                 *DEXP(-r%alpha*f2%n*Sinit*lg10) &
                                 *(p%sig/r%sigma0)**(r%beta*f2%n-f2%zeta) &
                                 *DEXP(-f2%Q/in%R*(1._8/Tinit-1._8/f2%To)), f2%n, &
                     Vinit)
          CASE(3)
             f1=r%flow(1)
             f2=r%flow(2)
             f3=r%flow(3)
             ! no state variable nor normal-stress dependence for f3
             ! except for the pressure-dependence of activation temperature
             tau0=findRoot3( &
                     f1%Vo/(f1%c0+r%mu0*p%sig)**f1%n &
                                 *DEXP(-r%alpha*f1%n*Sinit*lg10) &
                                 *(p%sig/r%sigma0)**(r%beta*f1%n-f1%zeta) &
                                 *DEXP(-f1%Q/in%R*(1._8/Tinit-1._8/f1%To)), f1%n, &
                     f2%Vo/(f2%c0+r%mu0*p%sig)**f2%n &
                                 *DEXP(-r%alpha*f2%n*Sinit*lg10) &
                                 *(p%sig/r%sigma0)**(r%beta*f2%n-f2%zeta) &
                                 *DEXP(-f2%Q/in%R*(1._8/Tinit-1._8/f2%To)), f2%n, &
                     f3%Vo/(f3%c0              )**f3%n &
                                 *(p%sig/r%sigma0)**(           -f3%zeta) &
                                 *DEXP(-f3%Q/in%R*(1._8/Tinit-1._8/f3%To)), f3%n, &
                     Vinit);
          CASE DEFAULT
             WRITE (0,'("incorrect flow law ", I2)') r%nFlow
             WRITE_DEBUG_INFO(100)
             STOP 3
          END SELECT
       ELSE
          tau0=p%tau0

          ! set initial velocity corresponding to stress
          SELECT CASE(r%nFlow)
          CASE(1)
             f1=r%flow(1)
             V1=f1%Vo*(tau0/(f1%c0+r%mu0*p%sig))**(f1%n) &
                     *DEXP(-r%alpha*f1%n*Sinit*lg10) &
                     *(p%sig/r%sigma0)**(r%beta*f1%n-f1%zeta) &
                     *DEXP(-f1%Q/in%R*(1._8/Tinit-1._8/f1%To))
             Vinit=V1
          CASE(2)
             f1=r%flow(1)
             f2=r%flow(2)
             V1=f1%Vo*(tau0/(f1%c0+r%mu0*p%sig))**(f1%n) &
                     *DEXP(-r%alpha*f1%n*Sinit*lg10) &
                     *(p%sig/r%sigma0)**(r%beta*f1%n-f1%zeta) &
                     *DEXP(-f1%Q/in%R*(1._8/Tinit-1._8/f1%To))
             V2=f2%Vo*(tau0/(f2%c0+r%mu0*p%sig))**(f2%n) &
                     *DEXP(-r%alpha*f2%n*Sinit*lg10) &
                     *(p%sig/r%sigma0)**(r%beta*f2%n-f2%zeta) &
                     *DEXP(-f2%Q/in%R*(1._8/Tinit-1._8/f2%To))
             Vinit=V1+V2
          CASE(3)
             f1=r%flow(1)
             f2=r%flow(2)
             f3=r%flow(3)
             V1=f1%Vo*(tau0/(f1%c0+r%mu0*p%sig))**(f1%n) &
                     *DEXP(-r%alpha*f1%n*Sinit*lg10) &
                     *(p%sig/r%sigma0)**(r%beta*f1%n-f1%zeta) &
                     *DEXP(-f1%Q/in%R*(1._8/Tinit-1._8/f1%To))
             V2=f2%Vo*(tau0/(f2%c0+r%mu0*p%sig))**(f2%n) &
                     *DEXP(-r%alpha*f2%n*Sinit*lg10) &
                     *(p%sig/r%sigma0)**(r%beta*f2%n-f2%zeta) &
                     *DEXP(-f2%Q/in%R*(1._8/Tinit-1._8/f2%To))
             V3=f3%Vo*(tau0/(f3%c0             ))**(f3%n) &
                     *(p%sig/r%sigma0)**(           -f3%zeta) &
                     *DEXP(-f3%Q/in%R*(1._8/Tinit-1._8/f3%To))
             Vinit=V1+V2+V3
          CASE DEFAULT
             WRITE (0,'("incorrect flow law ", I2)') r%nFlow
             WRITE_DEBUG_INFO(100)
             STOP 3
          END SELECT
       END IF

       ! traction in strike direction
       y(l+STATE_VECTOR_TRACTION_STRIKE) = tau0*COS(p%rake)

       ! traction in dip direction
       y(l+STATE_VECTOR_TRACTION_DIP) = tau0*SIN(p%rake)

       ! traction in normal direction
       y(l+STATE_VECTOR_TRACTION_NORMAL) = 0._8

       ! state variable log10(theta)
       y(l+STATE_VECTOR_STATE_1) = Sinit

       ! maximum velocity
       vMax=MAX(p%Vl,vMax)

       ! maximum temperature
       tMax=MAX(Tinit,tMax)

       ! moment-rate
       momentRate=momentRate+(Vinit-p%Vl)*in%mu*area

       ! slip velocity log10(V)
       y(l+STATE_VECTOR_VELOCITY) = LOG(Vinit)/lg10

       ! initial temperature
       y(l+STATE_VECTOR_TEMPERATURE) = Tinit

       l=l+in%dPatch

    END DO

  END SUBROUTINE initStateVector

  !-----------------------------------------------------------------------
  !> subroutine odefun
  ! evalutes the derivative of the state vector
  !
  ! @param n - number of state elements own by current thread
  ! @param m - degrees of freedom
  !
  ! DESCRIPTION:
  !   1- extract slip velocity and strain rate from state vector
  !   2- calculate the rate of traction and rate of stress
  !   3- calculate the rate of remaining state variables
  !----------------------------------------------------------------------
  SUBROUTINE odefun(n,time,y,dydt)
    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: n
    REAL*8, INTENT(IN)    :: time
    REAL*8, INTENT(IN)    :: y(n)
    REAL*8, INTENT(INOUT) :: dydt(n)

    INTEGER :: i,j,k,l,ierr
    INTEGER :: elementType,elementIndex
    TYPE(PATCH_ELEMENT_STRUCT) :: p
    TYPE(ROCK_STRUCT) :: r
    TYPE(HEALING_STRUCT) :: h1,h2
    TYPE(FLOW_STRUCT) :: f1,f2,f3

    REAL*8 :: correction

    ! traction components in the strike and dip directions
    REAL*8 :: ts, td

    ! scalar shear traction and rate of change
    REAL*8 :: tau,dtau

    ! slip velocity in the strike direction
    REAL*8 :: velocity,V1,V2,V3

    ! temperature
    REAL*8 :: Temperature

    ! slip velocity in the strike and dip directions
    REAL*8 :: vs,vd

    ! rake of traction and velocity
    REAL*8 :: rake

    ! normal stress
    REAL*8 :: sigma

    ! patch area
    REAL*8 :: area

    ! effective parameters
    REAL*8 :: nt,Qt,zetat,mut

    ! maximum velocity
    vMax=0._8

    ! initialize moment-rate
    momentRate=0._8

    ! initialize rate of state vector
    dydt=0._8

    !--------------------------------------------------------------------
    ! step 1/3 - extract slip velocity and strain rate from state vector
    !--------------------------------------------------------------------

    ! element index in v vector
    k=1
    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementType= layout%elementType(i)
       elementIndex=layout%elementIndex(i)

       SELECT CASE (elementType)
       CASE (FLAG_RECTANGLE)
          p=in%rectangle%s(elementIndex)
          area=in%rectangle%length(elementIndex)*in%rectangle%width(elementIndex)
       CASE (FLAG_TRIANGLE)
          p=in%triangle%s(elementIndex)
          area=in%triangle%area(elementIndex)
       CASE DEFAULT
          WRITE(STDERR,'("wrong case: this is a bug.")')
          WRITE_DEBUG_INFO(-1)
          STOP 2
       END SELECT

       ! traction and rake
       ts=y(l+STATE_VECTOR_TRACTION_STRIKE)
       td=y(l+STATE_VECTOR_TRACTION_DIP)
       rake=ATAN2(td,ts)

       ! slip velocity
       velocity=DEXP(y(l+STATE_VECTOR_VELOCITY)*lg10)
       vs=velocity*COS(rake)
       vd=velocity*SIN(rake)

       ! maximum velocity
       vMax=MAX(velocity,vMax)

       ! maximum temperature
       tMax=MAX(y(l+STATE_VECTOR_TEMPERATURE),tMax)

       ! moment-rate
       momentRate=momentRate+(velocity-p%Vl)*in%mu*area

       ! update state vector (rate of slip components)
       dydt(l+STATE_VECTOR_SLIP_STRIKE)=vs
       dydt(l+STATE_VECTOR_SLIP_DIP   )=vd

       ! v(k:k+layout%elementVelocityDGF(i)-1) = slip velocity
       v(k:k+layout%elementVelocityDGF(i)-1)=(/ &
                     vs-p%Vl*COS(p%rake), &
                     vd-p%Vl*SIN(p%rake) /)

       l=l+in%dPatch

       k=k+layout%elementVelocityDGF(i)
    END DO

    ! all threads gather velocity from all threads
    CALL MPI_ALLGATHERV(v,layout%listVelocityN(1+rank),MPI_REAL8, &
                        vAll,layout%listVelocityN,layout%listVelocityOffset,MPI_REAL8, &
                        MPI_COMM_WORLD,ierr)

    !-----------------------------------------------------------------
    ! step 2/3 - calculate the rate of traction and rate of stress
    !-----------------------------------------------------------------

#ifdef __BLAS__
    ! use the BLAS library to compute the matrix vector product
    CALL DGEMV("T",SIZE(G,1),SIZE(G,2), &
                1._8,G,SIZE(G,1),vAll,1,0.d0,t,1)
#else
    ! slower, Fortran intrinsic
    t=MATMUL(TRANSPOSE(G),vAll)
#endif

    !-----------------------------------------------------------------
    ! step 3/3 - calculate the rate of remaining state variables
    !-----------------------------------------------------------------

    ! element index in t vector
    j=1
    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementType= layout%elementType(i)
       elementIndex=layout%elementIndex(i)

       SELECT CASE (elementType)
       CASE (FLAG_RECTANGLE)
          p=in%rectangle%s(elementIndex)
       CASE (FLAG_TRIANGLE)
          p=in%triangle%s(elementIndex)
       CASE DEFAULT
          WRITE(STDERR,'("wrong case: this is a bug.")')
          WRITE_DEBUG_INFO(-1)
          STOP 2
       END SELECT

       ! current rock
       r=in%rock(p%rockType)

       ! slip velocity
       velocity=DEXP(y(l+STATE_VECTOR_VELOCITY)*lg10)

       ! traction and rake
       ts=y(l+STATE_VECTOR_TRACTION_STRIKE)
       td=y(l+STATE_VECTOR_TRACTION_DIP)
       tau=SQRT(ts**2+td**2)
       rake=ATAN2(td,ts)

       ! temperature
       temperature=y(l+STATE_VECTOR_TEMPERATURE)

       ! rate of state
       SELECT CASE (evolutionLaw)
       CASE(1)
          ! aging-law end-member
          SELECT CASE (r%nHealing)
          CASE(1)
             h1=r%healing(1)
             dydt(l+STATE_VECTOR_STATE_1)=( &
                     h1%f0*DEXP(-h1%p*y(l+STATE_VECTOR_STATE_1)*lg10)/h1%p &
                     *(p%sig/r%sigma0)**h1%q &
                     *DEXP(-h1%H/in%R*(1._8/temperature-1._8/h1%To)) &
                     -r%lambda*velocity/2/p%h)/lg10
          CASE(2)
             h1=r%healing(1)
             h2=r%healing(2)
             dydt(l+STATE_VECTOR_STATE_1)=( &
                     h1%f0*DEXP(-h1%p*y(l+STATE_VECTOR_STATE_1)*lg10)/h1%p &
                     *(p%sig/r%sigma0)**h1%q &
                     *DEXP(-h1%H/in%R*(1._8/temperature-1._8/h1%To)) &
                    +h2%f0*DEXP(-h2%p*y(l+STATE_VECTOR_STATE_1)*lg10)/h2%p &
                     *(p%sig/r%sigma0)**h2%q &
                     *DEXP(-h2%H/in%R*(1._8/temperature-1._8/h2%To)) &
                     -r%lambda*velocity/2/p%h)/lg10
          CASE DEFAULT
             WRITE (0,'("unhandled option ", I2, " (this is a bug)")') r%nHealing
             WRITE_DEBUG_INFO(100)
             STOP 3
          END SELECT
       CASE(2)
          ! slip-law end-member
          SELECT CASE (r%nHealing)
          CASE(1)
             h1=r%healing(1)
             dydt(l+STATE_VECTOR_STATE_1)=(r%lambda*velocity*LOG( &
                     2*p%h/r%lambda/velocity*( &
                      h1%f0*DEXP(-h1%p*y(l+STATE_VECTOR_STATE_1)*lg10)/h1%p &
                     *(p%sig/r%sigma0)**h1%q &
                     *DEXP(-h1%H/in%R*(1._8/temperature-1._8/h1%To)) &
                     )))/lg10
          CASE(2)
             h1=r%healing(1)
             h2=r%healing(2)
             dydt(l+STATE_VECTOR_STATE_1)=(r%lambda*velocity*LOG( &
                     2*p%h/r%lambda/velocity*( &
                      h1%f0*DEXP(-h1%p*y(l+STATE_VECTOR_STATE_1)*lg10)/h1%p &
                     *(p%sig/r%sigma0)**h1%q &
                     *DEXP(-h1%H/in%R*(1._8/temperature-1._8/h1%To)) &
                     +h2%f0*DEXP(-h2%p*y(l+STATE_VECTOR_STATE_1)*lg10)/h2%p &
                     *(p%sig/r%sigma0)**h2%q &
                     *DEXP(-h2%H/in%R*(1._8/temperature-1._8/h2%To)) &
                     )))/lg10
          CASE DEFAULT
             WRITE (0,'("unhandled option ", I2, " (this is a bug)")') r%nHealing
             WRITE_DEBUG_INFO(100)
             STOP 3
          END SELECT
       CASE DEFAULT
          WRITE (0,'("unhandled option ", I2, " (this is a bug)")') evolutionLaw
       WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT
      
       ! rate of temperature
       dydt(l+STATE_VECTOR_TEMPERATURE)=-p%DW2*(temperature-p%Tb)+tau*velocity/p%wRhoC

       ! scalar rate of shear traction
       dtau=t(j+TRACTION_VECTOR_STRIKE)*COS(rake) &
           +t(j+TRACTION_VECTOR_DIP)   *SIN(rake)
          
       ! normal stress
       sigma=p%sig-y(l+STATE_VECTOR_TRACTION_NORMAL)
       !sigma=p%sig

       ! effective frictional properties
       SELECT CASE(r%nFlow)
       CASE(1)
          f1=r%flow(1)
          nt=f1%n
          Qt=f1%Q
          zetat=f1%zeta
          mut=r%mu0*sigma/(f1%c0+r%mu0*sigma)
       CASE(2)
          f1=r%flow(1)
          f2=r%flow(2)
          V1=f1%Vo*(tau/(f1%c0+r%mu0*p%sig))**f1%n &
                  *DEXP(-r%alpha*f1%n*y(l+STATE_VECTOR_STATE_1)*lg10) &
                  *(p%sig/r%sigma0)**(r%beta*f1%n-f1%zeta) &
                  *DEXP(-f1%Q/in%R*(1._8/temperature-1._8/f1%To))
          V2=f2%Vo*(tau/(f2%c0+r%mu0*p%sig))**(f2%n) &
                  *DEXP(-r%alpha*f2%n*y(l+STATE_VECTOR_STATE_1)*lg10) &
                  *(p%sig/r%sigma0)**(r%beta*f2%n-f2%zeta) &
                  *DEXP(-f2%Q/in%R*(1._8/temperature-1._8/f2%To))
          nt=(V1*f1%n+V2*f2%n)/(V1+V2)
          Qt=(V1*f1%Q+V2*f2%Q)/(V1+V2)
          zetat=(V1*f1%zeta+V2*f2%zeta)/(V1+V2)
          mut=(r%mu0*sigma/(f1%c0+r%mu0*sigma)*V1 &
              +r%mu0*sigma/(f2%c0+r%mu0*sigma)*V2)/(V1+V2)
       CASE(3)
          f1=r%flow(1)
          f2=r%flow(2)
          f3=r%flow(3)
          V1=f1%Vo*(tau/(f1%c0+r%mu0*p%sig))**(f1%n) &
                  *DEXP(-r%alpha*f1%n*y(l+STATE_VECTOR_STATE_1)*lg10) &
                  *(p%sig/r%sigma0)**(r%beta*f1%n-f1%zeta) &
                  *DEXP(-f1%Q/in%R*(1._8/temperature-1._8/f1%To))
          V2=f2%Vo*(tau/(f2%c0+r%mu0*p%sig))**(f2%n) &
                  *DEXP(-r%alpha*f2%n*y(l+STATE_VECTOR_STATE_1)*lg10) &
                  *(p%sig/r%sigma0)**(r%beta*f2%n-f2%zeta) &
                  *DEXP(-f2%Q/in%R*(1._8/temperature-1._8/f2%To))
          V3=f3%Vo*(tau/(f3%c0             ))**(f3%n) &
                  *(p%sig/r%sigma0)**(           -f3%zeta) &
                  *DEXP(-f3%Q/in%R*(1._8/temperature-1._8/f3%To))
          nt=(V1*f1%n+V2*f2%n+V3*f3%n)/(V1+V2+V3)
          Qt=(V1*f1%Q+V2*f2%Q+V3*f3%Q)/(V1+V2+V3)
          zetat=(V1*f1%zeta+V2*f2%zeta+V3*f3%zeta)/(V1+V2+V3)
          mut=(r%mu0*sigma/(f1%c0+r%mu0*sigma)*V1 &
              +r%mu0*sigma/(f2%c0+r%mu0*sigma)*V2)/(V1+V2+V3)
       CASE DEFAULT
          WRITE (0,'("invalid flow type ", I2, " (this is a bug)")') r%nFlow
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT

       ! rate of log10(V/Vo)
       dydt(l+STATE_VECTOR_VELOCITY)= &
               (dtau - r%alpha*tau*dydt(l+STATE_VECTOR_STATE_1)*lg10 &
                     + (mut-r%beta+zetat/nt)*tau*dydt(l+STATE_VECTOR_TRACTION_NORMAL)/sigma &
                     + Qt*tau/nt/in%R*dydt(l+STATE_VECTOR_TEMPERATURE)/temperature**2) &
              /(tau/nt+in%damping*velocity) / lg10
      
       ! correction
       correction=in%damping*velocity*dydt(l+STATE_VECTOR_VELOCITY)*lg10

       ! traction rate
       dydt(l+STATE_VECTOR_TRACTION_STRIKE)=t(j+TRACTION_VECTOR_STRIKE)-correction*COS(rake)
       dydt(l+STATE_VECTOR_TRACTION_DIP   )=t(j+TRACTION_VECTOR_DIP   )-correction*SIN(rake)
       dydt(l+STATE_VECTOR_TRACTION_NORMAL)=t(j+TRACTION_VECTOR_NORMAL)

       l=l+in%dPatch

       j=j+layout%elementForceDGF(i)
    END DO

  END SUBROUTINE odefun

  !-----------------------------------------------------------------------
  !> subroutine initParallelism()
  !! initialize variables describe the data layout
  !! for parallelism.
  !!
  !! OUTPUT:
  !! layout    - list of receiver type and type index
  !-----------------------------------------------------------------------
  SUBROUTINE initParallelism(in,layout)
    IMPLICIT NONE
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in
    TYPE(LAYOUT_STRUCT), INTENT(OUT) :: layout

    INTEGER :: i,j,k,ierr,remainder
    INTEGER :: cumulativeIndex,cumulativeVelocityIndex
    INTEGER :: rank,csize
    INTEGER :: nElements

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

    ! list of number of elements in thread
    ALLOCATE(layout%listElements(csize), &
             layout%listOffset(csize),STAT=ierr)
    IF (ierr>0) STOP "could not allocate the list"

    ! total number of elements
    nElements=in%rectangle%ns+in%triangle%ns

    remainder=nElements-INT(nElements/csize)*csize
    IF (0 .LT. remainder) THEN
       layout%listElements(1:(csize-remainder))      =INT(nElements/csize)
       layout%listElements((csize-remainder+1):csize)=INT(nElements/csize)+1
    ELSE
       layout%listElements(1:csize)=INT(nElements/csize)
    END IF

    ! element start index in thread
    j=0
    k=0
    DO i=1,csize
       j=k+1
       k=k+layout%listElements(i)
       layout%listOffset(i)=j
    END DO

    ALLOCATE(layout%elementType       (layout%listElements(1+rank)), &
             layout%elementIndex      (layout%listElements(1+rank)), &
             layout%elementStateIndex (layout%listElements(1+rank)), &
             layout%elementVelocityDGF(layout%listElements(1+rank)), &
             layout%elementVelocityIndex(layout%listElements(1+rank)), &
             layout%elementStateDGF   (layout%listElements(1+rank)), &
             layout%elementForceDGF   (layout%listElements(1+rank)),STAT=ierr)
    IF (ierr>0) STOP "could not allocate the layout elements"

    j=1
    cumulativeIndex=0
    cumulativeVelocityIndex=0
    DO i=1,in%rectangle%ns
       IF ((i .GE. layout%listOffset(1+rank)) .AND. &
           (i .LT. (layout%listOffset(1+rank)+layout%listElements(1+rank)))) THEN
          layout%elementType(j)=FLAG_RECTANGLE
          layout%elementIndex(j)=i
          layout%elementStateIndex(j)=cumulativeIndex+STATE_VECTOR_DGF_PATCH
          cumulativeIndex=layout%elementStateIndex(j)
          layout%elementVelocityDGF(j)=DGF_PATCH
          layout%elementVelocityIndex(j)=cumulativeVelocityIndex+DGF_PATCH
          cumulativeVelocityIndex=layout%elementVelocityIndex(j)
          layout%elementStateDGF(j)=in%dPatch
          layout%elementForceDGF(j)=DGF_VECTOR
          j=j+1
       END IF
    END DO
    DO i=1,in%triangle%ns
       IF (((i+in%rectangle%ns) .GE. layout%listOffset(1+rank)) .AND. &
           ((i+in%rectangle%ns) .LT. (layout%listOffset(1+rank)+layout%listElements(1+rank)))) THEN
          layout%elementType(j)=FLAG_TRIANGLE
          layout%elementIndex(j)=i
          layout%elementStateIndex(j)=cumulativeIndex+STATE_VECTOR_DGF_PATCH
          cumulativeIndex=layout%elementStateIndex(j)
          layout%elementVelocityDGF(j)=DGF_PATCH
          layout%elementVelocityIndex(j)=cumulativeVelocityIndex+DGF_PATCH
          cumulativeVelocityIndex=layout%elementVelocityIndex(j)
          layout%elementStateDGF(j)=in%dPatch
          layout%elementForceDGF(j)=DGF_VECTOR
          j=j+1
       END IF
    END DO

    ! for MPI_ALLGATHERV
    ALLOCATE(layout%listVelocityN(csize), &
             layout%listVelocityOffset(csize), &
             layout%listStateN(csize), &
             layout%listStateOffset(csize), &
             layout%listForceN(csize), &
             STAT=ierr)
    IF (ierr>0) STOP "could not allocate the size list"

    ! share number of elements in threads
    CALL MPI_ALLGATHER(SUM(layout%elementVelocityDGF),1,MPI_INTEGER,layout%listVelocityN,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
    CALL MPI_ALLGATHER(SUM(layout%elementStateDGF),   1,MPI_INTEGER,layout%listStateN,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
    CALL MPI_ALLGATHER(SUM(layout%elementForceDGF),   1,MPI_INTEGER,layout%listForceN,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)

    j=0
    k=0
    DO i=1,csize
       j=k+1
       k=k+layout%listVelocityN(i)
       layout%listVelocityOffset(i)=j-1
    END DO

    j=0
    k=0
    DO i=1,csize
       j=k+1
       k=k+layout%listStateN(i)
       layout%listStateOffset(i)=j-1
    END DO

  END SUBROUTINE initParallelism

  !-----------------------------------------------------------------------
  !> subroutine initGeometry
  ! initializes the position and local reference system vectors
  !
  ! INPUT:
  ! @param in      - input parameters data structure
  !-----------------------------------------------------------------------
  SUBROUTINE initGeometry(in)
    USE stuart97
    USE okada92
    USE types_3d_thermobaric
    TYPE(SIMULATION_STRUCT), INTENT(INOUT) :: in

    IF (0 .LT. in%rectangle%ns) THEN
       CALL computeReferenceSystemOkada92( &
                in%rectangle%ns, &
                in%rectangle%x, &
                in%rectangle%length, &
                in%rectangle%width, &
                in%rectangle%strike, &
                in%rectangle%dip, &
                in%rectangle%sv, &
                in%rectangle%dv, &
                in%rectangle%nv, &
                in%rectangle%xc)
    END IF

    IF (0 .LT. in%triangle%ns) THEN
       CALL computeReferenceSystemStuart97( &
                in%triangle%nVe, &
                in%triangle%v, &
                in%triangle%ns, &
                in%triangle%i1, &
                in%triangle%i2, &
                in%triangle%i3, &
                in%triangle%sv, &
                in%triangle%dv, &
                in%triangle%nv, &
                in%triangle%xc)
    END IF

  END SUBROUTINE initGeometry

  !---------------------------------------------------------------------
  !> subroutine init
  !! reads simulation parameters from the standard input and initialize
  !! model parameters.
  !!
  !! INPUT:
  !! @param unit - the unit number used to read input data
  !!
  !! OUTPUT:
  !! @param in
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !---------------------------------------------------------------------
  SUBROUTINE init(in)
    USE types_3d_thermobaric
    USE getopt_m

    TYPE(SIMULATION_STRUCT), INTENT(OUT) :: in

    CHARACTER :: ch
    CHARACTER(512) :: dataline
    CHARACTER(256) :: filename
    INTEGER :: iunit,noptions
!$  INTEGER :: omp_get_num_procs,omp_get_max_threads
    TYPE(OPTION_S) :: opts(16)
    REAL*8, DIMENSION(3) :: normal

    INTEGER :: i,k,rank,size,ierr,position
    INTEGER :: maxVertexIndex=-1
    INTEGER, PARAMETER :: psize=1024
    INTEGER :: dummy
    CHARACTER, DIMENSION(psize) :: packed

    TYPE(PATCH_ELEMENT_STRUCT) :: patchMin, patchMax

    REAL*8 :: d_diff,w_diff,w_heat,rhoc
    REAL*8 :: d_diff_min,w_diff_min,w_heat_min,rhoc_min
    REAL*8 :: d_diff_max,w_diff_max,w_heat_max,rhoc_max
  
    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,size,ierr)

    ! define long options, such as --dry-run
    ! parse the command line for options
    opts( 1)=OPTION_S("version",.FALSE.,CHAR(21))
    opts( 2)=OPTION_S("dry-run",.FALSE.,CHAR(22))
    opts( 3)=OPTION_S("epsilon",.TRUE.,'e')
    opts( 4)=OPTION_S("export-greens",.TRUE.,'g')
    opts( 5)=OPTION_S("export-netcdf",.FALSE.,'n')
    opts( 6)=OPTION_S("export-slip",.FALSE.,'l')
    opts( 7)=OPTION_S("export-stress",.FALSE.,'t')
    opts( 8)=OPTION_S("export-slip-distribution",.TRUE.,'s')
    opts( 9)=OPTION_S("import-state",.TRUE.,'k')
    opts(10)=OPTION_S("export-state",.FALSE.,'x')
    opts(11)=OPTION_S("evolution-law",.TRUE.,'j')
    opts(12)=OPTION_S("import-greens",.TRUE.,'p')
    opts(13)=OPTION_S("maximum-step",.TRUE.,'m')
    opts(14)=OPTION_S("maximum-iterations",.TRUE.,'i')
    opts(15)=OPTION_S("help",.FALSE.,'h')
    opts(16)=OPTION_S("verbose",.TRUE.,'v')

    noptions=0
    DO
       ch=getopt("he:f:g:i:j:k:lm:np:s:tv:x",opts)
       SELECT CASE(ch)
       CASE(CHAR(0))
          EXIT
       CASE(CHAR(21))
          ! option version
          in%isversion=.TRUE.
       CASE(CHAR(22))
          ! option dry-run
          in%isdryrun=.TRUE.
       CASE('e')
          ! numerical accuracy (variable epsilon sits in the rk module)
          READ(optarg,*) epsilon
          noptions=noptions+1
       CASE('g')
          ! export Greens functions to netcdf file
          READ(optarg,'(a)') in%greensFunctionDirectory
          in%isExportGreens=.TRUE.
          noptions=noptions+1
       CASE('i')
          ! maximum number of iterations
          READ(optarg,*) maximumIterations
          noptions=noptions+1
       CASE('j')
          ! type of evolution law
          READ(optarg,*) evolutionLaw
          noptions=noptions+1
       CASE('l')
          ! export slip in netcdf format
          in%isExportSlip=.TRUE.
       CASE('m')
          ! maximum time step (variable maximumTimeStep sits in the rk module)
          READ(optarg,*) maximumTimeStep
          noptions=noptions+1
       CASE('n')
          ! export in netcdf format
          in%isExportNetcdf=.TRUE.
       CASE('p')
          ! import Greens functions from netcdf file
          READ(optarg,'(a)') in%greensFunctionDirectory
          in%isImportGreens=.TRUE.
          noptions=noptions+1
       CASE('s')
          ! export slip distribution
          READ(optarg,*) exportSlipDistributionRate
          noptions=noptions+1
       CASE('t')
          ! export stress in netcdf format
          in%isExportStress=.TRUE.
       CASE('k')
          ! import state vector
          in%isImportState=.TRUE.
          READ(optarg,'(a)') importStateDir
          noptions=noptions+1
       CASE('x')
          ! export in netcdf format
          in%isExportState=.TRUE.
       CASE('h')
          ! option help
          in%ishelp=.TRUE.
       CASE('v')
          ! verbosity
          READ(optarg,*) verbose
          noptions=noptions+1
       CASE('?')
          WRITE_DEBUG_INFO(100)
          in%ishelp=.TRUE.
          EXIT
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug)")') optopt
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT
       noptions=noptions+1
    END DO

    IF (in%isversion) THEN
       CALL printversion()
       ! abort parameter input
       CALL MPI_FINALIZE(ierr)
       STOP
    END IF

    IF (in%ishelp) THEN
       CALL printhelp()
       ! abort parameter input
       CALL MPI_FINALIZE(ierr)
       STOP
    END IF

    in%nPatch=0
    ! minimum number of dynamic variables for patches
    in%dPatch=STATE_VECTOR_DGF_PATCH
    in%rectangle%ns=0
    in%triangle%ns=0
    in%triangle%nVe=0
    in%nVolume=0
    in%dVolume=STATE_VECTOR_DGF_VOLUME
    in%cuboid%ns=0

    IF (0.EQ.rank) THEN
       PRINT 2000
       PRINT '("# RATESTATE")'
       PRINT '("# quasi-dynamic earthquake simulation in three-dimensional media")'
       PRINT '("# with the integral method.")'
       SELECT CASE(evolutionLaw)
       CASE(1)
          PRINT '("# evolution law: aging-law end-member")'
       CASE(2)
          PRINT '("# evolution law: slip-law end-member")'
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug)")') evolutionLaw
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT
       PRINT '("# numerical accuracy: ",ES11.4)', epsilon
       PRINT '("# maximum iterations: ",I11)', maximumIterations
       PRINT '("# maximum time step: ",ES12.4)', maximumTimeStep
       PRINT '("# number of threads: ",I12)', csize
       IF (in%isExportNetcdf) THEN
          PRINT '("# export velocity to netcdf:      yes")'
       ELSE
          PRINT '("# export velocity to netcdf:       no")'
       END IF
#ifdef NETCDF
       IF (0 .LT. exportSlipDistributionRate) THEN
          PRINT '("# export slip distribution every ",I4," steps")', exportSlipDistributionRate
       END IF
       IF (in%isImportGreens) THEN
          WRITE (STDOUT,'("# import greens function:         yes (",a,")")') TRIM(in%greensFunctionDirectory)
       ELSE IF (in%isExportGreens) THEN
          WRITE (STDOUT,'("# export greens function:         yes (",a,")")') TRIM(in%greensFunctionDirectory)
       END IF
#endif
!$     PRINT '("#     * parallel OpenMP implementation with ",I3.3,"/",I3.3," threads")', &
!$                  omp_get_max_threads(),omp_get_num_procs()
       PRINT 2000

       IF (noptions .LT. COMMAND_ARGUMENT_COUNT()) THEN
          ! read from input file
          iunit=25
          CALL GET_COMMAND_ARGUMENT(noptions+1,filename)
          OPEN (UNIT=iunit,FILE=filename,IOSTAT=ierr)
       ELSE
          ! get input parameters from standard input
          iunit=5
       END IF

       PRINT '("# output directory")'
       CALL getdata(iunit,dataline)
       READ (dataline,'(a)') in%wdir
       PRINT '(2X,a)', TRIM(in%wdir)

       ! test write permissions on output directory
       in%timeFilename=TRIM(in%wdir)//"/time.dat"
       OPEN (UNIT=FPTIME,FILE=in%timeFilename,POSITION="APPEND",&
               IOSTAT=ierr,FORM="FORMATTED")
       IF (ierr>0) THEN
          WRITE_DEBUG_INFO(102)
          WRITE (STDERR,'("error: unable to access ",a)') TRIM(in%timefilename)
          STOP 1
       END IF
       CLOSE(FPTIME)
   
       PRINT '("# elastic moduli (lambda, mu), radiation damping (G/2Vs), universal gas constant")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%lambda,in%mu,in%damping,in%R
       PRINT '(4ES9.2E1)', in%lambda,in%mu,in%damping,in%R

       IF (0 .GT. in%mu) THEN
          WRITE_DEBUG_INFO(-1)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("input error: shear modulus must be positive")')
          STOP 2
       END IF

       in%nu=in%lambda/2._8/(in%lambda+in%mu)
       IF (-1._8 .GT. in%nu) THEN
          WRITE_DEBUG_INFO(-1)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("input error: Poisson''s ratio must be greater than -1.")')
          STOP 2
       END IF
       IF (0.5_8 .LT. in%nu) THEN
          WRITE_DEBUG_INFO(-1)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("input error: Poisson''s ratio must be lower than 0.5.")')
          STOP 2
       END IF

       IF (0 .GT. in%damping) THEN
          WRITE_DEBUG_INFO(-1)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("input error: radiation damping must be positive")')
          STOP 2
       END IF

       IF (0 .GE. in%R) THEN
          WRITE_DEBUG_INFO(200)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("error in input file: R must be positive.")')
          STOP 1
       END IF

       PRINT '("# time interval")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%interval
       PRINT '(ES20.12E2)', in%interval

       IF (in%interval .LE. 0._8) THEN
          WRITE (STDERR,'("**** error **** ")')
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("simulation time must be positive. exiting.")')
          STOP 1
       END IF

       ! number of rocks
       PRINT '("# number of rocks")'
       CALL getdata(iunit,dataline)
       READ (dataline,*) in%nRock
       PRINT '(2I2)', in%nRock

       ! check for rock rheology model
       IF (0.GE.in%nRock) STOP "could not allocate the state vector"

       ! rheology models
       ALLOCATE(in%rock(in%nRock),STAT=ierr)
       IF (ierr>0) STOP "could not allocate the rheology list"

       DO j=1,in%nRock
          PRINT '("# rock index, rock name")'
          CALL getdata(iunit,dataline)
          READ (dataline,*) i,in%rock(j)%name
          PRINT '(I2,X,a)', i, TRIM(in%rock(j)%name)
          IF (i.NE.j) STOP "index mismatch for rock type"

          PRINT '("#     mu0        d0    sigma0     alpha      beta")'
          CALL getdata(iunit,dataline)
          READ (dataline,*) in%rock(j)%mu0
          READ (dataline,*)  &
                in%rock(j)%mu0, &
                in%rock(j)%d0, &
                in%rock(j)%sigma0, &
                in%rock(j)%alpha, &
                in%rock(j)%beta
   
          PRINT '(F9.3,4ES10.2E1)', &
               in%rock(j)%mu0, &
               in%rock(j)%d0, &
               in%rock(j)%sigma0, &
               in%rock(j)%alpha, &
               in%rock(j)%beta

          PRINT '("# number of flow laws for rock ",I2)',j
          CALL getdata(iunit,dataline)
          READ (dataline,*,IOSTAT=ierr) in%rock(j)%nFlow
          PRINT '(I2)', in%rock(j)%nFlow
          IF (0.GE.in%rock(j)%nFlow) STOP "rocks must have a flow law"

          ! rheology models
          ALLOCATE(in%rock(j)%flow(in%rock(j)%nFlow),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the flow list"

          PRINT '("#  n        Vo        c0         n         Q        To      zeta")'
          DO k=1,in%rock(j)%nFlow
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                   in%rock(j)%flow(k)%Vo, &
                   in%rock(j)%flow(k)%c0, &
                   in%rock(j)%flow(k)%n, &
                   in%rock(j)%flow(k)%Q, &
                   in%rock(j)%flow(k)%To, &
                   in%rock(j)%flow(k)%zeta
             IF (i.NE.k) STOP "flow law index mismatch"

             PRINT '(I4,6ES10.4E1)', i, &
                  in%rock(j)%flow(k)%Vo, &
                  in%rock(j)%flow(k)%c0, &
                  in%rock(j)%flow(k)%n, &
                  in%rock(j)%flow(k)%Q, &
                  in%rock(j)%flow(k)%To, &
                  in%rock(j)%flow(k)%zeta
          END DO
          PRINT '("# number of healing terms for rock ",I2)',j
          CALL getdata(iunit,dataline)
          READ (dataline,*,IOSTAT=ierr) in%rock(j)%nHealing
          PRINT '(I2)', in%rock(j)%nHealing
          IF (0.GE.in%rock(j)%nHealing) STOP "rocks must have at least one healing term."

          ! healing terms
          ALLOCATE(in%rock(j)%healing(in%rock(j)%nHealing),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the healing list"

          PRINT '("#  n        fo         p         q         H        To")'
          DO k=1,in%rock(j)%nHealing
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                   in%rock(j)%healing(k)%f0, &
                   in%rock(j)%healing(k)%p, &
                   in%rock(j)%healing(k)%q, &
                   in%rock(j)%healing(k)%H, &
                   in%rock(j)%healing(k)%To
             IF (i.NE.k) STOP "healing index mismatch"

             PRINT '(I4,5ES10.4E1)', i, &
                  in%rock(j)%healing(k)%f0, &
                  in%rock(j)%healing(k)%p, &
                  in%rock(j)%healing(k)%q, &
                  in%rock(j)%healing(k)%H, &
                  in%rock(j)%healing(k)%To

             IF (0 .GT. in%rock(j)%healing(k)%f0) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'("reference rate of healing must be positive")')
                STOP 1
             END IF

             IF (0 .GT. in%rock(j)%healing(k)%p) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'("size power exponent must be positive")')
                STOP 1
             END IF

             IF (0 .GT. in%rock(j)%healing(k)%q) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'("normal stress power exponent must be positive")')
                STOP 1
             END IF

             IF (0 .GT. in%rock(j)%healing(k)%H) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'("activation energy must be positive")')
                STOP 1
             END IF

             IF (0 .GE. in%rock(j)%healing(k)%To) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'("activation temperature must be position")')
                STOP 1
             END IF

          END DO

          ! remove second healing mechanism with null contribution
          IF (2 .EQ. in%rock(j)%nHealing) THEN
             IF (0._8 .EQ. in%rock(j)%healing(2)%f0) THEN
                in%rock(j)%nHealing=1
             END IF
          END IF

          PRINT '("# reciprocal of characteristic strain for weakening (lambda)")'
          CALL getdata(iunit,dataline)
          READ (dataline,*,IOSTAT=ierr) in%rock(j)%lambda
          PRINT '(2ES9.2E1)', in%rock(j)%lambda
          IF (0._8.GE.in%rock(j)%lambda) STOP "characteristic strain must be positive."

       END DO ! number of rocks

       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !       R E C T A N G L E   P A T C H E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT '("# number of rectangle patches")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%rectangle%ns
       PRINT '(I5)', in%rectangle%ns
       IF (in%rectangle%ns .GT. 0) THEN
          ALLOCATE(in%rectangle%s(in%rectangle%ns), &
                   in%rectangle%x(3,in%rectangle%ns), &
                   in%rectangle%xc(3,in%rectangle%ns), &
                   in%rectangle%length(in%rectangle%ns), &
                   in%rectangle%width(in%rectangle%ns), &
                   in%rectangle%strike(in%rectangle%ns), &
                   in%rectangle%dip(in%rectangle%ns), &
                   in%rectangle%sv(3,in%rectangle%ns), &
                   in%rectangle%dv(3,in%rectangle%ns), &
                   in%rectangle%nv(3,in%rectangle%ns),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the rectangle patch list"
          PRINT 2000
          PRINT '("#    n        Vl       x1       x2      x3  length   width strike   dip   rake")'
          PRINT 2000
          DO k=1,in%rectangle%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                  in%rectangle%s(k)%Vl, &
                  in%rectangle%x(1,k), &
                  in%rectangle%x(2,k), &
                  in%rectangle%x(3,k), &
                  in%rectangle%length(k), &
                  in%rectangle%width(k), &
                  in%rectangle%strike(k), &
                  in%rectangle%dip(k), &
                  in%rectangle%s(k)%rake
             in%rectangle%s(k)%opening=0
   
             IF ((2 .LE. verbose) .OR. (4 .GE. i) .OR. (in%rectangle%ns-3 .LE. i)) THEN

                PRINT '(I6,ES10.2E2,2ES9.2E1,3ES8.2E1,f7.1,f6.1,f7.1)',i, &
                     in%rectangle%s(k)%Vl, &
                     in%rectangle%x(1,k), &
                     in%rectangle%x(2,k), &
                     in%rectangle%x(3,k), &
                     in%rectangle%length(k), &
                     in%rectangle%width(k), &
                     in%rectangle%strike(k), &
                     in%rectangle%dip(k), &
                     in%rectangle%s(k)%rake
             END IF

             ! convert to radians
             in%rectangle%strike(k)=in%rectangle%strike(k)*DEG2RAD     
             in%rectangle%dip(k)=in%rectangle%dip(k)*DEG2RAD     
             in%rectangle%s(k)%rake=in%rectangle%s(k)%rake*DEG2RAD     

             IF (i .NE. k) THEN
                WRITE (STDERR,'("invalid rectangle patch definition")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
             IF (MAX(in%rectangle%length(k),in%rectangle%width(k)) .LE. 0._8) THEN
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: patch length and width must be positive.")')
                STOP 1
             END IF
                
          END DO
   
          ! export rectangle patches
          filename=TRIM(in%wdir)//"/rfaults.flt"
          OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          WRITE (FPOUT,'("#    n              Vl              ", &
                & "x1              x2              x3    ", &
                & "length     width strike    dip   rake")')
          DO k=1,in%rectangle%ns
             WRITE (FPOUT,'(I6,4ES16.7E3,2ES10.4E1,f7.2,f7.2,f7.2)') k, &
                  in%rectangle%s(k)%Vl, &
                  in%rectangle%x(1,k), &
                  in%rectangle%x(2,k), &
                  in%rectangle%x(3,k), &
                  in%rectangle%length(k), &
                  in%rectangle%width(k), &
                  in%rectangle%strike(k)/DEG2RAD, &
                  in%rectangle%dip(k)/DEG2RAD, &
                  in%rectangle%s(k)%rake/DEG2RAD
          END DO
          CLOSE(FPOUT)
                
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          !     M E C H A N I C A L   P R O P E R T I E S
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          PRINT 2000
          PRINT '("# number of rectangle mechanical patches")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) dummy
          PRINT '(I5)', dummy
          IF (dummy .NE. in%rectangle%ns) THEN
             WRITE_DEBUG_INFO(-1)
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE(STDERR,'("input error: all rectangle patches require frictional properties")')
             STOP 2
          END IF

          ! parameter range
          patchMin%tau0=HUGE(1.e0)
          patchMin%sig=HUGE(1.e0)
          patchMin%h=HUGE(1.e0)
          patchMin%rockType=HUGE(1)

          patchMax%tau0=-HUGE(1.e0)
          patchMax%sig=-HUGE(1.e0)
          patchMax%h=-HUGE(1.e0)
          patchMax%rockType=-HUGE(1)

          PRINT '("#    n     tau0      sig rock        h")'
          PRINT 2000
          DO k=1,in%rectangle%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                   in%rectangle%s(k)%tau0, &
                   in%rectangle%s(k)%sig, &
                   in%rectangle%s(k)%rockType, &
                   in%rectangle%s(k)%h
   
             IF ((2 .LE. verbose) .OR. (4 .GE. i) .OR. (in%rectangle%ns-3 .LE. i)) THEN

                PRINT '(I6.6,2ES9.2E1,I5,ES9.2E1)',i, &
                      in%rectangle%s(k)%tau0, &
                      in%rectangle%s(k)%sig, &
                      in%rectangle%s(k)%rockType, &
                      in%rectangle%s(k)%h
                
             END IF
                
             ! parameter range
             patchMin%tau0    =MIN(patchMin%tau0,    in%rectangle%s(k)%tau0)
             patchMin%sig     =MIN(patchMin%sig,     in%rectangle%s(k)%sig)
             patchMin%rockType=MIN(patchMin%rockType,in%rectangle%s(k)%rockType)
             patchMin%h       =MIN(patchMin%h,       in%rectangle%s(k)%h)

             patchMax%tau0    =MAX(patchMax%tau0,    in%rectangle%s(k)%tau0)
             patchMax%sig     =MAX(patchMax%sig,     in%rectangle%s(k)%sig)
             patchMax%rockType=MAX(patchMax%rockType,in%rectangle%s(k)%rockType)
             patchMax%h       =MAX(patchMax%h,       in%rectangle%s(k)%h)

             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'("invalid friction property definition for rectangle patch")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO

          PRINT 2000
          PRINT '("# min:",2ES9.2E1,I5,ES9.2E1)', &
               patchMin%tau0, &
               patchMin%sig, &
               patchMin%rockType, &
               patchMin%h

          PRINT '("# max:",2ES9.2E1,I5,ES9.2E1)', &
               patchMax%tau0, &
               patchMax%sig, &
               patchMax%rockType, &
               patchMax%h
   
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          !   F A U L T   T H E R M A L   P R O P E R T I E S
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          PRINT 2000
          PRINT '("# number of thermal properties")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) dummy
          PRINT '(I5)', dummy
          IF (dummy .NE. in%rectangle%ns) THEN
             WRITE_DEBUG_INFO(-1)
             WRITE(STDERR,'("input error: all rectangle patches require thermal properties")')
             STOP 2
          END IF
          PRINT '("#    n        D        W        w     rhoc       Tb")'
          PRINT 2000

          ! parameter range
          d_diff_min=HUGE(1.e0)
          w_diff_min=HUGE(1.e0)
          w_heat_min=HUGE(1.e0)
          rhoc_min=HUGE(1.e0)
          patchMin%Tb=HUGE(1.e0)
   
          d_diff_max=-HUGE(1.e0)
          w_diff_max=-HUGE(1.e0)
          w_heat_max=-HUGE(1.e0)
          rhoc_max=-HUGE(1.e0)
          patchMax%Tb=-HUGE(1.e0)

          DO k=1,in%rectangle%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                   d_diff,w_diff,w_heat,rhoc, &
                   in%rectangle%s(k)%Tb

             IF ((2 .LE. verbose) .OR. (4 .GE. i) .OR. (in%rectangle%ns-3 .LE. i)) THEN
                PRINT '(I6.6,5ES9.3E1)',i, &
                      d_diff,w_diff,w_heat,rhoc, &
                      in%rectangle%s(k)%Tb
             END IF
                
             ! lumped parameters
             in%rectangle%s(k)%wRhoC=w_heat*rhoc
             in%rectangle%s(k)%DW2=d_diff/w_diff**2

             ! parameter range
             d_diff_min =MIN(d_diff_min, d_diff)
             w_diff_min =MIN(w_diff_min, w_diff)
             w_heat_min =MIN(w_heat_min, w_heat)
             rhoc_min   =MIN(rhoc_min,   rhoc)
             patchMin%Tb=MIN(patchMin%Tb,in%rectangle%s(k)%Tb)

             d_diff_max =MAX(d_diff_max, d_diff)
             w_diff_max =MAX(w_diff_max, w_diff)
             w_heat_max =MAX(w_heat_max, w_heat)
             rhoc_max   =MAX(rhoc_max,   rhoc)
             patchMax%Tb=MAX(patchMax%Tb,in%rectangle%s(k)%Tb)
   
             IF (i .ne. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'("invalid thermal friction property for rectangle patch")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO
   
          PRINT 2000
          PRINT '("# min:",5ES9.3E1)', &
               d_diff_min, &
               w_diff_min, &
               w_heat_min, &
               rhoc_min, &
               patchMin%Tb

          PRINT '("# max:",5ES9.3E1)', &
               d_diff_max, &
               w_diff_max, &
               w_heat_max, &
               rhoc_max, &
               patchMax%Tb

       END IF
          
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !        T R I A N G L E   P A T C H E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT '("# number of triangle patches")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%triangle%ns
       PRINT '(I5)', in%triangle%ns
       IF (in%triangle%ns .GT. 0) THEN
          ALLOCATE(in%triangle%s(in%triangle%ns), &
                   in%triangle%i1(in%triangle%ns), &
                   in%triangle%i2(in%triangle%ns), &
                   in%triangle%i3(in%triangle%ns), &
                   in%triangle%area(in%triangle%ns), &
                   in%triangle%sv(3,in%triangle%ns), &
                   in%triangle%dv(3,in%triangle%ns), &
                   in%triangle%nv(3,in%triangle%ns), &
                   in%triangle%xc(3,in%triangle%ns),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the triangle patch list"
          PRINT 2000
          PRINT '("#    n       Vl        i1        i2        i3   rake")'
          PRINT 2000
          DO k=1,in%triangle%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                  in%triangle%s(k)%Vl, &
                  in%triangle%i1(k), &
                  in%triangle%i2(k), &
                  in%triangle%i3(k), &
                  in%triangle%s(k)%rake
             in%triangle%s(k)%opening=0
   
             PRINT '(I6,ES9.2E1,3I10,f7.1)',i, &
                  in%triangle%s(k)%Vl, &
                  in%triangle%i1(k), &
                  in%triangle%i2(k), &
                  in%triangle%i3(k), &
                  in%triangle%s(k)%rake
                
             ! convert to radians
             in%triangle%s(k)%rake=in%triangle%s(k)%rake*DEG2RAD     

             ! check range of vertex index
             IF (in%triangle%i1(k) .GT. maxVertexIndex) THEN
                maxVertexIndex=in%triangle%i1(k)
             END IF
             IF (in%triangle%i2(k) .GT. maxVertexIndex) THEN
                maxVertexIndex=in%triangle%i2(k)
             END IF
             IF (in%triangle%i3(k) .GT. maxVertexIndex) THEN
                maxVertexIndex=in%triangle%i3(k)
             END IF
             IF ((0 .GT. in%triangle%i1(k)) .OR. &
                 (0 .GT. in%triangle%i2(k)) .OR. &
                 (0 .GT. in%triangle%i3(k))) THEN
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: negative index")')
                STOP 1
             END IF

             IF (i .NE. k) THEN
                WRITE (STDERR,'("error in input file: unexpected index")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("invalid triangle patch definition ")')
                STOP 1
             END IF
          END DO
                
          PRINT '("# number of vertices")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) in%triangle%nVe
          PRINT '(I5)', in%triangle%nVe
          IF (maxVertexIndex .GT. in%triangle%nVe) THEN
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE (STDERR,'("error in input file: not enough vertices")')
             STOP 1
          END IF
          IF (in%triangle%nVe .GT. 0) THEN
             ALLOCATE(in%triangle%v(3,in%triangle%nVe),STAT=ierr)
             IF (ierr>0) STOP "could not allocate the list of vertices"
             PRINT 2000
             PRINT '("#    n      x1       x2       x3")'
             PRINT 2000
             DO k=1,in%triangle%nVe
                CALL getdata(iunit,dataline)
                READ (dataline,*,IOSTAT=ierr) i, &
                      in%triangle%v(1,k), &
                      in%triangle%v(2,k), &
                      in%triangle%v(3,k)
   
                PRINT '(I3.3,3ES9.2E1)',i, &
                     in%triangle%v(1,k), &
                     in%triangle%v(2,k), &
                     in%triangle%v(3,k)
                
                IF (i .NE. k) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("invalid vertex definition ")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   WRITE (STDERR,'("error in input file: unexpected index")')
                   STOP 1
                END IF
             END DO
                
             ! area of triangle elements
             DO k=1,in%triangle%ns
                ! area
                CALL cross( &
                       in%triangle%v(:,in%triangle%i1(k))-in%triangle%v(:,in%triangle%i2(k)), &
                       in%triangle%v(:,in%triangle%i1(k))-in%triangle%v(:,in%triangle%i3(k)), &
                       normal)
                in%triangle%area(k)=NORM2(normal)/2
             END DO

             ! export the triangle patches
             filename=TRIM(in%wdir)//"/tfaults.tri"
             OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
             IF (ierr>0) THEN
                WRITE_DEBUG_INFO(102)
                WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
                STOP 1
             END IF
             WRITE (FPOUT,'("#    n       Vl        i1        i2        i3   rake")')
             DO k=1,in%triangle%ns
                WRITE (FPOUT,'(I6,ES9.2E1,3I10,f7.1)') k, &
                  in%triangle%s(k)%Vl, &
                  in%triangle%i1(k), &
                  in%triangle%i2(k), &
                  in%triangle%i3(k), &
                  in%triangle%s(k)%rake
             END DO
             CLOSE(FPOUT)
                
             filename=TRIM(in%wdir)//"/tfaults.ned"
             OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
             IF (ierr>0) THEN
                WRITE_DEBUG_INFO(102)
                WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
                STOP 1
             END IF
             WRITE (FPOUT,'("#    n      x1       x2       x3")')
             DO k=1,in%triangle%ns
                WRITE (FPOUT,'(I3.3,3ES9.2E1)') k, &
                     in%triangle%v(1,k), &
                     in%triangle%v(2,k), &
                     in%triangle%v(3,k)
             END DO
             CLOSE(FPOUT)
                
          END IF
   
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          !     M E C H A N I C A L   P R O P E R T I E S
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          PRINT 2000
          PRINT '("# number of triangle patches for mechanical properties")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) dummy
          PRINT '(I5)', dummy
          IF (dummy .NE. in%triangle%ns) THEN
             WRITE_DEBUG_INFO(-1)
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE(STDERR,'("input error: all triangle patches require mechanical properties")')
             STOP 2
          END IF

          ! parameter range
          patchMin%tau0=HUGE(1.e0)
          patchMin%sig=HUGE(1.e0)
          patchMin%h=HUGE(1.e0)
          patchMin%rockType=HUGE(1)

          patchMax%tau0=-HUGE(1.e0)
          patchMax%sig=-HUGE(1.e0)
          patchMax%h=-HUGE(1.e0)
          patchMax%rockType=-HUGE(1)

          PRINT '("#   n     tau0      sig rock      h")'
          PRINT 2000
          DO k=1,in%triangle%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                   in%triangle%s(k)%tau0, &
                   in%triangle%s(k)%sig, &
                   in%triangle%s(k)%rockType, &
                   in%triangle%s(k)%h
   
             IF ((2 .LE. verbose) .OR. (4 .GE. i) .OR. (in%triangle%ns-3 .LE. i)) THEN
                PRINT '(I6.6,2ES9.2E1,I5,ES9.2E1)',i, &
                      in%triangle%s(k)%tau0, &
                      in%triangle%s(k)%sig, &
                      in%triangle%s(k)%rockType, &
                      in%triangle%s(k)%h
             END IF
             
             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'("invalid friction property definition for triangle patch")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF

             ! parameter range
             patchMin%tau0    =MIN(patchMin%tau0,    in%triangle%s(k)%tau0)
             patchMin%sig     =MIN(patchMin%sig,     in%triangle%s(k)%sig)
             patchMin%rockType=MIN(patchMin%rockType,in%triangle%s(k)%rockType)
             patchMin%h       =MIN(patchMin%h,       in%triangle%s(k)%h)

             patchMax%tau0    =MAX(patchMax%tau0,    in%triangle%s(k)%tau0)
             patchMax%sig     =MAX(patchMax%sig,     in%triangle%s(k)%sig)
             patchMax%rockType=MAX(patchMax%rockType,in%triangle%s(k)%rockType)
             patchMax%h       =MAX(patchMax%h,       in%triangle%s(k)%h)

          END DO

          PRINT 2000
          PRINT '("# min",2ES9.2E1,I2,ES9.2E1)', &
               patchMin%tau0, &
               patchMin%sig, &
               patchMin%rockType, &
               patchMin%h

          PRINT '("# max",2ES9.2E1,I2,ES9.2E1)', &
               patchMax%tau0, &
               patchMax%sig, &
               patchMax%rockType, &
               patchMax%h
   
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          !   F A U L T   T H E R M A L   P R O P E R T I E S
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          PRINT 2000
          PRINT '("# number of thermal properties")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) dummy
          PRINT '(I5)', dummy
          IF (dummy .NE. in%triangle%ns) THEN
             WRITE_DEBUG_INFO(-1)
             WRITE(STDERR,'("input error: all triangle patches require thermal properties")')
             STOP 2
          END IF
          PRINT '("#   n        D        W        w     rhoc       Tb")'
          PRINT 2000

          ! parameter range
          d_diff_min=HUGE(1.e0)
          w_diff_min=HUGE(1.e0)
          w_heat_min=HUGE(1.e0)
          rhoc_min=HUGE(1.e0)
          patchMin%Tb=HUGE(1.e0)
   
          d_diff_max=-HUGE(1.e0)
          w_diff_max=-HUGE(1.e0)
          w_heat_max=-HUGE(1.e0)
          rhoc_max=-HUGE(1.e0)
          patchMax%Tb=-HUGE(1.e0)

          DO k=1,in%triangle%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                   d_diff,w_diff,w_heat,rhoc, &
                   in%triangle%s(k)%Tb

             PRINT '(I5.5,5ES9.3E1)',i, &
                   d_diff,w_diff,w_heat,rhoc, &
                   in%triangle%s(k)%Tb
                
             ! lumped parameters
             in%triangle%s(k)%wRhoC=w_heat*rhoc
             in%triangle%s(k)%DW2=d_diff/w_diff**2

             ! parameter range
             d_diff_min =MIN(d_diff_min, d_diff)
             w_diff_min =MIN(w_diff_min, w_diff)
             w_heat_min =MIN(w_heat_min, w_heat)
             rhoc_min   =MIN(rhoc_min,   rhoc)
             patchMin%Tb=MIN(patchMin%Tb,in%triangle%s(k)%Tb)

             d_diff_max =MAX(d_diff_max, d_diff)
             w_diff_max =MAX(w_diff_max, w_diff)
             w_heat_max =MAX(w_heat_max, w_heat)
             rhoc_max   =MAX(rhoc_max,   rhoc)
             patchMax%Tb=MAX(patchMax%Tb,in%triangle%s(k)%Tb)
   
             IF (i .ne. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'("invalid thermal property for triangle patch")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO
   
          PRINT 2000
          PRINT '("# min",5ES9.3E1)', &
               d_diff_min, &
               w_diff_min, &
               w_heat_min, &
               rhoc_min, &
               patchMin%Tb

          PRINT '("# max",5ES9.3E1)', &
               d_diff_max, &
               w_diff_max, &
               w_heat_max, &
               rhoc_max, &
               patchMax%Tb

       END IF
          
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !       O B S E R V A T I O N   P A T C H E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of observation patches")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%nObservationState
       PRINT '(I5)', in%nObservationState
       IF (0 .LT. in%nObservationState) THEN
          ALLOCATE(in%observationState(3,in%nObservationState),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the observation patches"
          PRINT 2000
          PRINT '("#   n      i rate")'
          PRINT 2000
          DO k=1,in%nObservationState
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i,in%observationState(1:2,k)

             PRINT '(I5,X,I6,X,I4)', i, in%observationState(1:2,k)

             IF (0 .GE. in%observationState(2,k)) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: invalid subsampling rate")')
                STOP 1
             END IF

             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO
       END IF

#ifdef NETCDF
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !       O B S E R V A T I O N   P R O F I L E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of observation profiles")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%nObservationProfiles
       PRINT '(I5)', in%nObservationProfiles
       IF (0 .LT. in%nObservationProfiles) THEN

          ALLOCATE(in%observationProfileVelocity(in%nObservationProfiles),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the velocity observation profiles"

          IF (in%isExportSlip) THEN
             ALLOCATE(in%observationProfileSlip(in%nObservationProfiles),STAT=ierr)
             IF (ierr>0) STOP "could not allocate the slip observation profiles"
          END IF

          IF (in%isExportStress) THEN
             ALLOCATE(in%observationProfileStress(in%nObservationProfiles),STAT=ierr)
             IF (ierr>0) STOP "could not allocate the stress observation profiles"
          END IF

          IF (in%isExportTemperature) THEN
             ALLOCATE(in%observationProfileTemperature(in%nObservationProfiles),STAT=ierr)
             IF (ierr>0) STOP "could not allocate the temperature observation profiles"
          END IF

          PRINT 2000
          PRINT '("#   n    n offset stride rate")'
          PRINT 2000

          DO k=1,in%nObservationProfiles
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                     in%observationProfileVelocity(k)%n, &
                     in%observationProfileVelocity(k)%offset, &
                     in%observationProfileVelocity(k)%stride, &
                     in%observationProfileVelocity(k)%rate

             PRINT '(I5,X,I4,X,I6,X,I6,X,I4)', i, &
                     in%observationProfileVelocity(k)%n, &
                     in%observationProfileVelocity(k)%offset, &
                     in%observationProfileVelocity(k)%stride, &
                     in%observationProfileVelocity(k)%rate

             IF (0 .GE. in%observationProfileVelocity(k)%n .OR. &
                 0 .GE. in%observationProfileVelocity(k)%stride .OR. &
                 0 .GE. in%observationProfileVelocity(k)%rate) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: invalid profile declaration")')
                STOP 1
             END IF

             IF (0 .GT. in%observationProfileVelocity(k)%offset) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: invalid offset in profile declaration")')
                STOP 1
             END IF

             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF

             IF (in%isExportSlip) THEN
                in%observationProfileSlip(k)%n     =in%observationProfileVelocity(k)%n
                in%observationProfileSlip(k)%offset=in%observationProfileVelocity(k)%offset
                in%observationProfileSlip(k)%stride=in%observationProfileVelocity(k)%stride
                in%observationProfileSlip(k)%rate  =in%observationProfileVelocity(k)%rate
             END IF

             IF (in%isExportStress) THEN
                in%observationProfileStress(k)%n     =in%observationProfileVelocity(k)%n
                in%observationProfileStress(k)%offset=in%observationProfileVelocity(k)%offset
                in%observationProfileStress(k)%stride=in%observationProfileVelocity(k)%stride
                in%observationProfileStress(k)%rate  =in%observationProfileVelocity(k)%rate
             END IF

             IF (in%isExportTemperature) THEN
                in%observationProfileTemperature(k)%n     =in%observationProfileVelocity(k)%n
                in%observationProfileTemperature(k)%offset=in%observationProfileVelocity(k)%offset
                in%observationProfileTemperature(k)%stride=in%observationProfileVelocity(k)%stride
                in%observationProfileTemperature(k)%rate  =in%observationProfileVelocity(k)%rate
             END IF
          END DO
       END IF

       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !       O B S E R V A T I O N   I M A G E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of observation images")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%nObservationImages
       PRINT '(I5)', in%nObservationImages
       IF (0 .LT. in%nObservationImages) THEN
          ALLOCATE(in%observationImageVelocity(in%nObservationImages),STAT=ierr)
          ALLOCATE(in%observationImageVelocityStrike(in%nObservationImages),STAT=ierr)
          ALLOCATE(in%observationImageVelocityDip(in%nObservationImages),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the observation profiles"
          PRINT 2000
          PRINT '("#   n   nl   nw offset stride rate")'
          PRINT 2000
          DO k=1,in%nObservationImages
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                     in%observationImageVelocity(k)%nl, &
                     in%observationImageVelocity(k)%nw, &
                     in%observationImageVelocity(k)%offset, &
                     in%observationImageVelocity(k)%stride, &
                     in%observationImageVelocity(k)%rate

             PRINT '(I5,X,I4,X,I4,X,I6,X,I6,X,I4)', i, &
                     in%observationImageVelocity(k)%nl, &
                     in%observationImageVelocity(k)%nw, &
                     in%observationImageVelocity(k)%offset, &
                     in%observationImageVelocity(k)%stride, &
                     in%observationImageVelocity(k)%rate

             IF (0 .GE. in%observationImageVelocity(k)%nl .OR. &
                 0 .GE. in%observationImageVelocity(k)%nw .OR. &
                 0 .GE. in%observationImageVelocity(k)%stride .OR. &
                 0 .GE. in%observationImageVelocity(k)%rate) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: invalid image declaration")')
                STOP 1
             END IF

             IF (0 .GT. in%observationImageVelocity(k)%offset) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: invalid offset for image declaration")')
                STOP 1
             END IF

             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF

             in%observationImageVelocityStrike=in%observationImageVelocity
             in%observationImageVelocityDip=in%observationImageVelocity
          END DO
       END IF
#endif

       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !        O B S E R V A T I O N   P O I N T S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of observation points")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%nObservationPoint
       PRINT '(I5)', in%nObservationPoint
       IF (0 .LT. in%nObservationPoint) THEN
          ALLOCATE(in%observationPoint(in%nObservationPoint),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the observation points"
          PRINT 2000
          PRINT '("# n       name       x1       x2       x3")'
          PRINT 2000
          DO k=1,in%nObservationPoint
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                     in%observationPoint(k)%name, &
                     in%observationPoint(k)%x(1), &
                     in%observationPoint(k)%x(2), &
                     in%observationPoint(k)%x(3)

             PRINT '(I3.3,X,a20,3ES9.2E1)',i, &
                     in%observationPoint(k)%name, &
                     in%observationPoint(k)%x(1), &
                     in%observationPoint(k)%x(2), &
                     in%observationPoint(k)%x(3)

             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO

          ! export list of observation points
          filename=TRIM(in%wdir)//"/opts.dat"
          OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          WRITE (FPOUT,'("# n       name       x1       x2       x3")')
          DO k=1,in%nObservationPoint
             WRITE (FPOUT,'(I3.3,X,a20,3ES9.2E1)') k, &
                     in%observationPoint(k)%name, &
                     in%observationPoint(k)%x(1), &
                     in%observationPoint(k)%x(2), &
                     in%observationPoint(k)%x(3)
          END DO
          CLOSE(FPOUT)

       END IF

       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !                  E V E N T S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT '("# number of events")'
       CALL getdata(iunit,dataline)
       READ (dataline,*) in%ne
       PRINT '(I5)', in%ne
       IF (in%ne .GT. 0) ALLOCATE(in%event(in%ne),STAT=ierr)
       IF (ierr>0) STOP "could not allocate the event list"
       
       DO i=1,in%ne
          IF (1 .NE. i) THEN
             PRINT '("# time of next event")'
             CALL getdata(iunit,dataline)
             READ (dataline,*) in%event(i)%time
             in%event(i)%i=i-1
             PRINT '(ES9.2E1)', in%event(i)%time
   
             IF (in%event(i)%time .LE. in%event(i-1)%time) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'(a,a)') "input file error. ", &
                     "timing of perturbations must increase, quiting."
                STOP 1
             END IF
          ELSE
             in%event(1)%time=0._8
             in%event(1)%i=0
          END IF
   
       END DO
   
       ! test the presence of dislocations for coseismic calculation
       IF ((in%rectangle%ns .EQ. 0) .AND. &
           (in%triangle%ns .EQ. 0) .OR. &
           (in%interval .LE. 0._8)) THEN
   
          WRITE_DEBUG_INFO(300)
          WRITE (STDERR,'("nothing to do. exiting.")')
          STOP 1
       END IF
   
       PRINT 2000
       ! flush standard output
       CALL FLUSH(6)      

       in%nPatch=in%rectangle%ns+in%triangle%ns

       ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
       ! BROADCAST INPUT PARAMETERS
       ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

       position=0
       CALL MPI_PACK(in%interval,            1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%lambda,              1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%mu,                  1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nu,                  1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%damping,             1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%R,                   1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%rectangle%ns,        1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%triangle%ns,         1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%triangle%nVe,        1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%ne,                  1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nRock,               1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nObservationState,   1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nObservationProfiles,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nObservationPoint,   1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)

       ! send working directory
       CALL MPI_BCAST(in%wdir,256,MPI_CHARACTER,0,MPI_COMM_WORLD,ierr)

       ! send the rocks
       DO i=1,in%nRock
          position=0
          CALL MPI_PACK(in%rock(i)%nFlow,   1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rock(i)%nHealing,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rock(i)%mu0,     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rock(i)%d0,      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rock(i)%sigma0,  1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rock(i)%alpha,   1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rock(i)%beta,    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rock(i)%lambda,  1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(in%rock(i)%name,80,MPI_CHARACTER,0,MPI_COMM_WORLD,ierr)

          ! send the flow laws
          DO j=1,in%rock(i)%nFlow
             position=0
             CALL MPI_PACK(in%rock(i)%flow(j)%Vo,  1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%rock(i)%flow(j)%c0,  1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%rock(i)%flow(j)%n,   1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%rock(i)%flow(j)%Q,   1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%rock(i)%flow(j)%To,  1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%rock(i)%flow(j)%zeta,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          END DO

          ! send the healing terms
          DO j=1,in%rock(i)%nHealing
             position=0
             CALL MPI_PACK(in%rock(i)%healing(j)%f0,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%rock(i)%healing(j)%p, 1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%rock(i)%healing(j)%q, 1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%rock(i)%healing(j)%H, 1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%rock(i)%healing(j)%To,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          END DO
       END DO

       ! send the rectangle patches (geometry and friction properties) 
       DO i=1,in%rectangle%ns
          position=0
          CALL MPI_PACK(in%rectangle%s(i)%Vl,      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%x(1,i),       1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%x(2,i),       1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%x(3,i),       1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%length(i),    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%width(i),     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%strike(i),    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%dip(i),       1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%rake,    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%opening, 1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%tau0,    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%sig,     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%wRhoC,   1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%DW2,     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%Tb,      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%h,       1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%rockType,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the triangle patches (geometry and friction properties) 
       DO i=1,in%triangle%ns
          position=0
          CALL MPI_PACK(in%triangle%s(i)%Vl,      1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%i1(i),        1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%i2(i),        1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%i3(i),        1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%area(i),      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%rake,    1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%opening, 1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%tau0,    1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%sig,     1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%wRhoC,   1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%DW2,     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%Tb,      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%h,       1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%rockType,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the triangle vertices
       IF (0 .NE. in%triangle%ns) THEN
          DO i=1,in%triangle%nVe
             position=0
             CALL MPI_PACK(in%triangle%v(1,i),1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%triangle%v(2,i),1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%triangle%v(3,i),1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          END DO
       END IF

       ! send the observation state
       DO i=1,in%nObservationState
          position=0
          CALL MPI_PACK(in%observationState(:,i),3,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       IF (in%isExportSlip) THEN
          DO i=1,in%nObservationProfiles
             position=0
             CALL MPI_PACK(in%observationProfileSlip(i)%n,     1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%observationProfileSlip(i)%offset,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%observationProfileSlip(i)%stride,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%observationProfileSlip(i)%rate,  1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          END DO
       END IF

       IF (in%isExportStress) THEN
          DO i=1,in%nObservationProfiles
             position=0
             CALL MPI_PACK(in%observationProfileStress(i)%n,     1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%observationProfileStress(i)%offset,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%observationProfileStress(i)%stride,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%observationProfileStress(i)%rate,  1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          END DO
       END IF

       IF (in%isExportTemperature) THEN
          DO i=1,in%nObservationProfiles
             position=0
             CALL MPI_PACK(in%observationProfileTemperature(i)%n,     1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%observationProfileTemperature(i)%offset,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%observationProfileTemperature(i)%stride,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%observationProfileTemperature(i)%rate,  1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          END DO
       END IF

       ! send the observation points
       DO i=1,in%nObservationPoint
          position=0
          CALL MPI_PACK(in%observationPoint(i)%name,20,MPI_CHARACTER,packed,psize,position,MPI_COMM_WORLD,ierr)
          DO k=1,3
             CALL MPI_PACK(in%observationPoint(i)%x(k),1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          END DO
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the perturbation events
       DO k=1,in%ne
          CALL MPI_PACK(in%event(k)%time,1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%event(k)%i,   1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

    ELSE ! if 0.NE.rank

       !------------------------------------------------------------------
       ! S L A V E S
       !------------------------------------------------------------------

       position=0
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%interval,            1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%lambda,              1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%mu,                  1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nu,                  1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%damping,             1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%R,                   1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%rectangle%ns,        1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%triangle%ns,         1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%triangle%nVe,        1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%ne,                  1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nRock,               1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nObservationState,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nObservationProfiles,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nObservationPoint,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)

       ! receive working directory
       CALL MPI_BCAST(in%wdir,256,MPI_CHARACTER,0,MPI_COMM_WORLD,ierr)

       ! rocks
       ALLOCATE(in%rock(in%nRock),STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for rocks"

       ! receive the rocks
       DO k=1,in%nRock
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rock(k)%nFlow,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rock(k)%nHealing,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rock(k)%mu0,     1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rock(k)%d0,      1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rock(k)%sigma0,  1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rock(k)%alpha,   1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rock(k)%beta,    1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rock(k)%lambda,  1,MPI_REAL8  ,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(in%rock(k)%name,80,MPI_CHARACTER,0,MPI_COMM_WORLD,ierr)

          ! flow laws
          ALLOCATE(in%rock(k)%flow(in%rock(k)%nFlow),STAT=ierr)
          IF (ierr>0) STOP "slave could not allocate memory for flow laws"

          ! receive the flow laws
          DO j=1,in%rock(k)%nFlow
             position=0
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%rock(k)%flow(j)%Vo,  1,MPI_REAL8,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%rock(k)%flow(j)%c0,  1,MPI_REAL8,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%rock(k)%flow(j)%n,   1,MPI_REAL8,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%rock(k)%flow(j)%Q,   1,MPI_REAL8,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%rock(k)%flow(j)%To,  1,MPI_REAL8,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%rock(k)%flow(j)%zeta,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          END DO

          ! healing terms
          ALLOCATE(in%rock(k)%healing(in%rock(k)%nHealing),STAT=ierr)
          IF (ierr>0) STOP "slave could not allocate memory for healing terms"

          ! receive the healing terms
          DO j=1,in%rock(k)%nHealing
             position=0
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%rock(k)%healing(j)%f0, 1,MPI_REAL8,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%rock(k)%healing(j)%p,  1,MPI_REAL8,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%rock(k)%healing(j)%q,  1,MPI_REAL8,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%rock(k)%healing(j)%H,  1,MPI_REAL8,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%rock(k)%healing(j)%To, 1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          END DO
       END DO

       IF (0 .LT. in%rectangle%ns) &
                    ALLOCATE(in%rectangle%s(in%rectangle%ns), &
                             in%rectangle%x(3,in%rectangle%ns), &
                             in%rectangle%xc(3,in%rectangle%ns), &
                             in%rectangle%length(in%rectangle%ns), &
                             in%rectangle%width(in%rectangle%ns), &
                             in%rectangle%strike(in%rectangle%ns), &
                             in%rectangle%dip(in%rectangle%ns), &
                             in%rectangle%sv(3,in%rectangle%ns), &
                             in%rectangle%dv(3,in%rectangle%ns), &
                             in%rectangle%nv(3,in%rectangle%ns), &
                             STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory"

       IF (0 .LT. in%triangle%ns) &
                    ALLOCATE(in%triangle%s(in%triangle%ns), &
                             in%triangle%xc(3,in%triangle%ns), &
                             in%triangle%i1(in%triangle%ns), &
                             in%triangle%i2(in%triangle%ns), &
                             in%triangle%i3(in%triangle%ns), &
                             in%triangle%area(in%triangle%ns), &
                             in%triangle%sv(3,in%triangle%ns), &
                             in%triangle%dv(3,in%triangle%ns), &
                             in%triangle%nv(3,in%triangle%ns),STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory"

       IF (0 .LT. in%triangle%nVe) ALLOCATE(in%triangle%v(3,in%triangle%nVe), STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory"

       IF (0 .LT. in%ne) ALLOCATE(in%event(in%ne),STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory"

       DO i=1,in%rectangle%ns
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%Vl,      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%x(1,i),       1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%x(2,i),       1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%x(3,i),       1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%length(i),    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%width(i),     1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%strike(i),    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%dip(i),       1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%rake,    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%opening, 1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%tau0,    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%sig,     1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%wRhoC,   1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%DW2,     1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%Tb,      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%h,       1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%rockType,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       END DO

       DO i=1,in%triangle%ns
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%Vl,      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%i1(i),        1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%i2(i),        1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%i3(i),        1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%area(i),      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%rake,    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%opening, 1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%tau0,    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%sig,     1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%wRhoC,   1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%DW2,     1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%Tb,      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%h,       1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%rockType,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       END DO

       IF (0 .NE. in%triangle%ns) THEN
          DO i=1,in%triangle%nVe
             position=0
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%triangle%v(1,i),1,MPI_REAL8,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%triangle%v(2,i),1,MPI_REAL8,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%triangle%v(3,i),1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          END DO
       END IF

       IF (0 .LT. in%nObservationState) &
                    ALLOCATE(in%observationState(3,in%nObservationState),STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for observation states"

       DO i=1,in%nObservationState
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%observationState(:,i),3,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       END DO

       IF (in%isExportSlip) THEN
          ALLOCATE(in%observationProfileSlip(in%nObservationProfiles),STAT=ierr)
          IF (ierr>0) STOP "slave could not allocate memory for slip observation profiles"
          DO i=1,in%nObservationProfiles
             position=0
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%observationProfileSlip(i)%n,     1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%observationProfileSlip(i)%offset,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%observationProfileSlip(i)%stride,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%observationProfileSlip(i)%rate,  1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          END DO
       END IF

       IF (in%isExportStress) THEN
          ALLOCATE(in%observationProfileStress(in%nObservationProfiles),STAT=ierr)
          IF (ierr>0) STOP "slave could not allocate memory for stress observation profiles"
          DO i=1,in%nObservationProfiles
             position=0
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%observationProfileStress(i)%n,     1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%observationProfileStress(i)%offset,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%observationProfileStress(i)%stride,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%observationProfileStress(i)%rate,  1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          END DO
       END IF

       IF (in%isExportTemperature) THEN
          ALLOCATE(in%observationProfileTemperature(in%nObservationProfiles),STAT=ierr)
          IF (ierr>0) STOP "slave could not allocate memory for temperature observation profiles"
          DO i=1,in%nObservationProfiles
             position=0
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%observationProfileTemperature(i)%n,     1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%observationProfileTemperature(i)%offset,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%observationProfileTemperature(i)%stride,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%observationProfileTemperature(i)%rate,  1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          END DO
       END IF

       IF (0 .LT. in%nObservationPoint) &
                    ALLOCATE(in%observationPoint(in%nObservationPoint), &
                             STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for observation points"

       DO i=1,in%nObservationPoint
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%observationPoint(i)%name,20,MPI_CHARACTER,MPI_COMM_WORLD,ierr)
          DO k=1,3
             CALL MPI_UNPACK(packed,psize,position,in%observationPoint(i)%x(k),1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          END DO
       END DO

       DO i=1,in%ne
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%event(i)%time,1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%event(i)%i,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       END DO

       CALL FLUSH(6)      

       in%nPatch=in%rectangle%ns+in%triangle%ns

    END IF ! master or slaves

2000 FORMAT ("# ----------------------------------------------------------------------------")
   
  END SUBROUTINE init
   
  !-----------------------------------------------
  !> subroutine cross
  !! compute the cross product between two vectors
  !-----------------------------------------------
  SUBROUTINE cross(u,v,w)
    REAL*8, DIMENSION(3), INTENT(IN) :: u,v
    REAL*8, DIMENSION(3), INTENT(OUT) :: w

    w(1)=u(2)*v(3)-u(3)*v(2)
    w(2)=u(3)*v(1)-u(1)*v(3)
    w(3)=u(1)*v(2)-u(2)*v(1)

  END SUBROUTINE cross

  !-----------------------------------------------
  !> function findRoot2
  !! finds the stress that satisfies
  !!
  !!   V = A1 tau^n1 + A2 tau^n2
  !-----------------------------------------------
  REAL*8 FUNCTION findRoot2(A1,n1,A2,n2,V)
    REAL*8, INTENT(IN) :: A1,n1,A2,n2
    REAL*8, INTENT(IN) :: V

    REAL*8 :: V1,V2
    REAL*8 :: tau0,tau1,tau2
    REAL*8, DIMENSION(10) :: extras
    REAL*8, EXTERNAL :: rootbisection

    ! first guess
    tau1=(V/A1)**(1/n1);
    tau2=(V/A2)**(1/n2);
    tau0=2/(1/tau1+1/tau2);

    ! refinement
    V1=A1*tau0**n1;
    V2=A2*tau0**n2;
    tau0=(V1+V2)/(V1/tau1+V2/tau2);

    ! numerical solution
    extras(1:5)=(/ A1,n1,A2,n2,V /)
    findRoot2=rootbisection(polynomial2,extras,tau0*0.80d0,tau0*1.20d0,1d-8);

  END FUNCTION findRoot2

  REAL*8 FUNCTION polynomial2(x,extras)
     IMPLICIT NONE
     REAL*8, INTENT(IN) :: x
     REAL*8, INTENT(IN), DIMENSION(10) :: extras

     polynomial2=extras(1)*x**extras(2)+extras(3)*x**extras(4)-extras(5)
  END FUNCTION polynomial2

  !-----------------------------------------------
  !> function findRoot3
  !! finds the stress that satisfies
  !!
  !!   V = A1 tau^n1 + A2 tau^n2 + A3 tau^n3
  !-----------------------------------------------
  REAL*8 FUNCTION findRoot3(A1,n1,A2,n2,A3,n3,V)
    REAL*8, INTENT(IN) :: A1,n1,A2,n2,A3,n3
    REAL*8, INTENT(IN) :: V

    REAL*8 :: V1,V2,V3
    REAL*8 :: tau0,tau1,tau2,tau3
    REAL*8, DIMENSION(10) :: extras
    REAL*8, EXTERNAL :: rootbisection

    ! first guess
    tau1=(V/A1)**(1/n1);
    tau2=(V/A2)**(1/n2);
    tau3=(V/A3)**(1/n3);
    tau0=3/(1/tau1+1/tau2+1/tau3);

    ! refinement
    V1=A1*tau0**n1;
    V2=A2*tau0**n2;
    V3=A3*tau0**n3;
    tau0=(V1+V2+V3)/(V1/tau1+V2/tau2+V3/tau3);

    ! numerical solution
    extras(1:7)=(/ A1,n1,A2,n2,A3,n3,V /)
    findRoot3=rootbisection(polynomial3,extras,tau0*0.70,tau0*1.30,1d-8)

  END FUNCTION findRoot3

  REAL*8 FUNCTION polynomial3(x,extras)
     IMPLICIT NONE
     REAL*8, INTENT(IN) :: x
     REAL*8, INTENT(IN), DIMENSION(10) :: extras

     polynomial3=extras(1)*x**extras(2)+extras(3)*x**extras(4) &
                +extras(5)*x**extras(6)-extras(7)
  END FUNCTION polynomial3

  SUBROUTINE printRock()
       INTEGER :: i,j,k

       PRINT '("# thread number")'
       PRINT '(I2)', rank

       ! number of rocks
       PRINT '("# number of rocks")'
       PRINT '(2I2)', in%nRock

       DO j=1,in%nRock
          PRINT '("# rock index, rock name")'
          PRINT '(I2,X,a)', j, TRIM(in%rock(j)%name)

          PRINT '("#     mu0        d0    sigma0     alpha      beta")'
          PRINT '(F9.3,4ES10.2E1)', &
               in%rock(j)%mu0, &
               in%rock(j)%d0, &
               in%rock(j)%sigma0, &
               in%rock(j)%alpha, &
               in%rock(j)%beta

          PRINT '("# number of flow laws for rock ",I2)',j
          PRINT '(I2)', in%rock(j)%nFlow

          PRINT '("#  n        Vo        c0         n         Q        To      zeta")'
          DO k=1,in%rock(j)%nFlow
             PRINT '(I4,6ES10.4E1)', i, &
                  in%rock(j)%flow(k)%Vo, &
                  in%rock(j)%flow(k)%c0, &
                  in%rock(j)%flow(k)%n, &
                  in%rock(j)%flow(k)%Q, &
                  in%rock(j)%flow(k)%To, &
                  in%rock(j)%flow(k)%zeta
          END DO
          PRINT '("# number of healing terms for rock ",I2)',j
          PRINT '(I2)', in%rock(j)%nHealing

          PRINT '("#  n        fo         p         q         H        To")'
          DO k=1,in%rock(j)%nHealing
             PRINT '(I4,5ES10.4E1)', i, &
                  in%rock(j)%healing(k)%f0, &
                  in%rock(j)%healing(k)%p, &
                  in%rock(j)%healing(k)%q, &
                  in%rock(j)%healing(k)%H, &
                  in%rock(j)%healing(k)%To
          END DO

          PRINT '("# reciprocal of characteristic strain for weakening (lambda)")'
          PRINT '(2ES9.2E1)', in%rock(j)%lambda

       END DO ! number of rocks
   
  END SUBROUTINE printRock

  !-----------------------------------------------
  !> subroutine printhelp
  !! displays a help message with master thread.
  !-----------------------------------------------
  SUBROUTINE printhelp()

    INTEGER :: rank,size,ierr

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,size,ierr)

    IF (0.EQ.rank) THEN
       PRINT '("usage:")'
       PRINT '("")'
       PRINT '("mpirun -n 2 unicycle-3d-thermobaric [-h] [--dry-run] [--help] [--epsilon 1e-6] [filename]")'
       PRINT '("                                    [--friction-law i] [--evolution-law i] [--export-netcdf]")'
       PRINT '("                                    [--import-state wdir] [--export-state] [--maximum-step int]")'
       PRINT '("                                    [--export-greens] [--import-greens wdir] [--maximum-iterations int]")'
       PRINT '("")'
       PRINT '("options:")'
       PRINT '("   -h                           prints this message and aborts calculation")'
       PRINT '("   --dry-run                    read input, export geometry, and abort further calculation")'
       PRINT '("   --help                       prints this message and aborts calculation")'
       PRINT '("   --version                    print version number and exit")'
       PRINT '("   --epsilon                    set the numerical accuracy [1E-6]")'
       PRINT '("   --evolution-law               type of evolution law [1]")'
       PRINT '("       1: aging-law end-member evolution law")'
       PRINT '("       2: slip-law end-member evolution law")'
       PRINT '("   --export-greens wdir         export the Greens function to file")'
       PRINT '("   --export-netcdf              export results to GMT-compatible netcdf files")'
       PRINT '("   --export-slip                export slip profiles to GMT-compatible netcdf files")'
       PRINT '("   --export-stress              export stress profiles to GMT-compatible netcdf files")'
       PRINT '("   --export-vtp                 export results to Paraview-compatible .vtp files")'
       PRINT '("   --export-slip-distribution r export slip distribution every r time steps")'
       PRINT '("   --import-greens wdir         import the Greens function from file")'
       PRINT '("   --import-state wdir          import the state vector from file in wdir")'
       PRINT '("   --maximum-iterations         set the maximum time step [1000000]")'
       PRINT '("   --maximum-step               set the maximum time step [none]")'
       PRINT '("   --verbose i                  set the verbosity [2]")'
       PRINT '("")'
       PRINT '("description:")'
       PRINT '("   simulates elasto-dynamics on faults in three dimensions")'
       PRINT '("   following the radiation-damping approximation")'
       PRINT '("   using the integral method.")'
       PRINT '("")'
       PRINT '("see also: ""man unicycle""")'
       PRINT '("")'
       PRINT '("           Uni-,,..__sCycle        ")'
       PRINT '("          =QWWWQUAAAKEWW@?`        ")'
       PRINT '("           ?9RWYESSWUT?^           ")'
       PRINT '("                 Q;                ")'
       PRINT '("                .C;                ")'
       PRINT '("                 O;                ")'
       PRINT '("                 M;                ")'
       PRINT '("                 P;                ")'
       PRINT '("                .U;                ")'
       PRINT '("                 T;                ")'
       PRINT '("                 E;                ")'
       PRINT '("                 Q;                ")'
       PRINT '("                 Q;                ")'
       PRINT '("                 Q;                ")'
       PRINT '("               ._Q;_.              ")'
       PRINT '("          _awgQWRRRWWmya,.         ")'
       PRINT '("       .amWT!^-  Q` -~<9Wmw,       ")'
       PRINT '("     .aQD>`     .W;      -9Qw,     ")'
       PRINT '("    _m@(         W;        <$mc    ")'
       PRINT '("   _QD`          W;          4Wc   ")'
       PRINT '("  _QD`           T;           4Wc  ")'
       PRINT '("  dQ`            T;   ___-__   $Q. ")'
       PRINT '(" _QF             T;  .oU!/     ]W[ ")'
       PRINT '(" ]Q[             T;_u!`        -Qk ")'
       PRINT '(" ]Q(            <O!^.           Qk ")'
       PRINT '(" ]Q[         _u?^              .Qk ")'
       PRINT '(" -Qk      _a?.                 ]Q[ ")'
       PRINT '("  4Q, -!!?T!-                 .m@  ")'
       PRINT '("  -Qm,                        jQ(  ")'
       PRINT '("   -Qm,                     .wQ[   ")'
       PRINT '("    -$Qc                   _m@^    ")'
       PRINT '("      ?$ma.              <yQP`     ")'
       PRINT '("        ?VQwa,,.   .__awQ@?`       ")'
       PRINT '("          -??$QRRRWQWBT!~          ")'
       PRINT '("                ---                ")'
       PRINT '("")'
       CALL FLUSH(6)
    END IF

  END SUBROUTINE printhelp

  !-----------------------------------------------
  !> subroutine printversion
  !! displays code version with master thread.
  !-----------------------------------------------
  SUBROUTINE printversion()

    INTEGER :: rank,size,ierr

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,size,ierr)

    IF (0.EQ.rank) THEN
       PRINT '("unicycle-3d-thermobaric version 1.0.0, compiled on ",a)', __DATE__
       PRINT '("")'
       CALL FLUSH(6)
    END IF

  END SUBROUTINE printversion

END PROGRAM thermobaric




