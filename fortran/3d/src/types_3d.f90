!-----------------------------------------------------------------------
! Copyright 2017-2025 Sylvain Barbot
!
! This file is part of UNICYCLE
!
! UNICYCLE is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! UNICYCLE is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with UNICYCLE.  If not, see <http://www.gnu.org/licenses/>.
!-----------------------------------------------------------------------

#include "macros.h90"

MODULE types_3d

  IMPLICIT NONE

  TYPE PATCH_ELEMENT_STRUCT
     SEQUENCE
     INTEGER :: dum
     REAL*8 :: Vl,rake,opening
     REAL*8 :: tau0,mu0,sig,a,b,L,Vo,V2,damping
#ifdef BATH
     REAL*8 :: wRhoC,DW2,Tb,To
#endif
  END TYPE PATCH_ELEMENT_STRUCT

  TYPE RECTANGLE_PATCH_STRUCT
     SEQUENCE
     INTEGER :: ns
     TYPE(PATCH_ELEMENT_STRUCT), DIMENSION(:), ALLOCATABLE :: s
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: sv,dv,nv
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: x,xc
     REAL*8, DIMENSION(:), ALLOCATABLE :: length,width,strike,dip
  END TYPE RECTANGLE_PATCH_STRUCT

  TYPE TRIANGLE_PATCH_STRUCT
     SEQUENCE
     INTEGER :: ns,nVe
     TYPE(PATCH_ELEMENT_STRUCT), DIMENSION(:), ALLOCATABLE :: s
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: v
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: sv,dv,nv
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: xc
     REAL*8, DIMENSION(:), ALLOCATABLE :: area
     INTEGER, DIMENSION(:), ALLOCATABLE :: i1,i2,i3
  END TYPE TRIANGLE_PATCH_STRUCT

  TYPE VOLUME_ELEMENT_STRUCT
     SEQUENCE

     ! initial stress
     REAL*8 :: sII

     ! background strain rate
     REAL*8 :: e11,e12,e13,e22,e23,e33

     ! transient (Kelvin) linear properties
     REAL*8 :: Gk,gammadot0k

     ! steady-state (Maxwell) linear properties
     REAL*8 :: gammadot0m

     ! transient (Kelvin) nonlinear properties
     REAL*8 :: nGk,ngammadot0k,npowerk

     ! steady-state (Maxwell) nonlinear properties
     REAL*8 :: ngammadot0m,npowerm

  END TYPE VOLUME_ELEMENT_STRUCT

  TYPE OBSERVATION_POINT_STRUCT
     SEQUENCE
     INTEGER :: file
     REAL*8, DIMENSION(3) :: x
     CHARACTER(LEN=20) :: name
  END TYPE OBSERVATION_POINT_STRUCT

  TYPE CUBOID_STRUCT
     SEQUENCE
     INTEGER :: ns

     ! number of linear Maxwell volumes
     INTEGER :: nMaxwell

     ! number of linear Kelvin volumes
     INTEGER :: nKelvin

     ! number of nonlinear Kelvin volumes
     INTEGER :: nNonlinearKelvin

     ! number of nonlinear Maxwell volumes
     INTEGER :: nNonlinearMaxwell

     ! array of volume element properties
     TYPE(VOLUME_ELEMENT_STRUCT), DIMENSION(:), ALLOCATABLE :: s

     ! strike, dip, and normal vectors
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: sv,dv,nv

     ! upper left central position and center position
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: x,xc

     ! dimension and orientation
     REAL*8, DIMENSION(:), ALLOCATABLE :: length,thickness,width,strike,dip
  END TYPE CUBOID_STRUCT

  TYPE TETRAHEDRON_STRUCT
     SEQUENCE

     ! number of tetrahedra
     INTEGER :: ns
     
     ! number of vertices
     INTEGER :: nVe

     ! number of linear Maxwell volumes
     INTEGER :: nMaxwell

     ! number of linear Kelvin volumes
     INTEGER :: nKelvin

     ! number of nonlinear Kelvin volumes
     INTEGER :: nNonlinearKelvin

     ! number of nonlinear Maxwell volumes
     INTEGER :: nNonlinearMaxwell

     ! array of volume element properties
     TYPE(VOLUME_ELEMENT_STRUCT), DIMENSION(:), ALLOCATABLE :: s

     ! strike, dip, and normal vectors
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: sv,dv,nv

     ! tetrahedron center
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: xc

     ! volume
     REAL*8, DIMENSION(:), ALLOCATABLE :: volume

     ! triangulation point index
     INTEGER, DIMENSION(:), ALLOCATABLE :: i1,i2,i3,i4

     ! vertex position
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: v

  END TYPE TETRAHEDRON_STRUCT

  TYPE EVENT_STRUC
     REAL*8 :: time
     INTEGER*4 :: i
  END TYPE EVENT_STRUC
  
#ifdef NETCDF
  TYPE PROFILE_STRUCT
     SEQUENCE
     INTEGER :: n,offset,stride,rate
     INTEGER :: ncid,y_varid,z_varid,ncCount
     CHARACTER(LEN=256) :: filename
  END TYPE PROFILE_STRUCT

  TYPE IMAGE_STRUCT
     SEQUENCE
     INTEGER :: nl,nw,offset,stride,rate
  END TYPE IMAGE_STRUCT
#endif

  TYPE :: SIMULATION_STRUCT
     ! elastic moduli
     REAL*8 :: lambda,mu,nu

     ! simulation time
     REAL*8 :: interval

     ! number of patches (rectangle and triangle)
     INTEGER :: nPatch

     ! number of degrees of freedom for faults
     INTEGER :: dPatch

     ! number of volume elements
     INTEGER :: nVolume

     ! number of degrees of freedom for volume elements
     INTEGER :: dVolume

     ! rectangular patches
     TYPE(RECTANGLE_PATCH_STRUCT) :: rectangle

     ! triangle patches
     TYPE(TRIANGLE_PATCH_STRUCT) :: triangle

     ! cuboid volume elements 
     TYPE(CUBOID_STRUCT) :: cuboid

     ! tetrahedron volume elements
     TYPE(TETRAHEDRON_STRUCT) :: tetrahedron

     ! output directory
     CHARACTER(256) :: wdir

     ! Greens function directory
     CHARACTER(256) :: greensFunctionDirectory

     ! filenames
     CHARACTER(256) :: timeFilename

     ! number of observation states
     INTEGER :: nObservationState

     ! number of observation profiles
     INTEGER :: nObservationProfiles

     ! number of observation images
     INTEGER :: nObservationImages

     ! observation state (patches and volumes)
     INTEGER, DIMENSION(:,:), ALLOCATABLE :: observationState

#ifdef NETCDF
     ! observation profiles (patches)
     TYPE(PROFILE_STRUCT), DIMENSION(:), ALLOCATABLE :: observationProfileVelocity
     TYPE(PROFILE_STRUCT), DIMENSION(:), ALLOCATABLE :: observationProfileSlip
     TYPE(PROFILE_STRUCT), DIMENSION(:), ALLOCATABLE :: observationProfileStress

     ! observation state (patches and volumes)
     TYPE(IMAGE_STRUCT), DIMENSION(:), ALLOCATABLE :: observationImageVelocity
     TYPE(IMAGE_STRUCT), DIMENSION(:), ALLOCATABLE :: observationImageVelocityStrike
     TYPE(IMAGE_STRUCT), DIMENSION(:), ALLOCATABLE :: observationImageVelocityDip
#endif

     ! number of observation points
     INTEGER :: nObservationPoint

     ! observation points
     TYPE(OBSERVATION_POINT_STRUCT), DIMENSION(:), ALLOCATABLE :: observationPoint

     ! number of perturbation events
     INTEGER :: ne

     ! perturbation events
     TYPE(EVENT_STRUC), DIMENSION(:), ALLOCATABLE :: event

     ! other options
     LOGICAL :: isdryrun=.FALSE.
     LOGICAL :: isExportGreens=.FALSE.
     LOGICAL :: isImportGreens=.FALSE.
     LOGICAL :: isImportState=.FALSE.
     LOGICAL :: isExportNetcdf=.FALSE.
     LOGICAL :: isExportState=.FALSE.
     LOGICAL :: isExportSlip=.FALSE.
     LOGICAL :: isExportStress=.FALSE.
     LOGICAL :: isExportVTP=.FALSE.
     LOGICAL :: isExportXYZ=.FALSE.
     LOGICAL :: ishelp=.FALSE.
     LOGICAL :: isversion=.FALSE.

  END TYPE SIMULATION_STRUCT

  TYPE LAYOUT_STRUCT

     ! list of number of elements in threads
     INTEGER, DIMENSION(:), ALLOCATABLE :: listElements,listOffset

     ! type of elements (FLAG_RECTANGLE, FLAG_TRIANGLE, FLAG_VOLUME)
     INTEGER, DIMENSION(:), ALLOCATABLE :: elementType

     ! index of elements
     INTEGER, DIMENSION(:), ALLOCATABLE :: elementIndex

     ! index of state indices
     INTEGER, DIMENSION(:), ALLOCATABLE :: elementStateIndex

     ! degrees of freedom for elements in velocity vector
     ! and strain rate tensor array:
     !   2 (strike and dip slip) for patches, and
     !   6 (e11, e12, e13, e22, e23, e33) for volume elements
     INTEGER, DIMENSION(:), ALLOCATABLE :: elementVelocityDGF,elementVelocityIndex

     ! degrees of freedom for elements in state vector:
     !   7 (strike, dip and normal traction, strike and dip slip, velocity, state) for patches, and
     !   at least 12 (sij, eij) for volume elements
     INTEGER, DIMENSION(:), ALLOCATABLE :: elementStateDGF

     ! degrees of freedom for elements in Greens function
     !   3 (strike, dip and normal traction) for patches, and
     !   6 (s11, s12, s13, s22, s23, s33) stress for volume elements
     INTEGER, DIMENSION(:), ALLOCATABLE :: elementForceDGF

     ! number of traction and stress element for each thread and array position
     INTEGER, DIMENSION(:), ALLOCATABLE :: listForceN

     ! number of velocity and strain rate components for each thread and array position
     INTEGER, DIMENSION(:), ALLOCATABLE :: listVelocityN,listVelocityOffset

     ! number of state parameters for each thread and array position
     INTEGER, DIMENSION(:), ALLOCATABLE :: listStateN,listStateOffset

  END TYPE LAYOUT_STRUCT

END MODULE types_3d

