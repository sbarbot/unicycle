!-----------------------------------------------------------------------
!> program Unicycle (unified cycle of earthquakes) simulates evolution
!! of fault slip and distributed strain using the integral method under
!! the radiation damping approximation.
!!
!! \mainpage
!! 
!! The Green's function for traction and stress interaction amongst 
!! triangle and rectangle boundary elements, and volume elements has the 
!! following layout
!!
!! \verbatim
!!
!!       / RR RT  RL \
!!       |           |
!!   G = | TR TT  TL |
!!       |           |
!!       \ LR LT  LL /
!!
!! \endverbatim
!!
!! where
!!
!!   RR is the matrix for traction on rectangle faults due to rectangle fault slip
!!   RT is the matrix for traction on rectangle faults due to triangle  fault slip
!!   TR is the matrix for traction on triangle  faults due to rectangle fault slip
!!   TT is the matrix for traction on triangle  faults due to triangle  fault slip
!!
!!   RL is the matrix for traction on rectangle faults due to volume strain
!!   TL is the matrix for traction on triangle  faults due to volume strain
!!
!!   LR is the matrix for stress in volumes due to rectangle fault slip
!!   LT is the matrix for stress in volumes due to triangle  fault slip
!!   LL is the matrix for stress in volumes due to strain in volume elements.
!!
!! The traction and stress interaction matrix can be thought as
!!
!! \verbatim
!!
!!       / KK  KL \
!!   G = |        |
!!       \ LK  LL /
!!
!! \endverbatim
!!
!! with
!!
!! \verbatim
!!
!!        / RR  RT \         / RL \
!!   KK = |        |,   KL = |    |,   LK = | LR  LT |
!!        \ TR  TT /         \ TL /         
!!
!! \endverbatim
!!
!! where KK is the traction due to fault slip,
!!       KL is the traction due to volume strain
!!       LK is the stress   due to fault slip
!!       LL is the stress   due to volume strain
!!
!! All faults (rectangle or triangle) have two slip directions:
!! strike slip and dip slip. The interaction matrices become
!!
!! \verbatim
!!
!!        / RRss  RRsd \        / RTss  RRsd \
!!   RR = |            |,  RT = |            |,  
!!        \ RRds  RRdd /        \ RRds  RRdd /
!!
!!        / TRss  TRsd \        / TTss  TTsd \
!!   TR = |            |,  TT = |            |
!!        \ TRds  TRdd /        \ TTds  TTdd /
!!
!! \endverbatim
!! 
!! The volumes have six strain directions: e11, e12, e13, e22, e23, e33.
!! The interaction matrices become
!!
!! \verbatim
!!
!!        / LL1111  LL1112  LL1113  LL1122  LL1123  LL1133 \
!!        |                                                |
!!        | LL1211  LL1212  LL1213  LL1222  LL1223  LL1233 |
!!        |                                                |
!!        | LL1311  LL1312  LL1313  LL1322  LL1323  LL1333 |
!!   LL = |                                                |
!!        | LL2211  LL2212  LL2213  LL2222  LL2223  LL2233 |
!!        |                                                |
!!        | LL2311  LL2312  LL2313  LL2322  LL2323  LL2333 |
!!        |                                                |
!!        \ LL3311  LL3312  LL3313  LL3322  LL3323  LL3333 /
!!
!!        / RLs11  RLs12  RLs13  RLs22  RLs23  RLs33 \
!!   RL = |                                          |
!!        \ RLd11  RLd12  RLd13  RLd22  RLd23  RLd33 /
!!
!!        / TLs11  TLs12  TLs13  TLs22  TLs23  TLs33 \
!!   TL = |                                          |
!!        \ TLd11  TLd12  TLd13  TLd22  TLd23  TLd33 /
!!
!!        / LR11s  LR11d \            / LT11s  LT11d \
!!        |              |            |              |
!!        | LR12s  LR12d |            | LT12s  LT12d |
!!        |              |            |              |
!!        | LR13s  LR13d |            | LT13s  LT13d |
!!   LR = |              |,      LT = |              |
!!        | LR22s  LR22d |            | LT22s  LT22d |
!!        |              |            |              |
!!        | LR23s  LR23d |            | LT23s  LT23d |
!!        |              |            |              |
!!        \ LR33s  LR33d /            \ LT33s  LT33d /
!!
!! \endverbatim
!!
!! The time evolution is evaluated numerically using the 4th/5th order
!! Runge-Kutta method with adaptive time steps. The state vector is 
!! as follows:
!!
!! \verbatim
!!
!!    / R1 1       \   +-----------------------+  +-----------------+
!!    | .          |   |                       |  |                 |
!!    | R1 dPatch  |   |                       |  |                 |
!!    | .          |   |  nRectangle * dPatch  |  |                 |
!!    | .          |   |                       |  |                 |
!!    | Rn 1       |   |                       |  |                 |
!!    | .          |   |                       |  |                 |
!!    | Rn dPatch  |   +-----------------------+  |                 |
!!    |            |                              | nPatch * dPatch |
!!    | T1 1       |   +-----------------------+  |                 |
!!    | .          |   |                       |  |                 |
!!    | T1 dPatch  |   |                       |  |                 |
!!    | .          |   |  nTriangle * dPatch   |  |                 |
!!    | .          |   |                       |  |                 |
!!    | Tn 1       |   |                       |  |                 |
!!    | .          |   |                       |  |                 |
!!    | Tn dPatch  |   +-----------------------+  +-----------------+
!!    |            |                           
!!    | V1 1       |   +-----------------------+  +-----------------+
!!    | .          |   |                       |  |                 |
!!    | V1 dVolume |   |                       |  |     nVolume     |
!!    | .          |   |   nVolume * dVolume   |  |        *        |
!!    | .          |   |                       |  |     dVolume     |
!!    | Vn 1       |   |                       |  |                 |
!!    | .          |   |                       |  |                 |
!!    \ Vn dVolume /   +-----------------------+  +-----------------+
!!
!! \endverbatim
!!
!! where nRectangle, nTriangle, and nVolume are the number of rectangle
!! and triangle patches and the number of volume elements, respectively,
!! and dPatch and dVolume are the degrees of freedom for patches and 
!! volumes. 
!!
!! For every rectangle or triangle patch, we have the following 
!! items in the state vector
!!
!! \verbatim
!!
!!   /  ss   \  1
!!   |  sd   |  .
!!   |  ts   |  .
!!   |  td   |  .
!!   |  tn   |  .
!!   ! theta*|  .
!!   \  v*   /  dPatch
!!
!! \endverbatim
!!
!! where ts, td, and td are the local traction in the strike, dip, and
!! normal directions, ss and sd are the total slip in the strike and dip 
!! directions, v* is the logarithm of the norm of the instantaneous slip 
!! velocity vector (v*=log10(V)), and theta* is the logarithm of the state
!! variable in the rate and state friction framework (theta*=log10(theta)).
!!
!! For every volume element, we have the following items in the state vector
!!
!! \verbatim
!!
!!   /  e11   \  1
!!   |  e12   |  .
!!   |  e13   |  . 
!!   |  e22   |  .
!!   |  e23   |  .
!!   |  e33   |  .
!!   |  s11   |  .
!!   |  s12   |  .
!!   |  s13   |  .
!!   |  s22   |  .
!!   |  s23   |  .
!!   \  s33  /   dVolume
!!
!! \endverbatim
!!
!! where s11, s12, s13, s22, s23, and s33 are the six independent components
!! of the local stress tensor, e11, e12, e13, e22, e23, and e33 are the six
!! independent components of the cumulative anelastic strain tensor.
!!
!! References:<br>
!!
!!   Barbot S., J. D.-P. Moore, and V. Lambert, "Displacement and Stress
!!   Associated with Distributed Anelastic Deformation in a Half-Space",
!!   Bull. Seism. Soc. Am., 10.1785/0120160237, 2017.
!!
!! \author Sylvain Barbot (2017).
!----------------------------------------------------------------------
PROGRAM viscouscycles

#include "macros.h90"

#ifndef ODE45
#ifndef ODE23
#define ODE45
#endif
#else
#ifdef ODE23
  ERROR: either ODE45 or ODE23 should be defined, but not both
#endif
#endif

#ifdef NETCDF
  USE exportnetcdf
#endif
  USE greens_3d
  USE rk
  USE mpi_f08
  USE types_3d

  IMPLICIT NONE

  REAL*8, PARAMETER :: DEG2RAD = 0.01745329251994329547437168059786927_8

  ! MPI rank and size
  INTEGER :: rank,csize

  ! error flag
  INTEGER :: ierr

  CHARACTER(512) :: filename

#ifdef NETCDF
  ! check file
  LOGICAL :: fileExist,fileExistAll
#endif

  ! maximum velocity
  REAL*8 :: vMax,vMaxAll

  ! maximum strain rate
  REAL*8 :: eMax,eMaxAll

  ! moment rate
  REAL*8 :: momentRate, momentRateAll

  ! scaling factor
  REAL*8, PARAMETER :: lg10=LOG(1.d1)

  ! state vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: y
  ! rate of change of state vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: dydt,yscal
  ! temporary variables
  REAL*8, DIMENSION(:), ALLOCATABLE :: ytmp,ytmp1,ytmp2,ytmp3

  ! slip velocity vector and strain rate tensor array (temporary space)
  REAL*8, DIMENSION(:), ALLOCATABLE :: v

  ! slip velocity vector and strain rate tensor array (temporary space)
  REAL*8, DIMENSION(:), ALLOCATABLE :: vAll

  ! vector array (strike and dip components of slip)
  REAL*4, DIMENSION(:), ALLOCATABLE :: vector,vectorAll

  ! traction vector and stress tensor array (temporary space)
  REAL*8, DIMENSION(:), ALLOCATABLE :: t

  ! displacement and kinematics vector
  REAL*8, DIMENSION(:), ALLOCATABLE :: d,u,dAll,dfAll,dlAll

  ! Green's functions
  REAL*8, DIMENSION(:,:), ALLOCATABLE :: G,O,Of,Ol

  ! time
  REAL*8 :: time,t0
  ! time step
  REAL*8 :: dt_try,dt_next,dt_done
  ! time steps
  INTEGER :: i,j

  ! type of friction law (default)
  INTEGER :: frictionLawType=1

  ! type of evolution law (default)
  INTEGER :: evolutionLawType=1

  ! maximum number of time steps (default)
  INTEGER :: maximumIterations=1000000

  ! rate of NETCDF export (default)
  INTEGER :: exportSlipDistributionRate=-1

  ! layout for parallelism
  TYPE(LAYOUT_STRUCT) :: layout

  ! model parameters
  TYPE(SIMULATION_STRUCT) :: in

  ! initialization
  CALL MPI_INIT(ierr)
  CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
  CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

  ! start time
  time=0.d0

  ! initial tentative time step
  dt_next=1.0d-3

  ! retrieve input parameters from command line
  CALL init(in)
  CALL FLUSH(STDOUT)

  ! export geometrical layout information
  CALL exportGeometry(in)

  IF (in%isdryrun .AND. 0 .EQ. rank) THEN
     PRINT '("dry run: abort calculation")'
  END IF
  IF (in%isdryrun .OR. in%isversion .OR. in%ishelp) THEN
     CALL MPI_FINALIZE(ierr)
     STOP
  END IF

  ! calculate basis vectors
  CALL initGeometry(in)

  ! describe data layout for parallelism
  CALL initParallelism(in,layout)

#ifdef NETCDF
  WRITE (filename,'(a,"/greens-",I4.4,".grd")') TRIM(in%greensFunctionDirectory),rank
  INQUIRE(FILE=filename,EXIST=fileExist)
  CALL MPI_ALLREDUCE(fileExist,fileExistAll,1,MPI_LOGICAL,MPI_LAND,MPI_COMM_WORLD,ierr)
  IF (in%isImportGreens .AND. fileExistAll) THEN
     CALL initG(in,layout,G)
     IF (0 .EQ. rank) THEN
        PRINT '("# import stress interaction Green''s function.")'
     END IF
     CALL importGreensNetcdf(G)
  ELSE
     ! calculate the stress interaction matrix
     IF (0 .EQ. rank) THEN
        PRINT '("# computing stress interaction Green''s functions.")'
     END IF

     CALL buildG(in,layout,G)
     CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)

     IF (in%isExportGreens) THEN
        IF (0 .EQ. rank) THEN
           PRINT '("# exporting stress interaction Green''s functions to ",a)',TRIM(in%greensFunctionDirectory)
        END IF
        CALL exportGreensNetcdf(G)
     END IF
  END IF
#else
  ! calculate the stress interaction matrix
  IF (0 .EQ. rank) THEN
     PRINT '("# computing Green''s functions.")'
  END IF
  CALL buildG(in,layout,G)
#endif

#ifdef NETCDF
  WRITE (filename,'(a,"/greens-O-",I4.4,".grd")') TRIM(in%greensFunctionDirectory),rank
  INQUIRE(FILE=filename,EXIST=fileExist)
  CALL MPI_ALLREDUCE(fileExist,fileExistAll,1,MPI_LOGICAL,MPI_LAND,MPI_COMM_WORLD,ierr)
  IF (in%isImportGreens .AND. fileExistAll) THEN
     CALL initO(in,layout,O,Of,Ol)
     IF (0 .EQ. rank) THEN
        PRINT '("# import displacement Green''s function.")'
     END IF
     CALL importGreensObsNetcdf(O,Of,Ol)
  ELSE
     ! calculate the stress interaction matrix
     IF (0 .EQ. rank) THEN
        PRINT '("# computing displacement Green''s functions.")'
     END IF

     CALL buildO(in,layout,O,Of,Ol)
     CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)

     IF (in%isExportGreens) THEN
        IF (0 .EQ. rank) THEN
           PRINT '("# exporting displacement Green''s functions to ",a)',TRIM(in%greensFunctionDirectory)
        END IF
        CALL exportGreensObsNetcdf(O,Of,Ol)
     END IF
  END IF
#else
  ! calculate the stress interaction matrix
  IF (0 .EQ. rank) THEN
     PRINT '("# computing displacement Green''s functions.")'
  END IF
  CALL buildO(in,layout,O,Of,Ol)
#endif


  ! synchronize threads before writing to standard output
  CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)

  IF (0 .EQ. rank) THEN
     PRINT 2000
  END IF

  ! velocity vector and strain rate tensor array (t=G*vAll)
  ALLOCATE(u(layout%listVelocityN(1+rank)), &
           v(layout%listVelocityN(1+rank)), &
           vAll(SUM(layout%listVelocityN)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the velocity and strain rate vector"

  IF (0 .LT. exportSlipDistributionRate) THEN
     ALLOCATE(vector(layout%listVelocityN(1+rank)),STAT=ierr)
     IF (ierr>0) STOP "could not allocate the two-component vector"
     IF (0 .EQ. rank) THEN
        ALLOCATE(vectorAll(SUM(layout%listVelocityN)),STAT=ierr)
        IF (ierr>0) STOP "could not allocate the two-component vector"
     END IF
  END IF

  ! traction vector and stress tensor array (t=G*v)
  ALLOCATE(t(layout%listForceN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the traction and stress vector"

  ! displacement vector (d=O*v)
  ALLOCATE(d    (in%nObservationPoint*DISPLACEMENT_VECTOR_DGF), &
           dAll (in%nObservationPoint*DISPLACEMENT_VECTOR_DGF), &
           dfAll(in%nObservationPoint*DISPLACEMENT_VECTOR_DGF), &
           dlAll(in%nObservationPoint*DISPLACEMENT_VECTOR_DGF),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the displacement vector"

  ! state vector
  ALLOCATE(y(layout%listStateN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the state vector"

  ! rate of state vector
  ALLOCATE(dydt(layout%listStateN(1+rank)), &
           yscal(layout%listStateN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the state vectors"

  ALLOCATE(ytmp (layout%listStateN(1+rank)), &
           ytmp1(layout%listStateN(1+rank)), &
           ytmp2(layout%listStateN(1+rank)), &
           ytmp3(layout%listStateN(1+rank)),STAT=ierr)
  IF (ierr>0) STOP "could not allocate the RKQS work space"

  ! allocate buffer from rk module
#ifdef ODE45
  ALLOCATE(buffer(layout%listStateN(1+rank),5),STAT=ierr)
#else
  ALLOCATE(buffer(layout%listStateN(1+rank),3),STAT=ierr)
#endif
  IF (ierr>0) STOP "could not allocate the buffer work space"

  IF (0 .EQ. rank) THEN
     OPEN (UNIT=FPTIME,FILE=in%timeFilename,IOSTAT=ierr,FORM="FORMATTED")
     IF (ierr>0) THEN
        WRITE_DEBUG_INFO(102)
        WRITE (STDERR,'("error: unable to access ",a)') TRIM(in%timefilename)
        STOP 1
     END IF
  END IF

  ! initialize the y vector
  IF (0 .EQ. rank) THEN
     PRINT '("# initialize state vector.")'
  END IF
  CALL initStateVector(layout%listStateN(1+rank),y,in)
  IF (0 .EQ. rank) THEN
     PRINT 2000
  END IF

#ifdef NETCDF
  ! initialize netcdf output
  IF (in%isExportNetcdf) THEN
    IF (0 .EQ. rank) THEN

       DO i=1,in%nObservationProfiles
          WRITE (in%observationProfileVelocity(i)%filename,'(a,"/log10v.",I3.3,".grd")') TRIM(in%wdir),i
          CALL initNetcdf(in%observationProfileVelocity(i))
       END DO

       IF (in%isExportSlip) THEN
          DO i=1,in%nObservationProfiles
             WRITE (in%observationProfileSlip(i)%filename,'(a,"/slip.",I3.3,".grd")') TRIM(in%wdir),i
             CALL initNetcdf(in%observationProfileSlip(i))
          END DO
       END IF

       IF (in%isExportStress) THEN
          DO i=1,in%nObservationProfiles
             WRITE (in%observationProfileStress(i)%filename,'(a,"/stress.",I3.3,".grd")') TRIM(in%wdir),i
             CALL initNetcdf(in%observationProfileStress(i))
          END DO
       END IF
    END IF
  END IF
#endif

  ! gather maximum velocity
  CALL MPI_REDUCE(vMax,vMaxAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

  ! gather moment rate
  CALL MPI_REDUCE(momentRate,momentRateAll,1,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

  ! gather maximum strain-rate
  CALL MPI_REDUCE(eMax,eMaxAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

  ! initialize output
  IF (0 .EQ. rank) THEN
     WRITE(STDOUT,'("# export to ",a)') TRIM(in%wdir)
     PRINT 2000
     WRITE(STDOUT,'("#       n               time                 dt       vMax       eMax")')
     WRITE(STDOUT,'(I9.9,ES19.12E2,ES19.12E2,2ES11.4E2)') 0,time,dt_next,vMaxAll,eMaxAll
     WRITE(FPTIME,'("#               time                 dt               vMax         moment-rate                eMax")')
     WRITE(FPTIME,'(ES20.14E2,2ES19.12E2,2ES20.12E2)') 0._8,dt_next,vMaxAll,momentRateAll,eMaxAll
  END IF

  ! initialize observation patch
  DO j=1,in%nObservationState
     IF ((in%observationState(1,j) .GE. layout%listOffset(rank+1)) .AND. &
         (in%observationState(1,j) .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN

        SELECT CASE(layout%elementType(in%observationState(1,j)-layout%listOffset(rank+1)+1))
        CASE (FLAG_RECTANGLE,FLAG_TRIANGLE)
           WRITE (filename,'(a,"/patch-",I8.8,"-",I8.8,".dat")') TRIM(in%wdir), &
                   in%observationState(3,j), in%observationState(1,j)
        CASE (FLAG_CUBOID,FLAG_TETRAHEDRON)
           WRITE (filename,'(a,"/volume-",I8.8,"-",I8.8,".dat")') TRIM(in%wdir), &
                   in%observationState(3,j), in%observationState(1,j)-in%nPatch
        CASE DEFAULT
           WRITE (STDERR,'("wrong case: this is a bug.")')
           WRITE_DEBUG_INFO(-1)
           STOP 2
        END SELECT

        in%observationState(4,j)=100+j
        OPEN (UNIT=in%observationState(4,j), &
              FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
        IF (ierr>0) THEN
           WRITE_DEBUG_INFO(102)
           WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
           STOP 1
        END IF

        SELECT CASE(layout%elementType(in%observationState(1,j)-layout%listOffset(rank+1)+1))
        CASE (FLAG_RECTANGLE,FLAG_TRIANGLE)
           WRITE (in%observationState(4,j), &
                 '("#            time (s),", &
                 & "     strike slip (m),        dip slip (m),", &
                 & "     strike traction,        dip traction,     normal traction,", &
                 & "        log10(theta),     log10(velocity),", &
                 & "     strike velocity,        dip velocity,", &
                 & "d strike traction/dt,   d dip traction/dt,", &
                 & "d normal traction/dt,        d theta / dt,    acceleration / v")')
        CASE (FLAG_CUBOID,FLAG_TETRAHEDRON)
           WRITE (in%observationState(4,j), &
                 '("#            time (s),                 ", &
                 & "e11,                 e12,                 e13,                 ", &
                 & "e22,                 e23,                 e33,                 ", &
                 & "s11,                 s12,                 s13,                 ", &
                 & "s22,                 s23,                 s33,           ", &
                 & "d e11 /dt,           d e12 /dt,           d e13 /dt,           ", &
                 & "d e22 /dt,           d e23 /dt,           d e33 /dt,           ", &
                 & "d s11 /dt,           d s12 /dt,           d s13 /dt,           ", &
                 & "d s22 /dt,           d s23 /dt,           d s33 /dt")')
        CASE DEFAULT
           WRITE (STDERR,'("wrong case: this is a bug.")')
           WRITE_DEBUG_INFO(-1)
           STOP 2
        END SELECT

     END IF
  END DO

  ! initialize observation points
  IF (0 .EQ. rank) THEN
     DO j=1,in%nObservationPoint
        in%observationPoint(j)%file=100000+j
        WRITE (filename,'(a,"/opts-",a,".dat")') TRIM(in%wdir),TRIM(in%observationPoint(j)%name)
        OPEN (UNIT=in%observationPoint(j)%file, &
              FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
        IF (ierr>0) THEN
           WRITE_DEBUG_INFO(102)
           WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
           STOP 1
        END IF
        WRITE (in%observationPoint(j)%file,'("#               time            ", &
                                           & "u1 (all)            u2 (all)            u3 (all)          ", &
                                           & "u1 (fault)          u2 (fault)          u3 (fault)        ", &
                                           & "u1 (viscous)        u2 (viscous)        u3 (viscous)")')

     END DO
  END IF

  ! main loop
#ifdef ODE23
  CALL odefun(layout%listStateN(1+rank),time,y,dydt)
#endif
  DO i=1,maximumIterations

#ifdef ODE45
     CALL odefun(layout%listStateN(1+rank),time,y,dydt)
#endif

     CALL export()
     CALL exportPoints()

     IF (0 .LT. exportSlipDistributionRate) THEN
        IF (0 .EQ. MOD(i-1,exportSlipDistributionRate)) THEN
           CALL exportAsciiSlipDistribution()
        END IF
     END IF

#ifdef NETCDF
     IF (in%isExportNetcdf) THEN

        IF (0 .EQ. rank) THEN
           DO j=1,in%nObservationProfiles
              IF (0 .EQ. MOD(i,in%observationProfileVelocity(j)%rate)) THEN
                 CALL exportNetcdfVelocity(in%observationProfileVelocity(j))
              END IF
           END DO
        END IF

        IF (in%isExportSlip) THEN
           DO j=1,in%nObservationProfiles
              IF (0 .EQ. MOD(i,in%observationProfileSlip(j)%rate)) THEN
                 CALL exportNetcdfSlip(in%observationProfileSlip(j))
              END IF
           END DO
        END IF

        IF (in%isExportStress) THEN
           DO j=1,in%nObservationProfiles
              IF (0 .EQ. MOD(i,in%observationProfileStress(j)%rate)) THEN
                 CALL exportNetcdfStress(in%observationProfileStress(j))
              END IF
           END DO
        END IF
     END IF
#endif

#ifdef NETCDF
     IF (0 .EQ. rank) THEN
        IF (in%isexportnetcdf) THEN
           DO j=1,in%nObservationImages
              IF (0 .EQ. MOD(i,in%observationImageVelocity(j)%rate)) THEN
                 CALL exportImageNetcdfVelocity(in%observationImageVelocity(j))
                 CALL exportImageNetcdfVelocityStrike(in%observationImageVelocityStrike(j))
                 CALL exportImageNetcdfVelocityDip(in%observationImageVelocityDip(j))
              END IF
           END DO
        END IF
     END IF
#endif

     dt_try=dt_next
     yscal=ABS(y)+ABS(dt_try*dydt)+TINY

     t0=time
#ifdef ODE45
     CALL rungeKutta(layout%listStateN(1+rank),t0,y,dydt, &
               yscal,ytmp1,ytmp2,ytmp3,dt_try,dt_done,dt_next,odefun,timeStep45)
#else
     CALL rungeKutta(layout%listStateN(1+rank),t0,y,dydt, &
               yscal,ytmp1,ytmp2,ytmp3,dt_try,dt_done,dt_next,odefun,timeStep23)
#endif

     time=time+dt_done

     ! end calculation
     IF (in%interval .LE. time) THEN
        EXIT
     END IF
   
  END DO

  IF (0 .EQ. rank) THEN
     PRINT '(I9.9," time steps.")', i
  END IF

  IF (0 .EQ. rank) THEN
     CLOSE(FPTIME)
  END IF

  ! close observation state files
  DO j=1,in%nObservationState
     IF ((in%observationState(1,j) .GE. layout%listOffset(rank+1)) .AND. &
         (in%observationState(1,j) .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN
        CLOSE(in%observationState(4,j))
     END IF
  END DO

  ! close the observation points
  IF (0 .EQ. rank) THEN
     DO j=1,in%nObservationPoint
        CLOSE(in%observationPoint(j)%file)
     END DO
  END IF

#ifdef NETCDF
  IF (0 .EQ. rank) THEN
     IF (in%isexportnetcdf) THEN
        DO j=1,in%nObservationProfiles
           CALL closeNetcdfUnlimited(in%observationProfileVelocity(j)%ncid, &
                                     in%observationProfileVelocity(j)%y_varid, &
                                     in%observationProfileVelocity(j)%z_varid, &
                                     in%observationProfileVelocity(j)%ncCount)
        END DO
        IF (in%isExportSlip) THEN
           DO j=1,in%nObservationProfiles
              CALL closeNetcdfUnlimited(in%observationProfileSlip(j)%ncid, &
                                        in%observationProfileSlip(j)%y_varid, &
                                        in%observationProfileSlip(j)%z_varid, &
                                        in%observationProfileSlip(j)%ncCount)
           END DO
        END IF
        IF (in%isExportStress) THEN
           DO j=1,in%nObservationProfiles
              CALL closeNetcdfUnlimited(in%observationProfileStress(j)%ncid, &
                                        in%observationProfileStress(j)%y_varid, &
                                        in%observationProfileStress(j)%z_varid, &
                                        in%observationProfileStress(j)%ncCount)
           END DO
        END IF
     END IF
  END IF
#endif

  DEALLOCATE(y,dydt,yscal)
  DEALLOCATE(ytmp,ytmp1,ytmp2,ytmp3)
  DEALLOCATE(buffer)
  DEALLOCATE(G,v,vAll,t)
#ifdef NETCDF
  IF (ALLOCATED(in%observationProfileVelocity)) DEALLOCATE(in%observationProfileVelocity)
  IF (ALLOCATED(in%observationProfileSlip)) DEALLOCATE(in%observationProfileSlip)
  IF (ALLOCATED(in%observationProfileStress)) DEALLOCATE(in%observationProfileStress)

  IF (ALLOCATED(in%observationImageVelocity)) DEALLOCATE(in%observationImageVelocity)
  IF (ALLOCATED(in%observationImageVelocityStrike)) DEALLOCATE(in%observationImageVelocityStrike)
  IF (ALLOCATED(in%observationImageVelocityDip)) DEALLOCATE(in%observationImageVelocityDip)
#endif
  DEALLOCATE(layout%listForceN)
  DEALLOCATE(layout%listVelocityN,layout%listVelocityOffset)
  DEALLOCATE(layout%listStateN,layout%listStateOffset)
  DEALLOCATE(layout%elementStateIndex)
  DEALLOCATE(layout%listElements,layout%listOffset)
  DEALLOCATE(O,Of,Ol,d,u,dAll,dfAll,dlAll)

  IF (0 .LT. exportSlipDistributionRate) THEN
     DEALLOCATE(vector)
     IF (0 .EQ. rank) DEALLOCATE(vectorAll)
  END IF

  CALL MPI_FINALIZE(ierr)

2000 FORMAT ("# ----------------------------------------------------------------------------")
     
CONTAINS

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportGreensNetcdf
  ! export the Greens function by rank
  !----------------------------------------------------------------------
  SUBROUTINE exportGreensNetcdf(M)
    REAL*8, DIMENSION(:,:), INTENT(IN) :: M

    REAL*8, DIMENSION(:), ALLOCATABLE :: x,y

    INTEGER :: i,ierr
    CHARACTER(LEN=256) :: filename

    ALLOCATE(x(SIZE(M,1)),y(SIZE(M,2)),STAT=ierr)
    IF (ierr/=0) STOP "could not allocate netcdf coordinate for Greens function"

    ! loop over all patch elements
    DO i=1,SIZE(M,1)
       x(i)=REAL(i,8)
    END DO
    DO i=1,SIZE(M,2)
       y(i)=REAL(i,8)
    END DO

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/greens-",I4.4,".grd")') TRIM(in%greensFunctionDirectory),rank
    CALL writeNetcdf(filename,SIZE(M,1),x,SIZE(M,2),y,M,1)

    DEALLOCATE(x,y)

  END SUBROUTINE exportGreensNetcdf

  !-----------------------------------------------------------------------
  !> subroutine exportGreensObsNetcdf
  ! export the Greens function by rank
  !----------------------------------------------------------------------
  SUBROUTINE exportGreensObsNetcdf(O,Of,Ol)
    REAL*8, DIMENSION(:,:), INTENT(IN) :: O,Of,Ol

    REAL*8, DIMENSION(:), ALLOCATABLE :: x,y

    INTEGER :: i,ierr
    CHARACTER(LEN=256) :: filename

    ALLOCATE(x(SIZE(O,1)),y(SIZE(O,2)),STAT=ierr)
    IF (ierr/=0) STOP "could not allocate netcdf coordinate for Greens function"

    ! loop over all patch elements
    DO i=1,SIZE(O,1)
       x(i)=REAL(i,8)
    END DO
    DO i=1,SIZE(O,2)
       y(i)=REAL(i,8)
    END DO

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/greens-O-",I4.4,".grd")') TRIM(in%greensFunctionDirectory),rank
    CALL writeNetcdf(filename,SIZE(O,1),x,SIZE(O,2),y,O,1)

    WRITE (filename,'(a,"/greens-Of-",I4.4,".grd")') TRIM(in%greensFunctionDirectory),rank
    CALL writeNetcdf(filename,SIZE(Of,1),x,SIZE(Of,2),y,Of,1)

    WRITE (filename,'(a,"/greens-Ol-",I4.4,".grd")') TRIM(in%greensFunctionDirectory),rank
    CALL writeNetcdf(filename,SIZE(Ol,1),x,SIZE(Ol,2),y,Ol,1)

    DEALLOCATE(x,y)

  END SUBROUTINE exportGreensObsNetcdf
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine importGreensNetcdf
  ! import the Greens function by rank
  !----------------------------------------------------------------------
  SUBROUTINE importGreensNetcdf(M)
    REAL*8, DIMENSION(:,:), INTENT(OUT) :: M

    CHARACTER(LEN=256) :: filename

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/greens-",I4.4,".grd")') TRIM(in%greensFunctionDirectory),rank
    CALL readNetcdf(filename,SIZE(M,1),SIZE(M,2),M)

  END SUBROUTINE importGreensNetcdf

  !-----------------------------------------------------------------------
  !> subroutine importGreensObsNetcdf
  ! import the Greens function by rank
  !----------------------------------------------------------------------
  SUBROUTINE importGreensObsNetcdf(O,Of,Ol)
    REAL*8, DIMENSION(:,:), INTENT(OUT) :: O,Of,Ol

    CHARACTER(LEN=256) :: filename

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/greens-O-",I4.4,".grd")') TRIM(in%greensFunctionDirectory),rank
    CALL readNetcdf(filename,SIZE(O,1),SIZE(O,2),O)

    WRITE (filename,'(a,"/greens-Of-",I4.4,".grd")') TRIM(in%greensFunctionDirectory),rank
    CALL readNetcdf(filename,SIZE(Of,1),SIZE(Of,2),Of)

    WRITE (filename,'(a,"/greens-Ol-",I4.4,".grd")') TRIM(in%greensFunctionDirectory),rank
    CALL readNetcdf(filename,SIZE(Ol,1),SIZE(Ol,2),Ol)

  END SUBROUTINE importGreensObsNetcdf
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine initNetcdf
  ! initializes the coordinates of netcdf files
  !----------------------------------------------------------------------
  SUBROUTINE initNetcdf(profile)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: profile

    REAL*8, DIMENSION(:), ALLOCATABLE :: x

    INTEGER :: i,ierr

    ! initialize the number of exports
    profile%ncCount=0

    ALLOCATE(x(profile%n),STAT=ierr)
    IF (ierr/=0) STOP "could not allocate netcdf coordinate"

    ! loop over all patch elements
    DO i=1,profile%n
       x(i)=REAL(i,8)
    END DO

    ! netcdf file is compatible with GMT
    CALL openNetcdfUnlimited( &
             profile%filename, &
             profile%n, &
             x, &
             profile%ncid, &
             profile%y_varid, &
             profile%z_varid)

    DEALLOCATE(x)

  END SUBROUTINE initNetcdf
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportNetcdfVelocity
  ! export time series of log10(v)
  !----------------------------------------------------------------------
  SUBROUTINE exportNetcdfVelocity(profile)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: profile

    REAL*4, DIMENSION(profile%n) :: z

    INTEGER :: j,i
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    ! update the export count
    profile%ncCount=profile%ncCount+1

    ! loop over patch elements by stride
    DO j=1,profile%n
       i=1+profile%offset+(j-1)*profile%stride
       IF (in%rectangle%ns .GE. i) THEN
          patch=in%rectangle%s(i)
       ELSE
          patch=in%triangle%s(i)
       END IF
       ! norm of slip rate vector
       z(j)=REAL(LOG10(SQRT( &
               (patch%Vl*COS(patch%rake)+vAll(1+DGF_PATCH*(i-1)))**2 &
              +(patch%Vl*SIN(patch%rake)+vAll(2+DGF_PATCH*(i-1)))**2 &
               )),4)
    END DO

    CALL writeNetcdfUnlimited(profile%ncid,profile%y_varid,profile%z_varid,profile%ncCount,profile%n,z)

    ! flush every so often
    IF (0 .EQ. MOD(profile%ncCount,50)) THEN
       CALL flushNetcdfUnlimited(profile%ncid,profile%y_varid,profile%ncCount)
    END IF

  END SUBROUTINE exportNetcdfVelocity

  !-----------------------------------------------------------------------
  !> subroutine exportNetcdfSlip
  ! export time series of cumulative slip
  !----------------------------------------------------------------------
  SUBROUTINE exportNetcdfSlip(profile)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: profile

    REAL*4, DIMENSION(profile%n) :: z,zAll

    INTEGER :: j,i,c,l

    ! number of sample per thread and offset per thread
    INTEGER, DIMENSION(csize) :: cAll,oAll

    ! initialize number of profile element in current thread
    c=0

    ! loop over patch elements by stride
    DO j=1,profile%n
       i=1+profile%offset+(j-1)*profile%stride

       ! test whether element is owned by current thread
       IF ((i .GE. layout%listOffset(rank+1)) .AND. &
           (i .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN

          ! state vector index
          l=layout%elementStateIndex(i-layout%listOffset(rank+1))+1

          ! update count for current thread
          c=c+1

          ! norm of cumulative slip vector
          z(c)=REAL(SQRT(y(l+STATE_VECTOR_SLIP_STRIKE)**2+y(l+STATE_VECTOR_SLIP_DIP)**2),4)
       END IF
    END DO

    ! share dimension of all threads among all threads
    CALL MPI_ALLGATHER(c,1,MPI_INTEGER,cAll,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)

    ! thread offset
    oAll(2:csize)=cAll(1:(csize-1))
    oAll(1)=0

    ! master thread gathers data from all threads
    CALL MPI_GATHERV(z,c,MPI_REAL4, &
                     zAll,cAll,oAll,MPI_REAL4, &
                     0,MPI_COMM_WORLD,ierr)

    IF (0 .EQ. rank) THEN
       ! update the export count
       profile%ncCount=profile%ncCount+1

       CALL writeNetcdfUnlimited(profile%ncid,profile%y_varid,profile%z_varid,profile%ncCount,profile%n,zAll)

       ! flush every so often
       IF (0 .EQ. MOD(profile%ncCount,50)) THEN
          CALL flushNetcdfUnlimited(profile%ncid,profile%y_varid,profile%ncCount)
       END IF

    END IF

  END SUBROUTINE exportNetcdfSlip

  !-----------------------------------------------------------------------
  !> subroutine exportNetcdfStress
  ! export time series of instantaneous shear stress
  !----------------------------------------------------------------------
  SUBROUTINE exportNetcdfStress(profile)
    TYPE(PROFILE_STRUCT), INTENT(INOUT) :: profile

    REAL*4, DIMENSION(profile%n) :: z,zAll

    INTEGER :: j,i,c,l

    ! number of sample per thread and offset per thread
    INTEGER, DIMENSION(csize) :: cAll,oAll

    ! initialize number of profile element in current thread
    c=0

    ! loop over patch elements by stride
    DO j=1,profile%n
       i=1+profile%offset+(j-1)*profile%stride

       ! test whether element is owned by current thread
       IF ((i .GE. layout%listOffset(rank+1)) .AND. &
           (i .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN

          ! state vector index
          l=layout%elementStateIndex(i-layout%listOffset(rank+1))+1

          ! update count for current thread
          c=c+1

          ! norm of cumulative shear traction vector
          z(c)=REAL(SQRT(y(l+STATE_VECTOR_TRACTION_STRIKE)**2+y(l+STATE_VECTOR_TRACTION_DIP)**2),4)
       END IF
    END DO

    ! share dimension of all threads among all threads
    CALL MPI_ALLGATHER(c,1,MPI_INTEGER,cAll,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)

    ! thread offset
    oAll(2:csize)=cAll(1:(csize-1))
    oAll(1)=0

    ! master thread gathers data from all threads
    CALL MPI_GATHERV(z,c,MPI_REAL4, &
                     zAll,cAll,oAll,MPI_REAL4, &
                     0,MPI_COMM_WORLD,ierr)

    IF (0 .EQ. rank) THEN
       ! update the export count
       profile%ncCount=profile%ncCount+1

       CALL writeNetcdfUnlimited(profile%ncid,profile%y_varid,profile%z_varid,profile%ncCount,profile%n,zAll)

       ! flush every so often
       IF (0 .EQ. MOD(profile%ncCount,50)) THEN
          CALL flushNetcdfUnlimited(profile%ncid,profile%y_varid,profile%ncCount)
       END IF

    END IF

  END SUBROUTINE exportNetcdfStress
#endif

#ifdef NETCDF
  !-----------------------------------------------------------------------
  !> subroutine exportImageNetcdfVelocity
  ! export an image of patches by strides
  !----------------------------------------------------------------------
  SUBROUTINE exportImageNetcdfVelocity(image)
    TYPE(IMAGE_STRUCT), INTENT(IN) :: image

    REAL*8, DIMENSION(image%nl) :: x
    REAL*8, DIMENSION(image%nw) :: y
    REAL*8, DIMENSION(image%nl,image%nw) :: z

    INTEGER :: j,k,index
    CHARACTER(LEN=256) :: filename
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    ! loop over all patch elements
    DO j=1,image%nl
       x(j)=REAL(j,8)
    END DO
    DO k=1,image%nw
       y(k)=REAL(k,8)
    END DO

    ! loop over patch elements by stride
    DO k=1,image%nw
       DO j=1,image%nl
          index=1+image%offset+(j-1)*image%stride+(k-1)*image%stride*image%nl
          IF (in%rectangle%ns .GE. index) THEN
             patch=in%rectangle%s(index)
          ELSE
             patch=in%triangle%s(index)
          END IF
          ! norm of slip rate vector
          z(j,k)=LOG10(SQRT( &
                  (patch%Vl*COS(patch%rake)+vAll(1+DGF_PATCH*(index-1)))**2 &
                 +(patch%Vl*SIN(patch%rake)+vAll(2+DGF_PATCH*(index-1)))**2 &
                  ))
       END DO
    END DO

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/image-",I6.6,"-log10v.grd")') TRIM(in%wdir),i
    CALL writeNetcdf(filename,image%nl,x,image%nw,y,z,1)

  END SUBROUTINE exportImageNetcdfVelocity

  !-----------------------------------------------------------------------
  !> subroutine exportImageNetcdfVelocityStrike
  ! export an image of patches by strides
  !----------------------------------------------------------------------
  SUBROUTINE exportImageNetcdfVelocityStrike(image)
    TYPE(IMAGE_STRUCT), INTENT(IN) :: image

    REAL*8, DIMENSION(image%nl) :: x
    REAL*8, DIMENSION(image%nw) :: y
    REAL*8, DIMENSION(image%nl,image%nw) :: z

    INTEGER :: j,k,index
    CHARACTER(LEN=256) :: filename
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    ! loop over all patch elements
    DO j=1,image%nl
       x(j)=REAL(j,8)
    END DO
    DO k=1,image%nw
       y(k)=REAL(k,8)
    END DO

    ! loop over patch elements by stride
    DO k=1,image%nw
       DO j=1,image%nl
          index=1+image%offset+(j-1)*image%stride+(k-1)*image%stride*image%nl
          IF (in%rectangle%ns .GE. index) THEN
             patch=in%rectangle%s(index)
          ELSE
             patch=in%triangle%s(index)
          END IF
          ! norm of slip rate vector
          z(j,k)=patch%Vl*COS(patch%rake)+vAll(1+DGF_PATCH*(index-1))
       END DO
    END DO

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/image-",I6.6,"-v-strike.grd")') TRIM(in%wdir),i
    CALL writeNetcdf(filename,image%nl,x,image%nw,y,z,1)

  END SUBROUTINE exportImageNetcdfVelocityStrike

  !-----------------------------------------------------------------------
  !> subroutine exportImageNetcdfVelocityDip
  ! export an image of patches by strides
  !----------------------------------------------------------------------
  SUBROUTINE exportImageNetcdfVelocityDip(image)
    TYPE(IMAGE_STRUCT), INTENT(IN) :: image

    REAL*8, DIMENSION(image%nl) :: x
    REAL*8, DIMENSION(image%nw) :: y
    REAL*8, DIMENSION(image%nl,image%nw) :: z

    INTEGER :: j,k,index
    CHARACTER(LEN=256) :: filename
    TYPE(PATCH_ELEMENT_STRUCT) :: patch

    ! loop over all patch elements
    DO j=1,image%nl
       x(j)=REAL(j,8)
    END DO
    DO k=1,image%nw
       y(k)=REAL(k,8)
    END DO

    ! loop over patch elements by stride
    DO k=1,image%nw
       DO j=1,image%nl
          index=1+image%offset+(j-1)*image%stride+(k-1)*image%stride*image%nl
          IF (in%rectangle%ns .GE. index) THEN
             patch=in%rectangle%s(index)
          ELSE
             patch=in%triangle%s(index)
          END IF
          ! norm of slip rate vector
          z(j,k)=patch%Vl*SIN(patch%rake)+vAll(2+DGF_PATCH*(index-1))
       END DO
    END DO

    ! netcdf file is compatible with GMT
    WRITE (filename,'(a,"/image-",I6.6,"-v-dip.grd")') TRIM(in%wdir),i
    CALL writeNetcdf(filename,image%nl,x,image%nw,y,z,1)

  END SUBROUTINE exportImageNetcdfVelocityDip
#endif

  !-----------------------------------------------------------------------
  !> subroutine exportAsciiSlipDistribution
  ! export cumulative fault slip to ASCII file
  !----------------------------------------------------------------------
  SUBROUTINE exportAsciiSlipDistribution()

    INTEGER :: k,l

    ! loop over elements owned by current thread
    DO k=1,SIZE(layout%elementIndex)
       ! element index in state vector
       l=layout%elementStateIndex(k)-in%dPatch+1

       ! cumulative slip
       vector(2*(k-1)+1)=REAL(y(l+STATE_VECTOR_SLIP_STRIKE),4)
       vector(2*(k-1)+2)=REAL(y(l+STATE_VECTOR_SLIP_DIP),4)
    END DO

    ! all threads gather velocity from all threads
    CALL MPI_GATHERV(vector,layout%listVelocityN(1+rank),MPI_REAL4, &
                     vectorAll,layout%listVelocityN,layout%listVelocityOffset,MPI_REAL4, &
                     0,MPI_COMM_WORLD,ierr)

    ! export to ASCII
    IF (0 .EQ. rank) THEN
          ! export the fault patches
          WRITE (filename,'(a,"/slip-distribution-",I8.8,".dat")') TRIM(in%wdir),i
          OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          WRITE (FPOUT,'("# strike-slip, dip-slip")')
          DO k=1,in%npatch
             WRITE (FPOUT,'(2ES18.10E2)') vectorAll(2*(k-1)+1), vectorAll(2*(k-1)+2)
          END DO
          CLOSE(FPOUT)
    END IF

  END SUBROUTINE exportAsciiSlipDistribution

  !-----------------------------------------------------------------------
  !> subroutine exportGeometry
  ! export geometry of fault patches
  !----------------------------------------------------------------------
  SUBROUTINE exportGeometry(in)
    IMPLICIT NONE
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in

    CHARACTER(512) :: filename

    IF (0 .EQ. rank) THEN
       IF (0 .NE. in%rectangle%ns) THEN
          IF (in%isExportVTP) THEN
             WRITE (filename,'(a,"/patch-rectangle.vtp")') TRIM(in%wdir)
             OPEN (UNIT=FPVTP,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
             IF (ierr>0) THEN
                WRITE_DEBUG_INFO(102)
                WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
                STOP 1
             END IF
             CALL exportvtk_rfaults(in%rectangle%ns, &
                                    in%rectangle%x, &
                                    in%rectangle%length, &
                                    in%rectangle%width, &
                                    in%rectangle%strike, &
                                    in%rectangle%dip, &
                                    in%rectangle%s, &
                                    FPVTP)
             CLOSE(FPVTP)
          END IF

          IF (in%isExportXYZ) THEN
             WRITE (filename,'(a,"/patch-rectangle.xyz")') TRIM(in%wdir)
             OPEN (UNIT=FPVTP,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
             IF (ierr>0) THEN
                WRITE_DEBUG_INFO(102)
                WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
                STOP 1
             END IF
             CALL exportxyz_rfaults(in%rectangle%ns, &
                                    in%rectangle%x, &
                                    in%rectangle%length, &
                                    in%rectangle%width, &
                                    in%rectangle%strike, &
                                    in%rectangle%dip, &
                                    in%rectangle%s, &
                                    FPVTP)
             CLOSE(FPVTP)
          END IF
       END IF

       IF (0 .NE. in%triangle%ns) THEN
          IF (in%isExportVTP) THEN
             WRITE (filename,'(a,"/patch-triangle.vtp")') TRIM(in%wdir)
             OPEN (UNIT=FPVTP,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
             IF (ierr>0) THEN
                WRITE_DEBUG_INFO(102)
                WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
                STOP 1
             END IF
             CALL exportvtk_tfaults(in%triangle%ns, &
                                    in%triangle%i1, &
                                    in%triangle%i2, &
                                    in%triangle%i3, &
                                    in%triangle%nVe, &
                                    in%triangle%v, &
                                    in%triangle%s, &
                                    FPVTP)
             CLOSE(FPVTP)
          END IF
       END IF

       IF (0 .NE. in%cuboid%ns) THEN
          IF (in%isExportVTP) THEN
             WRITE (filename,'(a,"/cuboid.vtp")') TRIM(in%wdir)
             OPEN (UNIT=FPVTP,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
             IF (ierr>0) THEN
                WRITE_DEBUG_INFO(102)
                WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
                STOP 1
             END IF
             CALL exportvtk_cuboids(in%cuboid%ns, &
                                    in%cuboid%x, &
                                    in%cuboid%length, &
                                    in%cuboid%thickness, &
                                    in%cuboid%width, &
                                    in%cuboid%strike, &
                                    in%cuboid%dip, &
                                    in%cuboid%s, &
                                    FPVTP)
             CLOSE(FPVTP)
          END IF

          IF (in%isExportXYZ) THEN
             WRITE (filename,'(a,"/cuboid.xyz")') TRIM(in%wdir)
             OPEN (UNIT=FPVTP,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
             IF (ierr>0) THEN
                WRITE_DEBUG_INFO(102)
                WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
                STOP 1
             END IF
             CALL exportxyz_cuboids(in%cuboid%ns, &
                                    in%cuboid%x, &
                                    in%cuboid%length, &
                                    in%cuboid%thickness, &
                                    in%cuboid%width, &
                                    in%cuboid%strike, &
                                    in%cuboid%dip, &
                                    in%cuboid%s, &
                                    FPVTP)
             CLOSE(FPVTP)
          END IF
       END IF

       IF (0 .NE. in%tetrahedron%ns) THEN
          IF (in%isExportVTP) THEN
             WRITE (filename,'(a,"/tetrahedron.vtp")') TRIM(in%wdir)
             OPEN (UNIT=FPVTP,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
             IF (ierr>0) THEN
                WRITE_DEBUG_INFO(102)
                WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
                STOP 1
             END IF
             CALL exportvtk_tetrahedron(in%tetrahedron%ns, &
                                    in%tetrahedron%i1, &
                                    in%tetrahedron%i2, &
                                    in%tetrahedron%i3, &
                                    in%tetrahedron%i4, &
                                    in%tetrahedron%nVe, &
                                    in%tetrahedron%v, &
                                    in%tetrahedron%s, &
                                    FPVTP)
             CLOSE(FPVTP)
          END IF

          IF (in%isExportXYZ) THEN
             WRITE (filename,'(a,"/tetrahedron.xyz")') TRIM(in%wdir)
             OPEN (UNIT=FPVTP,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
             IF (ierr>0) THEN
                WRITE_DEBUG_INFO(102)
                WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
                STOP 1
             END IF
             CALL exportxyz_tetrahedron(in%tetrahedron%ns, &
                                    in%tetrahedron%i1, &
                                    in%tetrahedron%i2, &
                                    in%tetrahedron%i3, &
                                    in%tetrahedron%i4, &
                                    in%tetrahedron%nVe, &
                                    in%tetrahedron%v, &
                                    in%tetrahedron%s, &
                                    FPVTP)
             CLOSE(FPVTP)
          END IF
       END IF
    END IF

  END SUBROUTINE exportGeometry

  !-----------------------------------------------------------------------
  !> subroutine exportVTK
  ! export geometry of fault patches
  !----------------------------------------------------------------------
  SUBROUTINE exportVTK(in)
    IMPLICIT NONE
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in

    CHARACTER(512) :: filename

    IF (0 .EQ. rank) THEN
       IF (0 .NE. in%rectangle%ns) THEN
          WRITE (filename,'(a,"/patch-rectangle-",I7.7,".vtp")') TRIM(in%wdir),i
          OPEN (UNIT=FPVTP,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          CALL exportvtk_rfaults_field(in%rectangle%ns, &
                                 in%rectangle%x, &
                                 in%rectangle%length, &
                                 in%rectangle%width, &
                                 in%rectangle%strike, &
                                 in%rectangle%dip, &
                                 FPVTP,vAll)
          CLOSE(FPVTP)
       END IF

       IF (0 .NE. in%triangle%ns) THEN
          WRITE (filename,'(a,"/patch-triangle-",I7.7,".vtp")') TRIM(in%wdir),i
          OPEN (UNIT=FPVTP,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          CALL exportvtk_tfaults_field(in%triangle%ns, &
                                 in%triangle%i1, &
                                 in%triangle%i2, &
                                 in%triangle%i3, &
                                 in%triangle%nVe, &
                                 in%triangle%v, &
                                 FPVTP,vAll(1+in%rectangle%ns*DGF_PATCH))
          CLOSE(FPVTP)
       END IF

       IF (0 .NE. in%cuboid%ns) THEN
          WRITE (filename,'(a,"/cuboid-",I7.7,".vtp")') TRIM(in%wdir),i
          OPEN (UNIT=FPVTP,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          CALL exportvtk_cuboids_field(in%cuboid%ns, &
                                 in%cuboid%x, &
                                 in%cuboid%length, &
                                 in%cuboid%thickness, &
                                 in%cuboid%width, &
                                 in%cuboid%strike, &
                                 in%cuboid%dip, &
                                 FPVTP,vAll(1+(in%rectangle%ns+in%triangle%ns)*DGF_PATCH))
          CLOSE(FPVTP)
       END IF

       IF (0 .NE. in%tetrahedron%ns) THEN
          WRITE (filename,'(a,"/tetrahedron-",I7.7,".vtp")') TRIM(in%wdir),i
          OPEN (UNIT=FPVTP,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          CALL exportvtk_tetrahedron_field(in%tetrahedron%ns, &
                                 in%tetrahedron%i1, &
                                 in%tetrahedron%i2, &
                                 in%tetrahedron%i3, &
                                 in%tetrahedron%i4, &
                                 in%tetrahedron%nVe, &
                                 in%tetrahedron%v, &
                                 FPVTP,vAll(1+(in%rectangle%ns+in%triangle%ns)*DGF_PATCH &
                                              +in%cuboid%ns*DGF_VOLUME))
          CLOSE(FPVTP)
       END IF
    END IF

  END SUBROUTINE exportVTK

  !-----------------------------------------------------------------------
  !> subroutine exportXYZ
  ! export dynamic variables of surface and volume elements
  !----------------------------------------------------------------------
  SUBROUTINE exportXYZ(in)
    IMPLICIT NONE
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in

    CHARACTER(512) :: filename

    IF (0 .EQ. rank) THEN
       IF (0 .NE. in%rectangle%ns) THEN
          WRITE (filename,'(a,"/patch-rectangle-",I7.7,".xyz")') TRIM(in%wdir),i
          OPEN (UNIT=FPVTP,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          CALL exportxyz_rfaults_field(in%rectangle%ns, &
                                 in%rectangle%x, &
                                 in%rectangle%length, &
                                 in%rectangle%width, &
                                 in%rectangle%strike, &
                                 in%rectangle%dip, &
                                 FPVTP,vAll)
          CLOSE(FPVTP)
       END IF

       IF (0 .NE. in%triangle%ns) THEN
          WRITE (filename,'(a,"/patch-triangle-",I7.7,".xyz")') TRIM(in%wdir),i
          OPEN (UNIT=FPVTP,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          CALL exportxyz_tfaults_field(in%triangle%ns, &
                                 in%triangle%i1, &
                                 in%triangle%i2, &
                                 in%triangle%i3, &
                                 in%triangle%nVe, &
                                 in%triangle%v, &
                                 FPVTP,vAll(1+in%rectangle%ns*DGF_PATCH))
          CLOSE(FPVTP)
       END IF

       IF (0 .NE. in%cuboid%ns) THEN
          WRITE (filename,'(a,"/cuboid-",I7.7,".xyz")') TRIM(in%wdir),i
          OPEN (UNIT=FPVTP,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          CALL exportxyz_cuboids_field(in%cuboid%ns, &
                                 in%cuboid%x, &
                                 in%cuboid%length, &
                                 in%cuboid%thickness, &
                                 in%cuboid%width, &
                                 in%cuboid%strike, &
                                 in%cuboid%dip, &
                                 FPVTP,vAll(1+(in%rectangle%ns+in%triangle%ns)*DGF_PATCH))
          CLOSE(FPVTP)
       END IF

       IF (0 .NE. in%tetrahedron%ns) THEN
          WRITE (filename,'(a,"/tetrahedron-",I7.7,".xyz")') TRIM(in%wdir),i
          OPEN (UNIT=FPVTP,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          CALL exportxyz_tetrahedron_field(in%tetrahedron%ns, &
                                 in%tetrahedron%i1, &
                                 in%tetrahedron%i2, &
                                 in%tetrahedron%i3, &
                                 in%tetrahedron%i4, &
                                 in%tetrahedron%nVe, &
                                 in%tetrahedron%v, &
                                 FPVTP,vAll(1+(in%rectangle%ns+in%triangle%ns)*DGF_PATCH &
                                              +in%cuboid%ns*DGF_VOLUME))
          CLOSE(FPVTP)
       END IF
    END IF

  END SUBROUTINE exportXYZ

  !-----------------------------------------------------------------------
  !> subroutine exportPoints
  ! export observation points
  !----------------------------------------------------------------------
  SUBROUTINE exportPoints()
    IMPLICIT NONE

    INTEGER :: i,k,l,ierr
    INTEGER :: elementIndex,elementType
    CHARACTER(1024) :: formatString
    TYPE(PATCH_ELEMENT_STRUCT) :: patch
    TYPE(VOLUME_ELEMENT_STRUCT) :: volume

    !-----------------------------------------------------------------
    ! step 1/3 - gather the kinematics from the state vector
    !-----------------------------------------------------------------

    ! initialize
    u=0._8

    ! element index in d vector
    k=1
    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementIndex=layout%elementIndex(i)
       elementType= layout%elementType(i)

       SELECT CASE (elementType)
       CASE (FLAG_RECTANGLE,FLAG_TRIANGLE)

          SELECT CASE (elementType)
          CASE (FLAG_RECTANGLE)
             patch=in%rectangle%s(elementIndex)
          CASE (FLAG_TRIANGLE)
             patch=in%triangle%s(elementIndex)
          CASE DEFAULT
             WRITE(STDERR,'("wrong case: this is a bug.")')
             WRITE_DEBUG_INFO(-1)
             STOP 2
          END SELECT

          ! strike slip and dip slip
          u(k:k+layout%elementVelocityDGF(i)-1)= (/ &
                  y(l+STATE_VECTOR_SLIP_STRIKE), &
                  y(l+STATE_VECTOR_SLIP_DIP) /)

          l=l+in%dPatch

       CASE (FLAG_CUBOID,FLAG_TETRAHEDRON)

          SELECT CASE (elementType)
          CASE (FLAG_CUBOID)
             volume=in%cuboid%s(elementIndex)
          CASE (FLAG_TETRAHEDRON)
             volume=in%tetrahedron%s(elementIndex)
          CASE DEFAULT
             WRITE(STDERR,'("wrong case: this is a bug.")')
             WRITE_DEBUG_INFO(-1)
             STOP 2
          END SELECT

          u(k:k+layout%elementVelocityDGF(i)-1)=(/ &
                  y(l+STATE_VECTOR_E11), &
                  y(l+STATE_VECTOR_E12), &
                  y(l+STATE_VECTOR_E13), &
                  y(l+STATE_VECTOR_E22), &
                  y(l+STATE_VECTOR_E23), &
                  y(l+STATE_VECTOR_E33) /)

          l=l+in%dVolume

       CASE DEFAULT
          WRITE(STDERR,'("wrong case: this is a bug.")')
          WRITE_DEBUG_INFO(-1)
          STOP 2
       END SELECT

       k=k+layout%elementVelocityDGF(i)

    END DO

    !-----------------------------------------------------------------
    ! step 2/3 - calculate the rate of traction and rate of stress
    !            master thread adds the contribution of all elements
    !-----------------------------------------------------------------

#ifdef __BLAS__
    ! use the BLAS library to compute the matrix vector product
    CALL DGEMV("T",SIZE(O,1),SIZE(O,2), &
                1._8,O,SIZE(O,1),u,1,0.d0,d,1)
#else
    ! slower, Fortran intrinsic
    d=MATMUL(TRANSPOSE(O),u)
#endif

    CALL MPI_REDUCE(d,dAll,in%nObservationPoint*DISPLACEMENT_VECTOR_DGF, &
                    MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

#ifdef __BLAS__
    ! use the BLAS library to compute the matrix vector product
    CALL DGEMV("T",SIZE(Of,1),SIZE(Of,2), &
                1._8,Of,SIZE(Of,1),u,1,0.d0,d,1)
#else
    ! slower, Fortran intrinsic
    d=MATMUL(TRANSPOSE(Of),u)
#endif

    CALL MPI_REDUCE(d,dfAll,in%nObservationPoint*DISPLACEMENT_VECTOR_DGF, &
                    MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

#ifdef __BLAS__
    ! use the BLAS library to compute the matrix vector product
    CALL DGEMV("T",SIZE(Ol,1),SIZE(Ol,2), &
                1._8,Ol,SIZE(Ol,1),u,1,0.d0,d,1)
#else
    ! slower, Fortran intrinsic
    d=MATMUL(TRANSPOSE(Ol),u)
#endif

    CALL MPI_REDUCE(d,dlAll,in%nObservationPoint*DISPLACEMENT_VECTOR_DGF, &
                    MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

    !-----------------------------------------------------------------
    ! step 3/3 - master thread writes to disk
    !-----------------------------------------------------------------

    IF (0 .EQ. rank) THEN
       formatString="(ES20.14E2"
       DO i=1,DISPLACEMENT_VECTOR_DGF
          formatString=TRIM(formatString)//",X,ES19.12E2,X,ES19.12E2,X,ES19.12E2"
       END DO
       formatString=TRIM(formatString)//")"

       ! element index in d vector
       k=1
       DO i=1,in%nObservationPoint
          WRITE (in%observationPoint(i)%file,TRIM(formatString)) &
                  time, &
                  dAll (k:k+DISPLACEMENT_VECTOR_DGF-1), &
                  dfAll(k:k+DISPLACEMENT_VECTOR_DGF-1), &
                  dlAll(k:k+DISPLACEMENT_VECTOR_DGF-1)
          k=k+DISPLACEMENT_VECTOR_DGF
       END DO
    END IF

  END SUBROUTINE exportPoints

  !-----------------------------------------------------------------------
  !> subroutine export
  ! write the state variables of elements, either patch or volume, and 
  ! other information.
  !----------------------------------------------------------------------
  SUBROUTINE export()

    ! degrees of freedom
    INTEGER :: dgf

    ! counters
    INTEGER :: j,k

    ! index in state vector
    INTEGER :: index

    ! format string
    CHARACTER(1024) :: formatString

    ! gather maximum velocity
    CALL MPI_REDUCE(vMax,vMaxAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

    ! gather moment-rate
    CALL MPI_REDUCE(momentRate,momentRateAll,1,MPI_REAL8,MPI_SUM,0,MPI_COMM_WORLD,ierr)

    ! gather maximum strain-rate
    CALL MPI_REDUCE(eMax,eMaxAll,1,MPI_REAL8,MPI_MAX,0,MPI_COMM_WORLD,ierr)

    ! export observation state
    DO j=1,in%nObservationState
       IF ((in%observationState(1,j) .GE. layout%listOffset(rank+1)) .AND. &
           (in%observationState(1,j) .LT. layout%listOffset(rank+1)+layout%listElements(rank+1))) THEN

          ! check observation state sampling rate
          IF (0 .EQ. MOD(i-1,in%observationState(2,j))) THEN
             SELECT CASE(layout%elementType(in%observationState(1,j)-layout%listOffset(rank+1)+1))
             CASE (FLAG_RECTANGLE,FLAG_TRIANGLE)
                dgf=in%dPatch
             CASE (FLAG_CUBOID,FLAG_TETRAHEDRON)
                dgf=in%dVolume
             CASE DEFAULT
                WRITE(STDERR,'("wrong case: this is a bug.")')
                WRITE_DEBUG_INFO(-1)
                STOP 2
             END SELECT

             formatString="(ES21.15E2"
             DO k=1,dgf
                formatString=TRIM(formatString)//",X,ES20.12E3,X,ES20.12E3"
             END DO
             formatString=TRIM(formatString)//")"

             index=layout%elementStateIndex(in%observationState(1,j)-layout%listOffset(rank+1)+1)-dgf
             WRITE (in%observationState(4,j),TRIM(formatString)) time, &
                       y(index+1:index+dgf), &
                    dydt(index+1:index+dgf)
         END IF
       END IF
    END DO

    IF (0 .EQ. rank) THEN
       WRITE(FPTIME,'(ES20.14E2,ES19.12E2,ES19.12E2,2ES20.12E2)') time,dt_done,vMaxAll,momentRateAll,eMaxAll
       IF (0 .EQ. MOD(i,50)) THEN
          WRITE(STDOUT,'(I9.9,ES19.12E2,ES19.12E2,2ES11.4E2)') i,time,dt_done,vMaxAll,eMaxAll
          CALL FLUSH(STDOUT)
          CALL FLUSH(FPTIME)
          IF (in%isExportVTP) THEN
             CALL exportVTK(in)
          END IF
          IF (in%isExportXYZ) THEN
             CALL exportXYZ(in)
          END IF
       END IF
    END IF

  END SUBROUTINE export

  !-----------------------------------------------------------------------
  !> subroutine initStateVector
  ! initialize the state vector
  !
  ! INPUT:
  ! @param n - number of state elements own by current thread
  ! @param y - the state vector (segment owned by currect thread)
  !
  !----------------------------------------------------------------------
  SUBROUTINE initStateVector(n,y,in)
    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: n
    REAL*8, INTENT(OUT)    :: y(n)
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in

    INTEGER :: i,l
    INTEGER :: elementType,elementIndex
    TYPE(PATCH_ELEMENT_STRUCT) :: patch
    TYPE(VOLUME_ELEMENT_STRUCT) :: volume

    ! velocity perturbation
    REAL*8, PARAMETER :: modifier = 0.99d0

    ! norm of loading rate
    REAL*8 :: eps

    ! initial stress
    REAL*8 :: tau

    ! initial velocity
    REAL*8 :: Vinit

    ! patch area
    REAL*8 :: area

    ! isotropic strain
    REAL*8 :: ekk

    ! deviatoric strain components
    REAL*8 :: e11p,e22p,e33p

    ! stress components
    REAL*8 :: s11, s12, s13, s22, s23, s33

    ! zero out state vector
    y=0._8

    ! maximum velocity
    vMax=0._8

    ! moment-rate
    momentRate=0._8

    ! maximum strain rate
    eMax=0._8

    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementType= layout%elementType(i)
       elementIndex=layout%elementIndex(i)

       SELECT CASE (elementType)
       CASE (FLAG_RECTANGLE,FLAG_TRIANGLE)

          SELECT CASE (elementType)
          CASE (FLAG_RECTANGLE)
             patch=in%rectangle%s(elementIndex)
             area=in%rectangle%length(elementIndex)*in%rectangle%width(elementIndex)
          CASE (FLAG_TRIANGLE)
             patch=in%triangle%s(elementIndex)
             area=in%triangle%area(elementIndex)
          CASE DEFAULT
             WRITE (0,'("unhandled option ", a, " (this is a bug")') elementType
             WRITE_DEBUG_INFO(100)
             STOP 3
          END SELECT

          ! strike slip
          y(l+STATE_VECTOR_SLIP_STRIKE) = 0._8

          ! dip slip
          y(l+STATE_VECTOR_SLIP_DIP) = 0._8

          ! traction
          IF (0 .GT. patch%tau0) THEN

             ! initial velocity
             Vinit=modifier*patch%Vl

             SELECT CASE(frictionLawType)
             CASE(1)
                ! multiplicative form of rate-state friction (Barbot, 2019)
                tau = patch%mu0*patch%sig*exp((patch%a-patch%b)/patch%mu0*log(patch%Vl/patch%Vo))
             CASE(2)
                ! additive form of rate-state friction (Ruina, 1983)
                tau = patch%sig*(patch%mu0+(patch%a-patch%b)*log(patch%Vl/patch%Vo))
             CASE(3)
                ! arcsinh form of rate-state friction (Rice & Benzion, 1996)
                tau = patch%a*patch%sig*ASINH(patch%Vl/2/patch%Vo*exp((patch%mu0+patch%b*log(patch%Vo/patch%Vl))/patch%a))
             CASE(4)
                ! cut-off velocity form of rate-state friction (Okubo, 1989)
                tau = patch%sig*(patch%mu0-patch%a*log(1._8+patch%Vo/patch%Vl)+patch%b*log(1._8+patch%V2/patch%Vl))
             CASE DEFAULT
                WRITE (0,'("unhandled option ", a, " (this is a bug")') frictionLawType
                WRITE_DEBUG_INFO(100)
                STOP 3
             END SELECT
          ELSE
             tau = patch%tau0
             SELECT CASE(frictionLawType)
             CASE(1)
                ! multiplicative form of rate-state friction (Barbot, 2019)
                Vinit=patch%Vo*(tau/(patch%mu0*patch%sig))**(patch%mu0/patch%a)*(patch%Vo/patch%Vl)**(patch%mu0/patch%b)
             CASE(2)
                ! additive form of rate-state friction (Ruina, 1983)
                Vinit=patch%Vo*EXP((tau/patch%sig-patch%mu0-patch%b*LOG(patch%Vo/patch%Vl))/patch%a)
             CASE(3)
                ! arcsinh form of rate-state friction (Rice & Benzion, 1996)
                Vinit=2*patch%Vo*SINH(tau/(patch%a*patch%sig))*EXP(-((patch%mu0+patch%b*LOG(patch%Vo/Patch%Vl))/patch%a))
             CASE DEFAULT
                WRITE (0,'("unhandled option ", a, " (this is a bug")') frictionLawType
                WRITE_DEBUG_INFO(100)
                STOP 3
             END SELECT
          END IF

          ! traction in strike direction
          y(l+STATE_VECTOR_TRACTION_STRIKE) = tau*COS(patch%rake)

          ! traction in dip direction
          y(l+STATE_VECTOR_TRACTION_DIP) = tau*SIN(patch%rake)

          ! traction in normal direction
          y(l+STATE_VECTOR_TRACTION_NORMAL) = 0._8

          ! state variable log10(theta)
          y(l+STATE_VECTOR_STATE_1) = LOG(patch%L/patch%Vl)/lg10

          ! maximum velocity
          vMax=MAX(patch%Vl,vMax)

          ! moment-rate
          momentRate=momentRate+(Vinit-patch%Vl)*in%mu*area

          ! slip velocity log10(V)
          y(l+STATE_VECTOR_VELOCITY) = LOG(Vinit)/lg10

          l=l+in%dPatch

       CASE (FLAG_CUBOID,FLAG_TETRAHEDRON)

          SELECT CASE(elementType)
          CASE(FLAG_CUBOID)
             ! cuboid volume element
             volume=in%cuboid%s(elementIndex)
          CASE(FLAG_TETRAHEDRON)
             ! tetrahedron volume element
             volume=in%tetrahedron%s(elementIndex)
          CASE DEFAULT
                WRITE (0,'("unhandled option ", a, " (this is a bug")') elementType
                WRITE_DEBUG_INFO(100)
                STOP 3
          END SELECT

          ! isotropic strain-rate
          ekk=volume%e11+volume%e22+volume%e33

          ! deviatoric strain-rate
          e11p=volume%e11-ekk/3d0
          e22p=volume%e22-ekk/3d0
          e33p=volume%e33-ekk/3d0

          ! norm of loading rate
          eps=SQRT((e11p**2+ &
                    2*volume%e12**2+ &
                    2*volume%e13**2+ &
                    e22p**2+ &
                    2*volume%e23**2+ &
                    e33p**2)/2._8)

          ! stress components s11, s12, s13, s22, s23, and s33
          IF (0._8 .GT. volume%sII) THEN
             IF ((0d0 .LT. eps) .AND. (0d0 .LT. volume%ngammadot0m)) THEN
                tau=(eps/volume%ngammadot0m)**(1d0/volume%npowerm)
                s11 = tau*e11p/eps
                s12 = tau*volume%e12/eps
                s13 = tau*volume%e13/eps
                s22 = tau*e22p/eps
                s23 = tau*volume%e23/eps
                s33 = tau*e33p/eps
             ELSE
                s11 = 0._8
                s12 = 0._8
                s13 = 0._8
                s22 = 0._8
                s23 = 0._8
                s33 = 0._8
             END IF
          ELSE
             IF (0d0 .LT. eps) THEN
                s11 = volume%sII*e11p/eps
                s12 = volume%sII*volume%e12/eps
                s13 = volume%sII*volume%e13/eps
                s22 = volume%sII*e22p/eps
                s23 = volume%sII*volume%e23/eps
                s33 = volume%sII*e33p/eps
             ELSE
                s11 = 0._8
                s12 = 0._8
                s13 = 0._8
                s22 = 0._8
                s23 = 0._8
                s33 = 0._8
             END IF
          END IF

          ! stress components s11, s12, s13, s22, s23, and s33
          y(l+STATE_VECTOR_S11) = s11*modifier
          y(l+STATE_VECTOR_S12) = s12*modifier
          y(l+STATE_VECTOR_S13) = s13*modifier
          y(l+STATE_VECTOR_S22) = s22*modifier
          y(l+STATE_VECTOR_S23) = s23*modifier
          y(l+STATE_VECTOR_S33) = s33*modifier

          ! strain components
          y(l+STATE_VECTOR_E11) = 0._8
          y(l+STATE_VECTOR_E12) = 0._8
          y(l+STATE_VECTOR_E13) = 0._8
          y(l+STATE_VECTOR_E22) = 0._8
          y(l+STATE_VECTOR_E23) = 0._8
          y(l+STATE_VECTOR_E33) = 0._8

          ! maximum strain rate
          eMax=MAX(eMax,eps)

          l=l+in%dVolume

       CASE DEFAULT
          WRITE(STDERR,'("wrong case: this is a bug.")')
          WRITE_DEBUG_INFO(-1)
          STOP 2
       END SELECT

    END DO

  END SUBROUTINE initStateVector

  !-----------------------------------------------------------------------
  !> subroutine odefun
  ! evalutes the derivative of the state vector
  !
  ! @param n - number of state elements own by current thread
  ! @param m - degrees of freedom
  !
  ! DESCRIPTION:
  !   1- extract slip velocity and strain rate from state vector
  !   2- calculate the rate of traction and rate of stress
  !   3- calculate the rate of remaining state variables
  !----------------------------------------------------------------------
  SUBROUTINE odefun(n,time,y,dydt)
    IMPLICIT NONE

    INTEGER, INTENT(IN)   :: n
    REAL*8, INTENT(IN)    :: time
    REAL*8, INTENT(IN)    :: y(n)
    REAL*8, INTENT(INOUT) :: dydt(n)

    INTEGER :: i,j,k,l,ierr
    INTEGER :: elementType,elementIndex
    TYPE(PATCH_ELEMENT_STRUCT) :: patch
    TYPE(VOLUME_ELEMENT_STRUCT) :: volume
    REAL*8 :: correction
    INTEGER :: nNonlinearMaxwell=0

    ! traction components in the strike and dip directions
    REAL*8 :: ts, td

    ! scalar rate of shear traction
    REAL*8 :: dtau

    ! velocity scalar
    REAL*8 :: velocity

    ! slip velocity in the strike and dip directions
    REAL*8 :: vs,vd

    ! rake of traction and velocity
    REAL*8 :: rake

    ! friction
    REAL*8 :: friction

    ! state variable theta
    REAL*8 :: theta

    ! normal stress
    REAL*8 :: sigma

    ! modifier for the arcsinh form of rate-state friction
    REAL*8 :: reg

    ! patch area
    REAL*8 :: area

    ! independent components of the stress tensor
    REAL*8 :: s11,s12,s13,s22,s23,s33

    ! norm of deviatoric stress
    REAL*8 :: sII

    ! pressure
    REAL*8 :: p

    ! independent components of the strain rate tensor
    REAL*8 :: e11,e12,e13,e22,e23,e33

    ! scalar strain rate
    REAL*8 :: eII

    ! initialize rate of state vector
    dydt=0._8

    ! maximum strain rate
    eMax=0._8

    ! maximum velocity
    vMax=0._8

    ! initialize moment-rate
    momentRate=0._8

    ! initialize rate of state vector
    dydt=0._8

    !--------------------------------------------------------------------
    ! step 1/3 - extract slip velocity and strain rate from state vector
    !--------------------------------------------------------------------

    ! element index in v vector
    k=1
    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementType= layout%elementType(i)
       elementIndex=layout%elementIndex(i)

       SELECT CASE (elementType)
       CASE (FLAG_RECTANGLE,FLAG_TRIANGLE)
          ! v(k:k+layout%elementVelocityDGF(i)-1) = slip velocity

          SELECT CASE (elementType)
          CASE (FLAG_RECTANGLE)
             patch=in%rectangle%s(elementIndex)
             area=in%rectangle%length(elementIndex)*in%rectangle%width(elementIndex)
          CASE (FLAG_TRIANGLE)
             patch=in%triangle%s(elementIndex)
             area=in%triangle%area(elementIndex)
          CASE DEFAULT
             WRITE(STDERR,'("wrong case: this is a bug.")')
             WRITE_DEBUG_INFO(-1)
             STOP 2
          END SELECT

          ! traction and rake
          ts=y(l+STATE_VECTOR_TRACTION_STRIKE)
          td=y(l+STATE_VECTOR_TRACTION_DIP)
          rake=ATAN2(td,ts)

          ! slip velocity
          velocity=DEXP(y(l+STATE_VECTOR_VELOCITY)*lg10)
          vs=velocity*COS(rake)
          vd=velocity*SIN(rake)

          ! maximum velocity
          vMax=MAX(velocity,vMax)

          ! moment-rate
          momentRate=momentRate+(velocity-patch%Vl)*in%mu*area

          ! update state vector (rate of slip components)
          dydt(l+STATE_VECTOR_SLIP_STRIKE)=vs
          dydt(l+STATE_VECTOR_SLIP_DIP   )=vd

          v(k:k+layout%elementVelocityDGF(i)-1)=(/ &
                        vs-patch%Vl*COS(patch%rake), &
                        vd-patch%Vl*SIN(patch%rake) /)

          l=l+in%dPatch

       CASE (FLAG_CUBOID,FLAG_TETRAHEDRON)
          ! v(k:k+layout%elementVelocityDGF(i)-1)= strain rate

          SELECT CASE (elementType)
          CASE (FLAG_CUBOID)
             ! cuboid volume
             volume=in%cuboid%s(elementIndex)
             nNonlinearMaxwell=in%cuboid%nNonlinearMaxwell
          CASE (FLAG_TETRAHEDRON)
             ! tetrahedron volume
             volume=in%tetrahedron%s(elementIndex)
             nNonlinearMaxwell=in%tetrahedron%nNonlinearMaxwell
          CASE DEFAULT
             WRITE (0,'("unhandled option ", a, " (this is a bug")') elementType
             WRITE_DEBUG_INFO(100)
             STOP 3
          END SELECT

          ! stress components
          s11=y(l+STATE_VECTOR_S11)
          s12=y(l+STATE_VECTOR_S12)
          s13=y(l+STATE_VECTOR_S13)
          s22=y(l+STATE_VECTOR_S22)
          s23=y(l+STATE_VECTOR_S23)
          s33=y(l+STATE_VECTOR_S33)

          ! pressure
          p=(s11+s22+s33)/3._8

          ! deviatoric stress components
          s11=s11-p
          s22=s22-p
          s33=s33-p

          ! initiate strain rate
          e11=0._8
          e12=0._8
          e13=0._8
          e22=0._8
          e23=0._8
          e33=0._8

          sII=SQRT((s11**2+2._8*s12**2+2._8*s13**2+ &
                    s22**2+2._8*s23**2+     s33**2)/2._8)

          ! strain rate from nonlinear viscosity in Maxwell element
          IF (0 .LT. nNonlinearMaxwell) THEN

             eII=volume%ngammadot0m*sII**(volume%npowerm-1)

             e11=e11+eII*s11
             e12=e12+eII*s12
             e13=e13+eII*s13
             e22=e22+eII*s22
             e23=e23+eII*s23
             e33=e33+eII*s33

          END IF

          ! strain rate tensor
          dydt(l+STATE_VECTOR_E11)=e11
          dydt(l+STATE_VECTOR_E12)=e12
          dydt(l+STATE_VECTOR_E13)=e13
          dydt(l+STATE_VECTOR_E22)=e22
          dydt(l+STATE_VECTOR_E23)=e23
          dydt(l+STATE_VECTOR_E33)=e33

          ! maximum strain rate
          eMax=MAX(eMax,SQRT((e11**2+2._8*e12**2+2._8*e13**2+e22**2+2._8*e23**2+e33**2)/2._8))

          v(k:k+layout%elementVelocityDGF(i)-1)=(/ &
                  e11-volume%e11, &
                  e12-volume%e12, &
                  e13-volume%e13, &
                  e22-volume%e22, &
                  e23-volume%e23, &
                  e33-volume%e33 &
                  /)

          l=l+in%dVolume

       CASE DEFAULT
          WRITE(STDERR,'("wrong case: this is a bug.")')
          WRITE_DEBUG_INFO(-1)
          STOP 2
       END SELECT

       k=k+layout%elementVelocityDGF(i)
    END DO

    ! all threads gather velocity from all threads
    CALL MPI_ALLGATHERV(v,layout%listVelocityN(1+rank),MPI_REAL8, &
                        vAll,layout%listVelocityN,layout%listVelocityOffset,MPI_REAL8, &
                        MPI_COMM_WORLD,ierr)

    !-----------------------------------------------------------------
    ! step 2/3 - calculate the rate of traction and rate of stress
    !-----------------------------------------------------------------

#ifdef __BLAS__
    ! use the BLAS library to compute the matrix vector product
    CALL DGEMV("T",SIZE(G,1),SIZE(G,2), &
                1._8,G,SIZE(G,1),vAll,1,0.d0,t,1)
#else
    ! slower, Fortran intrinsic
    t=MATMUL(TRANSPOSE(G),vAll)
#endif

    !-----------------------------------------------------------------
    ! step 3/3 - calculate the rate of remaining state variables
    !-----------------------------------------------------------------

    ! element index in t vector
    j=1
    ! element index in state vector
    l=1
    ! loop over elements owned by current thread
    DO i=1,SIZE(layout%elementIndex)
       elementType= layout%elementType(i)
       elementIndex=layout%elementIndex(i)

       SELECT CASE (elementType)
       CASE (FLAG_RECTANGLE,FLAG_TRIANGLE)

          SELECT CASE (elementType)
          CASE (FLAG_RECTANGLE)
             patch=in%rectangle%s(elementIndex)
          CASE (FLAG_TRIANGLE)
             patch=in%triangle%s(elementIndex)
          CASE DEFAULT
             WRITE(STDERR,'("wrong case: this is a bug.")')
             WRITE_DEBUG_INFO(-1)
             STOP 2
          END SELECT

          ! traction and rake
          ts=y(l+STATE_VECTOR_TRACTION_STRIKE)
          td=y(l+STATE_VECTOR_TRACTION_DIP)
          rake=ATAN2(td,ts)

          ! slip velocity
          velocity=DEXP(y(l+STATE_VECTOR_VELOCITY)*lg10)

          ! rate of state
          SELECT CASE(evolutionLawType)
          CASE(1)
             ! isothermal, isobaric aging law
             dydt(l+STATE_VECTOR_STATE_1)=(EXP(-y(l+STATE_VECTOR_STATE_1)*lg10)-velocity/patch%L)/lg10
          CASE(2)
             ! isothermal, isobaric slip law
             dydt(l+STATE_VECTOR_STATE_1)=(-velocity/patch%L*(LOG(velocity/patch%L)+y(l+STATE_VECTOR_STATE_1)*lg10))
          CASE DEFAULT
             WRITE (0,'("unhandled option ", a, " (this is a bug")') evolutionLawType
             WRITE_DEBUG_INFO(100)
             STOP 3
          END SELECT

          ! scalar rate of shear traction
          dtau=t(j+TRACTION_VECTOR_STRIKE)*COS(rake) &
              +t(j+TRACTION_VECTOR_DIP)   *SIN(rake)
          
          ! normal stress
          !sigma=patch%sig-y(l+STATE_VECTOR_TRACTION_NORMAL)
          sigma=patch%sig

          SELECT CASE(frictionLawType)
          CASE(1)
             ! multiplicative form of rate-state friction (Barbot, 2019)
             friction=patch%mu0*EXP(patch%a/patch%mu0*LOG(velocity/patch%Vo) &
                                   +patch%b/patch%mu0*(y(l+STATE_VECTOR_STATE_1)*lg10+LOG(patch%Vo/patch%L)))

             ! acceleration (1/V dV/dt) / log(10)
             !                               +friction*t(j+TRACTION_VECTOR_NORMAL)) / &
             dydt(l+STATE_VECTOR_VELOCITY)= &
                  (dtau-patch%b*sigma*friction/patch%mu0*dydt(l+STATE_VECTOR_STATE_1)*lg10 &
                                                                                  ) / &
                  (patch%a*sigma*friction/patch%mu0+patch%damping*velocity) / lg10
          CASE(2)
             ! additive form of rate-state friction (Ruina, 1983)
             friction=patch%mu0+patch%a*LOG(velocity/patch%Vo) &
                               +patch%b*LOG(EXP(y(l+STATE_VECTOR_STATE_1)*lg10)/patch%L*patch%Vo)

             ! acceleration (1/V dV/dt) / log(10)
             dydt(l+STATE_VECTOR_VELOCITY)= &
                  (dtau-patch%b*sigma*dydt(l+STATE_VECTOR_STATE_1)*lg10 &
                                             +friction*t(j+TRACTION_VECTOR_NORMAL)) / &
                  (patch%a*sigma+patch%damping*velocity) / lg10
          CASE(3)
             ! arcsinh form of rate-state friction (Rice & Benzion, 1996)
             reg=2.0d0*patch%Vo/velocity*EXP(-(patch%mu0+patch%b*(y(STATE_VECTOR_STATE_1)*lg10-LOG(patch%L/patch%Vo)))/patch%a)

             friction=patch%a*ASINH(1._8/reg)

             reg=1d0/SQRT(1._8+reg**2)

             ! acceleration
             dydt(l+STATE_VECTOR_VELOCITY)= &
                     (dtau-patch%b*sigma*dydt(l+STATE_VECTOR_STATE_1)*lg10*reg &
                                             +friction*t(j+TRACTION_VECTOR_NORMAL)) / &
                     (patch%a*sigma*reg+patch%damping*velocity) / lg10
          CASE(4)
             ! cut-off velocity form of rate-state friction (Okubo, 1989)
             friction=patch%mu0-patch%a*LOG(1._8+patch%Vo/velocity) &
                               +patch%b*LOG(1._8+exp(y(l+STATE_VECTOR_STATE_1)*lg10)/patch%L*patch%V2)

             ! state variable
             theta=EXP(y(l+STATE_VECTOR_STATE_1)*lg10)

             ! acceleration
             dydt(l+STATE_VECTOR_VELOCITY)= &
                  (dtau-patch%b*sigma*dydt(l+STATE_VECTOR_STATE_1)*lg10/(1._8+patch%L/patch%V2/theta) &
                                             +friction*t(j+TRACTION_VECTOR_NORMAL)) / &
                  (patch%a*sigma/(1._8+velocity/patch%Vo)+patch%damping*velocity) / lg10
          CASE DEFAULT
             WRITE (0,'("unhandled option ", a, " (this is a bug")') frictionLawType
             WRITE_DEBUG_INFO(100)
             STOP 3
          END SELECT

          ! correction
          correction=patch%damping*velocity*dydt(l+STATE_VECTOR_VELOCITY)*lg10

          ! traction rate
          dydt(l+STATE_VECTOR_TRACTION_STRIKE)=t(j+TRACTION_VECTOR_STRIKE)-correction*COS(rake)
          dydt(l+STATE_VECTOR_TRACTION_DIP   )=t(j+TRACTION_VECTOR_DIP   )-correction*SIN(rake)
          dydt(l+STATE_VECTOR_TRACTION_NORMAL)=t(j+TRACTION_VECTOR_NORMAL)

          l=l+in%dPatch

       CASE (FLAG_CUBOID,FLAG_TETRAHEDRON)

          ! return the stress rate
          dydt(l+STATE_VECTOR_S11)=t(j+TRACTION_VECTOR_S11)
          dydt(l+STATE_VECTOR_S12)=t(j+TRACTION_VECTOR_S12)
          dydt(l+STATE_VECTOR_S13)=t(j+TRACTION_VECTOR_S13)
          dydt(l+STATE_VECTOR_S22)=t(j+TRACTION_VECTOR_S22)
          dydt(l+STATE_VECTOR_S23)=t(j+TRACTION_VECTOR_S23)
          dydt(l+STATE_VECTOR_S33)=t(j+TRACTION_VECTOR_S33)

          l=l+in%dVolume

       CASE DEFAULT
          WRITE(STDERR,'("wrong case: this is a bug.")')
          WRITE_DEBUG_INFO(-1)
          STOP 2
       END SELECT

       j=j+layout%elementForceDGF(i)
    END DO

  END SUBROUTINE odefun

  !-----------------------------------------------------------------------
  !> subroutine initParallelism()
  !! initialize variables describe the data layout
  !! for parallelism.
  !!
  !! OUTPUT:
  !! layout    - list of receiver type and type index
  !-----------------------------------------------------------------------
  SUBROUTINE initParallelism(in,layout)
    IMPLICIT NONE
    TYPE(SIMULATION_STRUCT), INTENT(IN) :: in
    TYPE(LAYOUT_STRUCT), INTENT(OUT) :: layout

    INTEGER :: i,j,k,ierr,remainder,cumulativeIndex
    INTEGER :: rank,csize
    INTEGER :: nElements

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,csize,ierr)

    ! total number of elements
    nElements=in%rectangle%ns+in%triangle%ns+in%cuboid%ns+in%tetrahedron%ns

    ! list of number of elements in thread
    ALLOCATE(layout%listElements(csize), &
             layout%listOffset(csize),STAT=ierr)
    IF (ierr>0) STOP "could not allocate the list"

    remainder=nElements-INT(nElements/csize)*csize
    IF (0 .LT. remainder) THEN
       layout%listElements(1:(csize-remainder))      =INT(nElements/csize)
       layout%listElements((csize-remainder+1):csize)=INT(nElements/csize)+1
    ELSE
       layout%listElements(1:csize)=INT(nElements/csize)
    END IF

    ! element start index in thread
    j=0
    k=0
    DO i=1,csize
       j=k+1
       k=k+layout%listElements(i)
       layout%listOffset(i)=j
    END DO

    ALLOCATE(layout%elementType       (layout%listElements(1+rank)), &
             layout%elementIndex      (layout%listElements(1+rank)), &
             layout%elementStateIndex (layout%listElements(1+rank)), &
             layout%elementVelocityDGF(layout%listElements(1+rank)), &
             layout%elementStateDGF   (layout%listElements(1+rank)), &
             layout%elementForceDGF   (layout%listElements(1+rank)),STAT=ierr)
    IF (ierr>0) STOP "could not allocate the layout elements"

    j=1
    cumulativeIndex=0
    DO i=1,in%rectangle%ns
       IF ((i .GE. layout%listOffset(1+rank)) .AND. &
           (i .LT. (layout%listOffset(1+rank)+layout%listElements(1+rank)))) THEN
          layout%elementType(j)=FLAG_RECTANGLE
          layout%elementIndex(j)=i
          layout%elementStateIndex(j)=cumulativeIndex+STATE_VECTOR_DGF_PATCH
          cumulativeIndex=layout%elementStateIndex(j)
          layout%elementVelocityDGF(j)=DGF_PATCH
          layout%elementStateDGF(j)=in%dPatch
          layout%elementForceDGF(j)=DGF_VECTOR
          j=j+1
       END IF
    END DO
    DO i=1,in%triangle%ns
       IF (((i+in%rectangle%ns) .GE. layout%listOffset(1+rank)) .AND. &
           ((i+in%rectangle%ns) .LT. (layout%listOffset(1+rank)+layout%listElements(1+rank)))) THEN
          layout%elementType(j)=FLAG_TRIANGLE
          layout%elementIndex(j)=i
          layout%elementStateIndex(j)=cumulativeIndex+STATE_VECTOR_DGF_PATCH
          cumulativeIndex=layout%elementStateIndex(j)
          layout%elementVelocityDGF(j)=DGF_PATCH
          layout%elementStateDGF(j)=in%dPatch
          layout%elementForceDGF(j)=DGF_VECTOR
          j=j+1
       END IF
    END DO
    DO i=1,in%cuboid%ns
       IF (((i+in%rectangle%ns+in%triangle%ns) .GE. layout%listOffset(1+rank)) .AND. &
           ((i+in%rectangle%ns+in%triangle%ns) .LT. (layout%listOffset(1+rank)+layout%listElements(1+rank)))) THEN
          layout%elementType(j)=FLAG_CUBOID
          layout%elementIndex(j)=i
          layout%elementStateIndex(j)=cumulativeIndex+STATE_VECTOR_DGF_VOLUME
          cumulativeIndex=layout%elementStateIndex(j)
          layout%elementVelocityDGF(j)=DGF_VOLUME
          layout%elementStateDGF(j)=in%dVolume
          layout%elementForceDGF(j)=DGF_TENSOR
          j=j+1
       END IF
    END DO
    DO i=1,in%tetrahedron%ns
       IF (((i+in%rectangle%ns+in%triangle%ns+in%cuboid%ns) .GE. layout%listOffset(1+rank)) .AND. &
           ((i+in%rectangle%ns+in%triangle%ns+in%cuboid%ns) .LT. (layout%listOffset(1+rank)+layout%listElements(1+rank)))) THEN
          layout%elementType(j)=FLAG_TETRAHEDRON
          layout%elementIndex(j)=i
          layout%elementStateIndex(j)=cumulativeIndex+STATE_VECTOR_DGF_VOLUME
          cumulativeIndex=layout%elementStateIndex(j)
          layout%elementVelocityDGF(j)=DGF_VOLUME
          layout%elementStateDGF(j)=in%dVolume
          layout%elementForceDGF(j)=DGF_TENSOR
          j=j+1
       END IF
    END DO

    ALLOCATE(layout%listVelocityN(csize), &
             layout%listVelocityOffset(csize), &
             layout%listStateN(csize), &
             layout%listStateOffset(csize), &
             layout%listForceN(csize), &
             STAT=ierr)
    IF (ierr>0) STOP "could not allocate the size list"

    ! share number of elements in threads
    CALL MPI_ALLGATHER(SUM(layout%elementVelocityDGF),1,MPI_INTEGER,layout%listVelocityN,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
    CALL MPI_ALLGATHER(SUM(layout%elementStateDGF),   1,MPI_INTEGER,layout%listStateN,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
    CALL MPI_ALLGATHER(SUM(layout%elementForceDGF),   1,MPI_INTEGER,layout%listForceN,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)

    j=0
    k=0
    DO i=1,csize
       j=k+1
       k=k+layout%listVelocityN(i)
       layout%listVelocityOffset(i)=j-1
    END DO

    j=0
    k=0
    DO i=1,csize
       j=k+1
       k=k+layout%listStateN(i)
       layout%listStateOffset(i)=j-1
    END DO

  END SUBROUTINE initParallelism

  !-----------------------------------------------------------------------
  !> subroutine initGeometry
  ! initializes the position and local reference system vectors
  !
  ! INPUT:
  ! @param in      - input parameters data structure
  !-----------------------------------------------------------------------
  SUBROUTINE initGeometry(in)
    USE cuboid
    USE tetrahedron
    USE stuart97
    USE okada92
    USE types_3d
    TYPE(SIMULATION_STRUCT), INTENT(INOUT) :: in

    IF (0 .LT. in%rectangle%ns) THEN
       CALL computeReferenceSystemOkada92( &
                in%rectangle%ns, &
                in%rectangle%x, &
                in%rectangle%length, &
                in%rectangle%width, &
                in%rectangle%strike, &
                in%rectangle%dip, &
                in%rectangle%sv, &
                in%rectangle%dv, &
                in%rectangle%nv, &
                in%rectangle%xc)
    END IF

    IF (0 .LT. in%triangle%ns) THEN
       CALL computeReferenceSystemStuart97( &
                in%triangle%nVe, &
                in%triangle%v, &
                in%triangle%ns, &
                in%triangle%i1, &
                in%triangle%i2, &
                in%triangle%i3, &
                in%triangle%sv, &
                in%triangle%dv, &
                in%triangle%nv, &
                in%triangle%xc)
    END IF

    IF (0 .LT. in%cuboid%ns) THEN
       CALL computeReferenceSystemVerticalCuboid( &
                in%cuboid%ns, &
                in%cuboid%x, &
                in%cuboid%length, &
                in%cuboid%width, &
                in%cuboid%strike, &
                in%cuboid%sv, &
                in%cuboid%dv, &
                in%cuboid%nv, &
                in%cuboid%xc)
    END IF

    IF (0 .LT. in%tetrahedron%ns) THEN
       CALL computeReferenceSystemTetrahedron( &
                in%tetrahedron%ns, &
                in%tetrahedron%i1, &
                in%tetrahedron%i2, &
                in%tetrahedron%i3, &
                in%tetrahedron%i4, &
                in%tetrahedron%nVe, &
                in%tetrahedron%v, &
                in%tetrahedron%sv, &
                in%tetrahedron%dv, &
                in%tetrahedron%nv, &
                in%tetrahedron%xc)
    END IF

  END SUBROUTINE initGeometry

  !---------------------------------------------------------------------
  !> subroutine init
  !! reads simulation parameters from the standard input and initialize
  !! model parameters.
  !!
  !! INPUT:
  !! @param unit - the unit number used to read input data
  !!
  !! OUTPUT:
  !! @param in
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !---------------------------------------------------------------------
  SUBROUTINE init(in)
    USE types_3d
    USE getopt_m

    TYPE(SIMULATION_STRUCT), INTENT(OUT) :: in

    CHARACTER :: ch
    CHARACTER(512) :: dataline
    CHARACTER(256) :: filename
    INTEGER :: iunit,noptions
!$  INTEGER :: omp_get_num_procs,omp_get_max_threads
    TYPE(OPTION_S) :: opts(16)
    REAL*8, DIMENSION(3) :: normal

    INTEGER :: i,j,k,ierr,rank,size,position
    INTEGER :: maxVertexIndex=-1
    INTEGER, PARAMETER :: psize=1024
    INTEGER :: dummy
    CHARACTER, DIMENSION(psize) :: packed

    INTEGER :: nObservationPatch,nObservationVolume
    INTEGER, DIMENSION(:,:), ALLOCATABLE :: observationPatch,observationVolume
  
    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,size,ierr)

    ! define long options, such as --dry-run
    ! parse the command line for options
    opts( 1)=OPTION_S("version",.FALSE.,CHAR(21))
    opts( 2)=OPTION_S("dry-run",.FALSE.,CHAR(22))
    opts( 3)=OPTION_S("epsilon",.TRUE.,'e')
    opts( 4)=OPTION_S("export-greens",.TRUE.,'g')
    opts( 5)=OPTION_S("export-netcdf",.FALSE.,'n')
    opts( 6)=OPTION_S("export-vtp",.FALSE.,'v')
    opts( 7)=OPTION_S("export-xyz",.FALSE.,'x')
    opts( 8)=OPTION_S("export-slip",.FALSE.,'l')
    opts( 9)=OPTION_S("export-stress",.FALSE.,'t')
    opts(10)=OPTION_S("export-slip-distribution",.TRUE.,'s')
    opts(11)=OPTION_S("friction-law",.TRUE.,'f')
    opts(12)=OPTION_S("evolution-law",.TRUE.,'j')
    opts(13)=OPTION_S("import-greens",.TRUE.,'p')
    opts(14)=OPTION_S("maximum-step",.TRUE.,'m')
    opts(15)=OPTION_S("maximum-iterations",.TRUE.,'i')
    opts(16)=OPTION_S("help",.FALSE.,'h')

    noptions=0;
    DO
       ch=getopt("he:f:g:i:j:lm:np:s:tv:",opts)
       SELECT CASE(ch)
       CASE(CHAR(0))
          EXIT
       CASE(CHAR(21))
          ! option version
          in%isversion=.TRUE.
       CASE(CHAR(22))
          ! option dry-run
          in%isdryrun=.TRUE.
       CASE('e')
          ! numerical accuracy (variable epsilon sits in the ode45 module)
          READ(optarg,*) epsilon
          noptions=noptions+1
       CASE('f')
          ! type of friction law
          READ(optarg,*) frictionLawType
          noptions=noptions+1
       CASE('g')
          ! export Greens functions to netcdf file
          READ(optarg,'(a)') in%greensFunctionDirectory
          in%isExportGreens=.TRUE.
          noptions=noptions+1
       CASE('i')
          ! maximum number of iterations
          READ(optarg,*) maximumIterations
          noptions=noptions+1
       CASE('j')
          ! type of evolution law
          READ(optarg,*) evolutionLawType
          noptions=noptions+1
       CASE('l')
          ! export slip in netcdf format
          in%isExportSlip=.TRUE.
       CASE('m')
          ! maximum time step (variable maximumTimeStep sits in the ode45 module)
          READ(optarg,*) maximumTimeStep
          noptions=noptions+1
       CASE('n')
          ! export in netcdf format
          in%isExportNetcdf=.TRUE.
       CASE('v')
          ! export in Paraview-compatible .vtp format
          in%isExportVTP=.TRUE.
       CASE('x')
          ! export in GMT-compatible ASCII .xyz format
          in%isExportXYZ=.TRUE.
       CASE('p')
          ! import Greens functions from netcdf file
          READ(optarg,'(a)') in%greensFunctionDirectory
          in%isImportGreens=.TRUE.
          noptions=noptions+1
       CASE('s')
          ! export slip distribution
          READ(optarg,*) exportSlipDistributionRate
          noptions=noptions+1
       CASE('t')
          ! export stress in netcdf format
          in%isExportStress=.TRUE.
       CASE('h')
          ! option help
          in%ishelp=.TRUE.
       CASE('?')
          WRITE_DEBUG_INFO(100)
          in%ishelp=.TRUE.
          EXIT
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug")') optopt
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT
       noptions=noptions+1
    END DO

    IF (in%isversion) THEN
       CALL printversion()
       CALL MPI_FINALIZE(ierr)
       ! abort parameter input
       STOP
    END IF

    IF (in%ishelp) THEN
       CALL printhelp()
       CALL MPI_FINALIZE(ierr)
       ! abort parameter input
       STOP
    END IF

    in%nPatch=0
    ! minimum number of dynamic variables for boundary elements
    in%dPatch=STATE_VECTOR_DGF_PATCH
    in%nVolume=0
    ! minimum number of dynamic variables for volume elements
    in%dVolume=STATE_VECTOR_DGF_VOLUME
    ! number of rectangle surface elements
    in%rectangle%ns=0
    ! number of triangle surface elements
    in%triangle%ns=0
    ! number of triangle vertices
    in%triangle%nVe=0
    ! number of cuboid volume elements
    in%cuboid%ns=0
    ! number of tetrahedron volume elements
    in%tetrahedron%ns=0
    ! number of tetrahedron vertices
    in%tetrahedron%nVe=0

    IF (0 .EQ. rank) THEN
       PRINT 2000
       PRINT '("# VISCOUSCYCLES")'
       PRINT '("# quasi-dynamic earthquake simulation in three-dimensional viscoelastic media")'
       PRINT '("# with the integral method.")'
       SELECT CASE(frictionLawType)
       CASE(1)
          PRINT '("# friction law: multiplicative form of rate-state friction (Barbot, 2019)")'
       CASE(2)
          PRINT '("# friction law: additive form of rate-state friction (Ruina, 1983)")'
       CASE(3)
          PRINT '("# friction law: arcsinh form of rate-state friction (Rice & BenZion, 1996)")'
       CASE(4)
          PRINT '("# friction law: rate-state with cut-off velocity (Okubo, 1989)")'
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug")') frictionLawType
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT
       SELECT CASE(evolutionLawType)
       CASE(1)
          PRINT '("# evolution law: isothermal, isobaric additive form (Ruina, 1983)")'
       CASE(2)
          PRINT '("# evolution law: isothermal, isobaric multiplicative form (Ruina, 1983)")'
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug")') evolutionLawType
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT
       PRINT '("# numerical accuracy: ",ES11.4)', epsilon
       PRINT '("# maximum iterations: ",I11)', maximumIterations
       PRINT '("# maximum time step: ",ES12.4)', maximumTimeStep
       PRINT '("# number of threads: ",I12)', csize
       IF (in%isExportNetcdf) THEN
          PRINT '("# export velocity to netcdf:      yes")'
       ELSE
          PRINT '("# export velocity to netcdf:       no")'
       END IF
       IF (in%isExportVTP) THEN
          PRINT '("# export to VTP format:           yes")'
       ELSE
          PRINT '("# export to VTP format:            no")'
       END IF
       IF (in%isExportXYZ) THEN
          PRINT '("# export to ASCII XYZ format:     yes")'
       ELSE
          PRINT '("# export to ASCII XYZ format:      no")'
       END IF
#ifdef NETCDF
       IF (in%isImportGreens) THEN
          WRITE (STDOUT,'("# import greens function:         yes (",a,")")') TRIM(in%greensFunctionDirectory)
       ELSE IF (in%isExportGreens) THEN
          WRITE (STDOUT,'("# export greens function:         yes (",a,")")') TRIM(in%greensFunctionDirectory)
       END IF
#endif
!$     PRINT '("#     * parallel OpenMP implementation with ",I3.3,"/",I3.3," threads")', &
!$                  omp_get_max_threads(),omp_get_num_procs()
       PRINT 2000

       IF (noptions .LT. COMMAND_ARGUMENT_COUNT()) THEN
          ! read from input file
          iunit=25
          CALL GET_COMMAND_ARGUMENT(noptions+1,filename)
          OPEN (UNIT=iunit,FILE=filename,IOSTAT=ierr)
       ELSE
          ! get input parameters from standard input
          iunit=5
       END IF

       PRINT '("# output directory")'
       CALL getdata(iunit,dataline)
       READ (dataline,'(a)') in%wdir
       PRINT '(2X,a)', TRIM(in%wdir)

       ! test write permissions on output directory
       in%timeFilename=TRIM(in%wdir)//"/time.dat"
       OPEN (UNIT=FPTIME,FILE=in%timeFilename,POSITION="APPEND",&
               IOSTAT=ierr,FORM="FORMATTED")
       IF (ierr>0) THEN
          WRITE_DEBUG_INFO(102)
          WRITE (STDERR,'("error: unable to access ",a)') TRIM(in%timefilename)
          STOP 1
       END IF
       CLOSE(FPTIME)
   
       PRINT '("# Lame parameter, rigidity (lambda, mu)")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%lambda,in%mu
       PRINT '(2ES9.2E1)', in%lambda,in%mu

       IF (0 .GT. in%mu) THEN
          WRITE_DEBUG_INFO(-1)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("input error: shear modulus must be positive")')
          STOP 2
       END IF

       in%nu=in%lambda/2._8/(in%lambda+in%mu)
       IF (-1._8 .GT. in%nu) THEN
          WRITE_DEBUG_INFO(-1)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("input error: Poisson''s ratio must be greater than -1.")')
          STOP 2
       END IF
       IF (0.5_8 .LT. in%nu) THEN
          WRITE_DEBUG_INFO(-1)
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("input error: Poisson''s ratio must be lower than 0.5.")')
          STOP 2
       END IF

       PRINT '("# time interval")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%interval
       PRINT '(ES20.12E2)', in%interval

       IF (in%interval .LE. 0._8) THEN
          WRITE (STDERR,'("**** error **** ")')
          WRITE (STDERR,'(a)') TRIM(dataline)
          WRITE (STDERR,'("simulation time must be positive. exiting.")')
          STOP 1
       END IF

       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !       R E C T A N G L E   P A T C H E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT '("# number of rectangle patches")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%rectangle%ns
       PRINT '(I5)', in%rectangle%ns
       IF (in%rectangle%ns .GT. 0) THEN
          ALLOCATE(in%rectangle%s(in%rectangle%ns), &
                   in%rectangle%x(3,in%rectangle%ns), &
                   in%rectangle%xc(3,in%rectangle%ns), &
                   in%rectangle%length(in%rectangle%ns), &
                   in%rectangle%width(in%rectangle%ns), &
                   in%rectangle%strike(in%rectangle%ns), &
                   in%rectangle%dip(in%rectangle%ns), &
                   in%rectangle%sv(3,in%rectangle%ns), &
                   in%rectangle%dv(3,in%rectangle%ns), &
                   in%rectangle%nv(3,in%rectangle%ns),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the rectangle patch list"
          PRINT 2000
          PRINT '("#    n        Vl       x1       x2       x3  length   width strike   dip   rake")'
          PRINT 2000
          DO k=1,in%rectangle%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                  in%rectangle%s(k)%Vl, &
                  in%rectangle%x(1,k), &
                  in%rectangle%x(2,k), &
                  in%rectangle%x(3,k), &
                  in%rectangle%length(k), &
                  in%rectangle%width(k), &
                  in%rectangle%strike(k), &
                  in%rectangle%dip(k), &
                  in%rectangle%s(k)%rake
             in%rectangle%s(k)%opening=0
   
             PRINT '(I6,ES10.2E2,3ES9.2E1,2ES8.2E1,f7.1,f6.1,f7.1)',i, &
                  in%rectangle%s(k)%Vl, &
                  in%rectangle%x(1,k), &
                  in%rectangle%x(2,k), &
                  in%rectangle%x(3,k), &
                  in%rectangle%length(k), &
                  in%rectangle%width(k), &
                  in%rectangle%strike(k), &
                  in%rectangle%dip(k), &
                  in%rectangle%s(k)%rake

             ! convert to radians
             in%rectangle%strike(k)=in%rectangle%strike(k)*DEG2RAD     
             in%rectangle%dip(k)=in%rectangle%dip(k)*DEG2RAD     
             in%rectangle%s(k)%rake=in%rectangle%s(k)%rake*DEG2RAD     

             IF (i .NE. k) THEN
                WRITE (STDERR,'("invalid rectangle patch definition")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
             IF (MAX(in%rectangle%length(k),in%rectangle%width(k)) .LE. 0._8) THEN
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: patch length and width must be positive.")')
                STOP 1
             END IF
                
          END DO
   
          ! export rectangle patches
          filename=TRIM(in%wdir)//"/rfaults.flt"
          OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          WRITE (FPOUT,'("#    n            Vl            x1            x2            x3   length    width strike   dip   rake")')
          DO k=1,in%rectangle%ns
             WRITE (FPOUT,'(I6,4ES14.5E3,2ES9.3E1,f7.1,f6.1,f7.1)') k, &
                  in%rectangle%s(k)%Vl, &
                  in%rectangle%x(1,k), &
                  in%rectangle%x(2,k), &
                  in%rectangle%x(3,k), &
                  in%rectangle%length(k), &
                  in%rectangle%width(k), &
                  in%rectangle%strike(k)/DEG2RAD, &
                  in%rectangle%dip(k)/DEG2RAD, &
                  in%rectangle%s(k)%rake/DEG2RAD
          END DO
          CLOSE(FPOUT)
                
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          !        F R I C T I O N   P R O P E R T I E S
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          PRINT 2000
          PRINT '("# number of frictional rectangle patches")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) dummy
          PRINT '(I5)', dummy
          IF (dummy .NE. in%rectangle%ns) THEN
             WRITE_DEBUG_INFO(-1)
             WRITE(STDERR,'("input error: all rectangle patches require frictional properties")')
             STOP 2
          END IF

          SELECT CASE(frictionLawType)
          CASE(1:3)
             PRINT '("#    n     tau0      mu0      sig        a        b        L       Vo  G/(2Vs)")'
             PRINT 2000
             DO k=1,in%rectangle%ns
                CALL getdata(iunit,dataline)
                READ (dataline,*,IOSTAT=ierr) i, &
                      in%rectangle%s(k)%tau0, &
                      in%rectangle%s(k)%mu0, &
                      in%rectangle%s(k)%sig, &
                      in%rectangle%s(k)%a, &
                      in%rectangle%s(k)%b, &
                      in%rectangle%s(k)%L, &
                      in%rectangle%s(k)%Vo, &
                      in%rectangle%s(k)%damping
   
                PRINT '(I6,8ES9.2E1)',i, &
                     in%rectangle%s(k)%tau0, &
                     in%rectangle%s(k)%mu0, &
                     in%rectangle%s(k)%sig, &
                     in%rectangle%s(k)%a, &
                     in%rectangle%s(k)%b, &
                     in%rectangle%s(k)%L, &
                     in%rectangle%s(k)%Vo, &
                     in%rectangle%s(k)%damping
                
                IF (i .NE. k) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("invalid friction property definition for rectangle patch")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   WRITE (STDERR,'("error in input file: unexpected index")')
                   STOP 1
                END IF
             END DO
   
          CASE(4)
             PRINT '("#    n     tau0      mu0      sig        a        b        L       V1       V2  G/(2Vs)")'
             PRINT 2000
             DO k=1,in%rectangle%ns
                CALL getdata(iunit,dataline)
                READ (dataline,*,IOSTAT=ierr) i, &
                      in%rectangle%s(k)%tau0, &
                      in%rectangle%s(k)%mu0, &
                      in%rectangle%s(k)%sig, &
                      in%rectangle%s(k)%a, &
                      in%rectangle%s(k)%b, &
                      in%rectangle%s(k)%L, &
                      in%rectangle%s(k)%Vo, &
                      in%rectangle%s(k)%V2, &
                      in%rectangle%s(k)%damping
   
                PRINT '(I6,9ES9.2E1)',i, &
                     in%rectangle%s(k)%tau0, &
                     in%rectangle%s(k)%mu0, &
                     in%rectangle%s(k)%sig, &
                     in%rectangle%s(k)%a, &
                     in%rectangle%s(k)%b, &
                     in%rectangle%s(k)%L, &
                     in%rectangle%s(k)%Vo, &
                     in%rectangle%s(k)%V2, &
                     in%rectangle%s(k)%damping
                
                IF (i .NE. k) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("invalid friction property definition for rectangle patch")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   WRITE (STDERR,'("error in input file: unexpected index")')
                   STOP 1
                END IF
             END DO
   
          CASE DEFAULT
             WRITE (0,'("unhandled option ", a, " (this is a bug")') frictionLawType
             WRITE_DEBUG_INFO(100)
             STOP 3
          END SELECT
   
       END IF
          
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !        T R I A N G L E   P A T C H E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT '("# number of triangle patches")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%triangle%ns
       PRINT '(I5)', in%triangle%ns
       IF (in%triangle%ns .GT. 0) THEN
          ALLOCATE(in%triangle%s(in%triangle%ns), &
                   in%triangle%i1(in%triangle%ns), &
                   in%triangle%i2(in%triangle%ns), &
                   in%triangle%i3(in%triangle%ns), &
                   in%triangle%area(in%triangle%ns), &
                   in%triangle%sv(3,in%triangle%ns), &
                   in%triangle%dv(3,in%triangle%ns), &
                   in%triangle%nv(3,in%triangle%ns), &
                   in%triangle%xc(3,in%triangle%ns),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the triangle patch list"
          PRINT 2000
          PRINT '("#    n       Vl        i1        i2        i3   rake")'
          PRINT 2000
          DO k=1,in%triangle%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                  in%triangle%s(k)%Vl, &
                  in%triangle%i1(k), &
                  in%triangle%i2(k), &
                  in%triangle%i3(k), &
                  in%triangle%s(k)%rake
             in%triangle%s(k)%opening=0
   
             PRINT '(I6,ES9.2E1,3I10,f7.1)',i, &
                  in%triangle%s(k)%Vl, &
                  in%triangle%i1(k), &
                  in%triangle%i2(k), &
                  in%triangle%i3(k), &
                  in%triangle%s(k)%rake
                
             ! convert to radians
             in%triangle%s(k)%rake=in%triangle%s(k)%rake*DEG2RAD     

             ! check range of vertex index
             IF (in%triangle%i1(k) .GT. maxVertexIndex) THEN
                maxVertexIndex=in%triangle%i1(k)
             END IF
             IF (in%triangle%i2(k) .GT. maxVertexIndex) THEN
                maxVertexIndex=in%triangle%i2(k)
             END IF
             IF (in%triangle%i3(k) .GT. maxVertexIndex) THEN
                maxVertexIndex=in%triangle%i3(k)
             END IF
             IF ((0 .GT. in%triangle%i1(k)) .OR. &
                 (0 .GT. in%triangle%i2(k)) .OR. &
                 (0 .GT. in%triangle%i3(k))) THEN
                WRITE (STDERR,'("error in input file: negative index")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                STOP 1
             END IF

             IF (i .NE. k) THEN
                WRITE (STDERR,'("error in input file: unexpected index")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("invalid triangle patch definition ")')
                STOP 1
             END IF
          END DO
                
          PRINT '("# number of triangle vertices")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) in%triangle%nVe
          PRINT '(I5)', in%triangle%nVe
          IF (maxVertexIndex .GT. in%triangle%nVe) THEN
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE (STDERR,'("error in input file: not enough vertices")')
             STOP 1
          END IF
          IF (in%triangle%nVe .GT. 0) THEN
             ALLOCATE(in%triangle%v(3,in%triangle%nVe),STAT=ierr)
             IF (ierr>0) STOP "could not allocate the list of vertices"
             PRINT 2000
             PRINT '("#    n      x1       x2       x3")'
             PRINT 2000
             DO k=1,in%triangle%nVe
                CALL getdata(iunit,dataline)
                READ (dataline,*,IOSTAT=ierr) i, &
                      in%triangle%v(1,k), &
                      in%triangle%v(2,k), &
                      in%triangle%v(3,k)
   
                PRINT '(I6,3ES9.2E1)',i, &
                     in%triangle%v(1,k), &
                     in%triangle%v(2,k), &
                     in%triangle%v(3,k)
                
                IF (i .NE. k) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("invalid vertex definition ")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   WRITE (STDERR,'("error in input file: unexpected index")')
                   STOP 1
                END IF
             END DO

             ! area of triangle elements
             DO k=1,in%triangle%ns
                ! area
                CALL cross( &
                       in%triangle%v(:,in%triangle%i1(k))-in%triangle%v(:,in%triangle%i2(k)), &
                       in%triangle%v(:,in%triangle%i1(k))-in%triangle%v(:,in%triangle%i3(k)), &
                       normal)
                in%triangle%area(k)=NORM2(normal)/2
             END DO

             ! export the triangle patches
             filename=TRIM(in%wdir)//"/tfaults.tri"
             OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
             IF (ierr>0) THEN
                WRITE_DEBUG_INFO(102)
                WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
                STOP 1
             END IF
             WRITE (FPOUT,'("#    n       Vl        i1        i2        i3   rake")')
             DO k=1,in%triangle%ns
                WRITE (FPOUT,'(I6,ES9.2E1,3I10,f7.1)') k, &
                  in%triangle%s(k)%Vl, &
                  in%triangle%i1(k), &
                  in%triangle%i2(k), &
                  in%triangle%i3(k), &
                  in%triangle%s(k)%rake
             END DO
             CLOSE(FPOUT)
                
             filename=TRIM(in%wdir)//"/tfaults.ned"
             OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
             IF (ierr>0) THEN
                WRITE_DEBUG_INFO(102)
                WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
                STOP 1
             END IF
             WRITE (FPOUT,'("#    n      x1       x2       x3")')
             DO k=1,in%triangle%ns
                WRITE (FPOUT,'(I3.3,3ES9.2E1)') k, &
                     in%triangle%v(1,k), &
                     in%triangle%v(2,k), &
                     in%triangle%v(3,k)
             END DO
             CLOSE(FPOUT)
                
          END IF
   
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          !        F R I C T I O N   P R O P E R T I E S
          ! - - - - - - - - - - - - - - - - - - - - - - - - - -
          PRINT 2000
          PRINT '("# number of frictional triangle patches")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) dummy
          PRINT '(I5)', dummy
          IF (dummy .NE. in%triangle%ns) THEN
             WRITE_DEBUG_INFO(-1)
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE(STDERR,'("input error: all triangle patches require frictional properties")')
             STOP 2
          END IF

          SELECT CASE(frictionLawType)
          CASE(1:3)
             PRINT '("#    n     tau0      mu0      sig        a        b        L       Vo    2G/Vs")'
             PRINT 2000
             DO k=1,in%triangle%ns
                CALL getdata(iunit,dataline)
                READ (dataline,*,IOSTAT=ierr) i, &
                      in%triangle%s(k)%tau0, &
                      in%triangle%s(k)%mu0, &
                      in%triangle%s(k)%sig, &
                      in%triangle%s(k)%a, &
                      in%triangle%s(k)%b, &
                      in%triangle%s(k)%L, &
                      in%triangle%s(k)%Vo, &
                      in%triangle%s(k)%damping
   
                PRINT '(I6,8ES9.2E1)',i, &
                     in%triangle%s(k)%tau0, &
                     in%triangle%s(k)%mu0, &
                     in%triangle%s(k)%sig, &
                     in%triangle%s(k)%a, &
                     in%triangle%s(k)%b, &
                     in%triangle%s(k)%L, &
                     in%triangle%s(k)%Vo, &
                     in%triangle%s(k)%damping
                
                IF (i .NE. k) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("invalid friction property definition for triangle patch")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   WRITE (STDERR,'("error in input file: unexpected index")')
                   STOP 1
                END IF
             END DO

          CASE(4)
             PRINT '("#    n     tau0      mu0      sig        a        b        L       V1       V2    2G/Vs")'
             PRINT 2000
             DO k=1,in%triangle%ns
                CALL getdata(iunit,dataline)
                READ (dataline,*,IOSTAT=ierr) i, &
                      in%triangle%s(k)%tau0, &
                      in%triangle%s(k)%mu0, &
                      in%triangle%s(k)%sig, &
                      in%triangle%s(k)%a, &
                      in%triangle%s(k)%b, &
                      in%triangle%s(k)%L, &
                      in%triangle%s(k)%Vo, &
                      in%triangle%s(k)%V2, &
                      in%triangle%s(k)%damping
   
                PRINT '(I6,9ES9.2E1)',i, &
                     in%triangle%s(k)%tau0, &
                     in%triangle%s(k)%mu0, &
                     in%triangle%s(k)%sig, &
                     in%triangle%s(k)%a, &
                     in%triangle%s(k)%b, &
                     in%triangle%s(k)%L, &
                     in%triangle%s(k)%Vo, &
                     in%triangle%s(k)%V2, &
                     in%triangle%s(k)%damping
                
                IF (i .NE. k) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("invalid friction property definition for triangle patch")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   WRITE (STDERR,'("error in input file: unexpected index")')
                   STOP 1
                END IF
             END DO

          CASE DEFAULT
             WRITE (0,'("unhandled option ", a, " (this is a bug")') frictionLawType
             WRITE_DEBUG_INFO(100)
             STOP 3
          END SELECT
   
       END IF
          
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !    C U B O I D   V O L U M E   E L E M E N T S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT '("# number of cuboid volume elements")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%cuboid%ns
       PRINT '(I5)', in%cuboid%ns
       IF (in%cuboid%ns .GT. 0) THEN
          ALLOCATE(in%cuboid%s(in%cuboid%ns), &
                   in%cuboid%x(3,in%cuboid%ns), &
                   in%cuboid%xc(3,in%cuboid%ns), &
                   in%cuboid%length(in%cuboid%ns), &
                   in%cuboid%width(in%cuboid%ns), &
                   in%cuboid%thickness(in%cuboid%ns), &
                   in%cuboid%strike(in%cuboid%ns), &
                   in%cuboid%dip(in%cuboid%ns), &
                   in%cuboid%sv(3,in%cuboid%ns), &
                   in%cuboid%dv(3,in%cuboid%ns), &
                   in%cuboid%nv(3,in%cuboid%ns),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the list of volume elements"
          PRINT 2000
          PRINT '("#    n       e11       e12       e13       e22       e23       e33       ", &
                & "x1       x2       x3  length   width thickness strike dip")'
          PRINT 2000
          DO k=1,in%cuboid%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                  in%cuboid%s(k)%e11, &
                  in%cuboid%s(k)%e12, &
                  in%cuboid%s(k)%e13, &
                  in%cuboid%s(k)%e22, &
                  in%cuboid%s(k)%e23, &
                  in%cuboid%s(k)%e33, &
                  in%cuboid%x(1,k), &
                  in%cuboid%x(2,k), &
                  in%cuboid%x(3,k), &
                  in%cuboid%length(k), &
                  in%cuboid%thickness(k), &
                  in%cuboid%width(k), &
                  in%cuboid%strike(k), &
                  in%cuboid%dip(k)
   
             PRINT '(I6,6ES10.2E2,3ES9.2E1,3ES8.2E1,f7.1,f7.1)',i, &
                  in%cuboid%s(k)%e11, &
                  in%cuboid%s(k)%e12, &
                  in%cuboid%s(k)%e13, &
                  in%cuboid%s(k)%e22, &
                  in%cuboid%s(k)%e23, &
                  in%cuboid%s(k)%e33, &
                  in%cuboid%x(1,k), &
                  in%cuboid%x(2,k), &
                  in%cuboid%x(3,k), &
                  in%cuboid%length(k), &
                  in%cuboid%thickness(k), &
                  in%cuboid%width(k), &
                  in%cuboid%strike(k), &
                  in%cuboid%dip(k)
                
             IF (90. .NE. in%cuboid%dip(k)) THEN
                WRITE_DEBUG_INFO(-1)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("input error: volume element must be vertical (dip=90).")')
                STOP 2
             END IF

             ! convert to radians
             in%cuboid%strike(k)=in%cuboid%strike(k)*DEG2RAD     
             in%cuboid%dip(k)=in%cuboid%dip(k)*DEG2RAD     

             IF (0 .GT. in%cuboid%x(3,k)) THEN
                WRITE_DEBUG_INFO(-1)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("input error: depth must be positive")')
                STOP 2
             END IF

             IF (i .NE. k) THEN
                WRITE (STDERR,'("invalid volume element definition")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("input error: unexpected index")')
                STOP 1
             END IF
             IF (MAX(in%cuboid%length(k),in%cuboid%width(k),in%cuboid%thickness(k)) .LE. 0._8) THEN
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("input error: volume element dimension must be positive.")')
                STOP 1
             END IF
                
          END DO
   
          ! export cuboid volume elements
          filename=TRIM(in%wdir)//"/rvolume.shz"
          OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          WRITE (FPOUT,'("#    n       e11       e12       e13       e22       e23       e33           ", &
                & "x1           x2           x3  length thickne   width strike    dip")')
          DO k=1,in%cuboid%ns
             WRITE (FPOUT,'(I6,6ES10.2E2,3ES13.4E3,3ES8.2E1,f7.1,f7.1)') k, &
                  in%cuboid%s(k)%e11, &
                  in%cuboid%s(k)%e12, &
                  in%cuboid%s(k)%e13, &
                  in%cuboid%s(k)%e22, &
                  in%cuboid%s(k)%e23, &
                  in%cuboid%s(k)%e33, &
                  in%cuboid%x(1,k), &
                  in%cuboid%x(2,k), &
                  in%cuboid%x(3,k), &
                  in%cuboid%length(k), &
                  in%cuboid%thickness(k), &
                  in%cuboid%width(k), &
                  in%cuboid%strike(k)/DEG2RAD, &
                  in%cuboid%dip(k)/DEG2RAD
          END DO
          CLOSE(FPOUT)

          ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
          !   N O N L I N E A R   M A X W E L L   P R O P E R T I E S
          ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
          PRINT 2000
          PRINT '("# number of nonlinear Maxwell volume elements")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) in%cuboid%nNonlinearMaxwell
          PRINT '(I5)', in%cuboid%nNonlinearMaxwell
          IF (0 .NE. in%cuboid%nNonlinearMaxwell) THEN
             IF (in%cuboid%ns .NE. in%cuboid%nNonlinearMaxwell) THEN
                WRITE_DEBUG_INFO(-1)
                WRITE(STDERR,'("input error: nonlinear Maxwell properties ", &
                             & "are for none or all volume elements")')
                STOP 2
             END IF

             PRINT '("#    n      sII  gammadot0m        n")'
             PRINT 2000
             DO k=1,in%cuboid%ns
                CALL getdata(iunit,dataline)
                READ (dataline,*,IOSTAT=ierr) i, &
                      in%cuboid%s(k)%sII, &
                      in%cuboid%s(k)%ngammadot0m, &
                      in%cuboid%s(k)%npowerm
   
                PRINT '(I6,ES9.2E1,ES12.4E2,2ES9.2E1)',i, &
                      in%cuboid%s(k)%sII, &
                      in%cuboid%s(k)%ngammadot0m, &
                      in%cuboid%s(k)%npowerm
                
                ! set Kelvin properties to default values (no transient)
                in%cuboid%s(k)%ngammadot0k=0._8
                in%cuboid%s(k)%nGk=in%mu
                in%cuboid%s(k)%npowerk=1._8
   
                IF (i .NE. k) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("invalid property definition for volume element")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   WRITE (STDERR,'("error in input file: unexpected index")')
                   STOP 1
                END IF
             END DO

          END IF

          ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
          !   N O N L I N E A R   K E L V I N   P R O P E R T I E S
          ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
          PRINT 2000
          PRINT '("# number of nonlinear Kelvin cuboid volume elements")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) in%cuboid%nNonlinearKelvin
          PRINT '(I5)', in%cuboid%nNonlinearKelvin
          IF (0 .NE. in%cuboid%nNonlinearKelvin) THEN
             IF (in%cuboid%ns .NE. in%cuboid%nNonlinearKelvin) THEN
                WRITE_DEBUG_INFO(-1)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE(STDERR,'("input error: nonlinear Kelvin properties ", &
                             & "are for none or all cuboid volume elements")')
                STOP 2
             END IF

             PRINT '("#  n   gammadot0k           Gk         n")'
             PRINT 2000
             DO k=1,in%cuboid%ns
                CALL getdata(iunit,dataline)
                READ (dataline,*,IOSTAT=ierr) i, &
                      in%cuboid%s(k)%ngammadot0k, &
                      in%cuboid%s(k)%nGk, &
                      in%cuboid%s(k)%npowerk
   
                PRINT '(I4.4,2ES13.6E2,ES10.4E1)',i, &
                      in%cuboid%s(k)%ngammadot0k, &
                      in%cuboid%s(k)%nGk, &
                      in%cuboid%s(k)%npowerk
                
                IF (i .NE. k) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("invalid property definition for volume element")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   WRITE (STDERR,'("error in input file: unexpected index")')
                   STOP 1
                END IF
             END DO

          END IF
   
       END IF ! IF 0 .NE. number of cuboid volume elements
          
       ! - - - - - - - - - - - - - - - - - - - - - - - - - - - -
       !  T E T R A H E D R O N   V O L U M E   E L E M E N T S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT '("# number of tetrahedron volume elements")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%tetrahedron%ns
       PRINT '(I5)', in%tetrahedron%ns
       IF (in%tetrahedron%ns .GT. 0) THEN
          ALLOCATE(in%tetrahedron%s(in%tetrahedron%ns), &
                   in%tetrahedron%xc(3,in%tetrahedron%ns), &
                   in%tetrahedron%i1(in%tetrahedron%ns), &
                   in%tetrahedron%i2(in%tetrahedron%ns), &
                   in%tetrahedron%i3(in%tetrahedron%ns), &
                   in%tetrahedron%i4(in%tetrahedron%ns), &
                   in%tetrahedron%volume(in%tetrahedron%ns), &
                   in%tetrahedron%sv(3,in%tetrahedron%ns), &
                   in%tetrahedron%dv(3,in%tetrahedron%ns), &
                   in%tetrahedron%nv(3,in%tetrahedron%ns),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the list of volume elements"
          ! reset max vertex
          maxVertexIndex=-1
          PRINT 2000
          PRINT '("#    n       e11       e12       e13       e22       e23       e33       ", &
                & "i1       i2       i3      i4")'
          PRINT 2000
          DO k=1,in%tetrahedron%ns
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                  in%tetrahedron%s(k)%e11, &
                  in%tetrahedron%s(k)%e12, &
                  in%tetrahedron%s(k)%e13, &
                  in%tetrahedron%s(k)%e22, &
                  in%tetrahedron%s(k)%e23, &
                  in%tetrahedron%s(k)%e33, &
                  in%tetrahedron%i1(k), &
                  in%tetrahedron%i2(k), &
                  in%tetrahedron%i3(k), &
                  in%tetrahedron%i4(k)
   
             PRINT '(I6,6ES10.2E2,4I6)',i, &
                  in%tetrahedron%s(k)%e11, &
                  in%tetrahedron%s(k)%e12, &
                  in%tetrahedron%s(k)%e13, &
                  in%tetrahedron%s(k)%e22, &
                  in%tetrahedron%s(k)%e23, &
                  in%tetrahedron%s(k)%e33, &
                  in%tetrahedron%i1(k), &
                  in%tetrahedron%i2(k), &
                  in%tetrahedron%i3(k), &
                  in%tetrahedron%i4(k)
                
             IF (0 .GE. MIN(in%tetrahedron%i1(k), &
                            in%tetrahedron%i2(k), &
                            in%tetrahedron%i3(k), &
                            in%tetrahedron%i4(k))) THEN
                WRITE_DEBUG_INFO(-1)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("input error: vertex index must be positive.")')
                STOP 2
             END IF

             ! maximum index
             maxVertexIndex=MAX(maxVertexIndex, &
                                in%tetrahedron%i1(k), &
                                in%tetrahedron%i2(k), &
                                in%tetrahedron%i3(k), &
                                in%tetrahedron%i4(k))

             IF (i .NE. k) THEN
                WRITE (STDERR,'("invalid volume element definition")')
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("input error: unexpected line index")')
                STOP 1
             END IF
                
          END DO
   
          PRINT 2000
          PRINT '("# number of tetrahedron vertices")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) in%tetrahedron%nVe
          PRINT '(I5)', in%tetrahedron%nVe
          IF (maxVertexIndex .GT. in%tetrahedron%nVe) THEN
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE (STDERR,'("error in input file: not enough tetrahedron vertices")')
             STOP 1
          END IF
          IF (in%tetrahedron%nVe .GT. 0) THEN
             ALLOCATE(in%tetrahedron%v(3,in%tetrahedron%nVe),STAT=ierr)
             IF (ierr>0) STOP "could not allocate the list of tetrahedron vertices"
             PRINT 2000
             PRINT '("#    n       x1       x2       x3")'
             PRINT 2000
             DO k=1,in%tetrahedron%nVe
                CALL getdata(iunit,dataline)
                READ (dataline,*,IOSTAT=ierr) i, &
                      in%tetrahedron%v(1,k), &
                      in%tetrahedron%v(2,k), &
                      in%tetrahedron%v(3,k)
   
                PRINT '(I6,3ES9.2E1)',i, &
                     in%tetrahedron%v(1,k), &
                     in%tetrahedron%v(2,k), &
                     in%tetrahedron%v(3,k)
                
                IF (i .NE. k) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("invalid tetrahedron vertex definition ")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   WRITE (STDERR,'("error in input file: unexpected line index")')
                   STOP 1
                END IF
             END DO

             ! volume of tetrahedron volume elements
             DO k=1,in%tetrahedron%ns
                ! volume
                CALL cross( &
                       in%tetrahedron%v(:,in%tetrahedron%i2(k))-in%tetrahedron%v(:,in%tetrahedron%i1(k)), &
                       in%tetrahedron%v(:,in%tetrahedron%i3(k))-in%tetrahedron%v(:,in%tetrahedron%i1(k)), &
                       normal)
                in%tetrahedron%volume(k)=ABS(SUM(normal*( &
                     in%tetrahedron%v(:,in%tetrahedron%i4(k))-in%tetrahedron%v(:,in%tetrahedron%i1(k)))))/2
             END DO

             ! export tetrahedron volume elements
             filename=TRIM(in%wdir)//"/tvolume.tet"
             OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
             IF (ierr>0) THEN
                WRITE_DEBUG_INFO(102)
                WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
                STOP 1
             END IF
             WRITE (FPOUT,'("# tetrahedron volume elements")')
             WRITE (FPOUT,'("#    n       e11       e12       e13       e22       e23       e33       ", &
                   & " i1        i2        i3        i4")')
             DO k=1,in%tetrahedron%ns
                WRITE (FPOUT,'(I6,6ES10.3E2,4I10)') k, &
                     in%tetrahedron%s(k)%e11, &
                     in%tetrahedron%s(k)%e12, &
                     in%tetrahedron%s(k)%e13, &
                     in%tetrahedron%s(k)%e22, &
                     in%tetrahedron%s(k)%e23, &
                     in%tetrahedron%s(k)%e33, &
                     in%tetrahedron%i1(k), &
                     in%tetrahedron%i2(k), &
                     in%tetrahedron%i3(k), &
                     in%tetrahedron%i4(k)
             END DO
             CLOSE(FPOUT)

             filename=TRIM(in%wdir)//"/tvolume.ned"
             OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
             IF (ierr>0) THEN
                WRITE_DEBUG_INFO(102)
                WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
                STOP 1
             END IF
             WRITE (FPOUT,'("# vertices of tetrahedron volume elements")')
             WRITE (FPOUT,'("#    n               x1               x2               x3")')
             DO k=1,in%tetrahedron%nVe
                WRITE (FPOUT,'(I6.6,3ES17.7E3)') k, &
                     in%tetrahedron%v(1,k), &
                     in%tetrahedron%v(2,k), &
                     in%tetrahedron%v(3,k)
             END DO
             CLOSE(FPOUT)
                
          END IF
   
          ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
          !   N O N L I N E A R   M A X W E L L   P R O P E R T I E S
          ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
          PRINT 2000
          PRINT '("# number of nonlinear Maxwell tetrahedron volume elements")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) in%tetrahedron%nNonlinearMaxwell
          PRINT '(I5)', in%tetrahedron%nNonlinearMaxwell
          IF (0 .NE. in%tetrahedron%nNonlinearMaxwell) THEN
             IF (in%tetrahedron%ns .NE. in%tetrahedron%nNonlinearMaxwell) THEN
                WRITE_DEBUG_INFO(-1)
                WRITE(STDERR,'("input error: nonlinear Maxwell properties ", &
                             & "are for none or all tetrahedron volume elements")')
                STOP 2
             END IF

             PRINT '("#    n      sII  gammadot0m        n")'
             PRINT 2000
             DO k=1,in%tetrahedron%ns
                CALL getdata(iunit,dataline)
                READ (dataline,*,IOSTAT=ierr) i, &
                      in%tetrahedron%s(k)%sII, &
                      in%tetrahedron%s(k)%ngammadot0m, &
                      in%tetrahedron%s(k)%npowerm
   
                PRINT '(I6,ES9.2E1,ES12.4E2,2ES9.2E1)',i, &
                      in%tetrahedron%s(k)%sII, &
                      in%tetrahedron%s(k)%ngammadot0m, &
                      in%tetrahedron%s(k)%npowerm
                
                IF (i .NE. k) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("invalid property definition for volume element")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   WRITE (STDERR,'("error in input file: unexpected line index")')
                   STOP 1
                END IF
             END DO

          END IF

          ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
          !   N O N L I N E A R   K E L V I N   P R O P E R T I E S
          ! - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
          PRINT 2000
          PRINT '("# number of nonlinear Kelvin tetrahedron volume elements")'
          CALL getdata(iunit,dataline)
          READ  (dataline,*) in%tetrahedron%nNonlinearKelvin
          PRINT '(I5)', in%tetrahedron%nNonlinearKelvin
          IF (0 .NE. in%tetrahedron%nNonlinearKelvin) THEN
             IF (in%tetrahedron%ns .NE. in%tetrahedron%nNonlinearKelvin) THEN
                WRITE_DEBUG_INFO(-1)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE(STDERR,'("input error: nonlinear Kelvin properties ", &
                             & "are for none or all volume elements")')
                STOP 2
             END IF

             PRINT '("#  n   gammadot0k           Gk         n")'
             PRINT 2000
             DO k=1,in%tetrahedron%ns
                CALL getdata(iunit,dataline)
                READ (dataline,*,IOSTAT=ierr) i, &
                      in%tetrahedron%s(k)%ngammadot0k, &
                      in%tetrahedron%s(k)%nGk, &
                      in%tetrahedron%s(k)%npowerk
   
                PRINT '(I4.4,2ES13.6E2,ES10.4E1)',i, &
                      in%tetrahedron%s(k)%ngammadot0k, &
                      in%tetrahedron%s(k)%nGk, &
                      in%tetrahedron%s(k)%npowerk
                
                IF (i .NE. k) THEN
                   WRITE_DEBUG_INFO(200)
                   WRITE (STDERR,'("invalid property definition for volume element")')
                   WRITE (STDERR,'(a)') TRIM(dataline)
                   WRITE (STDERR,'("error in input file: unexpected index")')
                   STOP 1
                END IF
             END DO

          END IF
          
       END IF ! IF 0 .NE. number of tetrahedron volume elements
          
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !       O B S E R V A T I O N   P A T C H E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of observation patches")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) nObservationPatch
       PRINT '(I5)', nObservationPatch
       IF (0 .LT. nObservationPatch) THEN
          ALLOCATE(observationPatch(2,nObservationPatch),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the observation patches"
          PRINT 2000
          PRINT '("#    n      i rate")'
          PRINT 2000
          DO k=1,nObservationPatch
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i,observationPatch(:,k)

             PRINT '(I6,X,I6,X,I4)',i,observationPatch(:,k)

             IF (0 .GE. observationPatch(2,k)) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: sampling rate must be a strictly positive integer")')
                STOP 1
             END IF
             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO
       END IF

       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !       O B S E R V A T I O N   V O L U M E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of observation volumes")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) nObservationVolume
       PRINT '(I5)', nObservationVolume
       IF (0 .LT. nObservationVolume) THEN
          ALLOCATE(observationVolume(2,nObservationVolume),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the observation volumes"
          PRINT 2000
          PRINT '("#    n      i rate")'
          PRINT 2000
          DO k=1,nObservationVolume
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i,observationVolume(:,k)

             PRINT '(I6,X,I6,X,I4)',i,observationVolume(:,k)

             IF ((in%cuboid%ns+in%tetrahedron%ns) .LT. observationVolume(1,k)) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: observation volume does not exist.")')
                STOP 1
             END IF

             IF (0 .GE. observationVolume(2,k)) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: sampling rate must be strictly positive.")')
                STOP 1
             END IF

             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO
       END IF

#ifdef NETCDF
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !       O B S E R V A T I O N   P R O F I L E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of observation profiles")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%nObservationProfiles
       PRINT '(I5)', in%nObservationProfiles
       IF (0 .LT. in%nObservationProfiles) THEN
          ALLOCATE(in%observationProfileVelocity(in%nObservationProfiles),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the observation profiles"
          PRINT 2000
          PRINT '("#   n    n offset stride rate")'
          PRINT 2000
          DO k=1,in%nObservationProfiles
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                     in%observationProfileVelocity(k)%n, &
                     in%observationProfileVelocity(k)%offset, &
                     in%observationProfileVelocity(k)%stride, &
                     in%observationProfileVelocity(k)%rate

             PRINT '(I5,X,I4,X,I6,X,I6,X,I4)', i, &
                     in%observationProfileVelocity(k)%n, &
                     in%observationProfileVelocity(k)%offset, &
                     in%observationProfileVelocity(k)%stride, &
                     in%observationProfileVelocity(k)%rate

             IF (0 .GE. in%observationProfileVelocity(k)%n .OR. &
                 0 .GE. in%observationProfileVelocity(k)%stride .OR. &
                 0 .GE. in%observationProfileVelocity(k)%rate) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: invalid profile declaration")')
                STOP 1
             END IF

             IF (0 .GT. in%observationProfileVelocity(k)%offset) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: invalid offset in profile declaration")')
                STOP 1
             END IF

             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO
       END IF

       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !       O B S E R V A T I O N   I M A G E S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of observation images")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%nObservationImages
       PRINT '(I5)', in%nObservationImages
       IF (0 .LT. in%nObservationImages) THEN
          ALLOCATE(in%observationImageVelocity(in%nObservationImages),STAT=ierr)
          ALLOCATE(in%observationImageVelocityStrike(in%nObservationImages),STAT=ierr)
          ALLOCATE(in%observationImageVelocityDip(in%nObservationImages),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the observation profiles"
          PRINT 2000
          PRINT '("#   n   nl   nw offset stride rate")'
          PRINT 2000
          DO k=1,in%nObservationImages
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                     in%observationImageVelocity(k)%nl, &
                     in%observationImageVelocity(k)%nw, &
                     in%observationImageVelocity(k)%offset, &
                     in%observationImageVelocity(k)%stride, &
                     in%observationImageVelocity(k)%rate

             PRINT '(I5,X,I4,X,I4,X,I6,X,I6,X,I4)', i, &
                     in%observationImageVelocity(k)%nl, &
                     in%observationImageVelocity(k)%nw, &
                     in%observationImageVelocity(k)%offset, &
                     in%observationImageVelocity(k)%stride, &
                     in%observationImageVelocity(k)%rate

             IF (0 .GE. in%observationImageVelocity(k)%nl .OR. &
                 0 .GE. in%observationImageVelocity(k)%nw .OR. &
                 0 .GE. in%observationImageVelocity(k)%stride .OR. &
                 0 .GE. in%observationImageVelocity(k)%rate) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: invalid image declaration")')
                STOP 1
             END IF

             IF (0 .GT. in%observationImageVelocity(k)%offset) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: invalid offset for image declaration")')
                STOP 1
             END IF

             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO

          in%observationImageVelocityStrike=in%observationImageVelocity
          in%observationImageVelocityDip=in%observationImageVelocity
       END IF
#endif

       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !        O B S E R V A T I O N   P O I N T S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT 2000
       PRINT '("# number of observation points")'
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%nObservationPoint
       PRINT '(I5)', in%nObservationPoint
       IF (0 .LT. in%nObservationPoint) THEN
          ALLOCATE(in%observationPoint(in%nObservationPoint),STAT=ierr)
          IF (ierr>0) STOP "could not allocate the observation points"
          PRINT 2000
          PRINT '("#    n name       x1       x2       x3")'
          PRINT 2000
          DO k=1,in%nObservationPoint
             CALL getdata(iunit,dataline)
             READ (dataline,*,IOSTAT=ierr) i, &
                     in%observationPoint(k)%name, &
                     in%observationPoint(k)%x(1), &
                     in%observationPoint(k)%x(2), &
                     in%observationPoint(k)%x(3)

             PRINT '(I6,X,a4,3ES9.2E1)',i, &
                     in%observationPoint(k)%name, &
                     in%observationPoint(k)%x(1), &
                     in%observationPoint(k)%x(2), &
                     in%observationPoint(k)%x(3)

             IF (i .NE. k) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'("error in input file: unexpected index")')
                STOP 1
             END IF
          END DO
          
          ! export list of observation points
          filename=TRIM(in%wdir)//"/opts.dat"
          OPEN (UNIT=FPOUT,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
          IF (ierr>0) THEN
             WRITE_DEBUG_INFO(102)
             WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
             STOP 1
          END IF
          WRITE (FPOUT,'("# n name       x1       x2       x3")')
          DO k=1,in%nObservationPoint
             WRITE (FPOUT,'(I3.3,X,a4,3ES9.2E1)') k, &
                     in%observationPoint(k)%name, &
                     in%observationPoint(k)%x(1), &
                     in%observationPoint(k)%x(2), &
                     in%observationPoint(k)%x(3)
          END DO
          CLOSE(FPOUT)

       END IF

       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       !            P E R T U R B A T I O N S
       ! - - - - - - - - - - - - - - - - - - - - - - - - - -
       PRINT '("# number of perturbations")'
       CALL getdata(iunit,dataline)
       READ (dataline,*) in%ne
       PRINT '(I5)', in%ne
       IF (in%ne .GT. 0) ALLOCATE(in%event(in%ne),STAT=ierr)
       IF (ierr>0) STOP "could not allocate the event list"
       
       DO i=1,in%ne
          IF (1 .NE. i) THEN
             PRINT '("# time of next perturbation")'
             CALL getdata(iunit,dataline)
             READ (dataline,*) in%event(i)%time
             in%event(i)%i=i-1
             PRINT '(ES9.2E1)', in%event(i)%time
   
             IF (in%event(i)%time .LE. in%event(i-1)%time) THEN
                WRITE_DEBUG_INFO(200)
                WRITE (STDERR,'(a)') TRIM(dataline)
                WRITE (STDERR,'(a,a)') "input file error. ", &
                     "timing of perturbations must increase, quiting."
                STOP 1
             END IF
          ELSE
             in%event(1)%time=0._8
             in%event(1)%i=0
          END IF
   
       END DO
   
       ! test the presence of dislocations for coseismic calculation
       IF ((in%rectangle%ns .EQ. 0) .AND. &
           (in%triangle%ns .EQ. 0) .AND. &
           (in%cuboid%ns .EQ. 0) .AND. &
           (in%tetrahedron%ns .EQ. 0) .OR. &
           (in%interval .LE. 0._8)) THEN
   
          WRITE_DEBUG_INFO(300)
          WRITE (STDERR,'("nothing to do. exiting.")')
          STOP 1
       END IF
   
       PRINT 2000
       ! flush standard output
       CALL FLUSH(6)      

       in%nPatch=in%rectangle%ns+in%triangle%ns

       ! combine observation patches and observation volumes into observation states
       in%nObservationState=nObservationPatch+nObservationVolume
       ALLOCATE(in%observationState(4,in%nObservationState))
       j=1
       DO i=1,nObservationPatch
          in%observationState(1,j)=observationPatch(1,i)
          in%observationState(2,j)=observationPatch(2,i)
          in%observationState(3,j)=i
          j=j+1
       END DO

       DO i=1,nObservationVolume
          in%observationState(1,j)=observationVolume(1,i)+in%nPatch
          in%observationState(2,j)=observationVolume(2,i)
          in%observationState(3,j)=i
          j=j+1
       END DO
       IF (ALLOCATED(observationPatch)) DEALLOCATE(observationPatch)
       IF (ALLOCATED(observationVolume)) DEALLOCATE(observationVolume)

       position=0
       CALL MPI_PACK(in%interval,                     1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%lambda,                       1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%mu,                           1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nu,                           1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%rectangle%ns,                 1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%triangle%ns,                  1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%triangle%nVe,                 1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%cuboid%ns,                    1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%cuboid%nNonlinearMaxwell,     1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%cuboid%nNonlinearKelvin,      1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%tetrahedron%ns,               1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%tetrahedron%nVe,              1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%tetrahedron%nNonlinearMaxwell,1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%tetrahedron%nNonlinearKelvin, 1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%ne,                           1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nObservationState,            1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_PACK(in%nObservationPoint,            1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)

       position=0
       CALL MPI_PACK(in%wdir,psize,MPI_CHARACTER,packed,psize,position,MPI_COMM_WORLD,ierr)
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)

       ! send the rectangle patches (geometry and friction properties) 
       DO i=1,in%rectangle%ns
          position=0
          CALL MPI_PACK(in%rectangle%s(i)%Vl,     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%x(1,i),      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%x(2,i),      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%x(3,i),      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%length(i),   1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%width(i),    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%strike(i),   1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%dip(i),      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%rake,   1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%opening,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%tau0,   1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%mu0,    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%sig,    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%a,      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%b,      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%L,      1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%Vo,     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%V2,     1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%rectangle%s(i)%damping,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the triangle patches (geometry and friction properties) 
       DO i=1,in%triangle%ns
          position=0
          CALL MPI_PACK(in%triangle%s(i)%Vl,     1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%i1(i),       1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%i2(i),       1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%i3(i),       1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%rake,   1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%opening,1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%tau0,   1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%mu0,    1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%sig,    1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%a,      1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%b,      1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%L,      1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%Vo,     1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%V2,     1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%triangle%s(i)%damping,1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the triangle vertices
       IF (0 .NE. in%triangle%ns) THEN
          DO i=1,in%triangle%nVe
             position=0
             CALL MPI_PACK(in%triangle%v(1,i),1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%triangle%v(2,i),1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%triangle%v(3,i),1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          END DO
       END IF

       ! send the cuboid volume elements
       DO i=1,in%cuboid%ns
          position=0
          CALL MPI_PACK(in%cuboid%s(i)%sII,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%cuboid%s(i)%e11,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%cuboid%s(i)%e12,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%cuboid%s(i)%e13,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%cuboid%s(i)%e22,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%cuboid%s(i)%e23,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%cuboid%s(i)%e33,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%cuboid%x(1,i),          1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%cuboid%x(2,i),          1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%cuboid%x(3,i),          1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%cuboid%length(i),       1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%cuboid%thickness(i),    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%cuboid%width(i),        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%cuboid%strike(i),       1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%cuboid%dip(i),          1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%cuboid%s(i)%Gk,         1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%cuboid%s(i)%gammadot0k, 1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%cuboid%s(i)%gammadot0m, 1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%cuboid%s(i)%nGk,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%cuboid%s(i)%ngammadot0k,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%cuboid%s(i)%npowerk,    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%cuboid%s(i)%ngammadot0m,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%cuboid%s(i)%npowerm,    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the tetrahedron volume elements
       DO i=1,in%tetrahedron%ns
          position=0
          CALL MPI_PACK(in%tetrahedron%s(i)%sII,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%tetrahedron%s(i)%e11,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%tetrahedron%s(i)%e12,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%tetrahedron%s(i)%e13,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%tetrahedron%s(i)%e22,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%tetrahedron%s(i)%e23,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%tetrahedron%s(i)%e33,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%tetrahedron%i1(i),           1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%tetrahedron%i2(i),           1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%tetrahedron%i3(i),           1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%tetrahedron%i4(i),           1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%tetrahedron%s(i)%Gk,         1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%tetrahedron%s(i)%gammadot0k, 1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%tetrahedron%s(i)%gammadot0m, 1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%tetrahedron%s(i)%nGk,        1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%tetrahedron%s(i)%ngammadot0k,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%tetrahedron%s(i)%npowerk,    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%tetrahedron%s(i)%ngammadot0m,1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%tetrahedron%s(i)%npowerm,    1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the tetrahedron vertices
       IF (0 .NE. in%tetrahedron%ns) THEN
          DO i=1,in%tetrahedron%nVe
             position=0
             CALL MPI_PACK(in%tetrahedron%v(1,i),1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%tetrahedron%v(2,i),1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_PACK(in%tetrahedron%v(3,i),1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          END DO
       END IF

       ! send the observation state
       DO i=1,in%nObservationState
          position=0
          CALL MPI_PACK(in%observationState(:,i),4,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the observation points
       DO i=1,in%nObservationPoint
          position=0
          CALL MPI_PACK(in%observationPoint(i)%name,10,MPI_CHARACTER,packed,psize,position,MPI_COMM_WORLD,ierr)
          DO k=1,3
             CALL MPI_PACK(in%observationPoint(i)%x(k),1,MPI_REAL8,packed,psize,position,MPI_COMM_WORLD,ierr)
          END DO
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

       ! send the perturbation events
       DO k=1,in%ne
          CALL MPI_PACK(in%event(k)%time,1,MPI_REAL8,  packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_PACK(in%event(k)%i,   1,MPI_INTEGER,packed,psize,position,MPI_COMM_WORLD,ierr)
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       END DO

    ELSE ! IF 0 .NE. rank

       !------------------------------------------------------------------
       ! S L A V E S
       !------------------------------------------------------------------

       position=0
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%interval,                      1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%lambda,                        1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%mu,                            1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nu,                            1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%rectangle%ns,             1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%triangle%ns,              1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%triangle%nVe,             1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%cuboid%ns,               1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%cuboid%nNonlinearMaxwell,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%cuboid%nNonlinearKelvin,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%ns,               1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%nVe,              1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%nNonlinearMaxwell,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%nNonlinearKelvin,1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%ne,                            1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nObservationState,             1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%nObservationPoint,             1,MPI_INTEGER,MPI_COMM_WORLD,ierr)

       position=0
       CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
       CALL MPI_UNPACK(packed,psize,position,in%wdir,256,MPI_CHARACTER,MPI_COMM_WORLD,ierr)

       IF (0 .LT. in%rectangle%ns) &
                    ALLOCATE(in%rectangle%s(in%rectangle%ns), &
                             in%rectangle%x(3,in%rectangle%ns), &
                             in%rectangle%xc(3,in%rectangle%ns), &
                             in%rectangle%length(in%rectangle%ns), &
                             in%rectangle%width(in%rectangle%ns), &
                             in%rectangle%strike(in%rectangle%ns), &
                             in%rectangle%dip(in%rectangle%ns), &
                             in%rectangle%sv(3,in%rectangle%ns), &
                             in%rectangle%dv(3,in%rectangle%ns), &
                             in%rectangle%nv(3,in%rectangle%ns), &
                             STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory"

       IF (0 .LT. in%triangle%ns) &
                    ALLOCATE(in%triangle%s(in%triangle%ns), &
                             in%triangle%xc(3,in%triangle%ns), &
                             in%triangle%i1(in%triangle%ns), &
                             in%triangle%i2(in%triangle%ns), &
                             in%triangle%i3(in%triangle%ns), &
                             in%triangle%area(in%triangle%ns), &
                             in%triangle%sv(3,in%triangle%ns), &
                             in%triangle%dv(3,in%triangle%ns), &
                             in%triangle%nv(3,in%triangle%ns),STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory"

       IF (0 .LT. in%triangle%nVe) ALLOCATE(in%triangle%v(3,in%triangle%nVe), STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory"

       IF (0 .LT. in%cuboid%ns) &
                    ALLOCATE(in%cuboid%s(in%cuboid%ns), &
                             in%cuboid%x(3,in%cuboid%ns), &
                             in%cuboid%xc(3,in%cuboid%ns), &
                             in%cuboid%length(in%cuboid%ns), &
                             in%cuboid%thickness(in%cuboid%ns), &
                             in%cuboid%width(in%cuboid%ns), &
                             in%cuboid%strike(in%cuboid%ns), &
                             in%cuboid%dip(in%cuboid%ns), &
                             in%cuboid%sv(3,in%cuboid%ns), &
                             in%cuboid%dv(3,in%cuboid%ns), &
                             in%cuboid%nv(3,in%cuboid%ns),STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory"

       IF (0 .LT. in%tetrahedron%ns) &
                    ALLOCATE(in%tetrahedron%s(in%tetrahedron%ns), &
                             in%tetrahedron%xc(3,in%tetrahedron%ns), &
                             in%tetrahedron%i1(in%tetrahedron%ns), &
                             in%tetrahedron%i2(in%tetrahedron%ns), &
                             in%tetrahedron%i3(in%tetrahedron%ns), &
                             in%tetrahedron%i4(in%tetrahedron%ns), &
                             in%tetrahedron%volume(in%tetrahedron%ns), &
                             in%tetrahedron%sv(3,in%tetrahedron%ns), &
                             in%tetrahedron%dv(3,in%tetrahedron%ns), &
                             in%tetrahedron%nv(3,in%tetrahedron%ns),STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory"

       IF (0 .LT. in%tetrahedron%nVe) ALLOCATE(in%tetrahedron%v(3,in%tetrahedron%nVe), STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory"

       IF (0 .LT. in%ne) ALLOCATE(in%event(in%ne),STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory"

       DO i=1,in%rectangle%ns
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%Vl,     1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%x(1,i),      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%x(2,i),      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%x(3,i),      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%length(i),   1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%width(i),    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%strike(i),   1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%dip(i),      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%rake,   1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%opening,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%tau0,   1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%mu0,    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%sig,    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%a,      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%b,      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%L,      1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%Vo,     1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%V2,     1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%rectangle%s(i)%damping,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
       END DO

       DO i=1,in%triangle%ns
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%Vl,     1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%i1(i),       1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%i2(i),       1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%i3(i),       1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%rake,   1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%opening,1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%tau0,   1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%mu0,    1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%sig,    1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%a,      1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%b,      1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%L,      1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%Vo,     1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%V2,     1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%triangle%s(i)%damping,1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
       END DO

       IF (0 .NE. in%triangle%ns) THEN
          DO i=1,in%triangle%nVe
             position=0
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%triangle%v(1,i),1,MPI_REAL8,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%triangle%v(2,i),1,MPI_REAL8,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%triangle%v(3,i),1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          END DO
       END IF

       DO i=1,in%cuboid%ns
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%cuboid%s(i)%sII,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%cuboid%s(i)%e11,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%cuboid%s(i)%e12,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%cuboid%s(i)%e13,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%cuboid%s(i)%e22,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%cuboid%s(i)%e23,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%cuboid%s(i)%e33,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%cuboid%x(1,i),          1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%cuboid%x(2,i),          1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%cuboid%x(3,i),          1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%cuboid%length(i),       1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%cuboid%thickness(i),    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%cuboid%width(i),        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%cuboid%strike(i),       1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%cuboid%dip(i),          1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%cuboid%s(i)%Gk,         1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%cuboid%s(i)%gammadot0k, 1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%cuboid%s(i)%gammadot0m, 1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%cuboid%s(i)%nGk,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%cuboid%s(i)%ngammadot0k,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%cuboid%s(i)%npowerk,    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%cuboid%s(i)%ngammadot0m,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%cuboid%s(i)%npowerm,    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
       END DO

       DO i=1,in%tetrahedron%ns
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%s(i)%sII,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%s(i)%e11,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%s(i)%e12,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%s(i)%e13,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%s(i)%e22,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%s(i)%e23,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%s(i)%e33,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%i1(i),           1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%i2(i),           1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%i3(i),           1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%i4(i),           1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%s(i)%Gk,         1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%s(i)%gammadot0k, 1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%s(i)%gammadot0m, 1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%s(i)%nGk,        1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%s(i)%ngammadot0k,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%s(i)%npowerk,    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%s(i)%ngammadot0m,1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%s(i)%npowerm,    1,MPI_REAL8,MPI_COMM_WORLD,ierr)
       END DO

       IF (0 .NE. in%tetrahedron%ns) THEN
          DO i=1,in%tetrahedron%nVe
             position=0
             CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%v(1,i),1,MPI_REAL8,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%v(2,i),1,MPI_REAL8,MPI_COMM_WORLD,ierr)
             CALL MPI_UNPACK(packed,psize,position,in%tetrahedron%v(3,i),1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          END DO
       END IF

       IF (0 .LT. in%nObservationState) &
                    ALLOCATE(in%observationState(4,in%nObservationState),STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for observation states"

       DO i=1,in%nObservationState
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%observationState(:,i),4,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       END DO

       IF (0 .LT. in%nObservationPoint) &
                    ALLOCATE(in%observationPoint(in%nObservationPoint), &
                             STAT=ierr)
       IF (ierr>0) STOP "slave could not allocate memory for observation points"

       DO i=1,in%nObservationPoint
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%observationPoint(i)%name,10,MPI_CHARACTER,MPI_COMM_WORLD,ierr)
          DO k=1,3
             CALL MPI_UNPACK(packed,psize,position,in%observationPoint(i)%x(k),1,MPI_REAL8,MPI_COMM_WORLD,ierr)
          END DO
       END DO

       DO i=1,in%ne
          position=0
          CALL MPI_BCAST(packed,psize,MPI_PACKED,0,MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%event(i)%time,1,MPI_REAL8,  MPI_COMM_WORLD,ierr)
          CALL MPI_UNPACK(packed,psize,position,in%event(i)%i,   1,MPI_INTEGER,MPI_COMM_WORLD,ierr)
       END DO

       CALL FLUSH(6)      

       in%nPatch=in%rectangle%ns+in%triangle%ns

    END IF ! master or slaves

    in%nVolume=in%cuboid%ns+in%tetrahedron%ns

2000 FORMAT ("# ----------------------------------------------------------------------------")
   
  END SUBROUTINE init
   
  !-----------------------------------------------
  !> subroutine cross
  !! compute the cross product between two vectors
  !-----------------------------------------------
  SUBROUTINE cross(u,v,w)
    REAL*8, DIMENSION(3), INTENT(IN) :: u,v
    REAL*8, DIMENSION(3), INTENT(OUT) :: w

    w(1)=u(2)*v(3)-u(3)*v(2)
    w(2)=u(3)*v(1)-u(1)*v(3)
    w(3)=u(1)*v(2)-u(2)*v(1)

  END SUBROUTINE cross

  !-----------------------------------------------
  !> subroutine printhelp
  !! displays a help message with master thread.
  !-----------------------------------------------
  SUBROUTINE printhelp()

    INTEGER :: rank,size,ierr

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,size,ierr)

    IF (0.EQ.rank) THEN
       PRINT '("usage:")'
       PRINT '("")'
       PRINT '("mpirun -n 2 unicycle-3d-viscouscycles [-h] [--dry-run] [--help] [--epsilon 1e-6] [filename]")'
       PRINT '("")'
       PRINT '("options:")'
       PRINT '("   -h                           prints this message and aborts calculation")'
       PRINT '("   --dry-run                    abort calculation, only output geometry")'
       PRINT '("   --help                       prints this message and aborts calculation")'
       PRINT '("   --version                    print version number and exit")'
       PRINT '("   --epsilon                    set the numerical accuracy [1E-6]")'
       PRINT '("   --friction-law               type of friction law [1]")'
       PRINT '("       1: multiplicative        form of rate-state friction (Barbot, 2019)")'
       PRINT '("       2: additive              form of rate-state friction (Ruina, 1983)")'
       PRINT '("       3: arcsinh               form of rate-state friction (Rice & Benzion, 1996)")'
       PRINT '("       4: cut-off velocity      form of rate-state friction (Okubo, 1989)")'
       PRINT '("   --evolution-law              type of evolution law [1]")'
       PRINT '("       1: aging law             isothermal, isobaric additive form (Ruina, 1983)")'
       PRINT '("       2: slip law              isothermal, isobaric multiplicative form (Ruina, 1983)")'
       PRINT '("   --export-greens wdir         export the Greens function to file")'
       PRINT '("   --import-greens wdir         import the Greens function from file")'
       PRINT '("   --export-netcdf              export results to GMT-compatible netcdf files")'
       PRINT '("   --export-slip                export slip profiles to GMT-compatible netcdf files")'
       PRINT '("   --export-stress              export stress profiles to GMT-compatible netcdf files")'
       PRINT '("   --export-vtp                 export results to Paraview-compatible .vtp files")'
       PRINT '("   --export-slip-distribution r export slip distribution every r time steps")'
       PRINT '("   --maximum-iterations         set the maximum time step [1000000]")'
       PRINT '("   --maximum-step               set the maximum time step [none]")'
       PRINT '("")'
       PRINT '("description:")'
       PRINT '("   simulates elasto-dynamics on faults in a viscoelastic")'
       PRINT '("   medium following the radiation-damping approximation")'
       PRINT '("   using the integral method.")'
       PRINT '("")'
       PRINT '("see also: ""man unicycle""")'
       PRINT '("")'
       PRINT '("           uu   uu    ")'
       PRINT '("            uuuuu     ")'
       PRINT '("              f       ")'
       PRINT '("              f       ")'
       PRINT '("              f       ")'
       PRINT '("            .88b.     ")'
       PRINT '("          .8P   Y8.   ")'
       PRINT '("          88 \|/ 88   ")'
       PRINT '("          88 /|\ 88   ")'
       PRINT '("           8b   d8    ")'
       PRINT '("            Y88P      ")'
       PRINT '("--------------------------------")'
       PRINT '("")'
       CALL FLUSH(6)
    END IF

  END SUBROUTINE printhelp

  !-----------------------------------------------
  !> subroutine printversion
  !! displays code version with master thread.
  !-----------------------------------------------
  SUBROUTINE printversion()

    INTEGER :: rank,size,ierr

    CALL MPI_COMM_RANK(MPI_COMM_WORLD,rank,ierr)
    CALL MPI_COMM_SIZE(MPI_COMM_WORLD,size,ierr)

    IF (0.EQ.rank) THEN
       PRINT '("unicycle-3d-viscouscycles version 1.0.0, compiled on ",a)', __DATE__
       PRINT '("")'
       CALL FLUSH(6)
    END IF

  END SUBROUTINE printversion

END PROGRAM viscouscycles




