# CMake project file for Unicycle

CMAKE_MINIMUM_REQUIRED(VERSION 3.5)
PROJECT(Unicycle C Fortran)

# Set the version
SET(VERSION 1.0.0)

# Add our local modules to the module path
SET(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake/")

# Uncomment if it is required that Fortran 90 is supported
IF(NOT CMAKE_Fortran_COMPILER_SUPPORTS_F90)
    MESSAGE(FATAL_ERROR "Fortran compiler does not support F90")
ENDIF(NOT CMAKE_Fortran_COMPILER_SUPPORTS_F90)

# Set some options the user may choose
# Uncomment the below if you want the user to choose a parallelization library
OPTION(USE_NETCDF "Use Netcdf for data import and export" ON)

OPTION(USE_ODE45 "Use the 4/5th-order accurate Runge-Kutta method" ON)
OPTION(USE_ODE23 "Use the 2/3rd-order accurate Runge-Kutta method" OFF)

IF (USE_ODE45 AND USE_ODE23)
  MESSAGE(FATAL_ERROR "Use either -D USE_ODE23 or -D USE_ODE45")
ENDIF()

IF (USE_NETCDF)
  SET (NETCDF_F90 TRUE)
  FIND_PACKAGE(NetCDF REQUIRED)
  INCLUDE_DIRECTORIES(${NETCDF_INCLUDES})
  ADD_DEFINITIONS(-DNETCDF)
  MESSAGE (STATUS "NetCDF path: " ${NETCDF_INCLUDES})
ENDIF()

ADD_COMPILE_OPTIONS("-cpp")

# Enable Fortran's C interoperability features
ENABLE_LANGUAGE(Fortran)
   
# shared libraries
ADD_SUBDIRECTORY(${CMAKE_SOURCE_DIR}/share/src)

# AP binary
ADD_SUBDIRECTORY(${CMAKE_SOURCE_DIR}/2d/antiplane/src)

# PS binary
ADD_SUBDIRECTORY(${CMAKE_SOURCE_DIR}/2d/planestrain/src)

# 3D binary
ADD_SUBDIRECTORY(${CMAKE_SOURCE_DIR}/3d/src)

# 3D-serial binary
#ADD_SUBDIRECTORY(${CMAKE_SOURCE_DIR}/3d-serial/src)

# Add a distclean target to the Makefile
ADD_CUSTOM_TARGET(distclean
    COMMAND ${CMAKE_COMMAND} -P ${CMAKE_SOURCE_DIR}/distclean.cmake
)

# installation
SET(TARGETS "unicycle-ap-ratestate;unicycle-ap-ratestate-cz;unicycle-ap-viscouscycles;unicycle-ap-ratestate-bath;unicycle-ap-thermobaric;unicycle-ps-ratestate;unicycle-ps-viscouscycles;unicycle-ps-ratestate-bath;unicycle-ps-thermobaric;unicycle-3d-ratestate")
INSTALL(TARGETS ${TARGETS}
  RUNTIME DESTINATION bin
  COMPONENT runtime)

