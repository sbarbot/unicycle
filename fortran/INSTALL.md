# BUILD

To build the Unicycle programs from the code base, you must have

  * Fortran compiler (gcc-9 and above) (https://gcc.gnu.org/)
  * netcdf and netcdff libraries (https://www.unidata.ucar.edu/software/netcdf/)

The compilation is conducted with *CMake* or *Makefile*. To compile and install all the binary files at once, use

    mkdir -p release && cd release
    cmake ..
    make
    make install

For a faster build, use `make -jN`, where N is the number of parallel threads. For an even faster build with *Ninja*, use

    mkdir -p release && cd release
    cmake -G Ninja ..
    ninja
    ninja install

Alternatively, use individual `makefiles`. To conform with your system configuration, make a copy of a template Makefile and link with a generic name. For example,

    cd unicycle/fortran/2d/antiplane
    cp makefile_brew makefile_my
    ln -s makefile_my makefile

Then, update the makefile with your system preferences (e.g., `vi makefile`), including compiler name, include and library paths. Finally, build the *unicycle-ap-ratestate*, *unicycle-ap-ratestate-cz*, and others codes with

    make all

The codes with the -ratestate suffix only resolve fault dynamics. The codes with the -viscouscycles suffix resolve the coupling between fault friction and viscoelastic flow. The binary files will be located in `unicycle/fortran/2d/antiplane/build/`. Shared Fortran module files will be placed in `unicycle/fortran/share/lib`.

A simple codebase without parallelism is available in `unicycle/fortran/2d/antiplane-serial` for educational purposes. To compile and build, use the following commands

    cd unicycle/fortran/2d/antiplane-serial
    makefile -f makefile_brew

All other Fortran-based codes are parallelized with Message Passing Interface (MPI). To compile the remaining binaries, repeat the operation with

    cd unicycle/fortran/2d/planestrain
    cp makefile_brew_serial makefile_my
    ln -s makefile_my makefile
    make all

For 3d calculations, compile with

    cd unicycle/fortran/3d
    cp makefile_brew makefile_my
    ln -s makefile_my makefile
    make all

The binary files will be located in the corresponding `build` directories.

# VISUALIZATION

For visualization purposes, it is recommended to install

  * gnuplot (http://www.gnuplot.info/)
  * GMT6 (http://gmt.soest.hawaii.edu/doc/latest/gmt.html)
  * xpdf (https://www.xpdfreader.com/)
  * Paraview (https://www.paraview.org/download/)

The simulation data consist of netcdf binary files compatible with the Generic Mapping Tools version 4.5 and above, with the `.grd` file extension. Additional simulation data is exported in ASCII format in files with the `.dat` extension. Files with the `.vtp` extension can be visualized with Paraview.

The `.grd` files can be plotted rapidly using GMT5 and above using the bash script `grdmap.sh` located in the `bash` directory. For example,

    grdmap.sh -H -p -12/1/0.01 -c cycles.cpt image-000100-log10v.grd

produces the postscript file `image-000100-log10v.grd-plot.ps` and the pdf file `image-000100-log10v.grd-plot.pdf` containing a snapshot of the instantaneous slip-rate along a fault. If the program `xpdf` is available, the `.pdf` file will be loaded automatically, unless the `-x` option is added. Multiple snapshots can be animated into a movie of rupture dynamics using standard tools such as `ffmpeg`.

The command

    grdmap.sh -p -12/1/0.01 -c cycles.cpt log10v.001.grd

will produce the postscript and pdf files of a time series of instantenous velocity along a profile for 3d models. In two-dimensional models (anti-plane or plane strain), the time series of instantaneous velocity for all patches can be found in log10v.grd.

Time series of peak velocity on fault 1 can always be visualized with `gnuplot` using

    > gnuplot
    set logscale y
    plot 'time.dat' using 1:3 with lines

The column index for the slip-rate is different for 2d and 3d models. Some tutorial scripts produce postscript and pdf files automatically. 

# EXECUTION

Example input files are located in the `examples/tutorials` directories. To run the bash scripts, add the code path to the `PATH` environment variable,

    export PATH=$PATH:unicycle/fortran/2d/antiplane/build
    export PATH=$PATH:unicycle/fortran/2d/planestrain/build
    export PATH=$PATH:unicycle/fortran/3d/build

Then, run the bash scripts. For two-dimensional models in antiplane strain, the bash scripts `brittle1.sh` and `brittle2.sh` simulate earthquake slow-slip cycles, respectively, on a vertical strike-slip fault. The script `viscous1.sh` simulate seismic cycles on a fault in a viscoelastic medium.

    cd unicycle/fortran/2d/antiplane/examples/tutorials
    ./brittle1.sh
    ./brittle2.sh
    ./viscous1.sh

For two-dimensional models in condition of in-plane strain, the bash scripts `brittle1.sh` simulates cycles of earthquakes on a subduction megathrust. The script `viscous1.sh` simulate seismic cycles on a megathrust, but includes viscoelastic flow in the oceanic asthenosphere and mantle wedge. The model uses line elements to represent fault slip and triangle elements to represent plastic strain.

    cd unicycle/fortran/2d/planestrain/examples/tutorials
    ./brittle1.sh
    ./viscous1.sh

For three-dimensional models, `brittle1.sh` computes cycles in a 3d elastic half-space for the case of a right-lateral strike-slip faults. `brittle2.sh` simulates seismic cycles in an elastic half-space for a 30º-dipping thrust fault. `brittle3.sh` computes seismic cycles in an elastic half-space for the case of a 60º-dipping normal fault. The scripts `viscous1.sh`, `viscou2.sh`, and `viscous3.sh` compute seismic cycles for strike-slip, thrust, and normal faults in a viscoelastic medium. 

    cd unicycle/fortran/3d/examples/tutorials
    ./brittle1.sh
    ./brittle2.sh
    ./brittle3.sh
    ./viscous1.sh

Tutorial models coupling fault dynamics and viscoelastic flow can be found in the same folder. `viscous1.sh` computes seismic cycle in a 3d viscoelastic medium for a right-lateral strike-slip fault using structured mesh of cuboid elements in the ductile substrate. `viscous4.sh` does the same using a tetrahedral mesh to capture the curvature of the Moho. Other examples include thrust and normal faults.

# DOXYGEN

You can generate a doxygen (https://doxygen.nl/) interface with the command

    doxygen .doxygen-ratestate
    doxygen .doxygen-viscouscycles

in the directories `fortran/2d/antiplane`, `fortran/2d/planestrain`, and `fortran/3d`. Other documentation is available in the man and markdown formats.

