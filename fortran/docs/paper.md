---
title: 'Unicycle: numerical simulations of seismic cycles in a viscoelastic half space with the integral method'
tags:
  - fault dynamics
  - friction
  - viscoelasticity
  - geophysics
  - Fortran90
authors:
  - name: Sylvain Barbot
    orcid: 0000-0003-4257-7409
    affiliation: 1
affiliations:
 - name: University of Southern California
   index: 1
date: 15 August 2022
bibliography: paper.bib
---

# Summary

The deformation of the Earth's crust and upper mantle spans widely different length scales, from extreme localization within fault zones, to broadly distributed viscoelastic strain in the asthenosphere. The `Unicycle` software, which stands for *Unified Cycle of Earthquakes*, provides an efficient representation of these deformation mechanisms by combining surface and volume elements. Numerical simulations of seismic cycles in a viscoelastic half-space are conducted using the integral method whereby the elastic interactions are computed semi-analytically using closed-form expressions of Green's functions [@barbot+17;  @barbot18a]. The method only requires meshing of the regions that undergo thermo-dynamically irreversible deformation, resulting in relatively small meshes that can be assembled easily. In strictly brittle models, the technique simplifies to the boundary integral method.

The approach accommodates complex structural settings with faults discretized with rectangle or triangle surface elements and ductile domains meshed with cuboid or tetrahedron volume elements. Forward models of seismic cycles with viscoelastic relaxation are obtained using the fifth-order Runge-Kutta method with adaptive time steps. The calculation is particularly efficient, requiring just a few matrix-vector multiplication per time step, which is parallelized for a distributed-memory architecture.

Crustal dynamics is computed based on a physical model of rate- and state-dependent friction [@barbot19a] and a nonlinear rheology for transient creep and steady-state creep of bulk rocks [@masuti+16; @masuti+barbot21]. Applications range from two-dimensional models of the lithosphere-asthenosphere system [@lambert+barbot16; @shi+20; @barbot20a] to three-dimensional models of faults interacting with a ductile asthenosphere [@shi+22].

# Statement of need

`Unicycle` is a series of Fortran90 standalone numerical modeling tools for simulations of crustal dynamics in a two-dimensional or three-dimensional half-space. The input file allows complex frictional, rheological, and structural settings and the automatic exploration of the parameter space. The simulation requires the initial time-consuming calculation of large matrices that capture the stress interactions among surface and volume elements. This matrix can be automatically saved and re-used in subsequent calculations that use different material properties but the same geometry. Meshing of the brittle faults and ductile regions is relatively straightforward and can be done with standard tools. The simulation output is provided in ASCII tables and netcdf binary files compatible with the General Mapping Tools version 5 and above [@wessel+19]. Additional output files enable three-dimensional visualization with the Paraview software [@ahrens+05].

`Unicycle` is designed for scientists conducting research in lithosphere dynamics. Applications include fault dynamics, e.g., the initiation, propagation, and arrest of ruptures, lower-crustal and asthenosphere dymamics with nonlinear rheology, e.g., power-law constitutive laws with transient creep, and the mechanical coupling between localized and distributed deformation in various tectonic environments. Successful simulation benchmarks for fault dynamics based on comparison with other software can be found in @jiang+22.

# Acknowledgements

This study is supported in part by the National Science Foundation under award number EAR-1848192.

# References
