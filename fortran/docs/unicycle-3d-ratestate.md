---
Date: 15 Aug 2022
Title: unicycle-3d-ratestate documentation
---

# NAME

**unicycle-3d-ratestate** - Unified Cycle of Earthquakes for a three-dimensional half-space. Evaluates the deformation due to fault slip using the boundary integral method.

# SYNOPSIS

**unicycle-3d-ratestate** [-h] [--dry-run] [--help] [--export-netcdf] [--export-greens dir] [--epsilon] [--friction-law ( 1 | 2 | 3 | 4)] [--import-greens dir] [--maximum-step] [--maximum-iterations] [--version]

# DESCRIPTION

`unicycle-3d-ratestate` computes the evolution of slip on rate- and state-dependent friction faults in isothermal condition in an elastic half-space.

# OPTIONS

`-h` print a short message and abort calculation

`--dry-run` write lightweight information files and abort calculation

`--help` print a short message and abort calculation

`--export-greens dir` export the Greens function to a GMT compatible netcdf file in directory dir

`--export-netcdf` export time series of instantaneous velocity in a GMT compatible netcdf file

`--epsilon [1e-6]` set the relative accuracy of the 4/5th order Runge-Kutta integration method

`--friction-law ( 1 \| 2 \| 3 )` select the type of rate- and state-dependent friction law [default: 1]
  * 1: multiplicative form (Barbot, 2019)
  * 2: additive form (Ruina, 1983)
  * 3: arcsinh form (Rice & Benzion, 1996)
  * 4: cut-off velocity form (Okubo, 1989)

`--import-greens dir` import the Greens function from directory `wdir`

`--maximum-step [Inf]` set the maximum time step

`--maximum-iterations [1000000]` set the maximum number of iterations

# ENVIRONMENT

The calculation is parallelized with MPI. Calling the programs with

    unicycle-3d-ratestate

is equivalent to using

    mpirun -n 1 unicycle-3d-ratestate

# INPUT PARAMETERS

**output directory (wdir)** :   All output files are written to the specified directory, including
    observation patches, observation points, and netcdf files.

**elastic moduli** :   The uniform Lame parameter (lambda) and rigidity (mu). For the Earth, typical values are lambda=mu=30 GPa. All physical quantities are assumed in SI units (meter, Pascal, second).

**time interval** :   Refers to the duration of the calculation in SI units (s), for example 3.15e7 for one year.

**number of rectangle patches** :   The number of rectangle fault patches. If the number of patches is positive, the input file must be followed by a list of patch properties

    # n Vl x1 x2 x3 length width strike dip rake

where Vl is the loading rate of the fault, for example 1e-9 m/s. For rake=0 and Vl>0 the fault is a left-lateral strike-slip. For rake=90, the fault is a thrust if the 0<dip<90.

**number of friction properties** :   This must be the number of rectangle patches. If the number of patches is positive, the input file must be followed by a list of frictional properties

    # n tau0 mu0 sig a b L Vo G/(2Vs)

where tau0 is the initial stress, mu0 and sig are the static coefficient of friction and the effective confining pressure, a and b are the dynamic friction coefficient of rate-and-state friction, Vo is the reference velocity and G/(2Vs) is the radiation damping coefficient. When tau0\<0, the initial stress is set to the value that makes the fault slip at the velocity Vl.

**number of triangle patches** :   The number of triangle fault patches. If the number of patches is positive, the input file must be followed by a list of patch properties

    # n Vl i1 i2 i3 rake

where i1, i2, and i3 are the indices of the vertices. This must be followed by the number of vertices

    # number of vertices

and a list of coordinates

    # n x1 x2 x3

**number of friction properties** :   This must be the number of triangle patches. If the number of triangle patches is positive, the input file must be followed by a list of frictional properties

    # n tau0 mu0 sig a b L Vo G/(2Vs)

where tau0 is the initial stress, mu0 and sig are the static coefficient of friction and the effective confining pressure, a and b are the dynamic friction coefficient of rate-and-state friction, Vo is the reference velocity and G/(2Vs) is the radiation damping coefficient. When tau0\<0, the initial stress is set to the value that makes the fault slip at the velocity Vl.

**number of observation patches** :   The number of patch elements that will be monitored during the calculation. For these patches, the time series of dynamic variables and their time derivatives will be exported in wdir/patch-0000001.dat, where 0000001 will be substituted with the patch number. These time series will include time, slip components, traction components, state variables, the log10 of the instantaneous velocity. The following columns of the file will contain the time derivatives of these variables. If the number is positive, this must be followed by

    # n index rate

where "index" is the index of the patch and \"rate\" is the sampling rate. A sampling rate of 1 exports all time steps.

**number of observation profiles** :   The number of observation profiles where the slip velocity is exported as time series in GMT-compatible netcdf .grd files. If the number is positive, it must be followed by:

    # m n offset stride rate

where m is the profile number, n is the number of points in the profile, offset in the initial offset in the index, stride is the index jump between consecutive samples, and rate is the sampling rate. Changing the offset and stride allows arbitrary numbers of horizontal and vertical profiles along or across the fault.

**number of observation images** :   The number of observation images to capture snapshots of fault evolution. For a single fault, a single observation image is necessary. More observation images can be used to sample different fault segments. If the number of observation images is positive, it must be followed by:

    # n nl nw offset stride rate

where n is the image number, nl and nw are the number of samples in the length and width direction of the fault, stride is the offset between consecutive samples, and rate is the sampling rate. Observation images are exported in GMT-compatible netcdf .grd files.

**number of observation points** :   The number of observation points where the displacement is exported as time series in ASCII files. If the number is positive, it must be followed by:

    # n NAME x1 x2 x3

where n is an index running starting at 1, and x1, x2 and x3 are the point coordinates. The time series of displacement at these points are written to files of the form opts-0001.dat.

# EXAMPLE INPUTS

The lines starting with the '#' symbol are commented.

CALLING SEQUENCE :   

    mpirun -n 4 unicycle-3d-ratestate input.dat

# FAULT GEOMETRY

Rectangle fault patches are defined in terms of position (x1,x2,x3), orientation (strike and dip), and dimension (length and width), as illustrated in the following figure. For positive slip, a zero rake corresponds to left-lateral strike-slip motion and a 90 degree rake corresponds to a thrust motion (when a positive dip is smaller than 90 degrees).

                N (x1)
                /
               /| strike
    x1,x2,x3 ->@--------------------------    E (x2)
               |\          .              \ w
               :-\        .                \ i
               |  \    l .                  \ d
               :90 \  V .                    \ t
               |-dip\  .                      \ h
               :     \. | Rake                 \
               |      --------------------------
               :             l e n g t h
             Z (x3)

Fault structures can be described as a combination of rectangle patches, for example:


    # number of rectangle patches
    4
    #  n   Vl x1   x2   x3 length width strike dip rake
       1 1e-9  0    0    0  1.0e3 2.0e3      0  90    0
       2 1e-9  0  1e3    0  1.0e3 2.0e3      0  90    0
       3 1e-9  0    0  2e3  1.0e3 2.0e3      0  90    0
       4 1e-9  0  1e3  2e3  1.0e3 2.0e3      0  90    0

# PHYSICAL UNITS

All physical quantities are assumed to be in SI units (meter, Pascal, second). A good practice is to use MPa instead of Pa for the confining pressure and the effective viscosity.

# REFERENCES

Barbot S., "Modulation of fault strength during the seismic cycle by grain-size evolution around contact junctions", Tectonophysics, https://doi.org/10.1016/j.tecto.2019.05.004, 2019a.

Barbot S., "Slow-slip, slow earthquakes, period-two cycles, full and partial ruptures, and deterministic chaos in a single asperity fault", Tectonophysics, https://doi.org/10.1016/j.tecto.2019.228171, 2019b.

# BENCHMARKS

Jiang J., Erickson B.A., Lambert V.R., Ampuero J.P., Ando R., Barbot S.D., Cattania C., Zilio L.D., Duan B., Dunham E.M. and Gabriel A.A., "Community‐driven code comparisons for three‐dimensional dynamic modeling of sequences of earthquakes and aseismic slip". J. Geophys. Res., https://dx.doi.org/10.1029/2021JB023519, 2022.

# AUTHOR

Sylvain Barbot (sbarbot@usc.edu)

                  Uni-,,..__sCycle
                 =QWWWQUAAAKEWW@?`
                  ?9RWYESSWUT?^
                        Q;
                       .C;
                        O;                          _                  _
                        M;           _   _  __   _ (_)  ___ _   _  ___| | ___
                       .U;          | | | ||   \| || | / __| | | |/ __| |/ _ \
                        T;          | |_| || |\ ' || || (__| |_| | (__| |  __/
                        E;           \___/ |_| \__||_| \___|\__, |\___|_|\___|
                        Q;                                  |___/
                        Q;  
                        Q;
                      ._Q;_.
                 _awgQWRRRWWmya,.
              .amWT!^-  Q` -~<9Wmw,
            .aQD>`     .W;      -9Qw,
           _m@(         W;        <$mc
          _QD`          W;          4Wc
         _QD`           T;           4Wc
         dQ`            T;   ___-__   $Q.
        _QF             T;  .oU!/     ]W[
        ]Q[             T;_u!`        -Qk
        ]Q(            <O!^.           Qk
        ]Q[         _u?^              .Qk
        -Qk      _a?.                 ]Q[
         4Q, -!!?T!-                 .m@
         -Qm,                        jQ(
          -Qm,                     .wQ[
           -$Qc                   _m@^
             ?$ma.              <yQP`
               ?VQwa,,.   .__awQ@?
                 -??$QRRRWQWBT!~
                       ---

# COPYRIGHT

UNICYCLE is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

UNICYCLE is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along
with UNICYCLE. If not, see \<http://www.gnu.org/licenses/\>.
