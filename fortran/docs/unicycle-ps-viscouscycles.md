---
Date: 4 Aug 2022
Title: unicycle-ps-viscouscycles documentation
---

# NAME

**unicycle-ap-viscouscycles** - Evaluates the deformation due to fault slip and viscoelastic flow using the integral method within the two-dimensional in-plane strain approximation.

# SYNOPSIS

unicycle-ps-viscouscycles [-h] [--dry-run] [--help]
[--export-netcdf] [--export-greens dir] [--epsilon]
[--friction-law ( 1 | 2 | 3 | 4)] [--maximum-step]
[--maximum-iterations] [--version]

# DESCRIPTION

`unicycle-ps-viscouscycles` computes the evolution of slip on rate- and state-dependent friction faults coupled to viscoelastic flow with a nonlinear (power-law) rheology in the bulk rocks with the two-dimensional in-plane strain approximation. Volume discretization is based on rectangle and triangle volume elements.

# OPTIONS

`-h` print a short message and abort calculation

`--dry-run` write lightweight information files and abort calculation

`--help` print a short message and abort calculation

`--export-greens dir` export the Greens function to a GMT compatible netcdf file in directory dir

`--export-netcdf` export time series of instantaneous velocity in a GMT compatible netcdf file

`--export-volume` export snapshots of strain-rate in volume elements in GMT compatible .xy files

`--epsilon [1e-6]` set the relative accuracy of the 4/5th order Runge-Kutta integration method

`--friction-law ( 1 | 2 | 3)` select the type of rate- and state-dependent friction law [default: 1]
  * 1: multiplicative form (Barbot, 2019a)
  * 2: additive form (Ruina, 1983)
  * 3: arcsinh form (Rice & Benzion, 1996)

**\--maximum-step [Inf]** :   set the maximum time step

**\--maximum-iterations [1000000]** :   set the maximum number of iterations

# ENVIRONMENT

The calculation is parallelized with MPI. Calling the programs with

    unicycle-ps-viscouscycles

is equivalent to using

    mpirun -n 1 unicycle-ps-viscouscycles

# INPUT PARAMETERS

**output directory (wdir)** :   All output files are written to the specified directory, including observation patches, observation volumes observation points, and netcdf files.

**rigidity and Lame parameter** :   The uniform rigidity (mu) and Lame parameter (lambda). For the Earth, typical values are 30 GPa. All physical quantities are assumed in SI units (meter, Pascal, second).

**time interval** :   Refers to the duration of the calculation in SI units (s), for example 3.15e7 for one year.

**number of patches** :   The number of fault segments. If the number of patches is positive, the input file must be followed by a list of patch properties

    # n Vl x2 x3 width dip

where Vl is the loading rate of the fault, for example 1e-9 m/s.

**number of friction properties** :   This must be the number of patches. If the number of patches is positive, the input file must be followed by a list of frictional properties

    # n tau0 mu0 sig a b L Vo G/(2Vs)

where tau0 is the initial stress, mu0 and sig are the static coefficient
of friction and the effective confining pressure, a and b are the
friction coefficients for the rate and state dependence, Vo is the
reference velocity and G/(2Vs) is the radiation damping coefficient.
When tau0\<0, the initial stress is set to the value that makes the
fault slip at the velocity Vl.

**number of rectangle volume elements** :   The number of rectangle volume elements. If the number of volume elements is positive, the input file must be followed by a list of properties, as follows

    # n e22 e23 e33 x2 x3 thickness width dip

where the eij are the tensor components of the loading rate in (1/s). The dimension and orientation of volume elements is defined by the thickness, width, and dip angle. Current implementation requires dip=90.

**number of nonlinear Maxwell volume elements** :   The number of nonlinear Maxwell viscoelastic properties. This must be the number of volume elements. This must be followed by a list of rheological properties for nonlinear viscoelastic flow, as follows:

    # n sII gammadot0m nm

where n is a counter starting from 1 and sII is the background stress. The parameters gammadot0m and nm define a flow law of the form d Em / dt
= gammadot0m tau\^nm. The temperature, water fugacity, and grain-size dependency must be incorporated in the reference strain-rate gammadot0m. If sII is negative, the background stress is initialized with sII=(e/gammadot0m)\^(1/nm), where e is the norm of the background
strain-rate tensor, and sij=sII\*eij/e. The suffix m is for Maxwell.

**number of nonlinear Kelvin volume elements** :   The number of nonlinear Kelvin viscoelastic properties. This must be the number of rectangle volume elements or zero. This must be followed by a list of rheological properties for nonlinear viscoelastic flow, as follows:

    # n gammadot0k Gk nk

where n is a counter from 1 to the number of elements. The parameters gammadot0k and nk define a flow law of the form d Ek / dt = gammadot0k (tau - Gk Ek)\^nk. The temperature, water fugacity, and grain-size dependency must be incorporated in the reference strain-rate gammadot0m. The Kelvin strain is always initialized with Ek = tau / Gk.

**number of triangle volume elements** :   The number of triangle volume elements. If the number of triangle volume elements is positive, the input file must be followed by a list of properties, as follows

    # n e22 e23 e33 i1 i2 i3

where the eij are the tensor components of the loading rate in (1/s).
The integers i1, i2, and i3 refer to the indices of three vertices of
coordinates defined below.

**number of vertices** :   The number of points forming the mesh of triangle elements. If the number is positive, it must be followed by a list of coordinates, as follows:

    # n x2 x3

where x2 and x3 are the east and depth coordinates of the points. The
counter n is used to build the triangle mesh for the indices i1, i2, and
i3 above.

**number of nonlinear Maxwell volume elements** :   The number of nonlinear Maxwell viscoelastic properties for the triangle volume elements. This must be the number of triangle volume elements. This must be followed by a list of rheological properties for nonlinear viscoelastic flow, as follows:

    # n sII gammadot0m nm

where n is a counter starting from 1 and sII is the background stress. The parameters gammadot0m and nm define a flow law of the form d Em / dt
= gammadot0m tau\^nm. The temperature, water fugacity, and grain-size dependency must be incorporated in the reference strain-rate gammadot0m. If sII is negative, the background stress is initialized with sII=(e/gammadot0m)\^(1/nm), where e is the norm of the background
strain-rate tensor, and sij=sII\*eij/e. The suffix m is for Maxwell.

**number of nonlinear Kelvin volume elements** :   The number of nonlinear Kelvin viscoelastic properties. This must be the number of triangle volume elements or zero. This must be followed by a list of rheological properties for nonlinear viscoelastic flow, as follows:

    # n gammadot0k Gk nk

where n is a counter from 1 to the number of elements. The parameters gammadot0k and nk define a flow law of the form d Ek / dt = gammadot0k (tau - Gk Ek)\^nk. The temperature, water fugacity, and grain-size dependency must be incorporated in the reference strain-rate gammadot0m. The Kelvin strain is always initialized with Ek = tau / Gk.

**number of observation patches** :   The number of patch elements that will be monitored during the calculation. For these patches, the time series of dynamic variables and their time derivatives will be exported in wdir/patch-0000000n-000index.dat, where the first number will be substituted with the counter and the last number will be substituted with the patch number. These time series will include the slip, shear traction, state variable, log10 of the instantaneous velocity. The following columns of the file will contain the time derivatives of these variables. If the number of observation patches is positive, this must be followed by

    # n index rate

where \"index\" is the index of the patch and rate is the sampling rate. A sampling rate of 1 exports all time steps.

**number of observation volumes** :   The number of volume elements that will be monitored during the calculation. For these volumes, the time series of dynamic variables and their time derivatives will be exported in wdir/volume-0000000n-000index.dat, where the first number is the counter and the second one is the volume index. These time series will include the strain components and the stress components. The following columns of the file will contain the time derivatives of these variables. If the number is positive, this must be following by

    # n index rate

where \"index\" is the index of the strain volume and rate is the sampling rate. A sampling rate of 1 exports all time steps.

**number of observation points** :   The number of observation points where the displacement is exported as time series in ASCII files. If the number is positive, it must be followed by:

    # n name x2 x3

where n is a counter starting at 1, and x2 and x3 are the point
coordinates. The time series of displacement at these points are written
to file opts-name.dat.

**number of events** :   Must be zero (not implemented).

# EXAMPLE INPUTS

The lines starting with the \'#\' symbol are commented.

CALLING SEQUENCE :

    mpirun -n 4 unicycle-ap-viscouscycles input.dat

# FAULT GEOMETRY

Fault patches are defined in terms of position (x2,x3) and dimension
(width), as illustrated in the following figure:

    @---------------->   E (x2)
    |
    :    x2,x3
    |      + -
    :    w  \| dip
    |     i  \
    :      d  \
    |       t  \
    :        h  \
    Z (x3)       +

Fault structures can be described as a combination of rectangular patches, for example:   

    # number of patches
    4
    #  n   Vl  x2   x3 width dip
       1 1e-9   0    0 2.0e3  90
       2 1e-9 1e3    0 2.0e3  90
       3 1e-9   0  2e3 2.0e3  90
       4 1e-9 1e3  2e3 2.0e3  90

# VOLUME ELEMENTS

Rectangle volume elements are defined in terms of position (x2,x3), dimension (thickness and width), and orientation (dip angle), as
illustrated in the following figure:

    @---------------->   E (x2)
    |
    :            x2,x3
    |        +-----+-----+
    :      w |           |
    |      i |           |
    :      d |           |
    |      t |           |
    :      h |           |
    |        +-----------+
    :          thickness
    Z (x3)

In the current implementation, only dip=90 is allowed.

The input can be defined as follows:

    # number of volume elements
    1
    #  n   e12 e13 x2  x3 thickness width dip
       1 1e-15   0  0 5e4       1e3   1e3  90

Triangle volume elements are defined in terms of the position of the vertices within a mesh (x2,x3), and a triplet of indices i1, i2, and i3 defining the vertices of any single triangle, as illustrated in the following figure:

    @---------------->   E (x2)
    |
    :        1          2          3
    |        +----------+----------+
    :        |\         |         /|
    |        |  \       |       /  |
    :        |    \     |     /    |
    |        |      \   |   /      |
    :        |        \ | /        |
    |        +----------+----------+
    :        4          5          6
    Z (x3)

The mesh is formed as follows:

    # n e22 e23 e33 i1 i2 i3
      1   0   1   0  1  4  5
      2   0   1   0  1  2  5
      3   0   1   0  2  3  5
      4   0   1   0  3  5  6

# PHYSICAL UNITS

All physical quantities are assumed to be in SI units (meter, Pascal, second). A good practice is to use MPa instead of Pa for the effective normal stress and the effective viscosity.

# REFERENCES

Barbot, S., "Asthenosphere flow modulated by megathrust earthquake
cycles". Geophysical Research Letters, 45(12), pp.6018-6031, https://doi.org/10.1029/2018GL078197, 2018.

Barbot S., "Deformation of a Half-Space from Anelastic Strain Confined in a Tetrahedral Volume", Bull. Seism. Soc. Am., https://doi.org/10.1785/0120180058,
2018.

Barbot S., "Slow-slip, slow earthquakes, period-two cycles, full and partial ruptures, and deterministic chaos in a single asperity fault", Tectonophysics, https://doi.org/j.tecto.2019.228171, 2019.

Barbot, S., "Frictional and structural controls of seismic super-cycles at the Japan trench". Earth, Planets and Space, 72(1), pp.1-25, https://doi.org/10.1186/s40623-020-01185-3, 2020.

# BUGS

The dip angles of rectangle volume elements must be 90 degrees.

# AUTHOR

Sylvain Barbot (sbarbot@usc.edu)

                            _                  _
    _O       _   _  __   _ (_)  ___ _   _  ___| | ___
    (`      | | | ||   \| || | / __| | | |/ __| |/ _ \
    |>      | |_| || |\ ' || || (__| |_| | (__| |  __/
    ,|.      \___/ |_| \__||_| \___|\__, |\___|_|\___|
    `-'                             |___/

# COPYRIGHT

UNICYCLE is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

UNICYCLE is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with UNICYCLE. If not, see \<http://www.gnu.org/licenses/\>.
