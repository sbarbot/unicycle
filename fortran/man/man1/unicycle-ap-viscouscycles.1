.\" Manpage for Unicycle 1.0.0.
.\" Contact sbarbot@usc.edu to correct errors or typos.
.TH man 1 "14 Jul 2017" "1.0.0" "unicycle man page"
.SH NAME
unicycle-ap-viscouscycles \- Unified Cycle of Earthquakes. Evaluates the deformation due to fault slip and viscoelastic flow using the integral method.
.SH SYNOPSIS

unicycle-ap-viscouscycles [-h] [--dry-run] [--help] [--export-netcdf] [--export-volume] [--export-greens dir] [--epsilon] [--friction-law ( 1 | 2 | 3 | 4)] [--maximum-step] [--maximum-iterations] [--version]

.SH DESCRIPTION

unicycle-ap-viscouscycles computes the evolution of slip on rate- and state-dependent friction faults coupled to viscoelastic flow with a nonlinear (power-law) rheology in the bulk rocks in condition of anti-plane strain.

.SH OPTIONS

.TP
.B \-h
print a short message and abort calculation
.TP
.B \-\-dry-run
write lightweight information files and abort calculation
.TP
.B \-\-help
print a short message and abort calculation
.TP
.B \-\-export-greens dir
export the Greens function to a GMT compatible netcdf file in directory dir
.TP
.B \-\-export-netcdf
export time series of instantaneous velocity in a GMT compatible netcdf file
.TP
.B \-\-export-volume
export snapshots of all the volume elements in GMT compatible .xy files
.TP
.B \-\-epsilon [1e-6]
set the relative accuracy of the 4/5th order Runge-Kutta integration method
.TP
.B \-\-friction-law ( 1 | 2 | 3 | 4)
select the type of friction law [default: 1]
       1: multiplicative   form of rate-state friction (Barbot, 2019)
       2: additive         form of rate-state friction (Ruina, 1983)
       3: arcsinh          form of rate-state friction (Rice & Benzion, 1996)
       4: cut-off velocity form of rate-state friction (Okubo, 1989)
.TP
.B \-\-maximum-step [Inf]
set the maximum time step
.TP
.B \-\-maximum-iterations [1000000]
set the maximum number of iterations

.SH ENVIRONMENT

The calculation is parallelized with MPI. Calling the programs with

.nf
unicycle-ap-viscouscycles
.fi

is equivalent to using

.nf
mpirun -n 1 unicycle-ap-viscouscycles
.fi


.SH "INPUT PARAMETERS"

.TP
.B output directory (wdir)
All output files are written to the specified directory, including observation patches, observation volumes, observation points, and netcdf files.

.TP
.B rigidity
The uniform rigidity (mu). For the Earth, a typical value is 30 GPa. All physical quantities are assumed in SI units (meter, Pascal, second).

.TP
.B time interval
Refers to the duration of the calculation in SI units (s), for example 3.15e7 for one year.

.TP
.B number of patches
The number of fault segments. If the number of patches is positive, the input file must be followed by a list of patch properties

# n     Vl     x2     x3     width     dip

where Vl is the loading rate of the fault, for example 1e-9 m/s. 

.TP
.B number of friction properties
This must be the number of patches. If the number of patches is positive, the input file must be followed by a list of frictional properties

# n   tau0   mu0   sig    a    b    L    Vo    G/(2Vs)

where tau0 is the initial stress, mu0 and sig are the static coefficient of frictiona and the effective confining pressure, a and b are the friction coefficients for the rate and state dependence, Vo is the reference velocity and G/(2Vs) is the radiation damping coefficient. When tau0<0, the initial stress is set to the value that makes the fault slip at the velocity Vl. For the definition of the friction law, see Barbot (2019).

.TP
.B number of rectangle volume elements
The number of volume elements. If the number of volume elements is positive, the input file must be followed by a list of properties, as follows

# n    e12     e13     x2   x3  thickness width dip

where the eij are the tensor components of the loading rate in (1/s).  The dimension and orientation of volume elements is defined by the thickness, width, and dip angle. Current implementation requires dip=90.

.TP
.B number of nonlinear Maxwell volume elements
The number of nonlinear Maxwell viscoelastic properties. This must be the number of volume elements. This must be followed by a list of rheological properties for nonlinear viscoelastic flow, as follows:

# n       sII gammadot0m        nm

where the first n is a counter starting from 1 and sII is the initial stress. gammadot0m, and nm define a flow law of the form d E / dt = gammadot0m tau\^n. The temperature, water fugacity, and grain-size dependency must be incorporated in the reference strain-rate gammadot0m. If sII is negative, the initial stress is initialized with sij = (e/gammadotm)^(1/nm)*e/eij, where e is the background strain-rate and eij are the individual strain-rate components.

.TP
.B number of nonlinear Kelvin volume elements
The number of nonlinear Kelvin viscoelastic properties. This must be the number of volume elements or zero. This must be followed by a list of rheological properties for nonlinear viscoelastic flow in the Kelvin element, as follows:

# n        Gk gammadot0k        nk

where the first n is a counter starting from 1, Gk is the work-hardening coefficient. The parameters gammadot0l, and nk define a flow law of the form d Ek / dt = gammadot0k (tau-Gk Ek)^nk. The temperature, water fugacity, and grain-size dependency must be incorporated in the reference strain-rate gammadot0k. See Masuti et al. (2016) and Masuti & Barbot (2021).

.TP
.B number of observation patches
The number of patch elements that will be monitored during the calculation. For these patches, the time series of dynamic variables and their time derivatives will be exported in wdir/patch-0000000n-000index.dat, where the first number will be substituted with the counter and the last number will be substituted with the patch number. These time series will include the slip, shear traction, state variable, log10 of the instantaneous velocity. The following columns of the file will contain the time derivatives of these variables. If the number of observation patches is positive, this must be followed by

# n   index  rate

where "index" is the index of the patch and rate is the sampling rate. A sampling rate of 1 exports all time steps.

.TP
.B number of observation volumes
The number of volume elements that will be monitored during the calculation. For these volumes, the time series of dynamic variables and their time derivatives will be exported in wdir/volume-0000000n-000index.dat, where the first number is the counter and the second one is the volume index. These time series will include the strain components and the stress components. The following columns of the file will contain the time derivatives of these variables. If the number is positive, this must be following by

# n   index  rate

where "index" is the index of the strain volume and rate is the sampling rate. A sampling rate of 1 exports all time steps.

.TP
.B number of observation points
The number of observation points where the displacement is exported as time series in ASCII files. If the number is positive, it must be followed by:

# n name   x2   x3

where n is a counter starting at 1, and x2 and x3 are the point coordinates. The time series of displacement at these points are written to file opts-name.dat.

.SH "EXAMPLE INPUTS"

The lines starting with the '#' symbol are commented.

.IP "CALLING SEQUENCE"

mpirun -n 4 unicycle-ap-viscouscycles input.dat

.SH "FAULT GEOMETRY"

Fault patches are defined in terms of position (x2,x3) and dimension (width), as illustrated in the following figure:

            @---------------->   E (x2)
            |
            :    x2,x3
            |      + - 
            :    w  \\| dip
            |     i  \\
            :      d  \\ 
            |       t  \\ 
            :        h  \\
            Z (x3)       +

.TP
Fault structures can be described as a combination of rectnagular patches, for example:

.nf
# number of patches
4
#  n   Vl  x2   x3 width dip
   1 1e-9   0    0 2.0e3  90
   2 1e-9 1e3    0 2.0e3  90
   3 1e-9   0  2e3 2.0e3  90
   4 1e-9 1e3  2e3 2.0e3  90
.fi

.SH "VOLUME ELEMENTS"

Volume elements are defined in terms of position (x2,x3), dimension (thickness and width), and orientation (dip angle), as illustrated in the following figure:

            @---------------->   E (x2)
            |
            :            x2,x3
            |        +-----+-----+
            :      w |           |
            |      i |           |
            :      d |           |
            |      t |           |
            :      h |           |
            |        +-----------+
            :          thickness
            Z (x3)

In the current implementation, only dip=90 is allowed.

.TP
The input can be defined as follows:

.nf
# number of volume elements
1
#  n   e12 e13 x2  x3 thickness width dip
   1 1e-15   0  0 5e4       1e3   1e3  90
.fi

.SH "PHYSICAL UNITS"

All physical quantities are assumed to be in SI units (meter, Pascal, second). A good practice is to use MPa instead of Pa for the effective normal stress and the effective viscosity.

.SH "REFERENCES"

Lambert, V., and S. Barbot. "Contribution of viscoelastic flow in earthquake cycles within the lithosphere‐asthenosphere system." Geophys. Res. Lett. 43.19 (2016).

Barbot S., J. D.-P. Moore, and V. Lambert, "Displacement and Stress Associated with Distributed Anelastic Deformation in a Half-Space", Bull. Seism. Soc. Am., 10.1785/0120160237, 2017.

Barbot S., "Modulation of fault strength during the seismic cycle by grain-size evolution around contact junctions", Tectonophysics, 10.1016/j.tecto.2019.05.004, 2019.

Masuti S., Barbot S.D., Karato S.I., Feng L. and Banerjee P., "Upper-mantle water stratification inferred from observations of the 2012 Indian Ocean earthquake". Nature, 538(7625), pp.373-377, 10.1038/nature19783, 2016.

Masuti S. and Barbot S., MCMC inversion of the transient and steady-state creep flow law parameters of dunite under dry and wet conditions. Earth, Planets and Space, 73(1), pp.1-21, 10.1186/s40623-021-01543-9, 2021.

.SH BUGS
The dip angles of rectangle volume elements must be 90 degrees.

.SH AUTHOR
Sylvain Barbot (sbarbot@usc.edu)

.SH COPYRIGHT

UNICYCLE is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

UNICYCLE is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with UNICYCLE.  If not, see <http://www.gnu.org/licenses/>.
