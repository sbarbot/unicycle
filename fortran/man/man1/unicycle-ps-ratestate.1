.\" Manpage for unicycle-ps-ratestate 1.0.0.
.\" Contact sbarbot@usc.edu to correct errors or typos.
.TH man 1 "17 Jul 2022" "1.0.0" "unicycle man page"
.SH NAME
unicycle-ps-ratestate \- Unified Cycle of Earthquakes for in-plane strain. Evaluates the deformation due to fault slip using the boundary integral method.
.SH SYNOPSIS

unicycle-ps-ratestate [-h] [--dry-run] [--help] [--export-netcdf] [--export-greens dir] [--epsilon] [--friction-law ( 1 | 2 | 3 | 4)] [--import-greens dir] [--maximum-step] [--maximum-iterations] [--version]

.SH DESCRIPTION

unicycle-ps-ratestate computes the evolution of slip on rate- and state-dependent friction faults in isothermal condition in an elastic half-space.

.SH OPTIONS

.TP
.B \-h
print a short message and abort calculation
.TP
.B \-\-dry-run
write lightweight information files and abort calculation
.TP
.B \-\-help
print a short message and abort calculation
.TP
.B \-\-export-greens dir
export the Greens function to a GMT compatible netcdf file in directory dir
.TP
.B \-\-export-netcdf
export time series of instantaneous velocity (log10(v)) in a GMT compatible netcdf file
.TP
.B \-\-export-netcdf-rate [20]
set the number of skipped time steps in netcdf file
.TP
.B \-\-export-netcdf-slip
export time series of cumulative slip in a GMT compatible netcdf file
.TP
.B \-\-export-netcdf-stress
export time series of shear stress in a GMT compatible netcdf file
.TP
.B \-\-epsilon [1e-6]
set the relative accuracy of the 4/5th order Runge-Kutta integration method
.TP
.B \-\-friction-law ( 1 | 2 | 3 | 4)
select the type of friction law [default: 1]
       1: multiplicative   form of rate-state friction (Barbot, 2019)
       2: additive         form of rate-state friction (Ruina, 1983)
       3: arcsinh          form of rate-state friction (Rice & Benzion, 1996)
       4: cut-off velocity form of rate-state friction (Okubo, 1989)
.TP
.B \-\-import-greens dir
import the Greens function from a GMT compatible netcdf file in directory dir
.TP
.B \-\-maximum-step [Inf]
set the maximum time step
.TP
.B \-\-maximum-iterations [1000000]
set the maximum number of iterations

.SH ENVIRONMENT

The calculation is parallelized with MPI. Calling the programs with

.nf
unicycle-ps-ratestate
.fi

is equivalent to using

.nf
mpirun -n 1 unicycle-ps-ratestate
.fi


.SH "INPUT PARAMETERS"

.TP
.B output directory (wdir)
All output files are written to the specified directory, including observation patches, observation points, and netcdf files.

.TP
.B Lame parameters
The uniform rigidity (mu) and the Lame parameter (lambda) in the elastic half-space. For the Earth, typical values are 30 GPa. All physical quantities are assumed in SI units (meter, Pascal, second).

.TP
.B time interval
Refers to the duration of the calculation in SI units (s), for example 3.15e7 for one year.

.TP
.B number of patches
The number of fault segments. If the number of patches is positive, the input file must be followed by a list of patch properties

# n  Vl   x2   x3  width  dip

where Vl is the loading rate of the fault, for example 1e-9 m/s, and the dip angle is in degrees. Use Vl>0 for thrust faults and Vl<0 for normal faults (assuming 0<=dip<=90.)

.TP
.B number of friction properties
This must be the number of patches. If the number of patches is positive, the input file must be followed by a list of frictional properties

# n  tau0  mu0  sig    a    b    L    Vo  G/(2Vs)

where tau0 is the initial stress, mu0 and sig are the static coefficient of friction and the effective confining pressure, a and b are the friction coefficient for the rate and state effects, Vo is the reference velocity, and G/(2Vs) is the radiation damping coefficient. When tau0<0, the initial stress is set to the value that makes the fault slip at the velocity Vl. For the definition of the friction law, see Barbot (2019a,b).

.TP
.B number of observation patches
The number of patch elements that will be monitored during the calculation. For these patches, the time series of dynamic variables and their time derivatives will be exported in wdir/patch-00000001.dat, where 00000001 will be substituted with the patch number. These time series will include slip, shear traction, state variable, and the log10 of the instantaneous velocity followed by the time derivative of these quantities. If the number of observation patches is positive, this must be followed by

# n   index  rate

where n is a counter, index is the index of the patch and rate is the sampling rate in time steps (integer). A sampling rate of 1 exports all computational time steps.

.TP
.B number of observation points
The number of observation points where the displacement is exported as time series in ASCII files. If the number is positive, it must be followed by:

# n  name    x2    x3

where n is a counter starting at 1, name is a 4-character name, and x2 and x3 are the point coordinates. The time series of displacement at these points are written to file opts-name.dat.

.SH "EXAMPLE INPUTS"

The lines starting with the '#' symbol are commented.

.IP "CALLING SEQUENCE"

mpirun -n 4 unicycle-ps-ratestate input.dat

.SH "FAULT GEOMETRY"

Fault patches are defined in terms of position (x2,x3), orientation (dip angle), and dimension (width), as illustrated in the following figure.

            @---------------->   E (x2)
            |
            :    x2,x3
            |      + - 
            :    w  \\| dip
            |     i  \\
            :      d  \\ 
            |       t  \\ 
            :        h  \\
            Z (x3)       +

.SH "PHYSICAL UNITS"

All physical quantities are assumed to be in SI units (meter, Pascal, second). A good practice is to use MPa instead of Pa for the effective normal stress.

.SH "REFERENCES"

Barbot S., "Modulation of fault strength during the seismic cycle by grain-size evolution around contact junctions", Tectonophysics, j.tecto.2019.05.004, 2019a.

Barbot S., "Slow-slip, slow earthquakes, period-two cycles, full and partial ruptures, and deterministic chaos in a single asperity fault", Tectonophysics, j.tecto.2019.228171, 2019b.

.SH AUTHOR
Sylvain Barbot (sbarbot@usc.edu)

.nf
         \\_o___
            \\ 
           __\\
           \\ /                    _                  _
           ~|\\     _   _  __   _ (_)  ___ _   _  ___| | ___
            |~    | | | ||   \\| || | / __| | | |/ __| |/ _ \\
            -     | |_| || |\\ ' || || (__| |_| | (__| |  __/
          ( * )    \\___/ |_| \\__||_| \\___|\\__, |\\___|_|\\___|
            -                             |___/
.fi

.SH COPYRIGHT

UNICYCLE is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

UNICYCLE is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with UNICYCLE.  If not, see <http://www.gnu.org/licenses/>.
