LIB=$(patsubst %,$(DST)/%, \
    getopt_m.o exportnetcdf.o rk.o gauss.o)

$(shell mkdir -p $(DST))

$(DST)/%.o: $(SRC)/%.f90
	$(COMPILE.f) $^ -o $(DST)/$*.o -J $(DST)

lib: $(LIB)

clean:
	$(RM) $(DST)/*.o $(DST)/*.mod

.NOTPARALLEL:

