MODULE gauss

  IMPLICIT NONE

CONTAINS

  SUBROUTINE gaussRule(order,x,w)
    IMPLICIT NONE
    INTEGER, INTENT(IN) :: order
    REAL*8, DIMENSION(order), INTENT(OUT) :: x,w

    REAL*8 :: alpha=0.0_8,beta=0.0_8

    CALL cgqf(order,1,alpha,beta,-1._8,1._8,x,w)

  END SUBROUTINE gaussRule

  !----------------------------------------------------------------------
  !! CDGQF computes a Gauss quadrature formula with default A, B and simple knots.
  !!
  !! This routine computes all the knots and weights of a Gauss quadrature
  !! formula with a classical weight function with default values for A and B,
  !! and only simple knots.
  !!
  !! There are no moments checks and no printing is done.
  !!
  !! Use routine EIQFS to evaluate a quadrature computed by CGQFS.
  !!
  !! Licensing:
  !! This code is distributed under the GNU LGPL license. 
  !!
  !! Modified: 04 January 2010
  !!
  !! Author:
  !!   Original FORTRAN77 version by Sylvan Elhay, Jaroslav Kautsky.
  !!   FORTRAN90 version by John Burkardt.
  !!
  !! Reference:
  !!   Sylvan Elhay, Jaroslav Kautsky,
  !!   Algorithm 655: IQPACK, FORTRAN Subroutines for the Weights of 
  !!   Interpolatory Quadrature,
  !!   ACM Transactions on Mathematical Software,
  !!   Volume 13, Number 4, December 1987, pages 399-415.
  !!
  !! Parameters:
  !!   Input, integer ( kind = 4 ) NT, the number of knots.
  !!
  !!   Input, integer ( kind = 4 ) KIND, the rule.
  !!   1, Legendre,             (a,b)       1.0
  !!   2, Chebyshev,            (a,b)       ((b-x)*(x-a))^(-0.5)
  !!   3, Gegenbauer,           (a,b)       ((b-x)*(x-a))^alpha
  !!   4, Jacobi,               (a,b)       (b-x)^alpha*(x-a)^beta
  !!   5, Generalized Laguerre, (a,inf)     (x-a)^alpha*exp(-b*(x-a))
  !!   6, Generalized Hermite,  (-inf,inf)  |x-a|^alpha*exp(-b*(x-a)^2)
  !!   7, Exponential,          (a,b)       |x-(a+b)/2.0|^alpha
  !!   8, Rational,             (a,inf)     (x-a)^alpha*(x+b)^beta
  !!
  !!   Input, real ( kind = 8 ) ALPHA, the value of Alpha, if needed.
  !!
  !!   Input, real ( kind = 8 ) BETA, the value of Beta, if needed.
  !!
  !!   Output, real ( kind = 8 ) T(NT), the knots.
  !!
  !!   Output, real ( kind = 8 ) WTS(NT), the weights.
  !----------------------------------------------------------------------
  SUBROUTINE cdgqf ( nt, kind, alpha, beta, t, wts )

    IMPLICIT NONE

    INTEGER ( kind = 4 ) nt

    REAL(KIND=8) aj(nt)
    REAL(KIND=8) alpha
    REAL(KIND=8) beta
    REAL(KIND=8) bj(nt)
    INTEGER(KIND=4) kind
    REAL(KIND=8) t(nt)
    REAL(KIND=8) wts(nt)
    REAL(KIND=8) zemu

    CALL parchk(kind,2*nt,alpha,beta)

    ! get the Jacobi matrix and zero-th moment.
    CALL class_matrix(kind,nt,alpha,beta,aj,bj,zemu)

    ! compute the knots and weights.
    CALL sgqf( nt, aj, bj, zemu, t, wts )

  END

  !----------------------------------------------------------------------
  !! CGQF computes knots and weights of a Gauss quadrature formula.
  !!
  !! The user may specify the interval (A,B).
  !!
  !! Only simple knots are produced.
  !!
  !! Use routine EIQFS to evaluate this quadrature formula.
  !!
  !! Licensing:
  !!    This code is distributed under the GNU LGPL license. 
  !!
  !!  Modified: 16 February 2010
  !!
  !!  Author:
  !!    Original FORTRAN77 version by Sylvan Elhay, Jaroslav Kautsky.
  !!    FORTRAN90 version by John Burkardt.
  !!
  !!  Reference:
  !!    Sylvan Elhay, Jaroslav Kautsky,
  !!    Algorithm 655: IQPACK, FORTRAN Subroutines for the Weights of 
  !!    Interpolatory Quadrature,
  !!    ACM Transactions on Mathematical Software,
  !!    Volume 13, Number 4, December 1987, pages 399-415.
  !!
  !!  Parameters:
  !!    Input, integer ( kind = 4 ) NT, the number of knots.
  !!
  !!    Input, integer ( kind = 4 ) KIND, the rule.
  !!    1, Legendre,             (a,b)       1.0
  !!    2, Chebyshev Type 1,     (a,b)       ((b-x)*(x-a))^-0.5)
  !!    3, Gegenbauer,           (a,b)       ((b-x)*(x-a))^alpha
  !!    4, Jacobi,               (a,b)       (b-x)^alpha*(x-a)^beta
  !!    5, Generalized Laguerre, (a,+oo)     (x-a)^alpha*exp(-b*(x-a))
  !!    6, Generalized Hermite,  (-oo,+oo)   |x-a|^alpha*exp(-b*(x-a)^2)
  !!    7, Exponential,          (a,b)       |x-(a+b)/2.0|^alpha
  !!    8, Rational,             (a,+oo)     (x-a)^alpha*(x+b)^beta
  !!    9, Chebyshev Type 2,     (a,b)       ((b-x)*(x-a))^(+0.5)
  !!
  !!    Input, real ( kind = 8 ) ALPHA, the value of Alpha, if needed.
  !!
  !!    Input, real ( kind = 8 ) BETA, the value of Beta, if needed.
  !!
  !!    Input, real ( kind = 8 ) A, B, the interval endpoints, or
  !!    other parameters.
  !!
  !!    Output, real ( kind = 8 ) T(NT), the knots.
  !!
  !!    Output, real ( kind = 8 ) WTS(NT), the weights.
  !----------------------------------------------------------------------
  SUBROUTINE cgqf(nt,kind,alpha,beta,a,b,t,wts)
    IMPLICIT NONE

    INTEGER ( KIND = 4 ) nt

    REAL(KIND=8) a
    REAL(KIND=8) alpha
    REAL(KIND=8) b
    REAL(KIND=8) beta
    INTEGER(KIND=4) i
    INTEGER(KIND=4) kind
    INTEGER(KIND=4), ALLOCATABLE :: mlt(:)
    INTEGER(KIND=4), ALLOCATABLE :: ndx(:)
    REAL(KIND=8) t(nt)
    REAL(KIND=8) wts(nt)

    ! compute the Gauss quadrature formula for default values of A and B.
    CALL cdgqf ( nt, kind, alpha, beta, t, wts )

    ! prepare to scale the quadrature formula to other weight function with 
    ! valid A and B.
    ALLOCATE(mlt(1:nt))

    mlt(1:nt) = 1

    ALLOCATE(ndx(1:nt))

    DO i=1,nt 
      ndx(i)=i
    END DO

    CALL scqf(nt,t,mlt,wts,nt,ndx,wts,t,kind,alpha,beta,a,b)

    DEALLOCATE(mlt)
    DEALLOCATE(ndx)
  END

  !----------------------------------------------------------------------
  !! CH_EQI is a case insensitive comparison of two characters for equality.  
  !!
  !!    CH_EQI ( 'A', 'a' ) is TRUE.
  !!
  !!  Licensing:
  !!    This code is distributed under the GNU LGPL license. 
  !!
  !!  Modified: 28 July 2000
  !!
  !!  Author:
  !!    John Burkardt
  !!
  !!  Parameters:
  !!    Input, character C1, C2, the characters to compare.
  !!    Output, logical CH_EQI, the result of the comparison.
  !----------------------------------------------------------------------
  FUNCTION ch_eqi ( c1, c2 )
    IMPLICIT NONE

    CHARACTER c1
    CHARACTER c1_cap
    CHARACTER c2
    CHARACTER c2_cap
    LOGICAL ch_eqi

    c1_cap = c1
    c2_cap = c2

    CALL ch_cap ( c1_cap )
    CALL ch_cap ( c2_cap )

    IF (c1_cap == c2_cap) THEN
      ch_eqi = .TRUE.
    ELSE
      ch_eqi = .FALSE.
    END IF

  END FUNCTION ch_eqi

  !----------------------------------------------------------------------
  !! CH_CAP capitalizes a single character.
  !!
  !! Instead of CHAR and ICHAR, we now use the ACHAR and IACHAR functions, 
  !! which guarantee the ASCII collating sequence.
  !!
  !! Licensing:
  !!    This code is distributed under the GNU LGPL license. 
  !!
  !!  Modified: 19 July 1998
  !!
  !!  Author:
  !!    John Burkardt
  !!
  !!  Parameters:
  !!    Input/output, character CH, the character to capitalize.
  !----------------------------------------------------------------------
  SUBROUTINE ch_cap ( ch )

    IMPLICIT NONE

    CHARACTER ch
    INTEGER (KIND=4) itemp

    itemp = iachar(ch)
 
    IF ( 97 <= itemp .and. itemp <= 122 ) then
      ch = achar ( itemp - 32 )
    END IF
  END

  !----------------------------------------------------------------------
  !! CH_TO_DIGIT returns the value of a base 10 digit.
  !! Instead of ICHAR, we now use the IACHAR function, which
  !! guarantees the ASCII collating sequence.
  !!
  !! Example:
  !!
  !!     CH  DIGIT
  !!    ---  -----
  !!    '0'    0
  !!    '1'    1
  !!    ...  ...
  !!    '9'    9
  !!    ' '    0
  !!    'X'   -1
  !!
  !! Licensing:
  !!    This code is distributed under the GNU LGPL license. 
  !!
  !! Modified: 04 August 1999
  !!
  !! Author:
  !!    John Burkardt
  !!
  !! Parameters:
  !!    Input, character CH, the decimal digit, '0' through '9' or blank
  !!    are legal. 
  !!
  !!    Output, integer ( kind = 4 ) DIGIT, the corresponding value.  
  !!    If CH was 'illegal', then DIGIT is -1.
  !----------------------------------------------------------------------
  SUBROUTINE ch_to_digit ( ch, digit )
    IMPLICIT NONE

    CHARACTER ch
    INTEGER (KIND=4) digit

    IF ( lle ( '0', ch ) .AND. lle ( ch, '9' ) ) THEN
      digit = iachar ( ch ) - 48
    ELSE IF ( ch == ' ' ) THEN
      digit = 0
    ELSE
      digit = -1
    END IF
  END

  !----------------------------------------------------------------------
  !! CLASS_MATRIX computes the Jacobi matrix for a quadrature rule.
  !!
  !! This routine computes the diagonal AJ and sub-diagonal BJ
  !! elements of the order M tridiagonal symmetric Jacobi matrix
  !! associated with the polynomials orthogonal with respect to
  !! the weight function specified by KIND.
  !!
  !! For weight functions 1-7, M elements are defined in BJ even
  !! though only M-1 are needed.  For weight function 8, BJ(M) is
  !! set to zero.
  !!
  !! The zero-th moment of the weight function is returned in ZEMU.
  !!
  !! Licensing:
  !!    This code is distributed under the GNU LGPL license. 
  !!
  !! Modified: 27 December 2009
  !!
  !! Author:
  !!    Original FORTRAN77 version by Sylvan Elhay, Jaroslav Kautsky.
  !!    FORTRAN90 version by John Burkardt.
  !!
  !! Reference:
  !!    Sylvan Elhay, Jaroslav Kautsky,
  !!    Algorithm 655: IQPACK, FORTRAN Subroutines for the Weights of 
  !!    Interpolatory Quadrature,
  !!    ACM Transactions on Mathematical Software,
  !!    Volume 13, Number 4, December 1987, pages 399-415.
  !!
  !! Parameters:
  !!    Input, integer ( kind = 4 ) KIND, the rule.
  !!    1, Legendre,             (a,b)       1.0
  !!    2, Chebyshev,            (a,b)       ((b-x)*(x-a))^(-0.5)
  !!    3, Gegenbauer,           (a,b)       ((b-x)*(x-a))^alpha
  !!    4, Jacobi,               (a,b)       (b-x)^alpha*(x-a)^beta
  !!    5, Generalized Laguerre, (a,inf)     (x-a)^alpha*exp(-b*(x-a))
  !!    6, Generalized Hermite,  (-inf,inf)  |x-a|^alpha*exp(-b*(x-a)^2)
  !!    7, Exponential,          (a,b)       |x-(a+b)/2.0|^alpha
  !!    8, Rational,             (a,inf)     (x-a)^alpha*(x+b)^beta
  !!
  !!    Input, integer ( kind = 4 ) M, the order of the Jacobi matrix.
  !!
  !!    Input, real ( kind = 8 ) ALPHA, the value of Alpha, if needed.
  !!
  !!    Input, real ( kind = 8 ) BETA, the value of Beta, if needed.
  !!
  !!    Output, real ( kind = 8 ) AJ(M), BJ(M), the diagonal and subdiagonal
  !!    of the Jacobi matrix.
  !!
  !!    Output, real ( kind = 8 ) ZEMU, the zero-th moment.
  !----------------------------------------------------------------------
  SUBROUTINE class_matrix ( kind, m, alpha, beta, aj, bj, zemu )
    IMPLICIT NONE

    INTEGER (KIND=4) m

    REAL(KIND=8) a2b2
    REAL(KIND=8) ab
    REAL(KIND=8) aba
    REAL(KIND=8) abi
    REAL(KIND=8) abj
    REAL(KIND=8) abti
    REAL(KIND=8) aj(m)
    REAL(KIND=8) alpha
    REAL(KIND=8) apone
    REAL(KIND=8) beta
    REAL(KIND=8) bj(m)
    INTEGER(KIND=4) i
    INTEGER(KIND=4) kind
    REAL(KIND=8), PARAMETER :: pi = 3.14159265358979323846264338327950D+00
    !REAL(KIND=8) r8_gamma
    REAL(KIND=8) temp
    REAL(KIND=8) temp2
    REAL(KIND=8) zemu

    temp = epsilon ( temp )

    CALL parchk ( kind, 2 * m - 1, alpha, beta )

    temp2 = r8_gamma ( 0.5D+00 )

    IF (500.0D+00*temp < ABS(temp2*temp2-pi)) THEN
      WRITE ( *, '(a)' ) ' '
      WRITE ( *, '(a)' ) 'CLASS_MATRIX - Fatal error!'
      WRITE ( *, '(a)' ) '  Gamma function does not match machine parameters.'
      STOP
    END IF

    IF (KIND==1) THEN
      ab = 0.0D+00
      zemu = 2.0D+00 / ( ab + 1.0D+00 )
      aj(1:m) = 0.0D+00
      DO i = 1, m
        abi = i + ab * mod ( i, 2 )
        abj = 2 * i + ab
        bj(i) = abi * abi / ( abj * abj - 1.0D+00 )
      END DO
      bj(1:m) =  sqrt ( bj(1:m) )
    ELSE IF (KIND==2) THEN
      zemu = pi
      aj(1:m) = 0.0D+00
      bj(1) =  sqrt ( 0.5D+00 )
      bj(2:m) = 0.5D+00
    ELSE IF (KIND==3) THEN
      ab = alpha * 2.0D+00
      zemu = 2.0D+00**( ab + 1.0D+00 ) * ( r8_gamma ( alpha + 1.0D+00 ) )**2 &
        / r8_gamma ( ab + 2.0D+00 )
      aj(1:m) = 0.0D+00
      bj(1) = 1.0D+00 / ( 2.0D+00 * alpha + 3.0D+00 )
      DO i = 2, m
        bj(i) = i * ( i + ab ) &
          / ( 4.0D+00 * ( i + alpha ) * ( i + alpha ) - 1.0D+00 )
      END DO
      bj(1:m) =  sqrt ( bj(1:m) )
    ELSE IF (KIND==4) THEN
      ab = alpha + beta
      abi = 2.0D+00 + ab
      zemu = 2.0D+00**( ab + 1.0D+00 ) * r8_gamma ( alpha + 1.0D+00 ) &
        * r8_gamma ( beta + 1.0D+00 ) / r8_gamma ( abi )
      aj(1) = ( beta - alpha ) / abi
      bj(1) = 4.0D+00 * ( 1.0 + alpha ) * ( 1.0D+00 + beta ) &
        / ( ( abi + 1.0D+00 ) * abi * abi )
      a2b2 = beta * beta - alpha * alpha

      DO i = 2, m
        abi = 2.0D+00 * i + ab
        aj(i) = a2b2 / ( ( abi - 2.0D+00 ) * abi )
        abi = abi * abi
        bj(i) = 4.0D+00 * i * ( i + alpha ) * ( i + beta ) * ( i + ab ) &
          / ( ( abi - 1.0D+00 ) * abi )
      END DO
      bj(1:m) =  sqrt ( bj(1:m) )
    ELSE IF (KIND==5) THEN
      zemu = r8_gamma ( alpha + 1.0D+00 )
      DO i = 1, m
        aj(i) = 2.0D+00 * i - 1.0D+00 + alpha
        bj(i) = i * ( i + alpha )
      END DO
      bj(1:m) =  sqrt ( bj(1:m) )
    ELSE IF (KIND==6) THEN
      zemu = r8_gamma ( ( alpha + 1.0D+00 ) / 2.0D+00 )
      aj(1:m) = 0.0D+00
      DO i = 1, m
        bj(i) = ( i + alpha * mod ( i, 2 ) ) / 2.0D+00
      END DO
      bj(1:m) =  sqrt ( bj(1:m) )
    ELSE IF (KIND == 7) THEN
      ab = alpha
      zemu = 2.0D+00 / ( ab + 1.0D+00 )
      aj(1:m) = 0.0D+00
      DO i = 1, m
        abi = i + ab * mod ( i, 2 )
        abj = 2 * i + ab
        bj(i) = abi * abi / ( abj * abj - 1.0D+00 )
      END DO
      bj(1:m) =  sqrt ( bj(1:m) )
    ELSE IF (KIND == 8) THEN
      ab = alpha + beta
      zemu = r8_gamma ( alpha + 1.0D+00 ) * r8_gamma ( - ( ab + 1.0D+00 ) ) &
        / r8_gamma ( - beta )
      apone = alpha + 1.0D+00
      aba = ab * apone
      aj(1) = - apone / ( ab + 2.0D+00 )
      bj(1) = - aj(1) * ( beta + 1.0D+00 ) / ( ab + 2.0D+00 ) / ( ab + 3.0D+00 )
      do i = 2, m
        abti = ab + 2.0D+00 * i
        aj(i) = aba + 2.0D+00 * ( ab + i ) * ( i - 1 )
        aj(i) = - aj(i) / abti / ( abti - 2.0D+00 )
      end do

      DO i = 2, m - 1
        abti = ab + 2.0D+00 * i
        bj(i) = i * ( alpha + i ) / ( abti - 1.0D+00 ) * ( beta + i ) &
          / ( abti * abti ) * ( ab + i ) / ( abti + 1.0D+00 )
      END DO

      bj(m) = 0.0D+00
      bj(1:m) =  sqrt ( bj(1:m) )
    END IF

  END

  !----------------------------------------------------------------------
  !! GET_UNIT returns a free FORTRAN unit number.
  !!
  !! A "free" FORTRAN unit number is an integer between 1 and 99 which
  !! is not currently associated with an I/O device.  A free FORTRAN unit
  !! number is needed in order to open a file with the OPEN command.
  !!
  !! If IUNIT = 0, then no free FORTRAN unit could be found, although
  !! all 99 units were checked (except for units 5, 6 and 9, which
  !! are commonly reserved for console I/O).
  !!
  !! Otherwise, IUNIT is an integer between 1 and 99, representing a
  !! free FORTRAN unit.  Note that GET_UNIT assumes that units 5 and 6
  !! are special, and will never return those values.
  !!
  !! Licensing:
  !!    This code is distributed under the GNU LGPL license. 
  !!
  !! Modified: 15 January 2008
  !!
  !! Author:
  !!    John Burkardt
  !!
  !! Parameters:
  !!    Output, integer ( kind = 4 ) IUNIT, the free unit number.
  !----------------------------------------------------------------------
  subroutine get_unit ( iunit )
    implicit none

    integer ( kind = 4 ) i
    integer ( kind = 4 ) ios
    integer ( kind = 4 ) iunit
    logical lopen
  
    iunit = 0

    do i = 1, 99

      if ( i /= 5 .and. i /= 6 .and. i /= 9 ) then

        inquire ( unit = i, opened = lopen, iostat = ios )

        if ( ios == 0 ) then
          if ( .not. lopen ) then
            iunit = i
            return
          end if
        end if
  
      end if
  
    end do
  
    return
  end

  !----------------------------------------------------------------------
  !! IMTQLX diagonalizes a symmetric tridiagonal matrix.
  !!
  !! This routine is a slightly modified version of the EISPACK routine to 
  !! perform the implicit QL algorithm on a symmetric tridiagonal matrix. 
  !!
  !! The authors thank the authors of EISPACK for permission to use this
  !! routine. 
  !!
  !! It has been modified to produce the product Q^t * Z, where Z is an input 
  !! vector and Q is the orthogonal matrix diagonalizing the input matrix.  
  !! The changes consist (essentially) of applying the orthogonal 
  !! transformations directly to Z as they are generated.
  !!
  !!  Licensing:
  !!    This code is distributed under the GNU LGPL license. 
  !!
  !!  Modified:
  !!    27 December 2009
  !!
  !!  Author:
  !!    Original FORTRAN77 version by Sylvan Elhay, Jaroslav Kautsky.
  !!    FORTRAN90 version by John Burkardt.
  !!
  !!  Reference:
  !!    Sylvan Elhay, Jaroslav Kautsky,
  !!    Algorithm 655: IQPACK, FORTRAN Subroutines for the Weights of 
  !!    Interpolatory Quadrature,
  !!    ACM Transactions on Mathematical Software,
  !!    Volume 13, Number 4, December 1987, pages 399-415.
  !!
  !!    Roger Martin, James Wilkinson,
  !!    The Implicit QL Algorithm,
  !!    Numerische Mathematik,
  !!    Volume 12, Number 5, December 1968, pages 377-383.
  !!
  !!  Parameters:
  !!    Input, integer ( kind = 4 ) N, the order of the matrix.
  !!
  !!    Input/output, real ( kind = 8 ) D(N), the diagonal entries of the matrix.
  !!    On output, the information in D has been overwritten.
  !!
  !!    Input/output, real ( kind = 8 ) E(N), the subdiagonal entries of the 
  !!    matrix, in entries E(1) through E(N-1).  On output, the information in
  !!    E has been overwritten.
  !!
  !!    Input/output, real ( kind = 8 ) Z(N).  On input, a vector.  On output,
  !!    the value of Q^t * Z, where Q is the matrix that diagonalizes the
  !!    input symmetric tridiagonal matrix.
  !----------------------------------------------------------------------
  SUBROUTINE imtqlx ( n, d, e, z )
    IMPLICIT NONE

    INTEGER ( kind = 4 ) n

    REAL(KIND=8) b
    REAL(KIND=8) c
    REAL(KIND=8) d(n)
    REAL(KIND=8) e(n)
    REAL(KIND=8) f
    REAL(KIND=8) g
    INTEGER(KIND=4) i
    INTEGER(KIND=4) ii
    INTEGER(KIND=4), parameter :: itn = 30
    INTEGER(KIND=4) j
    INTEGER(KIND=4) k
    INTEGER(KIND=4) l
    INTEGER(KIND=4) m
    INTEGER(KIND=4) mml
    REAL(KIND=8) p
    REAL(KIND=8) prec
    REAL(KIND=8) r
    REAL(KIND=8) s
    REAL(KIND=8) z(n)

    prec=EPSILON(prec)

    IF (n==1) THEN
      RETURN
    END IF

    e(n) = 0.0D+00

    DO l = 1, n
      j = 0

      DO
        DO m = l, n
          IF ( m == n ) THEN
            EXIT
          END IF

          IF (ABS(e(m)) <= prec*(ABS(d(m))+ABS(d(m+1)))) THEN
            EXIT
          END IF

        END DO

        p = d(l)

        IF (m ==l) THEN
          EXIT
        END IF

        IF ( itn <= j ) THEN
          WRITE ( *, '(a)' ) ' '
          WRITE ( *, '(a)' ) 'IMTQLX - Fatal error!'
          WRITE ( *, '(a)' ) '  Iteration limit exceeded.'
          WRITE ( *, '(a,i8)' ) '  J = ', j
          WRITE ( *, '(a,i8)' ) '  L = ', l
          WRITE ( *, '(a,i8)' ) '  M = ', m
          WRITE ( *, '(a,i8)' ) '  N = ', n
          STOP
        END IF

        j=j + 1
        g=( d(l+1) - p ) / ( 2.0D+00 * e(l) )
        r=SQRT(g*g+1.0D+00)
        g=d(m) - p + e(l) / ( g + sign ( r, g ) )
        s=1.0D+00
        c=1.0D+00
        p=0.0D+00
        mml = m - l

        DO ii = 1, mml

          i = m - ii
          f = s * e(i)
          b = c * e(i)

          IF (ABS(g) <= ABS(f)) THEN
            c=g/f
            r=SQRT( c * c + 1.0D+00 )
            e(i+1) = f*r
            s=1.0D+00/r
            c=c*s
          ELSE
            s = f / g
            r =  sqrt ( s * s + 1.0D+00 )
            e(i+1) = g * r
            c = 1.0D+00 / r
            s = s * c
          END IF

          g = d(i+1) - p
          r = ( d(i) - g ) * s + 2.0D+00 * c * b
          p = s * r
          d(i+1) = g + p
          g = c * r - b
          f = z(i+1)
          z(i+1) = s * z(i) + c * f
          z(i) = c * z(i) - s * f
        END DO

        d(l) = d(l) - p
        e(l) = g
        e(m) = 0.0D+00
      END DO

    END DO

    ! sorting
    DO ii = 2, n
      i = ii - 1
      k = i
      p = d(i)

      DO j = ii, n
        IF ( d(j) < p ) THEN
          k = j
          p = d(j)
        END IF
      END DO

      IF ( k /= i ) THEN
        d(k) = d(i)
        d(i) = p
        p = z(i)
        z(i) = z(k)
        z(k) = p
      END IF

    END DO

  END

!*****************************************************************************80
!
!! PARCHK checks parameters ALPHA and BETA for classical weight functions. 
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license. 
!
!  Modified:
!
!    27 December 2009
!
!  Author:
!
!    Original FORTRAN77 version by Sylvan Elhay, Jaroslav Kautsky.
!    FORTRAN90 version by John Burkardt.
!
!  Reference:
!
!    Sylvan Elhay, Jaroslav Kautsky,
!    Algorithm 655: IQPACK, FORTRAN Subroutines for the Weights of 
!    Interpolatory Quadrature,
!    ACM Transactions on Mathematical Software,
!    Volume 13, Number 4, December 1987, pages 399-415.
!
!  Parameters:
!
!    Input, integer ( kind = 4 ) KIND, the rule.
!    1, Legendre,             (a,b)       1.0
!    2, Chebyshev,            (a,b)       ((b-x)*(x-a))^(-0.5)
!    3, Gegenbauer,           (a,b)       ((b-x)*(x-a))^alpha
!    4, Jacobi,               (a,b)       (b-x)^alpha*(x-a)^beta
!    5, Generalized Laguerre, (a,inf)     (x-a)^alpha*exp(-b*(x-a))
!    6, Generalized Hermite,  (-inf,inf)  |x-a|^alpha*exp(-b*(x-a)^2)
!    7, Exponential,          (a,b)       |x-(a+b)/2.0|^alpha
!    8, Rational,             (a,inf)     (x-a)^alpha*(x+b)^beta
!
!    Input, integer ( kind = 4 ) M, the order of the highest moment to
!    be calculated.  This value is only needed when KIND = 8.
!
!    Input, real ( kind = 8 ) ALPHA, BETA, the parameters, if required
!    by the value of KIND.
!
subroutine parchk ( kind, m, alpha, beta )
  implicit none

  real ( kind = 8 ) alpha
  real ( kind = 8 ) beta
  integer ( kind = 4 ) kind
  integer ( kind = 4 ) m
  real ( kind = 8 ) tmp

  if ( kind <= 0 ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'PARCHK - Fatal error!'
    write ( *, '(a)' ) '  KIND <= 0.'
    stop
  end if
!
!  Check ALPHA for Gegenbauer, Jacobi, Laguerre, Hermite, Exponential.
!
  if ( 3 <= kind .and. alpha <= -1.0D+00 ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'PARCHK - Fatal error!'
    write ( *, '(a)' ) '  3 <= KIND and ALPHA <= -1.'
    stop
  end if
!
!  Check BETA for Jacobi.
!
  if ( kind == 4 .and. beta <= -1.0D+00 ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'PARCHK - Fatal error!'
    write ( *, '(a)' ) '  KIND == 4 and BETA <= -1.0.'
    stop
  end if
!
!  Check ALPHA and BETA for rational.
!
  if ( kind == 8 ) then
    tmp = alpha + beta + m + 1.0D+00
    if ( 0.0D+00 <= tmp .or. tmp <= beta ) then
      write ( *, '(a)' ) ' '
      write ( *, '(a)' ) 'PARCHK - Fatal error!'
      write ( *, '(a)' ) '  KIND == 8 but condition on ALPHA and BETA fails.'
      stop
    end if
  end if

  return
end

!*****************************************************************************80
!
!! R8_GAMMA evaluates Gamma(X) for a real argument.
!
!  Discussion:
!
!    This routine calculates the gamma function for a real argument X.
!
!    Computation is based on an algorithm outlined in reference 1.
!    The program uses rational functions that approximate the gamma
!    function to at least 20 significant decimal digits.  Coefficients
!    for the approximation over the interval (1,2) are unpublished.
!    Those for the approximation for 12 <= X are from reference 2.
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license. 
!
!  Modified:
!
!    11 February 2008
!
!  Author:
!
!    Original FORTRAN77 version by William Cody, Laura Stoltz.
!    FORTRAN90 version by John Burkardt.
!
!  Reference:
!
!    William Cody,
!    An Overview of Software Development for Special Functions,
!    in Numerical Analysis Dundee, 1975,
!    edited by GA Watson,
!    Lecture Notes in Mathematics 506,
!    Springer, 1976.
!
!    John Hart, Ward Cheney, Charles Lawson, Hans Maehly,
!    Charles Mesztenyi, John Rice, Henry Thatcher,
!    Christoph Witzgall,
!    Computer Approximations,
!    Wiley, 1968,
!    LC: QA297.C64.
!
!  Parameters:
!
!    Input, real ( kind = 8 ) X, the argument of the function.
!
!    Output, real ( kind = 8 ) R8_GAMMA, the value of the function.
!
function r8_gamma ( x )
  implicit none

  real ( kind = 8 ), dimension ( 7 ) :: c = (/ &
   -1.910444077728D-03, &
    8.4171387781295D-04, &
   -5.952379913043012D-04, &
    7.93650793500350248D-04, &
   -2.777777777777681622553D-03, &
    8.333333333333333331554247D-02, &
    5.7083835261D-03 /)
  real ( kind = 8 ), parameter :: eps = 2.22D-16
  real ( kind = 8 ) fact
  integer ( kind = 4 ) i
  integer ( kind = 4 ) n
  real ( kind = 8 ), dimension ( 8 ) :: p = (/ &
    -1.71618513886549492533811D+00, &
     2.47656508055759199108314D+01, &
    -3.79804256470945635097577D+02, &
     6.29331155312818442661052D+02, &
     8.66966202790413211295064D+02, &
    -3.14512729688483675254357D+04, &
    -3.61444134186911729807069D+04, &
     6.64561438202405440627855D+04 /)
  logical parity
  real ( kind = 8 ), parameter :: pi = 3.1415926535897932384626434D+00
  real ( kind = 8 ), dimension ( 8 ) :: q = (/ &
    -3.08402300119738975254353D+01, &
     3.15350626979604161529144D+02, &
    -1.01515636749021914166146D+03, &
    -3.10777167157231109440444D+03, &
     2.25381184209801510330112D+04, &
     4.75584627752788110767815D+03, &
    -1.34659959864969306392456D+05, &
    -1.15132259675553483497211D+05 /)
  real ( kind = 8 ) r8_gamma
  real ( kind = 8 ) res
  real ( kind = 8 ), parameter :: sqrtpi = 0.9189385332046727417803297D+00
  real ( kind = 8 ) sum
  real ( kind = 8 ) x
  real ( kind = 8 ), parameter :: xbig = 171.624D+00
  real ( kind = 8 ) xden
  real ( kind = 8 ), parameter :: xinf = 1.0D+30
  real ( kind = 8 ), parameter :: xminin = 2.23D-308
  real ( kind = 8 ) xnum
  real ( kind = 8 ) y
  real ( kind = 8 ) y1
  real ( kind = 8 ) ysq
  real ( kind = 8 ) z

  parity = .false.
  fact = 1.0D+00
  n = 0
  y = x
!
!  Argument is negative.
!
  if ( y <= 0.0D+00 ) then

    y = - x
    y1 = aint ( y )
    res = y - y1

    if ( res /= 0.0D+00 ) then

      if ( y1 /= aint ( y1 * 0.5D+00 ) * 2.0D+00 ) then
        parity = .true.
      end if

      fact = - pi / sin ( pi * res )
      y = y + 1.0D+00

    else

      res = xinf
      r8_gamma = res
      return

    end if

  end if
!
!  Argument is positive.
!
  if ( y < eps ) then
!
!  Argument < EPS.
!
    if ( xminin <= y ) then
      res = 1.0D+00 / y
    else
      res = xinf
      r8_gamma = res
      return
    end if

  else if ( y < 12.0D+00 ) then

    y1 = y
!
!  0.0 < argument < 1.0.
!
    if ( y < 1.0D+00 ) then

      z = y
      y = y + 1.0D+00
!
!  1.0 < argument < 12.0.
!  Reduce argument if necessary.
!
    else

      n = int ( y ) - 1
      y = y - real ( n, kind = 8 )
      z = y - 1.0D+00

    end if
!
!  Evaluate approximation for 1.0 < argument < 2.0.
!
    xnum = 0.0D+00
    xden = 1.0D+00
    do i = 1, 8
      xnum = ( xnum + p(i) ) * z
      xden = xden * z + q(i)
    end do

    res = xnum / xden + 1.0D+00
!
!  Adjust result for case  0.0 < argument < 1.0.
!
    if ( y1 < y ) then

      res = res / y1
!
!  Adjust result for case 2.0 < argument < 12.0.
!
    else if ( y < y1 ) then

      do i = 1, n
        res = res * y
        y = y + 1.0D+00
      end do

    end if

  else
!
!  Evaluate for 12.0 <= argument.
!
    if ( y <= xbig ) then

      ysq = y * y
      sum = c(7)
      do i = 1, 6
        sum = sum / ysq + c(i)
      end do
      sum = sum / y - y + sqrtpi
      sum = sum + ( y - 0.5D+00 ) * log ( y )
      res = exp ( sum )

    else

      res = xinf
      r8_gamma = res
      return

    end if

  end if
!
!  Final adjustments and return.
!
  if ( parity ) then
    res = - res
  end if

  if ( fact /= 1.0D+00 ) then
    res = fact / res
  end if

  r8_gamma = res

  return
end

!*****************************************************************************80
!! SCQF scales a quadrature formula to a nonstandard interval.
!
!  Discussion:
!
!    The arrays WTS and SWTS may coincide.
!
!    The arrays T and ST may coincide.
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license. 
!
!  Modified:
!
!    27 December 2009
!
!  Author:
!
!    Original FORTRAN77 version by Sylvan Elhay, Jaroslav Kautsky.
!    FORTRAN90 version by John Burkardt.
!
!  Reference:
!
!    Sylvan Elhay, Jaroslav Kautsky,
!    Algorithm 655: IQPACK, FORTRAN Subroutines for the Weights of 
!    Interpolatory Quadrature,
!    ACM Transactions on Mathematical Software,
!    Volume 13, Number 4, December 1987, pages 399-415.
!
!  Parameters:
!
!    Input, integer ( kind = 4 ) NT, the number of knots.
!
!    Input, real ( kind = 8 ) T(NT), the original knots.
!
!    Input, integer ( kind = 4 ) MLT(NT), the multiplicity of the knots.
!
!    Input, real ( kind = 8 ) WTS(NWTS), the weights.
!
!    Input, integer ( kind = 4 ) NWTS, the number of weights.
!
!    Input, integer ( kind = 4 ) NDX(NT), used to index the array WTS.  
!    For more details see the comments in CAWIQ.
!
!    Output, real ( kind = 8 ) SWTS(NWTS), the scaled weights.
!
!    Output, real ( kind = 8 ) ST(NT), the scaled knots.
!
!    Input, integer ( kind = 4 ) KIND, the rule.
!    1, Legendre,             (a,b)       1.0
!    2, Chebyshev Type 1,     (a,b)       ((b-x)*(x-a))^(-0.5)
!    3, Gegenbauer,           (a,b)       ((b-x)*(x-a))^alpha
!    4, Jacobi,               (a,b)       (b-x)^alpha*(x-a)^beta
!    5, Generalized Laguerre, (a,+oo)     (x-a)^alpha*exp(-b*(x-a))
!    6, Generalized Hermite,  (-oo,+oo)   |x-a|^alpha*exp(-b*(x-a)^2)
!    7, Exponential,          (a,b)       |x-(a+b)/2.0|^alpha
!    8, Rational,             (a,+oo)     (x-a)^alpha*(x+b)^beta
!    9, Chebyshev Type 2,     (a,b)       ((b-x)*(x-a))^(+0.5)
!
!    Input, real ( kind = 8 ) ALPHA, the value of Alpha, if needed.
!
!    Input, real ( kind = 8 ) BETA, the value of Beta, if needed.
!
!    Input, real ( kind = 8 ) A, B, the interval endpoints.
!
subroutine scqf ( nt, t, mlt, wts, nwts, ndx, swts, st, kind, alpha, beta, a, &
  b )
  implicit none

  integer ( kind = 4 ) nt
  integer ( kind = 4 ) nwts

  real ( kind = 8 ) a
  real ( kind = 8 ) al
  real ( kind = 8 ) alpha
  real ( kind = 8 ) b
  real ( kind = 8 ) be
  real ( kind = 8 ) beta
  integer ( kind = 4 ) i
  integer ( kind = 4 ) k
  integer ( kind = 4 ) kind
  integer ( kind = 4 ) l
  integer ( kind = 4 ) mlt(nt)
  integer ( kind = 4 ) ndx(nt)
  real ( kind = 8 ) p
  real ( kind = 8 ) shft
  real ( kind = 8 ) slp
  real ( kind = 8 ) st(nt)
  real ( kind = 8 ) swts(nwts)
  real ( kind = 8 ) t(nt)
  real ( kind = 8 ) temp
  real ( kind = 8 ) tmp
  real ( kind = 8 ) wts(nwts)

  temp = epsilon ( temp )

  call parchk ( kind, 1, alpha, beta )

  if ( kind == 1 ) then

    al = 0.0D+00
    be = 0.0D+00

    if ( abs ( b - a ) <= temp ) then
      write ( *, '(a)' ) ' '
      write ( *, '(a)' ) 'SCQF - Fatal error!'
      write ( *, '(a)' ) '  |B - A| too small.'
      stop
    end if

    shft = ( a + b ) / 2.0D+00
    slp = ( b - a ) / 2.0D+00

  else if ( kind == 2 ) then

    al = -0.5D+00
    be = -0.5D+00

    if ( abs ( b - a ) <= temp ) then
      write ( *, '(a)' ) ' '
      write ( *, '(a)' ) 'SCQF - Fatal error!'
      write ( *, '(a)' ) '  |B - A| too small.'
      stop
    end if

    shft = ( a + b ) / 2.0D+00
    slp = ( b - a ) / 2.0D+00

  else if ( kind == 3 ) then

    al = alpha
    be = alpha

    if ( abs ( b - a ) <= temp ) then
      write ( *, '(a)' ) ' '
      write ( *, '(a)' ) 'SCQF - Fatal error!'
      write ( *, '(a)' ) '  |B - A| too small.'
      stop
    end if

    shft = ( a + b ) / 2.0D+00
    slp = ( b - a ) / 2.0D+00

  else if ( kind == 4 ) then

    al = alpha
    be = beta

    if ( abs ( b - a ) <= temp ) then
      write ( *, '(a)' ) ' '
      write ( *, '(a)' ) 'SCQF - Fatal error!'
      write ( *, '(a)' ) '  |B - A| too small.'
      stop
    end if

    shft = ( a + b ) / 2.0D+00
    slp = ( b - a ) / 2.0D+00

  else if ( kind == 5 ) then

    if ( b <= 0.0D+00 ) then
      write ( *, '(a)' ) ' '
      write ( *, '(a)' ) 'SCQF - Fatal error!'
      write ( *, '(a)' ) '  B <= 0'
      stop
    end if

    shft = a
    slp = 1.0D+00 / b
    al = alpha
    be = 0.0D+00

  else if ( kind == 6 ) then

    if ( b <= 0.0D+00 ) then
      write ( *, '(a)' ) ' '
      write ( *, '(a)' ) 'SCQF - Fatal error!'
      write ( *, '(a)' ) '  B <= 0.'
      stop
    end if

    shft = a
    slp = 1.0D+00 / sqrt ( b )
    al = alpha
    be = 0.0D+00

  else if ( kind == 7 ) then

    al = alpha
    be = 0.0D+00

    if ( abs ( b - a ) <= temp ) then
      write ( *, '(a)' ) ' '
      write ( *, '(a)' ) 'SCQF - Fatal error!'
      write ( *, '(a)' ) '  |B - A| too small.'
      stop
    end if

    shft = ( a + b ) / 2.0D+00
    slp = ( b - a ) / 2.0D+00

  else if ( kind == 8 ) then

    if ( a + b <= 0.0D+00 ) then
      write ( *, '(a)' ) ' '
      write ( *, '(a)' ) 'SCQF - Fatal error!'
      write ( *, '(a)' ) '  A + B <= 0.'
      stop
    end if

    shft = a
    slp = a + b
    al = alpha
    be = beta

  else if ( kind == 9 ) then

    al = 0.5D+00
    be = 0.5D+00

    if ( abs ( b - a ) <= temp ) then
      write ( *, '(a)' ) ' '
      write ( *, '(a)' ) 'SCQF - Fatal error!'
      write ( *, '(a)' ) '  |B - A| too small.'
      stop
    end if

    shft = ( a + b ) / 2.0D+00
    slp = ( b - a ) / 2.0D+00

  end if

  p = slp**( al + be + 1.0D+00 )

  do k = 1, nt

    st(k) = shft + slp * t(k)
    l = abs ( ndx(k) )

    if ( l /= 0 ) then
      tmp = p
      do i = l, l + mlt(k) - 1
        swts(i) = wts(i) * tmp
        tmp = tmp * slp
      end do
    end if

  end do

  return
end

!*****************************************************************************80
!
!! SGQF computes knots and weights of a Gauss Quadrature formula.
!
!  Discussion:
!
!    This routine computes all the knots and weights of a Gauss quadrature
!    formula with simple knots from the Jacobi matrix and the zero-th
!    moment of the weight function, using the Golub-Welsch technique.
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license. 
!
!  Modified:
!
!    04 January 2010
!
!  Author:
!
!    Original FORTRAN77 version by Sylvan Elhay, Jaroslav Kautsky.
!    FORTRAN90 version by John Burkardt.
!
!  Reference:
!
!    Sylvan Elhay, Jaroslav Kautsky,
!    Algorithm 655: IQPACK, FORTRAN Subroutines for the Weights of 
!    Interpolatory Quadrature,
!    ACM Transactions on Mathematical Software,
!    Volume 13, Number 4, December 1987, pages 399-415.
!
!  Parameters:
!
!    Input, integer ( kind = 4 ) NT, the number of knots.
!
!    Input, real ( kind = 8 ) AJ(NT), the diagonal of the Jacobi matrix.
!
!    Input/output, real ( kind = 8 ) BJ(NT), the subdiagonal of the Jacobi 
!    matrix, in entries 1 through NT-1.  On output, BJ has been overwritten.
!
!    Input, real ( kind = 8 ) ZEMU, the zero-th moment of the weight function.
!
!    Output, real ( kind = 8 ) T(NT), the knots.
!
!    Output, real ( kind = 8 ) WTS(NT), the weights.
!
subroutine sgqf ( nt, aj, bj, zemu, t, wts )
  implicit none

  integer ( kind = 4 ) nt

  real ( kind = 8 ) aj(nt)
  real ( kind = 8 ) bj(nt)
  integer ( kind = 4 ) i
  real ( kind = 8 ) t(nt)
  real ( kind = 8 ) wts(nt)
  real ( kind = 8 ) zemu
!
!  Exit if the zero-th moment is not positive.
!
  if ( zemu <= 0.0D+00 ) then
    write ( *, '(a)' ) ' '
    write ( *, '(a)' ) 'SGQF - Fatal error!'
    write ( *, '(a)' ) '  ZEMU <= 0.'
    stop
  end if
!
!  Set up vectors for IMTQLX.
!
  t(1:nt) = aj(1:nt)

  wts(1) = sqrt ( zemu )
  wts(2:nt) = 0.0D+00
!
!  Diagonalize the Jacobi matrix.
!
  call imtqlx ( nt, t, bj, wts )

  wts(1:nt) = wts(1:nt)**2

  return
end

END MODULE gauss
