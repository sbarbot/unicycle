#!/bin/bash

set -e
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

WDIR=./waveform-1

if [ ! -e $WDIR ]; then
	echo "# creating directory $WDIR"
	mkdir -p $WDIR
fi

SRC=$WDIR/src.dat

		#print i+1,t,a**2*t*exp(-a*t)+0.2*H(t-5)*a**2*(t-5)*exp(-a*(t-5)),0
# source-time function
echo "" | awk 'function H(x){return (x>=0)?1:0}{
	a=20;
	for (i=0;i<1000;i++){
		t=i*0.01;
		print i+1,t,0,a**2*t*exp(-a*t)
	}
}' > $SRC

# create input file for post-processing
unicycle-waveform <<EOF
# output directory
$WDIR
# rigidity, Lame parameter, density
30e9 30e9 2800
# start time, interval
0e0 1e1
# number of patches
1
# n    x1 x2   x3 area strike dip
  1     0  0 10e3  1e6     90  90
# patch 1
# number of samples in time series
`grep -v "#" $SRC | wc -l`
# n time s-vel d-vel
`grep -v "#" $SRC`
# number of observation points
5
# n name    x1   x2   x3 rate
# no P-wave, no S-wave
  1 0000     0 10e3 10e3  100
# no P-wave
  2 0001  10e3    0 10e3  100
# no P-wave
  3 0002     0    0    0  100
# no S-wave
  4 0003   5e3    0  5e3  100
# P-wave and S-wave
  5 0004 2.5e3    0  5e3  100
EOF

