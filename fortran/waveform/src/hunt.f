C     subroutine hunt search neighborhood points in correlated
C     monotonically increasing or monotonically decreasing samples.
C     Given an array xx(1:n), and given a value x, returns a value jlo such that x is between
C     xx(jlo) and xx(jlo+1). xx(1:n) must be monotonic, either increasing or decreasing.
C     jlo=0 or jlo=n is returned to indicate that x is out of range. jlo on input is taken as
C     the initial guess for jlo on output.
C
C     Numerical recipies (Press et al., 1992)
C
      SUBROUTINE hunt(xx,n,x,jlo)
      IMPLICIT NONE
      INTEGER jlo,n
      REAL*8 x,xx(n)
      INTEGER inc,jhi,jm
      LOGICAL ascnd

C     ! true if ascending order of table, false otherwise.
      ascnd=xx(n) .GE. xx(1)

      IF ((jlo .LE. 0) .OR. (jlo .GT. n)) THEN
C        ! input guess not useful. go immediately to bisection.
         jlo=0
         jhi=n+1
         GOTO 3
      ENDIF

C     ! set the hunting increment
      inc=1
      IF (x .GE. xx(jlo) .EQV. ascnd) THEN
C        ! Hunt up:
1        jhi=jlo+inc
         IF (jhi .GT. n) THEN
C        ! Done hunting, since off end of table.
         jhi=n+1
         ELSE IF((x .GE. xx(jhi)) .EQV. ascnd) THEN
C              ! Not done hunting
               jlo=jhi
C              ! double the increment
               inc=inc+inc
               GOTO 1
C              ! Done hunting, value bracketed.
         ENDIF
      ELSE
C        ! Hunt down:
         jhi=jlo
2        jlo=jhi-inc
         IF (jlo .LT. 1) THEN
C           ! Done hunting, since off end of table.
            jlo=0
         ELSE IF (x .LT. xx(jlo) .EQV. ascnd) THEN
               jhi=jlo
               inc=inc+inc
               GOTO 2
C              ! Done hunting, value bracketed.
            ENDIF
C           ! Hunt is done, so begin the final bisection phase:
         ENDIF
3        IF (jhi-jlo .EQ. 1) THEN
            IF (x .EQ. xx(n)) jlo=n-1
            IF (x .EQ. xx(1)) jlo=1
            RETURN
         ENDIF

         jm=(jhi+jlo)/2
         IF (x .GE. xx(jm) .EQV. ascnd) THEN
            jlo=jm
         ELSE
            jhi=jm
         ENDIF

      GOTO 3

      END

