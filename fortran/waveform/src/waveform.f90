!> program waveform simulates synthetic seismograms in a full space 
!! from time series of moment rate.
!!
!! \mainpage
!!
!! We assume a point-source dislocation with the symmetric moment tensor M.
!! The source-receiver pair is separated by a distance r along a direction-
!! cosine G. The radiation coefficients are given by
!!
!!   AN = 3[5(G'MG) G - tr(M) G - 2 M'G]
!!   AIa = 6(G'MG) G - tr(M) G - 2 M'G
!!   AFP = (G'MG) G
!!   AIb = 6(G'MG) G - tr(M) G - 3M'G
!!   AFS = (G'M'G) G - M G
!!
!! The superscripts N, I, and F indicate near, intermediate, and far fields,
!! respectively. The component AFP is parallel to G, hence denoted as P wave.
!! The component AFS is perpendicular to G, hence denoted as shear wave.
!!
!! REFERENCES:
!!
!!   Pujol J., "Elastic wave propagation and generation in seismology", 
!!   Cambridge Univ. Press, 2003.
!! 
!! \author Sylvain Barbot (2019).
!----------------------------------------------------------------------

! standard output
#define STDOUT 6

! standard error
#define STDERR 0

#define WRITE_DEBUG_INFO(e) WRITE (0,'("ERROR ",I3.3," at line ",I5.5)') e,__LINE__

PROGRAM waveform

  IMPLICIT NONE

  TYPE PATCH_STRUCT
     SEQUENCE
     ! center coordinates
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: x
     ! moment tensors for strike-slip and dip-slip
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: M1, M2
     ! fault dimension and orientation
     REAL*8, DIMENSION(:), ALLOCATABLE :: area,strike,dip
  END TYPE PATCH_STRUCT

  TYPE RECEIVER_STRUCT
     ! output file
     INTEGER :: file

     ! position (m)
     REAL*8, DIMENSION(3) :: x

     ! sampling rate (Hz)
     REAL*8 :: rate

     ! short name
     CHARACTER(LEN=10) :: name

     ! start time
     REAL*8 :: tstart

     ! source-receiver orientation vector
     REAL*8, DIMENSION(3) :: gamma

     ! number of samples
     INTEGER :: n

     ! arrays of interpolated moment-rate, moment, moment-integral in the strike direction
     ! for P and S waves
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: sD0,sD1,sD2

     ! arrays of interpolated moment-rate, moment, moment-integral in the dip direction
     ! for P and S waves
     REAL*8, DIMENSION(:,:), ALLOCATABLE :: dD0,dD1,dD2

     ! seismic waveform (displacement vector components)
     REAL*8, DIMENSION(:), ALLOCATABLE :: u1,u2,u3
  END TYPE RECEIVER_STRUCT

  TYPE TIME_SERIES_STRUCT
     SEQUENCE

     ! number of samples in time series
     INTEGER :: n

     ! monotonically increasing time samples
     REAL*8, DIMENSION(:), ALLOCATABLE :: t

     ! associated time series of strike-slip velocity
     REAL*8, DIMENSION(:), ALLOCATABLE :: ssv0,ssv1,ssv2

     ! associated time series of dip-slip velocity
     REAL*8, DIMENSION(:), ALLOCATABLE :: dsv0,dsv1,dsv2

  END TYPE TIME_SERIES_STRUCT

  TYPE INPUT_STRUCT

     ! output directory
     CHARACTER(256) :: wdir

     ! elastic properties
     REAL*8 :: mu,lambda,rho,nu

     ! start of event
     REAL*8 :: tstart

     ! duration of event
     REAL*8 :: duration

     ! number of patches
     INTEGER :: ns

     ! patches
     TYPE(PATCH_STRUCT) :: patch

     ! moment-rate time series
     TYPE(TIME_SERIES_STRUCT), DIMENSION(:), ALLOCATABLE :: mrate

     ! number of observation points
     INTEGER :: nReceiver

     ! observation points
     TYPE(RECEIVER_STRUCT), DIMENSION(:), ALLOCATABLE :: receiver

     ! other options
     LOGICAL :: isdryrun=.FALSE.
     LOGICAL :: ishelp=.FALSE.
     LOGICAL :: isversion=.FALSE.

  END TYPE INPUT_STRUCT

  REAL*8, PARAMETER :: PI = 4*DATAN(1.0d0)
  REAL*8, PARAMETER :: DEG2RAD = ATAN(1._8)/45._8

  ! input structure
  TYPE(INPUT_STRUCT) :: in

  ! error flag
  INTEGER :: ierr

  CHARACTER(512) :: filename

  ! time steps
  INTEGER :: i,j

  ! source-receiver distance
  REAL*8 :: r

  ! P-wave and S-wave velocity
  REAL*8 :: alpha, beta

  ! source-receiver direction cosines vector
  REAL*8, DIMENSION(3) :: gamma

  ! radiation pattern
  REAL*8 :: radiation

  ! trace of moment tensor
  REAL*8 :: tr

  ! traction vector
  REAL*8, DIMENSION(3) :: traction

  ! radiation pattern coefficients
  REAL*8, DIMENSION(3) :: AN, AIa, AFP, AIb, AFS

  ! retrieve input parameters from command line
  CALL init(in)
  CALL FLUSH(STDOUT)

  IF (in%isdryrun) THEN
     PRINT '("dry run: abort calculation")'
  END IF
  IF (in%isdryrun .OR. in%isversion .OR. in%ishelp) THEN
     STOP
  END IF

  ! integrate the moment-rates
  DO i=1,in%ns
     ALLOCATE(in%mrate(i)%ssv1(in%mrate(i)%n), &
              in%mrate(i)%ssv2(in%mrate(i)%n), &
              in%mrate(i)%dsv1(in%mrate(i)%n), &
              in%mrate(i)%dsv2(in%mrate(i)%n),STAT=ierr)
     IF (ierr>0) STOP "could not allocate the integrated moment arrays"

     ! integrate moment-rate
     CALL int1(in%mrate(i)%n,in%mrate(i)%t,in%mrate(i)%ssv0,0.d0,in%mrate(i)%ssv1)
     CALL int1(in%mrate(i)%n,in%mrate(i)%t,in%mrate(i)%ssv1,0.d0,in%mrate(i)%ssv2)
     CALL int1(in%mrate(i)%n,in%mrate(i)%t,in%mrate(i)%dsv0,0.d0,in%mrate(i)%dsv1)
     CALL int1(in%mrate(i)%n,in%mrate(i)%t,in%mrate(i)%dsv1,0.d0,in%mrate(i)%dsv2)
  END DO

  ! loop over receivers
  DO i=1,in%nReceiver

     ! number of samples in waveform
     in%receiver(i)%n=CEILING(in%duration*in%receiver(i)%rate)

     PRINT '("# number of samples in station ",I3,": ",I10)',i,in%receiver(i)%n

     ! allocate interpolated time series
     ALLOCATE(in%receiver(i)%sD0(in%receiver(i)%n,2), &
              in%receiver(i)%sD1(in%receiver(i)%n,2), &
              in%receiver(i)%sD2(in%receiver(i)%n,2), &
              in%receiver(i)%dD0(in%receiver(i)%n,2), &
              in%receiver(i)%dD1(in%receiver(i)%n,2), &
              in%receiver(i)%dD2(in%receiver(i)%n,2),STAT=ierr)
     IF (ierr>0) STOP "could not allocate the interpolated time series of moments"

     ! allocate interpolated time series
     ALLOCATE(in%receiver(i)%u1(in%receiver(i)%n), &
              in%receiver(i)%u2(in%receiver(i)%n), &
              in%receiver(i)%u3(in%receiver(i)%n),STAT=ierr)
     IF (ierr>0) STOP "could not allocate the interpolated time series of moments"

     ! initialize waveform
     in%receiver(i)%u1=0
     in%receiver(i)%u2=0
     in%receiver(i)%u3=0

     ! time of first arrival
     in%receiver(i)%tstart=HUGE(1.d0)
     DO j=1,in%ns
        ! source-receiver distance
        r=SQRT(SUM((in%receiver(i)%x-in%patch%x(:,j))**2))

        ! P arrival from closest patch
        in%receiver(i)%tstart=MIN(in%receiver(i)%tstart,in%tstart+r/alpha)
     END DO

     PRINT '("# first arrival: ",F12.3)',in%receiver(i)%tstart
     PRINT '("# P-S arrival:   ",F12.3)',r/alpha-r/beta

     ! loop over sources
     DO j=1,in%ns

        ! source-receiver distance
        r=SQRT(SUM((in%receiver(i)%x-in%patch%x(:,j))**2))

        ! source-receiver unit direction vector (direction cosines)
        gamma=(in%receiver(i)%x-in%patch%x(:,j))/r

        ! interpolate moment-rate for strike-slip faulting for P-waves
        CALL interp1(in%mrate(j)%n,in%mrate(j)%t,in%mrate(j)%ssv0, &
                     in%tstart, &
                     1.d0/in%receiver(i)%rate, &
                     in%receiver(i)%n, &
                     in%receiver(i)%sD0(:,1))
        CALL interp1(in%mrate(j)%n,in%mrate(j)%t,in%mrate(j)%ssv1, &
                     in%tstart, &
                     1.d0/in%receiver(i)%rate, &
                     in%receiver(i)%n, &
                     in%receiver(i)%sD1(:,1))
        CALL interp1(in%mrate(j)%n,in%mrate(j)%t,in%mrate(j)%ssv2, &
                     in%tstart, &
                     1.d0/in%receiver(i)%rate, &
                     in%receiver(i)%n, &
                     in%receiver(i)%sD2(:,1))

        ! interpolate moment-rate for strike-slip faulting for S-waves
        CALL interp1(in%mrate(j)%n,in%mrate(j)%t,in%mrate(j)%ssv0, &
                     in%tstart+r/alpha-r/beta, &
                     1.d0/in%receiver(i)%rate, &
                     in%receiver(i)%n, &
                     in%receiver(i)%sD0(:,2))
        CALL interp1(in%mrate(j)%n,in%mrate(j)%t,in%mrate(j)%ssv1, &
                     in%tstart+r/alpha-r/beta, &
                     1.d0/in%receiver(i)%rate, &
                     in%receiver(i)%n, &
                     in%receiver(i)%sD1(:,2))
        CALL interp1(in%mrate(j)%n,in%mrate(j)%t,in%mrate(j)%ssv2, &
                     in%tstart+r/alpha-r/beta, &
                     1.d0/in%receiver(i)%rate, &
                     in%receiver(i)%n, &
                     in%receiver(i)%sD2(:,2))

        ! interpolate moment-rate for dip-slip faulting for P-waves
        CALL interp1(in%mrate(j)%n,in%mrate(j)%t,in%mrate(j)%dsv0, &
                     in%tstart, &
                     1.d0/in%receiver(i)%rate, &
                     in%receiver(i)%n, &
                     in%receiver(i)%dD0(:,1))
        CALL interp1(in%mrate(j)%n,in%mrate(j)%t,in%mrate(j)%dsv1, &
                     in%tstart, &
                     1.d0/in%receiver(i)%rate, &
                     in%receiver(i)%n, &
                     in%receiver(i)%dD1(:,1))
        CALL interp1(in%mrate(j)%n,in%mrate(j)%t,in%mrate(j)%dsv2, &
                     in%tstart, &
                     1.d0/in%receiver(i)%rate, &
                     in%receiver(i)%n, &
                     in%receiver(i)%dD2(:,1))

        ! interpolate moment-rate for dip-slip faulting for S-waves
        CALL interp1(in%mrate(j)%n,in%mrate(j)%t,in%mrate(j)%dsv0, &
                     in%tstart+r/alpha-r/beta, &
                     1.d0/in%receiver(i)%rate, &
                     in%receiver(i)%n, &
                     in%receiver(i)%dD0(:,2))
        CALL interp1(in%mrate(j)%n,in%mrate(j)%t,in%mrate(j)%dsv1, &
                     in%tstart+r/alpha-r/beta, &
                     1.d0/in%receiver(i)%rate, &
                     in%receiver(i)%n, &
                     in%receiver(i)%dD1(:,2))
        CALL interp1(in%mrate(j)%n,in%mrate(j)%t,in%mrate(j)%dsv2, &
                     in%tstart+r/alpha-r/beta, &
                     1.d0/in%receiver(i)%rate, &
                     in%receiver(i)%n, &
                     in%receiver(i)%dD2(:,2))

        ! radiation pattern for strike-slip
        radiation=proj(gamma,gamma,in%patch%M1(:,j))
        CALL mvp(in%patch%M1(:,j),gamma,traction)
        tr=trace(in%patch%M1(:,j))
        AN=3*(5*radiation*gamma-tr*gamma-2*traction)
        AIa=6*radiation*gamma-tr*gamma-2*traction
        AFP=radiation*gamma
        AIb=6*radiation*gamma-tr*gamma-3*traction
        AFS=radiation*gamma-traction

#ifdef DEBUG
        PRINT '("# radiation pattern for the strike-slip component")'
        PRINT '("M=  (",5(ES11.4E2,","),ES11.4E2,")")', in%patch%M1(:,j)
        PRINT '("G=  (",2(ES11.4E2,","),ES11.4E2,")")', gamma
        PRINT '("MG= (",2(ES11.4E2,","),ES11.4E2,")")', traction
        PRINT '("AN= (",2(ES11.4E2,","),ES11.4E2,")")', AN
        PRINT '("AIa=(",2(ES11.4E2,","),ES11.4E2,")")', AIa
        PRINT '("AFP=(",2(ES11.4E2,","),ES11.4E2,")")', AFP
        PRINT '("AIb=(",2(ES11.4E2,","),ES11.4E2,")")', AIb
        PRINT '("AFS=(",2(ES11.4E2,","),ES11.4E2,")")', AFS
        PRINT '("GMG=",ES11.4E2)', radiation
#endif

        ! strike-slip component
        in%receiver(i)%u1=in%receiver(i)%u1 + ( &
                   AN(1)*(1d0/r**4*in%receiver(i)%sD2(:,1)+1d0/(alpha*r**3)*in%receiver(i)%sD1(:,1)) &
                  +AIa(1)/(alpha**2*r**2)*in%receiver(i)%sD1(:,1)+AFP(1)/(alpha**3*r)*in%receiver(i)%sD0(:,1) &
                  -AN(1)*(1d0/r**4*in%receiver(i)%sD2(:,2)+1d0/( beta*r**3)*in%receiver(i)%sD1(:,2)) &
                  -AIb(1)/( beta**2*r**2)*in%receiver(i)%sD1(:,2)-AFS(1)/( beta**3*r)*in%receiver(i)%sD0(:,2) &
                          ) / (4d0*PI*in%rho)
        in%receiver(i)%u2=in%receiver(i)%u2 + ( &
                   AN(2)*(1d0/r**4*in%receiver(i)%sD2(:,1)+1d0/(alpha*r**3)*in%receiver(i)%sD1(:,1)) &
                  +AIa(2)/(alpha**2*r**2)*in%receiver(i)%sD1(:,1)+AFP(2)/(alpha**3*r)*in%receiver(i)%sD0(:,1) &
                  -AN(2)*(1d0/r**4*in%receiver(i)%sD2(:,2)+1d0/( beta*r**3)*in%receiver(i)%sD1(:,2)) &
                  -AIb(2)/( beta**2*r**2)*in%receiver(i)%sD1(:,2)-AFS(2)/( beta**3*r)*in%receiver(i)%sD0(:,2) &
                          ) / (4d0*PI*in%rho)
        in%receiver(i)%u3=in%receiver(i)%u3 + ( &
                   AN(3)*(1d0/r**4*in%receiver(i)%sD2(:,1)+1d0/(alpha*r**3)*in%receiver(i)%sD1(:,1)) &
                  +AIa(3)/(alpha**2*r**2)*in%receiver(i)%sD1(:,1)+AFP(3)/(alpha**3*r)*in%receiver(i)%sD0(:,1) &
                  -AN(3)*(1d0/r**4*in%receiver(i)%sD2(:,2)+1d0/( beta*r**3)*in%receiver(i)%sD1(:,2)) &
                  -AIb(3)/( beta**2*r**2)*in%receiver(i)%sD1(:,2)-AFS(3)/( beta**3*r)*in%receiver(i)%sD0(:,2) &
                          ) / (4d0*PI*in%rho)

        ! radiation pattern for dip-slip
        radiation=proj(gamma,gamma,in%patch%M2(:,j))
        CALL mvp(in%patch%M2(:,j),gamma,traction)
        tr=trace(in%patch%M2(:,j))
        AN=3*(5*radiation*gamma-tr*gamma-2*traction)
        AIa=6*radiation*gamma-tr*gamma-2*traction
        AFP=radiation*gamma
        AIb=6*radiation*gamma-tr*gamma-3*traction
        AFS=radiation*gamma-traction

#ifdef DEBUG
        PRINT '("# radiation pattern for the dip-slip component")'
        PRINT '("M=  (",5(ES11.4E2,","),ES11.4E2,")")', in%patch%M2(:,j)
        PRINT '("G=  (",2(ES11.4E2,","),ES11.4E2,")")', gamma
        PRINT '("MG= (",2(ES11.4E2,","),ES11.4E2,")")', traction
        PRINT '("AN= (",2(ES11.4E2,","),ES11.4E2,")")', AN
        PRINT '("AIa=(",2(ES11.4E2,","),ES11.4E2,")")', AIa
        PRINT '("AFP=(",2(ES11.4E2,","),ES11.4E2,")")', AFP
        PRINT '("AIb=(",2(ES11.4E2,","),ES11.4E2,")")', AIb
        PRINT '("AFS=(",2(ES11.4E2,","),ES11.4E2,")")', AFS
        PRINT '("G''MG=",ES11.4E2)', radiation
#endif

        ! dip-slip component
        in%receiver(i)%u1=in%receiver(i)%u1 + ( &
                   AN(1)*(1d0/r**4*in%receiver(i)%dD2(:,1)+1d0/(alpha*r**3)*in%receiver(i)%dD1(:,1)) &
                  +AIa(1)/(alpha**2*r**2)*in%receiver(i)%dD1(:,1)+AFP(1)/(alpha**3*r)*in%receiver(i)%dD0(:,1) &
                  -AN(1)*(1d0/r**4*in%receiver(i)%dD2(:,2)+1d0/( beta*r**3)*in%receiver(i)%dD1(:,2)) &
                  -AIb(1)/( beta**2*r**2)*in%receiver(i)%dD1(:,2)-AFS(1)/( beta**3*r)*in%receiver(i)%dD0(:,2) &
                          ) / (4d0*PI*in%rho)
        in%receiver(i)%u2=in%receiver(i)%u2 + ( &
                   AN(2)*(1d0/r**4*in%receiver(i)%dD2(:,1)+1d0/(alpha*r**3)*in%receiver(i)%dD1(:,1)) &
                  +AIa(2)/(alpha**2*r**2)*in%receiver(i)%dD1(:,1)+AFP(2)/(alpha**3*r)*in%receiver(i)%dD0(:,1) &
                  -AN(2)*(1d0/r**4*in%receiver(i)%dD2(:,2)+1d0/( beta*r**3)*in%receiver(i)%dD1(:,2)) &
                  -AIb(2)/( beta**2*r**2)*in%receiver(i)%dD1(:,2)-AFS(2)/( beta**3*r)*in%receiver(i)%dD0(:,2) &
                          ) / (4d0*PI*in%rho)
        in%receiver(i)%u3=in%receiver(i)%u3 + ( &
                   AN(3)*(1d0/r**4*in%receiver(i)%dD2(:,1)+1d0/(alpha*r**3)*in%receiver(i)%dD1(:,1)) &
                  +AIa(3)/(alpha**2*r**2)*in%receiver(i)%dD1(:,1)+AFP(3)/(alpha**3*r)*in%receiver(i)%dD0(:,1) &
                  -AN(3)*(1d0/r**4*in%receiver(i)%dD2(:,2)+1d0/( beta*r**3)*in%receiver(i)%dD1(:,2)) &
                  -AIb(3)/( beta**2*r**2)*in%receiver(i)%dD1(:,2)-AFS(3)/( beta**3*r)*in%receiver(i)%dD0(:,2) &
                          ) / (4d0*PI*in%rho)

     END DO

     WRITE (filename,'(a,"/waveform-",a,".dat")') TRIM(in%wdir),TRIM(in%receiver(i)%name)
     PRINT '("# export to ",a)', TRIM(filename)
     OPEN (UNIT=1001,FILE=filename,IOSTAT=ierr,FORM="FORMATTED")
     IF (ierr>0) THEN
        WRITE (STDERR,'("error: unable to access ",a)') TRIM(filename)
        STOP 1
     END IF
     WRITE (1001,'("#               time              u1              u2              u3", &
                                     & "              v1              v2              v3")')
     WRITE (1001,'(ES20.14E2,6ES16.8E2)') ( &
             in%receiver(i)%tstart+(j-1)/in%receiver(i)%rate, &
             in%receiver(i)%u1(j), &
             in%receiver(i)%u2(j), &
             in%receiver(i)%u3(j), &
             (in%receiver(i)%u1(MIN(j+1,in%receiver(i)%n))-in%receiver(i)%u1(j))*in%receiver(i)%rate, &
             (in%receiver(i)%u2(MIN(j+1,in%receiver(i)%n))-in%receiver(i)%u2(j))*in%receiver(i)%rate, &
             (in%receiver(i)%u3(MIN(j+1,in%receiver(i)%n))-in%receiver(i)%u3(j))*in%receiver(i)%rate, j=1,in%receiver(i)%n)
     CLOSE(1001)
     IF (in%nReceiver .NE. i) THEN
        PRINT '("#")'
     END IF

     ! deallocate interpolated time series
     DEALLOCATE(in%receiver(i)%sD0)
     DEALLOCATE(in%receiver(i)%sD1)
     DEALLOCATE(in%receiver(i)%sD2)
     DEALLOCATE(in%receiver(i)%dD0)
     DEALLOCATE(in%receiver(i)%dD1)
     DEALLOCATE(in%receiver(i)%dD2)

     ! deallocate waveforms
     DEALLOCATE(in%receiver(i)%u1)
     DEALLOCATE(in%receiver(i)%u2)
     DEALLOCATE(in%receiver(i)%u3)
  END DO

  DEALLOCATE(in%patch%x)
  DEALLOCATE(in%patch%area)
  DEALLOCATE(in%patch%strike)
  DEALLOCATE(in%patch%dip)
  DEALLOCATE(in%patch%M1)
  DEALLOCATE(in%patch%M2)
  DO i=1,in%ns
     DEALLOCATE(in%mrate(i)%t)
     DEALLOCATE(in%mrate(i)%ssv0)
     DEALLOCATE(in%mrate(i)%ssv1)
     DEALLOCATE(in%mrate(i)%ssv2)
     DEALLOCATE(in%mrate(i)%dsv0)
     DEALLOCATE(in%mrate(i)%dsv1)
     DEALLOCATE(in%mrate(i)%dsv2)
  END DO
  IF (ALLOCATED(in%mrate)) DEALLOCATE(in%mrate)
  IF (ALLOCATED(in%receiver)) DEALLOCATE(in%receiver)

CONTAINS

  !------------------------------------------------------------------------
  !> subroutine computeMomentTensor
  !! computes moment tensors associated with unit slip in the strike and
  !! dip directions.
  !!
  !!           /  n(1)s(1)         (n(1)s(2)+n(2)s(1))/2   (n(1)s(3)+n(3)s(1))/2  \
  !!   Mss = C |                           n(2)s(2)        (n(2)s(3)+n(2)s(3))/2  | 
  !!           \                                                   n(3)s(3)       /
  !!
  !!           /  n(1)d(1)         (n(1)d(2)+n(2)d(1))/2   (n(1)d(3)+n(3)d(1))/2  \
  !!   Mds = C |                           n(2)d(2)        (n(2)d(3)+n(2)d(3))/2  | 
  !!           \                                                   n(3)d(3)       /
  !!
  !! where C=mu*area
  !!
  !! INPUT:
  !! @param mu          - rigidity of surrounding rocks (Pa)
  !! @param strike      - fault strike angle (radian)
  !! @param dip         - fault dip angle (radian)
  !! @param area        - fault area
  !!
  !! OUTPUT:
  !! @param Mss         - unit moment tensor for strike-slip
  !! @param Mds         - unit moment tensor for dip-slip
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !------------------------------------------------------------------------
  SUBROUTINE computeMomentTensors(mu,area,strike,dip,Mss,Mds)
    IMPLICIT NONE
    REAL*8, INTENT(IN) :: mu,area,strike,dip
    REAL*8, DIMENSION(6), INTENT(OUT) :: Mss,Mds

    ! unit vector in the strike, dip, and normal directions
    REAL*8, DIMENSION(3) :: s,d,n

    ! unit vector in the strike direction
    s(1)=COS(strike)
    s(2)=SIN(strike)
    s(3)=0._8
            
    ! unit vector in the dip direction
    d(1)=+SIN(strike)*COS(dip)
    d(2)=-COS(strike)*COS(dip)
    d(3)=-SIN(dip)
            
    ! unit vector in the normal direction
    n(1)=-SIN(strike)*SIN(dip)
    n(2)=+COS(strike)*SIN(dip)
    n(3)=-COS(dip)

    ! moment tensor for unit slip in the strike direction
    Mss=(/ s(1)*n(1), (s(1)*n(2)+s(2)*n(1))/2, (s(1)*n(3)+s(3)*n(1))/2, &
                            s(2)*n(2),         (s(2)*n(3)+s(3)*n(2))/2, &
                                                       s(3)*n(3)      /)*mu*area
            
    ! moment tensor for unit slip in the dip direction
    Mds=(/ d(1)*n(1), (d(1)*n(2)+d(2)*n(1))/2, (d(1)*n(3)+d(3)*n(1))/2, &
                            d(2)*n(2),         (d(2)*n(3)+d(3)*n(2))/2, &
                                                       d(3)*n(3)      /)*mu*area
            
  END SUBROUTINE computeMomentTensors

  !-----------------------------------------------------------------------
  !> subroutine int1
  ! integration of unevenly sampled time series.
  !
  ! INPUT:
  ! @param n       - number of samples in arrays yp and y
  ! @param x       - x-axis of array yp
  ! @param yp      - unevenly sampled time series
  ! @param y0      - initial value of array y
  ! @param y       - array, integral of array yp
  !-----------------------------------------------------------------------
  SUBROUTINE int1(n,x,yp,y0,y)
    INTEGER, INTENT(IN) :: n
    REAL*8, DIMENSION(n), INTENT(IN) :: x,yp
    REAL*8, INTENT(IN) :: y0
    REAL*8, DIMENSION(n), INTENT(OUT) :: y

    INTEGER :: i

    y(1)=y0
    DO i=1,n-1
       y(i+1)=y(i)+(yp(i)+yp(i+1))/2*(x(i+1)-x(i))
    END DO

  END SUBROUTINE int1

  !-----------------------------------------------------------------------
  !> subroutine interp1
  ! linear interpolation of unevenly sampled time series.
  !
  ! INPUT:
  ! @param n       - number of samples in arrays x and y
  ! @param x       - x-axis of array y
  ! @param y       - unevenly sampled time series
  ! @param x0      - start time of array yi
  ! @param dx      - uniform sampling size of array yi
  ! @param m       - number of samples in array yi
  ! @param yi      - array of m interpolated values
  !-----------------------------------------------------------------------
  SUBROUTINE interp1(n,x,y,x0,dx,m,yi)
    INTEGER, INTENT(IN) :: n
    REAL*8, DIMENSION(n), INTENT(IN) :: x,y
    REAL*8, INTENT(IN) :: x0,dx
    INTEGER, INTENT(IN) :: m
    REAL*8, DIMENSION(m), INTENT(OUT) :: yi

    INTEGER :: i,jlo
    REAL*8 :: t

    jlo=1
    DO i=1,m
       t=x0+(i-1)*dx
       CALL hunt(x,n,t,jlo)

       IF (0 .EQ. jlo) THEN
          yi(i)=0.d0
       ELSE IF (n .EQ. jlo) THEN
          yi(i)=y(n)
       ELSE
          ! linear interpolation
          yi(i)=y(jlo)+(y(jlo+1)-y(jlo))*(t-x(jlo))/(x(jlo+1)-x(jlo))
       END IF
    END DO

  END SUBROUTINE interp1

  !---------------------------------------------------------------------
  !> subroutine init
  !! reads simulation parameters from the standard input and initialize
  !! model parameters.
  !!
  !! INPUT:
  !! @param unit - the unit number used to read input data
  !!
  !! OUTPUT:
  !! @param in
  !!
  !! \author Sylvain Barbot (sbarbot@usc.edu)
  !---------------------------------------------------------------------
  SUBROUTINE init(in)
    USE getopt_m

    TYPE(INPUT_STRUCT), INTENT(OUT) :: in

    CHARACTER :: ch
    CHARACTER(512) :: dataline
    CHARACTER(256) :: filename
    INTEGER :: iunit,noptions
    TYPE(OPTION_S) :: opts(3)

    INTEGER :: i,k,ierr

    ! define long options, such as --dry-run
    ! parse the command line for options
    opts(1)=OPTION_S("version",.FALSE.,CHAR(21))
    opts(2)=OPTION_S("dry-run",.FALSE.,CHAR(22))
    opts(3)=OPTION_S("help",.FALSE.,'h')

    noptions=0
    DO
       ch=getopt("h",opts)
       SELECT CASE(ch)
       CASE(CHAR(0))
          EXIT
       CASE(CHAR(21))
          ! option version
          in%isversion=.TRUE.
       CASE(CHAR(22))
          ! option dry-run
          in%isdryrun=.TRUE.
       CASE('h')
          ! option help
          in%ishelp=.TRUE.
       CASE('?')
          WRITE_DEBUG_INFO(100)
          in%ishelp=.TRUE.
          EXIT
       CASE DEFAULT
          WRITE (0,'("unhandled option ", a, " (this is a bug")') optopt
          WRITE_DEBUG_INFO(100)
          STOP 3
       END SELECT
       noptions=noptions+1
    END DO

    IF (in%isversion) THEN
       CALL printversion()
       ! abort parameter input
       STOP
    END IF

    IF (in%ishelp) THEN
       CALL printhelp()
       ! abort parameter input
       STOP
    END IF

    PRINT 2000
    PRINT '("# WAVEFORM")'
    PRINT '("# synthetic waveform simulation in full elastic space")'
    PRINT 2000

    IF (noptions .LT. COMMAND_ARGUMENT_COUNT()) THEN
       ! read from input file
       iunit=25
       CALL GET_COMMAND_ARGUMENT(noptions+1,filename)
       OPEN (UNIT=iunit,FILE=filename,IOSTAT=ierr)
    ELSE
       ! get input parameters from standard input
       iunit=5
    END IF

    PRINT '("# output directory")'
    CALL getdata(iunit,dataline)
    READ (dataline,'(a)') in%wdir
    PRINT '(2X,a)', TRIM(in%wdir)

    PRINT '("# rigidity, Lame parameter, density")'
    CALL getdata(iunit,dataline)
    READ  (dataline,*) in%mu,in%lambda,in%rho
    PRINT '(3ES12.2E2)', in%mu,in%lambda,in%rho

    IF (0 .GT. in%mu) THEN
       WRITE_DEBUG_INFO(-1)
       WRITE (STDERR,'(a)') TRIM(dataline)
       WRITE (STDERR,'("input error: shear modulus must be positive")')
       STOP 2
    END IF

    in%nu=in%lambda/2._8/(in%lambda+in%mu)
    IF (-1._8 .GT. in%nu) THEN
       WRITE_DEBUG_INFO(-1)
       WRITE (STDERR,'(a)') TRIM(dataline)
       WRITE (STDERR,'("input error: Poisson''s ratio must be greater than -1.")')
       STOP 2
    END IF
    IF (0.5_8 .LT. in%nu) THEN
       WRITE_DEBUG_INFO(-1)
       WRITE (STDERR,'(a)') TRIM(dataline)
       WRITE (STDERR,'("input error: Poisson''s ratio must be lower than 0.5.")')
       STOP 2
    END IF

    ! seismic wave velocity
    alpha=SQRT((in%lambda+2*in%mu)/in%rho)
    beta=SQRT(in%mu/in%rho)
    PRINT '("#      Vp,     Vs")'
    PRINT '("#",2F8.2)', alpha, beta

    PRINT '("# event start, duration")'
    CALL getdata(iunit,dataline)
    READ  (dataline,*) in%tstart,in%duration
    PRINT '(2ES20.12E2)', in%tstart,in%duration

    IF (in%duration .LE. 0._8) THEN
       WRITE (STDERR,'("**** error **** ")')
       WRITE (STDERR,'(a)') TRIM(dataline)
       WRITE (STDERR,'("event duration must be positive. exiting.")')
       STOP 1
    END IF

    ! - - - - - - - - - - - - - - - - - - - - - - - - - -
    !                    P A T C H E S
    ! - - - - - - - - - - - - - - - - - - - - - - - - - -
    PRINT '("# number of patches")'
    CALL getdata(iunit,dataline)
    READ  (dataline,*) in%ns
    PRINT '(I5)', in%ns
    IF (in%ns .GT. 0) THEN
       ALLOCATE(in%patch%x(3,in%ns), &
                in%patch%area(in%ns), &
                in%patch%strike(in%ns), &
                in%patch%dip(in%ns), &
                in%patch%M1(6,in%ns), &
                in%patch%M2(6,in%ns),STAT=ierr)
       IF (ierr>0) STOP "could not allocate the patch list"
       PRINT 2000
       PRINT '("#    n       x1       x2       x3       area strike   dip")'
       PRINT 2000
       DO k=1,in%ns
          CALL getdata(iunit,dataline)
          READ (dataline,*,IOSTAT=ierr) i, &
               in%patch%x(1,k), &
               in%patch%x(2,k), &
               in%patch%x(3,k), &
               in%patch%area(k), &
               in%patch%strike(k), &
               in%patch%dip(k)

          PRINT '(I6,3ES9.2E1,ES11.4E2,f7.1,f6.1)',i, &
               in%patch%x(1,k), &
               in%patch%x(2,k), &
               in%patch%x(3,k), &
               in%patch%area(k), &
               in%patch%strike(k), &
               in%patch%dip(k)

          ! convert to radians
          in%patch%strike(k)=in%patch%strike(k)*DEG2RAD     
          in%patch%dip(k)=in%patch%dip(k)*DEG2RAD     

          IF (i .NE. k) THEN
             WRITE (STDERR,'("invalid patch definition")')
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE (STDERR,'("error in input file: unexpected index")')
             STOP 1
          END IF
          IF (in%patch%area(k) .LE. 0._8) THEN
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE (STDERR,'("error in input file: patch area must be positive.")')
             STOP 1
          END IF
             
          ! calculate moment tensors for unit slip in the strike and dip directions
          CALL computeMomentTensors(in%mu, &
                  in%patch%area(k),in%patch%strike(k),in%patch%dip(k), &
                  in%patch%M1(:,k),in%patch%M2(:,k))
       END DO

    END IF
       
    ! - - - - - - - - - - - - - - - - - - - - - - - - - -
    !              M O M E N T - R A T E
    ! - - - - - - - - - - - - - - - - - - - - - - - - - -
    ALLOCATE(in%mrate(in%ns),STAT=ierr)
    IF (ierr>0) STOP "could not allocate the list of time series"
    DO i=1,in%ns
       PRINT 2000
       PRINT '("# number of samples for patch ",I5)', i
       CALL getdata(iunit,dataline)
       READ  (dataline,*) in%mrate(i)%n
       PRINT '(I7)', in%mrate(i)%n

       ALLOCATE(in%mrate(i)%t(in%mrate(i)%n), &
                in%mrate(i)%ssv0(in%mrate(i)%n), &
                in%mrate(i)%dsv0(in%mrate(i)%n),STAT=ierr)
       IF (ierr>0) STOP "could not allocate the time series"

       PRINT '("#     n          time           ssv           dsv")'
       DO k=1,in%mrate(i)%n
          CALL getdata(iunit,dataline)
          READ  (dataline,*) j,in%mrate(i)%t(k),in%mrate(i)%ssv0(k),in%mrate(i)%dsv0(k)

          IF (j .NE. k) THEN
             WRITE (6,'(a)') TRIM(dataline)
             STOP "wrong input. exiting."
          END IF
          IF (k .LE. 10) THEN
             PRINT '(I7,X,ES13.6E2,2ES14.6E2)', k,in%mrate(i)%t(k),in%mrate(i)%ssv0(k),in%mrate(i)%dsv0(k)
          END IF
          IF ((k .EQ. 11) .AND. (k .LT. (in%mrate(i)%n-10))) THEN
             PRINT '("                         ...")'
          END IF
          IF ((k .GT. 10) .AND. (k .GT. (in%mrate(i)%n-10))) THEN
             PRINT '(I7,X,ES13.6E2,2ES14.6E2)', k,in%mrate(i)%t(k),in%mrate(i)%ssv0(k),in%mrate(i)%dsv0(k)
          END IF

       END DO
    END DO

    ! - - - - - - - - - - - - - - - - - - - - - - - - - -
    !        O B S E R V A T I O N   P O I N T S
    ! - - - - - - - - - - - - - - - - - - - - - - - - - -
    PRINT 2000
    PRINT '("# number of receivers")'
    CALL getdata(iunit,dataline)
    READ  (dataline,*) in%nReceiver
    PRINT '(I5)', in%nReceiver
    IF (0 .LT. in%nReceiver) THEN
       ALLOCATE(in%receiver(in%nReceiver),STAT=ierr)
       IF (ierr>0) STOP "could not allocate the receivers"
       PRINT 2000
       PRINT '("# n name       x1       x2       x3     rate")'
       PRINT 2000
       DO k=1,in%nReceiver
          CALL getdata(iunit,dataline)
          READ (dataline,*,IOSTAT=ierr) i, &
                  in%receiver(k)%name, &
                  in%receiver(k)%x(1), &
                  in%receiver(k)%x(2), &
                  in%receiver(k)%x(3), &
                  in%receiver(k)%rate

          PRINT '(I3.3,X,a4,4ES9.2E1)',i, &
                  in%receiver(k)%name, &
                  in%receiver(k)%x(1), &
                  in%receiver(k)%x(2), &
                  in%receiver(k)%x(3), &
                  in%receiver(k)%rate

          IF (i .NE. k) THEN
             WRITE_DEBUG_INFO(200)
             WRITE (STDERR,'(a)') TRIM(dataline)
             WRITE (STDERR,'("error in input file: unexpected index")')
             STOP 1
          END IF
       END DO

    END IF

    PRINT 2000
    ! flush standard output
    CALL FLUSH(6)      

2000 FORMAT ("# ----------------------------------------------------------------------------")
   
  END SUBROUTINE init
   
  !-----------------------------------------------
  !> subroutine proj
  !! computes the projection of two vectors u,v
  !! with the weight A, z=u'A v.
  !-----------------------------------------------
  REAL*8 FUNCTION proj(u,v,A)
    REAL*8, DIMENSION(3), INTENT(IN) :: u,v
    REAL*8, DIMENSION(6), INTENT(IN) :: A

    REAL*8, DIMENSION(3) :: w

    CALL mvp(A,v,w)
    proj=dot(u,w)

  END FUNCTION proj

  !-----------------------------------------------
  !> subroutine dot
  !! computes the vector dot product
  !-----------------------------------------------
  REAL*8 FUNCTION dot(u,v)
    REAL*8, DIMENSION(3), INTENT(IN) :: u,v

    dot=u(1)*v(1)+u(2)*v(2)+u(3)*v(3)

  END FUNCTION dot

  !-----------------------------------------------
  !> subroutine mvp
  !! computes the matrix-vector product
  !-----------------------------------------------
  SUBROUTINE mvp(A,x,y)
    REAL*8, DIMENSION(6), INTENT(IN) :: A
    REAL*8, DIMENSION(3), INTENT(IN) :: x
    REAL*8, DIMENSION(3), INTENT(OUT) :: y

    y(1)=A(1)*x(1)+A(2)*x(2)+A(3)*x(3)
    y(2)=A(2)*x(1)+A(4)*x(2)+A(5)*x(3)
    y(3)=A(3)*x(1)+A(5)*x(2)+A(6)*x(3)

  END SUBROUTINE mvp

  !-----------------------------------------------
  !> subroutine trace
  !! compute the trace of a moment tensor
  !-----------------------------------------------
  REAL*8 FUNCTION trace(m)
    REAL*8, DIMENSION(6), INTENT(IN) :: m

    trace=m(1)+m(4)+m(6)

  END FUNCTION trace

  !-----------------------------------------------
  !> subroutine printhelp
  !! displays a help message.
  !-----------------------------------------------
  SUBROUTINE printhelp()

    PRINT '("usage:")'
    PRINT '("")'
    PRINT '("mpirun -n 2 unicycle-waveform [-h] [--dry-run] [--help] [filename]")'
    PRINT '("")'
    PRINT '("options:")'
    PRINT '("   -h                      prints this message and aborts calculation")'
    PRINT '("   --dry-run               abort calculation, only output geometry")'
    PRINT '("   --help                  prints this message and aborts calculation")'
    PRINT '("   --version               print version number and exit")'
    PRINT '("")'
    PRINT '("description:")'
    PRINT '("   simulates synthetic seismograms in an elastic full space.")'
    PRINT '("")'
    CALL FLUSH(6)

  END SUBROUTINE printhelp

  !-----------------------------------------------
  !> subroutine printversion
  !! displays code version.
  !-----------------------------------------------
  SUBROUTINE printversion()

    PRINT '("unicycle-waveform version 1.0.0, compiled on ",a)', __DATE__
    PRINT '("")'
    CALL FLUSH(6)

  END SUBROUTINE printversion

END PROGRAM waveform




