function [L1212,L1213, ...
          L1312,L1313]=computeAverageStressKernelsAntiplaneTriangle(src,rcv,G)
% function COMPUTEAVERAGESTRESSKERNELSANTIPLANETRIANGLE compute the
% stress kernels in plane strain condition for the deformation caused by 
% deforming triangle volume elements on other triangle volume elements using 
% the volume average.
%
%          +-------------+-------------+
%         / \ *   *   * / \ *   *   * / \
%        / * \   rcv   / * \   rcv   / * \
%       /     \ * * * / src \ * * * /     \
%      / * * * \     / * * * \     / * * * \
%     /   rcv   \ * /   rcv   \ * /   rcv   \
%    / *   *   * \ / *   *   * \ / *   *   * \
%   +-------------+-------------+-------------+
%   
% (*) integration point for volume average.
%
% function [L1212,L1213, ...
%           L1312,L1313]=computeAverageStressKernelsAntiplaneTriangle(src,rcv,G)
%
% Lijkl is the stress component Skl due to strain component Eij, 
% both in the reference (unprimed) system of coordinates.
%
% INPUT:
% src.t - 3xN integers describing the source triangle vertices
% src.p - coordinates of the vertices 2*M
% rcv.t - 3xN integers describing the receiver triangle vertices
% rcv.p - coordinates of the vertices 2*M
%
% Author: Sylvain Barbot (sbarbot@usc.edu) - Tokyo, May 14, 2022.

out=cell(2,2);

% diagonal matrix
I=eye(2);

% receiver vertices
K=rcv.p(rcv.t(:,1),:);
L=rcv.p(rcv.t(:,2),:);
M=rcv.p(rcv.t(:,3),:);

% surface integration with Gauss-Legendre quadrature
order=3;
[u,w]=gaussxw(-1,1,order);
[u,v]=meshgrid(u,u);
w=w*w'.*(1-v)/4;
u=u(:)';v=v(:)';
w=repmat(w(:)',size(rcv.t,1),1);

% mapping function
y=@(u,v,A,B,C) A*((1-u).*(1-v))/4+B*(1+u).*(1-v)/4+C*(1+v)/2;

% points for surface integration
x2=y(u,v,K(:,1),L(:,1),M(:,1));
x3=y(u,v,K(:,2),L(:,2),M(:,2));

for k=1:size(src.t,1)
        if 0==mod(fix((k-1)/src.N*1000),50)
            fprintf('.');
        end
        
        % source vertices
        A=src.p(src.t(k,1),:);
        B=src.p(src.t(k,2),:);
        C=src.p(src.t(k,3),:);
        
        % loop over strain components e22, e23, and e33
        for i=1:2
            [s12,s13]=unicycle.ps.greens.computeStressAntiplaneTriangle( ...
                x2(:),x3(:),A,B,C,I(i,1),I(i,2),G);
            
            % surface-averaged stress
            out{1,i}(:,k)=sum(reshape(s12(:),size(rcv.t,1),size(w,2)).*w,2);
            out{2,i}(:,k)=sum(reshape(s13(:),size(rcv.t,1),size(w,2)).*w,2);
        end
end

[L1212,L1213, ...
 L1312,L1313]=deal(out{:});

end

function [xn,wn]=gaussxw(a,b,n)
% GAUSSXW finds sample points xn and weights wn for Gaussian
% quadrature on (a,b) with n nodes
%
%    function [xn wn] = gaussxw(a,b, n)
%
%  Integral = wn(:)' * f (xn(:))
%  or  sum(wn.*f(xn))
%
%  Saves nodes and weights from previous call with same n.
%
% AUTHOR: Trefethen, Spectral Method in Matlab

persistent N x w

% check if we need new x, w
if (isempty(N) == 1 || N ~= n)
    N=n;
    beta=0.5./sqrt(1-(2*(1:N-1)).^(-2));
    T=diag(beta,1)+diag(beta,-1);
    [V,D]=eig(T);
    x=diag(D);
    [x,i]=sort(x);
    w=2*V(1,i).^2;
end

% finds sample x and weights for interval (a,b)
xn=(a+b+x(:)*(b-a))/2;
wn=(b-a)*w(:)/2;
end
