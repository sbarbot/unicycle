function [L1212,L1213, ...
          L1312,L1313]=computeAverageStressKernelsAntiplaneVerticalRectangle(src,rcv,G)
% function COMPUTEAVERAGESTRESSKERNELSANTIPLANEVERTICALRECTANGLE compute
% the stress kernels in antiplane condition for the deformation caused by 
% deforming vertical rectangle volume elements on other vertical rectangle 
% volume elements using the volume average.
%
%     +-------+-----------+---------+---------+
%     |*  *  *| *   *   * | *  *  * | *  *  * |
%     |  rcv  |    rcv    |   rcv   |   rcv   |
%     |*  *  *| *   *   * | *  *  * | *  *  * |
%     |       |    src    |         |         |
%     |*  *  *| *   *   * | *  *  * | *  *  * |
%     +-------+-----------+---------+---------+
%
% (*) integration point for volume average.
%
% function [L1212,L1213, ...
%           L1312,L1313]=computeAverageStressKernelsAntiplaneVerticalRectangle(src,rcv,G)
%
% Lijkl is the stress component Skl due to strain component Eij, 
% both in the reference (unprimed) system of coordinates.
%
% INPUT:
% src.x - east and depth coordinates of the top-middle point 
%         of the source rectangle volume element
% src.T - horizontal thickness of the source volume element
% src.W - depth extend of the source volume element
% rcv.x - east and depth coordinates of the top-middle point
%         of the receiver rectangle volume element
% rcv.W - depth extend of the receiver volume element
% G     - rigidity of the half space.
%
% Author: Sylvain Barbot (sbarbot@usc.edu) - Tokyo, May 14, 2022.

out=cell(2,2);

% diagonal matrix
I=eye(2);

% surface integration with Gauss-Legendre quadrature
order=5;
[u,w]=gaussxw(-1,1,order);
[u,v]=meshgrid(u,u);u=u(:)';v=v(:)';
w=w*w';w=ones(size(src.W,1),1)*w(:)'/4;

% points for surface integration
x2=(rcv.x(:,1)        )+rcv.T*u/2;
x3=(rcv.x(:,2)+rcv.W/2)+rcv.W*v/2;

for k=1:size(src.W,1)
        if 0==mod(fix((k-1)/size(src.W,1)*1000),50)
            fprintf('.');
        end
        
        % loop over strain components e12 and e13
        for i=1:2
            [s12,s13]=unicycle.ap.greens.computeStressAntiplaneVerticalRectangle( ...
                x2(:),x3(:),src.x(k,1),src.x(k,2),src.T(k),src.W(k),I(i,1),I(i,2),G);
            
            % surface-averaged stress
            out{1,i}(:,k)=sum(reshape(s12(:),size(src.W,1),size(w,2)).*w,2);
            out{2,i}(:,k)=sum(reshape(s13(:),size(src.W,1),size(w,2)).*w,2);
        end
end
fprintf('\n');

[L1212,L1213, ...
 L1312,L1313]=deal(out{:});

end

function [xn,wn]=gaussxw(a,b,n)
% GAUSSXW finds sample points xn and weights wn for Gaussian
% quadrature on (a,b) with n nodes
%
%    function [xn wn] = gaussxw(a,b, n)
%
%  Integral = wn(:)' * f (xn(:))
%  or  sum(wn.*f(xn))
%
%  Saves nodes and weights from previous call with same n.
%
% AUTHOR: Trefethen, Spectral Method in Matlab

persistent N x w

% check if we need new x, w
if (isempty(N) == 1 || N ~= n)
    N=n;
    beta=0.5./sqrt(1-(2*(1:N-1)).^(-2));
    T=diag(beta,1)+diag(beta,-1);
    [V,D]=eig(T);
    x=diag(D);
    [x,i]=sort(x);
    w=2*V(1,i).^2;
end

% finds sample x and weights for interval (a,b)
xn=(a+b+x(:)*(b-a))/2;
wn=(b-a)*w(:)/2;
end
