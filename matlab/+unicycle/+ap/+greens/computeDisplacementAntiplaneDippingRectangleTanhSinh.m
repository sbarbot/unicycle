function u1=computeDisplacementAntiplaneDippingRectangleTanhSinh( ...
    x2,x3,q2,q3,T,W,phi,epsv12p,epsv13p,varargin)
% function COMPUTEDISPLACEMENTANTIPLANEDIPPINGRECTANGLETANHSINH computes
% the displacement field associated with deforming rectangle volume element
% considering the following geometry using the double-exponential quadrature.
%
%              surface
%      -------------+-------------- E (x2)
%                   |
%                   | dip /
%                   |----/  . w
%                   |   /     . i 
%                   |  /        . d           
%                   | /           . t     
%                   |/              . h   
%           q2,q3 ->@                 .
%                  /|                   . 
%                 / :                  /  
%                /  |                 /  s
%               /   :                /  s
%              /    |               /  e
%             /     :              /  n
%               .   |             /  k
%                 . :            /  c
%                   .           /  i
%                   : .        /  h
%                   |   .     /  t
%                   :     .  /  
%                   |       .    
%                   D (x3)
%
% Input:
% x2, x3             east and depth coordinates of the observation point,
% q2, q3             east and depth coordinates of the volume element,
% T, W               thickness and width of the volume element,
% epsv12, epsv13     source strain component 12 and 13 in the volume element
%                    in the system of reference tied to the volume element.
%
% Options:
% 'precision'        intervals used in trapezoidal rule [0.001]
% 'bound'            bound of integration of the double exponential [3.5]
%
% Output:
% u1                 displacement component in the north (along-strike)
%                    direction.
%
% Author: Sylvain Barbot (sbarbot@usc.edu)

assert(min(x3(:))>=0,'depth must be positive.');

% process optional input
p = inputParser;
p.addParameter('precision',0.001,@validatePrecision);
p.addParameter('bound',3.5,@validateBound);
p.parse(varargin{:});
optionStruct = p.Results;

% rotate the eigenstrain from the shear-zone centric to the reference
% coordinate system
epsv12= sind(phi)*epsv12p+cosd(phi)*epsv13p;
epsv13=-cosd(phi)*epsv12p+sind(phi)*epsv13p;

y2=@(y2p,y3p) +y2p*sind(phi)+y3p*cosd(phi)+q2;
y3=@(y2p,y3p) -y2p*cosd(phi)+y3p*sind(phi)+q3;

% Green's functions
G11=@(y2,y3) log((x2-y2).^2+(x3-y3).^2)+log((x2-y2).^2+(x3+y3).^2);

% function IU1 is the integrand for displacement component u1
IU1=@(x) -1/2/pi*( ...
    sind(phi)*( epsv12*W/2*(G11(y2(T/2,(1+x)*W/2),y3(T/2,(1+x)*W/2))-G11(y2(-T/2,(1+x)*W/2),y3(-T/2,(1+x)*W/2))) ...
               +epsv13*T/2*(G11(y2(x*T/2,W),      y3(x*T/2,W))      -G11(y2(x*T/2,0),       y3(x*T/2,0)))) ...
   +cosd(phi)*( epsv12*T/2*(G11(y2(x*T/2,W),      y3(x*T/2,W))      -G11(y2(x*T/2,0),       y3(x*T/2,0))) ...
               -epsv13*W/2*(G11(y2(T/2,(1+x)*W/2),y3(T/2,(1+x)*W/2))-G11(y2(-T/2,(1+x)*W/2),y3(-T/2,(1+x)*W/2)))) ...
               );

% numerical solution
h=optionStruct.precision;
n=fix(1/h*optionStruct.bound);
u1=zeros(size(x2));

for k=-n:n
    wk=(0.5*h*pi*cosh(k*h))./(cosh(0.5*pi*sinh(k*h))).^2;
    xk=tanh(0.5*pi*sinh(k*h));
    u1=u1+wk*IU1(xk);
end

end

function p = validatePrecision(x)
if ~(isreal(x) && isscalar(x) && x > 0)
    error(message('MATLAB:mcmc:invalidPrecision'));
end
p = true;
end

function p = validateBound(x)
if ~(isreal(x) && isscalar(x) && x > 0)
    error(message('MATLAB:mcmc:invalidBound'));
end
p = true;
end











