function u1=computeDisplacementAntiplaneTriangleMixedQuad( ...
    x2,x3,A,B,C,e12,e13)
% function COMPUTEDISPLACEMENTANTIPLANETRIANGLEMIXEDQUAD computes
% the displacement field associated with deforming triangle volume element
% considering the following geometry using the double-exponential or the 
% Gauss quadrature based on distance from the circumcenter.
%
%              surface
%      -------------+-------------- E (x2)
%                   |
%                   |     + A
%                   |    /  .
%                   |   /     .
%                   |  /        .
%                   | /           .
%                   |/              + B
%                   /            .
%                  /|          /  
%                 / :       .
%                /  |    /
%               /   : .
%              /   /|
%             / .   :
%            +      |
%          C        :
%                   |
%                   D (x3)
%
% Input:
% x2, x3             east and depth coordinates of the observation point,
% A, B, C            east and depth coordinates of the vertices,
% e12, e13           source strain component 12 and 13 in the volume element.
%
% Output:
% u1                 displacement component in the north (along-strike)
%                    direction.
%
% Author: Sylvain Barbot (sbarbot@usc.edu) - April 7, 2018, Los Angeles.

assert(min(x3(:))>=0,'depth must be positive.');

% unit vectors
nA = [C(2)-B(2);
      B(1)-C(1)]/norm(C-B);
nB = [C(2)-A(2);
      A(1)-C(1)]/norm(C-A);
  
% check that unit vectors are pointing outward
if (nA'*(A(:)-(B(:)+C(:))/2))>0
    nA=-nA;
end
if (nB'*(B(:)-(A(:)+C(:))/2))>0
    nB=-nB;
end

% circumcenter of triangle
O=((B(:)+C(:))+nA*(nB(2)*(B(1)-A(1))-nB(1)*(B(2)-A(2)))/(nA(2)*nB(1)-nA(1)*nB(2)))/2;

% circumcircle radius
r=norm(O(:)-A(:));

inside=sqrt((x2-O(1)).^2+(x3-O(2)).^2)<1.5*r;
outside=~inside;

% initiate empty array
u1=zeros(size(x2));

% points outside the circumcircle
if numel(x3(outside))>0
    % numerical solution with Gauss quadrature
    u1(outside)=computeDisplacementAntiplaneTriangleGauss( ...
        x2(outside),x3(outside),A,B,C,e12,e13);
end

% points inside the circumcircle
if numel(x3(inside))>0
    % numerical solution with double-exponential quadrature
    %u1(inside)=computeDisplacementAntiplaneTriangleTanhSinh( ...
    %    x2(inside),x3(inside),A,B,C,e12,e13);
    % numerical solution with Gauss-Legendre quadrature
    u1(inside)=computeDisplacementAntiplaneTriangleGauss( ...
        x2(inside),x3(inside),A,B,C,e12,e13,'N',450);
end

end












