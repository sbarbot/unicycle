function u1=computeDisplacementAntiplaneTriangleTanhSinh( ...
    x2,x3,A,B,C,e12,e13,varargin)
% function COMPUTEDISPLACEMENTANTIPLANETRIANGLETANHSINH computes
% the displacement field associated with deforming triangle volume element
% considering the following geometry using the tanhsinh quadrature.
%
%              surface
%      -------------+-------------- E (x2)
%                   |
%                   |     + A
%                   |    /  .
%                   |   /     .
%                   |  /        .
%                   | /           .
%                   |/              + B
%                   /            .
%                  /|          /  
%                 / :       .
%                /  |    /
%               /   : .
%              /   /|
%             / .   :
%            +      |
%          C        :
%                   |
%                   D (x3)
%
% Input:
% x2, x3             east coordinates and depth of the observation point,
% A, B, C            east and depth coordinates of the vertices,
% e12, e13           source strain component 12 and 13 in the volume element.
%
% Output:
% u1                 displacement component in the north (along-strike)
%                    direction.
%
% Options:
% 'precision'        intervals used in trapezoidal rule [0.001]
% 'bound'            bound of integration of the double exponential [3.5]
%
% Author: Sylvain Barbot (sbarbot@usc.edu) - Feb 8, 2018, Los Angeles.

assert(min(x3(:))>=0,'depth must be positive.');

% process optional input
p = inputParser;
p.addParameter('precision',0.001,@validatePrecision);
p.addParameter('bound',3.5,@validateBound);
p.parse(varargin{:});
optionStruct = p.Results;

% unit vectors
nA = [C(2)-B(2);
      B(1)-C(1)]/norm(C-B);
nB = [C(2)-A(2);
      A(1)-C(1)]/norm(C-A);
nC = [B(2)-A(2);
      A(1)-B(1)]/norm(B-A);
  
% check that unit vectors are pointing outward
if (nA'*(A(:)-(B(:)+C(:))/2))>0
    nA=-nA;
end
if (nB'*(B(:)-(A(:)+C(:))/2))>0
    nB=-nB;
end
if (nC'*(C(:)-(A(:)+B(:))/2))>0
    nC=-nC;
end

% parameterized line integral
y2=@(t,A,B) (A(1)+B(1))/2+t*(B(1)-A(1))/2;
y3=@(t,A,B) (A(2)+B(2))/2+t*(B(2)-A(2))/2;

% Green's functions
G11=@(y2,y3) -1/4/pi*(log((x2-y2).^2+(x3-y3).^2)+log((x2-y2).^2+(x3+y3).^2));

% function IU1 is the integrand for displacement component u1
% (note that 2*e12 and R/2 cancel the factors of two.)
IU1=@(t) ...
    (e12*nC(1)+e13*nC(2))*norm(B-A)*G11(y2(t,A,B),y3(t,A,B)) ...
   +(e12*nA(1)+e13*nA(2))*norm(C-B)*G11(y2(t,B,C),y3(t,B,C)) ...
   +(e12*nB(1)+e13*nB(2))*norm(A-C)*G11(y2(t,C,A),y3(t,C,A));

% numerical solution with tanh-sinh quadrature
h=optionStruct.precision;
n=fix(1/h*optionStruct.bound);
u1=zeros(size(x2));

for k=-n:n
    wk=(0.5*h*pi*cosh(k*h))./(cosh(0.5*pi*sinh(k*h))).^2;
    xk=tanh(0.5*pi*sinh(k*h));
    u1=u1+wk*IU1(xk);
end

end

function p = validatePrecision(x)
if ~(isreal(x) && isscalar(x) && x > 0)
    error(message('MATLAB:mcmc:invalidPrecision'));
end
p = true;
end

function p = validateBound(x)
if ~(isreal(x) && isscalar(x) && x > 0)
    error(message('MATLAB:mcmc:invalidBound'));
end
p = true;
end











