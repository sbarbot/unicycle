function [s12,s13]=computeStressAntiplaneVerticalRectangle(x2,x3,y2,y3,T,W,e12,e13,G)
% function COMPUTESTRESSANTIPLANEVERTICALRECTANGLE computes the
% stress field associated with a deforming vertical rectangle volume
% element considering the following geometry
%
%              surface
%      -------------+-------------- E (x2)
%                   |
%                   |         y2,y3
%                   |     +-----+-----+
%                   |     |           |
%                   |     |           |
%                   |     |           |
%                   |     |           | W
%                   |     |           |
%                   |     |           |
%                   |     |           |
%                   |     +-----------+
%                   |           T
%                   |
%                   y3 (x3)
%
% Input:
% x2, x3             east and depth coordinates of the observation point,
% y2, y3             east and depth coordinates of the top-middle of the rectangle,
% T, W               thickness and width of the volume element,
% e12, e13           source strain component 12 and 13 in the volume element,
% G                  rigidity in the half space.
%
% Output:
% s12                horizontal stress component,
% s13                vertical stress component.
%
% Author: Sylvain Barbot (sbarbot@usc.edu)

assert(min(x3(:))>=0,'observation depth must be positive.');
assert(        y3>=0,'source depth must be positive.');

% boxcar function
boxc=@(x) (x+0.5>=0)-(x-0.5>=0);

% relative distance
r2=x2-y2;
r3m=x3-y3;
r3p=x3+y3;

% horizontal shear
s12=e12*G/pi*( ...
      atan((r3m  )./(r2+T/2))-atan((r3m  )./(r2-T/2)) ...
     +atan((r3m-W)./(r2-T/2))-atan((r3m-W)./(r2+T/2)) ...
     -atan((r3p+W)./(r2-T/2))-atan((r3p  )./(r2+T/2)) ...
     +atan((r3p  )./(r2-T/2))+atan((r3p+W)./(r2+T/2)))...
   +e13*G/(2*pi)*( ...
      log((r2-T/2).^2+(r3m-W).^2)-log((r2+T/2).^2+(r3m-W).^2) ...
     +log((r2-T/2).^2+(r3p+W).^2)-log((r2+T/2).^2+(r3p+W).^2) ...
     -log((r2-T/2).^2+(r3m  ).^2)+log((r2+T/2).^2+(r3m  ).^2) ...
     -log((r2-T/2).^2+(r3p  ).^2)+log((r2+T/2).^2+(r3p  ).^2)) ...
   -2*G*e12*boxc(r2/T).*boxc((r3m-W/2)/W);

% vertical shear
s13=e13*G/pi*( ...
      atan((r2+T/2)./(r3m  ))-atan((r2-T/2)./(r3m  )) ...
     -atan((r2+T/2)./(r3m-W))+atan((r2-T/2)./(r3m-W)) ...
     +atan((r2+T/2)./(r3p  ))-atan((r2-T/2)./(r3p  )) ...
     -atan((r2+T/2)./(r3p+W))+atan((r2-T/2)./(r3p+W))) ...
   +e12*G/(2*pi)*( ...
      log((r2-T/2).^2+(r3m-W).^2)-log((r2+T/2).^2+(r3m-W).^2) ...
     -log((r2-T/2).^2+(r3p+W).^2)+log((r2+T/2).^2+(r3p+W).^2) ...
     -log((r2-T/2).^2+(r3m  ).^2)+log((r2+T/2).^2+(r3m  ).^2) ...
     +log((r2-T/2).^2+(r3p  ).^2)-log((r2+T/2).^2+(r3p  ).^2)) ...
   -2*G*e13*boxc(r2/T).*boxc((r3m-W/2)/W);

end


