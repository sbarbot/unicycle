function [s12,s13]=computeStressAntiplaneVerticalSegment(x2,x3,y2,y3,W,slip,G)
% function COMPUTESTRESSANTIPLANEVERTICALSEGMENT computes the
% stress field associated with slip on a vertical fault element
% considering the following geometry
%
%              free surface
%      -------------+-------------- E (x2)
%                   |
%                   |     + (y2,y3)
%                   |     | 
%                   |     | 
%                   |     | 
%                   |     | slip
%                   |     | 
%                   |     | 
%                   |     | 
%                   |     + (y2,y3+W)
%                   |
%                   D (x3)
%
% Input:
% x2, x3             east and depth coordinates of the observation point,
% y2, y3             east and depth coordinates of the top edge,
% W                  width of the fault segment,
% slip               uniform slip,
% G                  rigidity in the half space.
%
% Output:
% s12                horizontal stress component,
% s13                vertical stress component.
%
% Author: Sylvain Barbot (sbarbot@usc.edu)

% indefinite integrals for u1,2
IU12=@(y3) 1/(2*pi)*( (x3-y3)./((x2-y2).^2+(x3-y3).^2) );

% indefinite integrals for u1,3
IU13=@(y3) 1/(2*pi)*( -(x2-y2)./((x2-y2).^2+(x3-y3).^2) );

% stress components
s12=slip*G*(IU12(y3+W)-IU12(y3)+IU12(-y3)-IU12(-y3-W));
s13=slip*G*(IU13(y3+W)-IU13(y3)+IU13(-y3)-IU13(-y3-W));

end
