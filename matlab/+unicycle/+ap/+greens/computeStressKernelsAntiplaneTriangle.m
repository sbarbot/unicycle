function [L1212,L1213, ...
          L1312,L1313]=computeStressKernelsAntiplaneTriangle(src,rcv,G)
% function COMPUTESTRESSKERNELSANTIPLANETRIANGLE compute the stress
% kernels in antiplane condition for the deformation caused by deforming
% triangle volume elements on other triangle volume elements using the
% collocation method.
%
%          +---------+---------+
%         / \  rcv  / \  rcv  / \
%        /   \  +  /src\  +  /   \
%       /  +  \   /  +  \   /  +  \
%      /  rcv  \ /  rcv  \ /  rcv  \
%     +---------+---------+---------+
%
% (+) representative point
%
% function [L1212,L1213, ...
%           L1312,L1313]=computeStressKernelsAntiplaneTriangle(src,rcv,G)
%
% Lijkl is the stress component Skl due to strain component Eij, 
% both in the reference (unprimed) system of coordinates.
%
% INPUT:
% src.t - 3xN integers describing the source triangle vertices
% src.p - coordinates of the vertices 2*M
% rcv.t - 3xN integers describing the receiver triangle vertices
% rcv.p - coordinates of the vertices 2*M
%
% Author: Sylvain Barbot (sbarbot@usc.edu) - Tokyo, May 14, 2022.

out=cell(2,2);

% diagonal matrix
I=eye(2);

% collocation points
xc=(rcv.p(rcv.t(:,1),:)+rcv.p(rcv.t(:,2),:)+rcv.p(rcv.t(:,3),:))/3;

for k=1:size(src.t,1)
        if 0==mod(fix((k-1)/size(src.t,1)*1000),50)
            fprintf('.');
        end
        
        A=src.p(src.t(k,1),:);
        B=src.p(src.t(k,2),:);
        C=src.p(src.t(k,3),:);
        
        for i=1:2
            [s12,s13]=unicycle.ps.greens.computeStressAntiplaneTriangle( ...
                xc(:,1),xc(:,2),A,B,C,I(i,1),I(i,2),G);
            
            out{1,i}(:,k)=s12(:);
            out{2,i}(:,k)=s13(:);
        end
end

[L1212,L1213, ...
 L1312,L1313]=deal(out{:});

end