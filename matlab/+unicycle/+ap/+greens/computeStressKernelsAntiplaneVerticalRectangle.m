function [L1212,L1213, ...
          L1312,L1313]=computeStressKernelsAntiplaneVerticalRectangle(src,rcv,G)
% function COMPUTESTRESSKERNELSANTIPLANEVERTICALRECTANGLE compute the stress
% kernels in antiplane condition for the deformation caused by deforming
% vertical rectangle volume elements on other vertical rectangle volume 
% elements using the collocation method.
%
%     +-----+-----------+-----+-------+
%     |     |           |     |       |
%     | rcv |    rcv    | rcv |  rcv  |
%     |  +  |     +     |  +  |   +   |
%     |     |    src    |     |       |
%     |     |           |     |       |
%     +-----+-----------+-----+-------+
%
% (+) representative point
%
% function [L1212,L1213, ...
%           L1312,L1313]=computeStressKernelsAntiplaneVerticalRectangle(src,rcv,G)
%
% Lijkl is the stress component Skl due to strain component Eij, 
% both in the reference (unprimed) system of coordinates.
%
% INPUT:
% src.x - east and depth coordinates of the top-middle point 
%         of the source rectangle volume element
% src.T - horizontal thickness of the source volume element
% src.W - depth extend of the source volume element
% rcv.x - east and depth coordinates of the top-middle point
%         of the receiver rectangle volume element
% rcv.W - depth extend of the receiver volume element
% G     - rigidity of the half space.
%
% Author: Sylvain Barbot (sbarbot@usc.edu) - Tokyo, May 14, 2022.

out=cell(2,2);

% diagonal matrix
I=eye(2);

% collocation points
x2=rcv.x(:,1);
x3=rcv.x(:,2)+rcv.W/2;

for k=1:size(src.W,1)
        if 0==mod(fix((k-1)/size(src.W,1)*1000),50)
            fprintf('.');
        end
        
        % loop over strain components e12 and e13
        for i=1:2
            [s12,s13]=unicycle.ap.greens.computeStressAntiplaneVerticalRectangle( ...
                x2,x3,src.x(k,1),src.x(k,2),src.T(k),src.W(k),I(i,1),I(i,2),G);
            
            out{1,i}(:,k)=s12(:);
            out{2,i}(:,k)=s13(:);
        end
end
fprintf('\n');

[L1212,L1213, ...
 L1312,L1313]=deal(out{:});

end