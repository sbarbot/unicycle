function exportxyz_rfaults(data,x,y,z,fname)
% EXPORTXYZ_RFAULTS export data to .xyz files for processing with GMT
% tools
%
%   exportxyz_rfaults(data,x,y,z,fname)
% 
% input:
% data            - dataset associated with the geometry
% x1,x2,x3        - list of coordinates of the patch corners (north, east, down)
% fname           - name (dir/example.flt) of the .flt Relax file

fid=fopen(fname,'wt');

fprintf(fid,'# export from unicycle\n');

for k=1:length(data)
    fprintf(fid,'> -Z%f\n%f %f %f\n%f %f %f\n%f %f %f\n%f %f %f\n',data(k),[x(:,k) y(:,k) z(:,k)]');
end

fclose(fid);