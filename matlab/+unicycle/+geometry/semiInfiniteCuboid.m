classdef semiInfiniteCuboid < handle
    properties
        N;
        % identification number
        id;
        % volume element position (upper corner at mid thickness and centre)
        x;xc;
        % volume element dimension (length and thickness)
        L;T;
        % volume element orientation (degrees)
        strike;dip;
        % volume element strain
        eps;
        % unit vectors in strike, dip, normal and rake directions
        sv;dv;nv;
        % structure of levels
        levels;
        % hash table directing level name to level index
        levelHashTable;
        % earth model
        earthModel;
        % plotting scale
        scale=1;
    end
    
    methods
        %% % % % % % % % % % % % % % % % % %
        %                                  %
        %      c o n s t r u c t o r       %
        %                                  %
        % % % % % % % % % % % % % % % % % %%
        function obj = semiInfiniteCuboid()
            % SEMIINFINITECUBOID is a meta class representing the geometry of
            % vertical, semi-infinite cuboid volume elements, defined in terms
	    % of position, orientation, and strain, as illustrated below:
            %
            %                      N (x1)
            %                     /
            %                    /| strike (theta)
            %                   @----------------- E (x2)
            %                   :
            %                   |     +--------------------------+
            %                   :    /                          /|
            %                   |   /                          / |
            %            Z (x3) :  /                          / s|
            %                   | /                          / s .
            %                   :/                          / e  .
            %        q1,q2,q3 ->+--------------------------+ n   .
            %                  /      l e n g t h  (L)    / k
            %                 /                          / c
            %                /                          / i
            %               /                          / h
            %              /                          / t
            %             +--------------------------+
            %             |                          |
            %             |                          |
            %             |                          |
            %             .                          .
            %             .                          .
            %             .                          .
            %             .                          .
            %
	    %
	    % AUTHOR: S. Barbot (sbarbot@usc.edu), Feb. 11, 2020, Los Angeles
            %
            % SEE ALSO: unicycle, unicycle.geometry.triangle
            
            if 0==nargin
                obj.N=0;
                return
            end
            
        end % constructor
        
        function [varargout]=tractionKernels(obj,rcv)
            % TRACTIONKERNELS computes the traction on receiver faults due
            % to strain in volume elements.
            %
            % rcv - receiver fault
            
            varargout = cell(1,nargout);
            [varargout{:}]=obj.earthModel.tractionKernels(obj,rcv);
        end
        
        function [varargout]=stressKernels(obj,rcv)
            % STRESSKERNELS computes the stress on receiver volume elements due
            % to motion of dislocations.
            %
            % rcv - receiver fault
            
            varargout = cell(1,nargout);
            [varargout{:}]=obj.earthModel.stressKernels(obj,rcv);
        end
        
        function [varargout]=displacementKernels(obj,x,vecsize)
            % DISPLACEMENTKERNELS computes the stress on receiver faults due to
            % motion of dislocations.
            %
            % x       - observations points coordinates
            % vecsize - length of displacement vector
            
            varargout = cell(1,nargout);
            [varargout{:}]=obj.earthModel.displacementKernels(obj,x,vecsize);
        end
        
        function [xp,yp,zp,dim]=computeVertexPosition(o)
            % COMPUTEVERTEXPOSITION computes the position of vertices
            %
            % SEE ALSO: unicycle
            
            xyz=cell(3,1);
            for k=1:3
                xyz{k}=[ ...
                    o.xc(:,k)-o.sv(:,k).*o.L/2-o.dv(:,k).*o.W/2-o.nv(:,k).*o.T/2, ...
                    o.xc(:,k)-o.sv(:,k).*o.L/2+o.dv(:,k).*o.W/2-o.nv(:,k).*o.T/2, ...
                    o.xc(:,k)-o.sv(:,k).*o.L/2-o.dv(:,k).*o.W/2+o.nv(:,k).*o.T/2, ...
                    o.xc(:,k)-o.sv(:,k).*o.L/2+o.dv(:,k).*o.W/2+o.nv(:,k).*o.T/2, ...
                    o.xc(:,k)+o.sv(:,k).*o.L/2-o.dv(:,k).*o.W/2-o.nv(:,k).*o.T/2, ...
                    o.xc(:,k)+o.sv(:,k).*o.L/2+o.dv(:,k).*o.W/2-o.nv(:,k).*o.T/2, ...
                    o.xc(:,k)+o.sv(:,k).*o.L/2-o.dv(:,k).*o.W/2+o.nv(:,k).*o.T/2, ...
                    o.xc(:,k)+o.sv(:,k).*o.L/2+o.dv(:,k).*o.W/2+o.nv(:,k).*o.T/2, ...
                    o.xc(:,k)-o.sv(:,k).*o.L/2-o.dv(:,k).*o.W/2-o.nv(:,k).*o.T/2, ...
                    o.xc(:,k)-o.sv(:,k).*o.L/2+o.dv(:,k).*o.W/2-o.nv(:,k).*o.T/2, ...
                    o.xc(:,k)+o.sv(:,k).*o.L/2-o.dv(:,k).*o.W/2-o.nv(:,k).*o.T/2, ...
                    o.xc(:,k)+o.sv(:,k).*o.L/2+o.dv(:,k).*o.W/2-o.nv(:,k).*o.T/2, ...
                    o.xc(:,k)-o.sv(:,k).*o.L/2-o.dv(:,k).*o.W/2+o.nv(:,k).*o.T/2, ...
                    o.xc(:,k)-o.sv(:,k).*o.L/2+o.dv(:,k).*o.W/2+o.nv(:,k).*o.T/2, ...
                    o.xc(:,k)+o.sv(:,k).*o.L/2-o.dv(:,k).*o.W/2+o.nv(:,k).*o.T/2, ...
                    o.xc(:,k)+o.sv(:,k).*o.L/2+o.dv(:,k).*o.W/2+o.nv(:,k).*o.T/2, ...
                    o.xc(:,k)-o.sv(:,k).*o.L/2-o.dv(:,k).*o.W/2-o.nv(:,k).*o.T/2, ...
                    o.xc(:,k)+o.sv(:,k).*o.L/2-o.dv(:,k).*o.W/2-o.nv(:,k).*o.T/2, ...
                    o.xc(:,k)-o.sv(:,k).*o.L/2-o.dv(:,k).*o.W/2+o.nv(:,k).*o.T/2, ...
                    o.xc(:,k)+o.sv(:,k).*o.L/2-o.dv(:,k).*o.W/2+o.nv(:,k).*o.T/2, ...
                    o.xc(:,k)-o.sv(:,k).*o.L/2+o.dv(:,k).*o.W/2-o.nv(:,k).*o.T/2, ...
                    o.xc(:,k)+o.sv(:,k).*o.L/2+o.dv(:,k).*o.W/2-o.nv(:,k).*o.T/2, ...
                    o.xc(:,k)-o.sv(:,k).*o.L/2+o.dv(:,k).*o.W/2+o.nv(:,k).*o.T/2, ...
                    o.xc(:,k)+o.sv(:,k).*o.L/2+o.dv(:,k).*o.W/2+o.nv(:,k).*o.T/2, ...
                    ]';
            end
            [xp,yp,zp]=deal(xyz{:});
            dim=24;
            
        end
        
        %% % % % % % % % % % % % % % % % % % % % % % % % % %
        %                                                  %
        %       plot contour of slip patches in 3d         %
        %                                                  %
        % % % % % % % % % % % % % % % % % % % % % % % % % %%
        function plotEdges(obj,varargin)
            % PLOTEDGES plot the 12 edges of volume elements as
            % follows:
            %
            %           +-------+
            %          /       /
            %         /   +   /
            %        /       /
            %       +-------+
            %
            % SEE ALSO: unicycle
            
            if 1>obj.N
                fprintf('unicycle.geometry.semiInfiniteCuboid: nothing to plot\n');
                return
            end
            
            if 1>=nargin
                
                % 4 segments
                pm=[1,-1];
                
                x1=[];
                x2=[];
                x3=[];
                
                % 2 along-strike segments
                for k=1:2
                    x1=[x1,[obj.xc(:,1)-obj.sv(:,1).*obj.L/2+pm(k)*obj.nv(:,1).*obj.T/2, ...
                        obj.xc(:,1)+obj.sv(:,1).*obj.L/2+pm(k)*obj.nv(:,1).*obj.T/2, ...
                        NaN(size(obj.sv(:,1)))]'];
                    x2=[x2,[obj.xc(:,2)-obj.sv(:,2).*obj.L/2+pm(k)*obj.nv(:,2).*obj.T/2, ...
                        obj.xc(:,2)+obj.sv(:,2).*obj.L/2+pm(k)*obj.nv(:,2).*obj.T/2, ...
                        NaN(size(obj.sv(:,2)))]'];
                    x3=[x3,[obj.xc(:,3)-obj.sv(:,3).*obj.L/2+pm(k)*obj.nv(:,3).*obj.T/2, ...
                        obj.xc(:,3)+obj.sv(:,3).*obj.L/2+pm(k)*obj.nv(:,3).*obj.T/2, ...
                        NaN(size(obj.sv(:,3)))]'];
                end
                
                % 2 normal-direction segments
                for j=1:2
                    x1=[x1,[obj.xc(:,1)+pm(j)*obj.sv(:,1).*obj.L/2+obj.nv(:,1).*obj.T/2, ...
                        obj.xc(:,1)+pm(j)*obj.sv(:,1).*obj.L/2-obj.nv(:,1).*obj.T/2, ...
                        NaN(size(obj.sv(:,1)))]'];
                    x2=[x2,[obj.xc(:,2)+pm(j)*obj.sv(:,2).*obj.L/2+obj.nv(:,2).*obj.T/2, ...
                        obj.xc(:,2)+pm(j)*obj.sv(:,2).*obj.L/2-obj.nv(:,2).*obj.T/2, ...
                        NaN(size(obj.sv(:,2)))]'];
                    x3=[x3,[obj.xc(:,3)+pm(j)*obj.sv(:,3).*obj.L/2+obj.nv(:,3).*obj.T/2, ...
                        obj.xc(:,3)+pm(j)*obj.sv(:,3).*obj.L/2-obj.nv(:,3).*obj.T/2, ...
                        NaN(size(obj.sv(:,3)))]'];
                end
                plot3(x1(:)/obj.scale,x2(:)/obj.scale,x3(:)/obj.scale,'k-');
                
            else
                % horizontal slice
                [xp,yp,zp,up]=unicycle.geometry.transform4patch_general(...
                    obj.x(:,1)-obj.nv(:,1).*obj.T/2, ...
                    obj.x(:,2)-obj.nv(:,2).*obj.T/2, ...
                  -(obj.x(:,3)-obj.nv(:,3).*obj.T/2), ...
                    varargin{1},...
                    obj.L(:),obj.T(:),obj.dip(:)-90,obj.strike(:));
                patch(xp/obj.scale,yp/obj.scale,-zp/obj.scale,up,'EdgeColor','None');
            end
            
        end
        
        %% % % % % % % % % % % % % % % % % % % % % % % % % %
        %                                                  %
        %       plot contour of slip patches in 3d         %
        %                                                  %
        % % % % % % % % % % % % % % % % % % % % % % % % % %%
        function plotEdgesById(obj,index,varargin)
            % PLOTEDGESBYID plot the 12 edges of volume elements as
            % follows:
            %
            %           +-------+
            %          /       /
            %         /   +   /
            %        /       /
            %       +-------+
            %
            % SEE ALSO: unicycle
            
            if 1>obj.N
                fprintf('unicycle.geometry.semiInfiniteCuboid: nothing to plot\n');
                return
            end
            
            if 2>=nargin
                
                % 4 segments
                pm=[1,-1];
                
                % 2 along-strike segments
                for k=1:2
                    x1=[obj.xc(index,1)-obj.sv(index,1).*obj.L(index)/2+pm(k)*obj.nv(index,1).*obj.T(index)/2, ...
                        obj.xc(index,1)+obj.sv(index,1).*obj.L(index)/2+pm(k)*obj.nv(index,1).*obj.T(index)/2, ...
                        NaN(size(obj.sv(index,1)))]';
                    x2=[obj.xc(index,2)-obj.sv(index,2).*obj.L(index)/2+pm(k)*obj.nv(index,2).*obj.T(index)/2, ...
                        obj.xc(index,2)+obj.sv(index,2).*obj.L(index)/2+pm(k)*obj.nv(index,2).*obj.T(index)/2, ...
                        NaN(size(obj.sv(index,2)))]';
                    x3=[obj.xc(index,3)-obj.sv(index,3).*obj.L(index)/2+pm(k)*obj.nv(index,3).*obj.T(index)/2, ...
                        obj.xc(index,3)+obj.sv(index,3).*obj.L(index)/2+pm(k)*obj.nv(index,3).*obj.T(index)/2, ...
                        NaN(size(obj.sv(index,3)))]';
                    
                    plot3(x1(:)/obj.scale,x2(:)/obj.scale,x3(:)/obj.scale,'k-');
                end
                
                % 2 normal-direction segments
                for j=1:2
                    x1=[obj.xc(index,1)+pm(j)*obj.sv(index,1).*obj.L(index)/2+obj.nv(index,1).*obj.T(index)/2, ...
                        obj.xc(index,1)+pm(j)*obj.sv(index,1).*obj.L(index)/2-obj.nv(index,1).*obj.T(index)/2, ...
                        NaN(size(obj.sv(index,1)))]';
                    x2=[obj.xc(index,2)+pm(j)*obj.sv(index,2).*obj.L(index)/2+obj.nv(index,2).*obj.T(index)/2, ...
                        obj.xc(index,2)+pm(j)*obj.sv(index,2).*obj.L(index)/2-obj.nv(index,2).*obj.T(index)/2, ...
                        NaN(size(obj.sv(index,2)))]';
                    x3=[obj.xc(index,3)+pm(j)*obj.sv(index,3).*obj.L(index)/2+obj.nv(index,3).*obj.T(index)/2, ...
                        obj.xc(index,3)+pm(j)*obj.sv(index,3).*obj.L(index)/2-obj.nv(index,3).*obj.T(index)/2, ...
                        NaN(size(obj.sv(index,3)))]';
                    
                    plot3(x1(:)/obj.scale,x2(:)/obj.scale,x3(:)/obj.scale,'k-');
                end
                
            else
                % horizontal slice
                [xp,yp,zp,up]=unicycle.geometry.transform4patch_general(...
                    obj.x(index,1)-obj.nv(index,1).*obj.T(index)/2, ...
                    obj.x(index,2)-obj.nv(index,2).*obj.T(index)/2, ...
                  -(obj.x(index,3)-obj.nv(index,3).*obj.T(index)/2), ...
                    varargin{1},...
                    obj.L(index),obj.T(index),obj.dip(index)-90,obj.strike(index));
                patch(xp/obj.scale,yp/obj.scale,-zp/obj.scale,up,'EdgeColor','None');
            end
            
        end
        
        %% % % % % % % % % % % % % % % % % % % % % % % % % %
        %                                                  %
        %       plot contour of slip patches in 3d         %
        %                                                  %
        % % % % % % % % % % % % % % % % % % % % % % % % % %%
        function plotCentroid(obj,varargin)
            % PLOTCENTROID plot the centroid of volume elements as
            % follows:
            %
            %           +-------+
            %          /       /
            %         /   +   /
            %        /       /
            %       +-------+
            %
            % SEE ALSO: unicycle
            
            if 1>obj.N
                fprintf('unicycle.geometry.semiInfiniteCuboid: nothing to plot\n');
                return
            end
            
            if 1>=nargin
                plot3(obj.xc(:,1)/obj.scale,obj.xc(:,2)/obj.scale,obj.xc(:,3)/obj.scale,'k+');
            end
        end
        
        function plotById(obj,shznum)
            % PLOTBYID plot the 4 edges of volume elements based on the
            % its number:
            % 
            %
            %          +-------+
            %         /       /
            %        /       /
            %       +-------+
            %
            % EXAMPLE:                                                   
            %   shz.plotById(2) or shz.plotById([1:3])
            
            if 1>obj.N
                fprintf('unicycle.geometry.semiInfiniteCuboid: nothing to plot\n');
                return
            end
            
            % 4 segments

            plot3(obj.xc(:,1)/obj.scale,obj.xc(:,2)/obj.scale,obj.xc(:,3)/obj.scale,'k+');

            pm=[1,-1];

            % 2 along-strike segments
            for k=1:2
                x1=[obj.xc(shznum,1)-obj.sv(shznum,1).*obj.L(shznum)/2+pm(k)*obj.nv(shznum,1).*obj.T(shznum)/2, ...
                    obj.xc(shznum,1)+obj.sv(shznum,1).*obj.L(shznum)/2+pm(k)*obj.nv(shznum,1).*obj.T(shznum)/2, ...
                    NaN(size(obj.sv(shznum,1)))]';
                x2=[obj.xc(shznum,2)-obj.sv(shznum,2).*obj.L(shznum)/2+pm(k)*obj.nv(shznum,2).*obj.T(shznum)/2, ...
                    obj.xc(shznum,2)+obj.sv(shznum,2).*obj.L(shznum)/2+pm(k)*obj.nv(shznum,2).*obj.T(shznum)/2, ...
                    NaN(size(obj.sv(shznum,2)))]';
                x3=[obj.xc(shznum,3)-obj.sv(shznum,3).*obj.L(shznum)/2+pm(k)*obj.nv(shznum,3).*obj.T(shznum)/2, ...
                    obj.xc(shznum,3)+obj.sv(shznum,3).*obj.L(shznum)/2+pm(k)*obj.nv(shznum,3).*obj.T(shznum)/2, ...
                    NaN(size(obj.sv(shznum,3)))]';

                plot3(x1(:)/obj.scale,x2(:)/obj.scale,x3(:)/obj.scale,'k-');
            end

            % 2 normal-direction segments
            for j=1:2
                x1=[obj.xc(shznum,1)+pm(j)*obj.sv(shznum,1).*obj.L(shznum)/2+obj.nv(shznum,1).*obj.T(shznum)/2, ...
                    obj.xc(shznum,1)+pm(j)*obj.sv(shznum,1).*obj.L(shznum)/2-obj.nv(shznum,1).*obj.T(shznum)/2, ...
                    NaN(size(obj.sv(shznum,1)))]';
                x2=[obj.xc(shznum,2)+pm(j)*obj.sv(shznum,2).*obj.L(shznum)/2+obj.nv(shznum,2).*obj.T(shznum)/2, ...
                    obj.xc(shznum,2)+pm(j)*obj.sv(shznum,2).*obj.L(shznum)/2-obj.nv(shznum,2).*obj.T(shznum)/2, ...
                    NaN(size(obj.sv(shznum,2)))]';
                x3=[obj.xc(shznum,3)+pm(j)*obj.sv(shznum,3).*obj.L(shznum)/2+obj.nv(shznum,3).*obj.T(shznum)/2, ...
                    obj.xc(shznum,3)+pm(j)*obj.sv(shznum,3).*obj.L(shznum)/2-obj.nv(shznum,3).*obj.T(shznum)/2, ...
                    NaN(size(obj.sv(shznum,3)))]';

                    plot3(x1(:)/obj.scale,x2(:)/obj.scale,x3(:)/obj.scale,'k-');
            end
%             for k=1:length(shznum)
%                 text(obj.xc(shznum(k),1),obj.xc(shznum(k),2),obj.xc(shznum(k),3),num2str(shznum(k)));
%             end
        end
        
        function plotUnitVectors(obj,sc)
            % PLOTUNITVECTORS plot normal, strike-slip and dip-slip unit
            % vectors.
            %
            % SEE ALSO: unicycle
            
            quiver3(obj.xc(:,1)/obj.scale,obj.xc(:,2)/obj.scale,obj.xc(:,3)/obj.scale,sc*obj.nv(:,1),sc*obj.nv(:,2),sc*obj.nv(:,3),0,'r');
            quiver3(obj.xc(:,1)/obj.scale,obj.xc(:,2)/obj.scale,obj.xc(:,3)/obj.scale,sc*obj.sv(:,1),sc*obj.sv(:,2),sc*obj.sv(:,3),0,'g');
            quiver3(obj.xc(:,1)/obj.scale,obj.xc(:,2)/obj.scale,obj.xc(:,3)/obj.scale,sc*obj.dv(:,1),sc*obj.dv(:,2),sc*obj.dv(:,3),0,'b');
        end
        
        %% % % % % % % % % % % % % % % % % % % % % % % % % %
        %                                                  %
        %          plot volume element index in 3d         %
        %                                                  %
        % % % % % % % % % % % % % % % % % % % % % % % % % %%
        function plotIndex(obj,varargin)
            if 1>obj.N
                fprintf('unicycle.geometry.semiInfiniteCuboid: nothing to plot\n');
                return
            end
            
            for k=1:length(obj.L)
                text(obj.xc(k,1)/obj.scale,obj.xc(k,2)/obj.scale,obj.xc(k,3)/obj.scale,num2str(k));
            end
        end
        
        %% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
        %                                                                                        %
        %        export geometry to Relax .shz format                                            %
        %                                                                                        %
        % INPUT:                                                                                 %
        %                                                                                        % 
        % scale        - scaling factor of geometrical features                                  %
        % fname        - output file name                                                        %
        % varargin     - pairs of variable name (string) and                                     %
        %                attributes.                                                             %
        %                                                                                        %
        % EXAMPLE:                                                                               %
        %                                                                                        %
        %  shz.exportVTP(1e-3,'output/semiInfiniteCuboid.shz','strain',e11,e12,e13,e22,e23,e33)  %
        %                                                                                        %
        % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %%
        function exportSHZ(obj,fname,varargin)
            if 1>obj.N
                return
            end
            
            fid=fopen(fname,'wt');
            fprintf(fid,'# export from unicycle\n');
            fprintf(fid,'# n e11 e12 e13 e22 e23 e33 x1 x2 x3 length width thickness strike dip\n');
            if nargin > 5
                fprintf(fid,'%05.5d %f %f %f %f %f %f %f %f %f %f     -1 %f %f %f\n', ...
                    [cumsum(ones(size(obj.xc,1),1)) varargin{1} varargin{2} varargin{3} varargin{4} varargin{5} varargin{6} obj.x(:,2) obj.x(:,1) -obj.x(:,3) obj.L obj.T obj.strike obj.dip]');
            else
                fprintf(fid,'%05.5d %f %f %f %f %f %f %f %f %f %f     -1 %f %f %f\n', ...
                    [cumsum(ones(size(obj.xc,1),1)) zeros(size(obj.xc,1),6) obj.x(:,2) obj.x(:,1) -obj.x(:,3) obj.L obj.T obj.strike obj.dip]');
            end
            fclose(fid);
        end
        
        %% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
        %                                                            %
        %        export geometry to Paraview VTP file                %
        %                                                            %
        % INPUT:                                                     %
        %                                                            % 
        % scale        - scaling factor of geometrical features      %
        % fname        - output file name                            %
        % varargin     - pairs of variable name (string) and         %
        %                attributes.                                 %
        %                                                            %
        % EXAMPLE:                                                   %
        %                                                            %
        %   shz.exportVTP(1e-3,'output/shz.vtp','strain',flt.slip)   %
        %                                                            %
        % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %%
        function exportVTP(obj,scale,fname,varargin)
            if 1>obj.N
                return
            end
            
            [xp,yp,zp,dim]=obj.computeVertexPosition();
            
            if nargin > 1
                unicycle.export.exportVTKshearZone( ...
                    scale*xp, ...
                    scale*yp, ...
                    scale*zp, ...
                    fname,...
                    varargin{:});
            else
                unicycle.export.exportVTKshearZone( ...
                    scale*xp, ...
                    scale*yp, ...
                    scale*zp, ...
                    fname);
            end
        end
        
        %% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
        %                                                                  %
        %        export geometry to GMT .xyz format                        %
        %                                                                  %
        % INPUT:                                                           %
        %                                                                  % 
        % scale        - scaling factor of geometrical features            %
        % fname        - output file name                                  %
        % value        - attribute to plot                                 % 
        %                                                                  %
        % EXAMPLE:                                                         %
        %                                                                  %
        %   cuboid.exportXYZ(1e-3,'output/semiInfiniteCuboid.xyz',value)   %
        %                                                                  %
        % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %%
        function exportXYZ(o,scale,fname,field)
            if 1>o.N
                return
            end
            
	    fid=fopen(fname,'wt');
            fprintf(fid,'# export from unicycle\n');
            for k=1:length(field)
                fprintf(fid,'> -Z%f\n',field(k));
                fprintf(fid,'%f %f %f\n%f %f %f\n%f %f %f\n%f %f %f\n', ...
                        [o.xc(k,:)-o.sv(k,:).*o.L(k)/2-o.nv(k,:).*o.T(k)/2; ...
                         o.xc(k,:)+o.sv(k,:).*o.L(k)/2-o.nv(k,:).*o.T(k)/2; ...
                         o.xc(k,:)+o.sv(k,:).*o.L(k)/2+o.nv(k,:).*o.T(k)/2; ...
                         o.xc(k,:)-o.sv(k,:).*o.L(k)/2+o.nv(k,:).*o.T(k)/2]'/scale);
            end
            fclose(fid);

        end
        
    end % methods
    
end
