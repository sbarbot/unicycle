classdef semiInfiniteTriangle < handle
    properties
        % number of semi-infinite triangles
        N;
        % identification number
        id;
        % triangle vertices
        x;
        % mesh information (vertices of triangles)
        vertices;
        % center position
        xc;
        % volume element strain
        eps;
        % unit vectors in strike, dip, normal and normal directions
        sv;
        dv;
        nv;
        % geometry
        circumRadius;
        % area of triangle
        area;
        % structure of levels
        levels;
        % hash table directing level name to level index
        levelHashTable;
        % earth model
        earthModel;
        % plotting scale
        scale=1;
    end
    
    methods
        %% % % % % % % % % % % % % % % % % %
        %                                  %
        %      c o n s t r u c t o r       %
        %                                  %
        % % % % % % % % % % % % % % % % % %%
        function obj = semiInfiniteTriangle()
            % SEMIINFINITETRIANGLE is a meta class representing the geometry of
            % vertical, semi-infinite triangle volume elements, defined in terms
            % of the position of three vertices, as follows:
            %
            %                     N (x1)      + A
            %                       /        /|\
            %                      /        / . \
            %                     /        /  |  \
            %                    /        /   .   \
            %                   /        /    |    + B
            %                  /        /     . /  |
            %                 /        /     .|    |
            %                /        /   /        |
            %               /        / .           |
            %              /     C  +              |
            %             /         |              |
            %            @----------------------------- E (x2)
            %            |          |
            %            |          |
            %            |          |
            %            |          |
            %            |          |Infinity
            %            x3
            %
            % AUTHOR: S. Barbot (sbarbot@usc.edu), July 1, 2020, Los Angeles
            %
            % SEE ALSO: unicycle, unicycle.geometry.triangle
            
            if 0==nargin
                obj.N=0;
                return
            end
            
        end % constructor
        
        function [varargout]=tractionKernels(obj,rcv)
            % TRACTIONKERNELS computes the traction on receiver faults due
            % to strain in volume elements.
            %
            % rcv - receiver fault
            
            varargout = cell(1,nargout);
            [varargout{:}]=obj.earthModel.tractionKernels(obj,rcv);
        end
        
        function [varargout]=stressKernels(obj,rcv)
            % STRESSKERNELS computes the stress on receiver volume elements due
            % to motion of dislocations.
            %
            % rcv - receiver fault
            
            varargout = cell(1,nargout);
            [varargout{:}]=obj.earthModel.stressKernels(obj,rcv);
        end
        
        function [varargout]=displacementKernels(obj,x,vecsize)
            % DISPLACEMENTKERNELS computes the stress on receiver faults due to
            % motion of dislocations.
            %
            % x       - observations points coordinates
            % vecsize - length of displacement vector
            
            varargout = cell(1,nargout);
            [varargout{:}]=obj.earthModel.displacementKernels(obj,x,vecsize);
        end
        
        %% % % % % % % % % % % % % % % % % % % % % % % % % %
        %                                                  %
        %       plot contour of slip patches in 3d         %
        %                                                  %
        % % % % % % % % % % % % % % % % % % % % % % % % % %%
        function plotVolume(obj,varargin)
            % PLOTVOLUME plot the 3 edges of volume elements as
            % follows:
            %
            %               +
            %              /.
            %             / .
            %            /  .
            %           /   .
            %          / +  .
            %         /     .
            %        /      .
            %       +-------+
            %
            % SEE ALSO: unicycle
            
            if 1>obj.N
                fprintf('unicycle.geometry.semiInfiniteTriangle: nothing to plot\n');
                return
            end
            
            if 1>=nargin
                %hold on
                trisurf( ...
                    obj.vertices, ...
                    obj.x(:,1)/obj.scale, ...
                    obj.x(:,2)/obj.scale, ...
                    obj.x(:,3)/obj.scale, ...
                    0*obj.x(:,3),'EdgeColor','k','LineWidth',1,'FaceColor','None');
                %plot3(obj.xc(:,1)/obj.scale,obj.xc(:,2)/obj.scale,obj.xc(:,3)/obj.scale,'k+');
            else
                hold on
                trisurf( ...
                    obj.vertices, ...
                    obj.x(:,1)/obj.scale, ...
                    obj.x(:,2)/obj.scale, ...
                    obj.x(:,3)/obj.scale, ...
                    varargin{1}), shading flat;
                
            end
            
        end
        
        %% % % % % % % % % % % % % % % % % % % % % % % % % %
        %                                                  %
        %       plot contour of slip patches in 3d         %
        %                                                  %
        % % % % % % % % % % % % % % % % % % % % % % % % % %%
        function plotVolumeById(obj,index,varargin)
            % PLOTVOLUMEBYID plot the 3 edges of volume elements as
            % follows:
            %
            %           +
            %          / \
            %         /   \
            %        /  +  \
            %       +-------+
            %
            % SEE ALSO: unicycle
            
            if 1>obj.N
                fprintf('unicycle.geometry.semiInfiniteTriangle: nothing to plot\n');
                return
            end
            
            if 1>=nargin
                trisurf(obj.vertices(index,[1,2,3]), ...
                    obj.x(:,1)/obj.scale, ...
                    obj.x(:,2)/obj.scale, ...
                    obj.x(:,3)/obj.scale, ...
                    varargin{1},'FaceAlpha',0.3), shading flat;
                
                plot3( ...
                    obj.xc(:,1)/obj.scale, ...
                    obj.xc(:,2)/obj.scale, ...
                    obj.xc(:,3)/obj.scale,'k+');
            else
                trisurf(obj.vertices(index,[1,2,3]), ...
                    obj.x(:,1)/obj.scale, ...
                    obj.x(:,2)/obj.scale, ...
                    obj.x(:,3)/obj.scale, ...
                    0.*obj.x(index,3),'FaceColor','None','LineWidth',1);
            end
            
        end
        
        function plotUnitVectors(obj,sc)
            % PLOTUNITVECTORS plot normal, strike-slip and dip-slip unit
            % vectors.
            %
            % SEE ALSO: unicycle
            
            quiver3( ...
                obj.xc(:,1)/obj.scale, ...
                obj.xc(:,2)/obj.scale, ...
                obj.xc(:,3)/obj.scale, ...
                sc*obj.nv(:,1), ...
                sc*obj.nv(:,2), ...
                sc*obj.nv(:,3),0,'r');
            quiver3( ...
                obj.xc(:,1)/obj.scale, ...
                obj.xc(:,2)/obj.scale, ...
                obj.xc(:,3)/obj.scale, ...
                sc*obj.sv(:,1), ...
                sc*obj.sv(:,2), ...
                sc*obj.sv(:,3),0,'g');
            quiver3( ...
                obj.xc(:,1)/obj.scale, ...
                obj.xc(:,2)/obj.scale, ...
                obj.xc(:,3)/obj.scale, ...
                sc*obj.dv(:,1), ...
                sc*obj.dv(:,2), ...
                sc*obj.dv(:,3),0,'b');
        end
        
        %% % % % % % % % % % % % % % % % % % % % % % % % % %
        %                                                  %
        %          plot volume element index in 3d         %
        %                                                  %
        % % % % % % % % % % % % % % % % % % % % % % % % % %%
        function plotIndex(obj,varargin)
            if 1>obj.N
                fprintf('unicycle.geometry.semiInfiniteTriangle: nothing to plot\n');
                return
            end
            
            for k=1:size(obj.x,1)
                text( ...
                    obj.xc(k,1)/obj.scale, ...
                    obj.xc(k,2)/obj.scale, ...
                    obj.xc(k,3)/obj.scale, ...
                    num2str(k));
            end
        end
        
        %% % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
        %                                                                  %
        %        export geometry to GMT .xyz format                        %
        %                                                                  %
        % INPUT:                                                           %
        %                                                                  %
        % scale        - scaling factor of geometrical features            %
        % fname        - output file name                                  %
        % value        - attribute to plot                                 %
        %                                                                  %
        % EXAMPLE:                                                         %
        %                                                                  %
        %   shz.exportXYZ(1e-3,'output/semiInfiniteTriangle.xyz',value)    %
        %                                                                  %
        % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %%
        function exportXYZ(o,scale,fname,field)
            if 1>o.N
                return
            end
            
            % vertices
            A=[o.x(o.vertices(:,1),1),o.x(o.vertices(:,1),2),o.x(o.vertices(:,1),3)]*scale;
            B=[o.x(o.vertices(:,2),1),o.x(o.vertices(:,2),2),o.x(o.vertices(:,2),3)]*scale;
            C=[o.x(o.vertices(:,3),1),o.x(o.vertices(:,3),2),o.x(o.vertices(:,3),3)]*scale;
            
            fid=fopen(fname,'wt');
            fprintf(fid,'# export from unicycle\n');
            for k=1:length(field)
                if (~isnan(field(k)))
                    fprintf(fid,'> -Z%f\n%f %f %f\n%f %f %f\n%f %f %f\n%\n', ...
                        [field(k);A(k,:)';B(k,:)';C(k,:)']);
                end
            end
            fclose(fid);
            
        end
        
    end % methods
    
    methods(Static)
        %% % % % % % % % % % % % % % % % % % % % % % %
        %                                            %
        %  compute area from position of vertices    %
        %                                            %
        % % % % % % % % % % % % % % % % % % % % % % %%
        function area = computeArea(x,vertices)
            % COMPUTEAREA compute the triangle areas.
            %
            %   area=semiInfiniteTriangle.computeArea(x,vertices)
            %
            % INPUT:
            %   x        - position of mesh points
            %   vertices - list of vertices forming triangle
            %
            % SEE ALSO: unicycle, unicycle.geometry.semiInfiniteTriangle
            
            % vertices
            A=[x(vertices(:,1),1),x(vertices(:,1),2),x(vertices(:,1),3)];
            B=[x(vertices(:,2),1),x(vertices(:,2),2),x(vertices(:,2),3)];
            C=[x(vertices(:,3),1),x(vertices(:,3),2),x(vertices(:,3),3)];
            
            % area = |(B-A) x (C-A)| / 2
            area=sqrt(((B(:,2)-A(:,2)).*(C(:,3)-A(:,3))-(B(:,3)-A(:,3)).*(C(:,2)-A(:,2))).^2 ...
                +((B(:,3)-A(:,3)).*(C(:,1)-A(:,1))-(B(:,1)-A(:,1)).*(C(:,3)-A(:,3))).^2 ...
                +((B(:,1)-A(:,1)).*(C(:,2)-A(:,2))-(B(:,2)-A(:,2)).*(C(:,1)-A(:,1))).^2)/2;
            
        end
        
        %% % % % % % % % % % % % % % % % % % % % % % % % % %
        %                                                  %
        %        load the triangle mesh from file          %
        %                                                  %
        % % % % % % % % % % % % % % % % % % % % % % % % % %%
        function [e11,e12,e13,e22,e23,e33,mesh] = loadVolume(filename)
            % LOADVOLUME loads the .tri mesh file and detect the availability
            % of eij.
            %
            %   [e11,e12,e13,e22,e23,e33,mesh]=loadVolume(filename)
            %
            % where mesh is a list of eij and mesh vertices, for example:
            %
            % # n e11 e12 e13 e22 e23 e33 i1 i2 i3
            %
            % or
            %
            % # n i1 i2 i3
            %
            % for receivers.
            
            % open and count the number of columns
            fid=fopen(filename);
            line=strtrim(fgetl(fid));
            while (strcmp('#',line(1)))
                line=strtrim(fgetl(fid));  % open the file, get the first line
            end
            fclose(fid);
            nColumn=numel(strsplit(line));
            
            switch nColumn
                case 10 % if eij is present
                    [~,e11,e12,e13,e22,e23,e33,i1,i2,i3]=...
                        textread(filename,'%u %f %f %f %f %f %f %d %d %d',...
                        'commentstyle','shell');
                case 4 % no eij
                    [~,i1,i2,i3]=...
                        textread(filename,'%u %d %d %d',...
                        'commentstyle','shell');
                    e11 = zeros(size(i1));
                    e12 = zeros(size(i1));
                    e13 = zeros(size(i1));
                    e22 = zeros(size(i1));
                    e23 = zeros(size(i1));
                    e33 = zeros(size(i1));
                otherwise
                    error('unicycle:geometry:triangle:invalid file format');
            end
            mesh = [i1(:),i2(:),i3(:)];
        end
    end % methods (Static)
    
end
