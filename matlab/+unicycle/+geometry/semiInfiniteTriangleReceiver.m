classdef semiInfiniteTriangleReceiver < unicycle.geometry.semiInfiniteTriangle
    properties
        % degrees of freedom (number of parameters solved in numerical integration)
        dgf;
        
        % strain
        e11,e12,e13,e22,e23,e33;
    end
    
    methods
        %% % % % % % % % % % % % % % % % % %
        %                                  %
        %      c o n s t r u c t o r       %
        %                                  %
        % % % % % % % % % % % % % % % % % %%
        function obj = semiInfiniteTriangleReceiver(varargin)
            % SEMIINFINITETRIANGLERECEIVER is a class representing the geometry and 
            % physicalproperties of receiver semi-infinite triangle volume elements.
            %
            %   src = geometry.semiInfiniteTriangleReceiver('basename',earthModel)
            %
            % or
            %
            %   src = geometry.semiInfiniteTriangleReceiver({'basename1','basename2'},earthModel)
            %
            % where 'basename' is short for the pair 'basename.tri' and 'basename.ned'.
            %
            % SEE ALSO: unicycle, unicycle.geometry.receiver
            
            import unicycle.geometry.semiInfiniteTriangle;
            
            if isempty(varargin)
                return
            else
                basename=varargin{1};
		if (1<length(varargin))
                    obj.earthModel=varargin{2};
	        end
            end
            
            if ~iscell(basename)
                basename={basename};
            end
            
            obj.x=[];
            obj.vertices=[];
            for k=1:length(basename)
                fname=[basename{k} '.ned'];
                assert(2==exist(fname,'file'),sprintf('error: can''t find %s',fname));
                [~,x1,x2,x3]=...
                    textread(fname,'%u %f %f %f','commentstyle','shell');
                obj.x=[obj.x;[x2,x1,-x3]];
                
                assert(0>=max(obj.x(:,3)),'error: all vertices should have positive depth.');
                assert(min(obj.x(:,3))==max(obj.x(:,3)),'error: all three vertices should have the same depth.');
                
                fname=[basename{k} '.tri'];
                assert(2==exist(fname,'file'),sprintf('error: can''t find %s',fname));
                    
                [e11,e12,e13,e22,e23,e33,mesh]=obj.loadVolume(fname);
                obj.vertices=[obj.vertices;mesh];
                obj.e11=[obj.e11;e11(:)];
                obj.e12=[obj.e12;e12(:)];
                obj.e13=[obj.e13;e13(:)];
                obj.e22=[obj.e22;e22(:)];
                obj.e23=[obj.e23;e23(:)];
                obj.e33=[obj.e33;e33(:)];
            end
            
            % patch properties
            obj.N=size(obj.vertices,1);
            obj.id=1:obj.N;
            
            % center of fault patch
            obj.xc=[(obj.x(obj.vertices(:,1),1)+ ...
                     obj.x(obj.vertices(:,2),1)+ ...
                     obj.x(obj.vertices(:,3),1))/3, ...
                    (obj.x(obj.vertices(:,1),2)+ ...
                     obj.x(obj.vertices(:,2),2)+ ...
                     obj.x(obj.vertices(:,3),2))/3, ...
                    (obj.x(obj.vertices(:,1),3)+ ...
                     obj.x(obj.vertices(:,2),3)+ ...
                     obj.x(obj.vertices(:,3),3))/3];
                
            % circumsphere radius of triangle
            obj.circumRadius=zeros(obj.N,1);
                        
            for k=1:obj.N
                A=obj.x(obj.vertices(k,1),:)';
                B=obj.x(obj.vertices(k,2),:)';
                C=obj.x(obj.vertices(k,3),:)';
                
                % unit vectors
                nA = [C(2)-B(2);
                      B(1)-C(1);
                      0]/norm(C-B);
                nB = [C(2)-A(2);
                      A(1)-C(1);
                      0]/norm(C-A);
                
                % circumcenter of triangle
                O=((B(:)+C(:))+nA*(nB(2)*(B(1)-A(1))-nB(1)*(B(2)-A(2)))/(nA(2)*nB(1)-nA(1)*nB(2)))/2;

                % circumcircle radius
                obj.circumRadius(k)=norm(O(:)-A(:));
            end
            
            % volume of tetrahedron
            obj.area=semiInfiniteTriangle.computeArea(obj.x,obj.vertices);
            
            % unit vectors (stress in north-east-depth coordinate system)
            obj.sv=[zeros(obj.N,1),ones(obj.N,1),zeros(obj.N,1)];
            obj.nv=[ones(obj.N,1),zeros(obj.N,1),zeros(obj.N,1)];
            obj.dv=[zeros(obj.N,1),zeros(obj.N,1),ones(obj.N,1)];

        end % constructor
        
    end % methods
    
end
