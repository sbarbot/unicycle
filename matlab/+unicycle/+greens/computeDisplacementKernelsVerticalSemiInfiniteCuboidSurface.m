function [L11,L12,L13,L22,L23,L33]=computeDisplacementKernelsVerticalSemiInfiniteCuboidSurface(shz,nu,x,vecsize)
% COMPUTEDISPLACEMENTKERNELSVERTICALSEMIINFINITECUBOIDSURFACE computes 
% inversion kernels (Green's functions) to connect strain in semi-infinite cuboid volume 
% elements to surface deformation using semi-analytic solutions with
% mixed Gauss-Legendre and double-exponential quadratures.
%
% INTERFACE:
%
%   [L11,L12,L13,L22,L23,L33] = ...
%            unicycle.greens.computeDisplacementKernelsVerticalSemiInfiniteCuboidSurface(shz,nu,x,vecsize)
%
% INPUT:
%
% shz     geometry.cuboid object
% nu      Poisson's ratio
% x       coordinates of observation point (east, north, up), up must be zero.
% vecsize number of component for displacement vector
%
%
% DATA LAYOUT:
%
%            strain eij
%       /e1               \
%       |n1               |
%       |u1               |
%       |e2               |
%       |n2               |
%       |u2               |
% Lij = | .               |
%       | .               |
%       | .               |
%       | .               |
%       |en               |
%       |nn               |
%       \un               /
%
% Author: Sylvain Barbot (sbarbot@usc.edu), Feb 10, 2020, Los Angeles.
%
% SEE ALSO: unicycle

import unicycle.utils.*

% number of GPS stations
D=size(x,1);

% Green's function matrix
L=cell(6,1);

% individual strain components
e=eye(6);

bar=textProgressBar('# semi-infinite cuboid surface displacement kernels: ');

for j=1:6

    G=zeros(vecsize*D,shz.N);
    
    for i=1:shz.N
        
        if 0==mod(i-1,2)
            bar.progress((i/shz.N+(j-1))*100/6);
        end
        [u1,u2,u3]=unicycle.greens.computeDisplacementVerticalSemiInfiniteCuboidSurfaceGauss( ...
            x(:,2),x(:,1), ...
            shz.x(i,2),shz.x(i,1),-shz.x(i,3), ...
            shz.L(i),shz.T(i),shz.strike(i), ...
            e(j,1),e(j,2),e(j,3),e(j,4),e(j,5),e(j,6),nu,'N',7,'precision',0.05);
            
        switch(vecsize)
            case 2
                u=[u2,u1]';
            case 3
                u=[u2,u1,-u3]';
            otherwise
                error('greens:computeDisplacementKernelsVerticalSemiInfiniteCuboidSurface','invalid degrees of freedom (%d)\n',...
                    unicycle.greens.model.dgf);
        end
       
        G(:,i)=u(:);
        
    end
    
    L{j,1}=G;
    
    
end

% distribute kernels into two separate variables.
[L11,L12,L13,L22,L23,L33]=deal(L{:});

bar.progress(100);
bar.close();

end



