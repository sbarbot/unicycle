function [u1,u2,u3]=computeDisplacementPolyhedronMixedQuad( ...
    X1,X2,X3,vertices,triangles,e11,e12,e13,e22,e23,e33,nu,varargin)
% function COMPUTEDISPLACEMENTPOLYHEDRONMIXEDQUAD computes the
% displacement field associated with deforming polyhedron 
% considering the following geometry using the Gauss-Legendre, double-
% exponential or numerical
% quadrature.
%
%                     North (x1)
%                      /
%                     /
%        surface     /
%      -------------+-------------- East (x2)
%                  /|
%                 / |    A
%                /  |     + -  -  -  -  -  -  -  -+ E
%               /   |    /  .                   ..
%                   |   /     .              .  . 
%                   |  /        .         .    .
%                   | /           .    .      . 
%                   |/              +        .
%                   /            .  | B     .
%                  /|          /    |      .
%                 / :       .       |     .
%                /  |    /          |    .
%               /   : .             |   .
%              /   /|               |  .
%             / .   :               | .
%            +------|---------------+
%          C        :                 D
%                   |
%                 Down (x3)
%
%
% INPUT:
% X1, X2, X3         north, east coordinates and depth of the observation point,
% vertices           north, east, and depth coordinates of the vertices,
% triangles          triangulation of the surface of the volume element
% eij                source strain component 11, 12, 13, 22, 23 and 33
%                    in the volume element,
% nu                 Poisson's ratio in the half space.
%
% OPTIONS:
% 'N',integer        the number of Gauss integration points
% 'convex',true      whether the volume element is assumed convex. if not so, 
%                    the order of the triangle vertices matters and is
%                    assumed to be given in clockwise direction when
%                    looking from outside.
%
% OUTPUT:
% u1                 displacement component in the north direction,
% u2                 displacement component in the east direction,
% u3                 displacement component in the down direction.
%
% AUTHOR: Sylvain Barbot (sbarbot@ntu.edu.sg) - July 2, 2018, Singapore.

assert(min(X3(:))>=0,'computeDisplacementPolyhedronMixedQuad: observation depth must be positive.');

% process optional input
p = inputParser;
p.addParameter('convex',true,@validateConvex);
p.addParameter('precision',0.01,@validatePrecision);
p.addParameter('bound',3.0,@validateBound);
p.addParameter('n',7,@validateN);
p.addParameter('radius',1.1,@validateRadius);
p.parse(varargin{:});
optionStruct = p.Results;

% Lame parameter
lambda=2*nu/(1-2*nu);

% isotropic strain
ekk=e11+e22+e33;

% moment density
m11=lambda*ekk+2*e11;
m12=2*e12;
m13=2*e13;
m22=lambda*ekk+2*e22;
m23=2*e23;
m33=lambda*ekk+2*e33;

% center coordinates
O=sum((vertices(triangles(:,1),:)+vertices(triangles(:,2),:)+vertices(triangles(:,3),:))/3,1)/size(triangles,1);

% parameterized surface integral, mapping uv space to three-dimensional space
y=@(u,v,A,B,C) A*(1-u).*(1-v)/4+B*(1+u).*(1-v)/4+C*(1+v)/2;

u1=zeros(size(X1));
u2=zeros(size(X2));
u3=zeros(size(X3));

% loop of receiver points
for l=1:numel(X1)
    
    x1=X1(l);
    x2=X2(l);
    x3=X3(l);
    
    % loop of triangle elements
    for i=1:size(triangles,1)
        
        A=vertices(triangles(i,1),:)';
        B=vertices(triangles(i,2),:)';
        C=vertices(triangles(i,3),:)';
        
        % unit normal vectors
        n=cross(B-A,C-A); % ABC
        n=n/norm(n);
        
        if optionStruct.convex
            % check that unit vectors are pointing outward
            if (n'*(O(:)-(A(:)+B(:)+C(:))/3))>0
                n=-n;
            end
        else
            % viewed from outside, vertices are given in clockwise order
            n=-n;
        end
        
        % area of triangle ABC
        ABC=norm(cross(C-A,B-A))/2;

        % Gauss-Legendre quadrature
        u1(l)=u1(l)+gaussLegendre2(@(u,v) (1-v).*IU1(u,v),optionStruct.n);
        u2(l)=u2(l)+gaussLegendre2(@(u,v) (1-v).*IU2(u,v),optionStruct.n);
        u3(l)=u3(l)+gaussLegendre2(@(u,v) (1-v).*IU3(u,v),optionStruct.n);
        
        %u1(l)=u1(l)+doubleExponential2(@(u,v) (1-v).*IU1(u,v),optionStruct.precision,optionStruct.bound);
        %u2(l)=u2(l)+doubleExponential2(@(u,v) (1-v).*IU2(u,v),optionStruct.precision,optionStruct.bound);
        %u3(l)=u3(l)+doubleExponential2(@(u,v) (1-v).*IU3(u,v),optionStruct.precision,optionStruct.bound);
        
        % integral2
        %u1(l)=u1(l)+integral2(@(u,v) (1-v).*IU1(u,v),-1,1,-1,1);
        %u2(l)=u2(l)+integral2(@(u,v) (1-v).*IU2(u,v),-1,1,-1,1);
        %u3(l)=u3(l)+integral2(@(u,v) (1-v).*IU3(u,v),-1,1,-1,1);
        
    end
end

    % Green's functions
    function d = G11(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*( ...
            (3-4*nu)./lr1 ...
            +1./lr2 ...
            +(x1-y1).^2./lr1.^3 ...
            +(3-4*nu)*(x1-y1).^2./lr2.^3 ...
            +2*x3.*y3.*(lr2.^2-3*(x1-y1).^2)./lr2.^5 ...
            +4*(1-2*nu)*(1-nu)*(lr2.^2-(x1-y1).^2+lr2.*(x3+y3))./(lr2.*(lr2+x3+y3).^2)...
            );
    end
    function d = G12(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=(x1-y1).*(x2-y2)/(16*pi*(1-nu)).*( ...
            1./lr1.^3 ...
            +(3-4*nu)./lr2.^3 ...
            -6*x3.*y3./lr2.^5 ...
            -4*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3).^2) ...
            );
    end
    function d = G13(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=(x1-y1)/(16*pi*(1-nu)).*( ...
            (x3-y3)./lr1.^3 ...
            +(3-4*nu)*(x3-y3)./lr2.^3 ...
            -6*x3.*y3.*(x3+y3)./lr2.^5 ...
            +4*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3)) ...
            );
    end
    function d = G21(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=(x1-y1).*(x2-y2)/(16*pi*(1-nu)).*( ...
            1./lr1.^3 ...
            +(3-4*nu)./lr2.^3 ...
            -6*x3.*y3./lr2.^5 ...
            -4*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3).^2) ...
            );
    end
    function d = G22(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*( ...
            (3-4*nu)./lr1 ...
            +1./lr2 ...
            +(x2-y2).^2./lr1.^3 ...
            +(3-4*nu)*(x2-y2).^2./lr2.^3 ...
            +2*x3.*y3.*(lr2.^2-3*(x2-y2).^2)./lr2.^5 ...
            +4*(1-2*nu)*(1-nu)*(lr2.^2-(x2-y2).^2+lr2.*(x3+y3))./(lr2.*(lr2+x3+y3).^2)...
            );
    end
    function d = G23(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=(x2-y2)/(16*pi*(1-nu)).*( ...
            (x3-y3)./lr1.^3 ...
            +(3-4*nu)*(x3-y3)./lr2.^3 ...
            -6*x3.*y3.*(x3+y3)./lr2.^5 ...
            +4*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3)) ...
            );
    end
    function d = G31(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=(x1-y1)/(16*pi*(1-nu)).*( ...
            (x3-y3)./lr1.^3 ...
            +(3-4*nu)*(x3-y3)./lr2.^3 ...
            +6*x3.*y3.*(x3+y3)./lr2.^5 ...
            -4*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3)) ...
            );
    end
    function d = G32(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=(x2-y2)/(16*pi*(1-nu)).*( ...
            (x3-y3)./lr1.^3 ...
            +(3-4*nu)*(x3-y3)./lr2.^3 ...
            +6*x3.*y3.*(x3+y3)./lr2.^5 ...
            -4*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3)) ...
            );
    end
    function d = G33(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*( ...
            (3-4*nu)./lr1 ...
            +(5-12*nu+8*nu^2)./lr2 ...
            +(x3-y3).^2./lr1.^3 ...
            +6*x3.*y3.*(x3+y3).^2./lr2.^5 ...
            +((3-4*nu)*(x3+y3).^2-2*x3.*y3)./lr2.^3 ...
            );
    end

    function d = IU1(u,v)
        % function IU1 is the integrand for displacement component u1

        y1=y(u,v,A(1),B(1),C(1));
        y2=y(u,v,A(2),B(2),C(2));
        y3=y(u,v,A(3),B(3),C(3));
        
        d=zeros(size(u));
        if (m11*n(1)+m12*n(2)+m13*n(3)) ~= 0
            d=d+ABC/4*(m11*n(1)+m12*n(2)+m13*n(3))*G11(y1,y2,y3);
        end
        if (m12*n(1)+m22*n(2)+m23*n(3)) ~= 0
            d=d+ABC/4*(m12*n(1)+m22*n(2)+m23*n(3))*G21(y1,y2,y3);
        end
        if (m13*n(1)+m23*n(2)+m33*n(3)) ~= 0
            d=d+ABC/4*(m13*n(1)+m23*n(2)+m33*n(3))*G31(y1,y2,y3);
        end
    end

    function d = IU2(u,v)
        % function IU2 is the integrand for displacement component u2
        
        y1=y(u,v,A(1),B(1),C(1));
        y2=y(u,v,A(2),B(2),C(2));
        y3=y(u,v,A(3),B(3),C(3));
        
        d=zeros(size(u));
        if (m11*n(1)+m12*n(2)+m13*n(3)) ~= 0
            d=d+ABC/4*(m11*n(1)+m12*n(2)+m13*n(3))*G12(y1,y2,y3);
        end
        if (m12*n(1)+m22*n(2)+m23*n(3)) ~= 0
            d=d+ABC/4*(m12*n(1)+m22*n(2)+m23*n(3))*G22(y1,y2,y3);
        end
        if (m13*n(1)+m23*n(2)+m33*n(3)) ~= 0
            d=d+ABC/4*(m13*n(1)+m23*n(2)+m33*n(3))*G32(y1,y2,y3);
        end
    end

    function d = IU3(u,v)
        % function IU3 is the integrand for displacement component u3
        
        y1=y(u,v,A(1),B(1),C(1));
        y2=y(u,v,A(2),B(2),C(2));
        y3=y(u,v,A(3),B(3),C(3));
        
        d=zeros(size(u));
        if (m11*n(1)+m12*n(2)+m13*n(3)) ~= 0
            d=d+ABC/4*(m11*n(1)+m12*n(2)+m13*n(3))*G13(y1,y2,y3);
        end
        if (m12*n(1)+m22*n(2)+m23*n(3)) ~= 0
            d=d+ABC/4*(m12*n(1)+m22*n(2)+m23*n(3))*G23(y1,y2,y3);
        end
        if (m13*n(1)+m23*n(2)+m33*n(3)) ~= 0
            d=d+ABC/4*(m13*n(1)+m23*n(2)+m33*n(3))*G33(y1,y2,y3);
        end
    end

end

function d=gaussLegendre2(fun,n)
% function GAUSSLEGENDRE2 evaluates the double integral of @fun(u,v) over
% the integral u=0..1, v=0..1 using the Gauss-Legendre quadrature with n
% integration points.

% numerical solution with Gauss-Legendre quadrature
[x,w]=gaussxw(-1,1,n);

d=sum(sum((w*w').*fun(repmat(x,1,n),repmat(x',n,1))));

end

function d=doubleExponential2(fun,precision,bound)
% numerical solution with tanh/sinh quadrature

h=precision;
n=fix(1/h*bound);

k=(-n:n)';
w=(0.5*h*pi*cosh(k*h))./(cosh(0.5*pi*sinh(k*h))).^2;
x=tanh(0.5*pi*sinh(k*h));

d=sum(sum((w*w').*fun(repmat(x,1,2*n+1),repmat(x',2*n+1,1))));

end

function p = validateN(x)
if ~(x > 0)
    error('computeDisplacementPolyhedronMixedQuad','invalid number of integration points');
end
p = true;
end

function p = validateConvex(x)
if ~islogical(x)
    error('computeDisplacementPolyhedronMixedQuad','invalid convex (must be logical)');
end
p = true;
end

function p = validatePrecision(x)
if ~(isreal(x) && isscalar(x) && x > 0)
    error('computeDisplacementPolyhedronMixedQuad','invalid precision');
end
p = true;
end

function p = validateBound(x)
if ~(isreal(x) && isscalar(x) && x > 0)
    error('computeDisplacementPolyhedronMixedQuad','invalid truncation');
end
p = true;
end

function p = validateRadius(x)
if ~(isreal(x) && isscalar(x) && x > 0)
    error('computeDisplacementPolyhedronMixedQuad','invalid radius');
end
p = true;
end

function [xn,wn]  = gaussxw(a,b, n)
% GAUSSXW finds sample points xn and weights wn for Gaussian
% quadrature on (a,b) with n nodes
%
%    function [xn wn]  = gaussxw(a,b, n)
%
%  Integral = wn(:)' * f (xn(:))
%  or  sum(wn.*f(xn))
%
%  Saves nodes and weights from previous call with same n.
%
% AUTHOR: Trefethen, Spectral Method in Matlab

persistent N x w

% check if we need new x, w
if (isempty(N) == 1 || N ~= n)
    N=n;
    beta=0.5./sqrt(1-(2*(1:N-1)).^(-2));
    T=diag(beta,1)+diag(beta,-1);
    [V,D]=eig(T);
    x=diag(D);
    [x,i]=sort(x);
    w=2*V(1,i).^2;
end

% finds sample x and weights for interval (a,b)
xn=(a+b+x(:)*(b-a))/2;
wn=(b-a)*w(:)/2;
end

