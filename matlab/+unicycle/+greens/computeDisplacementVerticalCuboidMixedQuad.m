function [u1,u2,u3]=computeDisplacementVerticalCuboidMixedQuad( ...
    x1,x2,x3,q1,q2,q3,L,T,W,theta, ...
    e11p,e12p,e13p,e22p,e23p,e33p,nu,varargin)
% function COMPUTEDISPLACEMENTVERTICALCUBOIDMIXEDQUAD computes the
% displacement field associated with deforming cuboid volume elements
% considering using either the double-exponential or the Gauss-Legendre
% quadrature using the following geometry
%
%
%                      N (x1)
%                     /
%                    /| strike (theta)
%        q1,q2,q3 ->@-------------------------+   E (x2)
%                   |                         | w    +
%                   :                         | i   /
%                   |                         | d  / s
%                   :                         | t / s
%                   |                         | h/ e
%                   :                         | / n
%                   +-------------------------+  k
%                   :        l e n g t h      / c
%                   |                        / i
%                   :                       / h
%                   |                      / t
%                   :                     /
%                   |                    +
%                   Z (x3)
%
%
% INPUT:
% x1, x2, x3         north, east coordinates and depth of the observation point,
% L, T, W            length, thickness and width of the cuboid,
% theta (degree)     strike angle from north (from x1) of the cuboid.
% eijp               source strain component ij in the cuboid
%                    in the system of reference tied to the cuboid,
% nu                 rigidity and Poisson's ratio in the half space.
%
% OUTPUT:
% sij                stress components ij.
%
% AUTHOR: Sylvain Barbot (sbarbot@usc.edu) - September 24, 2018, Los Angeles.

assert(min(x3(:))>=0,'computeDisplacementVerticalCuboidMixedQuad: observation depth must be positive.');
assert(q3>=0,'computeDisplacementVerticalCuboidMixedQuad: source depth must be positive.');

import unicycle.greens.*;

% process optional input
p = inputParser;
p.addParameter('precision',0.01,@validatePrecision);
p.addParameter('bound',3.0,@validateBound);
p.addParameter('n',7,@validateN);
p.addParameter('radius',1.1,@validateRadius);
p.parse(varargin{:});
optionStruct = p.Results;

% center of cuboid
O(1)=q1+L/2*cosd(theta);
O(2)=q2+L/2*sind(theta);
O(3)=q3+W/2;

% radius
r=max([L/2,W/2,sqrt(W*L)])*sqrt(2);

inside=sqrt((x1-O(1)).^2+(x2-O(2)).^2+(x3-O(3)).^2)<optionStruct.radius*r;
outside=~inside;

% initiate empty array
u1=zeros(size(x1));
u2=zeros(size(x1));
u3=zeros(size(x1));

% numerical solution with Gauss-Legendre quadrature for points
% outside the cuboid
if numel(x1(outside))>0
    [u1(outside),u2(outside),u3(outside)]= ...
        computeDisplacementVerticalCuboidGauss( ...
            x1(outside),x2(outside),x3(outside), ...
            q1,q2,q3,L,T,W,theta, ...
            e11p,e12p,e13p,e22p,e23p,e33p, ...
            nu,'n',optionStruct.n);
end

% numerical solution with double-exponential quadrature for points
% inside the cuboid
if numel(x1(inside))>0
    [u1(inside),u2(inside),u3(inside)]= ...
        computeDisplacementVerticalCuboidTanhSinh( ...
            x1(inside),x2(inside),x3(inside), ...
            q1,q2,q3,L,T,W,theta, ...
            e11p,e12p,e13p,e22p,e23p,e33p,nu, ...
            'precision',optionStruct.precision, ...
            'bound',optionStruct.bound);
end

end

function p = validatePrecision(x)
if ~(isreal(x) && isscalar(x) && x > 0)
    error('computeDisplacementVerticalCuboidMixedQuad:invalidPrecision','invalid precision');
end
p = true;
end

function p = validateBound(x)
if ~(isreal(x) && isscalar(x) && x > 0)
    error('computeDisplacementVerticalCuboidMixedQuad:invalidBound','invalid truncation');
end
p = true;
end

function p = validateN(x)
if ~(x > 0)
    error('computeDisplacementVerticalCuboidMixedQuad:invalid','invalid number of integration points');
end
p = true;
end

function p = validateRadius(x)
if ~(isreal(x) && isscalar(x) && x > 0)
    error('computeDisplacementVerticalCuboidMixedQuad:invalidRadius','invalid radius');
end
p = true;
end

