function [u1,u2,u3]=computeDisplacementVerticalCuboidSurfaceMixedQuad( ...
    x1,x2,q1,q2,q3,L,T,W,theta, ...
    epsv11,epsv12,epsv13,epsv22,epsv23,epsv33,nu,varargin)
% function COMPUTEDISPLACEMENTKERNELVERTICALCUBOIDSURFACEMIXEDQUAD 
% computes the displacement field associated with deforming vertical cuboid
% volume element using either the Gauss-Legendre or the double-exponential 
% quadrature based on the distance to the center of the volume element
% assuming the following geometry:
%
%                      N (x1)
%                     /
%                    /| strike (theta)
%        q1,q2,q3 ->@-------------------------+   E (x2)
%                   |                         | w    +
%                   :                         | i   /
%                   |                         | d  / s
%                   :                         | t / s
%                   |                         | h/ e
%                   :                         | / n
%                   +-------------------------+  k
%                   :        l e n g t h      / c
%                   |                        / i
%                   :                       / h
%                   |                      / t
%                   :                     /
%                   |                    +
%                   Z (x3)
%
% INPUT:
% x1, x2             north and east coordinates of the observation point
% q1, q2, q3         north, east and depth coordinates of the cuboid
% L, T, W            length, thickness and width of the cuboid
% theta (degree)     strike angle from north (from x1) of the cuboid
% epsvij             source strain component ij in the cuboid in the 
%                    reference system tied to the cuboid.
% nu                 Poisson's ratio in the half space.
%
% OPTIONS:
% 'n',integer        number of integration points for the Gauss-Legendre
%                    quadrature [15].
% 'precision'        controls the number of points used for the
%                    double-exponential integration [0.001].
% 'bound'            truncation of the double-exponential integral [3.5].
%
% OUTPUT:
% u1                 displacement component in the north direction,
% u2                 displacement component in the east  direction,
% u3                 displacement component in the down  direction.
%
% AUTHOR: Sylvain Barbot (sbarbot@ntu.edu.sg) - June 2, 2018, Los Angeles.

assert(min(x3(:))>=0,'computeDisplacementVerticalCuboidSurfaceMixedQuad: observation depth must be positive.');
assert(q3>=0,'computeDisplacementVerticalCuboidSurfaceMixedQuad: source depth must be positive.');

% cuboid center
O=[q1+L/2*cosd(theta);
   q2+L/2*sind(theta);
   q3+W/2];

A=[q1+T/2*sind(theta);
   q2+T/2*cosd(theta);
   q3];

% diagonal length
r=norm(O(:)-A(:));

inside=sqrt((x1-O(1)).^2+(x2-O(2)).^2+(x3-O(3)).^2)<1.2*r;
outside=~inside;

% initiate empty array
u1=zeros(size(x1));
u2=zeros(size(x1));
u3=zeros(size(x1));

% numerical solution with Gauss-Legendre quadrature for points 
% outside the circumsphere
if numel(x1(outside))>0
    [u1(outside),u2(outside),u3(outside)]= ...
        computeDisplacementVerticalCuboidSurfaceGauss( ...
                x1,x2,q1,q2,q3,L,T,W,theta, ...
                epsv11,epsv12,epsv13,epsv22,epsv23,epsv33,nu,'N',7);
end

% numerical solution with double-exponential quadrature for points 
% inside the circumsphere
if numel(x1(inside))>0
    [u1(inside),u2(inside),u3(inside)]= ...
        computeDisplacementVerticalCuboidSurfaceTanhSinh( ...
                x1,x2,q1,q2,q3,L,T,W,theta, ...
                epsv11,epsv12,epsv13,epsv22,epsv23,epsv33,nu,'Precision',0.01,'Bound',3);
end

end

