function [u1,u2,u3]=computeDisplacementVerticalSemiInfiniteTriangleGauss( ...
    x1,x2,x3,A,B,C, ...
    e11,e12,e13,e22,e23,e33,nu,varargin)
% function COMPUTEDISPLACEMENTKERNELVERTICALTRIANGLESHEARZONEGAUSS
% computes the surface displacement field associated with deforming
% vertical volume elements using the Gauss-Legendre quadrature considering
% the following geometry:
%
%                     N (x1)      + A
%                       /        /|\
%                      /        / . \
%                     /        /  |  \
%                    /        /   .   \
%                   /        /    |    + B
%                  /        /     . /  |
%                 /        /     .|    |
%                /        /   /        |
%               /        / .           |
%              /     C  +              |
%             /         |              |
%            @----------------------------- E (x2)
%            |          |               
%            |          |          
%            |          |
%            |          |
%            |          |infinity
%            x3
%
% Input:
% x1, x2, x3         north, east, and depth coordinates of the observation points
% A, B, C            north, east, and depth coordinates of the top vertices,
% eij                source strain component ij in the volume element in the 
%                    reference system tied to the volume element.
% nu                 Poisson's ratio in the half space.
%
% Options:
% 'N',integer        number of integration points for the Gauss-Legendre
%                    quadrature [15].
% 'precision'        controls the number of points used for the
%                    double-exponential integration [0.001].
% 'bound'            truncation of the double-exponential integral [3.5].
%
% Output:
% u1                 displacement component in the north direction,
% u2                 displacement component in the east direction,
% u3                 displacement component in the down direction.
%
% Author: Sylvain Barbot (sbarbot@usc.edu) - June 29, 2020, Culver City.

assert(min(x3(:))>=0,'observation depth must be positive.');
assert(min([A(3),B(3),C(3)])==max([A(3),B(3),C(3)]),'A, B, and C must be at the same depth.');
assert(A(3)>0,'source depth must be positive.');

% depth of triangle face
q3=A(3);

% process optional input
p = inputParser;
p.addParameter('N',15,@validateN);
p.addParameter('precision',0.001,@validatePrecision);
p.addParameter('bound',3.5,@validateBound);
p.parse(varargin{:});
optionStruct = p.Results;

% Lame parameter
lambda=2*nu/(1-2*nu);

% array size
s=size(x1);

% unit vectors
nA = [C(2)-B(2);
      B(1)-C(1);
      0]/norm(C-B);
nB = [C(2)-A(2);
      A(1)-C(1);
      0]/norm(C-A);
nC = [B(2)-A(2);
      A(1)-B(1);
      0]/norm(B-A);

% check that unit vectors are pointing outward
if (nA'*(A(:)-(B(:)+C(:))/2))>0
    nA=-nA;
end
if (nB'*(B(:)-(A(:)+C(:))/2))>0
    nB=-nB;
end
if (nC'*(C(:)-(A(:)+B(:))/2))>0
    nC=-nC;
end

% area of triangle ABC
ABC=norm(cross(C-A,B-A))/2;

% Radii
r1=@(y1,y2,y3) sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
r2=@(y1,y2,y3) sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);

%% numerical solution with tanh/sinh quadrature

% numerical solution with Gauss-Legendre quadrature
[x,w]=gaussxw(-1,1,optionStruct.N);

u1=zeros(s);
u2=zeros(s);
u3=zeros(s);

% numerical solution with tanh/sinh quadrature
h=optionStruct.precision;
n=fix(1/h*optionStruct.bound);

% isotropic strain
ekk=e11+e22+e33;

% moment density
m11=lambda*ekk+2*e11;
m12=2*e12;
m13=2*e13;
m22=lambda*ekk+2*e22;
m23=2*e23;
m33=lambda*ekk+2*e33;

% parameterized surface integral, mapping uv space to three-dimensional space
y=@(u,v,A,B,C) A*(1-u)*(1-v)/4+B*(1+u)*(1-v)/4+C*(1+v)/2;

% Gauss-Legendre integration over horizontal faces
for k=1:length(x)
    for j=1:length(x)
        u1=u1+w(j)*w(k)*(1-x(k))*IU1h(x(j),x(k));
        u2=u2+w(j)*w(k)*(1-x(k))*IU2h(x(j),x(k));
        u3=u3+w(j)*w(k)*(1-x(k))*IU3h(x(j),x(k));
    end
end

% parameterized line integral
z=@(t,A,B) (A+B)/2+t*(B-A)/2;

% Gauss-Legendre and Double-Exponential integration over vertical faces
for j=-n:n
    wj=0.5*h*pi*cosh(j*h).*exp(0.5*pi*sinh(j*h));
    xj=exp(0.5*pi*sinh(j*h));
    for k=1:length(x)
        u1=u1+wj*w(k)*IU1v(x(k),xj);
        u2=u2+wj*w(k)*IU2v(x(k),xj);
        u3=u3+wj*w(k)*IU3v(x(k),xj);
    end
end

        function d = G11(y1,y2,y3)
        lr1=r1(y1,y2,y3);
        lr2=r2(y1,y2,y3);
        d=1/(16*pi*(1-nu))*( ...
            (3-4*nu)./lr1+1./lr2+(x1-y1).^2./lr1.^3 ...
            +(3-4*nu)*(x1-y1).^2./lr2.^3+2*x3.*y3.*(lr2.^2-3*(x1-y1).^2)./lr2.^5 ...
            +4*(1-2*nu)*(1-nu)*(lr2.^2-(x1-y1).^2+lr2.*(x3+y3))./(lr2.*(lr2+x3+y3).^2)...
            );
    end
    function d = G12(y1,y2,y3)
        lr1=r1(y1,y2,y3);
        lr2=r2(y1,y2,y3);
        d=(x1-y1).*(x2-y2)/(16*pi*(1-nu)).*( ...
            1./lr1.^3+(3-4*nu)./lr2.^3-6*x3.*y3./lr2.^5 ...
            -4*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3).^2) ...
            );
    end
    function d = G13(y1,y2,y3)
        lr1=r1(y1,y2,y3);
        lr2=r2(y1,y2,y3);
        d=(x1-y1)/(16*pi*(1-nu)).*( ...
            (x3-y3)./lr1.^3+(3-4*nu)*(x3-y3)./lr2.^3 ...
            -6*x3.*y3.*(x3+y3)./lr2.^5+4*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3)) ...
            );
    end
    function d = G21(y1,y2,y3)
        lr1=r1(y1,y2,y3);
        lr2=r2(y1,y2,y3);
        d=(x1-y1).*(x2-y2)/(16*pi*(1-nu)).*( ...
            1./lr1.^3+(3-4*nu)./lr2.^3-6*x3.*y3./lr2.^5 ...
            -4*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3).^2) ...
            );
    end
    function d = G22(y1,y2,y3)
        lr1=r1(y1,y2,y3);
        lr2=r2(y1,y2,y3);
        d=1/(16*pi*(1-nu))*( ...
            (3-4*nu)./lr1+1./lr2+(x2-y2).^2./lr1.^3 ...
            +(3-4*nu)*(x2-y2).^2./lr2.^3+2*x3.*y3.*(lr2.^2-3*(x2-y2).^2)./lr2.^5 ...
            +4*(1-2*nu)*(1-nu)*(lr2.^2-(x2-y2).^2+lr2.*(x3+y3))./(lr2.*(lr2+x3+y3).^2)...
            );
    end
    function d = G23(y1,y2,y3)
        lr1=r1(y1,y2,y3);
        lr2=r2(y1,y2,y3);
        d=(x2-y2)/(16*pi*(1-nu)).*( ...
            (x3-y3)./lr1.^3+(3-4*nu)*(x3-y3)./lr2.^3 ...
            -6*x3.*y3.*(x3+y3)./lr2.^5+4*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3)) ...
            );
    end
    function d = G31(y1,y2,y3)
        lr1=r1(y1,y2,y3);
        lr2=r2(y1,y2,y3);
        d=(x1-y1)/(16*pi*(1-nu)).*( ...
            (x3-y3)./lr1.^3+(3-4*nu)*(x3-y3)./lr2.^3 ...
            +6*x3.*y3.*(x3+y3)./lr2.^5-4*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3)) ...
            );
    end
    function d = G32(y1,y2,y3)
        lr1=r1(y1,y2,y3);
        lr2=r2(y1,y2,y3);
        d=(x2-y2)/(16*pi*(1-nu)).*( ...
            (x3-y3)./lr1.^3+(3-4*nu)*(x3-y3)./lr2.^3 ...
            +6*x3.*y3.*(x3+y3)./lr2.^5-4*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3)) ...
            );
    end
    function d = G33(y1,y2,y3)
        lr1=r1(y1,y2,y3);
        lr2=r2(y1,y2,y3);
        d=1/(16*pi*(1-nu))*( ...
            (3-4*nu)./lr1+(5-12*nu+8*nu^2)./lr2+(x3-y3).^2./lr1.^3 ...
            +6*x3.*y3.*(x3+y3).^2./lr2.^5+((3-4*nu)*(x3+y3).^2-2*x3.*y3)./lr2.^3 ...
            );
    end

    % integration over (-1,1) for the horizontal faces
    function d = IU1h(u,v)
        % function IU1 is the integrand for displacement component u1
        d=zeros(s);
        if m13 ~= 0
            d=d-m13*ABC/4*G11(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if m23 ~= 0
            d=d-m23*ABC/4*G21(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if m33 ~= 0
            d=d-m33*ABC/4*G31(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
    end

    function d = IU2h(u,v)
        % function IU2 is the integrand for displacement component u1
        d=zeros(s);
        if m13 ~= 0
            d=d-m13*ABC/4*G12(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if m23 ~= 0
            d=d-m23*ABC/4*G22(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if m33 ~= 0
            d=d-m33*ABC/4*G32(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
    end

    function d = IU3h(u,v)
        % function IU3 is the integrand for displacement component u3
        d=zeros(s);
        if m13 ~= 0
            d=d-m13*ABC/4*G13(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if m23 ~= 0
            d=d-m23*ABC/4*G23(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if m33 ~= 0
            d=d-m33*ABC/4*G33(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
    end

    % integration over (0,infinity) for the vertical faces
    function u = IU1v(t,y)
        % function IU1 is the integrand for displacement component u1
        
        u=zeros(s);
        
        % face nA
        tA=[m11*nA(1)+m12*nA(2);
            m12*nA(1)+m22*nA(2);
            m13*nA(1)+m23*nA(2)];
        if 0 ~= tA(1)
            u=u+tA(1)*norm(B-C)/2*G11(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        if 0 ~= tA(2)
            u=u+tA(2)*norm(B-C)/2*G21(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        if 0 ~= tA(3)
            u=u+tA(3)*norm(B-C)/2*G31(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        
        % face nB
        tB=[m11*nB(1)+m12*nB(2);
            m12*nB(1)+m22*nB(2);
            m13*nB(1)+m23*nB(2)];
        if 0 ~= tB(1)
            u=u+tB(1)*norm(A-C)/2*G11(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        if 0 ~= tB(2)
            u=u+tB(2)*norm(A-C)/2*G21(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        if 0 ~= tB(3)
            u=u+tB(3)*norm(A-C)/2*G31(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        
        % face nC
        tC=[m11*nC(1)+m12*nC(2);
            m12*nC(1)+m22*nC(2);
            m13*nC(1)+m23*nC(2)];
        if 0 ~= tC(1)
            u=u+tC(1)*norm(A-B)/2*G11(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
        if 0 ~= tC(2)
            u=u+tC(2)*norm(A-B)/2*G21(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
        if 0 ~= tC(3)
            u=u+tC(3)*norm(A-B)/2*G31(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
    end

    function u = IU2v(t,y)
        % function IU2 is the integrand for displacement component u1
        
        u=zeros(s);
        
        % face nA
        tA=[m11*nA(1)+m12*nA(2);
            m12*nA(1)+m22*nA(2);
            m13*nA(1)+m23*nA(2)];
        if 0 ~= tA(1)
            u=u+tA(1)*norm(B-C)/2*G12(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        if 0 ~= tA(2)
            u=u+tA(2)*norm(B-C)/2*G22(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        if 0 ~= tA(3)
            u=u+tA(3)*norm(B-C)/2*G32(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        
        % face nB
        tB=[m11*nB(1)+m12*nB(2);
            m12*nB(1)+m22*nB(2);
            m13*nB(1)+m23*nB(2)];
        if 0 ~= tB(1)
            u=u+tB(1)*norm(A-C)/2*G12(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        if 0 ~= tB(2)
            u=u+tB(2)*norm(A-C)/2*G22(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        if 0 ~= tB(3)
            u=u+tB(3)*norm(A-C)/2*G32(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        
        % face nC
        tC=[m11*nC(1)+m12*nC(2);
            m12*nC(1)+m22*nC(2);
            m13*nC(1)+m23*nC(2)];
        if 0 ~= tC(1)
            u=u+tC(1)*norm(A-B)/2*G12(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
        if 0 ~= tC(2)
            u=u+tC(2)*norm(A-B)/2*G22(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
        if 0 ~= tC(3)
            u=u+tC(3)*norm(A-B)/2*G32(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
    end

    function u = IU3v(t,y)
        % function IU3 is the integrand for displacement component u3
        
        u=zeros(s);
        
        % face nA
        tA=[m11*nA(1)+m12*nA(2);
            m12*nA(1)+m22*nA(2);
            m13*nA(1)+m23*nA(2)];
        if 0 ~= tA(1)
            u=u+tA(1)*norm(B-C)/2*G13(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        if 0 ~= tA(2)
            u=u+tA(2)*norm(B-C)/2*G23(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        if 0 ~= tA(3)
            u=u+tA(3)*norm(B-C)/2*G33(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        
        % face nB
        tB=[m11*nB(1)+m12*nB(2);
            m12*nB(1)+m22*nB(2);
            m13*nB(1)+m23*nB(2)];
        if 0 ~= tB(1)
            u=u+tB(1)*norm(A-C)/2*G13(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        if 0 ~= tB(2)
            u=u+tB(2)*norm(A-C)/2*G23(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        if 0 ~= tB(3)
            u=u+tB(3)*norm(A-C)/2*G33(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        
        % face nC
        tC=[m11*nC(1)+m12*nC(2);
            m12*nC(1)+m22*nC(2);
            m13*nC(1)+m23*nC(2)];
        if 0 ~= tC(1)
            u=u+tC(1)*norm(A-B)/2*G13(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
        if 0 ~= tC(2)
            u=u+tC(2)*norm(A-B)/2*G23(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
        if 0 ~= tC(3)
            u=u+tC(3)*norm(A-B)/2*G33(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
    end

end

function p = validatePrecision(x)
if ~(isreal(x) && isscalar(x) && x > 0)
    error(message('MATLAB:mcmc:invalidPrecision'));
end
p = true;
end

function p = validateBound(x)
if ~(isreal(x) && isscalar(x) && x > 0)
    error(message('MATLAB:mcmc:invalidBound'));
end
p = true;
end

function p = validateN(x)
if ~(x > 0)
    error('MATLAB:invalid','invalid number of integration points');
end
p = true;
end

function [xn,wn]  = gaussxw(a,b, n)
% GAUSSXW finds sample points xn and weights wn for Gaussian
% quadrature on (a,b) with n nodes
%
%    function [xn wn]  = gaussxw(a,b, n)
%
%  Integral = wn(:)' * f (xn(:))
%  or  sum(wn.*f(xn))
%
%  Saves nodes and weights from previous call with same n.
%
% AUTHOR: Trefethen, Spectral Method in Matlab

persistent N x w

% check if we need new x, w
if (isempty(N) == 1 || N ~= n)
    N=n;
    beta=0.5./sqrt(1-(2*(1:N-1)).^(-2));
    T=diag(beta,1)+diag(beta,-1);
    [V,D]=eig(T);
    x=diag(D);
    [x,i]=sort(x);
    w=2*V(1,i).^2;
end

% finds sample x and weights for interval (a,b)
xn=(a+b+x(:)*(b-a))/2;
wn=(b-a)*w(:)/2;
end
