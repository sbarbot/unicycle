function [s11,s12,s13,s22,s23,s33]=computeStressVerticalSemiInfiniteTriangleGauss( ...
    x1,x2,x3,A,B,C, ...
    e11,e12,e13,e22,e23,e33,G,nu,varargin)
% function COMPUTESTRESSVERTICALSEMIINFINITETRIANGLEGAUSS
% computes the surface displacement field associated with a deforming
% semi-infinite triangle volume element using the Gauss-Legendre quadrature
% considering the following geometry:
%
%                     N (x1)      + A
%                       /        /|\
%                      /        / . \
%                     /        /  |  \
%                    /        /   .   \
%                   /        /    |    + B
%                  /        /     . /  |
%                 /        /     .|    |
%                /        /   /        |
%               /        / .           |
%              /     C  +              |
%             /         |              |
%            @----------------------------- E (x2)
%            |          |               
%            |          |          
%            |          |
%            |          |
%            |          |?
%            x3
%
% Input:
% x1, x2, x3         north, east, and depth coordinates of the observation points
% A, B, C            north, east, and depth coordinates of the top vertices,
% eij                source strain component ij in the volume element in the 
%                    reference system tied to the volume element.
% G,nu               Rigidity and Poisson's ratio in the half space.
%
% Options:
% 'N',integer        number of integration points for the Gauss-Legendre
%                    quadrature [15].
% 'precision'        controls the number of points used for the
%                    double-exponential integration [0.001].
% 'bound'            truncation of the double-exponential integral [3.5].
%
% Output:
% sij                stress components ij in the (north, east, down)
%                    coordinate system.
%
% Author: Sylvain Barbot (sbarbot@usc.edu) - June 29, 2020, Culver City.

assert(min(x3(:))>=0,'observation depth must be positive.');
assert(min([A(3),B(3),C(3)])==max([A(3),B(3),C(3)]),'A, B, and C must be at the same depth.');
assert(A(3)>0,'source depth must be positive.');

% depth of triangle face
q3=A(3);

% process optional input
p = inputParser;
p.addParameter('N',15,@validateN);
p.addParameter('precision',0.001,@validatePrecision);
p.addParameter('bound',3.5,@validateBound);
p.parse(varargin{:});
optionStruct = p.Results;

% Lame parameter
lambda=2*nu/(1-2*nu);

% array size
s=size(x1);

% unit vectors
nA = [C(2)-B(2);
      B(1)-C(1);
      0]/norm(C-B);
nB = [C(2)-A(2);
      A(1)-C(1);
      0]/norm(C-A);
nC = [B(2)-A(2);
      A(1)-B(1);
      0]/norm(B-A);

% check that unit vectors are pointing outward
if (nA'*(A(:)-(B(:)+C(:))/2))>0
    nA=-nA;
end
if (nB'*(B(:)-(A(:)+C(:))/2))>0
    nB=-nB;
end
if (nC'*(C(:)-(A(:)+B(:))/2))>0
    nC=-nC;
end

% area of triangle ABC
ABC=norm(cross(C-A,B-A))/2;

% Radii
r1=@(y1,y2,y3) sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
r2=@(y1,y2,y3) sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);

%% numerical solution with tanh/sinh quadrature

% numerical solution with Gauss-Legendre quadrature
[x,w]=gaussxw(-1,1,optionStruct.N);

u11=zeros(s);
u12=zeros(s);
u13=zeros(s);
u21=zeros(s);
u22=zeros(s);
u23=zeros(s);
u31=zeros(s);
u32=zeros(s);
u33=zeros(s);

% numerical solution with tanh/sinh quadrature
h=optionStruct.precision;
n=fix(1/h*optionStruct.bound);

% isotropic strain
ekk=e11+e22+e33;

% moment density
m11=lambda*ekk+2*e11;
m12=2*e12;
m13=2*e13;
m22=lambda*ekk+2*e22;
m23=2*e23;
m33=lambda*ekk+2*e33;

% parameterized surface integral, mapping uv space to three-dimensional space
y=@(u,v,A,B,C) A*(1-u)*(1-v)/4+B*(1+u)*(1-v)/4+C*(1+v)/2;

% Gauss-Legendre integration over horizontal faces
for k=1:length(x)
    for j=1:length(x)
        u11=u11+w(j)*w(k)*(1-x(k))*IU11h(x(j),x(k));
        u12=u12+w(j)*w(k)*(1-x(k))*IU12h(x(j),x(k));
        u13=u13+w(j)*w(k)*(1-x(k))*IU13h(x(j),x(k));
        
        u21=u21+w(j)*w(k)*(1-x(k))*IU21h(x(j),x(k));
        u22=u22+w(j)*w(k)*(1-x(k))*IU22h(x(j),x(k));
        u23=u23+w(j)*w(k)*(1-x(k))*IU23h(x(j),x(k));
        
        u31=u31+w(j)*w(k)*(1-x(k))*IU31h(x(j),x(k));
        u32=u32+w(j)*w(k)*(1-x(k))*IU32h(x(j),x(k));
        u33=u33+w(j)*w(k)*(1-x(k))*IU33h(x(j),x(k));
    end
end

% parameterized line integral
z=@(t,A,B) (A+B)/2+t*(B-A)/2;

% Gauss-Legendre and Double-Exponential integration over vertical faces
for j=-n:n
    wj=0.5*h*pi*cosh(j*h).*exp(0.5*pi*sinh(j*h));
    xj=exp(0.5*pi*sinh(j*h));
    for k=1:length(x)
        u11=u11+wj*w(k)*IU11v(x(k),xj);
        u12=u12+wj*w(k)*IU12v(x(k),xj);
        u13=u13+wj*w(k)*IU13v(x(k),xj);
        
        u21=u21+wj*w(k)*IU21v(x(k),xj);
        u22=u22+wj*w(k)*IU22v(x(k),xj);
        u23=u23+wj*w(k)*IU23v(x(k),xj);
        
        u31=u31+wj*w(k)*IU31v(x(k),xj);
        u32=u32+wj*w(k)*IU32v(x(k),xj);
        u33=u33+wj*w(k)*IU33v(x(k),xj);
    end
end

% remove anelastic strain
omega=  heaviside(((A(1)+B(1))/2-x1)*nC(1)+((A(2)+B(2))/2-x2)*nC(2)) ...
      .*heaviside(((B(1)+C(1))/2-x1)*nA(1)+((B(2)+C(2))/2-x2)*nA(2)) ...
      .*heaviside(((C(1)+A(1))/2-x1)*nB(1)+((C(2)+A(2))/2-x2)*nB(2)) ...
      .*heaviside(x3-q3);

% strain components
e11=u11        -omega*e11;
e12=(u12+u21)/2-omega*e12;
e13=(u13+u31)/2-omega*e13;
e22=u22        -omega*e22;
e23=(u23+u32)/2-omega*e23;
e33=u33        -omega*e33;

% divergence
ekk=e11+e22+e33;

% stress components
s11=G*(2*e11+lambda*ekk);
s12=G*2*e12;
s13=G*2*e13;
s22=G*(2*e22+lambda*ekk);
s23=G*2*e23;
s33=G*(2*e33+lambda*ekk);

        % Green's functions
    function d=G11d1(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*(x1-y1).*( ...
            -(3-4*nu)./lr1.^3 ...
            -1./lr2.^3 ...
            +(2*lr1.^2-3*(x1-y1).^2)./lr1.^5 ...
            +(3-4*nu)*(2*lr2.^2-3*(x1-y1).^2)./lr2.^5 ...
            -6*y3.*x3.*(3*lr2.^2-5*(x1-y1).^2)./lr2.^7 ...
            -4*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3).^2) ...
            -8*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3).^2) ...
            +4*(1-2*nu)*(1-nu)*(x1-y1).^2./(lr2.^3.*(lr2+x3+y3).^2) ...
            +8*(1-2*nu)*(1-nu)*(x1-y1).^2./(lr2.^2.*(lr2+x3+y3).^3) ...
            );
    end

    function d=G11d2(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*(x2-y2).*( ...
            -(3-4*nu)./lr1.^3 ...
            -1./lr2.^3 ...
            -3*(x1-y1).^2./lr1.^5 ...
            -3*(3-4*nu)*(x1-y1).^2./lr2.^5 ...
            -6*y3.*x3.*(lr2.^2-5*(x1-y1).^2)./lr2.^7 ...
            -4*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3).^2) ...
            +4*(1-2*nu)*(1-nu)*(x1-y1).^2.*(3*lr2+x3+y3)./(lr2.^3.*(lr2+x3+y3).^3) ...
            );
    end

    function d=G11d3(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*( ...
            -(3-4*nu)*(x3-y3)./lr1.^3 ...
            -(x3+y3)./lr2.^3 ...
            -3*(x1-y1).^2.*(x3-y3)./lr1.^5 ...
            -3*(3-4*nu)*(x1-y1).^2.*(x3+y3)./lr2.^5 ...
            +2*y3.*(lr2.^2-3*x3.*(x3+y3))./lr2.^5 ...
            -6*y3.*(x1-y1).^2.*(lr2.^2-5*x3.*(x3+y3))./lr2.^7 ...
            -4*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3)) ...
            +4*(1-2*nu)*(1-nu)*(x1-y1).^2.*(2*lr2+x3+y3)./(lr2.^3.*(lr2+x3+y3).^2) ...
            );
    end

    function d=G21d1(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*(x2-y2).*( ...
            +(lr1.^2-3*(x1-y1).^2)./lr1.^5 ...
            +(3-4*nu)*(lr2.^2-3*(x1-y1).^2)./lr2.^5 ...
            -6*y3.*x3.*(lr2.^2-5*(x1-y1).^2)./lr2.^7 ...
            -4*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3).^2) ...
            +4*(1-2*nu)*(1-nu)*(x1-y1).^2.*(3*lr2+x3+y3)./(lr2.^3.*(lr2+x3+y3).^3) ...
            );
    end

    function d=G21d2(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*(x1-y1).*( ...
            +(lr1.^2-3*(x2-y2).^2)./lr1.^5 ...
            +(3-4*nu)*(lr2.^2-3*(x2-y2).^2)./lr2.^5 ...
            -6*y3.*x3.*(lr2.^2-5*(x2-y2).^2)./lr2.^7 ...
            -4*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3).^2) ...
            +4*(1-2*nu)*(1-nu)*(x2-y2).^2.*(3*lr2+x3+y3)./(lr2.^3.*(lr2+x3+y3).^3) ...
            );
    end

    function d=G21d3(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*(x1-y1).*(x2-y2).*( ...
            -3*(x3-y3)./lr1.^5 ...
            -3*(3-4*nu)*(x3+y3)./lr2.^5 ...
            -6*y3.*(lr2.^2-5*x3.*(x3+y3))./lr2.^7 ...
            +4*(1-2*nu)*(1-nu)*(2*lr2+x3+y3)./(lr2.^3.*(lr2+x3+y3).^2) ...
            );
    end

    function d=G31d1(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*( ...
            +(x3-y3).*(lr1.^2-3*(x1-y1).^2)./lr1.^5 ...
            +(3-4*nu)*(x3-y3).*(lr2.^2-3*(x1-y1).^2)./lr2.^5 ...
            +6*y3.*x3.*(x3+y3).*(lr2.^2-5*(x1-y1).^2)./lr2.^7 ...
            -4*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3)) ...
            +4*(1-2*nu)*(1-nu)*(x1-y1).^2.*(2*lr2+x3+y3)./(lr2.^3.*(lr2+x3+y3).^2) ...
            );
    end

    function d=G31d2(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*(x1-y1).*(x2-y2).*( ...
            -3*(x3-y3)./lr1.^5 ...
            -3*(3-4*nu)*(x3-y3)./lr2.^5 ...
            -30*y3.*x3.*(x3+y3)./lr2.^7 ...
            +4*(1-2*nu)*(1-nu)*(2*lr2+x3+y3)./(lr2.^3.*(lr2+x3+y3).^2) ...
            );
    end

    function d=G31d3(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*(x1-y1).*( ...
            +(lr1.^2-3*(x3-y3).^2)./lr1.^5 ...
            +(3-4*nu)*(lr2.^2-3*(x3.^2-y3.^2))./lr2.^5 ...
            +6*y3.*(2*x3+y3)./lr2.^5 ...
            -30*y3.*x3.*(x3+y3).^2./lr2.^7 ...
            +4*(1-2*nu)*(1-nu)./lr2.^3 ...
            );
    end

    function d=G12d1(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*(x2-y2).*( ...
            +(lr1.^2-3*(x1-y1).^2)./lr1.^5 ...
            +(3-4*nu)*(lr2.^2-3*(x1-y1).^2)./lr2.^5 ...
            -6*y3.*x3.*(lr2.^2-5*(x1-y1).^2)./lr2.^7 ...
            -4*(1-nu)*(1-2*nu)./(lr2.*(lr2+x3+y3).^2) ...
            +4*(1-nu)*(1-2*nu)*(x1-y1).^2.*(3*lr2+x3+y3)./(lr2.^3.*(lr2+x3+y3).^3) ...
            );
    end

    function d=G12d2(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*(x1-y1).*( ...
            +(lr1.^2-3*(x2-y2).^2)./lr1.^5 ...
            +(3-4*nu)*(lr2.^2-3*(x2-y2).^2)./lr2.^5 ...
            -6*y3.*x3.*(lr2.^2-5*(x2-y2).^2)./lr2.^7 ...
            -4*(1-nu)*(1-2*nu)./(lr2.*(lr2+x3+y3).^2) ...
            +4*(1-nu)*(1-2*nu)*(x2-y2).^2.*(3*lr2+x3+y3)./(lr2.^3.*(lr2+x3+y3).^3) ...
            );
    end

    function d=G12d3(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*(x1-y1).*(x2-y2).*( ...
            -3*(x3-y3)./lr1.^5 ...
            -3*(3-4*nu)*(x3+y3)./lr2.^5 ...
            -6*y3.*(lr2.^2-5*x3.*(x3+y3))./lr2.^7 ...
            +4*(1-2*nu)*(1-nu)*(2*lr2+x3+y3)./(lr2.^3.*(lr2+x3+y3).^2) ...
            );
    end

    function d=G22d1(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*(x1-y1).*( ...
            -(3-4*nu)./lr1.^3 ...
            -1./lr2.^3 ...
            -3*(x2-y2).^2./lr1.^5 ...
            -3*(3-4*nu)*(x2-y2).^2./lr2.^5 ...
            -6*y3.*x3.*(lr2.^2-5*(x2-y2).^2)./lr2.^7 ...
            -4*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3).^2) ...
            +4*(1-2*nu)*(1-nu)*(x2-y2).^2.*(3*lr2+x3+y3)./(lr2.^3.*(lr2+x3+y3).^3) ...
            );
    end

    function d=G22d2(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*(x2-y2).*( ...
            -(3-4*nu)./lr1.^3 ...
            -1./lr2.^3 ...
            +(2*lr1.^2-3*(x2-y2).^2)./lr1.^5 ...
            +(3-4*nu)*(2*lr2.^2-3*(x2-y2).^2)./lr2.^5 ...
            -6*y3.*x3.*(3*lr2.^2-5*(x2-y2).^2)./lr2.^7 ...
            -12*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3).^2) ...
            +4*(1-2*nu)*(1-nu)*(x2-y2).^2.*(3*lr2+x3+y3)./(lr2.^3.*(lr2+x3+y3).^3) ...
            );
    end

    function d=G22d3(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*( ...
            -(3-4*nu)*(x3-y3)./lr1.^3 ...
            -(x3+y3)./lr2.^3 ...
            -3*(x2-y2).^2.*(x3-y3)./lr1.^5 ...
            -3*(3-4*nu)*(x2-y2).^2.*(x3+y3)./lr2.^5 ...
            +2*y3.*(lr2.^2-3*x3.*(x3+y3))./lr2.^5 ...
            -6*y3.*(x2-y2).^2.*(lr2.^2-5*x3.*(x3+y3))./lr2.^7 ...
            -4*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3)) ...
            +4*(1-2*nu)*(1-nu)*(x2-y2).^2.*(2*lr2+x3+y3)./(lr2.^3.*(lr2+x3+y3).^2) ...
            );
    end

    function d=G32d1(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*(x1-y1).*(x2-y2).*( ...
            -3*(x3-y3)./lr1.^5 ...
            -3*(3-4*nu)*(x3-y3)./lr2.^5 ...
            -30*y3.*x3.*(x3+y3)./lr2.^7 ...
            +4*(1-2*nu)*(1-nu)*(2*lr2+x3+y3)./(lr2.^3.*(lr2+x3+y3).^2) ...
            );
    end

    function d=G32d2(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*( ...
            +(x3-y3).*(lr1.^2-3*(x2-y2).^2)./lr1.^5 ...
            +(3-4*nu)*(x3-y3).*(lr2.^2-3*(x2-y2).^2)./lr2.^5 ...
            +6*y3.*x3.*(x3+y3).*(lr2.^2-5*(x2-y2).^2)./lr2.^7 ...
            -4*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3)) ...
            +4*(1-2*nu)*(1-nu)*(x2-y2).^2.*(2*lr2+x3+y3)./(lr2.^3.*(lr2+x3+y3).^2) ...
            );
    end

    function d=G32d3(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*(x2-y2).*( ...
            +(lr1.^2-3*(x3-y3).^2)./lr1.^5 ...
            +(3-4*nu)*(lr2.^2-3*(x3.^2-y3.^2))./lr2.^5 ...
            +6*y3.*(2*x3+y3)./lr2.^5 ...
            -30*y3.*x3.*(x3+y3).^2./lr2.^7 ...
            +4*(1-2*nu)*(1-nu)./lr2.^3 ...
            );
    end

    function d=G13d1(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*( ...
            +(x3-y3).*(lr1.^2-3*(x1-y1).^2)./lr1.^5 ...
            +(3-4*nu)*(x3-y3).*(lr2.^2-3*(x1-y1).^2)./lr2.^5 ...
            -6*y3.*x3.*(x3+y3).*(lr2.^2-5*(x1-y1).^2)./lr2.^7 ...
            +4*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3)) ...
            -4*(1-2*nu)*(1-nu)*(x1-y1).^2.*(2*lr2+x3+y3)./(lr2.^3.*(lr2+x3+y3).^2) ...
            );
    end

    function d=G13d2(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*(x1-y1).*(x2-y2).*( ...
            -3*(x3-y3)./lr1.^5 ...
            -3*(3-4*nu)*(x3-y3)./lr2.^5 ...
            +30*y3.*x3.*(x3+y3)./lr2.^7 ...
            -4*(1-2*nu)*(1-nu)*(2*lr2+x3+y3)./(lr2.^3.*(lr2+x3+y3).^2) ...
            );
    end

    function d=G13d3(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*(x1-y1).*( ...
            +(lr1.^2-3*(x3-y3).^2)./lr1.^5 ...
            +(3-4*nu)*(lr2.^2-3*(x3.^2-y3.^2))./lr2.^5 ...
            -6*y3.*(2*x3+y3)./lr2.^5 ...
            +30*y3.*x3.*(x3+y3).^2./lr2.^7 ...
            -4*(1-2*nu)*(1-nu)./lr2.^3 ...
            );
    end

    function d=G23d1(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*(x1-y1).*(x2-y2).*( ...
            -3*(x3-y3)./lr1.^5 ...
            -3*(3-4*nu)*(x3-y3)./lr2.^5 ...
            +30*y3.*x3.*(x3+y3)./lr2.^7 ...
            -4*(1-2*nu)*(1-nu)*(2*lr2+x3+y3)./(lr2.^3.*(lr2+x3+y3).^2) ...
            );
    end

    function d=G23d2(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*( ...
            +(x3-y3).*(lr1.^2-3*(x2-y2).^2)./lr1.^5 ...
            +(3-4*nu)*(x3-y3).*(lr2.^2-3*(x2-y2).^2)./lr2.^5 ...
            -6*y3.*x3.*(x3+y3).*(lr2.^2-5*(x2-y2).^2)./lr2.^7 ...
            +4*(1-2*nu)*(1-nu)./(lr2.*(lr2+x3+y3)) ...
            -4*(1-2*nu)*(1-nu)*(x2-y2).^2.*(2*lr2+x3+y3)./(lr2.^3.*(lr2+x3+y3).^2) ...
            );
    end

    function d=G23d3(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*(x2-y2).*( ...
            +(lr1.^2-3*(x3-y3).^2)./lr1.^5 ...
            +(3-4*nu)*(lr2.^2-3*(x3.^2-y3.^2))./lr2.^5 ...
            -6*y3.*(2*x3+y3)./lr2.^5 ...
            +30*y3.*x3.*(x3+y3).^2./lr2.^7 ...
            -4*(1-2*nu)*(1-nu)./lr2.^3 ...
            );
    end

    function d=G33d1(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*(x1-y1).*( ...
            -(3-4*nu)./lr1.^3 ...
            -(5-12*nu+8*nu^2)./lr2.^3 ...
            -3*(x3-y3).^2./lr1.^5 ...
            -30*y3.*x3.*(x3+y3).^2./lr2.^7 ...
            -3*(3-4*nu)*(x3+y3).^2./lr2.^5 ...
            +6*y3.*x3./lr2.^5 ...
            );
    end

    function d=G33d2(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*(x2-y2).*( ...
            -(3-4*nu)./lr1.^3 ...
            -(5-12*nu+8*nu^2)./lr2.^3 ...
            -3*(x3-y3).^2./lr1.^5 ...
            -30*y3.*x3.*(x3+y3).^2./lr2.^7 ...
            -3*(3-4*nu)*(x3+y3).^2./lr2.^5 ...
            +6*y3.*x3./lr2.^5 ...
            );
    end

    function d=G33d3(y1,y2,y3)
        % Radii
        lr1=sqrt((x1-y1).^2+(x2-y2).^2+(x3-y3).^2);
        lr2=sqrt((x1-y1).^2+(x2-y2).^2+(x3+y3).^2);
        
        d=1/(16*pi*(1-nu))*( ...
            -(3-4*nu)*(x3-y3)./lr1.^3 ...
            -(5-12*nu+8*nu^2)*(x3+y3)./lr2.^3 ...
            +(x3-y3).*(2*lr1.^2-3*(x3-y3).^2)./lr1.^5 ...
            +6*y3.*(x3+y3).^2./lr2.^5 ...
            +6*y3.*x3.*(x3+y3).*(2*lr2.^2-5*(x3+y3).^2)./lr2.^7 ...
            +(3-4*nu)*(x3+y3).*(2*lr2.^2-3*(x3+y3).^2)./lr2.^5 ...
            -2*y3.*(lr2.^2-3*x3.*(x3+y3))./lr2.^5 ...
            );
    end

    % integration over (-1,1) for the horizontal faces
    function d = IU11h(u,v)
        % function IU1 is the integrand for displacement component u1
        d=zeros(s);
        if m13 ~= 0
            d=d-m13*ABC/4*G11d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if m23 ~= 0
            d=d-m23*ABC/4*G21d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if m33 ~= 0
            d=d-m33*ABC/4*G31d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
    end

    function d = IU12h(u,v)
        % function IU1 is the integrand for displacement component u1
        d=zeros(s);
        if m13 ~= 0
            d=d-m13*ABC/4*G11d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if m23 ~= 0
            d=d-m23*ABC/4*G21d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if m33 ~= 0
            d=d-m33*ABC/4*G31d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
    end

    function d = IU13h(u,v)
        % function IU1 is the integrand for displacement component u1
        d=zeros(s);
        if m13 ~= 0
            d=d-m13*ABC/4*G11d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if m23 ~= 0
            d=d-m23*ABC/4*G21d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if m33 ~= 0
            d=d-m33*ABC/4*G31d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
    end

    function d = IU21h(u,v)
        % function IU2 is the integrand for displacement component u1
        d=zeros(s);
        if m13 ~= 0
            d=d-m13*ABC/4*G12d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if m23 ~= 0
            d=d-m23*ABC/4*G22d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if m33 ~= 0
            d=d-m33*ABC/4*G32d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
    end

    function d = IU22h(u,v)
        % function IU2 is the integrand for displacement component u1
        d=zeros(s);
        if m13 ~= 0
            d=d-m13*ABC/4*G12d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if m23 ~= 0
            d=d-m23*ABC/4*G22d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if m33 ~= 0
            d=d-m33*ABC/4*G32d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
    end

    function d = IU23h(u,v)
        % function IU2 is the integrand for displacement component u1
        d=zeros(s);
        if m13 ~= 0
            d=d-m13*ABC/4*G12d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if m23 ~= 0
            d=d-m23*ABC/4*G22d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if m33 ~= 0
            d=d-m33*ABC/4*G32d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
    end

    function d = IU31h(u,v)
        % function IU3 is the integrand for displacement component u3
        d=zeros(s);
        if m13 ~= 0
            d=d-m13*ABC/4*G13d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if m23 ~= 0
            d=d-m23*ABC/4*G23d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if m33 ~= 0
            d=d-m33*ABC/4*G33d1(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
    end

    function d = IU32h(u,v)
        % function IU3 is the integrand for displacement component u3
        d=zeros(s);
        if m13 ~= 0
            d=d-m13*ABC/4*G13d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if m23 ~= 0
            d=d-m23*ABC/4*G23d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if m33 ~= 0
            d=d-m33*ABC/4*G33d2(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
    end

    function d = IU33h(u,v)
        % function IU3 is the integrand for displacement component u3
        d=zeros(s);
        if m13 ~= 0
            d=d-m13*ABC/4*G13d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if m23 ~= 0
            d=d-m23*ABC/4*G23d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
        if m33 ~= 0
            d=d-m33*ABC/4*G33d3(y(u,v,A(1),B(1),C(1)),y(u,v,A(2),B(2),C(2)),y(u,v,A(3),B(3),C(3)));
        end
    end

    % integration over (0,infinity) for the vertical faces
    function u = IU11v(t,y)
        % function IU1 is the integrand for displacement component u1
        
        u=zeros(s);
        
        % face nA
        tA=[m11*nA(1)+m12*nA(2);
            m12*nA(1)+m22*nA(2);
            m13*nA(1)+m23*nA(2)];
        if 0 ~= tA(1)
            u=u+tA(1)*norm(B-C)/2*G11d1(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        if 0 ~= tA(2)
            u=u+tA(2)*norm(B-C)/2*G21d1(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        if 0 ~= tA(3)
            u=u+tA(3)*norm(B-C)/2*G31d1(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        
        % face nB
        tB=[m11*nB(1)+m12*nB(2);
            m12*nB(1)+m22*nB(2);
            m13*nB(1)+m23*nB(2)];
        if 0 ~= tB(1)
            u=u+tB(1)*norm(A-C)/2*G11d1(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        if 0 ~= tB(2)
            u=u+tB(2)*norm(A-C)/2*G21d1(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        if 0 ~= tB(3)
            u=u+tB(3)*norm(A-C)/2*G31d1(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        
        % face nC
        tC=[m11*nC(1)+m12*nC(2);
            m12*nC(1)+m22*nC(2);
            m13*nC(1)+m23*nC(2)];
        if 0 ~= tC(1)
            u=u+tC(1)*norm(A-B)/2*G11d1(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
        if 0 ~= tC(2)
            u=u+tC(2)*norm(A-B)/2*G21d1(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
        if 0 ~= tC(3)
            u=u+tC(3)*norm(A-B)/2*G31d1(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
    end

    function u = IU12v(t,y)
        % function IU1 is the integrand for displacement component u1
        
        u=zeros(s);
        
        % face nA
        tA=[m11*nA(1)+m12*nA(2);
            m12*nA(1)+m22*nA(2);
            m13*nA(1)+m23*nA(2)];
        if 0 ~= tA(1)
            u=u+tA(1)*norm(B-C)/2*G11d2(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        if 0 ~= tA(2)
            u=u+tA(2)*norm(B-C)/2*G21d2(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        if 0 ~= tA(3)
            u=u+tA(3)*norm(B-C)/2*G31d2(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        
        % face nB
        tB=[m11*nB(1)+m12*nB(2);
            m12*nB(1)+m22*nB(2);
            m13*nB(1)+m23*nB(2)];
        if 0 ~= tB(1)
            u=u+tB(1)*norm(A-C)/2*G11d2(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        if 0 ~= tB(2)
            u=u+tB(2)*norm(A-C)/2*G21d2(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        if 0 ~= tB(3)
            u=u+tB(3)*norm(A-C)/2*G31d2(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        
        % face nC
        tC=[m11*nC(1)+m12*nC(2);
            m12*nC(1)+m22*nC(2);
            m13*nC(1)+m23*nC(2)];
        if 0 ~= tC(1)
            u=u+tC(1)*norm(A-B)/2*G11d2(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
        if 0 ~= tC(2)
            u=u+tC(2)*norm(A-B)/2*G21d2(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
        if 0 ~= tC(3)
            u=u+tC(3)*norm(A-B)/2*G31d2(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
    end

    function u = IU13v(t,y)
        % function IU1 is the integrand for displacement component u1
        
        u=zeros(s);
        
        % face nA
        tA=[m11*nA(1)+m12*nA(2);
            m12*nA(1)+m22*nA(2);
            m13*nA(1)+m23*nA(2)];
        if 0 ~= tA(1)
            u=u+tA(1)*norm(B-C)/2*G11d3(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        if 0 ~= tA(2)
            u=u+tA(2)*norm(B-C)/2*G21d3(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        if 0 ~= tA(3)
            u=u+tA(3)*norm(B-C)/2*G31d3(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        
        % face nB
        tB=[m11*nB(1)+m12*nB(2);
            m12*nB(1)+m22*nB(2);
            m13*nB(1)+m23*nB(2)];
        if 0 ~= tB(1)
            u=u+tB(1)*norm(A-C)/2*G11d3(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        if 0 ~= tB(2)
            u=u+tB(2)*norm(A-C)/2*G21d3(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        if 0 ~= tB(3)
            u=u+tB(3)*norm(A-C)/2*G31d3(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        
        % face nC
        tC=[m11*nC(1)+m12*nC(2);
            m12*nC(1)+m22*nC(2);
            m13*nC(1)+m23*nC(2)];
        if 0 ~= tC(1)
            u=u+tC(1)*norm(A-B)/2*G11d3(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
        if 0 ~= tC(2)
            u=u+tC(2)*norm(A-B)/2*G21d3(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
        if 0 ~= tC(3)
            u=u+tC(3)*norm(A-B)/2*G31d3(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
    end

    function u = IU21v(t,y)
        % function IU2 is the integrand for displacement component u1
        
        u=zeros(s);
        
        % face nA
        tA=[m11*nA(1)+m12*nA(2);
            m12*nA(1)+m22*nA(2);
            m13*nA(1)+m23*nA(2)];
        if 0 ~= tA(1)
            u=u+tA(1)*norm(B-C)/2*G12d1(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        if 0 ~= tA(2)
            u=u+tA(2)*norm(B-C)/2*G22d1(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        if 0 ~= tA(3)
            u=u+tA(3)*norm(B-C)/2*G32d1(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        
        % face nB
        tB=[m11*nB(1)+m12*nB(2);
            m12*nB(1)+m22*nB(2);
            m13*nB(1)+m23*nB(2)];
        if 0 ~= tB(1)
            u=u+tB(1)*norm(A-C)/2*G12d1(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        if 0 ~= tB(2)
            u=u+tB(2)*norm(A-C)/2*G22d1(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        if 0 ~= tB(3)
            u=u+tB(3)*norm(A-C)/2*G32d1(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        
        % face nC
        tC=[m11*nC(1)+m12*nC(2);
            m12*nC(1)+m22*nC(2);
            m13*nC(1)+m23*nC(2)];
        if 0 ~= tC(1)
            u=u+tC(1)*norm(A-B)/2*G12d1(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
        if 0 ~= tC(2)
            u=u+tC(2)*norm(A-B)/2*G22d1(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
        if 0 ~= tC(3)
            u=u+tC(3)*norm(A-B)/2*G32d1(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
    end

    function u = IU22v(t,y)
        % function IU2 is the integrand for displacement component u1
        
        u=zeros(s);
        
        % face nA
        tA=[m11*nA(1)+m12*nA(2);
            m12*nA(1)+m22*nA(2);
            m13*nA(1)+m23*nA(2)];
        if 0 ~= tA(1)
            u=u+tA(1)*norm(B-C)/2*G12d2(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        if 0 ~= tA(2)
            u=u+tA(2)*norm(B-C)/2*G22d2(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        if 0 ~= tA(3)
            u=u+tA(3)*norm(B-C)/2*G32d2(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        
        % face nB
        tB=[m11*nB(1)+m12*nB(2);
            m12*nB(1)+m22*nB(2);
            m13*nB(1)+m23*nB(2)];
        if 0 ~= tB(1)
            u=u+tB(1)*norm(A-C)/2*G12d2(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        if 0 ~= tB(2)
            u=u+tB(2)*norm(A-C)/2*G22d2(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        if 0 ~= tB(3)
            u=u+tB(3)*norm(A-C)/2*G32d2(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        
        % face nC
        tC=[m11*nC(1)+m12*nC(2);
            m12*nC(1)+m22*nC(2);
            m13*nC(1)+m23*nC(2)];
        if 0 ~= tC(1)
            u=u+tC(1)*norm(A-B)/2*G12d2(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
        if 0 ~= tC(2)
            u=u+tC(2)*norm(A-B)/2*G22d2(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
        if 0 ~= tC(3)
            u=u+tC(3)*norm(A-B)/2*G32d2(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
    end

    function u = IU23v(t,y)
        % function IU2 is the integrand for displacement component u1
        
        u=zeros(s);
        
        % face nA
        tA=[m11*nA(1)+m12*nA(2);
            m12*nA(1)+m22*nA(2);
            m13*nA(1)+m23*nA(2)];
        if 0 ~= tA(1)
            u=u+tA(1)*norm(B-C)/2*G12d3(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        if 0 ~= tA(2)
            u=u+tA(2)*norm(B-C)/2*G22d3(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        if 0 ~= tA(3)
            u=u+tA(3)*norm(B-C)/2*G32d3(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        
        % face nB
        tB=[m11*nB(1)+m12*nB(2);
            m12*nB(1)+m22*nB(2);
            m13*nB(1)+m23*nB(2)];
        if 0 ~= tB(1)
            u=u+tB(1)*norm(A-C)/2*G12d3(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        if 0 ~= tB(2)
            u=u+tB(2)*norm(A-C)/2*G22d3(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        if 0 ~= tB(3)
            u=u+tB(3)*norm(A-C)/2*G32d3(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        
        % face nC
        tC=[m11*nC(1)+m12*nC(2);
            m12*nC(1)+m22*nC(2);
            m13*nC(1)+m23*nC(2)];
        if 0 ~= tC(1)
            u=u+tC(1)*norm(A-B)/2*G12d3(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
        if 0 ~= tC(2)
            u=u+tC(2)*norm(A-B)/2*G22d3(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
        if 0 ~= tC(3)
            u=u+tC(3)*norm(A-B)/2*G32d3(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
    end

    function u = IU31v(t,y)
        % function IU3 is the integrand for displacement component u3
        
        u=zeros(s);
        
        % face nA
        tA=[m11*nA(1)+m12*nA(2);
            m12*nA(1)+m22*nA(2);
            m13*nA(1)+m23*nA(2)];
        if 0 ~= tA(1)
            u=u+tA(1)*norm(B-C)/2*G13d1(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        if 0 ~= tA(2)
            u=u+tA(2)*norm(B-C)/2*G23d1(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        if 0 ~= tA(3)
            u=u+tA(3)*norm(B-C)/2*G33d1(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        
        % face nB
        tB=[m11*nB(1)+m12*nB(2);
            m12*nB(1)+m22*nB(2);
            m13*nB(1)+m23*nB(2)];
        if 0 ~= tB(1)
            u=u+tB(1)*norm(A-C)/2*G13d1(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        if 0 ~= tB(2)
            u=u+tB(2)*norm(A-C)/2*G23d1(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        if 0 ~= tB(3)
            u=u+tB(3)*norm(A-C)/2*G33d1(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        
        % face nC
        tC=[m11*nC(1)+m12*nC(2);
            m12*nC(1)+m22*nC(2);
            m13*nC(1)+m23*nC(2)];
        if 0 ~= tC(1)
            u=u+tC(1)*norm(A-B)/2*G13d1(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
        if 0 ~= tC(2)
            u=u+tC(2)*norm(A-B)/2*G23d1(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
        if 0 ~= tC(3)
            u=u+tC(3)*norm(A-B)/2*G33d1(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
    end

    function u = IU32v(t,y)
        % function IU3 is the integrand for displacement component u3
        
        u=zeros(s);
        
        % face nA
        tA=[m11*nA(1)+m12*nA(2);
            m12*nA(1)+m22*nA(2);
            m13*nA(1)+m23*nA(2)];
        if 0 ~= tA(1)
            u=u+tA(1)*norm(B-C)/2*G13d2(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        if 0 ~= tA(2)
            u=u+tA(2)*norm(B-C)/2*G23d2(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        if 0 ~= tA(3)
            u=u+tA(3)*norm(B-C)/2*G33d2(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        
        % face nB
        tB=[m11*nB(1)+m12*nB(2);
            m12*nB(1)+m22*nB(2);
            m13*nB(1)+m23*nB(2)];
        if 0 ~= tB(1)
            u=u+tB(1)*norm(A-C)/2*G13d2(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        if 0 ~= tB(2)
            u=u+tB(2)*norm(A-C)/2*G23d2(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        if 0 ~= tB(3)
            u=u+tB(3)*norm(A-C)/2*G33d2(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        
        % face nC
        tC=[m11*nC(1)+m12*nC(2);
            m12*nC(1)+m22*nC(2);
            m13*nC(1)+m23*nC(2)];
        if 0 ~= tC(1)
            u=u+tC(1)*norm(A-B)/2*G13d2(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
        if 0 ~= tC(2)
            u=u+tC(2)*norm(A-B)/2*G23d2(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
        if 0 ~= tC(3)
            u=u+tC(3)*norm(A-B)/2*G33d2(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
    end

    function u = IU33v(t,y)
        % function IU3 is the integrand for displacement component u3
        
        u=zeros(s);
        
        % face nA
        tA=[m11*nA(1)+m12*nA(2);
            m12*nA(1)+m22*nA(2);
            m13*nA(1)+m23*nA(2)];
        if 0 ~= tA(1)
            u=u+tA(1)*norm(B-C)/2*G13d3(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        if 0 ~= tA(2)
            u=u+tA(2)*norm(B-C)/2*G23d3(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        if 0 ~= tA(3)
            u=u+tA(3)*norm(B-C)/2*G33d3(z(t,B(1),C(1)),z(t,B(2),C(2)),y+q3);
        end
        
        % face nB
        tB=[m11*nB(1)+m12*nB(2);
            m12*nB(1)+m22*nB(2);
            m13*nB(1)+m23*nB(2)];
        if 0 ~= tB(1)
            u=u+tB(1)*norm(A-C)/2*G13d3(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        if 0 ~= tB(2)
            u=u+tB(2)*norm(A-C)/2*G23d3(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        if 0 ~= tB(3)
            u=u+tB(3)*norm(A-C)/2*G33d3(z(t,A(1),C(1)),z(t,A(2),C(2)),y+q3);
        end
        
        % face nC
        tC=[m11*nC(1)+m12*nC(2);
            m12*nC(1)+m22*nC(2);
            m13*nC(1)+m23*nC(2)];
        if 0 ~= tC(1)
            u=u+tC(1)*norm(A-B)/2*G13d3(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
        if 0 ~= tC(2)
            u=u+tC(2)*norm(A-B)/2*G23d3(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
        if 0 ~= tC(3)
            u=u+tC(3)*norm(A-B)/2*G33d3(z(t,A(1),B(1)),z(t,A(2),B(2)),y+q3);
        end
    end

end



function p = validatePrecision(x)
if ~(isreal(x) && isscalar(x) && x > 0)
    error(message('MATLAB:mcmc:invalidPrecision'));
end
p = true;
end

function p = validateBound(x)
if ~(isreal(x) && isscalar(x) && x > 0)
    error(message('MATLAB:mcmc:invalidBound'));
end
p = true;
end

function p = validateN(x)
if ~(x > 0)
    error('MATLAB:invalid','invalid number of integration points');
end
p = true;
end

function [xn,wn]  = gaussxw(a,b, n)
% GAUSSXW finds sample points xn and weights wn for Gaussian
% quadrature on (a,b) with n nodes
%
%    function [xn wn]  = gaussxw(a,b, n)
%
%  Integral = wn(:)' * f (xn(:))
%  or  sum(wn.*f(xn))
%
%  Saves nodes and weights from previous call with same n.
%
% AUTHOR: Trefethen, Spectral Method in Matlab

persistent N x w

% check if we need new x, w
if (isempty(N) == 1 || N ~= n)
    N=n;
    beta=0.5./sqrt(1-(2*(1:N-1)).^(-2));
    T=diag(beta,1)+diag(beta,-1);
    [V,D]=eig(T);
    x=diag(D);
    [x,i]=sort(x);
    w=2*V(1,i).^2;
end

% finds sample x and weights for interval (a,b)
xn=(a+b+x(:)*(b-a))/2;
wn=(b-a)*w(:)/2;
end

function y=heaviside(x)
y=x>=0;
end