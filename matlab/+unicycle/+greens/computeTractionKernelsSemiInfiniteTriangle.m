function [K11s,K12s,K13s,K22s,K23s,K33s, ...
          K11d,K12d,K13d,K22d,K23d,K33d, ...
          K11n,K12n,K13n,K22n,K23n,K33n]=computeTractionKernelsSemiInfiniteTriangle(src,rcv,G,nu)
% computeTractionKernelsVerticalCuboid computes the stress on a receiver faults due to
% strain on a volume element.
%
%   [K11s,K12s,K13s,K22s,K23s,K33s, ...
%    K11d,K12d,K13d,K22d,K23d,K33d, ...
%    K11n,K12n,K13n,K22n,K23n,K33n]=COMPUTETRACTIONKERNELSSEMIINFINITETRIANGLE(src,rcv,G,nu)
%
% where nu is Poisson's ratio, G is the shear modulus. src is a structure
% with volume element geometry:
%
%   src.A         : position of triangle vertices A
%   src.B         : position of triangle vertices B
%   src.C         : position of triangle vertices C
%
% rcv is a structure with fault position and geometry:
%
%   rcv.xc : patch center position (:,3)
%   rcv.sv : patch strike unit vector (:,3)
%   rcv.dv : patch dip unit vector (:,3)
%   rcv.nv : patch normal unit vector (:,3)
%
% OUTPUT:
%
%   [Kss,Ksd,Ksn,Kds,Kdd,Kdn]=TRIANGLESTRESSKERNELS(src,rcv,G,nu)
%
% returns the shear stress kernels. Kss is the traction in the strike
% direction due to strike slip. Ksd is the traction in the dip direction
% due to strike slip. Kds is the tarction in the strike direction due to
% dip slip and Kdd is the traction in the dip direction due to dip slip.
%
%
% AUTHOR: Sylvain Barbot, 07/16/2016, Earth Observatory of Singapore
%
% SEE ALSO: unicycle

import unicycle.greens.*
import unicycle.utils.*

M=size(rcv.xc,1);

lambda=G*2*nu/(1-2*nu);

out=cell(6,3);

bar=textProgressBar('# semi-infinite triangle traction kernels: ');

e=eye(6);

% change of coordinates
R=[[0,1, 0];
   [1,0, 0];
   [0,0,-1]];

% loop of causes (strain on volume elements)
for j=1:6
    
    % initialize stress kernels
    Kes=zeros(M,src.N);
    Ked=zeros(M,src.N);
    Ken=zeros(M,src.N);
    
    for k=1:src.N
        if 0==mod(k-1,2)
            bar.progress(((j-1)*src.N+k)/(6*src.N)*100);
        end
        
        % stress components
        [s22,s12,s23,s11,s13,s33]=computeStressVerticalSemiInfiniteTriangleGauss( ...
                rcv.xc(:,2),rcv.xc(:,1),-rcv.xc(:,3), ...
                R*src.x(src.vertices(k,1),:)',R*src.x(src.vertices(k,2),:)',R*src.x(src.vertices(k,3),:)', ...
                e(1,j),e(2,j),e(3,j),e(4,j),e(5,j),e(6,j), ...
                G,nu,'N',7,'precision',0.01);
    
        s13=-s13;
        s23=-s23;
        
        % full traction vector on receiver faults
        t=[...
            s11.*rcv.nv(:,1)+s12.*rcv.nv(:,2)+s13.*rcv.nv(:,3), ...
            s12.*rcv.nv(:,1)+s22.*rcv.nv(:,2)+s23.*rcv.nv(:,3), ...
            s13.*rcv.nv(:,1)+s23.*rcv.nv(:,2)+s33.*rcv.nv(:,3)];
        
        % shear stress in strike direction
        Kes(:,k)=sum(t.*rcv.sv,2);
        
        % shear stress in dip direction
        Ked(:,k)=sum(t.*rcv.dv,2);
        
        % stress in normal direction
        Ken(:,k)=sum(t.*rcv.nv,2);
        
    end
    
    out{j,1}=Kes;
    out{j,2}=Ked;
    out{j,3}=Ken;
    
end

[K11s,K12s,K13s,K22s,K23s,K33s, ...
 K11d,K12d,K13d,K22d,K23d,K33d, ...
 K11n,K12n,K13n,K22n,K23n,K33n]=deal(out{:});

bar.progress(100);
bar.close();

end
