classdef semiInfiniteCuboidVolume < unicycle.greens.earthModel
    properties
        % rigidity
        G;
        % Poisson's ratio
        nu;
    end
    methods
        %% % % % % % % % % % % % % % % % % %
        %                                  %
        %      c o n s t r u c t o r       %
        %                                  %
        % % % % % % % % % % % % % % % % % %%
        function o=semiInfiniteCuboidVolume(G,nu)
            % SEMIINFINITECUBOIDVOLUME is a class providing the necessary functions to
            % compute the stress interactions between source and receiver
            % volume elements.
            %
            %   earthModel = greens.semiInfiniteCuboidVolume(G,nu);
            %
            % where G is rigidity and nu is the Poisson's ratio of a
            % homogeneous elastic half space.
            %
            % SEE ALSO: unicycle
            
            if (0==nargin)
                return
            end
            
            assert(0<=G,'unicycle.greens.semiInfiniteCuboidVolume::rigidity must be positive.')
            assert(nu<=0.5,'unicycle.greens.semiInfiniteCuboidVolume::Poisson''s ratio should be lower than 0.5.')
            assert(-1<=nu,'unicycle.greens.semiInfiniteCuboidVolume::Poisson''s ratio should be greater than -1.')
            
            o.G=G;
            o.nu=nu;
        end
        
        function [varargout]=tractionKernels(obj,src,rcv)
            % TRACTIONKERNELS computes the stress on receiver faults due to
            % motion of triangular dislocations.
            %
            % rcv - receiver fault
            %
            % SEE ALSO: unicycle, geometry.triangle
            
            import unicycle.greens.*
            
            varargout = cell(1,nargout);
            [varargout{:}]=computeTractionKernelsVerticalSemiInfiniteCuboid(src,rcv,obj.G,obj.nu);
        end
        
        function [varargout]=stressKernels(obj,src,rcv)
            % STRESSKERNELS computes the stress on receiver faults due to
            % motion of rectangular dislocations in a half space.
            %
            % rcv - receiver shear zone
            %
            % SEE ALSO: unicycle
            
            import unicycle.greens.*
            
            varargout = cell(1,nargout);
            [varargout{:}]=computeStressKernelsVerticalSemiInfiniteCuboid(src,rcv,obj.G,obj.nu);
        end
        
        function [varargout]=displacementKernels(obj,src,x,vecsize)
            % DISPLACEMENTKERNELS computes the stress on receiver faults due to
            % motion of rectangular dislocations in a half space.
            %
            % src - source fault
            %
            % SEE ALSO: unicycle
            
            varargout = cell(1,nargout);
            [varargout{:}]=unicycle.greens.computeDisplacementKernelsVerticalSemiInfiniteCuboid(src,obj.nu,x,vecsize);
        end
    end % methods
end % class definition

