classdef semiInfiniteTriangle < unicycle.greens.earthModel
    properties
        % rigidity
        G;
        % Poisson's ratio
        nu;
    end
    methods
        %% % % % % % % % % % % % % % % % % %
        %                                  %
        %      c o n s t r u c t o r       %
        %                                  %
        % % % % % % % % % % % % % % % % % %%
        function o=semiInfiniteTriangle(G,nu)
            % SEMIINFINITETRIANGLE is a class providing the necessary functions to
            % compute the stress interactions between source and receiver
            % semi-infinite triangle volume elements.
            %
            %   earthModel = greens.semiInfiniteTriangle(G,nu);
            %
            % where G is rigidity and nu is the Poisson's ratio of a
            % homogeneous elastic half space.
            %
            % SEE ALSO: unicycle
            
            if (0==nargin)
                return
            end
            
            assert(0<=G,'rigidity must be positive.')
            assert(nu<=0.5,'Poisson''s ratio should be lower than 0.5.')
            assert(-1<=nu,'Poisson''s ratio should be greater than -1.')
            
            o.G=G;
            o.nu=nu;
        end
        
        function [varargout]=tractionKernels(obj,src,rcv,varargin)
            % TRACTIONKERNELS computes the stress on receiver faults due to
            % motion of triangular dislocations.
            %
            % rcv - receiver fault
            %
            % SEE ALSO: unicycle, geometry.semiInfiniteTriangle
            
            varargout = cell(1,nargout);
            [varargout{:}]=unicycle.greens.computeTractionKernelsSemiInfiniteTriangle(src,rcv,obj.G,obj.nu,varargin{:});
        end
        
        function [varargout]=stressKernels(obj,src,rcv,varargin)
            % STRESSKERNELS computes the stress on receiver faults due to
            % motion of rectangular dislocations in a half space.
            %
            % rcv - receiver shear zone
            %
            % SEE ALSO: unicycle
            
            varargout = cell(1,nargout);
            [varargout{:}]=unicycle.greens.computeStressKernelsSemiInfiniteTriangle(src,rcv,obj.G,obj.nu,varargin{:});
        end
        
        function [varargout]=displacementKernels(obj,src,x,vecsize,varargin)
            % DISPLACEMENTKERNELS computes the stress on receiver faults due to
            % motion of rectangular dislocations in a half space.
            %
            % src - source fault
            %
            % SEE ALSO: unicycle

            varargout = cell(1,nargout);
            [varargout{:}]=unicycle.greens.computeDisplacementKernelsSemiInfiniteTriangle(src,obj.nu,x,vecsize,varargin{:});
        end
    end % methods
end % class definition

