classdef gpsReceiver < unicycle.manifold.gps
    methods
        %% % % % % % % % % % % % % % % % % %
        %                                  %
        %      c o n s t r u c t o r       %
        %                                  %
        % % % % % % % % % % % % % % % % % %%
        function obj = gpsReceiver(network,evl,vecsize)
            % GPS is a class representing GPS data and Green's functions.
            %
            %   gps = manifold.gpsReceiver(network,evl,vecsize)
            %
            % creates a instance of GPS data connected to a simulation.
            %
            % INPUT:
            %
            % network  if network is a filename, for example 'sopac.dat', 
            %          must contain
            %
            %                    # i NAME x1 x2 x3
            %                      1 GPS1  0  0  0
            %                      2 GPS2  1  1  0
            %
            %          alternatively, network may be a N x vecsize vector
            %          containing locations of stations for evaluating 
            %          displacements.
            %
            % evl      object of type ode.evolution containing a time
            %          series of fault displacement, can be empty.
            %
            % vecsize  number of components of displacement vector
            %
            % SEE ALSO: unicycle
            
            import unicycle.greens.*
            
            obj.vecsize=vecsize;
            
            if isnumeric(network)
                obj.x=network;
                obj.D=size(network,1);
                obj.stationName=1:obj.D;
            else
                filename=network;
                [~,obj.stationName,x1,x2,x3]=...
                    textread(filename,'%d %s %f %f %f','commentstyle','shell');
                obj.x=[x2,x1,0*x3];
                obj.D=size(x2,1);
            end
            
            % compute displacement kernels
            if ~isempty(evl)
                obj.displacementKernels(evl);
            end
            
        end % constructor
        
        % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
        %
        %    C O M P U T E   D I S P L A C E M E N T   K E R N E L S
        %
        % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
        function displacementKernels(obj,evl)
            
           % source Green's functions (includes strike slip and dip slip)
            if isobject(evl.src)
                obj.kernelLabel.FO={'s', 'd'};
                
                if ~exist(evl.kernelPath)
                    % compute displacement Green's function, no export
                    [obj.FO{1,1},obj.FO{2,1}]=evl.src.displacementKernels(obj.x,obj.vecsize);
                    warning('# directory %s must exist for export\n',evl.kernelPath);
                else
                    if ~exist(strcat(evl.kernelPath,'FO_s.grd'),'file')
                        % compute displacement Green's function
                        [obj.FO{1,1},obj.FO{2,1}]=evl.src.displacementKernels(obj.x,obj.vecsize);
                        % export to .grd file
                        for i = 1:numel(obj.FO)
                            fname=strcat(evl.kernelPath,'FO_',obj.kernelLabel.FO{i},'.grd');
                            unicycle.export.grdwrite([0 1], [0 1],obj.FO{i},fname)
                        end
                    else
                        % load .grd files
                        for i = 1:numel(obj.FO)
                            fname=strcat(evl.kernelPath,'FO_',obj.kernelLabel.FO{i},'.grd');
                            [~,~,obj.FO{i}]=unicycle.export.grdread(fname);
                        end
                    end
                end
            end
          
            % receiver Green's functions (includes strike slip and dip slip)
            if isobject(evl.flt)
                obj.kernelLabel.KO={'s', 'd'};
                if ~exist(evl.kernelPath)
                    % compute displacement Green's function, no export
                    [obj.KO{1,1},obj.KO{2,1}]=evl.flt.displacementKernels(obj.x,obj.vecsize);
                    warning('# directory %s must exist for export\n',evl.kernelPath);
                else
                    if ~exist(strcat(evl.kernelPath,'KO_s.grd'),'file')
                        % compute displacement Green's function
                        [obj.KO{1,1},obj.KO{2,1}]=evl.flt.displacementKernels(obj.x,obj.vecsize);
                        % export .grd files
                        for i = 1:numel(obj.KO)
                            fname=strcat(evl.kernelPath,'/KO_',obj.kernelLabel.KO{i},'.grd');
                            unicycle.export.grdwrite([0 1], [0 1],obj.KO{i},fname)
                        end
                    else
                        for i = 1:numel(obj.KO)
                            % load .grd files
                            fname=strcat(evl.kernelPath,'KO_',obj.kernelLabel.KO{i},'.grd');
                            [~,~,obj.KO{i}]=unicycle.export.grdread(fname);
                        end
                    end
                end
            end
                        
             % Strain Volume Green's functions (includes strike slip and dip slip)
             if isobject(evl.shz)
                 obj.kernelLabel.LO={'11','12','13','22','23','33'};
                 if ~exist(evl.kernelPath)
                     % compute displacement Green's function, no export
                     [obj.LO{1,1},obj.LO{2,1},obj.LO{3,1}, ...
                            obj.LO{4,1},obj.LO{5,1},obj.LO{6,1}]=evl.shz.displacementKernels(obj.x,obj.vecsize);
                     warning('# directory %s must exist for export\n',evl.kernelPath);
                 else
                    if ~exist(strcat(evl.kernelPath,'LO_11.grd'),'file')
                        % compute displacement Green's function
                        [obj.LO{1,1},obj.LO{2,1},obj.LO{3,1}, ...
                            obj.LO{4,1},obj.LO{5,1},obj.LO{6,1}]=evl.shz.displacementKernels(obj.x,obj.vecsize);
                        % export .grd file
                        for i = 1:numel(obj.LO)
                            fname=strcat(evl.kernelPath,'LO_',obj.kernelLabel.LO{i},'.grd');
                            unicycle.export.grdwrite([0 1], [0 1],obj.LO{i},fname)
                        end
                    else
                        for i = 1:numel(obj.LO)
                            % load .grd files
                            fname=strcat(evl.kernelPath,'LO_',obj.kernelLabel.LO{i},'.grd');
                            [~,~,obj.LO{i}]=unicycle.export.grdread(fname);
                        end
                    end
                 end
            end            
            
            % builds forward models of geodetic data if simulation exists
            if ~isempty(evl.y)
                obj.simulation(evl);
            end 
        end
  
    end % methods
end
