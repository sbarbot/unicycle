function [L2222,L2223,L2233, ...
          L2322,L2323,L2333, ...
          L3322,L3323,L3333]=computeStressKernelsPlaneStrainTriangle(src,rcv,G,nu)
% function COMPUTESTRESSKERNELSPLANESTRAINTRIANGLE compute the stress
% kernels in plane strain condition for the deformation caused by deforming
% triangle volume elements on other triangle volume elements using the
% collocation method.
%
% function [L2222,L2223,L2233, ...
%           L2322,L2323,L2333, ...
%           L3322,L3323,L3333]=computeStressKernelsPlaneStrainTriangle(src,rcv,G,nu)
%
% Lijkl is the stress component Skl due to strain component Eij, 
% both in the volume-element-centric (primed) system of coordinates.
%
% INPUT:
% src.t - 3xN integers describing the source triangle vertices
% src.p - coordinates of the vertices 2*M
% rcv.t - 3xN integers describing the receiver triangle vertices
% rcv.p - coordinates of the vertices 2*M

out=cell(3,3);

% diagonal matrix
I=eye(3);

% collocation points
xc=(rcv.p(rcv.t(:,1),:)+rcv.p(rcv.t(:,2),:)+rcv.p(rcv.t(:,3),:))/3;

for k=1:size(src.t,1)
        if 0==mod(fix((k-1)/size(src.t,1)*1000),50)
            fprintf('.');
        end
        
        A=src.p(src.t(k,1),:);
        B=src.p(src.t(k,2),:);
        C=src.p(src.t(k,3),:);
        
        for i=1:3
            [s22,s23,s33]=unicycle.ps.greens.computeStressPlaneStrainTriangleMixedQuad( ...
                xc(:,1),xc(:,2),A,B,C,I(i,1),I(i,2),I(i,3),G,nu);
            
            out{1,i}(:,k)=s22(:);
            out{2,i}(:,k)=s23(:);
            out{3,i}(:,k)=s33(:);
        end
end

[L2222,L2223,L2233, ...
 L2322,L2323,L2333, ...
 L3322,L3323,L3333]=deal(out{:});

end