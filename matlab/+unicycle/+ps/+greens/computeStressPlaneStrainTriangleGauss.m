function [s22,s23,s33]=computeStressPlaneStrainTriangleGauss( ...
    x2,x3,A,B,C,e22,e23,e33,G,nu,varargin)
% function COMPUTESTRESSPLANESTRAINTRIANGLEGAUSS computes the
% stress field associated with deforming triangle volume element
% considering the following geometry using the Gauss-Legendre quadrature.
%
%              surface
%      -------------+-------------- E (x2)
%                   |
%                   |     + A
%                   |    /  . 
%                   |   /     .  
%                   |  /        .            
%                   | /           .      
%                   |/              + B
%                   /            .
%                  /|          /  
%                 / :       .
%                /  |    /
%               /   : .
%              /   /|
%             / .   :
%            +      |
%          C        :
%                   |
%                   D (x3)
%
%
% Input:
% x2, x3             east coordinates and depth of the observation point,
% A, B, C            east and depth coordinates of the vertices,
% eij                source strain component 22, 23 and 33 in the volume element,
% G, nu              shear modulus and Poisson's ratio in the half-space.
%
% Output:
% s22                uniaxial stress component in the east direction,
% s23                shear stress component,
% s33                uniaxial stress component in the vertical direction.
%
% Author: Sylvain Barbot (sbarbot@usc.edu) - April 7, 2018, Los Angeles.

assert(min(x3(:))>=0,'depth must be positive.');

% process optional input
p = inputParser;
p.addParameter('N',15,@validateN);
p.parse(varargin{:});
optionStruct = p.Results;

% Lame parameter
lambda=2*nu/(1-2*nu);

% isotropic strain
ekk=e22+e33;

% unit vectors
nA = [C(2)-B(2);
      B(1)-C(1)]/norm(C-B);
nB = [C(2)-A(2);
      A(1)-C(1)]/norm(C-A);
nC = [B(2)-A(2);
      A(1)-B(1)]/norm(B-A);
  
% check that unit vectors are pointing outward
if (nA'*(A(:)-(B(:)+C(:))/2))>0
    nA=-nA;
end
if (nB'*(B(:)-(A(:)+C(:))/2))>0
    nB=-nB;
end
if (nC'*(C(:)-(A(:)+B(:))/2))>0
    nC=-nC;
end

% parameterized line integral
y2=@(t,A,B) (A(1)+B(1))/2+t*(B(1)-A(1))/2;
y3=@(t,A,B) (A(2)+B(2))/2+t*(B(2)-A(2))/2;

% Radii
r1=@(y2,y3) sqrt((x2-y2).^2+(x3-y3).^2);
r2=@(y2,y3) sqrt((x2-y2).^2+(x3+y3).^2);

G22d2=@(y2,y3) -1/(2*pi*(1-nu))*( ...
     1/4*(3-4*nu).*(x2-y2)./r1(y2,y3).^2 ...
    +1/4*(8*nu.^2-12.*nu+5).*(x2-y2)./r2(y2,y3).^2 ...
    -1/2*(x3-y3).^2./r1(y2,y3).^4 .*(x2-y2) ...
    -1/2*((3-4*nu)*(x3+y3).^2+2*y3.*(x3+y3)-2*y3.^2)./r2(y2,y3).^4 .*(x2-y2) ...
    +4*y3.*x3.*(x3+y3).^2./r2(y2,y3).^6.*(x2-y2) ...
    );

G22d3=@(y2,y3) -1/(2*pi*(1-nu))*( ...
     1/4*(3-4*nu)*(x3-y3)./r1(y2,y3).^2 ...
    +1/4*(8*nu^2-12*nu+5).*(x3+y3)./r2(y2,y3).^2 ...
    +1/2*(x3-y3)./r1(y2,y3).^2 ...
    -1/2*(x3-y3).^2./r1(y2,y3).^4.*(x3-y3) ...
    +1/2*((3-4*nu)*(x3+y3)+y3)./r2(y2,y3).^2 ...
    -1/2*((3-4*nu).*(x3+y3).^2+2*y3.*(x3+y3)-2*y3.^2)./r2(y2,y3).^4.*(x3+y3) ...
    -y3.*(x3+y3).^2./r2(y2,y3).^4 ...
    -2*y3.*x3.*(x3+y3)./r2(y2,y3).^4 ...
    +4*y3.*x3.*(x3+y3).^2./r2(y2,y3).^6.*(x3+y3));

G23d2=@(y2,y3) 1/(2*pi*(1-nu))*( ...
    (1-2*nu).*(1-nu).*(x3+y3)./r2(y2,y3).^2 ...
    +1/4*(x3-y3)./r1(y2,y3).^2 ...
    -1/2*(x3-y3).*(x2-y2)./r1(y2,y3).^4 .*(x2-y2) ...
    +1/4*(3-4*nu).*(x3-y3)./r2(y2,y3).^2 ...
    -1/2*(3-4*nu).*(x3-y3).*(x2-y2)./r2(y2,y3).^4.*(x2-y2) ...
    -y3.*x3.*(x3+y3)./r2(y2,y3).^4 ...
    +2*y3.*x3.*(x2-y2).*(x3+y3)./r2(y2,y3).^6.*(2.*x2-2.*y2) ...
    );

G23d3=@(y2,y3) 1/(2*pi*(1-nu))*( ...
    -(1-2*nu).*(1-nu).*(x2-y2)./r2(y2,y3).^2 ...
    +1/4*(x2-y2)./r1(y2,y3).^2 ...
    -1/2*(x3-y3).*(x2-y2)./r1(y2,y3).^4.*(x3-y3) ...
    +1/4*(3-4*nu).*(x2-y2)./r2(y2,y3).^2 ...
    -1/2*(3-4*nu).*(x3-y3).*(x2-y2)./r2(y2,y3).^4.*(x3+y3) ...
    -y3.*(x2-y2).*(x3+y3)./r2(y2,y3).^4-y3.*x3.*(x2-y2)./r2(y2,y3).^4 ...
    +4*y3.*x3.*(x2-y2).*(x3+y3)./r2(y2,y3).^6.*(x3+y3) ...
    );

G32d2=@(y2,y3) 1/(2*pi*(1-nu))*( ...
    -(1-2*nu).*(1-nu).*(x3+y3)./r2(y2,y3).^2 ...
    +1/4*(x3-y3)./r1(y2,y3).^2 ...
    -1/2*(x3-y3).*(x2-y2)./r1(y2,y3).^4.*(x2-y2) ...
    +1/4*(3-4*nu).*(x3-y3)./r2(y2,y3).^2 ...
    -1/2*(3-4*nu).*(x3-y3).*(x2-y2)./r2(y2,y3).^4.*(x2-y2) ...
    +y3.*x3.*(x3+y3)./r2(y2,y3).^4 ...
    -4*y3.*x3.*(x2-y2).*(x3+y3)./r2(y2,y3).^6.*(x2-y2) ...
    );

G32d3=@(y2,y3) 1/(2*pi*(1-nu))*( ...
    (1-2*nu).*(1-nu).*(x2-y2)./r2(y2,y3).^2 ...
    +1/4*(x2-y2)./r1(y2,y3).^2 ...
    -1/2*(x3-y3).*(x2-y2)./r1(y2,y3).^4.*(x3-y3) ...
    +1/4*(3-4*nu).*(x2-y2)./r2(y2,y3).^2 ...
    -1/2*(3-4*nu).*(x3-y3).*(x2-y2)./r2(y2,y3).^4.*(x3+y3) ...
    +y3.*(x2-y2).*(x3+y3)./r2(y2,y3).^4 ...
    +y3.*x3.*(x2-y2)./r2(y2,y3).^4 ...
    -4.*y3.*x3.*(x2-y2).*(x3+y3)./r2(y2,y3).^6.*(x3+y3) ...
    );

G33d2=@(y2,y3) 1/(2*pi*(1-nu))*( ...
    -1/4*(3-4*nu).*(x2-y2)./r1(y2,y3).^2 ...
    -1/4*(8*nu^2-12*nu+5).*(x2-y2)./r2(y2,y3).^2 ...
    -1/2*(x2-y2)./r1(y2,y3).^2 ...
    +1/2*(x2-y2).^2./r1(y2,y3).^4.*(x2-y2) ...
    -1/2*(3-4*nu).*(x2-y2)./r2(y2,y3).^2 ...
    -1/2*(2*y2.*x3-(3-4.*nu).*(x2-y2).^2)./r2(y2,y3).^4.*(x2-y2) ...
    -2*y3.*x3.*(x2-y2)./r2(y2,y3).^4 ...
    +4*y3.*x3.*(x2-y2).^2./r2(y2,y3).^6.*(x2-y2) ...
    );

G33d3=@(y2,y3) 1/(2*pi*(1-nu))*( ...
    -1/4*(3-4.*nu).*(x3-y3)./r1(y2,y3).^2 ...
    -1/4*(8.*nu.^2-12.*nu+5).*(x3+y3)./r2(y2,y3).^2 ...
    +1/2*(x2-y2).^2./r1(y2,y3).^4.*(x3-y3) ...
    +1/2*y2./r2(y2,y3).^2 ...
    -1/2*(2.*y2.*x3-(3-4.*nu).*(x2-y2).^2)./r2(y2,y3).^4.*(x3+y3) ...
    -y3.*(x2-y2).^2./r2(y2,y3).^4 ...
    +4*y3.*x3.*(x2-y2).^2./r2(y2,y3).^6.*(x3+y3) ...
    );

% moment density (scaled by rigidity)
m22=lambda*ekk+2*e22;
m23=2*e23;
m33=lambda*ekk+2*e33;
 
% function IU2d2 is the integrand for displacement gradient component d u2 / dx2
IU2d2=@(t) ...
    (m22*nC(1)+m23*nC(2))*norm(B-A)/2*G22d2(y2(t,A,B),y3(t,A,B)) ...
   +(m23*nC(1)+m33*nC(2))*norm(B-A)/2*G32d2(y2(t,A,B),y3(t,A,B)) ...
   +(m22*nA(1)+m23*nA(2))*norm(C-B)/2*G22d2(y2(t,B,C),y3(t,B,C)) ...
   +(m23*nA(1)+m33*nA(2))*norm(C-B)/2*G32d2(y2(t,B,C),y3(t,B,C)) ...
   +(m22*nB(1)+m23*nB(2))*norm(A-C)/2*G22d2(y2(t,C,A),y3(t,C,A)) ...
   +(m23*nB(1)+m33*nB(2))*norm(A-C)/2*G32d2(y2(t,C,A),y3(t,C,A));

% function IU3d2 is the integrand for displacement gradient component d u3 / dx2
IU3d2=@(t) ...
    (m22*nC(1)+m23*nC(2))*norm(B-A)/2*G23d2(y2(t,A,B),y3(t,A,B)) ...
   +(m23*nC(1)+m33*nC(2))*norm(B-A)/2*G33d2(y2(t,A,B),y3(t,A,B)) ...
   +(m22*nA(1)+m23*nA(2))*norm(C-B)/2*G23d2(y2(t,B,C),y3(t,B,C)) ...
   +(m23*nA(1)+m33*nA(2))*norm(C-B)/2*G33d2(y2(t,B,C),y3(t,B,C)) ...
   +(m22*nB(1)+m23*nB(2))*norm(A-C)/2*G23d2(y2(t,C,A),y3(t,C,A)) ...
   +(m23*nB(1)+m33*nB(2))*norm(A-C)/2*G33d2(y2(t,C,A),y3(t,C,A));

% function IU2d3 is the integrand for displacement gradient component d u2 / dx3
IU2d3=@(t) ...
    (m22*nC(1)+m23*nC(2))*norm(B-A)/2*G22d3(y2(t,A,B),y3(t,A,B)) ...
   +(m23*nC(1)+m33*nC(2))*norm(B-A)/2*G32d3(y2(t,A,B),y3(t,A,B)) ...
   +(m22*nA(1)+m23*nA(2))*norm(C-B)/2*G22d3(y2(t,B,C),y3(t,B,C)) ...
   +(m23*nA(1)+m33*nA(2))*norm(C-B)/2*G32d3(y2(t,B,C),y3(t,B,C)) ...
   +(m22*nB(1)+m23*nB(2))*norm(A-C)/2*G22d3(y2(t,C,A),y3(t,C,A)) ...
   +(m23*nB(1)+m33*nB(2))*norm(A-C)/2*G32d3(y2(t,C,A),y3(t,C,A));

% function IU3d3 is the integrand for displacement gradient component d u2 / dx3
IU3d3=@(t) ...
    (m22*nC(1)+m23*nC(2))*norm(B-A)/2*G23d3(y2(t,A,B),y3(t,A,B)) ...
   +(m23*nC(1)+m33*nC(2))*norm(B-A)/2*G33d3(y2(t,A,B),y3(t,A,B)) ...
   +(m22*nA(1)+m23*nA(2))*norm(C-B)/2*G23d3(y2(t,B,C),y3(t,B,C)) ...
   +(m23*nA(1)+m33*nA(2))*norm(C-B)/2*G33d3(y2(t,B,C),y3(t,B,C)) ...
   +(m22*nB(1)+m23*nB(2))*norm(A-C)/2*G23d3(y2(t,C,A),y3(t,C,A)) ...
   +(m23*nB(1)+m33*nB(2))*norm(A-C)/2*G33d3(y2(t,C,A),y3(t,C,A));

% numerical solution with Gauss-Legendre quadrature
[sk,gk]=gaussxw(-1,1,optionStruct.N);

u2d2=zeros(size(x2));
u2d3=zeros(size(x2));
u3d2=zeros(size(x2));
u3d3=zeros(size(x2));

% numerical integration
for k=1:length(sk)
    u2d2=u2d2+gk(k)*IU2d2(sk(k));
    u2d3=u2d3+gk(k)*IU2d3(sk(k));
    u3d2=u3d2+gk(k)*IU3d2(sk(k));
    u3d3=u3d3+gk(k)*IU3d3(sk(k));
end

% remove anelastic strain
Omega=@(x2,x3) heaviside(((A(1)+B(1))/2-x2)*nC(1)+((A(2)+B(2))/2-x3)*nC(2)) ...
             .*heaviside(((B(1)+C(1))/2-x2)*nA(1)+((B(2)+C(2))/2-x3)*nA(2)) ...
             .*heaviside(((C(1)+A(1))/2-x2)*nB(1)+((C(2)+A(2))/2-x3)*nB(2));
         
% elastic strain components
e22=u2d2         -Omega(x2,x3)*e22;
e23=(u2d3+u3d2)/2-Omega(x2,x3)*e23;
e33=u3d3         -Omega(x2,x3)*e33; 
        
% stress components
s22=G*lambda*(e22+e33)+2*G*e22;
s23=2*G*e23;
s33=G*lambda*(e22+e33)+2*G*e33;

    function y=heaviside(x)
        y=x>0;
    end
end

function p = validateN(x)
if ~(x > 0)
    error('MATLAB:invalid','invalid number of integration points');
end
p = true;
end

function [xn,wn]=gaussxw(a,b,n)
% GAUSSXW finds sample points xn and weights wn for Gaussian
% quadrature on (a,b) with n nodes
%
%    function [xn wn]  = gaussxw(a,b, n)
%
%  Integral = wn(:)' * f (xn(:))
%  or  sum(wn.*f(xn))
%
%  Saves nodes and weights from previous call with same n.
%
% AUTHOR: Trefethen, Spectral Method in Matlab

persistent N x w

% check if we need new x, w
if (isempty(N) == 1 || N ~= n)
    N=n;
    beta=0.5./sqrt(1-(2*(1:N-1)).^(-2));
    T=diag(beta,1)+diag(beta,-1);
    [V,D]=eig(T);
    x=diag(D);
    [x,i]=sort(x);
    w=2*V(1,i).^2;
end

% finds sample x and weights for interval (a,b)
xn=(a+b+x(:)*(b-a))/2;
wn=(b-a)*w(:)/2;
end

function y=heaviside(x)
y=x>=0;
end
