function [s22,s23,s33]=computeStressPlaneStrainTriangleMixedQuad( ...
    x2,x3,A,B,C,e22,e23,e33,G,nu)
% function COMPUTEDISPLACEMENTPLANESTRAINTRIANGLEMIXEDQUAD computes
% the displacement field associated with deforming triangle volume element
% considering the following geometry using the double-exponential or
% Gauss-Legendre numerical quadrature based on the distance from the 
% circumcenter.
%
%              surface
%      -------------+-------------- E (x2)
%                   |
%                   |     + A
%                   |    /  . 
%                   |   /     .  
%                   |  /        .            
%                   | /           .      
%                   |/              + B
%                   /            .
%                  /|          /  
%                 / :       .
%                /  |    /
%               /   : .
%              /   /|
%             / .   :
%            +      |
%          C        :
%                   |
%                   D (x3)
%
%
% Input:
% x2, x3             east coordinates and depth of the observation point,
% A, B, C            east and depth coordinates of the vertices,
% eij                source strain component 22, 23 and 33 in the volume element,
% G, nu              shear modulus and Poisson's ratio in the half-space.
%
% Output:
% s22                uniaxial stress component in the east direction,
% s23                shear stress component,
% s33                uniaxial stress component in the vertical direction.
%
% Author: Sylvain Barbot (sbarbot@usc.edu) - April 7, 2018, Los Angeles.

assert(min(x3(:))>=0,'depth must be positive.');

% unit vectors
nA = [C(2)-B(2);
      B(1)-C(1)]/norm(C-B);
nB = [C(2)-A(2);
      A(1)-C(1)]/norm(C-A);
  
% check that unit vectors are pointing outward
if (nA'*(A(:)-(B(:)+C(:))/2))>0
    nA=-nA;
end
if (nB'*(B(:)-(A(:)+C(:))/2))>0
    nB=-nB;
end

% circumcenter of triangle
O=((B(:)+C(:))+nA(:)*(nB(2)*(B(1)-A(1))-nB(1)*(B(2)-A(2)))/(nA(2)*nB(1)-nA(1)*nB(2)))/2;

% circumcircle radius
r=norm(O(:)-A(:));

inside=sqrt((x2-O(1)).^2+(x3-O(2)).^2)<1.75*r;
outside=~inside;

% initiate empty array
s22=zeros(size(x2));
s23=zeros(size(x2));
s33=zeros(size(x2));

% numerical solution with Gauss-Legendre quadrature for points outside the circumcircle
if numel(x3(outside))>0
    [s22(outside),s23(outside),s33(outside)]=unicycle.ps.greens.computeStressPlaneStrainTriangleGauss( ...
        x2(outside),x3(outside),A,B,C,e22,e23,e33,G,nu);
end

% points inside the circumcircle
if numel(x3(inside))>0
    % numerical solution with double-exponential quadrature
    %[s22(inside),s23(inside),s33(inside)]=unicycle.ps.greens.computeStressPlaneStrainTriangleTanhSinh( ...
    %    x2(inside),x3(inside),A,B,C,e22,e23,e33,G,nu);
    % numerical solution with Gauss-Legendre quadrature
    [s22(inside),s23(inside),s33(inside)]=unicycle.ps.greens.computeStressPlaneStrainTriangleGauss( ...
        x2(inside),x3(inside),A,B,C,e22,e23,e33,G,nu,'N',450);
end

end
