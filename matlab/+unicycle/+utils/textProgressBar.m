classdef textProgressBar < handle
    properties
        % Initialization
        strCR;
        
        % length of percentage string (>5)
        strPercentageLength = 10;

        % total number of dots in a progress bar
        strDotsMaximum = 10;
    end
    methods
        %% % % % % % % % % % % % % % % % % %
        %                                  %
        %      c o n s t r u c t o r       %
        %                                  %
        % % % % % % % % % % % % % % % % % %%
        function o=textProgressBar(c)
            % creates a text progress bar.
            % STRING c initiates the title of the progress bar
            
            if isempty(o.strCR) && ~ischar(c)
                error('The text progress must be initialized with a string');
            elseif isempty(o.strCR) && ischar(c)
                % initialization
                fprintf('%s',c);
                o.strCR = -1;
            end
        end
        
        function progress(o,c)
	    % c is number between 0 and 100 indicating progress.

            % normal progress
            c = floor(c);
            percentageOut=[num2str(c) '%%'];
            percentageOut=[percentageOut repmat(' ',1,o.strPercentageLength-length(percentageOut)-1)];
            nDots=floor(c/100*o.strDotsMaximum);
            dotOut=['[' repmat('.',1,nDots) repmat(' ',1,o.strDotsMaximum-nDots) ']'];
            strOut=[percentageOut dotOut];
            
            if o.strCR == -1
                % no carriage return during first run
                fprintf(strOut);
            else
                % carriage return during all the other runs
                fprintf([o.strCR strOut]);
            end
            
            % update carriage return
            o.strCR = repmat('\b',1,length(strOut)-1);
        end
        
        function close(o)
            fprintf('\n');
        end
    end
end

