% Unicycle (Unified Cycle of Earthquakes) is a framework for modeling
% crustal deformation with moving faults and deforming volumes.
%
% Geometry
%   unicycle.geometry.coseismicPatch         - coseismic slip distribution
%   unicycle.geometry.rectangle              - fault patch
%   unicycle.geometry.passiveReceiver        - passive stressed faults (no motion)
%   unicycle.geometry.source                 - moving fault loading receiver faults
%   unicycle.geometry.rectangleReceiver      - receiver fault with slip evolution
%   unicycle.geometry.segment                - fault segment grouping patches
%   unicycle.geometry.triangle               - triangular fault patch
%   unicycle.geometry.triangleReceiver       - triangular receiver w/ slip evolution
%   unicycle.geometry.cuboid                 - cuboid element
%   unicycle.geometry.cuboidReceiver         - cuboid element with rheological parameters
%   unicycle.geometry.tetrahedron            - tetrahedron element
%   unicycle.geometry.tetrahedronReceiver    - tetrahedron element with rheological parameters
%   unicycle.geometry.polyhedron             - polyhedron element
%   unicycle.geometry.polyhedronReceiver     - polyhedron element with rheological parameters
%
% Green's function
%   unicycle.greens.computeDisplacementMeade07
%   unicycle.greens.computeDisplacementNikkhoo15
%   unicycle.greens.computeDisplacementOkada85
%   unicycle.greens.computeDisplacementOkada92
%   unicycle.greens.computeDisplacementPlaneStrainCuboid
%   unicycle.greens.computeDisplacementPlaneStrainTriangle
%   unicycle.greens.computeDisplacementPolyhedronMixedQuad
%   unicycle.greens.computeDisplacementTetrahedronGauss
%   unicycle.greens.computeDisplacementTetrahedronMixedQuad
%   unicycle.greens.computeDisplacementTetrahedronTanhSinh
%   unicycle.greens.computeDisplacementVerticalCuboid
%   unicycle.greens.computeDisplacementVerticalCuboidGauss
%   unicycle.greens.computeDisplacementVerticalCuboidMixedQuad
%   unicycle.greens.computeDisplacementVerticalCuboidSurfaceGauss
%   unicycle.greens.computeDisplacementVerticalCuboidSurfaceMixedQuad
%   unicycle.greens.computeDisplacementVerticalCuboidSurfaceTanhSinh
%   unicycle.greens.computeDisplacementVerticalCuboidTanhSinh
%   unicycle.greens.computeDisplacementVerticalSemiInfiniteCuboidGauss
%   unicycle.greens.computeDisplacementVerticalSemiInfiniteCuboidSurfaceGauss
%   unicycle.greens.computeDisplacementVerticalSemiInfiniteCuboidSurfaceTanhSinh
%   unicycle.greens.computeDisplacementVerticalSemiInfiniteCuboidTanhSinh
%   unicycle.greens.computeStressKernelsGimbutas12
%   unicycle.greens.computeStressKernelsMeade07
%   unicycle.greens.computeStressKernelsNikkhoo15
%   unicycle.greens.computeStressKernelsOkada92
%   unicycle.greens.computeStressKernelsPolyhedron
%   unicycle.greens.computeStressKernelsTetrahedron
%   unicycle.greens.computeStressKernelsVerticalCuboid
%   unicycle.greens.computeTractionKernelsNikkhoo15
%   unicycle.greens.computeTractionKernelsOkada92
%   unicycle.greens.computeTractionKernelsPolyhedron
%   unicycle.greens.computeTractionKernelsVerticalCuboid
%
%   hmmvp/unihmmvp                           - Green's functions with Hierarchical Matrices
%   
% Manifold
%   unicycle/manifold/gps                    - create a gps object
%
% Ordinary Differential Equation
%   unicycle.ode.evolution                   - models of fault slip evolution
%   unicycle.ode.rateandstate                - rate-and-state friction
%   unicycle.ode.rateandstatedamping         - rate-and-state friction w/ radiation damping
%   unicycle.ode.ratestrengthening           - rate-strengthening approximation
%   unicycle.ode.ratestrengthening_prestress - rate-strengthening friction w/ pre-stress
%
% Input/Output and formats
%   unicycle.export.exportflt_rfaults        - compatible w/ Relax, EDCMP, Gamra
%   unicycle.export.exportvtk_rfaults        - 3D visualization w/ Paraview
%   unicycle.export.exportxyz_rfaults        - GMT ASCII format
%   unicycle.export.grdread                  - read GMT GRD binary file
%   unicycle.export.grdwrite                 - write GMT GRD binary file
%
% Optimization
%   unicycle.optim.sim_anl                   - simulated annealing
%   unicycle.optim.mh                        - Metropolis Hastings
%   unicycle.optim.computeLaplacian          - smoothing matrix operator
%
% AUTHOR: Sylvain Barbot, May 24, 2013-2020
%
% SEE ALSO: unicycle
