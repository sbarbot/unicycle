% Wang, L. and Barbot, S., 2020. Excitation of San Andreas 
% tremors by thermal instabilities below the seismogenic zone. 
% Science advances, 6(36), p.eabb2057.

function[]=OrbitCycle
    clear variables; clc;
    
    p=Par0; p0=p;
    PlotPar='Fig2'; %'Period';
    figure(1);clf
    switch PlotPar
        case 'Fig2'   % mu~V/T; period
            plotFaultSketch
            PlotMu_V8T
            PlotPeriod
            plotCycle
            print('-bestfit','tmp','-dpdf','-r400')
        case 'Period'
            PlotPeriod
            print('-bestfit','PhaseDiagram','-dpdf','-r350');
    end
            
    function[p]=Par0
        p.K=273.15; p.Tb=400+p.K;
        p.a=0.0102; p.b=0.006;  p.sigma=19.92; p.Vo=1e-6; p.L=0.0031; 
        p.Vl=1e-9; p.alpha=5.0; p.R=8.33; p.D=7.4345e-7; p.rhoc=2.6;
        p.mu0=0.5449; p.mu0=0.54;
        p.Q=9.0028e+04; p.H=5.9755e+04;
        p.w=1.3718e-04; p.W=1.6976;
        p.To=p.Tb+(p.mu0*p.sigma/p.w*p.Vl/p.rhoc)/p.D*p.W^2;
        p.k=43.978; 
        
    end

    function[]=PlotMu_V8T
        p0=Par0;
        p=p0;
        T=p.To;
        ks=-3:.1:5; ks=10.^ks;
        Vs=p.Vo*ks;
        mu_V=p.mu0+(p.a-p.b)*log10(Vs/p.Vo)+(p.a*p.Q-p.b*p.H)/p.R*(1./T-1/p.To);
        V=p.Vo; 
        Ts=p.Tb:1:1200;
        mu_T=p.mu0+(p.a-p.b)*log10(V/p.Vo)+(p.a*p.Q-p.b*p.H)/p.R*(1./Ts-1/p.To);
        
        axes('position',[.54 .7 .3 .3]); set(gca,'fontsize',11)
        yyaxis left;
        set(gca,'fontsize',11);
        loglog(mu_V,Vs,'linewidth',1,'color','k'); hold on
        ylabel('Velocity (m/s)')
        set(gca,'YColor','k')
        xlabel('\mu');grid
        loglog([p.mu0 p.mu0],[1e-9 1e-1],'--k','linewidth',1.1)
        text(p.mu0-.005,3e-9,'\mu_0')
        text(0.553,3e-2,'(b)')
        ylim([1e-9 1e-1])
        yticks(10.^(-9:2:-1));
        yticklabels({'10^-^9','10^-^7','10^-^5','10^-^3','10^-^1'})

        yyaxis right;
        loglog(mu_T,Ts-p.K,'linewidth',1.3,'color','r'); hold on
        ylabel('Temperature (^oC)');
        set(gca,'YColor','r')
    end
        

    function[]=PlotPeriod
        p=p0;
        [t,Y]=Simulate(p); %ix=1:length(t);
        ix=t>1900*3.15e+7;
        Vix=log10(p.Vo*exp(Y(ix,4)));
        Tau_ix=Y(ix,2);
        Tix=Y(ix,5)-273.15;
        theta_ix=Y(ix,3);
        % steady state temperature
        To=p.Tb+(p.mu0*p.sigma/p.w*p.Vl/p.rhoc)/p.D*p.W^2;
        % steady state velocity
        A1=(p.a*p.Q-p.b*p.Q)/p.R*(1./Y(ix,5)-1/To);
        Vss=p.Vo*exp((Y(ix,2)/p.sigma-p.mu0)/(p.a-p.b));
        
        myoption=5;
        switch myoption
            case 4
                axes('position',[.5 .3 .3 .3])
                yyaxis right;set(gca,'YColor',[.0 .0 .0]);set(gca,'fontsize',11)
                plot(Y(ix,2),Y(ix,5)-273.15,'linewidth',1.1,'color',[0 0 0]); hold on
                plot([8 9.5],[To-273.15 To-273.15],'--','linewidth',1.3,'color',[.5 .5 .5])
                xlabel('Stress (MPa)'); grid
                arrow([Tau_ix(300),Tix(300)],[Tau_ix(301) Tix(301)],'facecolor',[0 0 0],'edgecolor',[.0 .0 .0],'baseangle',20)
                arrow([Tau_ix(30),Tix(30)],[Tau_ix(31) Tix(31)],'facecolor',[0 0 0],'edgecolor',[.0 .0 .0],'baseangle',20)
                text(9.35,965,'(d)')
                yticks([400 600 800 1000]);yticklabels({'400','600','800','1000'})
                ylabel('T (^oC)');
                yyaxis left;set(gca,'YColor',[.0 .0 .0]);
                plot(Y(ix,2),Y(ix,5)-273.15,'linewidth',1.1,'color',[0 0 0]); grid;grid
                yticks([400 600 800 1000]); yticklabels({'','','',''})
                
                axes('position',[.08 .3 .3 .3])
                yyaxis left;
                set(gca,'fontsize',11);
                
                semilogy(Y(ix,5)-273.15,(p.Vo*exp(Y(ix,4))),'linewidth',1.1,'color','k'); hold on
                patch([400 1000 1000 400],10.^([-11 -11 -9 -9]),[.95 .95 .95]); hold on
                semilogy(Y(ix,5)-273.15,(p.Vo*exp(Y(ix,4))),'linewidth',1.1,'color','k');
                %arrow([Tix(90),Vix(90)],[Tix(91) Vix(91)],'facecolor',[.5 .5 .5],'edgecolor',[.0 .0 .0],'baseangle',20)
                grid;grid
                plot([To-273.15 To-273.15],10.^([-11 -7]),'--','linewidth',1.3,'color',[.5 .5 .5])
                ylabel('V (m/s)');
                set(gca,'YColor','k')
                xlabel('T (^oC)');grid;
                ylim([1e-11 1e-7]);
                yticks([1e-11 1e-10 1e-9 1e-8 1e-7]);
                yticklabels({'10^-^1^1','10^-^1^0','10^-^9','10^-^8','10^-^7'})
                text(950,6e-8,'(c)')

                arrow([Tix(53),10^(Vix(53))],[Tix(56) 10^(Vix(56))],'facecolor',[.5 .5 .5],'edgecolor',[.0 .0 .0],'baseangle',20,'length',15)
                yyaxis right;
                
                semilogy(Y(ix,5)-273.15,theta_ix,'linewidth',1.1,'color',[.5 .5 .5]); hold on
                arrow([Tix(90),theta_ix(90)],[Tix(91) theta_ix(91)],'facecolor',[.5 .5 .5],'edgecolor',[.5 .5 .5],'baseangle',20,'length',15)
                %arrow([Tix(57),theta_ix(57)],[Tix(58) theta_ix(58)],'facecolor',[.5 .5 .5],'edgecolor',[.0 .0 .0],'baseangle',20)
                ylabel('State variable');
                set(gca,'YColor',[.5 .5 .5])
                xlim([400 1000]);ylim([5 10])
                %print('-bestfit','Period4','-dpdf','-r350');
            case 5
                T=Y(ix,5)-273.15;
                V=p.Vo*exp(Y(ix,4));
                S=Y(ix,2);
                th=theta_ix;
                Tmin=min(T);
                IX=1:length(T);
                if false
                close all;plot(T)
                for i=1:length(T)
                    text(i,T(i),num2str(i))
                end
                end
                
                ix_1cycle=49:110;
                T=T(ix_1cycle);
                V=V(ix_1cycle);
                S=S(ix_1cycle);
                th=th(ix_1cycle);
                
                T_Vmin=T(V==min(V));
                T_Vmax=T(V==max(V));
                V_Tmin=V(T==min(T));
                V_Tmax=V(T==max(T));
                T_th_min=T(th==min(th));
                T_Smax=T(S==max(S));
                T_Smin=T(S==min(S));
                th_Tmax=th(T==max(T));
                
                %% for manually determine the index of different phases
                if false
                    figure
                    yyaxis left;
                    semilogy(T,V,'linewidth',1.1,'color','k'); hold on
                    for i=1:length(T)
                        text(T(i),V(i),num2str(i));
                    end
                    semilogy(T_Vmin,min(V),'+r','markersize',10)
                    semilogy(T_Vmax,max(V),'+r','markersize',10)
                    semilogy(min(T),V_Tmin, '+r','markersize',10)
                    semilogy(max(T),V_Tmax, '+r','markersize',10)
                    yyaxis right
                    plot(T,S); hold on
                    for i=1:length(T)
                        text(T(i),S(i),num2str(i))
                    end
                    plot(T_Smax,max(S),'+r','markersize',10)
                    plot(T_Smin,min(S),'+r','markersize',10)                
                    figure
                end
                
                %% these numbers were manually determined using the code above
                ix0=20:28;
                ix1=28:35;
                ix2=35:49;
                ix3=49:61;
                ix4=1:6;
                ix5=6:20;
                
                axes('position',[.08 .3 .3 .3])
                yyaxis left;
                set(gca,'fontsize',11);
                semilogy(Y(ix,5)-273.15,(p.Vo*exp(Y(ix,4))),'linewidth',2.3,'color','k'); hold on
                patch([370 1020 1020 370],10.^([-11 -11 -9 -9]),[.95 .95 .95]); hold on
                semilogy(Y(ix,5)-273.15,(p.Vo*exp(Y(ix,4))),'linewidth',2.3,'color','k');
                grid;grid
                
                ylabel('Velocity (m/s)');
                set(gca,'YColor','k')
                xlabel('T (^oC)');grid;
                ylim([1e-11 1e-7]);
                yticks([1e-11 1e-10 1e-9 1e-8 1e-7]);
                yticklabels({'10^-^1^1','10^-^1^0','10^-^9','10^-^8','10^-^7'})
                arrow([Tix(53),10^(Vix(53))],[Tix(56) 10^(Vix(56))],'facecolor',[.5 .5 .5],'edgecolor',[.0 .0 .0],'baseangle',20,'length',15)
       
                plot(T_Vmin, min(V),'^','color',[.0 .0 .0],'markersize',9)
                plot(T_Vmax, max(V),'v','color',[.0 .0 .0],'markersize',9)
                
                semilogy(T(ix0),V(ix0),'--','linewidth',1,'color','c')
                semilogy(T(ix1),V(ix1),'--','linewidth',1,'color','y')
                semilogy(T(ix2),V(ix2),'--','linewidth',1,'color','r')
                semilogy(T(ix3),V(ix3),'--','linewidth',1,'color','g')
                semilogy(T(ix4),V(ix4),'--','linewidth',1,'color','m')
                semilogy(T(ix5),V(ix5),'--','linewidth',1,'color','w')
                
                text(378, 2e-11, '$\textcircled{1}$', 'Interpreter', 'latex','color','k')
                text(377, 2e-10, '$\textcircled{2}$', 'Interpreter', 'latex','color','k')
                text(600, 3e-8, '$\textcircled{3}$', 'Interpreter', 'latex','color','k')
                text(900, 1.8e-8, '$\textcircled{4}$', 'Interpreter', 'latex','color','k')
                text(900, 2.5e-9, '$\textcircled{5}$', 'Interpreter', 'latex','color','k')
                text(700, 2.7e-10, '$\textcircled{6}$', 'Interpreter', 'latex','color','k')
                
                xlim([370 1020])
                yyaxis right;set(gca,'YColor',[.5 .5 .5]);set(gca,'fontsize',11)
                plot(Y(ix,5)-273.15,Y(ix,2),'linewidth',2.3,'color',[.5 .5 .5]); hold on
                
                ylabel('Shear stress (MPa)'); 
                arrow([Tix(25) Tau_ix(25) ],[Tix(33) Tau_ix(33)],'facecolor',[.5 .5 .5],'edgecolor',[.5 .5 .5],'baseangle',20)
                text(950,9.4,'(c)')
                xticks([400 600 800 1000]);xticklabels({'400','600','800','1000'})
                xlabel('Temperature (^oC)');
                
                plot(T(ix0),S(ix0),'--','linewidth',1,'color','c')
                plot(T(ix1),S(ix1),'--','linewidth',1,'color','y')
                plot(T(ix2),S(ix2),'--','linewidth',1,'color','r')
                plot(T(ix3),S(ix3),'--','linewidth',1,'color','g')
                plot(T(ix4),S(ix4),'--','linewidth',1,'color','m')
                plot(T(ix5),S(ix5),'--','linewidth',1,'color','w')
                plot(T_Smax, max(S),'v','color',[.5 .5 .5],'markersize',9)
                plot(T_Smin,min(S),'^','color',[.5 .5 .5],'markersize',9)
                
                axes('position',[.54 .3 .3 .3])
                yyaxis right;
                set(gca,'fontsize',11);
                semilogy(Y(ix,5)-273.15,(p.Vo*exp(Y(ix,4))),'linewidth',.9,'color',[.5 .5 .5]); hold on
                %patch([370 1020 1020 370],10.^([-11 -11 -9 -9]),[.95 .95 .95]); hold on
                semilogy(Y(ix,5)-273.15,(p.Vo*exp(Y(ix,4))),'linewidth',.9,'color',[.5 .5 .5]);
                %arrow([Tix(90),Vix(90)],[Tix(91) Vix(91)],'facecolor',[.5 .5 .5],'edgecolor',[.0 .0 .0],'baseangle',20)
                grid;grid
                %plot([To-273.15 To-273.15],10.^([-11 -7]),'--','linewidth',1.3,'color',[.5 .5 .5])
                ylabel('Velocity (m/s)','color',[.5 .5 .5]);
                set(gca,'YColor',[.5 .5 .5])
                
                ylim([1e-11 1e-7]);
                yticks([1e-11 1e-10 1e-9 1e-8 1e-7]);
                yticklabels({'10^-^1^1','10^-^1^0','10^-^9','10^-^8','10^-^7'})
                text(920,6e-8,'(d)');xlabel('Temperature (^oC)');grid;
                arrow([Tix(53),10^(Vix(53))],[Tix(56) 10^(Vix(56))],'facecolor',[.5 .5 .5],'edgecolor',[.5 .5 .5],'baseangle',20,'length',15)

                semilogy([max(T)+6 max(T)+6],[1e-7 1e-11],'-.','linewidth',1.1,'color',[0 0 0])
                
                yyaxis left;
                semilogy(Y(ix,5)-273.15,theta_ix,'linewidth',2.3,'color',[.0 .0 .0]); hold on
                arrow([Tix(90),theta_ix(90)],[Tix(91) theta_ix(91)],'facecolor',[.0 .0 .0],'edgecolor',[.0 .0 .0],'baseangle',20,'length',15)
                %arrow([Tix(57),theta_ix(57)],[Tix(58) theta_ix(58)],'facecolor',[.5 .5 .5],'edgecolor',[.0 .0 .0],'baseangle',20)
                ylabel('Age of contact (s)');
                yticks([5 6 7 8 9 10]);
                yticklabels({'10^5','10^6','10^7','10^8','10^9','10^1^0'})
                set(gca,'YColor',[.0 .0 .0])
                xlim([370 1020]);ylim([5 10])
                
                plot(T_th_min,min(th),'^','color',[.0 .0 .0],'markersize',9)
                
                plot(T(ix0),th(ix0),'--','linewidth',1,'color','c')
                plot(T(ix1),th(ix1),'--','linewidth',1,'color','y')
                plot(T(ix2),th(ix2),'--','linewidth',1,'color','r')
                plot(T(ix3),th(ix3),'--','linewidth',1,'color','g')
                plot(T(ix4),th(ix4),'--','linewidth',1,'color','m')
                plot(T(ix5),th(ix5),'--','linewidth',1,'color','w')
                
                text(405, 9.75, '$\textcircled{1}$', 'Interpreter', 'latex','color','k')
                text(385, 9.1, '$\textcircled{2}$', 'Interpreter', 'latex','color','k')
                text(510, 7.5, '$\textcircled{3}$', 'Interpreter', 'latex','color','k')
                text(900, 5.8, '$\textcircled{4}$', 'Interpreter', 'latex','color','k')
                text(900, 8.4, '$\textcircled{5}$', 'Interpreter', 'latex','color','k')
                text(800, 8.9, '$\textcircled{6}$', 'Interpreter', 'latex','color','k')
                plot([370 1020],[6.9078 6.9078],'-.k','linewidth',1.1)
        end
    end

    function[]=plotCycle
         p=p0;
        [t,Y]=Simulate(p); 
        %ix=1:length(t);
        ix=t>1900*3.15e+7;
        tt=t/3.15e7;
              
        T=Y(ix,5)-273.15;
        V=p.Vo*exp(Y(ix,4));
        S=Y(ix,2);
        th=Y(ix,3);
        tt=tt(ix); 
       
        ix_cycle=5196:5385;
        T=T(ix_cycle);
        V=V(ix_cycle);
        S=S(ix_cycle);
        th=th(ix_cycle);
        tt=tt(ix_cycle); tt=tt-tt(1);
      
        %% these numbers were manually determined based on the runs at the end of this program 
        ix0=63:72;
        ix1=72:79;
        ix2=79:93;
        ix3=93:105;
        ix4=105:110;
        ix5=110:124;
        
        myoption=1;
        switch myoption
            case 1
                axes('position',[.08 .11 .76 .09]); set(gca,'fontsize',11);
                yyaxis left
                semilogy(tt,V,'-k','linewidth',2.0); hold on;grid
                patch([0 3.6 3.6 0],10.^([-11 -11 -9 -9]),[.95 .95 .95]); 
                semilogy(tt,V,'-k','linewidth',2.0); 
                
                set(gca,'YColor',[0 0 0]);ylabel('V (m/s)')
                ylim([1e-11 8e-8]);
                semilogy(tt(ix0),V(ix0),'--','linewidth',1,'color','c')
                semilogy(tt(ix1),V(ix1),'--','linewidth',1,'color','y')
                semilogy(tt(ix2),V(ix2),'--','linewidth',1,'color','r')
                semilogy(tt(ix3),V(ix3),'--','linewidth',1,'color','g')
                semilogy(tt(ix4),V(ix4),'--','linewidth',1,'color','m')
                semilogy(tt(ix5),V(ix5),'--','linewidth',1,'color','w')
                yticks([1e-10 1e-9 1e-8]);
                yticklabels({'10^-^1^0','10^-^9','10^-^8'})
                text(3.5,1e-8,'(e)')

                yyaxis right
                plot(tt,T,'color',[.5 .5 .5],'linewidth',2.0); hold on
                set(gca,'YColor',[.5 .5 .5]);
                ylabel('T (^oC)')
                yticks([500 800]);yticklabels({'500','800'})
                xticks((0:.5:3.5))
                xticklabels({'', '','','','','','',''})
                plot(tt(ix0),T(ix0),'--','linewidth',1,'color','c')
                plot(tt(ix1),T(ix1),'--','linewidth',1,'color','y')
                plot(tt(ix2),T(ix2),'--','linewidth',1,'color','r')
                plot(tt(ix3),T(ix3),'--','linewidth',1,'color','g')
                plot(tt(ix4),T(ix4),'--','linewidth',1,'color','m')
                plot(tt(ix5),T(ix5),'--','linewidth',1,'color','w')
                ylim([350 1000])
                xlim([0 3.6])
                
                text(405, 9.75, '$\textcircled{1}$', 'Interpreter', 'latex','color','k')
                text(385, 9.1, '$\textcircled{2}$', 'Interpreter', 'latex','color','k')
                text(510, 7.5, '$\textcircled{3}$', 'Interpreter', 'latex','color','k')
                text(900, 5.8, '$\textcircled{4}$', 'Interpreter', 'latex','color','k')
                text(900, 8.4, '$\textcircled{5}$', 'Interpreter', 'latex','color','k')
                text(800, 8.9, '$\textcircled{6}$', 'Interpreter', 'latex','color','k')
                
                axes('position',[.08 .01 .76 .09]); set(gca,'fontsize',11);
                yyaxis right
                plot(tt,S,'color',[.5 .5 .5],'linewidth',2.0); hold on
                set(gca,'YColor',[.5 .5 .5]); 
                ylabel('\tau (MPa)')
                xticks((0:.5:3.5))
                xticklabels({'0', '.5','1.0','1.5','2.0','2.5','3.0','3.5'})
                plot(tt(ix0),S(ix0),'--','linewidth',1,'color','c')
                plot(tt(ix1),S(ix1),'--','linewidth',1,'color','y')
                plot(tt(ix2),S(ix2),'--','linewidth',1,'color','r')
                plot(tt(ix3),S(ix3),'--','linewidth',1,'color','g')
                plot(tt(ix4),S(ix4),'--','linewidth',1,'color','m')
                plot(tt(ix5),S(ix5),'--','linewidth',1,'color','w')
                
                yyaxis left
                plot(tt,th,'color',[0 0 0],'linewidth',2.0); hold on
                set(gca,'YColor',[0 0 0]);grid;ylabel('\theta (s)')
                yticks([5 7 9]);
                yticklabels({'10^5','10^7','10^9'})
                plot(tt(ix0),th(ix0),'--','linewidth',1,'color','c')
                plot(tt(ix1),th(ix1),'--','linewidth',1,'color','y')
                plot(tt(ix2),th(ix2),'--','linewidth',1,'color','r')
                plot(tt(ix3),th(ix3),'--','linewidth',1,'color','g')
                plot(tt(ix4),th(ix4),'--','linewidth',1,'color','m')
                plot(tt(ix5),th(ix5),'--','linewidth',1,'color','w')
                text(3.5,8.2,'(f)')
                xlim([0 3.6])
                xlabel('Time (year)')
        end
        
        %%  ! !!!! for manually determine the index of different phase
        if false   
            tt1=tt(56:136);
            V1=V(56:136);
            S1=S(56:136);
            th1=th(56:136);
            T1=T(56:110);
            figure
            subplot(4,1,1); plot(tt,V); hold on; 
            plot(tt1(V1==max(V1)),max(V1),'+r','markersize',15);
            plot(tt1(V1==min(V1)),min(V1),'+r','markersize',15);
            for i=1:length(tt)
                text(tt(i),V(i),num2str(i))
            end
            title('V')
            subplot(4,1,2); plot(tt,T); hold on; 
            plot(tt1(T1==max(T1)),max(T1),'+r','markersize',15);
            plot(tt1(T1==min(T1)),min(T1),'+r','markersize',15);
            for i=1:length(tt)
                text(tt(i),T(i),num2str(i))
            end
            title(T)
            
            subplot(4,1,3); plot(tt,th); hold on; 
            plot(tt1(th1==max(th1)),max(th1),'+r','markersize',15);
            plot(tt1(th1==min(th1)),min(th1),'+r','markersize',15);
            for i=1:length(tt)
                text(tt(i),th(i),num2str(i))
            end
            title('\theta')
            
           subplot(4,1,4); plot(tt,S); hold on; 
            plot(tt1(S1==max(S1)),max(S1),'+r','markersize',15);
            plot(tt1(S1==min(S1)),min(S1),'+r','markersize',15);
            for i=1:length(tt)
                text(tt(i),S(i),num2str(i))
            end
            title('\tau')
        
        end
        
    end

    function[]=plotFaultSketch
        % 1. fault zone schetch
        axes('position',[.08 .7 .3 .3]);set(gca,'fontsize',11)
        
        xscal=5;
        yscal=2;
        x=-1:.01:1;
        a=cos(2*x);
        
        plot(x,cos(2*x)-min(a),'-k','linewidth',1.1); hold on;
        plot([-xscal xscal]/1.1, [0 0],'-k','linewidth',1.2);
        ay=-.2:-.01:-yscal; 
        b=(ay-min(ay))/(max(ay)-min(ay))*2*pi;
        bx=sin(b)*(-.2+yscal)-.2;
        patch([-xscal-bx/8 xscal-flip(bx)/8]/1.5,[ay flip(ay)], [.95 .95 .95],'linestyle','none')
        %patch([-xscal -xscal xscal xscal]/1.5,[-yscal -.2 -.2 -yscal],);
        patch([-1 -1 1 1],[-yscal -.2 -.2 -yscal],[.8 .8 .8],'linestyle','none');
        patch([-.1 -.1 .1 .1],[-yscal -.2 -.2 -yscal],[.5 .5 .5]);
        plot([0 0],[max(a)-min(a) 0],'--k')
        plot([0 0],[-.2 -yscal],'--k')
        plot([0 0],[0 max(a)-min(a)+.7],'-k','linewidth',1.1)
        arrow([0,max(a)-min(a)+.7],[0 max(a)-min(a)+.8],'facecolor',[1 1 1 ],'edgecolor',[1 1 1],'baseangle',30,'linewidth',1.1)
        %quiver([0],[max(a)-min(a)+.7],0,.2,0,'ShowArrowHead','on')
        xlim([-xscal xscal]*1.5)
        axis off
        text(.4, max(a)-min(a)+.5,'T','fontsize',13)
        text(-xscal/1.1-1.5, 0,'T_b','fontsize',13);
        text(xscal/1.1+.1, 0,'T_b','fontsize',13);
        text(-xscal/1.8,-yscal/2,'T_b','fontsize',13)
        text(xscal/2.5,-yscal/2,'T_b','fontsize',13)
        text(-.1,-yscal-.2,'w','fontsize',13);
        text(-.2,-yscal-.8,'W','fontsize',13)
        text(xscal*1.2, max(a)-min(a)+1,'(a)')
    end

    function yp=ode_thermal_1(t,y,p)
        % function ODE_THERMAL_1
        %
        %   |     s      |
        %   |    tau     |
        % y=| log(theta) |
        %   |   log(v)   |
        %   |     T      |
        %
        
        % stress (MPa)
        
        tau=y(2);
        
        % state  (1)
        psi=y(3);
        
        % velocity (m/s)
        V=p.Vo*exp(y(4));
        
        % temperature (K)
        T=y(5);
        
        % d psi / dt
        dpsi=(p.Vo*exp(-p.H/p.R*(1/T-1/p.To)-psi)-V)/p.L;
        
        % d T / dt
        dT=-p.D/p.W^2*(T-p.Tb)+tau*V/p.w/p.rhoc;
        
        % initiate state derivative
        yp=zeros(size(y));
        
        % V = d s / dt
        yp(1)=V;
        
        % d psi / dt
        yp(3)=dpsi;
        
        % d V / dt / V
        yp(4)=(-p.k*(V-p.Vl)-p.b*p.sigma*dpsi+p.a*p.sigma*p.Q/p.R*dT/T^2)/(p.a*p.sigma+p.alpha*V);
        
        % d tau / dt
        yp(2)=-p.k*(V-p.Vl)-p.alpha*V*yp(4);
        
        % d T / dt
        yp(5)=dT;
        
    end

    function[t,Y]=Simulate(p)            % years: simulation year
        ode=@(t,y) ode_thermal_1(t,y,p);
        y0=[0,p.sigma*(p.mu0+(p.a-p.b)*log(p.Vl/p.Vo)),log(p.L/p.Vl),log(p.Vl/p.Vo),p.Tb]';
        options=odeset('Refine',1,'RelTol',3e-7,'InitialStep',1e-3,'MaxStep',3e6);
        [t,Y]=ode45(ode,[0 2000*3.15e7],y0,options);
    end


    function[out]=OutPar(t,Y,p)
        % 'Thermal properties' 
        ThermPar1=p.a*p.Q-p.b*p.H;                %aQ-bH
        ThermPar2=(p.b/p.a*p.H-p.Q)/1e3;     %b/a H-Q
        
        % 'Steady-state temperature'  
        Tss=p.Tb+(p.mu0*p.sigma/p.w*p.Vl/p.rhoc)/p.D*p.W^2;
        
        Vts=Y(t>=(t(end)-100*3.15e7),4);
        Vts_max=max(Vts);
        Tauts=Y(t>=(t(end)-100*3.15e7),2);
        Tau_max=max(Tauts);
        Tts=Y(t>=(t(end)-100*3.15e7),5);         % temperature  serier
        Tts_max=max(Tts); Tts_min=min(Tts);
        
        out=[ThermPar1, ThermPar2, Tss, Tts_max, Tts_min Vts_max Tau_max];
    end
   
   
end
