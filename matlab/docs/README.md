
# DESCRIPTION

This directory contains Matlab scripts to simulate seismic cycles in a spring-slider system or on a semi-infinite fault embedded in a two-dimensional half-space in anti-plane strain. The simulations illustrate the use of the boundary-integral method with the radiation damping approximation. The scripts are for educational purposes.

  * `antiplane_deformation.m` compute the displacement and stress around a semi-infinite strike-slip fault embedded in a two-dimensional half-space.
  * `spring_slider.m` simulates seismic cycles for a spring-slider system with a rate- and state-dependent friction law with the radiation damping approximation
  * `boundary_integral_antiplane.m` simulates seismic cycles on a strike-slip fault embedded in a two-dimensional half-space with the additive form of a rate- and state-dependent friction law and the radiation damping approximation.
  * `boundary_integral_antiplane_multiplicative.m` simulates seismic cycles on a strike-slip fault embedded in a two-dimensional half-space with the multiplicative form of a rate- and state-dependent friction law and the radiation damping approximation.

Additional scripts illustrate the use of Green's functions for the inversion of geodetic data.

  * `antiplane_inversion.m` decribes several strategies to invert geodetic data for the distribution of slip and semi-infinite strike-slip fault embedded in a two-dimensional half-space. 
  * `antiplane_inversion_mh.m` performs a Bayesian inversion of fault slip distribution using the Metropolis-Hasting sampling algorithm
  * `mh.m` Metropolis-Hasting sampling algorithm

Additional scripts illustrate the behavior of velocity-strengthening, temperature-weakening faults

  * `OrbitCycle.m` simulate slow-slip events as thermal instabilities for a spring-slider system with the membrane diffusion approximation
  * `arrow.m` plots arrows in Matlab
