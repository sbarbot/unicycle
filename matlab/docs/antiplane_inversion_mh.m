
%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%
% We use the following coordinate system for the 
% description of the geometry.
%
%                 N x1 (no deformation in this direction)
%                /
%               / 
%   y1,y2,y3 ->@--------------  x2 (Matlab x)
%              |
%              :
%              | 
%              :
%              Z x3 (Matlab y)
%
% Fault slip occurs along the x3 direction. Positive
% displacement is pointing in the x1 direction.
%
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

clear variables

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%            P H Y S I C A L   M O D E L               %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

u1h=@(x2,x3,y2,y3,W) ...
    (-atan2((x2-y2),(x3-y3))+atan2((x2-y2),(x3+y3))...
     +atan2((x2-y2),(x3-y3-W))-atan2((x2-y2),(x3+y3+W)) ...
    )/2/pi;

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%            S Y N T H E T I C   D A T A               %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

% locking depth
L=1;

N=128;
dx2=0.25;
x2=(-N/2:N/2-1)'*dx2;

rng('default');
sigmad=0.01;
d=u1h(x2,0,0,0,L)+2*(rand(N,1)-0.5)*sigmad;

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%          G R E E N ' S   F U N C T I O N S           %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

u1h=@(x2,x3,y2,y3,W) ...
    (-atan2((x2-y2),(x3-y3))+atan2((x2-y2),(x3+y3))...
     +atan2((x2-y2),(x3-y3-W))-atan2((x2-y2),(x3+y3+W)) ...
    )/2/pi;

M=10;
dz=0.4;
y3=(0:M-1)'*dz;
W=ones(M,1)*dz;

% forward model
G=zeros(N,M);
for k=1:M
    G(:,k)=u1h(x2,0,0,y3(k),W(k));
end

m1=ones(M,1);
m1(y3>=L)=0;


%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%       S M O O T H I N G   C O N S T R A I N T S      %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

% smoothing constraints
L=diag(ones(M-1,1),1)-diag(ones(M,1),0);
L(M,:)=0;

% smoothing weight
lambda=1e-3; 

% best fit
H=[G;lambda*L]; % combine Green functions
h=[d;zeros(M,1)]; % combine data and smoothing constraint


%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%                  I N V E R S I O N                   %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

% least-squares inversion with positivity constraint
m2=lsqnonneg(G,d);

% least-squares inversion with smoothing and positivity constraint
m3=lsqnonneg(H,h);

% least-squares inversion, no smoothing
m4=(G.'*G)\(G.'*d);

% least-squares inversion, smoothing
m5=(H.'*H)\(H.'*h);

% pseudo-inverse 
tolerance=1e-1;
m6=pinv(G,tolerance)*d;

% least-squares inversion, no smoothing, fewest possible non-zero
% components
m7=H\h;

% pseudo-inverse with smoothing
%tolerance=1e-1;
m8=pinv(H,tolerance)*h;

% sparse Basis pursuit denoising solution
sigma=0.09; % ||Ax - b||_2 < sigma
m9=spgl1.bpdn(G,d,sigma);

% sparse Lasso solution
tau=5; % || m ||_1 < tau
m10=spgl1.lasso(G,d,tau);

% Metropolis-Hasting algorithm
params.burnin=1e4;
params.njumps=1e5;
params.sampleevery=1e2;
params.update=50;
%
tic
samples=mh(d/sigmad,-2*ones(size(m1)),@(m) (G*(10.^m))/sigmad,-10*ones(M,1),2*ones(M,1),params)';
toc
m11=10.^(mean(samples,2));
C11=cov(samples');
Corr11=corr(samples');

m12=10.^(median(samples,2));

%% % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                        %
% F O R W A R D   M O D E L   A N D   R E S I D U A L S  %
%                                                        %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % %%

% model predictions
dt=G*m12;

% residuals between observations and best-fitting model
r=d-dt;

% variance reduction
theta=1-sum(r.^2)/sum(d.^2);


%% % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                        %
%                     F I G U R E S                      %
%                                                        %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % %%

% plot forward models (the data is a linear combinations of these modes)
figure(1);clf;
subplot(2,1,1);cla
hold on, box on
plot(x2,d,'k-');
plot(x2,G)
set(gca,'ylim',[-1 1]*0.5)
title('Basis functions')
ylabel('Surface displacements')
xlabel('Across-fault distance (km)')
axis tight
legend('Data','basis functions')

subplot(2,1,2);cla
hold on, box on
Lambda=svds(G,M);
plot(log10(Lambda));
plot([1 M],[1 1]*log10(sigmad/1))
axis tight
xlabel('Eigenvalue number')
ylabel('Energy (log10)')
legend('Eigenvalue','SNR')


%% plot data, forward model, residuals

figure(2);clf
subplot(2,1,1);cla
hold on
plot(x2,d,'g+-');
plot(x2,dt,'k-.');
ylabel('Surface displacements')
legend('Data','Best forward model')
title('Data & model')
box on, axis tight

subplot(2,1,2);cla
plot(x2,r,'r');
title(sprintf('Residuals (variance reduction: %6.2f %%)',theta*100))
xlabel('x')
ylabel('Surface displacements')
box on, axis tight

%% target and best-fitting slip distributions
figure(3);clf
hold on
plot(m1,y3,'ko-','MarkerFaceColor','k','MarkerSize',3)
errorbar(m11,y3,m11-m11./10.^sqrt(diag(C11)),m11.*10.^sqrt(diag(C11))-m11,'horizontal','r+')
plot(m12,y3,'bo-','MarkerSize',3,'MarkerFaceColor','b')
legend('Target model','mean','median','Location','SouthEast')
xlabel('Slip (m)')
ylabel('Depth (km)')
set(gca,'YDir','Reverse','xlim',[-0.2 2.4]);
title('Slip distributions')
box on

%% bivariate plots

figure(4);clf;
lim=[-11 2];
for m=1:M
    subplot(M,M,(m-1)*M+m);cla;box on; hold on
    histogram(samples(m,:),'FaceColor','k','EdgeColor','k');
    set(gca,'xlim',lim)
    set(gca,'YTickLabel',[]);
    if m~=M
        set(gca,'XTickLabel',[]);
    else
        xlabel(sprintf('m_{%d}',M))
    end
    plot(log10(m11(m))*[1 1],get(gca,'ylim'))
    set(gca,'TickDir','Out')
    
    for k=m+1:M
        subplot(M,M,(k-1)*M+m);cla;box on; hold on
        histogram2(samples(m,:),samples(k,:),'DisplayStyle','tile','EdgeColor','None'), shading flat;
        colormap(gray)
        scatter(log10(m11(m)),log10(m11(k)),'r+')
        set(gca,'xlim',lim,'ylim',lim)
        if 1~=m
            set(gca,'YTickLabel',[])
        else
            ylabel(sprintf('m_{%d}',k));
        end
        if (M~=k)
            set(gca,'XTickLabel',[]);
        else
            xlabel(sprintf('m_{%d}',m));
        end
        set(gca,'TickDir','Out')
    end
end

%%
figure(5);clf;
imagesc(Corr11);
colormap(jet)
h=colorbar();
ylabel(h,'Correlation')
set(gca,'clim',[-1 1])
xlabel('Model parameters')
ylabel('Model parameters')
set(gca,'TickDir','Out')
axis equal tight
