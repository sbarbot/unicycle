
%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
% Evaluates the slip history on a fault in antiplane   %
% strain under the rate- and state-dependent friction  %
% Using L=0.05 m produces cycles of earthquakes.       %
% Using L=0.15 m produces cycles of slow-slip events.  %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%


%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%            P H Y S I C A L   M O D E L               %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

% rigidity (MPa)
ss.G=30e3;

% stress-interaction function
s12h=@(x2,x3,y2,y3,W) ss.G*( ...
    -(x3-y3)./((x2-y2).^2+(x3-y3).^2)+(x3+y3)./((x2-y2).^2+(x3+y3).^2) ...
    +(x3-y3-W)./((x2-y2).^2+(x3-y3-W).^2)-(x3+y3+W)./((x2-y2).^2+(x3+y3+W).^2) ...
    )/2/pi;

s13h=@(x2,x3,y2,y3,W) ss.G*( ...
    +(x2-y2)./((x2-y2).^2+(x3-y3).^2)-(x2-y2)./((x2-y2).^2+(x3+y3).^2) ...
    -(x2-y2)./((x2-y2).^2+(x3-y3-W).^2)+(x2-y2)./((x2-y2).^2+(x3+y3+W).^2) ...
    )/2/pi;

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%                        M E S H                       %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

% number of patches
M=100;
% patch size (m)
dz=0.1e3;
% coordinates of top patches
y3=(0:M-1)'*dz;
% width (down dip) of each slip patch
W=ones(M,1)*dz;

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%             S T R E S S   K E R N E L S              %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

ss.K=zeros(M,M);
for k=1:M
    % stress at the center of slip patches
    ss.K(:,k)=s12h(0,y3+dz/2,0,y3(k),W(k));
end

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%         F R I C T I O N   P A R A M E T E R S        %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

% friction properties
% static friction coefficient
ss.mu0=0.6*ones(size(y3));
% frictional parameters
ss.a=1e-2*ones(size(y3));
ss.b=ss.a-4e-3+8e-3*(y3<5e3);
% normal stress
ss.sigma=100*ones(size(y3));
% characteristic weakening distance
ss.L=0.05*ones(size(y3));
% plate velocity
ss.Vl=1e-9*ones(size(y3));
% reference slip rate
ss.Vo=1e-6*ones(size(y3));
% shear wave speed (m/s)
ss.Vs=3000;

% depth distribution of frictional properties
figure(3);clf;hold on;box on; grid on
plot(y3/1e3,ss.a,'k','LineWidth',2)
plot(y3/1e3,ss.a-ss.b,'r','LineWidth',2)
legend('a','a-b');
set(gca,'ylim',[-1 1]*1.5e-2)
xlabel('Depth (km)')
ylabel('a, a-b')

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%         N U M E R I C A L   S O L U T I O N          %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

ss.dgf=4;
Y0=zeros(M*ss.dgf,1);
Y0(1:ss.dgf:end)=ss.Vl;
Y0(2:ss.dgf:end)=ss.mu0.*ss.sigma;
Y0(3:ss.dgf:end)=zeros(M,1);
Y0(4:ss.dgf:end)=ss.Vl*0.98;

% initialize the function handle with 
% set constitutive parameters
yp=@(t,y) odefunAntiplane(t,y,ss);

% solve the system
options=odeset('Refine',1,'RelTol',3e-9,'InitialStep',1e-7);
[t,Y]=ode23(yp,[0 600*3.15e7],Y0,options);

% compute the instantaneous derivative
Yp=zeros(size(Y));
for i=1:length(t)
    Yp(i,:)=yp(t(i),Y(i,:)');
end

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%                    F I G U R E S                     %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

% fault dynamics as a function of time
figure(1);clf;set(gcf,'name','Time evolution')

subplot(2,1,1);cla;
pcolor(t/3.15e7,y3/1e3,log10(Yp(:,1:ss.dgf:end)')), shading flat
h=colorbar('Location','NorthOutside');
colormap(cycles);
title(h,'Velocity (m/s)')
xlabel('Time (yr)')
ylabel('Depth (km)');
set(gca,'YDir','reverse');
set(gca,'TickDir','Out')

subplot(2,1,2);cla; hold on; box on; grid on
plot(t/3.15e7,log10(Yp(:,(M/2-1)*ss.dgf+1)'),'k','LineWidth',2)
% velocity threshold for seismic slip
plot([0 t(end)]/3.15e7,log10(2*ss.Vs/ss.G*max(ss.a.*ss.sigma)*[1 1]),'k-.','LineWidth',2)
xlabel('Time (yr)')
ylabel('Velocity (m/s) log10')
title('Time series at the fault center')
set(gca,'TickDir','Out')


%% Fault dynamics as a function of time steps
figure(2);clf;set(gcf,'name','Evolution with time steps')

subplot(2,1,1);cla;
pcolor((1:length(t))/1e3,y3/1e3,log10(Yp(:,1:ss.dgf:end)')), shading flat
h=colorbar('Location','NorthOutside');
colormap(cycles);
title(h,'Velocity (m/s)')
xlabel('Step (integer x1000)')
ylabel('Depth (km)');
set(gca,'YDir','reverse');
set(gca,'TickDir','Out')

subplot(2,1,2);cla;hold on; box on; grid on
plot((1:length(t))/1e3,log10(Yp(:,(M/2-1)*ss.dgf+1)'),'k','LineWidth',2)
% velocity threshold for seismic slip
plot([0 length(t)]/1e3,log10(2*ss.Vs/ss.G*max(ss.a.*ss.sigma)*[1 1]),'k-.','LineWidth',2)
xlabel('Step (integer x1000)')
ylabel('Velocity (m/s) log10')
title('Evolution at the fault center')
set(gca,'TickDir','Out')


function [yp] = odefunAntiplane(~,y,ss)
% function ODEFUNANTIPLANE describes the evolution of the ordinary
% differential equation y' = f(t,y), where the state 
% vector y is 
%
%        /        s          \
%        |       tau         |
%    y = |                   |
%        | log(theta Vo / L) |
%        \    log(V / Vo)    /
%
% Instead of integrating numerically the againg law
%
%    d theta / dt = 1 - V theta / L
%
% as is, we operate the following change of variable
%
%    phi = ln (theta Vo / L)
%
% Considering the following relationships
%
%    d phi = d theta / theta
%
%    d theta = d phi theta = d phi exp(phi) L / Vo
%
%    d theta / dt = d phi exp(phi) / dt L / Vo
%
%    d phi exp(phi) / dt L / Vo = 1 - V theta / L = 1 - V exp(phi) / Vo
%
% we obtain the evolution law for the new variable
% 
%    d phi / dt = ( Vo exp(-phi) - V ) / L
%

% state variable
th=y(3:ss.dgf:end);

% norm of velocity
V=ss.Vo.*exp(y(4:ss.dgf:end));

% initialize yp
yp=zeros(size(y));

% velocity
yp(1:ss.dgf:end)=V;

% rate of state
yp(3:ss.dgf:end)=(ss.Vo.*exp(-th)-V)./ss.L;

% acceleration (rate of ln(V/Vo))
kv=ss.K*(V-ss.Vl);
yp(4:ss.dgf:end)=(kv-ss.b.*ss.sigma.*yp(3:ss.dgf:end)) ...
    ./(ss.a.*ss.sigma+ss.G*V/(2*ss.Vs));

% shear stress rate of change
yp(2:ss.dgf:end)=kv-ss.G*V/(2*ss.Vs).*yp(4:ss.dgf:end);

end

