% % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                         %
%         E a r t h q u a k e   c y c l e s               %
%      o n   a   s t r i k e - s l i p   f a u l t        %
%                                                         %
% DESCRIPTION:                                            %
% Solves the governing equations for fault slip evolution % 
% using the multiplicative rate- and state-dependent      %
% friction law                                            %
%                                                         %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

clear all

% % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                       %
%            P H Y S I C A L   M O D E L                %
%                                                       %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % %

% Rigidity (MPa)
G=30e3;

% Stress kernels for fault slip
s12h=@(x2,x3,y2,y3,Wf) G*( ...
    -(x3-y3)./((x2-y2).^2+(x3-y3).^2)+(x3+y3)./((x2-y2).^2+(x3+y3).^2) ...
    +(x3-y3-Wf)./((x2-y2).^2+(x3-y3-Wf).^2)-(x3+y3+Wf)./((x2-y2).^2+(x3+y3+Wf).^2) ...
    )/2/pi;

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%                        M E S H                       %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % %  %

%                         0       
% _______________________________________________ 0 km   +--------> x2
%                         |                              |
%                         |                              |
%                         |      Brittle                 |
%                         |                              V
%                         |                              x3
%                         | fault        
%                         + 35 km

% Fault mesh
y3=0e3; % starting depth (m)
y2=0e3; % horizontal position (m)

% Fault depth (m)
transition=35e3;

% number of fault patches
ss.M=200;

dz=transition/ss.M;
fpoles=y3+(0:ss.M)'*dz;

% top of fault patches
ss.y3f=fpoles(1:end-1); 
% width of fault patches
Wf=ones(ss.M,1)*dz; 
  
%% stress kernels

ss.K=zeros(ss.M,ss.M);

% stress at the center of the fault
for k=1:ss.M
    ss.K(:,k)=s12h(y2,ss.y3f+dz/2,y2,ss.y3f(k),Wf(k));
end

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%         F R I C T I O N   P A R A M E T E R S        %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

% effective confining pressure on fault (MPa)
ss.sigmab=100;

% frictional parameters ( velocity-weakening friction, a-b < 0 )
ss.a=1e-2*ones(size(ss.y3f));
ss.b=ss.a+4e-3*ones(size(ss.y3f));

% static friction coefficient
ss.mu0=0.6*ones(size(ss.y3f));

% characteristic weakening distance (m)
ss.L=0.01*ones(size(ss.y3f));

% plate velocity (m/s)
ss.Vl=1e-9*ones(size(ss.y3f));

% reference slip rate (m/s)
ss.Vo=1e-6*ones(size(ss.y3f));

% shear wave speed (m/s)
ss.Vs=3e3*ones(size(ss.y3f));

% radiation damping term
ss.damping=G./ss.Vs/2;

% Velocity-strengthening at top and bottom ( a-b > 0 )
% 5-15 km velocity-weakening
top   =floor(5e3/(transition/ss.M));
bottom=ceil(15e3/(transition/ss.M));
ss.b(1:top)     =ss.a(1:top)     -4e-3*ones(top,1);
ss.b(bottom:end)=ss.a(bottom:end)-4e-3*ones(length(ss.a(bottom:end)),1);

% Fault strength
ss.strength=ss.sigmab.*(ss.mu0+(ss.a-ss.b).*log(ss.Vl./ss.Vo))+G*ss.Vl./(2*ss.Vs);

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%         N U M E R I C A L   S O L U T I O N          %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

% state parameters
ss.dgf=4;

%% state vector
Y0=zeros(ss.M*ss.dgf,1);
Y0(1:ss.dgf:end)=zeros(size(ss.y3f));         % slip
Y0(2:ss.dgf:end)=ss.strength;                 % stress
Y0(3:ss.dgf:end)=log(ss.Vo./ss.Vl);      % log(theta Vo / L)
Y0(4:ss.dgf:end)=log(ss.Vl*0.98./ss.Vo); % log(V/Vo)

% function handle with constitutive parameters
yp=@(t,y) earthquake_cycle_multiplicative_ode(t,y,ss);
tic
% numerical solution
options=odeset('Refine',1,'RelTol',3e-7,'InitialStep',1e-3,'MaxStep',3e6); 
[t,Y]=ode45(yp,[0 500]*3.15e7,Y0,options);
toc

% velocity
V=repmat(ss.Vo',size(Y,1)-1,1).*exp(Y(2:end,4:ss.dgf:ss.M*ss.dgf));

% Peal velocity
Vmax=max(V,[],2);

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%                    F I G U R E S                     %
%                                                      %
%                   Function of time                   %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

figure(2);clf;set(gcf,'name','Time evolution')
f2a=subplot(3,1,1);cla;
pcolor(t(1:end-1)/3.15e7,ss.y3f/1e3,log10(V')), shading flat
set(gca,'YDir','reverse');

h=colorbar('Location','NorthOutside');
set(gca,'clim',[-12 1])
colormap(f2a,cycles);
title(h,'Slip-rate (m/s) log10')
set(gca,'TickDir','Out')
ylabel('Depth (km)');
xlabel('Time (yr)')

f2b=subplot(3,1,2);cla;
plot(t(1:end-1)/3.15e7,Vmax,'k','LineWidth',2)
ylabel('Velocity (m/s)')
set(gca,'YScale','Log','TickDir','Out')
title('Maximum slip-rate')
grid on

f2c=subplot(3,1,3);cla; box on; grid on
plot(t(1:end-1)/3.15e7,V(:,floor((top+bottom)/2)),'k','LineWidth',2)
xlabel('Time (yr)')
ylabel('Velocity (m/s) log10')
title('Time series at center of seismogenic zone')
set(gca,'TickDir','Out','YSCale','Log')
box on, grid on

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                Function of Time steps                %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%
figure(3);clf;set(gcf,'name','Time-step evolution')

f3a=subplot(3,1,1);cla;
pcolor((0:(length(t)-2))/1000,ss.y3f/1e3,log10(V')), shading flat
set(gca,'YDir','reverse','TickDir','Out');

h=colorbar('Location','NorthOutside');
set(gca,'clim',[-12 1])
colormap(f3a,cycles);
title(h,'Slip rate (m/s)')
xlabel('Time step (integer x1000)')
ylabel('Depth (km)');

f3b=subplot(3,1,2);cla;
plot((0:length(t)-2)/1000,Vmax,'k','LineWidth',2)
ylabel('Velocity (m/s)')
title('Maximum slip-rate on fault')
set(gca,'YScale','Log','TickDir','Out')
grid on

f3c=subplot(3,1,3);cla;
plot((0:length(t)-2)/1000,V(:,floor((top+bottom)/2)),'k','LineWidth',2)
xlabel('Time step (integer x1000)')
ylabel('Velocity (m/s)')
title('Center of seismogenic zone')
set(gca,'YScale','Log','TickDir','Out')
grid on

 function [Yp]= earthquake_cycle_multiplicative_ode(~,Y,ss)
% function odeViscoelastic describes the evolution of the ordinary
% differential equation y'=f(t,y), where the state vector y is 
%
%        /        s          \            
%        |       tau         |         
%    y=| log(theta Vo / L) |           
%        |   log( V / Vo )   |
%        \       ...         /
%
% Instead of integrating numerically the aging law
%
%    d theta / dt=1 - V theta / L
%
% as is, we operate the following change of variable
%
%    phi=ln (theta Vo / L)
%
% Considering the following relationships
%
%    d phi=d theta / theta
%
%    d theta=d phi theta=d phi exp(phi) L / Vo
%
%    d theta / dt=d phi exp(phi) / dt L / Vo
%
%    d phi exp(phi) / dt L / Vo=1 - V theta / L=1 - V exp(phi) / Vo
%
% we obtain the evolution law for the new variable
% 
%    d phi / dt=( Vo exp(-phi) - V ) / L
%

% shear stress
tau=Y(2:ss.dgf:end);

% state variable
th=Y(3:ss.dgf:end);

% velocity
V=ss.Vo.*exp(Y(4:ss.dgf:end));

% derivative of state vector
Yp=zeros(size(Y));

% slip velocity
Yp(1:ss.dgf:ss.M*ss.dgf)=V;

% rate of state (rate of log(theta/theta0))
dth=(ss.Vo.*exp(-th)-V)./ss.L;
Yp(3:ss.dgf:ss.M*ss.dgf)=dth;

% acceleration (rate of log(V/Vo))
kv=ss.K*(V-ss.Vl);
Yp(4:ss.dgf:ss.M*ss.dgf)=(kv-ss.b.*tau.*dth)./(ss.a.*tau+ss.damping.*V);

% stress rate
Yp(2:ss.dgf:ss.M*ss.dgf)=kv-ss.damping.*V.*Yp(4:ss.dgf:ss.M*ss.dgf);

end

