function samples = mh(varargin)
% samples = MH(dobs,m0,@g,[LB,UB,params])
%
% Metropolis-Hasting Markov Chain sampling of the probability density 
% 
%   rhoD(g(m))
%
% where 
%
%   rhoD(d)=exp( -0.5 ||dobs-d||^2 )
%
% is a multivariate normal distribution of the data.
%
% INPUT:
%   dobs  = data (Nx1)
%   m0    = initial parameters (Mx1)
%   @g    = function g(m)
%
%   Output is nsamples*M where nsamples=njumps/nthin
%
% Example:
%   % define a forward model (here y=a*exp(-bx))
%   myfun=@(m,x)(exp(-m(1)*x)+m(2));
%   % generate some noisy data
%   true_m = [1;2];
%   x=linspace(1,10,100);
%   dobs=myfun(true_m,x) + .05*randn(1,100);
%   % estimate parameters
%   m0=[1;2]; % you can get mt using nonlinear opt
%   samples=mh(dobs,m0,@(m)(myfun(m,x)));
%   figure,plot(samples)
%
% OPTIONAL INPUT PARAMETERS:
%   LB = lower bounds on m (default=[-inf]*ones(size(m0)))
%   UB = upper bounds on m (default=[+inf]*ones(size(m0)))
%   params.burnin = #burnin iterations (default=1000)
%   params.njumps = #jumps (default=5000)
%   params.nthin = keep every n jumps (default=10)
%   params.update = update proposal every n jumps (default=20)


[dobs,m0,g,LB,UB,params]=parseinputargs(varargin,nargin);

% log of data probability density function without prefactor
rhod=@(d) -1/2*sum((d-dobs).^2);

N=length(dobs);
m=m0;
e=rhod(g(m0));

acc=zeros(length(m),1);
rej=zeros(length(m),1);
keepp=zeros(params.njumps,length(m));
prop=ones(length(m),1);
count=0;

for i=1:params.njumps+params.burnin
    % loop over model parameters
    for k=1:length(m)
        oldm=m;
        m(k)=m(k)+randn*prop(k);
        if (m(k)<LB(k) || m(k)>UB(k))
            m(k)=oldm(k);
            rej(k)=rej(k)+1;
        else
            olde=e;
            e=rhod(g(m));
            if exp(e-olde)>rand
                acc(k)=acc(k)+1;
                if i<=params.njumps
                    count=count+1;
                end
            else
                m(k)=oldm(k);
                rej(k)=rej(k)+1;
                e=olde;
            end
        end
    end
    keepp(i,:)=m;
    
    % update jump distance
    if i<=params.njumps
        if (rem(i,params.update)==0)
            prop=prop.*sqrt((1+1.5*acc)./(1+rej));
            acc=0*acc;
            rej=0*rej;
        end
    end
end
fprintf('# acceptance rate: %4.1f%%\n',100*count/params.njumps/length(m));
samples=keepp(params.burnin+1:params.nthin:end,:);

function [y,x0,g,LB,UB,params]=parseinputargs(varargin,nargin)
y=varargin{1};
x0=varargin{2};
g=varargin{3};
if(nargin>3);LB=varargin{4};else LB=-inf*ones(size(x0));end
if(nargin>4);UB=varargin{5};else UB=+inf*ones(size(x0));end
if(nargin>5)
    params=varargin{6};
else
    params=struct('burnin',1000,'njumps',5000,'nthin',10,'update',20);
end
if(~isfield(params,'burnin'));params.burnin=1000;end
if(~isfield(params,'njumps'));params.njumps=5000;end
if(~isfield(params,'nthin'));params.nthin=10;end
if(~isfield(params,'update'));params.update=20;end




