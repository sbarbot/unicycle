
%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
% Evaluates the slip history on a spring-slider system %
% under the rate- and state-dependent friction.        %
% Describes the evolution of slip, stress, age of      %
% contact, and area of contact during seismic cycles.  %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%         F R I C T I O N   P A R A M E T E R S        %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

% reference friction coefficient
ss.mu0=0.6;
% stiffness (MPa/m)
ss.k=1;
% frictional parameters
ss.a=1e-2;
ss.b=ss.a+4e-3;
% normal stress (MPa)
ss.sigma=50.0;
% characteristic-weakening distance (m)
ss.L=0.1;
% loading velocity (m/s)
ss.Vl=1e-9;
% reference slip rate (m/s)
ss.Vo=1e-6;
% shear-wave space (m/s)
ss.Vs=3000;
% rigidity (MPa)
ss.G=30e3;
% material hardness for ploughing (MPa)
ss.chi=800;

fprintf('k=%f, (a-b)*sigma / L=%f\n', ...
    ss.k,(ss.b-ss.a)*ss.sigma/ss.L);
fprintf('recurrence time (a-b)*sigma / k / Vl=%f yr\n', ...
    (ss.b-ss.a)*ss.sigma/ss.k/ss.Vl/3.14e7);


%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%         N U M E R I C A L   S O L U T I O N          %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

% function handle with constitutive parameters
yp=@(t,y) odeFunction(t,y,ss);

% numerical solution with ode23 or ode45
options=odeset('Refine',1,'RelTol',2e-14,'InitialStep',1e-10);
y0=[0;ss.mu0*ss.sigma;-log(ss.Vl/ss.Vo);log(ss.Vl/ss.Vo*0.99)];
[t,Y]=ode23(yp,[0 1000]*3.15e7,y0,options);

% compute derivative
Yp=zeros(size(Y));
for i=1:length(t)
    Yp(i,:)=yp(t(i),Y(i,:));
end


%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%                    F I G U R E S                     %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

figure(1);clf;set(gcf,'name','Dynamic variables evolution')
subplot(5,1,1);cla;hold on; box on
plot(t/3.15e7,Y(:,1),'k','LineWidth',2)
plot([t(1) t(end)]/3.15e7,[t(1) t(end)]*ss.Vl,'k-.','LineWidth',2)
ylabel('Slip (m)')
set(gca,'TickDir','Out')
grid on

subplot(5,1,2);cla; box on
plot(t/3.15e7,Y(:,2),'k','LineWidth',2)
ylabel('Stress (MPa)')
set(gca,'TickDir','Out')
grid on

subplot(5,1,3);cla; box on
plot(t/3.15e7,ss.L/ss.Vo*exp(Y(:,3)),'k','LineWidth',2)
ylabel('Age of contact (s)')
set(gca,'TickDir','Out')
grid on
set(gca,'YScale','Log')

subplot(5,1,4);cla; box on; 
plot(t/3.15e7,ss.mu0*ss.sigma/ss.chi*exp(ss.b/ss.mu0*Y(:,3))*100,'k','LineWidth',2)
set(gca,'TickDir','Out')
xlabel('Time (yr)')
ylabel('Area of contact (%)')
grid on

subplot(5,1,5);cla; box on; 
plot(t/3.15e7,ss.Vo*exp(Y(:,4)),'k','LineWidth',2)
set(gca,'TickDir','Out')
set(gca,'YScale','Log')
xlabel('Time (yr)')
ylabel('Velocity (m/s)')
grid on


%% phase diagram

figure(2);clf;set(gcf,'name','Phase diagrams')
burnin=2000;
subplot(3,1,1);cla;hold on; box on; grid on
plot((Yp(burnin:end,1)),Y(burnin:end,2),'k','LineWidth',2)
plot([-14 0],(ss.mu0+(ss.a-ss.b)*log(10.^([-14 0])/ss.Vo))*ss.sigma,'k-.','LineWidth',2)
ylabel('Stress (MPa)')
set(gca,'XScale','Log','TickDir','Out')

subplot(3,1,2);cla;hold on; box on; grid on
plot(Yp(burnin:end,1),ss.L/ss.Vo*exp(Y(burnin:end,3)),'k','LineWidth',2)
ylabel('Age (s)')
set(gca,'XScale','Log','YScale','Log','TickDir','Out')

subplot(3,1,3);cla;hold on; box on; grid on
plot(Yp(burnin:end,1),ss.mu0*ss.sigma/ss.chi*exp(ss.b/ss.mu0*Y(burnin:end,3))*100,'k','LineWidth',2)
xlabel('Velocity (m/s)')
ylabel('Area of contact (%)')
set(gca,'XScale','Log','YScale','Log','TickDir','Out')

function [yp] = odeFunction(~,y,ss)
% function ODEFUNCTION describes the evolution of the ordinary
% differential equation y' = f(t,y), where the state 
% vector y is 
%
%        /        s          \
%    y = |       tau         |
%        | log(theta Vo / L) |
%        \  log(V / Vo)      /
%
% Instead of integrating numerically the againg law
%
%    d theta / dt = 1 - V theta / L
%
% as is, we operate the following change of variable
%
%    phi = ln (theta Vo / L)
%
% Considering the following relationships
%
%    d phi = d theta / theta
%
%    d theta = d phi theta = d phi exp(phi) L / Vo
%
%    d theta / dt = d phi exp(phi) / dt L / Vo
%
%    d phi exp(phi) / dt L / Vo = 1 - V theta / L = 1 - V exp(phi) / Vo
%
% we obtain the evolution law for the new variable
% 
%    d phi / dt = ( Vo exp(-phi) - V ) / L
%

V=ss.Vo*exp(y(4));

dth=(ss.Vo*exp(-y(3))-V)/ss.L;

dV=(ss.k*(ss.Vl-V)-ss.b*ss.sigma*dth)/(ss.a*ss.sigma+ss.G*V/2/ss.Vs);

yp=[V; ...
    ss.k*(ss.Vl-V)-ss.G/2*dV*V/ss.Vs; ...
    dth;
    dV];

end

