
# reference point
echo -120.374000 35.815000 | proj +proj=utm +zone=11
195144.78       3968685.64

# Community velocity data and model
https://topex.ucsd.edu/CGM/CGM_html/

# MIDAS velocity field
http://geodesy.unr.edu/magnet/Table3web.html

# SCEDC catalogue (https://service.scedc.caltech.edu/ftp/catalogs/SCSN/)
grep -h -v "#" scedc/*.catalog | awk 'BEGIN{print "#    lon    lat  mag year"} $5>6 {print $8,$7,$5,substr($1,1,4)}' > gmt/scedc-Mw6+_ll.dat
grep -v "#" gmt/scedc-Mw6+_ll.dat | proj +proj=utm +zone=11 | awk '{print $1-195144.78,$2-3968685.64,$3,$4}' > gmt/scedc-Mw6+_m.dat
# <PRE2

# https://service.scedc.caltech.edu/eq-catalogs/date_mag_loc.php
grep -v "#" scedc-Mw3+-1990-2020_ll.dat | awk '{print $8,$7,$0}' | proj +proj=utm +zone=11 | awk '{print $1-195144.78,$2-3968685.64,$7,substr($3,1,4)}' > scedc-Mw3+-1990-2020_m.dat

# NCEDC catalogue (https://ncedc.org/ncedc/catalog-search.html)
grep -v "#" gmt/ncedc-Mw6+_ll.dat | awk '{print $4,$3,$0}' | proj +proj=utm +zone=11 | awk 'BEGIN{print "# x y mag year"}{print $1-195144.78,$2-3968685.64,$8,substr($3,1,4)}' > gmt/ncedc-Mw6+_m.dat

# CFM5 fault catalogue
grep -v "#" CFM5-preferred-traces_ll.xyz | proj +proj=utm +zone=11 -t">" | awk '{if (">"==substr($0,1,1)){print "NaN NaN"}else{print $1-195144.78,$2-3968685.64}}' > ../matlab/CFM5-preferred-traces_m.dat

# convert picks to .flt format
cat picks.dat | awk 'BEGIN{c=1;pi=atan2(1,0)*2;n=0;print "# n             x1             x2    x3           len  wid         str dip rake"}{if (">"==substr($0,1,1)){n=0;print "#",substr($0,3,100)}else{if(0==n){x1o=$2;x2o=$1;n=1}else{x2=$1;x1=$2;theta=atan2(x2-x2o,x1-x10)*180/pi;l=sqrt((x2-x20)**2+(x1-x1o)**2);printf "%03d %14.6f %14.6f %d %13.6f %d %11.6f %3d %4d\n",c,x1o,x2o,15e3,l,5e3,theta,90,180;c++;x1o=x1;x2o=x2}}}' > brittle-creep.flt

# UCERF3 geologic slip rate - https://pubs.usgs.gov/of/2013/1165/ - Appendix B
grep -v "#" ucerf3/ofr2013-1165_tableB1-strike-slip.txt | awk -F"\t" '{split($9,a," - ");split($10,b,"-");gsub(/ /,"-",$1);print $13,$14,b[1]+0,a[1]+0,a[2]+0,$15,$4,$1}' | proj +proj=utm +zone=11 | awk 'BEGIN{print "# from UCERF3, Appendix B - ofr2013-1165_tableB1.xlsx";print "# rate in mm/yr; position in meters.";print "# n  rate   min   max         x2         x1 str dip name"}{printf "%3d %5.2f %5.2f %5.2f %10.2f %10.2f %3d %3d %s\n", NR,$3,$4,$5,$1-195144.78,$2-3968685.64,$6,$7,$8}' > gmt/ucerf3_m.dat

# earthquake surface trace
grep -v "#" ~/Documents/src/relax/examples/california/sanfrancisco/faults/song+08_km.xyz | awk '0==$3{print $1*1e3+10882.61,$2*1e3+4192606.41}' | invproj +proj=utm +zone=11 -f "%12.9f" > san-francisco-1906-trace_ll.xy

# convert shear zone model to _ll.xyz
grep -v "#" shz-checker-e12_m.xyz | awk '{if (">"==substr($0,1,1)){print $0}else{print $1+195144.78,$2+3968685.64,$3/1e3}}'| invproj +proj=utm +zone=11 -t">" -f "%14.7f" > shz-checker-e12_ll.xyz

# heat flow data from http://geothermal.smu.edu/static/DownloadFilesButtonPage.htm?
awk -F"," '{if (""!=$39){print $4,$3,$39}}' staging.smu_hf_view_materialized.csv > heat-flow_ll.xy

awk '{print $1,$2,$3}' gps-model-interp_ll.dat | surface -R-126/-113/32/42 -I0.1 -Ggps-model-interp_ll-east.grd

# global heat flow database http://www.heatflow.org/thermoglobe/map#
# https://engineering.und.edu/research/global-heat-flow-database/data.html
cat <(awk -F"," '{if (""!=$21 && 0!=$21){print $3,$2,$21}}' ThermoGlobe_05_03_20.csv) <(awk -F"," '{if (""!=$39 && 0!=$39){print $4,$3,$39}}' staging.smu_hf_view_materialized.csv) <(awk -F"," '{if (""!=$15 && 0!=$15){print $5,$4,$15}}' global2010.csv) | blockmean -R-130/-90/22/52 -I0.1 | surface -R-130/-90/22/52 -I0.1 -T1i -T0b -Gheat-flow-all_ll.grd
grdfilter heat-flow-all_ll.grd -Gheat-flow-all-smooth_ll.grd -Fc150 -D2

# USGS quaternary faults database
grep -e "<Placemark" -e "<name" -e "<coor" qfaults.kml | awk '{if (substr($1,1,2)=="<P"){print ">",substr($0,20,9)}; if (substr($1,1,2)=="<n"){split($0,a,">|<");print "> ",a[3]}; if (substr($1,1,2)=="<c"){print ">";for (i=2;i<=NF;i++){split($i,b,",");print b[1],b[2]}}}' > qfaults_ll.xy

# ridgecrest source
grep -v "#" ~/Documents/src/unicycle/matlab/examples/california/ridgecrest/faults/coseismicslip_Mw64_joint.xyz | awk '{if (substr($0,1,1)==">"){print $0}else{print $1+445311.83,$2+3958163.70,$3}}' | invproj +proj=utm +zone=11 -f "%f" -t">" > ridgecrest_Mw6.4_ll.xyz
grep -v "#" ~/Documents/src/unicycle/matlab/examples/california/ridgecrest/faults/coseismicslip_Mw71_joint.xyz | awk '{if (substr($0,1,1)==">"){print $0}else{print $1+445311.83,$2+3958163.70,$3}}' | invproj +proj=utm +zone=11 -f "%f" -t">" > ridgecrest_Mw7.1_ll.xyz

# interpolate plastic strain-rate
grep -v "#" shz-m6-e_ll.xyz | awk '{if (">"==substr($0,1,1)){v=substr($0,5,20)}else{print $1,$2,v}}' | surface -R-126/-113/32/42 -I0.01 -T0.1 -Ll0 -Gshz-m6-e_ll.grd

# correlation between data and model
set yrange [-0.02:0.02]
set xrange [-0.02:0.02]
set size ratio -1
set palette defined (0 '#2E4EEE',1 '#5677F0',2 '#87A0F4',3 '#BFCDF9',4 '#E9EEFD',5 '#FFFFFF',6 '#FEFDAA',7 '#F9EF5E',8 '#ECBD42',9 '#DD7D36',10 '#D0222A')
plot 'gps-data-model-res_ll.dat' u 3:9 w points lt 5, '' u 4:10 w points lt 8, '' u 5:11:(0.0005):5 w cir lc palette fill solid noborder
