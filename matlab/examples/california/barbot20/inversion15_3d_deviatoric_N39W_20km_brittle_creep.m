% inversion of 3d GPS velocity field for strain accumulation 
% at the root of California faults. Include a dilatation source below
% Long Valley Caldera to represent the outward and subsidsence motion and
% the creeping segment of the San Andreas fault. Force deviatoric strain by
% design to reduce model space.
%
% Barbot S. Flow distribution beneath the California margin, Nature
% Communications, 2020.

clear all

set(groot,'defaultFigureCreateFcn',@(fig,~)addToolbarExplorationButtons(fig))
set(groot,'defaultAxesCreateFcn',@(ax,~)set(ax.Toolbar,'Visible','off'))

[borders.x,borders.y]=textread('matlab/ca_borders_m.dat','%f %f','commentstyle','shell');
[faults.x,faults.y]=textread('matlab/ca_faults_m.dat','%f %f','commentstyle','shell');
[cfm5.x,cfm5.y]=textread('matlab/cfm5-preferred-traces_m.dat','%f %f','commentstyle','shell');
[cont.x,cont.y]=textread('matlab/continental-transform_m.dat','%f %f','commentstyle','shell');
[garlock.x,garlock.y]=textread('matlab/CFM5-garlock_m.dat','%f %f','commentstyle','shell');
[sanJacinto.x,sanJacinto.y]=textread('matlab/CFM5-san-jacinto_m.dat','%f %f','commentstyle','shell');
[elsinore.x,elsinore.y]=textread('matlab/CFM5-elsinore_m.dat','%f %f','commentstyle','shell');
[oakRidge.x,oakRidge.y]=textread('matlab/CFM5-oak-ridge_m.dat','%f %f','commentstyle','shell');
[mendecino.x,mendecino.y]=textread('matlab/mendecino-fracture-zone_m.dat','%f %f','commentstyle','shell');
[cascadia.x,cascadia.y]=textread('matlab/cascadia-trench_m.dat','%f %f','commentstyle','shell');
[gmrt.x,gmrt.y]=textread('matlab/gmrt-contour_m.dat','%f %f','commentstyle','shell');

[scedc.x,scedc.y,scedc.mag,scedc.year]=textread('gmt/scedc-Mw6+_m.dat','%f %f %f %f','commentstyle','shell');
[scedc3.x,scedc3.y,scedc3.mag,scedc3.year]=textread('gmt/scedc-Mw3+-1990-2020_m.dat','%f %f %f %f','commentstyle','shell');
[ncedc.x,ncedc.y,ncedc.mag,ncedc.year]=textread('gmt/ncedc-Mw6+_m.dat','%f %f %f %f','commentstyle','shell');
[ncedc3.x,ncedc3.y,ncedc3.mag,ncedc3.year]=textread('gmt/ncedc-Mw3+-1990-2020_m.dat','%f %f %f %f','commentstyle','shell');

[~,ucerf3.rate,ucerf3.min,ucerf3.max,ucerf3.x,ucerf3.y,ucerf3.str,ucerf3.dip,ucerf3.name]=textread('gmt/ucerf3_m.dat','%d %f %f %f %f %f %f %f %s','commentstyle','shell');

[~,gps.y,gps.x,gps.name]=textread('gps/network_m.dat','%d %f %f %s','commentstyle','shell');

[gps.longname,gps.lon,gps.lat,gps.vn,gps.ve,gps.vd,gps.sn,gps.se,gps.sd,gps.start,gps.end,~]= ...
    textread('gps/california-vel.dat','%s %f %f %f %f %f %f %f %f %f %f %f','commentstyle','shell');

relativePlateMotion=[49.6,48.2,50.35,49.62,50.34,47.85,50.22,48.62,47.53,48.37,50.49,48.21,45.20,48.09,38.50,45.58,45.17,47.27];

heavi=@(x) 0+(x>0);

%% GPS DATA AND MODEL
figure(1);clf;set(gcf,'Name',sprintf('Data'))
scale=3e3;
box on, axis equal, hold on

plot(borders.x/1e3,borders.y/1e3,'k-','LineWidth',1);
plot(faults.x/1e3,faults.y/1e3,'-','LineWidth',1,'Color',[1 1 1]*0.5);

plot(cont.x/1e3,cont.y/1e3,'r-','LineWidth',2);
plot(garlock.x/1e3,garlock.y/1e3,'r-','LineWidth',2);
plot(sanJacinto.x/1e3,sanJacinto.y/1e3,'r-','LineWidth',2);
plot(elsinore.x/1e3,elsinore.y/1e3,'r-','LineWidth',2);
plot(oakRidge.x/1e3,oakRidge.y/1e3,'r-','LineWidth',2);
plot(mendecino.x/1e3,mendecino.y/1e3,'r-','LineWidth',2);
%plot(cfm5.x/1e3,cfm5.y/1e3,'r-','LineWidth',1);

scatter(gps.x/1e3,gps.y/1e3,50,-gps.vd*1e3,'filled');

quiver(gps.x/1e3,gps.y/1e3,scale*(gps.ve+0.02),scale*(gps.vn+0.01),0,'k');
unicycle.plot.ellipse(scale*gps.se,scale*gps.sn,0*gps.x,gps.x/1e3+scale*(gps.ve+0.02),gps.y/1e3+scale*(gps.vn+0.01));

set(gca,'xlim',[-400 600],'ylim',[-400 700]);

set(gca,'clim',[-1 1]*median(abs(gps.vd))*2e3);
xlabel('East (km)');
ylabel('North (km)');
hbar=colorbar();
ylabel(hbar,'Uplift velocity (mm/yr)');
legend('Borders','Faults','Uplift rate','Velocity','Location','SouthWest');

%return

%% VOLUME MESH

eModel=unicycle.greens.semiInfiniteCuboidVolume(30e3,0.25);
shz=unicycle.geometry.semiInfiniteCuboidReceiver('faults/deep-shear-zone-rotated-20km',eModel);
shz.scale=1e3;

halfSpace=unicycle.greens.cuboidVolume17(30e3,0.25);
lvc=unicycle.geometry.cuboidReceiver('faults/long-valley-caldera',halfSpace);
lvc.scale=1e3;

dislocationModel=unicycle.greens.okada92(30e3,0.25);
seg=unicycle.geometry.rectangleReceiver('faults/creeping-segment-shallow.seg',dislocationModel);
seg.scale=1e3;

flt=unicycle.geometry.rectangleReceiver('faults/brittle-creep.flt',dislocationModel);
flt.scale=1e3;

%% GREEN'S FUNCTION

fct='matlab/greens-3d-T20km-D20km-S39W.mat';
if ~exist(fct,'file')
    [L11,L12,L13,L22,L23,L33]=shz.displacementKernels([gps.x,gps.y,0*gps.x],3);
    save(fct,'L11','L12','L13','L22','L23','L33');
else
    load(fct);
end

[interp.x,interp.y]=meshgrid((-400:10:600)'*1e3,(-400:10:700)'*1e3);
fct='matlab/greens-3d-T20km-D20km-S39W-interp-D100.mat';
if ~exist(fct,'file')    
    [L11i,L12i,L13i,L22i,L23i,L33i]=shz.displacementKernels([interp.x(:),interp.y(:),0*interp.x(:)-100e3],3);
    save(fct,'L11i','L12i','L13i','L22i','L23i','L33i');
else
    load(fct);
end

fct='matlab/greens-long-valley.mat';
if ~exist(fct,'file')
    [M11,M12,M13,M22,M23,M33]=lvc.displacementKernels([gps.x,gps.y,0*gps.x],3);
    save(fct,'M11','M12','M13','M22','M23','M33');
else
    load(fct);
end

fct='matlab/greens-creeping-segment-shallow.mat';
if ~exist(fct,'file')
    [Mss,Mds]=seg.displacementKernels([gps.x,gps.y,0*gps.x],3);
    save(fct,'Mss','Mds');
else
    load(fct);
end

fct='matlab/greens-brittle-creep.mat';
if ~exist(fct,'file')
    [Css,Cds]=flt.displacementKernels([gps.x,gps.y,0*gps.x],3);
    save(fct,'Css','Cds');
else
    load(fct);
end

%% MODEL DESIGN

% degrees of freedom
dgf=3;

% whole-array rotation and translation
R=zeros(dgf*length(gps.x),dgf);
R(1:dgf:end,1)=-1*gps.y/1e2;
R(2:dgf:end,1)= 1*gps.x/1e2;
R(1:dgf:end,2)= ones(length(gps.x),1);
R(2:dgf:end,3)= ones(length(gps.x),1);

% data vector
d=zeros(dgf*length(gps.x),1);
d(1:dgf:end)= gps.ve;
d(2:dgf:end)= gps.vn;
d(3:dgf:end)=-gps.vd;

% data weight Cd^-1/2
W=[1./(gps.se),1./(gps.sn),1./(gps.sd)]'/1e3;
W=diag(W(:));
%W=eye(dgf*length(gps.sn));

% horizontal strain
M=1e-3*[M11,M12,M22,M33];
F=1*[Mss,Css];
G=[1e-6*[L11-L22,L12],M,F,R];

% discrete Laplacian operator
[Lx,Ly]=meshgrid(1:shz.levels{1}.T/shz.T(1),1:shz.levels{1}.L/shz.L(1));
L=unicycle.optim.computeLaplacian(Lx(:),Ly(:),0*Lx(:),4);

% checkerboard test
%dc=1e-6*L12*(-7e-2*heavi(mod(Lx(:)+2+3*mod(fix(Ly(:)/3),2),6)-2.5)); % 3x3
dc=1e-6*L12*(-7e-2*heavi(mod(Lx(:)+3+4*mod(fix(Ly(:)/4),2),8)-3.5)); % 4x4

% positivity constraints
A=[zeros(shz.N) eye(shz.N) zeros(shz.N,size(M,2)) zeros(shz.N,size(F,2)) zeros(shz.N,size(R,2))];

% penalization (first-order Tikhonov)
P=[W*G;
    5e-2*eye(2*shz.N),zeros(2*shz.N,size(M,2)),zeros(2*shz.N,size(F,2)),zeros(2*shz.N,size(R,2))];
p=[W*d;
    zeros(2*shz.N,1)];

% smoothing (second-order Tikhonov)
H1=[W*G;
    2e-2*[L,zeros(size(L));zeros(size(L)),L],zeros(2*shz.N,size(M,2)),zeros(2*shz.N,size(F,2)),zeros(2*shz.N,size(R,2))];
h1=[W*d;
    zeros(2*shz.N,1)];

% first+second-order regularization
H2=[W*G;
    5e-2*eye(2*shz.N),zeros(2*shz.N,size(M,2)),zeros(2*shz.N,size(F,2)),zeros(2*shz.N,size(R,2));
    1e-2*[L,zeros(size(L));zeros(size(L)),L],zeros(2*shz.N,size(M,2)),zeros(2*shz.N,size(F,2)),zeros(2*shz.N,size(R,2))];
h2=[W*d;
    zeros(2*shz.N,1);
    zeros(2*shz.N,1)];

% first+second-order regularization, plate motion
PM=1e-6*repmat(eye(size(Lx,1)),1,size(Lx,2));
H3=[W*G;
    5e-2*eye(2*shz.N),zeros(2*shz.N,size(M,2)),zeros(2*shz.N,size(F,2)),zeros(2*shz.N,size(R,2));
    5e-3*[L,zeros(size(L));zeros(size(L)),L],zeros(2*shz.N,size(M,2)),zeros(2*shz.N,size(F,2)),zeros(2*shz.N,size(R,2));
    5e0*shz.T(1)*[zeros(size(Lx,1),shz.N),PM,zeros(size(Lx,1),size(M,2)),zeros(size(Lx,1),size(F,2)),zeros(size(Lx,1),size(R,2))]];
h3=[W*d;
    zeros(2*shz.N,1);
    zeros(2*shz.N,1);
    5e0*(-47*0.5)*1e-3*ones(size(Lx,1),1)];

% checkerboard data
h3c=[W*dc;
    zeros(2*shz.N,1);
    zeros(2*shz.N,1);
    5e0*(-47*0.5)*1e-3*ones(size(Lx,1),1)];

%% INVERSION

% least-squares with positivity constraints and first-order+second-order Tokhonov regularization
% relative plate motion
m6=lsqlin(H3,h3,A,zeros(shz.N,1));

%% PREFERRED MODEL

m.m=m6;
m.dt=G*m.m;
interp.dt=(1e-6*[L11i-L22i,L12i])*m.m(1:shz.N*2);
m.ve=m.dt(1:dgf:end);
m.vn=m.dt(2:dgf:end);
m.vu=m.dt(3:dgf:end);
m.e11= m.m(0*shz.N+1:1*shz.N);
m.e12= m.m(1*shz.N+1:2*shz.N);
m.e22=-m.m(0*shz.N+1:1*shz.N);
m.e=sqrt(m.e11.^2+2*m.e12.^2+m.e22.^2)/sqrt(2);
m.dtr=R*m.m(2*shz.N+4*lvc.N+seg.N+flt.N+1:2*shz.N+4*lvc.N+seg.N+flt.N+3);
m.r=d-m.dt;
m.vr=1-sqrt((m.r'*W*W*m.r)/(d'*W*W*d));

m.dtc=M*m.m(2*shz.N+1:2*shz.N+4*lvc.N);
m.vec=m.dtc(1:dgf:end);
m.vnc=m.dtc(2:dgf:end);
m.vuc=m.dtc(3:dgf:end);
m.e11c=m.m(2*shz.N+0*lvc.N+1:2*shz.N+1*lvc.N);
m.e12c=m.m(2*shz.N+1*lvc.N+1:2*shz.N+2*lvc.N);
m.e22c=m.m(2*shz.N+2*lvc.N+1:2*shz.N+3*lvc.N);
m.e33c=m.m(2*shz.N+3*lvc.N+1:2*shz.N+4*lvc.N);
m.ekkc=m.e11c+m.e22c+m.e33c;
m.ec=sqrt(m.e11c.^2+2*m.e12c.^2+m.e22c.^2);

seg.slip=m.m(2*shz.N+4*lvc.N+1:2*shz.N+4*lvc.N+seg.N);
flt.slip=m.m(2*shz.N+4*lvc.N+seg.N+1:2*shz.N+4*lvc.N+seg.N+flt.N);

% eigenvectors
m.p1=zeros(shz.N,2);
m.p2=zeros(shz.N,2);
m.eps1=zeros(shz.N,1);
m.eps2=zeros(shz.N,1);
m.theta=zeros(shz.N,1);
for i=1:1:shz.N
    [V,D]=eig([m.e11(i) m.e12(i);
        m.e12(i) m.e22(i)],'vector');
    [~,pos]=sort(D);
    m.p1(i,:)=D(pos(1))*(V(pos(1),1)*shz.nv(i,1:2)+V(pos(1),2)*shz.sv(i,1:2));
    m.p2(i,:)=D(pos(2))*(V(pos(2),1)*shz.nv(i,1:2)+V(pos(2),2)*shz.sv(i,1:2));
    m.eps2(i)=D(pos(1));
    m.eps1(i)=D(pos(2));
    m.theta(i)=atan2(m.p2(i,1),m.p2(i,2));
end

%% GPS DATA AND FORWARD MODEL
figure(2);clf;set(gcf,'Name',sprintf('Data, Model, Variance reduction: %f%%',m.vr*100))
addToolbarExplorationButtons(gcf)
scale=2e3;
box on, axis equal, hold on


plot(borders.x/1e3,borders.y/1e3,'k-','LineWidth',1)
plot(faults.x/1e3,faults.y/1e3,'-','LineWidth',1,'Color',[1 1 1]*0.5)

plot(cont.x/1e3,cont.y/1e3,'r-','LineWidth',2);
plot(garlock.x/1e3,garlock.y/1e3,'r-','LineWidth',2);
plot(sanJacinto.x/1e3,sanJacinto.y/1e3,'r-','LineWidth',2);
plot(elsinore.x/1e3,elsinore.y/1e3,'r-','LineWidth',2);
plot(oakRidge.x/1e3,oakRidge.y/1e3,'r-','LineWidth',2);
plot(cascadia.x/1e3,cascadia.y/1e3,'k-','LineWidth',2);
plot(mendecino.x/1e3,mendecino.y/1e3,'r-','LineWidth',2);

% vertical displacements
%scatter(gps.x/1e3,gps.y/1e3,200,m.vu*1e3,'filled')
%scatter(gps.x/1e3,gps.y/1e3,50,-gps.vd*1e3,'filled','MarkerEdgeColor','k')

% green's functions
index=10*74+30;
shz.plotEdgesById(index);
quiver(gps.x/1e3,gps.y/1e3,scale/1e5*(L11(1:dgf:end,index)),scale/1e5*(L11(2:dgf:end,index)),0,'r-.');

% horizontal displacements
%quiver(gps.x/1e3,gps.y/1e3,scale*(m.ve-m.dtr(1:dgf:end)),scale*(m.vn-m.dtr(2:dgf:end)),0,'r-.');

%quiver(gps.x/1e3,gps.y/1e3,scale*(gps.ve-m.dtr(1:dgf:end)),scale*(gps.vn-m.dtr(2:dgf:end)),0,'k');
%unicycle.plot.ellipse(scale*gps.se,scale*gps.sn,0*gps.x,gps.x/1e3+scale*(gps.ve-m.dtr(1:dgf:end)),gps.y/1e3+scale*(gps.vn-m.dtr(2:dgf:end)));

%quiver(gps.x/1e3,gps.y/1e3,scale*m.dtc(1:dgf:end),scale*m.dtc(2:dgf:end),0,'b-.');

%seg.plotPatch();

set(gca,'xlim',[-400 600],'ylim',[-400 700])
set(gca,'clim',[-1 1]*max(abs(get(gca,'clim')))/4)
xlabel('East (km)')
ylabel('North (km)')
hbar=colorbar();
ylabel(hbar,'Uplift rate (m/yr)');
legend('Borders','Faults','Modeled uplift','Observed uplift','Modeled horizontal','Observed horizontal','Location','SouthWest');

%% RESIDUALS
figure(3);clf;set(gcf,'Name',sprintf('Residuals; Variance reduction: %f%%',m.vr*100))
addToolbarExplorationButtons(gcf)
scale=5e3;
box on, axis equal, hold on
%lvc.plotEdges();

%plot(gmrt.x/1e3,gmrt.y/1e3,'k-','LineWidth',0.5);
plot(borders.x/1e3,borders.y/1e3,'k-','LineWidth',2);
plot(faults.x/1e3,faults.y/1e3,'-','LineWidth',1,'Color',[1 1 1]*0.5);
plot(cfm5.x/1e3,cfm5.y/1e3,'b-','LineWidth',1);
plot(cont.x/1e3,cont.y/1e3,'r-','LineWidth',2);
plot(garlock.x/1e3,garlock.y/1e3,'r-','LineWidth',2);
plot(sanJacinto.x/1e3,sanJacinto.y/1e3,'r-','LineWidth',2);
plot(elsinore.x/1e3,elsinore.y/1e3,'r-','LineWidth',2);
plot(oakRidge.x/1e3,oakRidge.y/1e3,'r-','LineWidth',2);

plot(ncedc3.x/1e3,ncedc3.y/1e3,'b.','MarkerSize',4);
plot(scedc3.x/1e3,scedc3.y/1e3,'b.','MarkerSize',4);

%scatter(gps.x/1e3,gps.y/1e3,150,(-gps.vd-m.vu)*1e3,'filled')
quiver(gps.x/1e3,gps.y/1e3,scale*(gps.ve-m.ve),scale*(gps.vn-m.vn),0,'k')
unicycle.plot.ellipse(scale*gps.se*2,scale*gps.sn*2,0*gps.x,gps.x/1e3,gps.y/1e3);

%quiver(gps.x/1e3,gps.y/1e3,1*scale*m.dtc(1:dgf:end),1*scale*m.dtc(2:dgf:end),0,'b-.');

seg.plotPatch();

set(gca,'xlim',[-400 600],'ylim',[-400 700])
set(gca,'clim',[-1 1]*max(abs(get(gca,'clim')))/4e1)
xlabel('East (km)')
ylabel('North (km)')
handle=colorbar();
ylabel(handle,'Uplift rate (mm/yr)');
legend('Borders','Faults','Residual uplift','Residual horizontal','Location','SouthWest');


%% GPS DATA AND INVERSE MODEL
figure(4);clf;set(gcf,'Name',sprintf('Data, Model, Variance reduction: %f%%',m.vr*100))
addToolbarExplorationButtons(gcf)
scale=2e3;
box on, axis equal, hold on
%shz.plotEdges();

% project strain in a specific direction
%A=-[0 1;1 0];
%proj=m.e11*A(1,1)+m.e12*A(1,2)+m.e12*A(2,1)+m.e22*A(2,2);
%shz.plotEdges(proj);
%shz.plotEdges(-m.e12);
%shz.plotEdges(7e-2*heavi(mod(Lx(:)+2+3*mod(fix(Ly(:)/3),2),6)-2.5))
%shz.plotEdges();
shz.plotEdges(m.e);
%shz.plotEdges(diag(R3(0*shz.N+1:1*shz.N,0*shz.N+1:1*shz.N)));
%shz.plotEdges(diag(R1(1*shz.N+1:2*shz.N,1*shz.N+1:2*shz.N)));
%contour(reshape(shz.xc(:,1),size(Lx))/1e3,reshape(shz.xc(:,2),size(Lx))/1e3,reshape(diag(R3(1*shz.N+1:2*shz.N,1*shz.N+1:2*shz.N)),size(Lx)),[0.04
%    0.04],'k')
%lvc.plotEdges(m.ec);

plot(borders.x/1e3,borders.y/1e3,'k-','LineWidth',1);
%plot(gmrt.x/1e3,gmrt.y/1e3,'k-','LineWidth',0.5);
plot(faults.x/1e3,faults.y/1e3,'-','LineWidth',1,'Color',[1 1 1]*0.5);
plot(cfm5.x/1e3,cfm5.y/1e3,'b-','LineWidth',1);
plot(cont.x/1e3,cont.y/1e3,'r-','LineWidth',2);
plot(mendecino.x/1e3,mendecino.y/1e3,'r-','LineWidth',2);
plot(cascadia.x/1e3,cascadia.y/1e3,'k-','LineWidth',2);
plot(garlock.x/1e3,garlock.y/1e3,'r-','LineWidth',2);
plot(sanJacinto.x/1e3,sanJacinto.y/1e3,'r-','LineWidth',2);
plot(elsinore.x/1e3,elsinore.y/1e3,'r-','LineWidth',2);
plot(oakRidge.x/1e3,oakRidge.y/1e3,'r-','LineWidth',2);
plot(ncedc3.x/1e3,ncedc3.y/1e3,'b.','MarkerSize',4);
plot(scedc3.x/1e3,scedc3.y/1e3,'b.','MarkerSize',4);

%quiver(gps.x/1e3,gps.y/1e3,scale*(gps.ve-m.dtr(1:dgf:end)),scale*(gps.vn-m.dtr(2:dgf:end)),0,'k')
%quiver(gps.x/1e3,gps.y/1e3,scale*(m.ve-m.dtr(1:dgf:end)),scale*(m.vn-m.dtr(2:dgf:end)),0,'r-.')

%quiver(gps.x/1e3,gps.y/1e3,scale*gps.ve,scale*gps.vn,0,'k');
%quiver(gps.x/1e3,gps.y/1e3,scale*m.ve,scale*m.vn,0,'r-.');

%plot(-m.m(2)/m.m(1)/1e3,m.m(3)/m.m(1)/1e3,'ko','MarkerFaceColor','k');
set(gca,'xlim',[-400 600],'ylim',[-400 700]);
%set(gca,'clim',[0 1]*max(abs(get(gca,'clim'))));
%set(gca,'clim',[-1 1]*7e-2)
set(gca,'clim',[-1 1]/4)
xlabel('East (km)')
ylabel('North (km)')
hbar=colorbar();
colormap(mma);
ylabel(hbar,'Microstrain / year');
legend('Strain-rate','','Long-Valley Caldera','','Borders','Faults','GPS','Model','Location','SouthWest');


%% GPS DATA AND INVERSE MODEL
figure(5);clf;set(gcf,'Name',sprintf('Data, Model, Variance reduction: %f%%',m.vr*100))
addToolbarExplorationButtons(gcf)
scale=2e3;
box on, axis equal, hold on

flt.plotPatch(-flt.slip);
seg.plotPatch(-seg.slip);

plot(borders.x/1e3,borders.y/1e3,'k-','LineWidth',1);
%plot(gmrt.x/1e3,gmrt.y/1e3,'k-','LineWidth',0.5);
plot(faults.x/1e3,faults.y/1e3,'-','LineWidth',1,'Color',[1 1 1]*0.5);
plot(cfm5.x/1e3,cfm5.y/1e3,'b-','LineWidth',1);
plot(cont.x/1e3,cont.y/1e3,'r-','LineWidth',2);
plot(mendecino.x/1e3,mendecino.y/1e3,'r-','LineWidth',2);
plot(cascadia.x/1e3,cascadia.y/1e3,'k-','LineWidth',2);
plot(garlock.x/1e3,garlock.y/1e3,'r-','LineWidth',2);
plot(sanJacinto.x/1e3,sanJacinto.y/1e3,'r-','LineWidth',2);
plot(elsinore.x/1e3,elsinore.y/1e3,'r-','LineWidth',2);
plot(oakRidge.x/1e3,oakRidge.y/1e3,'r-','LineWidth',2);
plot(ncedc3.x/1e3,ncedc3.y/1e3,'b.','MarkerSize',4);
plot(scedc3.x/1e3,scedc3.y/1e3,'b.','MarkerSize',4);

quiver(gps.x/1e3,gps.y/1e3,scale*(gps.ve-m.dtr(1:dgf:end)),scale*(gps.vn-m.dtr(2:dgf:end)),0,'k')
quiver(gps.x/1e3,gps.y/1e3,scale*(m.ve-m.dtr(1:dgf:end)),scale*(m.vn-m.dtr(2:dgf:end)),0,'r-.')

%quiver(gps.x/1e3,gps.y/1e3,scale*gps.ve,scale*gps.vn,0,'k');
%quiver(gps.x/1e3,gps.y/1e3,scale*m.ve,scale*m.vn,0,'r-.');

%plot(-m.m(2)/m.m(1)/1e3,m.m(3)/m.m(1)/1e3,'ko','MarkerFaceColor','k');
set(gca,'xlim',[-400 600],'ylim',[-400 700]);
%set(gca,'clim',[0 1]*max(abs(get(gca,'clim'))));
set(gca,'clim',[-1 1]/10)
xlabel('East (km)')
ylabel('North (km)')
hbar=colorbar();
colormap(mma);
ylabel(hbar,'Microstrain / year');
legend('Strain-rate','','Long-Valley Caldera','','Borders','Faults','GPS','Model','Location','SouthWest');


%% ANELASTIC STRAIN ORIENTATION
figure(6);clf;set(gcf,'Name',sprintf('Strain-rate orientation'))
addToolbarExplorationButtons(gcf)
scale=5e1;
box on, axis equal, hold on

plot(borders.x/1e3,borders.y/1e3,'k-','LineWidth',1)
plot(faults.x/1e3,faults.y/1e3,'r-','LineWidth',1)
scale=1e3;
[interp.sx,interp.sy]=meshgrid(-400:20:600, -400);
handle=streamline(stream2(interp.x/1e3,interp.y/1e3,reshape(interp.dt(1:dgf:end),size(interp.x)),reshape(interp.dt(2:dgf:end),size(interp.x)),interp.sx,interp.sy));
set(handle,'Color','k');
[interp.sx,interp.sy]=meshgrid(-400:20:600, 700);
handle=streamline(stream2(interp.x/1e3,interp.y/1e3,reshape(interp.dt(1:dgf:end),size(interp.x)),reshape(interp.dt(2:dgf:end),size(interp.x)),interp.sx,interp.sy));
set(handle,'Color','k');
quiver(interp.x(:)/1e3,interp.y(:)/1e3,scale*interp.dt(1:dgf:end),scale*interp.dt(2:dgf:end),0,'k')
%quiver(gps.x(:)/1e3,gps.y(:)/1e3,scale*m.dt(1:dgf:end),scale*m.dt(2:dgf:end),0,'k')
quiver(gps.x/1e3,gps.y/1e3,scale*(m.ve-m.dtr(1:dgf:end)),scale*(m.vn-m.dtr(2:dgf:end)),0,'k-.')
%quiver(shz.xc(:,1)/1e3+scale*m.p1(:,1),shz.xc(:,2)/1e3+scale*m.p1(:,2),-scale*m.p1(:,1),-scale*m.p1(:,2),0,'k')
%quiver(shz.xc(:,1)/1e3-scale*m.p1(:,1),shz.xc(:,2)/1e3-scale*m.p1(:,2),scale*m.p1(:,1),scale*m.p1(:,2),0,'k')
%quiver(shz.xc(:,1)/1e3,shz.xc(:,2)/1e3,scale*m.p2(:,1),scale*m.p2(:,2),0,'k')
%quiver(shz.xc(:,1)/1e3,shz.xc(:,2)/1e3,-scale*m.p2(:,1),-scale*m.p2(:,2),0,'k')

set(gca,'xlim',[-400 600],'ylim',[-400 700])
set(gca,'clim',[0 1]*max(abs(get(gca,'clim'))))
xlabel('East (km)')
ylabel('North (km)')
legend('Borders','Faults','Principal strain','Location','SouthWest')

%% EARTHQUAKES MOMENT TENSORS AND INVERSE MODEL
figure(7);clf;set(gcf,'Name',sprintf('Data, Model, Variance reduction: %f%%',m.vr*100))
addToolbarExplorationButtons(gcf)
scale=2e3;
box on, axis equal, hold on
%shz.plotEdges();
%shz.plotEdges(-m.e12);
shz.plotEdges(m.e);

plot(borders.x/1e3,borders.y/1e3,'k-','LineWidth',1);
plot(faults.x/1e3,faults.y/1e3,'-','LineWidth',1,'Color',[1 1 1]*0.5);
plot(cont.x/1e3,cont.y/1e3,'r-','LineWidth',2);
plot(mendecino.x/1e3,mendecino.y/1e3,'r-','LineWidth',2);
plot(cascadia.x/1e3,cascadia.y/1e3,'k-','LineWidth',2);
plot(garlock.x/1e3,garlock.y/1e3,'r-','LineWidth',2);
plot(sanJacinto.x/1e3,sanJacinto.y/1e3,'r-','LineWidth',2);
plot(elsinore.x/1e3,elsinore.y/1e3,'r-','LineWidth',2);

plot(scedc.x/1e3,scedc.y/1e3,'rp','LineWidth',1);
text(scedc.x/1e3,scedc.y/1e3,string(scedc.year));
plot(ncedc.x/1e3,ncedc.y/1e3,'bp','LineWidth',1);
text(ncedc.x/1e3,ncedc.y/1e3,string(ncedc.year));

set(gca,'xlim',[-500 600],'ylim',[-400 700]);
%set(gca,'clim',[0 1]*max(abs(get(gca,'clim'))));
set(gca,'clim',[-1 1]/3)
xlabel('East (km)')
ylabel('North (km)')
hbar=colorbar();
colormap(mma);
ylabel(hbar,'Microstrain / year');
legend('Strain-rate','','Long-Valley Caldera','','Borders','Faults','GPS','Model','Location','SouthWest');

%% cumulative strain across California

figure(8);clf;set(gcf,'Name',sprintf('Relative plate motion %f mm/yr',mean(sum(reshape(-m.e12,size(Lx))*20,2))))
addToolbarExplorationButtons(gcf)
cla, box on, hold on, grid on

errorbar(Ly(:,1)*20,2*sum(reshape(-m.e12,size(Lx))*20,2),0.05*sum(reshape(-m.e12,size(Lx))*20,2),'k')
%errorbar(Ly(:,1)*20,2*sum(reshape(7e-2*heavi(mod(Lx(:)+2+3*mod(fix(Ly(:)/3),2),6)-2.5),size(Lx))*20,2),0.05*sum(reshape(-m.e12,size(Lx))*20,2),'k');
%errorbar(Ly(:,1)*20,2*sum(reshape((7e-2*heavi(mod(Lx(:)+3+4*mod(fix(Ly(:)/4),2),8)-3.5)),size(Lx))*20,2),0.05*sum(reshape(-m.e12,size(Lx))*20,2),'k');

%plot(Ly(:,1)*20,sum(reshape(m.e22,size(Lx))*20,2),'r')
plot(get(gca,'xlim'),[1 1]*mean(relativePlateMotion),'k');
plot(get(gca,'xlim'),[1 1]*(mean(relativePlateMotion)+std(relativePlateMotion)));
plot(get(gca,'xlim'),[1 1]*(mean(relativePlateMotion)-std(relativePlateMotion)))
set(gca,'XDir','Reverse')
legend('Plate-parallel displacement')
xlabel('Plate-parallel distance')

%% comparison with UCERF3

figure(9);clf;set(gcf,'Name',sprintf('Relative plate motion %f mm/yr',mean(sum(reshape(-m.e12,size(Lx))*20,2))))
addToolbarExplorationButtons(gcf)
subplot(1,1,1); cla, box on, axis equal, hold on

plot(borders.x/1e3,borders.y/1e3,'k-','LineWidth',1);
plot(faults.x/1e3,faults.y/1e3,'-','LineWidth',1,'Color',[1 1 1]*0.5);
plot(cont.x/1e3,cont.y/1e3,'r-','LineWidth',2);
plot(mendecino.x/1e3,mendecino.y/1e3,'r-','LineWidth',2);
plot(cascadia.x/1e3,cascadia.y/1e3,'k-','LineWidth',2);
plot(garlock.x/1e3,garlock.y/1e3,'r-','LineWidth',2);
plot(sanJacinto.x/1e3,sanJacinto.y/1e3,'r-','LineWidth',2);
plot(elsinore.x/1e3,elsinore.y/1e3,'r-','LineWidth',2);

ucerf3.i=fix(((ucerf3.x-shz.levels{1}.x(1))*shz.levels{1}.sv(1)+(ucerf3.y-shz.levels{1}.x(2))*shz.levels{1}.sv(2))/shz.L(1))+1;
ucerf3.j=fix(((ucerf3.x-shz.levels{1}.x(1))*shz.levels{1}.nv(1)+(ucerf3.y-shz.levels{1}.x(2))*shz.levels{1}.nv(2))/shz.T(1)+size(Lx,2)/2);

scatter(ucerf3.x/1e3,ucerf3.y/1e3,200,ucerf3.rate,'filled','MarkerEdgeColor','k');
%plot(ucerf3.x/1e3,ucerf3.y/1e3,'ko')

for i=1:length(ucerf3.x)
    shz.plotEdgesById(ucerf3.j(i)*size(Lx,1)+ucerf3.i(i));
end

set(gca,'xlim',[-500 600],'ylim',[-400 700]);
colormap(flipud(sunset));
colorbar
xlabel('East (km)')
ylabel('North (km)')

subplot(1,1,1);cla; axis equal; box on
pos=ucerf3.j*size(Lx,1)+ucerf3.i;
ucerf3.sv=[sind(ucerf3.str+39),cosd(ucerf3.str+39)];
ucerf3.nv=[sind(ucerf3.str+39+90),cosd(ucerf3.str+39+90)];
ucerf3.dt=(m.e11(pos).*ucerf3.sv(:,1)+m.e12(pos).*ucerf3.sv(:,2)).*ucerf3.nv(:,1) ...
         +(m.e12(pos).*ucerf3.sv(:,2)+m.e22(pos).*ucerf3.sv(:,2)).*ucerf3.nv(:,2);

set(gca, 'XScale','log', 'YScale','log')
axis equal 
set(gca,'xlim',[1e-1,1e2],'ylim',[1e-1,1e2])
%loglog(-2*m.e12(pos)*20,ucerf3.rate,'+')
%hold on
%plot(abs(-2*ucerf3.dt*20),ucerf3.rate,'k^')
hold on
pos=find(abs(-2*ucerf3.dt*20)<=ucerf3.max & abs(-2*ucerf3.dt*20)>=ucerf3.min);
errorbar(ucerf3.rate,abs(-2*ucerf3.dt*20),0.2*abs(-2*ucerf3.dt*20),0.2*abs(-2*ucerf3.dt*20),ucerf3.rate-ucerf3.min,ucerf3.max-ucerf3.rate,'k+')
set(gca, 'XScale','log', 'YScale','log')
plot([1e-1 30],[1e-1 30])
text(ucerf3.rate,abs(-2*ucerf3.dt*20),string(ucerf3.name),'HorizontalAlignment','Center','FontSize',6)
ylabel('Strain-rate')
xlabel('UCERF3')

%% EXPORT
%
% fid=fopen('gmt/gps-data-model-res_ll.dat','wt');fprintf(fid,'%f %f %f %f %f %f %f %f %f %f %f %f %f %f\n',[gps.lon,gps.lat,gps.ve-m.dtr(1:dgf:end),gps.vn-m.dtr(2:dgf:end),-gps.vd,gps.se,gps.sn,gps.sd,m.ve-m.dtr(1:dgf:end),m.vn-m.dtr(2:dgf:end),m.vu,gps.ve-m.ve,gps.vn-m.vn,-gps.vd-m.vu]');fclose(fid);
%
% shz.exportXYZ(1,'gmt/shz-m6-e12_m.xyz',-m.e12)
%
% fid=fopen('gmt/shz-m6-strain_m.dat','wt');fprintf(fid,'%f %f %f %f %f\n',[shz.xc(:,1),shz.xc(:,2),m.eps1,m.eps2,m.theta*180/pi+90]');fclose(fid);