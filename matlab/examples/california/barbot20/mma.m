function c = mma(m)
% MMA(m) diverging red/white/blue m-by-3 color map matrix.
%
% EXAMPLE:
%
%    colormap(mma)
%
% SEE ALSO: HSV, GRAY, HOT, BONE, COPPER, PINK, FLAG, COLORMAP, RGBPLOT.

if nargin < 1, m = size(get(gcf,'colormap'),1); end

cpt=[
0.      46      78      238
0.1     86      119     240
0.2     135     160     244
0.3     191     205     249
0.4     233     238     253
0.5     255     255     255
0.6     254     253     170
0.7     249     239     94
0.8     236     189     66
0.9     221     125     54
];

x=(0:(m-1))'/(m-1)*0.9;

r=interp1(cpt(:,1),cpt(:,2),x)/255;
g=interp1(cpt(:,1),cpt(:,3),x)/255;
b=interp1(cpt(:,1),cpt(:,4),x)/255;

c = [r g b]; 

