function c = sunset(m)
% SUNSET shades of red and blue color map
%   SUNSET(M), is an M-by-3 matrix that defines a colormap.
%   The colors begin with bright blue, range through shades of
%   blue to white, and then through shades of red to bright red.
%
%   SUNSET is the same length as the current figure's
%   colormap. If no figure exists, MATLAB creates one.
%
%   For example, to reset the colormap of the current figure:
%
%             colormap(sunset)
%
%   See also HSV, GRAY, HOT, BONE, COPPER, PINK, FLAG, 
%   COLORMAP, RGBPLOT.

if nargin < 1, m = size(get(gcf,'colormap'),1); end

cpt=[
0.      0       0       0  
0.1     57      21      77 
0.2     116     41      117
0.3     180     60      81 
0.4     221     86      47 
0.5     250     115     13 
0.6     253     151     25 
0.7     255     184     52 
0.8     255     215     107
0.9     255     237     177
];

x=(0:(m-1))'/(m-1)*0.9;

r=interp1(cpt(:,1),cpt(:,2),x)/255;
g=interp1(cpt(:,1),cpt(:,3),x)/255;
b=interp1(cpt(:,1),cpt(:,4),x)/255;

c = [r g b]; 

