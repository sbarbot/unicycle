
% creates a mesh based on the megathrust geometry of Hubbard et al. (2016)
% with denser mesh elements in the Katmandu segment.

minmax=@(x) [min(x(:)),max(x(:))];

%% load contours
[~,my,mx,md]=textread('faults/qiu+15_1_receiver.ned','%d %f %f %f','commentstyle','shell');
mx=mx/1e3;
my=my/1e3;
md=md/1e3;
shrinkFactor=1;
pos=boundary(mx,my,shrinkFactor);
xc=mx(pos);
yc=my(pos);

%% create triangular mesh
jump=4;
pv=[xc(1:jump:end),yc(1:jump:end)];
tic
%[p,t]=distmesh.distmesh2d(@distmesh.dpoly,@distmesh.huniform,2,[-600,-1000; 2000,400],pv,pv);
azimuth=17;
fh=@(p,s) 1-0.5*unicycle.utils.omega((cosd(azimuth)*p(:,1)-sind(azimuth)*p(:,2)-60)/250,0.35);
[p,t]=distmesh.distmesh2d(@distmesh.dpoly,fh,1,[-200,-200; 300,200],pv,pv);
toc

%%

distmesh.simpplot(p,t)
F=scatteredInterpolant(mx,my,md,'natural');
zo=F(p(:,1),p(:,2));

prefix='faults/nepal_adaptive_1km_m';

fid=fopen([prefix '.tri'],'wt');
fprintf(fid,'%d %d %d %d 90\n',[(1:size(t,1))',t]');
fclose(fid);

fid=fopen([prefix '.ned'],'wt');
fprintf(fid,'%d %e %e %e\n',[(1:size(p,1))',p(:,2)*1e3,p(:,1)*1e3,zo*1e3]');
fclose(fid);

%% plot

rcv=unicycle.geometry.triangleReceiver(prefix,unicycle.greens.nikkhoo15(30e3,0.25));

figure(1);clf;hold on
rcv.plotPatch(-rcv.xc(:,3)/1e3);
rcv.plotPatch();
plot(pv(:,1)*1e3,pv(:,2)*1e3,'lineWidth',2);
view([130,30]);
colorbar
axis equal tight
box on
