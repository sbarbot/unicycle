% % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                       %
%            SEAS Benchmark Problem BP1                 %
%                                                       %
% DESCRIPTION:                                          %
% This initial benchmark considers a planar fault       %
% embedded in a homogeneous, linear elastic half-space  %
% with a free surface. The fault is governed by         %
% rate-and-state friction down to the depth Wf and      %
% creeps at an imposed constant rate Vp down to the     %
% infinite depth. The simulations will include the      %
% nucleation, propagation, and arrest of earthquakes,   %
% and aseismic slip in the post- and inter-seismic      %
% periods.                                              %
%                                                       %
% AUTHOR:                                               %
% Sylvain Barbot (April, 2018)                          %
%                                                       %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % %

clear all

% minimum and maximum function
minmax=@(x) [min(x(:)),max(x(:))];

% boxcar function
boxc=@(x) (x+0.5>=0)-(x-0.5>=0);

% Heaviside function
heavi=@(x) 0+x>=0;

% ramp function
ramp=@(x) x.*boxc(x-1/2)+heavi(x-1);

% % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                       %
%            P H Y S I C A L   M O D E L                %
%                                                       %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % %

% density (kg/m^3)
rho=2670;

% shear wave speed (m/s)
Vs=3464;

% rigidity (MPa)
G=rho*Vs^2/1e6;

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%                        M E S H                       %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % %  %

%                         0       
% _______________________________________________ 0 km   +--------> x2
%                         |                              |
%                         |                              |
%                         |      Brittle                 |
%                         |                              V
%                         |                              x3
%                         | fault        
%                         |
%                         |
%                         + 40 km
%

% fault mesh
y3=0e3; % fault starting depth (m)
y2=0e3; % fault horizontal position (m)

% fault tip (m)
transition=40e3;

% number of fault patches
flt.M=1600;

dz=transition/flt.M;
fpoles=y3+(0:flt.M)'*dz;

% top of fault patches
flt.y3=fpoles(1:end-1); 
% width of fault patches
Wf=ones(flt.M,1)*dz; 
  
%% stress kernels for patch interactions

flt.K=zeros(flt.M,flt.M);

% stress kernels for fault slip
s12h=@(x2,x3,y2,y3,Wf) G*( ...
    -(x3-y3)./((x2-y2).^2+(x3-y3).^2)+(x3+y3)./((x2-y2).^2+(x3+y3).^2) ...
    +(x3-y3-Wf)./((x2-y2).^2+(x3-y3-Wf).^2)-(x3+y3+Wf)./((x2-y2).^2+(x3+y3+Wf).^2) ...
    )/2/pi;

for k=1:flt.M
    flt.K(:,k)=s12h(y2,flt.y3+dz/2,y2,flt.y3(k),Wf(k));
end

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%         F R I C T I O N   P A R A M E T E R S        %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

% effective confining pressure on fault (MPa)
flt.sigmab = 50;

% frictional parameters ( velocity-weakening friction, a-b < 0 )
flt.a = 1e-2+ramp((flt.y3-15e3)/3e3)*(0.025-0.01);
flt.b = 0.015*ones(size(flt.y3));

% static friction coefficient
flt.mu0 = 0.6*ones(size(flt.y3));

% characteristic weakening distance (m)
flt.L = 8e-3*ones(size(flt.y3));

% plate velocity (m/s)
flt.Vpl = 1e-9*ones(size(flt.y3));

% reference slip rate (m/s)
flt.Vo = 1e-6*ones(size(flt.y3));

% radiation damping term
flt.damping=G/Vs/2;

% plot strength profiles
if false
    figure(1);clf;cla; hold on
    %plot(flt.y3/1e3,flt.strength)
    plot(flt.a,flt.y3/1e3,'k')
    plot(flt.b,flt.y3/1e3,'k--')
    plot(flt.a-flt.b,flt.y3/1e3,'r')
    plot(0*flt.a,flt.y3/1e3,'k-.')
    ylabel('Depth (km)')
    xlabel('a');
    set(gca,'YDir','Reverse');
    set(gca,'xlim',[-0.02 0.035])
    box on
    return
end

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                       %
%         N U M E R I C A L   S O L U T I O N           %
%                                                       %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % %

% state parameters
flt.dgf=4;

% initialize state vector
Y0=zeros(flt.M*flt.dgf,1);

% initial slip
Y0(1:flt.dgf:end) = zeros(size(flt.y3));
% initial shear stress
Y0(2:flt.dgf:end) = max(flt.a)*flt.sigmab.*asinh(flt.Vpl./flt.Vo/2.*exp((flt.mu0+flt.b.*log(flt.Vo./flt.Vpl))./max(flt.a)));
% initial state variable
Y0(3:flt.dgf:end) = flt.a./flt.b.*log(2*flt.Vo./flt.Vpl.*sinh((Y0(2:flt.dgf:end))./flt.a./flt.sigmab))-flt.mu0./flt.b;
% initial velocity
Y0(4:flt.dgf:end) = log(flt.Vpl./flt.Vo);

% initialize the function handle with
% set constitutive parameters
yp=@(t,y) BP1_ode(t,y,flt);
tic
% four/fifth order Runge-Kutta solver
options=odeset('Refine',1,'RelTol',3e-7,'InitialStep',1e-3,'MaxStep',3e6); 
[t,Y]=ode45(yp,[0 3000*3.15e7],Y0,options);
toc

%% post-processing

V=Y(:,4:flt.dgf:end)';
Vmax=max(V,[],1);

%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                      %
%                    F I G U R E S                     %
%                                                      %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %%

if false

    figure(1);clf;cla; hold on
    % function of time
    f2 = figure(2);clf;set(gcf,'name','Time Evolution')
    f2a = subplot(3,1,1);cla;
    %pcolor(t/3.15e7,flt.y3/1e3,(V)/log(10)-6), shading flat
    set(gca,'YDir','reverse');

    h=colorbar('Location','NorthOutside');
    caxis([-12 1]);
    colormap(f2a,parula);
    title(h,'Slip Rate (m/s)')
    xlabel('Time (yr)')
    ylabel('Depth (km)');

    f2c = subplot(3,1,2);cla;
    plot(t/3.15e7,(Vmax)/log(10)-6)
    xlabel('Time (Yr)')
    ylabel('Velocity (m/s) log10')
    title('Maximum slip rate on fault')

    f2d = subplot(3,1,3);cla;
    %plot(t(1:end-1)/3.15e7,log10(V(:,floor((top+bottom)/2))))
    xlabel('Time (Yr)')
    ylabel('Velocity (m/s) log10')
    title('Time series at center of seismogenic zone')

    %% function of time steps
    f3 = figure(3);clf;set(gcf,'name','Time Step Evolution')

    f3a = subplot(3,1,1);cla;
    %pcolor(1:length(t),flt.y3/1e3,V/log(10)-6), shading flat
    set(gca,'YDir','reverse');

    h=colorbar('Location','NorthOutside');
    caxis([-12 1]);
    colormap(f3a,parula);
    title(h,'Slip Rate (m/s)')
    xlabel('Time Steps')
    ylabel('Depth (km)');

    f3c = subplot(3,1,3);cla;
    plot(1:length(t),(Vmax)/log(10)-6)
    xlabel('Time Steps')
    ylabel('Velocity (m/s) log10')
    title('Maximum slip rates on fault')

end

%% export to file

skip=1;

for k=1:16
    % t - Time (s)
    % slip - Out-of-plane slip (m). Positive for right-lateral motion.
    % slip_rate - log10 of the out-of-plane slip-rate (log10 m/s). Positive for right-lateral motion.
    % shear_stress - Shear stress (MPa). Positive for shear stress that tends to cause right-lateral motion.
    % state - log10 of state variable (log10 s).
    depth=(k-1)*2.5e3;
    fid=fopen(sprintf('fltst_dp%03d',depth/100),'wt');
    patch=1+floor(depth/(flt.y3(2)-flt.y3(1)));
    fprintf(fid, ...
        ['# Benchmark problem (No.1)\n', ...
         '# Author: Sylvain Barbot\n', ...
         '# Date: April 12, 2018\n', ...
         '# Code: Matlab with ode45\n', ...
         '# Code version: N/A\n', ...
         '# Node spacing or element size: 100 m\n', ...
         sprintf('# Minimum time step: %e\n',min(t(2:end)-t(1:end-1))), ...
         sprintf('# Maximum time step: %e\n',max(t(2:end)-t(1:end-1))), ...
         sprintf('# Number of time steps in file: %d\n',length(t(1:skip:end))), ...
         sprintf('# Output subsample rate: %d\n',skip), ...
         sprintf('# Station location (depth): %f\n',depth), ...
         '# t - Time (s)\n', ...
         '# slip - Out-of-plane slip (m). Positive for right-lateral motion.\n', ...
         '# slip_rate - log10 of the out-of-plane slip-rate (log10 m/s). Positive for right-lateral motion.\n', ...
         '# shear_stress - Shear stress (MPa). Positive for shear stress that tends to cause right-lateral motion.\n', ...
         '# state - log10 of state variable (log10 s).\n']);
    fprintf(fid,'t slip slip_rate shear_stress state\n');
    fprintf(fid, ...
        '%25.18e %21.14e %21.14e %21.14e %21.14e\n', ...
        [t(1:skip:end), ...
         Y(1:skip:end,1+(patch-1)*flt.dgf), ...
         Y(1:skip:end,4+(patch-1)*flt.dgf)/log(10)-6, ...
         Y(1:skip:end,2+(patch-1)*flt.dgf), ...
         log10(flt.L(patch)/flt.Vo(patch)*exp(Y(1:skip:end,3+(patch-1)*flt.dgf))), ...
        ]');
    fclose(fid);
end




