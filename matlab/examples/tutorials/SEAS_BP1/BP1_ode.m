 function [Yp]= BP1_ode(~,Y,flt)            
% function odeViscoelastic describes the evolution of the ordinary
% differential equation y' = f(t,y), where the state vector y is 
%
%        /        s          \            
%        |       tau         |         
%        | log(theta Vo / L) |           
%        |   log( V / Vo )   |
%    y = |       ...         |
%        |        s          |            
%        |       tau         |         
%        | log(theta Vo / L) |           
%        \   log( V / Vo )   /
%
% Instead of integrating numerically the aging law
%
%    d theta / dt = 1 - V theta / L
%
% as is, we operate the following change of variable
%
%    phi = ln (theta Vo / L)
%
% Considering the following relationships
%
%    d phi = d theta / theta
%
%    d theta = d phi theta = d phi exp(phi) L / Vo
%
%    d theta / dt = d phi exp(phi) / dt L / Vo
%
%    d phi exp(phi) / dt L / Vo = 1 - V theta / L = 1 - V exp(phi) / Vo
%
% we obtain the evolution law for the new variable
% 
%    d phi / dt = ( Vo exp(-phi) - V ) / L
%
% With the asinh formulation of rate-state friction the acceleration
% is given by
%
%    1 d V    K (V - Vpl) - b sigma d phi / dt Q
%    - --- = ------------------------------------
%    V  dt         a sigma Q  + damping V
%
% where 
%
%                            1
%    Q = -----------------------------------------------
%        /                                             \(1/2)
%        |  1 + [2 Vo / V exp(-(mu + b phi) / a )]^2   |
%        \                                             /
%
% For the additive form of rate-state friction, Q=1.

% state variable
th   = Y(3:flt.dgf:end);

% slip velocity 
V    = flt.Vo.*exp(Y(4:flt.dgf:end));

% initiate state derivative
Yp=zeros(size(Y));  

% slip velocity
Yp(1:flt.dgf:end) = V;

% rate of state (rate of log(theta/theta0))
dth=(flt.Vo.*exp(-th)-V)./flt.L;
Yp(3:flt.dgf:end) = dth;

% acceleration (rate of log(V/Vo))
kv = flt.K*(V-flt.Vpl);
reg=2*flt.Vo./V.*exp(-(flt.mu0+flt.b.*th)./flt.a);
reg=1./sqrt(1+reg.^2);
Yp(4:flt.dgf:end) = (kv - flt.b.*flt.sigmab.*dth.*reg)./ ...
                    (flt.a.*flt.sigmab.*reg + flt.damping.*V);

% shear stress rate
Yp(2:flt.dgf:end) = kv - flt.damping.*V.*Yp(4:flt.dgf:end);

end
