
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % %
%
%         M A N T L E   W E D G E   D Y N A M I C S
%
% % % % % % % % % % % % % % % % % % % % % % % % % % % % % %

clear all

% Poisson's ratio
nu=0.25;
rig=30e3;

minmax=@(x) [min(x(:)),max(x(:))];
heaviside=@(x) x>=0;

%% geometry

% megathrust fault 
A=[  0;    0]*1e3;
B=[300;   30]*1e3;

% oceanic mantle
C=[-100;   50]*1e3;
D=[ 250;   50]*1e3;
E=[ 650;  276]*1e3;
F=[-100;  276]*1e3;

% observation points
obs.x=([1;0]*(-100:5:600))'*1e3;
obs.N=size(obs.x,1);
obs.dgf=2; % (two degrees of freedom: east and down displacement)

% strain-rate profile
ocean.profile.N=100;
ocean.profile.p=(repmat([150;100]*1e3,1,ocean.profile.N)+([0;100]*1e3)*(0:(ocean.profile.N-1))/ocean.profile.N)';

% mantle wedge
G=[300;   30]*1e3;
H=[700;   30]*1e3;
J=[700;  256]*1e3;

% strain-rate profile
wedge.profile.N=100;
wedge.profile.p=(repmat([300;30]*1e3,1,wedge.profile.N)+([400;200]*1e3)*(0:(wedge.profile.N-1))/wedge.profile.N)';


%% triangular mesh for strain volumes

% asthenosphere geometry (pv must be a closed loop)
pv=[C,D,E,F,C]';

ocean.meshSize=20e3;

figure(1);clf;set(gcf,'name','Mesh optimization (distmesh)')
[ocean.p,ocean.t]=distmesh.distmesh2d(@distmesh.dpoly,@distmesh.huniform,ocean.meshSize, ...
    [min(pv(:,1)),min(pv(:,2)); max(pv(:,1)),max(pv(:,2))],pv,pv);

ocean.N=size(ocean.t,1);

ocean.xc=[(ocean.p(ocean.t(:,1),1)+ocean.p(ocean.t(:,2),1)+ocean.p(ocean.t(:,3),1))/3, ...
          (ocean.p(ocean.t(:,1),2)+ocean.p(ocean.t(:,2),2)+ocean.p(ocean.t(:,3),2))/3];

% asthenosphere physical properties

% reference strain-rate (1/MPa/s)
ocean.diffusion.A    = 1e6*ones(ocean.N,1);
% activation energy wet olivine (J/mol)
ocean.diffusion.Q    = 335e3*ones(ocean.N,1);
% activation volume (m^3/mol)
ocean.diffusion.volume  = 4e-6*ones(ocean.N,1);  
% grain size (�m) for diffusion creep
ocean.diffusion.d       = 1e4*ones(ocean.N,1);
% grain size sensitivity exponent
ocean.diffusion.pexp    = 3*ones(ocean.N,1);
% water content sensitivity exponent
ocean.diffusion.r       = 1.0*ones(ocean.N,1);

% reference strain-rate (for stress in MPa)
ocean.dislocation.A = 90*ones(ocean.N,1);
% power-law exponent for dislocation creep
ocean.dislocation.n = 3.5*ones(ocean.N,1);
% exponent for water content sensitivity
ocean.dislocation.r = 1.0*ones(ocean.N,1);
% activation energy wet olivine (J/mol)
ocean.dislocation.Q = 480e3*ones(ocean.N,1);
% activation volume (m^3/mol)
ocean.dislocation.volume = 11e-6*ones(ocean.N,1);

% water fugacity (H ppm Si)
ocean.COH     = 1000*ones(ocean.N,1);


% temperature (K) and confining pressure (Pa)
ocean.k       = 3.138;         % thermal conductivity (W/m/K)
ocean.Cp      = 1171 ;         % specific heat (J/kg/K)
ocean.Rm      = 3330 ;         % mantle density (kg/m^3)
ocean.Kappa   = ocean.k/(ocean.Rm*ocean.Cp); % Thermal diffusivity (m^2/s)
ocean.t_plate = 2e15;          % plate age (s)
ocean.Tprof   = 273+1400*erf(ocean.xc(:,2)/(sqrt(4*ocean.Kappa*ocean.t_plate)));  % (Kelvin)
ocean.Pconf   = ocean.Rm*9.8*ocean.xc(:,2);

% effective viscosity for diffusion creep (MPa s)
ocean.diffusion.eta=1./(ocean.diffusion.A.*ocean.COH.^(ocean.diffusion.r).*ocean.diffusion.d.^(-ocean.diffusion.pexp) ...
    .*exp(-(ocean.diffusion.Q+ocean.Pconf.*ocean.diffusion.volume)./(8.314.*ocean.Tprof)));

% reference strain rate for dislocation creep (MPa^(-n) / s)
ocean.dislocation.e0=ocean.dislocation.A.*ocean.COH.^(ocean.dislocation.r) ...
    .*exp(-(ocean.dislocation.Q+ocean.Pconf.*ocean.dislocation.volume)./(8.314.*ocean.Tprof));

% background strain rate tensor (1 / s)
ocean.e22pl=ones(ocean.N,1)*(-1e-15);
ocean.e23pl=zeros(ocean.N,1);
ocean.e33pl=zeros(ocean.N,1);

%% wedge geometry (pv must be a closed loop)
pv=[G,H,J,G]';

wedge.meshSize=20e3;

figure(1);clf;set(gcf,'name','Mesh optimization (distmesh)')
[wedge.p,wedge.t]=distmesh.distmesh2d(@distmesh.dpoly,@distmesh.huniform,wedge.meshSize, ...
    [min(pv(:,1)),min(pv(:,2)); max(pv(:,1)),max(pv(:,2))],pv,pv);

wedge.N=size(wedge.t,1);

wedge.xc=[(wedge.p(wedge.t(:,1),1)+wedge.p(wedge.t(:,2),1)+wedge.p(wedge.t(:,3),1))/3, ...
          (wedge.p(wedge.t(:,1),2)+wedge.p(wedge.t(:,2),2)+wedge.p(wedge.t(:,3),2))/3];

% wedge physical properties

% reference Strain Rate (for stress in MPa)
wedge.diffusion.A       = 1e6*ones(wedge.N,1);
% activation energy wet olivine (J/mol)
wedge.diffusion.Q       = 335e3*ones(wedge.N,1);
% activation volume (m^3/mol)
wedge.diffusion.volume  = 4e-6*ones(wedge.N,1);  
% grain size (�m) for diffusion creep
wedge.diffusion.d       = 1e4*ones(wedge.N,1);
wedge.diffusion.pexp    = 3*ones(wedge.N,1);
% water content sensitivity exponent
wedge.diffusion.r       = 1.0*ones(wedge.N,1);

% reference strain-rate (for stress in MPa)
wedge.dislocation.A = 90*ones(wedge.N,1);
% power-law exponent for dislocation creep
wedge.dislocation.n = 3.5*ones(wedge.N,1);
% exponent for water content sensitivity
wedge.dislocation.r = 1.0*ones(wedge.N,1);
% activation energy wet olivine (J/mol)
wedge.dislocation.Q = 480e3*ones(wedge.N,1);
% activation volume (m^3/mol)
wedge.dislocation.volume = 11e-6*ones(wedge.N,1);

% water fugacity (H ppm Si)
wedge.COH     = 1000+9000*heaviside(400e3-wedge.xc(:,1));

% temperature (K) and confining pressure (Pa)
wedge.k       = 3.138;         % thermal conductivity (W/m/K)
wedge.Cp      = 1171 ;         % specific heat (J/kg/K)
wedge.Rm      = 3330 ;         % mantle density (kg/m^3)
wedge.Kappa   = wedge.k/(wedge.Rm*wedge.Cp); % Thermal diffusivity (m^2/s)
wedge.t_plate = 2.3e15;        % plate age (s)
wedge.Tprof   = 273+1400*erf( ...
    wedge.xc(:,2)./(sqrt(4*wedge.Kappa*wedge.t_plate)));  % (Kelvin)
wedge.Pconf   = wedge.Rm*9.8*wedge.xc(:,2);

% effective viscosity for diffusion creep (MPa s)
wedge.diffusion.eta = 1./(wedge.diffusion.A.*wedge.COH.^(wedge.diffusion.r).*wedge.diffusion.d.^(-wedge.diffusion.pexp) ...
    .*exp(-(wedge.diffusion.Q+wedge.Pconf.*wedge.diffusion.volume)./(8.314.*wedge.Tprof)));

% reference strain rate for dislocation creep (MPa^(-n) /s)
wedge.dislocation.e0 = wedge.dislocation.A.*wedge.COH.^(wedge.dislocation.r) ...
    .*exp(-(wedge.dislocation.Q+wedge.Pconf.*wedge.dislocation.volume)./(8.314.*wedge.Tprof));

% background strain rate tensor (1 / s)
wedge.e22pl=ones(wedge.N,1)*1e-15;
wedge.e23pl=zeros(wedge.N,1);
wedge.e33pl=zeros(wedge.N,1);

close(1);

%% fault segments for megathrust

flt.N=200;

flt.x  =repmat((0:(flt.N-1))'/flt.N,1,2).*repmat((B-A)',flt.N,1);
flt.xc =repmat((0.5+(0:(flt.N-1)))'/flt.N,1,2).*repmat((B-A)',flt.N,1);
flt.dip=atan2(B(2)-A(2),B(1)-A(1))*180/pi*ones(flt.N,1);
flt.W  =norm(B-A)/flt.N*ones(flt.N,1);

flt.dv=-[cosd(flt.dip), sind(flt.dip)];
flt.nv= [sind(flt.dip),-cosd(flt.dip)];

% effective confining pressure on fault (MPa)
flt.sigmab=100;

% frictional parameters ( velocity-weakening friction, a-b < 0 )
flt.a=1e-3*ones(flt.N,1);
flt.b=flt.a-4e-3;

% static friction coefficient
flt.mu0=0.6*ones(flt.N,1);

% characteristic weakening distance (m)
flt.L=5e-1*ones(flt.N,1);

% plate velocity (m/s)
flt.Vpl=1e-9*ones(flt.N,1);

% reference slip rate (m/s)
flt.Vo=1e-6*ones(flt.N,1);

% radiation damping coefficient
flt.damping=5;

% seismogenic zone
sz=find(flt.xc(:,2)>=10e3 & flt.xc(:,2)<20e3);
flt.b(sz)=flt.a(sz)+2e-3;

% slow-slip events
ss=find(flt.xc(:,2)>=23e3 & flt.xc(:,2)<25e3);
flt.b(ss)=flt.a(ss)+1e-3;
flt.L(ss)=5e-2;


%% merge oceanic asthenosphere and mantle wedge

mantle.N=ocean.N+wedge.N;
mantle.p=[ocean.p;wedge.p];
mantle.t=[ocean.t;wedge.t+ones(size(wedge.t))*size(ocean.p,1)];
mantle.xc=[ocean.xc;wedge.xc];
mantle.diffusion.eta=[ocean.diffusion.eta;wedge.diffusion.eta];
mantle.dislocation.e0=[ocean.dislocation.e0;wedge.dislocation.e0];
mantle.dislocation.n=[ocean.dislocation.n;wedge.dislocation.n];
mantle.COH=[ocean.COH;wedge.COH];
mantle.e22pl=[ocean.e22pl;wedge.e22pl];
mantle.e23pl=[ocean.e23pl;wedge.e23pl];
mantle.e33pl=[ocean.e33pl;wedge.e33pl];

if false
    fid=fopen('mesh.xyz','wt');
    for k=1:size(mantle.t,1)
        M=mantle.p(mantle.t(k,1),:)/1e3;
        N=mantle.p(mantle.t(k,2),:)/1e3;
        P=mantle.p(mantle.t(k,3),:)/1e3;
        fprintf(fid,'>\n');
        fprintf(fid,'%e %e\n',M(1),-M(2));
        fprintf(fid,'%e %e\n',N(1),-N(2));
        fprintf(fid,'%e %e\n',P(1),-P(2));
    end
    fclose(fid);
end

%% boundary-element and finite-element geometry

figure(2);clf;set(gcf,'name','Geometry')
subplot(2,1,1);cla;hold on
plot( flt.x(:,1)/1e3,flt.x(:,2)/1e3)
%plot(flt.xc(:,1)/1e3,flt.xc(:,2)/1e3,'+')
sc=1e1;
quiver(flt.xc(:,1)/1e3,flt.xc(:,2)/1e3,sc*flt.nv(:,1),sc*flt.nv(:,2),0)
%triplot(ocean.t,ocean.p(:,1)/1e3,ocean.p(:,2)/1e3)
%triplot(wedge.t,wedge.p(:,1)/1e3,wedge.p(:,2)/1e3)
%triplot(mantle.t,mantle.p(:,1)/1e3,mantle.p(:,2)/1e3)
toplot=log10(mantle.diffusion.eta*1e6);
pressure=(mantle.e22pl+mantle.e33pl)/2;
deviatoricStrain=sqrt((mantle.e22pl-pressure).^2+2*mantle.e23pl.^2+(mantle.e33pl-pressure).^2);
deviatoricStress=1e6*(deviatoricStrain./mantle.dislocation.e0).^(1./mantle.dislocation.n);
toplot=min(log10(deviatoricStress./deviatoricStrain),log10(1e6*mantle.diffusion.eta));
%toplot=log10(1e6*mantle.diffusion.eta);
%toplot=log10(mantle.COH);
patch([mantle.p(mantle.t(:,1),1),mantle.p(mantle.t(:,2),1),mantle.p(mantle.t(:,3),1),mantle.p(mantle.t(:,1),1)]'/1e3, ...
      [mantle.p(mantle.t(:,1),2),mantle.p(mantle.t(:,2),2),mantle.p(mantle.t(:,3),2),mantle.p(mantle.t(:,1),2)]'/1e3, ...
      [toplot,toplot,toplot,toplot]');
%toplot=(ocean.diffusion.eta);
%patch([ocean.p(ocean.t(:,1),1),ocean.p(ocean.t(:,2),1),ocean.p(ocean.t(:,3),1),ocean.p(ocean.t(:,1),1)]'/1e3, ...
%      [ocean.p(ocean.t(:,1),2),ocean.p(ocean.t(:,2),2),ocean.p(ocean.t(:,3),2),ocean.p(ocean.t(:,1),2)]'/1e3, ...
%      [toplot,toplot,toplot,toplot]');
colorbar

%for k=1:mantle.N
%    text(mantle.xc(k,1)/1e3,mantle.xc(k,2)/1e3,sprintf('%d',k));
%end

plot(obs.x(:,1)/1e3,obs.x(:,2)/1e3,'^')
unicycle.plot.cptcmap('~/Documents/src/relax/share/ViBlGrWhYeOrRe-reverse.cpt');
box on
axis equal tight
set(gca,'xlim',[-150,600],'ylim',[-20, max(ocean.p(:,2))/1e3]);
set(gca,'clim',[19 21])
set(gca,'YDir','Reverse');
xlabel('Distance (km)');
ylabel('Depth (km)');

subplot(2,1,2);cla;hold on
plot( flt.x(:,1)/1e3,flt.b-flt.a)

box on
set(gca,'xlim',[0,600]);
set(gca,'ylim',[-1 1]*1e-2)
set(gca,'YDir','Reverse');
xlabel('Distance (km)');
ylabel('Friction parameter (b-a)');
drawnow();

%return

%% export to .stv

if false
    fid=fopen('./mantle.trv','wt');
    fprintf(fid,'#   n          e22          e23          e33    i1    i2    i3\n');
    fprintf(fid,'%5.5d %11.6e %11.6e %11.6e %5.5d %5.5d %5.5d\n', ...
        [(1:mantle.N)',zeros(mantle.N,1),zeros(mantle.N,1),zeros(mantle.N,1),mantle.t(:,1),mantle.t(:,2),mantle.t(:,3)]');
    fclose(fid);
    
    fid=fopen('./mantle.ned','wt');
    fprintf(fid,'#   n            x2            x3\n');
    fprintf(fid,'%5.5d %+11.6e %+11.6e\n', ...
        [(1:size(mantle.p,1))',mantle.p(:,1),mantle.p(:,2)]');
    fclose(fid);
    return
end

%% Green's functions for triangular elements in mantle

% LL{i,j} is the stress in the i direction due to strain in the j
% direction, with i,j in 22, 23, 33 (in this order).
mantle.LL=cell(3,3);

% initialize stress kernels
for i=1:3
    for j=1:3
        mantle.LL{i,j}=zeros(mantle.N);
    end
end

% diagonal matrix
I=eye(3);

tic
fprintf('# Computing stress Green''s functions LL\n');
for k=1:mantle.N
    if 0==mod(fix((k-1)/mantle.N*1000),50)
        fprintf('.');
    end
    M=mantle.p(mantle.t(k,1),:);
    N=mantle.p(mantle.t(k,2),:);
    P=mantle.p(mantle.t(k,3),:);
    
    for i=1:3
        [s22,s23,s33]=unicycle.greens.computeStressPlaneStrainTriangleFiniteDifference( ...
            mantle.xc(:,1),mantle.xc(:,2),M,N,P,I(i,1),I(i,2),I(i,3),rig,nu);
        
        mantle.LL{1,i}(:,k)=s22(:);
        mantle.LL{2,i}(:,k)=s23(:);
        mantle.LL{3,i}(:,k)=s33(:);
    end
end
fprintf('\n');
toc

% LK{i} is the shear traction on fault due to strain in the i
% direction, with i in 22, 23, 33 (in this order).
mantle.LK=cell(3,1);

% initialize traction kernels
for i=1:3
    mantle.LK{i}=zeros(flt.N,mantle.N);
end

% diagonal matrix
I=eye(3);

tic
fprintf('# Computing traction Green''s functions LK\n');
for k=1:mantle.N
    if 0==mod(fix((k-1)/mantle.N*1000),50)
        fprintf('.');
    end
    M=mantle.p(mantle.t(k,1),:);
    N=mantle.p(mantle.t(k,2),:);
    P=mantle.p(mantle.t(k,3),:);
    
    for i=1:3
        [s22,s23,s33]=unicycle.greens.computeStressPlaneStrainTriangleFiniteDifference( ...
            flt.xc(:,1),flt.xc(:,2),M,N,P,I(i,1),I(i,2),I(i,3),rig,nu);
        
        t=[s22.*flt.nv(:,1)+s23.*flt.nv(:,2), ...
           s23.*flt.nv(:,1)+s33.*flt.nv(:,2) ];
        ts=t(:,1).*flt.dv(:,1)+t(:,2).*flt.dv(:,2);
        
        mantle.LK{i}(:,k)=ts(:);
    end
end
fprintf('\n');
toc

%% LO{i,j} is the displacement component i (i=2,3) at observation points 
% due to strain in the j direction, with j in 22, 23, 33 (in this order).

ocean.LO=cell(obs.dgf,3);

% initialize displacement kernels
for i=1:obs.dgf
    for j=1:3
        ocean.LO{i,j}=zeros(obs.N,ocean.N);
    end
end

% diagonal matrix
I=eye(3);

tic
fprintf('# Computing displacement Green''s functions LO\n');
for k=1:ocean.N
    if 0==mod(fix((k-1)/ocean.N*1000),50)
        fprintf('.');
    end
    M=ocean.p(ocean.t(k,1),:);
    N=ocean.p(ocean.t(k,2),:);
    P=ocean.p(ocean.t(k,3),:);
    
    
    for i=1:3
        [u2,u3]=unicycle.greens.computeDisplacementPlaneStrainTriangle( ...
            obs.x(:,1),obs.x(:,2),M,N,P,I(i,1),I(i,2),I(i,3),nu);
        
        ocean.LO{1,i}(:,k)=u2(:);
        ocean.LO{2,i}(:,k)=u3(:);
    end
end
fprintf('\n');
toc

wedge.LO=cell(obs.dgf,3);

% initialize displacement kernels
for i=1:obs.dgf
    for j=1:3
        wedge.LO{i,j}=zeros(obs.N,wedge.N);
    end
end

% diagonal matrix
I=eye(3);

tic
fprintf('# Computing displacement Green''s functions LO\n');
for k=1:wedge.N
    if 0==mod(fix((k-1)/wedge.N*1000),50)
        fprintf('.');
    end
    M=wedge.p(wedge.t(k,1),:);
    N=wedge.p(wedge.t(k,2),:);
    P=wedge.p(wedge.t(k,3),:);
    
    
    for i=1:3
        [u2,u3]=unicycle.greens.computeDisplacementPlaneStrainTriangle( ...
            obs.x(:,1),obs.x(:,2),M,N,P,I(i,1),I(i,2),I(i,3),nu);
        
        wedge.LO{1,i}(:,k)=u2(:);
        wedge.LO{2,i}(:,k)=u3(:);
    end
end
fprintf('\n');
toc

%% Green's functions for fault elements on megathrust

% KK is the shear traction on fault due to fault slip.
flt.KK=zeros(flt.N);

tic
fprintf('# Computing traction Green''s functions KK\n');
for k=1:flt.N
    if 0==mod(fix((k-1)/flt.N*1000),50)
        fprintf('.');
    end
    
    T=flt.W(k)/1e3;
    [s22,s23,s33]=unicycle.greens.computeStressPlaneStrainCuboid( ...
        flt.xc(:,1),flt.xc(:,2),flt.x(k,1),flt.x(k,2), ...
        T,flt.W(k),flt.dip(k),0,-1/2/T,0,rig,nu);

    t=[s22.*flt.nv(:,1)+s23.*flt.nv(:,2), ...
       s23.*flt.nv(:,1)+s33.*flt.nv(:,2) ];
    ts=t(:,1).*flt.dv(:,1)+t(:,2).*flt.dv(:,2);
    
    flt.KK(:,k)=ts(:);
end
fprintf('\n');
toc


% KL{i} is the stress in the i direction due to fault slip, 
% with i in 22, 23, 33 (in this order).
flt.KL=cell(3,1);

% initialize kernels
for i=1:3
    flt.KL{i}=zeros(mantle.N,flt.N);
end

tic
fprintf('# Computing stress Green''s functions KL\n');
for k=1:flt.N
    if 0==mod(fix((k-1)/flt.N*1000),50)
        fprintf('.');
    end
    
    T=flt.W(k)/1e3;
    [s22,s23,s33]=unicycle.greens.computeStressPlaneStrainCuboid( ...
        mantle.xc(:,1),mantle.xc(:,2),flt.x(k,1),flt.x(k,2), ...
        T,flt.W(k),flt.dip(k),0,-1/2/T,0,rig,nu);
        
    flt.KL{1}(:,k)=s22(:);
    flt.KL{2}(:,k)=s23(:);
    flt.KL{3}(:,k)=s33(:);
end
fprintf('\n');
toc

%% KO{i} is the displacement in the i direction due to fault slip, 
% with i (2, 3) (in this order).
flt.KO=cell(obs.dgf,1);

% initialize kernels
for i=1:obs.dgf
    flt.KO{i}=zeros(obs.N,flt.N);
end

tic
fprintf('# Computing displacement Green''s functions KO\n');
for k=1:flt.N
    if 0==mod(fix((k-1)/flt.N*1000),50)
        fprintf('.');
    end
    
    T=flt.W(k)/1e3;
    [u2,u3]=unicycle.greens.computeDisplacementPlaneStrainCuboid( ...
        obs.x(:,1),obs.x(:,2),flt.x(k,1),flt.x(k,2), ...
        T,flt.W(k),flt.dip(k),0,-1/2/T,0,rig,nu);
        
    flt.KO{1}(:,k)=u2(:);
    flt.KO{2}(:,k)=u3(:);
end
fprintf('\n');
toc

%% stress related figures

figure(3);clf;set(gcf,'name','Stress interactions (Green''s functions)')
subplot(2,1,1);cla;hold on
plot( flt.x(:,1)/1e3,flt.x(:,2)/1e3)
plot(flt.xc(:,1)/1e3,flt.xc(:,2)/1e3,'+')
toplot=sum(flt.KL{3},2);
%toplot=sum(mantle.LL{1,1}+mantle.LL{3,3},2)/1e6;
%k=10;toplot=(mantle.LL{1,1}(:,k)+mantle.LL{3,3}(:,k))/1e6;
%k=3;toplot=mantle.LL{3,3}(:,k)/1e6;
%tau=sum(flt.KL{2}+flt.KL{1},2);
%toplot=log10(abs(tau)./(mantle.dislocation.e0.*abs(tau).^mantle.dislocation.n));
patch([mantle.p(mantle.t(:,1),1),mantle.p(mantle.t(:,2),1),mantle.p(mantle.t(:,3),1),mantle.p(mantle.t(:,1),1)]'/1e3, ...
      [mantle.p(mantle.t(:,1),2),mantle.p(mantle.t(:,2),2),mantle.p(mantle.t(:,3),2),mantle.p(mantle.t(:,1),2)]'/1e3, ...
      [toplot,toplot,toplot,toplot]');

h=colorbar();
ylabel(h,'Stress (MPa)');
box on
axis equal tight
set(gca,'xlim',[-100,600],'ylim',[0, max(mantle.p(:,2))/1e3]);
%set(gca,'clim',[16 21])
set(gca,'clim',[-1 1]*max(abs(toplot)));
set(gca,'YDir','Reverse');
xlabel('Distance (km)');
ylabel('Depth (km)');

subplot(2,1,2);cla;hold on
k=170;
patch([mantle.p(mantle.t(k,1),1),mantle.p(mantle.t(k,2),1),mantle.p(mantle.t(k,3),1),mantle.p(mantle.t(k,1),1)]'/1e3, ...
      [mantle.p(mantle.t(k,1),2),mantle.p(mantle.t(k,2),2),mantle.p(mantle.t(k,3),2),mantle.p(mantle.t(k,1),2)]'/1e3, ...
      'k');

plot([C(1) D(1) E(1) F(1) C(1)]/1e3,[C(2) D(2) E(2) F(2) C(2)]/1e3);

title(sprintf('Triangle index %d',k));
box on
axis equal tight
set(gca,'xlim',[-100,600],'ylim',[0, max(mantle.p(:,2))/1e3]);
set(gca,'YDir','Reverse');
colorbar
ylabel('Depth (km)');
xlabel('Distance (km)');

%%
if false
    % check the Green's functions
    
    for i=1:3
        for j=1:3
            figure(34+(i-1)*3+j);clf;set(gcf,'name',sprintf('LL%i,%i\n',i,j))
            fprintf('LL%i,%i, min %d, max: %d\n',i,j,minmax(mantle.LL{i,j}))
            %subplot(3,3,(i-1)*3+j);cla;
            imagesc(mantle.LL{i,j})
            axis equal tight
            colorbar
            set(gca,'clim',[-1 1]*max(abs(get(gca,'clim'))))
        end
    end
    return
end

%return
%% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                                                       %
%         N U M E R I C A L   S O L U T I O N           %
%                                                       %
% % % % % % % % % % % % % % % % % % % % % % % % % % % % %

% state parameters
flt.dgf=4;
mantle.dgf=6;

% initialize State Vector
Y0=zeros(flt.N*flt.dgf+mantle.N*mantle.dgf,1);

% fault patches
Y0(1:flt.dgf:flt.N*flt.dgf) = zeros(flt.N,1); % slip
Y0(2:flt.dgf:flt.N*flt.dgf) = flt.sigmab.*(flt.mu0+(flt.a-flt.b).*log(flt.Vpl./flt.Vo))+flt.damping*flt.Vpl; % traction
Y0(3:flt.dgf:flt.N*flt.dgf) = log(flt.Vo./flt.Vpl); % state
Y0(4:flt.dgf:flt.N*flt.dgf) = log(flt.Vpl*0.98./flt.Vo); % log of velocity

% shear zones
Y0(flt.N*flt.dgf+1:mantle.dgf:end) = zeros(mantle.N,1); % s22
Y0(flt.N*flt.dgf+2:mantle.dgf:end) = zeros(mantle.N,1); % s23
Y0(flt.N*flt.dgf+3:mantle.dgf:end) = zeros(mantle.N,1); % s33
Y0(flt.N*flt.dgf+4:mantle.dgf:end) = zeros(mantle.N,1); % e22
Y0(flt.N*flt.dgf+5:mantle.dgf:end) = zeros(mantle.N,1); % e23
Y0(flt.N*flt.dgf+6:mantle.dgf:end) = zeros(mantle.N,1); % e33

% initialize function handle with constitutive parameters
yp=@(t,y) viscoplastic_ode(t,y,flt,mantle);

close all

tic
% solve
fprintf('# solving ODE\n')
options=odeset('Refine',1,'RelTol',3e-7,'InitialStep',1e-3,'MaxStep',3e6); 
[sim.t,sim.Y]=ode45(yp,[0 3000*3.15e7],Y0,options);
toc

beep

%% post-processing

tic
fprintf('# post-processing (e.g., interpolation along profiles)\n')

% fault results
flt.Y=sim.Y(:,1:flt.N*flt.dgf);
% ocean results
ocean.Y=sim.Y(:,flt.N*flt.dgf+1:flt.N*flt.dgf+ocean.N*mantle.dgf);
% wedge results
wedge.Y=sim.Y(:,flt.N*flt.dgf+ocean.N*mantle.dgf+1:end);

% pressure
ocean.pressure=(ocean.Y(:,1:mantle.dgf:end)+ocean.Y(:,3:mantle.dgf:end))/2;
% deviatoric stress
ocean.Sp=sqrt((ocean.Y(:,1:mantle.dgf:end)-ocean.pressure).^2+2*ocean.Y(:,2:mantle.dgf:end).^2+(ocean.Y(:,3:mantle.dgf:end)-ocean.pressure).^2);
% strain rate
ocean.dEdt=ocean.Sp./repmat(ocean.diffusion.eta',length(sim.t),1)+repmat(ocean.dislocation.e0',length(sim.t),1).*ocean.Sp.^repmat(ocean.dislocation.n',length(sim.t),1);

% pressure
wedge.pressure=(wedge.Y(:,1:mantle.dgf:end)+wedge.Y(:,3:mantle.dgf:end))/2;
% deviatoric stress
wedge.Sp=sqrt((wedge.Y(:,1:mantle.dgf:end)-wedge.pressure).^2+2*wedge.Y(:,2:mantle.dgf:end).^2+(wedge.Y(:,3:mantle.dgf:end)-wedge.pressure).^2);
% strain rate
wedge.dEdt=wedge.Sp./repmat(wedge.diffusion.eta',length(sim.t),1)+repmat(wedge.dislocation.e0',length(sim.t),1).*wedge.Sp.^repmat(wedge.dislocation.n',length(sim.t),1);

% strain rate profiles
ocean.profile.dEdt=zeros(length(sim.t),ocean.profile.N);
wedge.profile.dEdt=zeros(length(sim.t),wedge.profile.N);

for k=1:length(sim.t)
    ocean.profile.F=scatteredInterpolant(ocean.xc(:,1),ocean.xc(:,2),ocean.dEdt(k,:)');
    ocean.profile.dEdt(k,:)=ocean.profile.F(ocean.profile.p(:,1),ocean.profile.p(:,2));
    ocean.profile.F=scatteredInterpolant(ocean.xc(:,1),ocean.xc(:,2),ocean.Sp(k,:)');
    ocean.profile.tau(k,:)=ocean.profile.F(ocean.profile.p(:,1),ocean.profile.p(:,2));
    
    wedge.profile.F=scatteredInterpolant(wedge.xc(:,1),wedge.xc(:,2),wedge.dEdt(k,:)');
    wedge.profile.dEdt(k,:)=wedge.profile.F(wedge.profile.p(:,1),wedge.profile.p(:,2));
    wedge.profile.F=scatteredInterpolant(wedge.xc(:,1),wedge.xc(:,2),wedge.Sp(k,:)');
    wedge.profile.tau(k,:)=wedge.profile.F(wedge.profile.p(:,1),wedge.profile.p(:,2));
end

% observation points
flt.ue=flt.KO{1}*(flt.Y(:,1:flt.dgf:end)-sim.t*flt.Vpl')';

ocean.ue=( ...
    ocean.LO{1,1}*ocean.Y(:,4:mantle.dgf:end)' ...
   +ocean.LO{1,2}*ocean.Y(:,5:mantle.dgf:end)' ...
   +ocean.LO{1,3}*ocean.Y(:,6:mantle.dgf:end)');

wedge.ue=( ...
    wedge.LO{1,1}*wedge.Y(:,4:mantle.dgf:end)' ...
   +wedge.LO{1,2}*wedge.Y(:,5:mantle.dgf:end)' ...
   +wedge.LO{1,3}*wedge.Y(:,6:mantle.dgf:end)');

toc
beep

%% simulation results

figure(4);clf;set(gcf,'name','Time step evolution')

f41=subplot(5,1,1);cla;
pcolor(((1:length(sim.t))-1)',flt.xc(:,1)/1e3,flt.Y(:,4:flt.dgf:end)'/log(10)-6), shading flat
set(gca,'YDir','reverse');
colormap(f41,parula);
%h=colorbar('Location','NorthOutside');
%xlabel(h,'Slip velocity on megathrust (m/s)');
set(gca,'clim',[-12 1]);
ylabel('Distance from trench (km)');
set(gca,'xticklabel',[]);

f42=subplot(5,1,2);cla;
pcolor(((1:length(sim.t))-1)',ocean.profile.p(:,2)/1e3,log10(ocean.profile.tau'./ocean.profile.dEdt')), shading flat
set(gca,'YDir','reverse');
colormap(f42,jet);
%h=colorbar('Location','NorthOutside');
%xlabel(h,'Strain rate in oceanic asthenosphere (1/s)');
%set(gca,'clim',[-1 1]*max(abs(ocean.profile.dEdt(:))))
colorbar
ylabel('Depth (km)');
set(gca,'xticklabel',[]);

f42=subplot(5,1,3);cla;
pcolor(((1:length(sim.t))-1)',wedge.profile.p(:,1)/1e3,wedge.profile.dEdt'), shading flat
set(gca,'YDir','reverse');
colormap(f42,jet);
%h=colorbar('Location','NorthOutside');
%xlabel(h,'Strain rate in mantle wedge (1/s)');
set(gca,'clim',[-1 1]*max(abs(wedge.profile.dEdt(:))))
ylabel('Distance from trench (km)');
set(gca,'xticklabel',[]);

subplot(5,1,4);cla;hold on
cla;hold on
segment=fix(mean(sz));
component=4; % 1: slip, 2: stress, 3: state, 4: log10v
pos=component+(segment-1)*flt.dgf;
plot(log10(flt.Vo(segment)*exp(flt.Y(:,pos)')));
segment=fix(mean(ss));
component=4; % 1: slip, 2: stress, 3: state, 4: log10v
pos=component+(segment-1)*flt.dgf;
plot(log10(flt.Vo(segment)*exp(flt.Y(:,pos)')));
set(gca,'xlim',[0 1]*numel(sim.t));
ylabel('Slip velocity in seismogenic zone (m/s)');
box on
set(gca,'xticklabel',[]);

subplot(5,1,5);cla;hold on
triangle=70;
component=2; % 1: s22, 2: s23, 3: s33, 4: e22, 5: e23, 6: e33
%toplot=sim.Ys(:,component+(triangle-1)*mantle.dgf)';
toplot=ocean.Sp(:,1+(triangle-1))';
plot(toplot);
toplot=wedge.Sp(:,1+(triangle-1))';
plot(toplot);
xlabel('Time step (integer)');
ylabel('Deviatoric stress (MPa)');
box on
set(gca,'ylim',[-1 1]*max(abs(toplot)));
set(gca,'xlim',[0 1]*numel(sim.t));


%% export to GMT
if true
    fid=fopen('fault-dynamics.xyz','wt');
    fprintf(fid,'#   index (integer),  time (yr),     x2 (km),    x3 (km), slip (m), log10v (m/s)\n');
    fprintf(fid,'%d %+e %+e %+e %+e %+e\n', ...
        [repmat(((1:length(sim.t))-1)',size(flt.xc,1),1), ...
         repmat(sim.t/3.15e7,size(flt.xc,1),1), ...
         reshape((flt.xc(:,1)*ones(1,length(sim.t)))'/1e3,length(sim.t)*size(flt.xc,1),1), ...
         reshape((flt.xc(:,2)*ones(1,length(sim.t)))'/1e3,length(sim.t)*size(flt.xc,1),1), ...
         reshape(flt.Y(:,1:flt.dgf:end),length(sim.t)*size(flt.xc,1),1), ...
         reshape(flt.Y(:,4:flt.dgf:end)/log(10)-6,length(sim.t)*size(flt.xc,1),1)]');
    fclose(fid);
   
    fid=fopen('ocean-dynamics.xyz','wt');
    fprintf(fid,'#   index (integer),  time (yr),     x2 (km),    x3 (km),   dE/dt (1/s),    tau (MPa)\n');
    fprintf(fid,'%d %+e %+e %+e %+e %+e\n', ...
        [repmat(((1:length(sim.t))-1)',size(ocean.profile.p,1),1), ...
         repmat(sim.t/3.15e7,size(ocean.profile.p,1),1), ...
         reshape((ocean.profile.p(:,1)*ones(1,length(sim.t)))'/1e3,length(sim.t)*size(ocean.profile.p,1),1), ...
         reshape((ocean.profile.p(:,2)*ones(1,length(sim.t)))'/1e3,length(sim.t)*size(ocean.profile.p,1),1), ...
         reshape(ocean.profile.dEdt,length(sim.t)*size(ocean.profile.p,1),1), ...
         reshape(ocean.profile.tau, length(sim.t)*size(ocean.profile.p,1),1)]');
    fclose(fid);
    
    fid=fopen('wedge-dynamics.xyz','wt');
    fprintf(fid,'#   index (integer),  time (yr),     x2 (km),    x3 (km),    dE/dt,    tau (MPa) (1/s)\n');
    fprintf(fid,'%d %+e %+e %+e %+e %+e\n', ...
        [repmat(((1:length(sim.t))-1)',size(wedge.profile.p,1),1), ...
         repmat(sim.t/3.15e7,size(wedge.profile.p,1),1), ...
         reshape((wedge.profile.p(:,1)*ones(1,length(sim.t)))'/1e3,length(sim.t)*size(wedge.profile.p,1),1), ...
         reshape((wedge.profile.p(:,2)*ones(1,length(sim.t)))'/1e3,length(sim.t)*size(wedge.profile.p,1),1), ...
         reshape(wedge.profile.dEdt,length(sim.t)*size(wedge.profile.p,1),1), ...
         reshape(wedge.profile.tau, length(sim.t)*size(wedge.profile.p,1),1)]');
    fclose(fid);
    
    
end

%%

figure(5);clf;set(gcf,'name','Time evolution')

f51=subplot(5,1,1);cla;
pcolor(sim.t'/3.15e7,flt.xc(:,1)/1e3,flt.Y(:,4:flt.dgf:end)'/log(10)-6), shading flat
%pcolor(sim.Yf(:,4:flt.dgf:end)'/log(10)-6), shading flat
set(gca,'YDir','reverse');
colormap(f51,parula);
h=colorbar('Location','NorthOutside');
xlabel(h,'Slip velocity on megathrust (m/s)');
set(gca,'clim',[-12 1]);
ylabel('Distance from trench (km)');
set(gca,'xticklabel',[]);

f51=subplot(5,1,2);cla;
pcolor(sim.t'/3.15e7,ocean.profile.p(:,2)/1e3,ocean.profile.dEdt'), shading flat
set(gca,'YDir','reverse');
colormap(f51,parula);
%h=colorbar('Location','NorthOutside');
%xlabel(h,'Strain rate in mantle (1/s)');
set(gca,'clim',[-1 1]*max(abs(ocean.profile.dEdt(:))))
ylabel('Depth (km)');
set(gca,'xticklabel',[]);

f51=subplot(5,1,3);cla;
pcolor(sim.t'/3.15e7,wedge.profile.p(:,2)/1e3,wedge.profile.dEdt'), shading flat
set(gca,'YDir','reverse');
colormap(f51,parula);
%h=colorbar('Location','NorthOutside');
%xlabel(h,'Strain rate in mantle (1/s)');
set(gca,'clim',[-1 1]*max(abs(wedge.profile.dEdt(:))))
ylabel('Depth (km)');
set(gca,'xticklabel',[]);

subplot(5,1,4);cla;hold on
cla;hold on
segment=fix(mean(sz));
component=4; % 1: slip, 2: stress, 3: state, 4: log10v
pos=component+(segment-1)*flt.dgf;
plot(sim.t/3.15e7,log10(flt.Vo(segment)*exp(flt.Y(:,pos)')));
segment=fix(mean(ss));
component=4; % 1: slip, 2: stress, 3: state, 4: log10v
pos=component+(segment-1)*flt.dgf;
plot(sim.t/3.15e7,log10(flt.Vo(segment)*exp(flt.Y(:,pos)')));
ylabel('Slip velocity in seismogenic zone (m/s)');
box on
set(gca,'xlim',[sim.t(1) sim.t(end)]/3.15e7)
set(gca,'xticklabel',[]);
%colorbar

subplot(5,1,5);cla;hold on
triangle=70;
component=2; % 1: s22, 2: s23, 3: s33, 4: e22, 5: e23, 6: e33
%toplot=sim.Ys(:,component+(triangle-1)*mantle.dgf)';
toplot=ocean.Sp(:,1+(triangle-1))';
plot(sim.t/3.15e7,toplot);
toplot=wedge.Sp(:,1+(triangle-1))';
plot(sim.t/3.15e7,toplot);
xlabel('Time (yr)');
ylabel('Shear stress (MPa)');
box on
set(gca,'xlim',[sim.t(1) sim.t(end)]/3.15e7)
%colorbar

%%
if false
figure(6);clf;set(gcf,'name','Evolution of stress in the oceanic mantle (animation)')

for k=1:50:length(sim.t)
%subplot(2,1,1);
cla;hold on
plot( flt.x(:,1)/1e3,flt.x(:,2)/1e3);
plot(flt.xc(:,1)/1e3,flt.xc(:,2)/1e3,'+');
%toplot=sim.Ys(k,2:mantle.dgf:end)';
toplot=Sp(k,:)';
h=patch([mantle.p(mantle.t(:,1),1),mantle.p(mantle.t(:,2),1),mantle.p(mantle.t(:,3),1),mantle.p(mantle.t(:,1),1)]'/1e3, ...
        [mantle.p(mantle.t(:,1),2),mantle.p(mantle.t(:,2),2),mantle.p(mantle.t(:,3),2),mantle.p(mantle.t(:,1),2)]'/1e3, ...
        [toplot,toplot,toplot,toplot]');
set(h,'edgecolor','none');

v=0.01:0.25:max(Sp(:));
tri=delaunay(mantle.xc(:,1),mantle.xc(:,2));
[~,h]=tricontour(tri,mantle.xc(:,1)/1e3,mantle.xc(:,2)/1e3,Sp(k,:)',v);
for i=1:length(h)
    set(h(i),'EdgeColor','k');
end
    
h=colorbar();
ylabel(h,'Shear stress (MPa)');
title(sprintf('Time: %d',sim.t(k)/3.15e7));
box on
axis equal tight
set(gca,'xlim',[-100,600],'ylim',[0, max(mantle.p(:,2))/1e3]);
%set(gca,'clim',[-1 1]*max(abs(get(gca,'clim'))));
set(gca,'clim',[-1 1]*0.6);
set(gca,'YDir','Reverse');
xlabel('Distance (km)');
ylabel('Depth (km)');
drawnow()
end
end

%% 

figure(7);set(gcf,'name','Space-time stress contour plot in oceanic mantle')
clf;hold on

v=(0.2:0.2:1)*max([max(ocean.dEdt(:)),max(wedge.dEdt(:))]);

tri=delaunay(ocean.xc(:,1),ocean.xc(:,2));
for k=200:25:numel(sim.t)
    [fig7.C,fig7.h]=tricontour(tri,ocean.xc(:,1)/1e3,ocean.xc(:,2)/1e3,ocean.dEdt(k,:)',v);
    for i=1:size(fig7.h,1)
        set(fig7.h(i),'ZData',get(fig7.h(i),'YData'));
        set(fig7.h(i),'YData',get(fig7.h(i),'XData'));
        set(fig7.h(i),'XData',ones(length(get(fig7.h(i),'XData')),1)*sim.t(k)/3.15e7);
        set(fig7.h(i),'LineWidth',2);
    end
end

tri=delaunay(wedge.xc(:,1),wedge.xc(:,2));
for k=200:25:numel(sim.t)
    [fig7.C,fig7.h]=tricontour(tri,wedge.xc(:,1)/1e3,wedge.xc(:,2)/1e3,wedge.dEdt(k,:)',v);
    for i=1:size(fig7.h,1)
        set(fig7.h(i),'ZData',get(fig7.h(i),'YData'));
        set(fig7.h(i),'YData',get(fig7.h(i),'XData'));
        set(fig7.h(i),'XData',ones(length(get(fig7.h(i),'XData')),1)*sim.t(k)/3.15e7);
        set(fig7.h(i),'LineWidth',2);
    end
end

% megathrust
plot3( ...
    [sim.t(1) sim.t(1) sim.t(end) sim.t(end) sim.t(1)]/3.15e7, ...
    [A(1) B(1) B(1) A(1) A(1)]/1e3, ...
    [A(2) B(2) B(2) A(2) A(2)]/1e3,'k');
plot3( ...
    [sim.t(1) sim.t(1) sim.t(end) sim.t(end) sim.t(1)]/3.15e7, ...
    [G(1) J(1) J(1) G(1) G(1)]/1e3, ...
    [G(2) J(2) J(2) G(2) G(2)]/1e3,'k');

% oceanic asthenosphere
plot3( ...
    [sim.t(1) sim.t(1) sim.t(end) sim.t(end) sim.t(1)]/3.15e7, ...
    [C(1) D(1) D(1) C(1) C(1)]/1e3, ...
    [C(2) D(2) D(2) C(2) C(2)]/1e3,'k');
plot3( ...
    [sim.t(1) sim.t(1) sim.t(end) sim.t(end) sim.t(1)]/3.15e7, ...
    [D(1) E(1) E(1) D(1) D(1)]/1e3, ...
    [D(2) E(2) E(2) D(2) D(2)]/1e3,'k');

map=colormap(hot);
fig7.h=colorbar();
ylabel(fig7.h,'Deviatoric stress (MPa)');
axis equal tight
set(gca,'DataAspectRatio',[5 1 1]);
xlabel('Time (year)');
ylabel('Distance from trench (km)');
zlabel('Depth (km)');
set(gca,'XDir','Reverse');
set(gca,'ZDir','Reverse');
box on, grid on
set(gca,'view',[133.2 18.8]);
set(gca,'xlim',[0 sim.t(end)/3.15e7]);
set(gca,'zlim',[0 280]);
set(gca,'clim',[0 max(get(gca,'clim'))]);


if true
    
    v=[0.75 0.75]*max(ocean.dEdt(:));
    
    % export to .xyz file
    fid=fopen('ocean-3d.xyz','wt');
    
    tri=delaunay(ocean.xc(:,1),ocean.xc(:,2));
    for k=200:25:numel(sim.t)
        [fig7.C,fig7.h]=tricontour(tri,ocean.xc(:,1)/1e3,ocean.xc(:,2)/1e3,ocean.dEdt(k,:)',v);
        for i=1:size(fig7.h,1)
            set(fig7.h(i),'ZData',get(fig7.h(i),'YData'));
            set(fig7.h(i),'YData',get(fig7.h(i),'XData'));
            set(fig7.h(i),'XData',ones(length(get(fig7.h(i),'XData')),1)*sim.t(k)/3.15e7);
            set(fig7.h(i),'LineWidth',2);
        end
        
        for i=1:size(fig7.h,1)
            CData=get(fig7.h(i),'CData');
            fprintf(fid,'> -Z%e\n',CData(1)*1e15);
            fprintf(fid,'%e %e %e\n',[get(fig7.h(i),'YData'),get(fig7.h(i),'XData'),-get(fig7.h(i),'ZData')]');
        end
        
    end
    fclose(fid);
    
    
    v=[0.75 0.75]*max(wedge.dEdt(:));
    
    % export to .xyz file
    fid=fopen('wedge-3d.xyz','wt');
    
    tri=delaunay(wedge.xc(:,1),wedge.xc(:,2));
    for k=200:25:numel(sim.t)
        [fig7.C,fig7.h]=tricontour(tri,wedge.xc(:,1)/1e3,wedge.xc(:,2)/1e3,wedge.dEdt(k,:)',v);
        for i=1:size(fig7.h,1)
            set(fig7.h(i),'ZData',get(fig7.h(i),'YData'));
            set(fig7.h(i),'YData',get(fig7.h(i),'XData'));
            set(fig7.h(i),'XData',ones(length(get(fig7.h(i),'XData')),1)*sim.t(k)/3.15e7);
            set(fig7.h(i),'LineWidth',2);
        end
        
        for i=1:size(fig7.h,1)
            CData=get(fig7.h(i),'CData');
            fprintf(fid,'> -Z%e\n',CData(1)*1e15);
            fprintf(fid,'%e %e %e\n',[get(fig7.h(i),'YData'),get(fig7.h(i),'XData'),-get(fig7.h(i),'ZData')]');
        end
        
    end
    fclose(fid);
end

%% displacement time series

figure(8);clf;set(gcf,'name','Evolution of surface vertical displacements')

subplot(1,3,1);cla;hold on
pcolor(obs.x(:,1)/1e3,((1:length(sim.t))-1)',flt.ue'), shading flat
colormap(jet)
xlabel('Distance from trench (km)')
ylabel('Time step (integer)')
box on
colorbar
set(gca,'xlim',minmax(obs.x(:,1))/1e3);
set(gca,'ylim',[0 1]*numel(sim.t));
set(gca,'clim',[-1 1]*max(abs(flt.ue(:))));
title('Uz due to fault slip')

subplot(1,3,2);cla;hold on
pcolor(obs.x(:,1)/1e3,((1:length(sim.t))-1)',ocean.ue'), shading flat

xlabel('Distance from trench (km)')
box on
colorbar
set(gca,'xlim',minmax(obs.x(:,1))/1e3);
set(gca,'ylim',[0 1]*numel(sim.t));
set(gca,'clim',[-1 1]*max(abs(ocean.ue(:))));
title('Uz due to oceanic mantle flow')
set(gca,'ytick',[])
set(gca,'yticklabel',[])

subplot(1,3,3);cla;hold on
pcolor(obs.x(:,1)/1e3,((1:length(sim.t))-1)',wedge.ue'), shading flat

xlabel('Distance from trench (km)')
box on
colorbar
set(gca,'xlim',minmax(obs.x(:,1))/1e3);
set(gca,'ylim',[0 1]*numel(sim.t));
set(gca,'clim',[-1 1]*max(abs(wedge.ue(:))));
title('Uz due to wedge flow')
set(gca,'ytick',[])
set(gca,'yticklabel',[])

%%
figure(9);clf;set(gcf,'name','Time series of surface vertical displacements')

subplot(1,3,1);cla;hold on
pcolor(obs.x(:,1)/1e3,sim.t'/3.15e7,flt.ue'), shading flat
colormap(jet)
xlabel('Distance from trench (km)')
ylabel('Time (yr)')
box on
colorbar
set(gca,'xlim',minmax(obs.x(:,1))/1e3);
set(gca,'ylim',[sim.t(1) sim.t(end)]/3.15e7);
set(gca,'clim',[-1 1]*max(abs(flt.ue(:))));
title('Ue due to fault slip')

subplot(1,3,2);cla;hold on
pcolor(obs.x(:,1)/1e3,sim.t'/3.15e7,ocean.ue'), shading flat

xlabel('Distance from trench (km)')
box on
colorbar
set(gca,'xlim',minmax(obs.x(:,1))/1e3);
set(gca,'ylim',[sim.t(1) sim.t(end)]/3.15e7);
set(gca,'clim',[-1 1]*max(abs(ocean.ue(:))));
title('Ue due to oceanic mantle flow')
set(gca,'ytick',[])
set(gca,'yticklabel',[])

subplot(1,3,3);cla;hold on
pcolor(obs.x(:,1)/1e3,sim.t'/3.15e7,wedge.ue'), shading flat

xlabel('Distance from trench (km)')
box on
h=colorbar();
ylabel(h,'East displacement (m)')
set(gca,'xlim',minmax(obs.x(:,1))/1e3);
set(gca,'ylim',[sim.t(1) sim.t(end)]/3.15e7);
set(gca,'clim',[-1 1]*max(abs(wedge.ue(:))));
title('Ue due to mantle wedge flow')
set(gca,'ytick',[])
set(gca,'yticklabel',[])

if true
    % export to GMT
    fid=fopen('surface-displacement.dat','wt');
    fprintf(fid,'# index, time (yr), x2 (km), u2 fault (m), u2 ocean (m), u2 wedge (m)\n');
    fprintf(fid,'%d %+e %+e %+e %+e %+e\n', ...
        [repmat(((1:length(sim.t))-1)',size(obs.x,1),1), ...
         repmat(sim.t/3.15e7,size(obs.x,1),1), ...
         reshape((obs.x(:,1)*ones(1,length(sim.t)))'/1e3,length(sim.t)*size(obs.x,1),1), ...
         reshape(flt.ue',length(sim.t)*size(obs.x,1),1), ...
         reshape(ocean.ue',length(sim.t)*size(obs.x,1),1), ...
         reshape(wedge.ue',length(sim.t)*size(obs.x,1),1), ...
         ]');
    fclose(fid);
end






