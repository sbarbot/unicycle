 function [Yp]= viscoplastic_ode(~,Y,flt,stv)
% function viscoplastic_ode describes the evolution of the ordinary
% differential equation y' = f(t,y), where the state vector y is 
%
%        /        s          \            
%    y = |       tau         |         
%        | log(theta Vo / L) |           
%        |       ...         |
%        |       s22         |
%        |       s23         |
%        |       s33         |
%        |       e22         |
%        |       e23         |
%        \       e33         /
%
% Instead of integrating numerically the aging law
%
%    d theta / dt = 1 - V theta / L
%
% as is, we operate the following change of variable
%
%    phi = ln (theta Vo / L)
%
% Considering the following relationships
%
%    d phi = d theta / theta
%
%    d theta = d phi theta = d phi exp(phi) L / Vo
%
%    d theta / dt = d phi exp(phi) / dt L / Vo
%
%    d phi exp(phi) / dt L / Vo = 1 - V theta / L = 1 - V exp(phi) / Vo
%
% we obtain the evolution law for the new variable
% 
%    d phi / dt = ( Vo exp(-phi) - V ) / L
%
%  The state vector is split with initial cells considering fault parameters
%  and the latter portion considering strain in finite volumes with (s12,s13)
%  and (e12,e13) being the 2D antiplane shear stress and strain components.

% fault state variable
th=Y(3:flt.dgf:flt.N*flt.dgf);

% fault slip velocity 
V=flt.Vo.*exp(Y(4:flt.dgf:flt.N*flt.dgf));

% stress in zones of distributed deformation
tau22 = Y(flt.N*flt.dgf+1:stv.dgf:end);  
tau23 = Y(flt.N*flt.dgf+2:stv.dgf:end);
tau33 = Y(flt.N*flt.dgf+3:stv.dgf:end);

% deviatoric stress
p=(tau22+tau33)/2;
tau22p=tau22-p;
tau33p=tau33-p;

% diffusion strain-rate
e22=tau22p./stv.diffusion.eta;
e23=tau23 ./stv.diffusion.eta;
e33=tau33p./stv.diffusion.eta;

% dislocation strain-rate
tau=sqrt(tau22.^2+2*tau23.^2+tau33.^2);
e22=e22+stv.dislocation.e0.*tau.^(stv.dislocation.n-1).*tau22p;
e23=e23+stv.dislocation.e0.*tau.^(stv.dislocation.n-1).*tau23;
e33=e33+stv.dislocation.e0.*tau.^(stv.dislocation.n-1).*tau33p;

% Initiate state derivative
Yp=zeros(size(Y));  

% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                       Fault                         %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %

% slip velocity
Yp(1:flt.dgf:flt.N*flt.dgf)=V;

% rate of state (rate of log(theta/theta0))
dth=(flt.Vo.*exp(-th)-V)./flt.L;
Yp(3:flt.dgf:flt.N*flt.dgf) = dth;

kv = flt.KK*(V-flt.Vpl) ...
     +stv.LK{1}*(e22-stv.e22pl) + ...
     +stv.LK{2}*(e23-stv.e23pl) + ...
     +stv.LK{3}*(e33-stv.e33pl);
 
% acceleration (rate of log(V/Vo)) 
Yp(4:flt.dgf:flt.N*flt.dgf)=(kv-flt.b.*flt.sigmab.*dth) ./ ...
                           (flt.a.*flt.sigmab + flt.damping.*V);

% shear stress rate on fault
Yp(2:flt.dgf:flt.N*flt.dgf)=kv-flt.damping.*V;

% % % % % % % % % % % % % % % % % % % % % % % % % % % %
%                     Shear Zones                     %
% % % % % % % % % % % % % % % % % % % % % % % % % % % %

% stress rate due to triangular volumes and fault velocity
Yp(flt.N*flt.dgf+1:stv.dgf:end)=stv.LL{1,1}*(e22-stv.e22pl) ...
                                 +stv.LL{1,2}*(e23-stv.e23pl) ...
                                 +stv.LL{1,3}*(e33-stv.e33pl) ...
                                 +flt.KL{1}*(V-flt.Vpl);
Yp(flt.N*flt.dgf+2:stv.dgf:end)=stv.LL{2,1}*(e22-stv.e22pl) ...
                                 +stv.LL{2,2}*(e23-stv.e23pl) ...
                                 +stv.LL{2,3}*(e33-stv.e33pl) ...
                                 +flt.KL{2}*(V-flt.Vpl);
Yp(flt.N*flt.dgf+3:stv.dgf:end)=stv.LL{3,1}*(e22-stv.e22pl) ...
                                 +stv.LL{3,2}*(e23-stv.e23pl) ...
                                 +stv.LL{3,3}*(e33-stv.e33pl) ...
                                 +flt.KL{3}*(V-flt.Vpl);
                             
% strain rate in triangular volumes
Yp(flt.N*flt.dgf+4:stv.dgf:end)=e22;
Yp(flt.N*flt.dgf+5:stv.dgf:end)=e23;
Yp(flt.N*flt.dgf+6:stv.dgf:end)=e33;


end
