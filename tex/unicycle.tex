\documentclass[letterpaper,12pt]{article}

\usepackage{amsmath,amsfonts}
\usepackage{natbib}

\if x\pdfoutput\undefined  
\usepackage[dvips]{graphicx}  
\else  
\usepackage[pdftex]{graphicx}
\pdfcompresslevel=1  
\fi  
\graphicspath{{./},{./graphics/}}
\usepackage[T1]{fontenc}
\usepackage{fouriernc}

\usepackage[breaklinks=true]{hyperref}

\newcommand*\diff{\mathop{}\!\mathrm{d}}

\begin{document}

\thispagestyle{empty}
\begin{center}
\Large\scshape Unified Cycle of Earthquakes
\vspace{3cm}\\
\includegraphics[width=\textwidth]{unicycle-icon}
\vspace{0.5cm}\\
\flushright\footnotesize\textcopyright{} 2014-2023, Sylvain Barbot \\
\end{center}

\clearpage

\tableofcontents

\clearpage

\setcounter{page} {1}

\section{Lithosphere mechanics}

\subsection{Constitutive framework for fault friction}

The frictional behavior of fault rocks is complex subject to much ongoing research. Here, we describe the rate-, state-, and temperature-dependent constitutive law implemented in the Unicycle code that is valid for a limited range of temperature and slip-rate~\citep{barbot19a}. The healing of fault surfaces surrounding contact junctions and the rejuvenation of the contact population during sliding affects the frictional strength during seismic cycles. The shear and normal traction are supported by micro-asperities at individual grain-to-grain contacts in a gouge layer or as isolated topographic highs on a rough interface for bare contacts. Due to fault roughness, there is a random distribution of asperity size, influencing the average area of contact within a representative surface element. The real area of contact depends on the effective normal stress and the geometry of the micro-asperities surrounding the contact junctions, as in
%
\begin{equation}\label{eqn:real-area-contact}
\mathcal{A}=\frac{c_0+\mu_0\bar{\sigma}}{\chi}\left(\frac{\theta V_0}{L}\right)^\frac{b}{\mu_0}~,
\end{equation}
where $\mathcal{A}$ is the real area of contact density, i.e., the area of all contact junctions divided by the nominal area of the representative surface element, $c_0$ is the cohesion, $\mu_0$ is the reference coefficient of friction, $\bar{\sigma}=\sigma_n-p_w$ is the effective normal stress, i.e., the difference between the applied normal stress $\sigma_n$ and the pore-fluid pressure $p_w$, and $\chi$ is the hardness for ploughing. The state variable $\theta$ represents the age of micro-asperity contact, $L/V_0$ being a reference lifespan. The age of contact is related to the size of micro-asperities.

The constitutive behavior is based on the assumption that the yield strength is proportional to the real area of contact and the material hardness for ploughing, as in the following expression
\begin{equation}\label{eqn:strength}
\sigma_Y=\mathcal{A}\chi~.
\end{equation}
Thus, any time- or slip-dependent change in the area of contact directly affects the interface strength. The constitutive law of the model is formulated as a thermally activated process with a power-law relationship between shear traction and velocity. Unicycle implements a formulation involving vectors, but we describe the scalar form for simplicity, as in
\begin{equation}\label{eqn:constitutive-basic}
V=V_0\left(\frac{\tau}{\sigma_Y}\right)^\frac{\mu_0}{a}\exp\left[-\frac{Q}{R}\left(\frac{1}{T}-\frac{1}{T_0}\right)\right]~,
\end{equation}
where $\tau$ is the norm of the shear traction vector, $T$ is the temperature of the active shear zone with the energy and temperature of activation $Q$ and $T_0$, respectively, and $R$ is the universal gas constant. Combining Equations (\ref{eqn:real-area-contact}-\ref{eqn:constitutive-basic}), we obtain the constitutive law
\begin{equation}\label{eqn:constitutive-main}
\begin{aligned}
V=V_0&\left(\frac{\tau}{c_0+\mu_0\bar{\sigma}}\right)^\frac{\mu_0}{a}\left(\frac{\theta V_0}{L}\right)^{-\frac{b}{a}}\exp\left[-\frac{Q}{R}\left(\frac{1}{T}-\frac{1}{T_0}\right)\right]~.
\end{aligned}
\end{equation}
The multiplicative form above accommodates a wide range of sliding velocities and does not require additional regularization for vanishing slip speed. When the shear traction approaches or exceeds the yield strength, rapid failure occurs. Fault slip also occurs below the yield strength, but the rates are vanishingly small. 

The radius of curvature of micro-asperities at contact junctions modulates the fault strength, and any process that flattens the micro-asperities leads to healing of the fault interface and increased frictional resistance. Different microphysical mechanisms, such as viscous creep, pressure-solution creep, and sub-critical crack growth, can contribute to compaction of the fault gouge. Within a specific range of temperature and slip-rates, a single healing mechanism operates and the evolution law can be written as the aging law in non-isothermal conditions
\begin{equation}\label{eqn:evolution-main-model}
\begin{aligned}
\dot{\theta}=\exp\left[\!-\frac{H}{R}\left(\frac{1}{T}-\frac{1}{T_o}\right)\right]-\frac{V\theta}{L}~,
\end{aligned}
\end{equation}
where the first term on the right-hand side is an Arrhenius law with the energy and temperature of activation $H$ and $T_0$, respectively. The generalization of the slip law in non-isothermal condition is written
\begin{equation}\label{eqn:evolution-main-model-slip-law}
\dot{\theta}=-\frac{V\theta}{L}\bigg[\ln \frac{V\theta}{L}+\frac{H}{R}\bigg(\frac{1}{T}-\frac{1}{T_0}\bigg)\bigg]~.
\end{equation}
At steady-state, the strength simplifies to
\begin{equation}\label{eqn:friction-steady-state}
\tau=\left(c_0+\mu_0\bar{\sigma}\right)\left(\frac{V}{V_0}\right)^\frac{a-b}{\mu_0}\exp\left[\frac{aQ-bH}{\mu_0 R}\left(\frac{1}{T}-\frac{1}{T_0}\right)\right]~,
\end{equation}
indicating a remaining rate and temperature dependence. The velocity dependence of steady-state friction depends on $a-b$, with positive and negative values representing velocity-strengthening and velocity-weakening friction, respectively. The temperature dependence of steady-state friction is controlled by $aQ-bH$, with positive and negative values corresponding to temperature-softening and temperature-hardening, respectively. Temperature-hardening is possible because healing is thermally activated and increases strength. 

Despite the seemingly complex formulation of the constitutive law, it is an extension of the rate-independent Coulomb failure criterion. The traction-velocity relationship of Equation~(\ref{eqn:constitutive-main}) can be inverted to obtain
\begin{equation}\label{eqn:friction-main-model}
\tau=\left(c_0+\mu_0\bar{\sigma}\right)\left(\frac{V}{V_0}\right)^\frac{a}{\mu_0}\left(\frac{\theta V_0}{L}\right)^\frac{b}{\mu_0}\exp\left[\frac{aQ}{\mu_0 R}\left(\frac{1}{T}-\frac{1}{T_0}\right)\right]~.
\end{equation}
As $a\ll 1$ and $b\ll1$, there is a weak rate, state, and temperature dependence of shear traction. Hence, the relation
\begin{equation}
\tau\approx c_0+\mu_0\bar{\sigma}
\end{equation}
represents a useful approximation of the average fault strength. Using the Taylor series approximation $x^y\approx 1+y\ln x$ for $y\ll1$ and $c_0=0$, Equation~(\ref{eqn:friction-main-model}) simplifies to the additive formulation
\begin{equation}\label{eqn:additive-form}
\tau\approx \left[\mu_0+a\ln\frac{V}{V_0}+b\ln\frac{\theta V_0}{L}+a\frac{Q}{ R}\left(\frac{1}{T}-\frac{1}{T_0}\right)\right]\bar{\sigma}~,
\end{equation}
which is ill-posed at sufficiently small slip-rates. However, the parameters $\mu_0$, $a$, $b$, and $L$ play the same roles in the additive and multiplicative forms. 


\subsection{Rheology}

Unicycle includes the mechanical coupling between fault slip and viscoelastic deformation. In the viscoelastic domain, we adopt a rheological framework that captures the transient creep and the steady-state creep of rocks. The bulk deformation of any rocks parcel is modeled with a Burgers assembly where the dashpots obey a nonlinear stress versus strain-rate relationship~\citep{masuti+16,masuti+barbot21}. Unicycle implements a formulation involving tensors, but we describe the scalar form of the constitutive law for simplicity. The total anelastic strain-rate is given by 
\begin{equation}
\dot{\epsilon}^i=\dot{\epsilon}_K+\dot{\epsilon}_M~,
\end{equation}
where $\dot{\epsilon}_K$ and $\dot{\epsilon}_M$ are the instantaneous strain rates in the Kelvin element and the Maxwell element, respectively. The stress versus strain-rate relationship in the Maxwell dashpot is given by~\citep{karato+jung03,hirth+kohlstedt03}
\begin{equation}\label{eqn:maxwell}
\dot{\varepsilon}_\mathrm{M} =A\,\sigma^{n}\exp\!\left(-\frac{E}{RT}\right)~,
\end{equation}
where $\sigma$ is the norm of the deviatoric stress, $n$ is the stress exponent, and $T$ is the absolute temperature. The pre-exponential factor $A$ must include the effects of water fugacity and grain size. The activation energy $E$ must also include the effect of ambient pressure through an activation volume. The effective viscosity varies dynamically, depending on temperature, stress, and cumulative plastic strain. The strain-rate in the Kelvin dashpot is a function of the effective stress
\begin{equation}\label{eqn:effective-stress}
q=\sigma-2G_\mathrm{K}\varepsilon_\mathrm{K}~,
\end{equation}
where $\varepsilon_\mathrm{K}$ is the cumulative strain in the Kelvin element, and $G_\mathrm{K}$ is a work-hardening coefficient. The cumulative strain in the Kelvin dashpot can be viewed as a state variable for viscoelastic flow. The product $2G_\mathrm{K}\varepsilon_\mathrm{K}$ represents the internal stress associated with strain partitioning among several dislocation planes within a representative volume element. The strain-rate in the Kelvin dashpot is then given by
\begin{equation}\label{eqn:kelvin}
\dot{\varepsilon}_\mathrm{K} = A\,q\,||q||^{n-1} \,\mathrm{exp}\!\left(-\frac{E}{RT}\right)~.
\end{equation}
In general, the constitutive parameters for the Maxwell and Kelvin elements may differ, but we use the same variables for simplicity. 

\section{The integral method}

\subsection{Conservation of momentum}

Unicycle simulates the dynamics of fault slip and viscoelastic flow during the seismic cycle with the integral method~\citep{barbot18b,barbot20a}. The approach extends the boundary integral method to include both surface and volume elements. As anelastic strain accrues in the viscoelastic domain, the change of fault traction depends on the surrounding fault slip and the distribution of strain in the ductile domains. The conservation of momentum can be written in integral form, where the rate of change of shear traction along the fault becomes
\begin{equation}\label{eqn:greens-traction}
\dot{\boldsymbol{\tau}}=\int_{\partial\Gamma}\textbf{K}\cdot(\textbf{v}-\textbf{v}_L)\,\mathrm{d}\!A+\int_{\Delta}\textbf{J}\cdot(\dot{\boldsymbol{\epsilon}}-\dot{\boldsymbol{\epsilon}}_L)\,\mathrm{d}V-\frac{G}{2V_S}\dot{\textbf{V}}~,
\end{equation}
where $\textbf{K}$ is the stress kernel for slip on the megathrust surface $\partial\Gamma$, $\textbf{v}$ is the instantaneous slip velocity vector, $\textbf{v}_L$ is the local loading rate on the fault, $\dot{\boldsymbol{\epsilon}}$ represents the anelastic strain-rate tensor in the ductile domain $\Delta$, and $\textbf{J}$ is another stress kernel connecting distributed deformation to fault traction. The last term on the right-hand side of equation (\ref{eqn:greens-traction}) corresponds to radiation-damping with the rigidity $G$, shear wave velocity $V_S$ and slip acceleration vector $\dot{\textbf{V}}$~\citep{rice93}. The shear traction and the slip velocity vectors are co-aligned, varying only in magnitude. Similarly, the evolution of stress in the ductile region can be written
\begin{equation}\label{eqn:greens-stress}
\dot{\boldsymbol{\sigma}}=\int_{\Delta}\textbf{M}\cdot(\dot{\boldsymbol{\epsilon}}-\dot{\boldsymbol{\epsilon}}_L)\,\mathrm{d}V+\int_{\partial\Gamma}\textbf{L}\cdot(\textbf{v}-\textbf{v}_L)\,\mathrm{d}\!A~,
\end{equation}
where $\textbf{M}$ is stress kernel associated with viscoelastic flow and $\textbf{L}$ is another stress kernel associated with fault slip. The plastic strain-rate and stress tensors are also co-aligned, characterized by different norms, but the same principal directions. 

The dynamics of deformation includes four directions of coupling captured by the stress kernels $\textbf{M}$ for fault slip self-interactions, $\textbf{J}$ for strain reloading of the fault, $\textbf{M}$ for viscoelastic relaxation, and $\textbf{L}$ for post-seismic deformation. The formulation assumes a long-term slip-rate $\textbf{v}_L$ and strain-rate $\dot{\boldsymbol{\epsilon}}_L$, both possibly non-uniform, for which no stress is accumulating. Combining the constitutive laws for frictional resistance and viscoelastic flow with the above equations forms a closed system that can be evaluated numerically. The integral equations~(\ref{eqn:greens-traction}) and~(\ref{eqn:greens-stress}) are approximated with finite sums over surface and volume elements of finite size. The traction and stress in a half-space caused by fault slip are calculated with the analytic solutions of~\cite{okada92} for rectangle surface elements and of~\cite{nikkhoo+15} for triangle surface elements. For the volume elements, we use a numerical solution that averages the stress over the whole volume to avoid numerical instabilities~\citep{shi+22}. 

\subsection{Numerical quadrature}

The mechanical system obeys a governing equation that can be written in canonical form as
\begin{equation}
\dot{\textbf{y}}=f(t,\textbf{y})~,
\end{equation}
where $\textbf{y}$ is a state vector containing the relevant dynamic variables, $t$ is time, and $f(t,\textbf{y})$ is a nonlinear function representing coupled ordinary equations. The dynamic variables can be separated into those associated with surface elements describing fault dynamics
\begin{equation}
\textbf{y}^s=\left(\begin{matrix}
\textbf{s} \\
\textbf{t} \\
\ln \theta \\
\ln V 
\end{matrix}\right)~,
\end{equation}
and those associated with volume elements describing distributed deformation
\begin{equation}
\textbf{y}^v=\left(\begin{matrix}
\boldsymbol{\sigma} \\
\boldsymbol{\epsilon}_M \\
\boldsymbol{\epsilon}_K \\
\end{matrix}\right)~,
\end{equation}
where $\boldsymbol{\sigma}$ is the stress tensor, $\boldsymbol{\epsilon}_M$ is the cumulative strain in the Maxwell element, and $\boldsymbol{\epsilon}_K$ is the cumulative strain in the Kelvin element, such that
\begin{equation}
\textbf{y}=\left(\begin{matrix}
\textbf{y}^s \\
\textbf{y}^v
\end{matrix}\right)~.
\end{equation} 
The time stepping is then evaluated numerically using the fourth/fifth-order Runge-Kutta method that also provides adaptive time steps~\citep{press+92}. The code implements a triangle or rectangle mesh for the faults and a cuboid or tetrahedron mesh for the ductile substrate. Using tetrahedra significantly increases the degrees of freedom, as, for example, it takes 6 tetrahedra to fill a cuboid, slowing down the calculation. 

\clearpage

\section{Unicycle software}

\subsection{Installation}

To build the \emph{Unicycle} programs from the code base, you must have a Fortran compiler (\verb$gcc$ version 9 or above), the \verb$lapack$, \verb$netcdff$, and \verb$openmpi$ libraries, and a Message Passing Interface (MPI) compiler. The compilation is conducted with \verb$Makefile$. To conform with your system configuration, make a copy of a template Makefile and link with a generic name. For example,
\begin{verbatim}
    cd unicycle/fortran/2d/antiplane
    cp makefile_brew makefile_my
    ln -s makefile_my makefile
\end{verbatim}
Then, update the makefile with your system preferences (e.g., \verb$vi makefile$), including compiler name, include and library paths. Finally, build the code with
\begin{verbatim}
    make unicycle-ap-ratestate
    make unicycle-ap-viscouscycles
\end{verbatim}
The codes with the \verb$-ratestate$ suffix only resolve fault dynamics. The codes with the \verb$-viscouscycles$ suffix resolve the coupling between fault friction and viscoelastic flow. The binary files will be located in the directory \verb$unicycle/2d/antiplane/build/$. 

A simple codebase without parallelism is available in \verb$unicycle/2d/antiplane-serial$ for educational purposes. To compile and build, use the following commands
\begin{verbatim}
    cd unicycle/fortran/2d/antiplane-serial
    makefile -f makefile_brew
\end{verbatim}
All other Fortran-based codes are parallelized with MPI. To compile the remaining binaries, repeat the operation with
\begin{verbatim}
    cd unicycle/2d/planestrain
    cp makefile_brew makefile_my
    ln -s makefile_my makefile
    make unicycle-ps-ratestate
    make unicycle-ps-viscouscycles
\end{verbatim}
For three-dimensional calculations, compile the parallel implementation with
\begin{verbatim}
    cd unicycle/3d
    cp makefile_brew makefile_my
    ln -s makefile_my makefile
    make unicycle-3d-ratestate
    make unicycle-3d-viscouscycles
\end{verbatim}
The binary files will be located in the corresponding \verb$build$ directories.



\subsection{Execution}

Example input files are located in the \verb$examples/tutorials$ directories. To run the bash scripts, add the code path to the \verb$PATH$ environment variable,
\begin{verbatim}
    export PATH=$PATH:unicycle/fortran/2d/antiplane/build
    export PATH=$PATH:unicycle/fortran/2d/planestrain/build
    export PATH=$PATH:unicycle/fortran/3d/build
\end{verbatim}
The best way to manage the environment variables in a posix environment is to use \verb$module$, which can be found at \url{https://modules.sourceforge.net/}. Then, run the bash scripts. For two-dimensional models in antiplane strain, the bash scripts \verb$brittle1.sh$ and \verb$brittle2.sh$ simulate earthquake and slow-slip cycles, respectively, on a vertical strike-slip fault.
\begin{verbatim}
    cd unicycle/2d/antiplane/examples/tutorials
    ./brittle1.sh
    ./brittle2.sh
\end{verbatim}
For two-dimensional models in condition of in-plane strain, the bash scripts \verb$brittle1.sh$ simulates cycles of earthquakes on a subduction megathrust. The script \verb$viscous1.sh$ simulate seismic cycles on a megathrust, but includes viscoelastic flow in the oceanic asthenosphere and mantle wedge. The model uses line elements to represent fault slip and triangle elements to represent plastic strain.
\begin{verbatim}
    cd unicycle/2d/planestrain/examples/tutorials
    ./brittle1.sh
    ./viscous1.sh
\end{verbatim}
For three-dimensional models, \verb$brittle1.sh$ computes seismic cycle in a 3d elastic half-space for the case of a right-lateral strike-slip fault; \verb$brittle2.sh$ computes seismic cycles in an elastic half-space for a 30$^\circ$-dipping thrust fault; \verb$brittle3.sh$ computes seismic cycles in an elastic half-space for the case of a 60$^\circ$-dipping normal fault.
\begin{verbatim}
    cd unicycle/3d/examples/tutorials
    ./brittle1.sh
    ./brittle2.sh
    ./brittle3.sh
\end{verbatim}
Tutorial models coupling fault dynamics and viscoelastic flow can be found in the same folder. \verb$viscous1.sh$ computes seismic cycle in a 3d viscoelastic medium for a right-lateral strike-slip fault using structured mesh of cuboid elements in the ductile substrate. \verb$viscous4.sh$ does the same using a tetrahedral mesh to capture the curvature of the Moho. Other examples include thrust and normal faults.

\clearpage

\bibliographystyle{agu}
\small
\bibliography{./unicycle}


\end{document}
