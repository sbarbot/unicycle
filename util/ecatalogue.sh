#!/bin/bash

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# script ecatalogue.sh
# build an event catalogue from the output file time.dat of unicycle.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

set -e
self=$(basename $0)
selfdir=$(dirname $0)
cmdline=$*
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

catalogue(){
	grep -v "#" "$IN" | \
	awk -v vci=$VCI -v vco=$VCO -v wdir=$WDIR -v vcol=$VINDEX -v mcol=$MINDEX 'function log10(x){return log(x)/log(10)} \
		BEGIN{
			E=0;
			tstartprev=0;
			Mo=0;
			i=0;
		}{ \
			if($vcol>vci && 0==E){
				E=1;
				i=i+1;
				tstart=$1;
				istart=NR;
				vmax=$vcol;
				next;
			};
			if(1==E){
				Mo=Mo+$mcol*$2;
				if (vmax<$vcol){
					vmax=$vcol;
				};
				if ($vcol<vco){
					E=0;
					printf "%06d %09d %09d %23.17e %23.17e %12.6e %12.7f %12.6e %13.4e  %s\n", \
						i,istart,NR,tstart,$1,$1-tstart,log10(vmax),(tstart-tstartprev),Mo,wdir;
					tstartprev=tstart
					Mo=0;
				}
			}
		}'

}

usage(){
        echo "usage: $self [-b 1e-1/1e-3] [-h] time.dat"
	echo "or"
        echo "usage: $self [-b 1e-1/1e-3] [-h] time1.dat ... timeN.dat"
	echo ""
        echo "options:"
        echo "         -b initial/final sets the initial and final event velocities [0.1/0.001]"
        echo "         -h display this error message and exit"
        echo "         -m column index for moment-rate [4]"
        echo "         -v column index for slip-rate [3]"
        echo "         -t use standard input"
	echo ""
	echo "Creates an event catalogue based on information stored in unicycle time.dat"
	echo "output (or from standard input)."
	echo ""
        
        exit
}

while getopts "b:hm:v:" flag
do
	case "$flag" in
		b) bset=1;BOUND=$OPTARG;;
		h) hset=1;;
		m) mset=1;MINDEX=$OPTARG;;
		v) vset=1;VINDEX=$OPTARG;;
	esac
done
for item in $bset $mset $vset;do
	shift;shift
done
for item in $hset;do
	shift;
done

if [ "$hset" == "1" ]; then
	usage
	exit
fi

if [ "$bset" == "1" ]; then
	VCI=`echo $BOUND | awk -F "/" '{print $1}'`
	VCO=`echo $BOUND | awk -F "/" '{print $2}'`
else
	VCI=0.1
	VCO=0.001
fi

if [ "$mset" != "1" ]; then
	MINDEX=4;
fi

if [ "$vset" != "1" ]; then
	VINDEX=3;
fi

echo "# $self $cmdline"
echo "# using bounds ${VCI}/${VCO}"

echo "#    n,  start i,    end i,         start time (s),           end time (s), duration(s), log10(vmax),      Tr (s), Moment (N m), directory";

if [ ! -t 0 ]; then
	IN="-"
	WDIR="-"
	catalogue
else
	while [ "$#" != "0" ];do
		WDIR="$(dirname $1)"
		IN="$WDIR/time.dat"
		catalogue
		shift
	done
fi
