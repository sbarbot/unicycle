#!/bin/bash

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# script isochrons.sh
# build seismic and aseismic isochrons from the output file time.dat of unicycle.
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

set -e
self=$(basename $0)
selfdir=$(dirname $0)
cmdline=$*
trap 'echo $self: Some errors occurred. Exiting.; exit' ERR

usage(){
        echo "usage: $self [-b 1e-1/1e-3] [-h] time.dat"
	echo "or"
        echo "usage: $self [-b 1e-1/1e-3] [-h] time1.dat ... timeN.dat"
	echo ""
        echo "options:"
        echo "         -b initial/final sets the initial and final event velocities [0.1/0.001]"
        echo "         -i time steps for seismic and aseismic phases [1/3.15e7]"
        echo "         -h display this error message and exit"
	echo ""
	echo "Creates seismic and aseismic isochrons based on information stored in unicycle"
	echo "time.dat output."
	echo ""
        
        exit
}

while getopts "b:hi:" flag
do
	case "$flag" in
		b) bset=1;BOUND=$OPTARG;;
		i) iset=1;INTERVAL=$OPTARG;;
		h) hset=1;;
	esac
done
for item in $bset $iset;do
	shift;shift
done
for item in $hset;do
	shift;
done

if [ "$hset" == "1" ]; then
	usage
	exit
fi

if [ "$bset" == "1" ]; then
	IFS=/ read VCI VCO <<< "$(echo $BOUND)"
else
	VCI=0.1
	VCO=0.001
fi

if [ "$iset" == "1" ]; then
	IFS="/" read DTS DTA <<< "$(echo "$INTERVAL")"
else
	DTS=1
	DTA=3.15e7
fi

echo "# $self $cmdline"
echo "# using bounds ${VCI}/${VCO}"
echo "# using intervals ${DTS}/$DTA"

while [ "$#" != "0" ];do

	WDIR=$(dirname $1)

	ISOCHRONA=$WDIR/isochrons-aseismic.dat
	ISOCHRONS=$WDIR/isochrons-seismic.dat

	echo "# processing $ISOCHRONA"

	grep -v "#" $WDIR/time.dat | \
		awk -v dt=$DTA '
		BEGIN{
			t=dt;
			print "# using interval ", dt;
			print "#    n         time         vmax";
		}{
			if ($1>=t){
				printf "%06d %e %e\n",NR,$1,$3;
				t=t+dt;
			}
		}' > $ISOCHRONA

	echo "# processing $ISOCHRONS"
	grep -v "#" $WDIR/time.dat | \
		awk -v vci=$VCI -v vco=$VCO -v dt=$DTS 'function log10(x){return log(x)/log(10)} \
		BEGIN{
			E=0;
			c=0;
			i=0;
			print "#    n              time     vmax event"
		}{ \
			if($3>vci && 0==E){
				c=c+1;
				E=1;
				t=$1+dt;
				vmax=$3
				printf "%06d %f %f %04d\n",NR,$1,$3,c;
				next;
			};
			if(1==E){
				if (vmax<$3){
					vmax=$3;
				}
				if ($1>=t){
					printf "%06d %f %f %04d\n",NR,$1,$3,c;
					t=t+dt;
				};
				if ($3<vco){
					E=0;
				}
			}
		}' > $ISOCHRONS

	shift
done

